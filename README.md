# lilitools

A set of tools and save editors for [Lilith's Throne](https://github.com/Innoxia/liliths-throne-public).

[[_TOC_]]

## A Note

As a save editor, lilitools is _not_ supported by the developers of Lilith's Throne or the game's modders. By using lilitools, you can potentially break saves and trigger bugs. **You use lilitools at your own risk**.

So, **don't bug LT devs about bugs triggered by lilitools** in LT. They aren't responsible, and can't fix our problems anyway.

## Mod Support

Most mod XMLs are supported. Source mods are not currently supported.

## Installing

### From Source (Linux)

- Install [CPython](https://python.org) >= 3.9.
- Install git.

```shell
# Install using pip
pip install git+https://gitlab.com/Anonymous-BCFED/lilitools.git
```

### Binary

Not ready yet.

## Updating

### From Source

```shell
# Update using pip
pip install -U git+https://gitlab.com/Anonymous-BCFED/lilitools.git
```

## More Information

**[Documentation Site](https://anonymous-bcfed.gitlab.io/lilitools-docs)** ([Repo](https://gitlab.com/Anonymous-BCFED/lilitools-docs))

These files are carefully-curated saves with sample data in them.

**They are not intended to be played on.**

# Characters
## Male

### Buildout
* Name: (Male,Male,Male) Sample
* Sex: Male
* Femininity: Masculine
* Sexuality: Ambiphilic
* Traits: Confident, Kind, Naive, Brave, Innocent

## Female

### Buildout
* Name: (Female,Female,Female) Sample
* Sex: Female
* Femininity: Feminine
* Sexuality: Ambiphilic
* Traits: Confident, Kind, Naive, Brave, Innocent

# Updating

1. Select clean, unmodded build of LT
1. `. devtools/DEPLOY_SAVE_SAMPLES.sh`
1. bin/lilirun
    1. Open Male
    1. IMMEDIATELY save over Male
    1. close
1. bin/lilirun
    1. Open Female
    1. IMMEDIATELY save over Female
    1. close
1. `. devtools/COLLECT_SAVE_SAMPLES.sh`
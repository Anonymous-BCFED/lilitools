SAVEPATH=dist/data/saves
SAMPLEPATH=data/samples/saves
function collectSave() {
    orig=$SAVEPATH/$1.xml
    raw=$SAMPLEPATH/$2.xml
    python devtools/idiotCheck.py $orig || exit 1
    cleaned=$SAMPLEPATH/$2.cleaned.xml
    echo "CP    $orig -> $raw"
    cp $orig $raw
    echo "DUMP  $orig -> $cleaned"
    lilitool save dump $orig $cleaned
}

collectSave Male male
python devtools/dumpFromXMLTo.py $SAVEPATH/Male.xml ./playerCharacter/character/characterInventory -o tests/data/character/character_inventory/full.xml

collectSave Female female
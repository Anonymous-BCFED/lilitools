set -ex

cd docs-src
git clean -fdx
git reset --hard
cd ..
set +ex
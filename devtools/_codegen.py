import ast
from pathlib import Path
from typing import Optional, TextIO

import black
import isort
from mkast.formatters.autoflake import AutoflakeFormattingProvider
from mkast.formatters.base import BaseFormattingProvider
from mkast.formatters.black import BlackFormattingProvider
from mkast.formatters.isort import ISortFormattingProvider
from mkast.generators.astor import AstorCodeGenerator

__all__ = [
    "LTExpandedCodeGenerator",
    "LTMinifiableCodeGenerator",
    "PythonMinifierFormattingProvider",
]

def writeGeneratedPythonWarningHeader(
    f: TextIO,
    purpose: str,
    generator_filepath: Path,
    source_filepath: Optional[Path] = None,
) -> None:
    lines = [
        "WARNING: @GENERATED CODE; DO NOT EDIT BY HAND",
        f"BY {generator_filepath.absolute().relative_to(Path.cwd()).as_posix()}",
    ]
    if source_filepath is not None:
        lines.append(
            f"FROM {generator_filepath.absolute().relative_to(Path.cwd()).as_posix()}"
        )
    lines.append(purpose)
    maxlen = max([len(x) for x in lines]) + 2  # padding
    startstop = "#" * (maxlen + 2) + "\n"
    f.write(startstop)
    for l in lines:
        f.write(f'#{l.center(maxlen," ")}#\n')
    f.write(startstop)


class PythonMinifierFormattingProvider(BaseFormattingProvider):
    def __init__(self) -> None:
        super().__init__()

    def formatCode(self, code: str) -> str:
        try:
            import python_minifier

            return python_minifier.minify(code)
        except:
            return code


class LTMinifiableCodeGenerator(AstorCodeGenerator):
    MINIFY: bool = False

    def __init__(self) -> None:
        super().__init__()
        cfg = isort.Config(
            line_length=65355,
            ignore_whitespace=True,
            quiet=True,
            lines_after_imports=0,
            lines_before_imports=0,
            lines_between_sections=0,
            lines_between_types=0,
        )
        self.addFormatter(ISortFormattingProvider(cfg))
        self.addFormatter(AutoflakeFormattingProvider(remove_all_unused_imports=True))
        if self.MINIFY:
            self.addFormatter(ISortFormattingProvider(cfg))
            self.addFormatter(PythonMinifierFormattingProvider(cfg))
        else:
            self.addFormatter(BlackFormattingProvider(black.Mode(line_length=200)))
            self.addFormatter(ISortFormattingProvider(cfg))


class LTExpandedCodeGenerator(AstorCodeGenerator):
    def __init__(self) -> None:
        super().__init__()
        icfg = isort.Config(
            line_length=65355,
            ignore_whitespace=True,
            quiet=True,
            lines_after_imports=0,
            lines_before_imports=0,
            lines_between_sections=0,
            lines_between_types=0,
        )
        self.addFormatter(ISortFormattingProvider(icfg))
        self.addFormatter(AutoflakeFormattingProvider(remove_all_unused_imports=True))
        self.addFormatter(BlackFormattingProvider(black.Mode(line_length=200)))
        self.addFormatter(ISortFormattingProvider(icfg))

    def generate(self, node: ast.AST) -> str:
        o: str = self._unparse(node)
        o = o.replace(",", ",\n")
        try:
            for fp in self.formattingProviders:
                o = fp.formatCode(o)
        except Exception as e:
            print(o)
            raise e
        return o

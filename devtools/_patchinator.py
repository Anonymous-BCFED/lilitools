from __future__ import annotations

import os
import re
import typing
import uuid
from abc import ABC, abstractmethod
from enum import IntEnum, auto
from pathlib import Path
from typing import Callable, Final, List, Optional, Set, TextIO, TypeAlias, Union, cast

import click
import construct

GREEN_CHECKMARK = click.style('✓', fg='green')
RED_CROSS = click.style('✗', fg='red')


def _sop2pattern(inp: StrOrPattern) -> re.Pattern:
    if isinstance(inp, str):
        return re.compile(re.escape(inp))
    else:
        return inp


def _str2pat(s: str) -> re.Pattern:
    return re.compile(s)


class EActionType(IntEnum):
    FIND_STRIPPED_LINE = auto()
    FIND_UNSTRIPPED_LINE = auto()
    REPLACE_LINE = auto()
    REPLACE_ALL = auto()
    REMOVE_UNSTRIPPED_LINE = auto()
    REMOVE_STRIPPED_LINE = auto()
    INJECT_LINES = auto()
    FINISH = auto()
    REMOVE_ALL = auto()


class PatchinatorAction(ABC):
    ID: EActionType

    def __init__(self, pp: PatchinatorPatch, desc: str) -> None:
        self.pp: PatchinatorPatch = pp
        self.pnor: PatchinatorFile = pp.pnor
        self.desc: str = desc

    @property
    def infile(self) -> TextIO:
        assert self.pp.pnor.infile is not None
        return self.pp.pnor.infile

    @property
    def outfile(self) -> TextIO:
        assert self.pp.pnor.outfile is not None
        return self.pp.pnor.outfile

    def __str__(self) -> str:
        return self.desc

    @abstractmethod
    def act(self) -> bool: ...

    def execute(self) -> None:
        if self.act():
            click.secho(f'      {GREEN_CHECKMARK} {self}')
        else:
            click.secho(f'      {RED_CROSS} {self}')
            raise FailedToMatchAnythingException()


class FindStrippedLineAction(PatchinatorAction):
    ID = EActionType.FIND_STRIPPED_LINE

    def __init__(self, pp: PatchinatorPatch, desc: str, p: re.Pattern, pass_thru: bool = True) -> None:
        super().__init__(pp, desc)
        self.pattern: re.Pattern = p
        self.pass_thru: bool = pass_thru

    def act(self) -> bool:
        while True:
            self.pnor.current_line += 1
            ln = self.infile.readline()
            if not ln:
                return False
            stripped = ln.strip()
            if (m := self.pattern.match(stripped)) is not None:
                self.desc += f' (Found @ {self.pnor.current_line})'
                if self.pass_thru:
                    self.outfile.write(ln)
                return True
            self.outfile.write(ln)
        return False


class ReplaceLineAction(PatchinatorAction):
    ID = EActionType.REPLACE_LINE

    def __init__(self, pp: PatchinatorPatch, desc: str, p: re.Pattern, replacement: Union[str, Callable[[re.Match], str]]) -> None:
        super().__init__(pp, desc)
        self.pattern: re.Pattern = p
        self.replacement: Union[str, Callable[[re.Match], str]] = replacement

    def act(self) -> bool:
        while True:
            self.pnor.current_line += 1
            ln = self.infile.readline()
            if not ln:
                return False
            if (m := self.pattern.match(ln)) is not None:
                self.desc += f' (Found @ {self.pnor.current_line})'
                b4 = ln
                ln = self.pattern.sub(self.replacement, ln)
                self.desc += f' {b4!r} -> {ln!r}'
                self.outfile.write(ln)
                return True
            self.outfile.write(ln)
        return False


class ReplaceAllAction(PatchinatorAction):
    ID = EActionType.REPLACE_ALL

    def __init__(self, pp: PatchinatorPatch, desc: str, p: re.Pattern, replacement: Union[str, Callable[[re.Match], str]]) -> None:
        super().__init__(pp, desc)
        self.pattern: re.Pattern = p
        self.replacement: Union[str, Callable[[re.Match], str]] = replacement

    def act(self) -> bool:
        finds = 0
        while True:
            self.pnor.current_line += 1
            ln = self.infile.readline()
            if not ln:
                self.desc += f' (Found {finds} matches)'
                return True
            if (m := self.pattern.match(ln)) is not None:
                finds += 1
                b4 = ln
                ln = self.pattern.sub(self.replacement, ln)
                self.outfile.write(ln)
            self.outfile.write(ln)
        self.desc += f' (Found {finds} matches)'
        return True


class FindUnstrippedLineAction(PatchinatorAction):
    ID = EActionType.FIND_UNSTRIPPED_LINE

    def __init__(self, pp: PatchinatorPatch, desc: str, p: re.Pattern, pass_thru: bool = True) -> None:
        super().__init__(pp, desc)
        self.pattern: re.Pattern = p
        self.pass_thru: bool = pass_thru

    def act(self) -> bool:
        while True:
            self.pnor.current_line += 1
            ln = self.infile.readline()
            if not ln:
                return False
            if (m := self.pattern.match(ln)) is not None:
                self.desc += f' (Found @ {self.pnor.current_line})'
                if self.pass_thru:
                    self.outfile.write(ln)
                return True
            self.outfile.write(ln)
        return False


class RemoveUnstrippedLineAction(PatchinatorAction):
    ID = EActionType.REMOVE_UNSTRIPPED_LINE

    def __init__(self, pp: PatchinatorPatch, desc: str, p: re.Pattern) -> None:
        super().__init__(pp, desc)
        self.pattern: re.Pattern = p

    def act(self) -> bool:
        while True:
            self.pnor.current_line += 1
            ln = self.infile.readline()
            if not ln:
                return False
            if (m := self.pattern.match(ln)) is not None:
                self.desc += f' (Removed @ {self.pnor.current_line})'
                return True
            self.outfile.write(ln)
        return False


class RemoveStrippedLineAction(PatchinatorAction):
    ID = EActionType.REMOVE_STRIPPED_LINE

    def __init__(self, pp: PatchinatorPatch, desc: str, p: re.Pattern) -> None:
        super().__init__(pp, desc)
        self.pattern: re.Pattern = p

    def act(self) -> bool:
        while True:
            self.pnor.current_line += 1
            ln = self.infile.readline()
            if not ln:
                return False
            if (m := self.pattern.match(ln.strip())) is not None:
                self.desc += f' (Removed @ {self.pnor.current_line})'
                return True
            self.outfile.write(ln)
        return False


class RemoveAllAction(PatchinatorAction):
    ID = EActionType.REMOVE_ALL

    def __init__(self, pp: PatchinatorPatch, desc: str, p: re.Pattern) -> None:
        super().__init__(pp, desc)
        self.pattern: re.Pattern = p

    def act(self) -> bool:
        finds = 0
        while True:
            self.pnor.current_line += 1
            ln = self.infile.readline()
            if not ln:
                self.desc += f' (Found {finds} matches)'
                return True
            if (m := self.pattern.match(ln)) is not None:
                finds += 1
                continue
            self.outfile.write(ln)
        self.desc += f' (Found {finds} matches)'
        return True


class InjectLinesAction(PatchinatorAction):
    ID = EActionType.INJECT_LINES

    def __init__(self, pp: PatchinatorPatch, desc: str, newlines: List[str]) -> None:
        super().__init__(pp, desc)
        self.newlines: List[str] = newlines

    def act(self) -> bool:
        self.outfile.writelines(map(lambda x: x + '\n', self.newlines))
        return True


class FinishFile(PatchinatorAction):
    ID = EActionType.FINISH

    def __init__(self, pp: PatchinatorPatch) -> None:
        super().__init__(pp, desc='Finish sending lines to the new file.')

    def act(self) -> bool:
        while True:
            self.pnor.current_line += 1
            ln = self.infile.readline()
            if not ln:
                return True
            self.outfile.write(ln)
        return True


class FailedToMatchAnythingException(Exception):
    pass


StrOrPattern: TypeAlias = Union[str, re.Pattern]


class PatchinatorPatch:
    def __init__(self, pnor: PatchinatorFile, name: str) -> None:
        self.pnor: PatchinatorFile = pnor
        self.name: str = name
        self.actions: List[PatchinatorAction] = []
        self.finished: bool = False

    @property
    def infile(self) -> Optional[TextIO]:
        return self.pnor.infile

    @property
    def outfile(self) -> Optional[TextIO]:
        return self.pnor.outfile

    def __enter__(self) -> PatchinatorPatch:
        return self

    def __exit__(self, a, b, c) -> None:
        if not self.finished:
            self.finish()
        return

    def execute(self) -> None:
        click.secho(f'    >>> {self.name}', fg='green')
        for a in self.actions:
            a.execute()

    def __str__(self) -> str:
        return self.name

    def findStrippedLine(self, needle: StrOrPattern, pass_thru: bool = True) -> PatchinatorPatch:
        assert not self.finished
        p = _sop2pattern(needle)
        self.actions.append(FindStrippedLineAction(self, f'Find (stripped) line matching {p.pattern!r}', p, pass_thru))
        return self

    def findUnstrippedLine(self, needle: StrOrPattern, pass_thru: bool = True) -> PatchinatorPatch:
        assert not self.finished
        p = _sop2pattern(needle)
        self.actions.append(FindUnstrippedLineAction(self, f'Find line matching {p.pattern!r}', p, pass_thru))
        return self

    def injectLines(self, lines: List[str]) -> PatchinatorPatch:
        assert not self.finished
        self.actions.append(InjectLinesAction(self, f'Add {len(lines)} lines', lines))
        return self

    def replaceLine(self, needle: StrOrPattern, replacement: Union[str, Callable[[re.Match], str]]) -> PatchinatorPatch:
        assert not self.finished
        p = _sop2pattern(needle)
        self.actions.append(ReplaceLineAction(self, f'Replace line matching {p.pattern!r}', p, replacement))
        return self

    def replaceAll(self, needle: StrOrPattern, replacement: Union[str, Callable[[re.Match], str]]) -> None:
        assert not self.finished
        p = _sop2pattern(needle)
        self.actions.append(ReplaceAllAction(self, f'Replace all lines matching {p.pattern!r}', p, replacement))
        self.finished = True

    def removeUnstrippedLine(self, needle: StrOrPattern) -> PatchinatorPatch:
        assert not self.finished
        p = _sop2pattern(needle)
        self.actions.append(RemoveUnstrippedLineAction(self, f'Remove line matching {p.pattern!r}', p))
        return self

    def removeStrippedLine(self, needle: StrOrPattern) -> PatchinatorPatch:
        assert not self.finished
        p = _sop2pattern(needle)
        self.actions.append(RemoveStrippedLineAction(self, f'Remove stripped line matching {p.pattern!r}', p))
        return self

    def removeAll(self, needle: StrOrPattern) -> None:
        assert not self.finished
        p = _sop2pattern(needle)
        self.actions.append(RemoveAllAction(self, f'Remove all lines matching {p.pattern!r}', p))
        self.finished = True

    def finish(self) -> None:
        assert not self.finished
        self.actions.append(FinishFile(self))
        self.finished = True


class PatchinatorFile:
    def __init__(self, pi: Patchinator, original: Path, patched: Path) -> None:
        self.pi: Patchinator = pi
        self.original_path: Path = original
        self.patched_path: Path = patched
        self.infile: Optional[TextIO] = None
        self.outfile: Optional[TextIO] = None

        self.current_line: int = 0

        self.patches: List[PatchinatorPatch] = []

    def addPatch(self, name: str) -> PatchinatorPatch:
        pp = PatchinatorPatch(self, name)
        self.patches.append(pp)
        return pp

    def __enter__(self) -> PatchinatorFile:
        return self

    def __exit__(self, a, b, c) -> None:
        return

    def patch(self) -> None:
        click.secho(f'  In {self.original_path}:')
        fq: List[Path] = [self.original_path]
        tempfiles: Set[Path] = set()
        for _ in range(len(self.patches)):
            tempfilename = self.original_path.with_name((uuid.uuid4().hex + '.tmp'))
            fq.append(tempfilename)
            tempfiles.add(tempfilename)
        try:
            for i in range(len(self.patches)):
                p = self.patches[i]
                infile = fq[i + 0]
                outfile = fq[i + 1]
                #print(f'{infile} -> {outfile}')
                with infile.open('r') as rf, outfile.open('w') as wf:
                    self.infile = rf
                    assert self.infile is not None
                    self.outfile = wf
                    assert self.outfile is not None
                    p.execute()
        finally:
            if os.path.isfile(fq[-1]):
                #print(f'{fq[-1]} -> {self.patched_path}')
                os.replace(fq[-1], self.patched_path)
            for tfn in tempfiles:
                if os.path.isfile(tfn):
                    os.unlink(tfn)


class Patchinator:
    def __init__(self, name: str) -> None:
        self.name: str = name
        self.files: List[PatchinatorFile] = []

    def addFile(self, infile: Path, outfile: Path) -> PatchinatorFile:
        pf = PatchinatorFile(self, infile, outfile)
        self.files.append(pf)
        return pf

    def patch(self) -> None:
        click.secho(f'{self.name}:')
        for pf in self.files:
            pf.patch()


class BinarySerializer:
    VERSION: Final[int] = 2023_05_31
    SIGNATURE: Final[bytes] = b'PATCHINATOR-DO NOT EDIT'
    PSTRING = construct.PascalString(construct.VarInt, 'utf-8')

    def __init__(self) -> None:
        pass

    def serialize(self, w: typing.BinaryIO, p: Patchinator, relto: Optional[Path] = None) -> None:
        w.write(self.SIGNATURE)
        w.write(self.VERSION.to_bytes(4, 'big', signed=False))
        self.PSTRING.build_stream(p.name, w)
        construct.VarInt.build_stream(len(p.files), w)
        for file in p.files:
            self._serialize_file(w, file, relto)

    def deserialize(self, w: typing.BinaryIO, relto: Optional[Path] = None) -> Patchinator:
        construct.Const(self.SIGNATURE).parse_stream(w)
        construct.Const(self.VERSION.to_bytes(4, 'big', signed=False)).parse_stream(w)

        p = Patchinator('')
        p.name = self.PSTRING.parse_stream(w)
        p.files = []
        for _ in range(construct.VarInt.parse_stream(w)):
            p.files.append(self._deserialize_file(w, p, relto))
        return p

    def _serialize_file(self, w: typing.BinaryIO, f: PatchinatorFile, relto: Optional[Path] = None) -> None:
        op = f.original_path
        pp = f.patched_path
        if relto is not None:
            op = op.relative_to(relto)
            pp = pp.relative_to(relto)
        self.PSTRING.build_stream(op.as_posix(), w)
        self.PSTRING.build_stream(pp.as_posix(), w)
        construct.VarInt.build_stream(len(f.patches), w)
        for p in f.patches:
            self._serialize_patch(w, f, p, relto)

    def _deserialize_file(self, w: typing.BinaryIO, p: Patchinator, relto: Optional[Path] = None) -> PatchinatorFile:
        inp = Path(self.PSTRING.parse_stream(w))
        outp = Path(self.PSTRING.parse_stream(w))
        if relto:
            inp = relto / inp
            outp = relto / outp
        f = PatchinatorFile(p, inp, outp)
        f.patches = []
        for _ in range(construct.VarInt.parse_stream(w)):
            f.patches.append(self._deserialize_patch(w, p, f))
        return f

    def _serialize_patch(self, w: typing.BinaryIO, f: PatchinatorFile, p: PatchinatorPatch, relto: Optional[Path] = None) -> None:
        self.PSTRING.build_stream(p.name, w)
        construct.VarInt.build_stream(len(p.actions), w)
        for a in p.actions:
            w.write(a.ID.to_bytes(1, 'big', signed=False))
            self.PSTRING.build_stream(a.desc, w)
            match a.ID:
                case EActionType.FIND_STRIPPED_LINE:
                    a = cast(FindStrippedLineAction, a)
                    self.PSTRING.build_stream(a.pattern.pattern, w)
                    w.write(b'\1' if a.pass_thru else b'\0')
                case EActionType.FIND_UNSTRIPPED_LINE:
                    a = cast(FindUnstrippedLineAction, a)
                    self.PSTRING.build_stream(a.pattern.pattern, w)
                    w.write(b'\1' if a.pass_thru else b'\0')
                case EActionType.REPLACE_LINE:
                    a = cast(ReplaceLineAction, a)
                    self.PSTRING.build_stream(a.pattern.pattern, w)
                    self.PSTRING.build_stream(a.replacement, w)
                case EActionType.REPLACE_ALL:
                    a = cast(ReplaceAllAction, a)
                    self.PSTRING.build_stream(a.pattern.pattern, w)
                    self.PSTRING.build_stream(a.replacement, w)
                case EActionType.REMOVE_UNSTRIPPED_LINE:
                    a = cast(RemoveUnstrippedLineAction, a)
                    self.PSTRING.build_stream(a.pattern.pattern, w)
                case EActionType.REMOVE_STRIPPED_LINE:
                    a = cast(RemoveStrippedLineAction, a)
                    self.PSTRING.build_stream(a.pattern.pattern, w)
                case EActionType.INJECT_LINES:
                    a = cast(InjectLinesAction, a)
                    construct.VarInt.build_stream(len(a.newlines), w)
                    for l in a.newlines:
                        self.PSTRING.build_stream(l, w)
                case EActionType.FINISH:
                    pass
                case EActionType.REMOVE_ALL:
                    a = cast(RemoveAllAction, a)
                    self.PSTRING.build_stream(a.pattern.pattern, w)

    def _deserialize_patch(self, w: typing.BinaryIO, p: Patchinator, f: PatchinatorFile, relto: Optional[Path] = None) -> PatchinatorPatch:

        name: str = self.PSTRING.parse_stream(w)
        pp = PatchinatorPatch(f, name)
        pp.actions = []
        for _ in range(construct.VarInt.parse_stream(w)):
            id = EActionType(int.from_bytes(w.read(1), 'big', signed=False))
            desc = self.PSTRING.build_stream(a.desc, w)
            a: PatchinatorAction
            match id:
                case EActionType.FIND_STRIPPED_LINE:
                    pattern = self.PSTRING.parse_stream(w)
                    pass_thru = w.read(1) == b'\1'
                    a = FindStrippedLineAction(pp, desc, _str2pat(pattern), pass_thru)
                case EActionType.FIND_UNSTRIPPED_LINE:
                    pattern = self.PSTRING.parse_stream(w)
                    pass_thru = w.read(1) == b'\1'
                    a = FindUnstrippedLineAction(pp, desc, _str2pat(pattern), pass_thru)
                case EActionType.REPLACE_LINE:
                    pattern = self.PSTRING.parse_stream(w)
                    replacement = self.PSTRING.parse_stream(w)
                    a = ReplaceLineAction(pp, desc, _str2pat(pattern), replacement)
                case EActionType.REPLACE_ALL:
                    pattern = self.PSTRING.parse_stream(w)
                    replacement = self.PSTRING.parse_stream(w)
                    a = ReplaceAllAction(pp, desc, _str2pat(pattern), replacement)
                case EActionType.REMOVE_UNSTRIPPED_LINE:
                    pattern = self.PSTRING.parse_stream(w)
                    a = RemoveUnstrippedLineAction(pp, desc, _str2pat(pattern))
                case EActionType.REMOVE_STRIPPED_LINE:
                    pattern = self.PSTRING.parse_stream(w)
                    a = RemoveStrippedLineAction(pp, desc, _str2pat(pattern))
                case EActionType.INJECT_LINES:
                    newlines: List[str] = []
                    for _ in range(construct.VarInt.parse_stream(w)):
                        newlines.append(self.PSTRING.parse_stream(w))
                    a = InjectLinesAction(pp, desc, newlines)
                case EActionType.FINISH:
                    pp.finish()
                    break
                case EActionType.REMOVE_ALL:
                    pattern = self.PSTRING.parse_stream(w)
                    a = RemoveAllAction(pp, desc, _str2pat(pattern))
            pp.actions.append(a)
        return pp

import ast
import hashlib
import re
from pathlib import Path
import sys
from typing import Dict, Optional, Tuple

from devtools._codegen import writeGeneratedPythonWarningHeader

import black
import isort
from mkast.formatters.autoflake import AutoflakeFormattingProvider
from mkast.formatters.base import BaseFormattingProvider
from mkast.formatters.black import BlackFormattingProvider
from mkast.formatters.isort import ISortFormattingProvider
from mkast.generators.astor import AstorCodeGenerator
from mkast.modulefactory import ModuleFactory
from pregex import Pregex
from regast.reparse import reparse_regex

__all__ = ["file2builddir",'PythonMinifierFormattingProvider','LTMinifiableCodeGenerator','RegexConsts']
BUILD_DIR = Path.cwd() / ".build"


def file2builddir(path: str) -> Path:
    return (
        BUILD_DIR
        / hashlib.md5(Path(path).absolute().as_posix().encode("utf-8")).hexdigest()
    )


class PythonMinifierFormattingProvider(BaseFormattingProvider):
    ID='python-minifier'
    def __init__(self) -> None:
        super().__init__()

    def formatCode(self, code: str) -> str:
        try:
            import python_minifier

            return python_minifier.minify(code)
        except:
            return code


class LTMinifiableCodeGenerator(AstorCodeGenerator):
    MINIFY: bool = True

    def __init__(self) -> None:
        super().__init__()
        cfg = isort.Config(
            line_length=65355,
            ignore_whitespace=True,
            quiet=True,
            lines_after_imports=0,
            lines_before_imports=0,
            lines_between_sections=0,
            lines_between_types=0,
        )
        self.addFormatter(ISortFormattingProvider(cfg))
        self.addFormatter(AutoflakeFormattingProvider(remove_all_unused_imports=True))
        if self.MINIFY:
            self.addFormatter(ISortFormattingProvider(cfg))
            self.addFormatter(PythonMinifierFormattingProvider())
        else:
            self.addFormatter(BlackFormattingProvider(black.Mode(line_length=200)))
            self.addFormatter(ISortFormattingProvider(cfg))


class RegexConsts:
    def __init__(self) -> None:
        self.vars: Dict[str, Tuple[Pregex, re.RegexFlag]] = {}

    def addExpr(self, constid: str, p: Pregex, debug: bool = False) -> None:
        self.vars[constid] = (p, (re.RegexFlag.DEBUG if debug else re.RegexFlag(0)))

    def to_file(self, filename: Path, builder: Optional[str]=None) -> None:
        if builder is None:
            builder=sys.argv[0]
        mod = ModuleFactory()
        mod.addVariableDecl("__all__", None, ast.List(elts=[ast.Constant(x) for x in sorted(set(self.vars.keys()))]))
        mod.addImport(["re"])
        mod.addImportFrom("typing", ["Pattern", "TypeAlias", "Callable"])
        mod.addVariableDecl(
            "_C",
            ast.Subscript(
                value=ast.Name("Callable"),
                slice=ast.Tuple(
                    elts=[ast.List(elts=[ast.Name("str")]), ast.Name("Pattern")]
                ),
            ),
            ast.Attribute(
                value=ast.Name(id="re", ctx=ast.Load()),
                attr="compile",
                ctx=ast.Load(),
            ),
            final=True,
        )
        mod.addVariableDecl(
            "_P",
            ast.Name("TypeAlias"),
            ast.Name("Pattern"),
            final=False,
        )
        for constid, (p, f) in sorted(self.vars.items()):
            debug = (f & re.RegexFlag.DEBUG) == re.RegexFlag.DEBUG
            mod.addVariableDecl(
                name=constid,
                annotation=ast.Name(id="_P", ctx=ast.Load()),
                value=ast.Call(
                    func=ast.Name("_C"),
                    args=[
                        ast.Constant(value=reparse_regex(p.get_pattern(), debug=debug)),
                        ast.Constant(value=int(f.value)),
                    ],
                    keywords=[],
                ),
            )
        with filename.open("w") as f:
            writeGeneratedPythonWarningHeader(f,'Pre-compiled regular expressions',Path(__file__))
            f.write(LTMinifiableCodeGenerator().generate(mod.generate()))

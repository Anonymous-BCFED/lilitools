import argparse
import os
from binascii import b2a_base64
from pathlib import Path
import re
from typing import Iterable, Optional

from analysis.function_hash import FuncHash
from analysis.savable_metadata import SavableMetadata
from declarative_argparse import BoolDO, DeclarativeOptionParser, IntDO, StrDO

from ruamel.yaml import YAML as Yaml  # isort: skip
yaml = Yaml(typ='rt')


class DOPMain(DeclarativeOptionParser):
    def __init__(self) -> None:
        super().__init__(argp=argparse.ArgumentParser(description='Add a funchash to a savable to make change detection easier.'))
        self.yamlfile: StrDO = self.addStr('yamlfile')
        self.id: StrDO = self.addStr('id')
        self.filename: StrDO = self.addStr('filename')
        self.find: StrDO = self.addStr('--find') \
            .setDefault(None) \
            .setMetaVar('"something"') \
            .setDescription('The first line matching this argument will be selected as the signature.')
        self.signature: StrDO = self.addStr('--signature') \
            .setDefault(None) \
            .setDescription('Set the signature to this argument.')
        self.line: IntDO = self.addInt('--line') \
            .setDefault(None) \
            .setDescription('Select the line at the given position as the signature.')
        self.saveAsXml: BoolDO = self.addStoreTrue('--save-as-xml') \
            .setDescription('Select the bog-standard saveAsXML method as the funchash signature.')


def tryToFindSigIn(filename: str, sig: str) -> Optional[str]:
    with open(filename, 'r') as f:
        for l in f:
            if sig in l:
                return l.strip()
    return None


def getSigFromLine(filename: str, ln: int) -> Optional[str]:
    with open(filename, 'r') as f:
        return f.readlines()[ln - 1]
    return None


def addFuncHashToSavable(
    ymlfile: Path,
    javafile: Path,
    sigid: str,
    line_number: Optional[int] = None,
    search_line: Optional[str] = None,
    signature: Optional[str] = None,
    saveAsXml: bool = False,
) -> FuncHash:
    with ymlfile.open('r') as f:
        data = yaml.load(f)
    s = SavableMetadata()
    s.deserialize(data)

    fh = FuncHash()
    fh.id = sigid
    fh.filename = str(javafile)
    if saveAsXml:
        fh.signature = tryToFindSigIn(fh.filename, ' saveAsXML(')
    elif search_line is not None:
        fh.signature = tryToFindSigIn(fh.filename, search_line)
    elif line_number is not None:
        fh.signature = getSigFromLine(fh.filename, line_number)
    elif signature is not None:
        fh.signature = signature
    fh.rehash()

    fh.filename = fh.filename.replace(os.path.join('lib', 'liliths-throne-public'), '[LT]')
    s.function_hashes[fh.id] = fh

    tmpfile = ymlfile.with_suffix(ymlfile.suffix + '~')
    try:
        with tmpfile.open('w') as f:
            yaml.dump(s.serialize(), f)
        os.rename(tmpfile, ymlfile)
    finally:
        if tmpfile.is_file():
            tmpfile.unlink()
    return fh


def addFuncHashToSavables(
    ymlfiles: Iterable[Path],
    javafile: Path,
    sigid: str,
    line_number: Optional[int] = None,
    search_line: Optional[str] = None,
    signature: Optional[str] = None,
    saveAsXml: bool = False,
) -> None:
    print(f'{javafile}:')
    for yf in ymlfiles:
        fh = addFuncHashToSavable(ymlfile=yf, javafile=javafile, sigid=sigid, line_number=line_number, search_line=search_line, signature=signature, saveAsXml=saveAsXml)
        encoded_hash = b2a_base64(fh.digest).decode("utf-8").strip()
        print(f' - {yf}: {fh.signature} @ {encoded_hash}')


def main():
    args = DOPMain()
    args.parseArguments()

    fh = addFuncHashToSavable(
        Path(args.yamlfile.get_value()),
        Path(args.filename.get_value()),
        args.id.get_value(),
        line_number=args.line.get_value(),
        search_line=args.find.get_value(),
        signature=args.signature.get_value(),
        saveAsXml=args.saveAsXml.get_value())

    print('filename:', fh.filename)
    print('signature:', fh.signature)
    print('hash:', fh.digest.hex())


if __name__ == "__main__":
    main()

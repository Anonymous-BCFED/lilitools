from __future__ import annotations, absolute_import

import ast
import collections
from inspect import Attribute
import os
from pathlib import Path
from typing import Any, Dict, List, Optional, OrderedDict, Set

import autopep8
import isort
import javalang
from buildtools.maestro.base_target import SingleBuildTarget
from javalang.tree import ClassCreator, FieldDeclaration
from lxml import etree

import astor
from astor.source_repr import split_lines

from enumFrom import ArgumentFetcher, simplifyArgs, transformToPythonEnum

from _codegen import writeGeneratedPythonWarningHeader


# SFX_LIB = Path('lib') / 'liliths-throne-public' / 'src' / 'com' / 'lilithsthrone' / 'game' / 'character' / 'effects' / 'StatusEffect.java'
# SFX_PY = Path('lilitools') / 'game' / 'enums' / 'known_status_effect.py'


class ClothingTransformer(ast.NodeTransformer):
    NAME_ID_REPL: Dict[str, str] = {
        "PresetColour": "EPresetColour",
        "ColourListPresets": "colour_preset_lists",
    }

    def visit_Name(self, node: ast.Name) -> Any:
        if node.id in self.NAME_ID_REPL.keys():
            node.id = self.NAME_ID_REPL[node.id]
        return node


class ClothingTypeData:
    def __init__(self) -> None:
        self.name: str = ""
        self.namePlural: str = ""
        self.description: str = ""
        self.determiner: str = ""
        # self.pathName: str = ''
        self.baseValue: int = 0
        self.isPlural: bool = False
        self.possibleGeneratedColours: List[Set[ast.AST]] = []
        self.possibleDyeColours: List[Set[ast.AST]] = []
        self.effects: Dict[ast.Attribute, ast.Constant] = {}

    def toAST(self) -> ast.AST:
        return ast.Call(
            func=ast.Name(id="ClothingType", ctx=ast.Load()), args=self.getSimpRow()
        )

    def getSimpRow(self) -> List[Optional[ast.AST]]:
        return [
            ast.Constant(self.baseValue),
            ast.Constant(self.determiner),
            ast.Constant(self.name),
            ast.Constant(self.namePlural),
            ast.Constant(self.isPlural),
            # ast.Constant(self.pathName),
            ast.Constant(self.description),
            self.getPossibleGeneratedColours(),
            ast.Constant(None),
            # ast.List(elts=self.effects) if self.effects is not None else None
        ]

    def getPossibleGeneratedColours(self) -> Optional[ast.List]:
        o = []
        for i, s in enumerate(self.possibleGeneratedColours):
            if s is None:
                o += [ast.Constant(None)]
            else:
                if isinstance(s, ast.List):
                    l = []
                    for e in s.elts:
                        if isinstance(e, ast.Attribute) and e.value == "PresetColours":
                            e = ast.Attribute(
                                value=ast.Attribute(
                                    value=ast.Name(id="EPresetColour", ctx=ast.Load()),
                                    attr=e.attr,
                                    ctx=ast.Load(),
                                ),
                                attr="name",
                                ctx=ast.Load(),
                            )
                        l.append(e)
                    s = ast.List(elts=l, ctx=ast.Load())
                # if not isinstance(s, ast.Attribute):
                #    print(ast.dump(s))
                o += [s]
        return ast.List(elts=o, ctx=ast.Load())

    @staticmethod
    def GetAttributes() -> OrderedDict[str, str]:
        o = collections.OrderedDict()
        o["baseValue"] = "int"
        o["determiner"] = "str"
        o["nameSingular"] = "str"
        o["namePlural"] = "str"
        o["isPlural"] = "bool"
        # o['pathName'] = 'str'
        o["description"] = "str"
        o["colourPossibilities"] = "List[Set[str]]"
        o["effects"] = "List[ItemEffect]"
        return o

    def resizeList(self, lst: List[Any], sz: int, empty: Any = None) -> None:
        while len(lst) < sz:
            lst.append(empty)

    def setPrimaryColorPossibilities(self, val: Any) -> None:
        if val is None:
            return
        if isinstance(val, ast.Attribute):
            if val.value.id == "ColourListPresets":
                val = ast.Attribute(
                    value=ast.Name(id="colour_preset_lists", ctx=ast.Load()),
                    attr=val.attr,
                    ctx=ast.Load(),
                )
        self.resizeList(self.possibleGeneratedColours, 1, None)
        self.possibleGeneratedColours[0] = val

    def setSecondaryColorPossibilities(self, val: Any) -> None:
        if val is None:
            return
        if isinstance(val, ast.Attribute):
            if val.value.id == "ColourListPresets":
                val = ast.Attribute(
                    value=ast.Name(id="colour_preset_lists", ctx=ast.Load()),
                    attr=val.attr,
                    ctx=ast.Load(),
                )
        self.resizeList(self.possibleGeneratedColours, 2, None)
        self.possibleGeneratedColours[1] = val

    def setTertiaryColorPossibilities(self, val: Any) -> None:
        if val is None:
            return
        if isinstance(val, ast.Attribute):
            if val.value.id == "ColourListPresets":
                val = ast.Attribute(
                    value=ast.Name(id="colour_preset_lists", ctx=ast.Load()),
                    attr=val.attr,
                    ctx=ast.Load(),
                )
        self.resizeList(self.possibleGeneratedColours, 3, None)
        self.possibleGeneratedColours[2] = val


def getClothingTypesFromJavaEnum(file: Path) -> OrderedDict[str, ClothingTypeData]:
    cu = javalang.parse.parse(file.read_text())
    values = collections.OrderedDict()
    for stmt in cu.types[0].body:
        if (
            not isinstance(stmt, FieldDeclaration)
            or "static" not in stmt.modifiers
            or "public" not in stmt.modifiers
        ):
            continue
        typeid: Optional[str] = None
        if len(stmt.type.dimensions) == 0:  # No arrays
            for vdec in stmt.declarators:
                if isinstance(vdec.initializer, ClassCreator):
                    simped = simplifyArgs(vdec.initializer.arguments)
                    ctd = ClothingTypeData()
                    """
                    0 int baseValue,
                    1 String determiner,
                    2 boolean plural,
                    3 String name,
                    4 String namePlural,
                    5 String description,
                    6 float physicalResistance,
                    7 Femininity femininityRestriction,
                    8 InventorySlot equipSlot,
                    9 Rarity rarity,
                    10 AbstractSetBonus clothingSet,
                    11 String pathName,
                    12 List<ItemEffect> effects,
                    13 List<BlockedParts> blockedPartsList,
                    14 List<InventorySlot> incompatibleSlotsList,
                    15 List<Colour> availablePrimaryColours,
                    16 List<Colour> availablePrimaryDyeColours,
                    17 List<Colour> availableSecondaryColours,
                    18 List<Colour> availableSecondaryDyeColours,
                    19 List<Colour> availableTertiaryColours,
                    20 List<Colour> availableTertiaryDyeColours,
                    21 List<ItemTag> itemTags
                    """
                    fxl: ast.List
                    # print(len(simped))
                    if len(simped) == 22:
                        ctd.isPlural = simped[2].value
                        ctd.name = simped[3].value
                        ctd.namePlural = simped[4].value
                        # ctd.pathName = simped[11].value
                        ctd.baseValue = simped[0].value
                        ctd.description = (
                            "[CANNOT DETERMINE BY STATIC ANALYSIS]"
                            if isinstance(simped[5], ast.BinOp)
                            else simped[5].value
                        )
                        ctd.determiner = simped[1].value
                        ctd.setPrimaryColorPossibilities(simped[15])
                        ctd.setSecondaryColorPossibilities(simped[17])
                        ctd.setTertiaryColorPossibilities(simped[19])
                        fxl = simped[12]

                        """
                        0 int baseValue,
                        1 String determiner,
                        2 boolean plural,
                        3 String name,
                        4 String namePlural,
                        5 String description,
                        6 float physicalResistance,
                        7 Femininity femininityRestriction,
                        8 List<InventorySlot> equipSlots,
                        9 Rarity rarity,
                        10 AbstractSetBonus clothingSet,
                        11 String pathName,
                        12 Map<InventorySlot, String> pathNameEquipped,
                        13 List<ItemEffect> effects,
                        14 Map<InventorySlot, List<BlockedParts>> blockedPartsMap,
                        15 Map<InventorySlot, List<InventorySlot>> incompatibleSlotsMap,
                        16 List<Colour> availablePrimaryColours,
                        17 List<Colour> availablePrimaryDyeColours,
                        18 List<Colour> availableSecondaryColours,
                        19 List<Colour> availableSecondaryDyeColours,
                        20 List<Colour> availableTertiaryColours,
                        21 List<Colour> availableTertiaryDyeColours,
                        22 Map<InventorySlot, List<ItemTag>> itemTags
                        """
                    elif len(simped) == 23:
                        ctd.baseValue = simped[0].value
                        ctd.determiner = simped[1].value
                        ctd.isPlural = simped[2].value
                        ctd.name = simped[3].value
                        ctd.namePlural = simped[4].value
                        ctd.description = simped[5].value
                        # ctd.pathName = simped[11].value
                        ctd.setPrimaryColorPossibilities(simped[16])
                        ctd.setSecondaryColorPossibilities(simped[18])
                        ctd.setTertiaryColorPossibilities(simped[20])
                        fxl = simped[13]
                    else:
                        print(len(simped))
                        print(repr(simped))
                    if isinstance(fxl, ast.List):
                        ctd.effects = fxl  # TODO
                    # print(ased.name, ased.pathName, ased.effects)
                    values[vdec.name] = ctd
                    # print(ast.dump(ast.Tuple(elts=simped)))

    return values


class GrabClothingTarget(SingleBuildTarget):
    BT_LABEL: str = "ENUM"

    def __init__(
        self,
        outfile: Path,
        infile: Path,
        sfx_dir: Path,
        fields: Optional[collections.OrderedDict] = None,
        fetchers: Optional[List[ArgumentFetcher]] = None,
        dependencies: List[str] = [],
    ) -> None:
        self.outfile = outfile
        self.infile = infile
        self.fields = fields or collections.OrderedDict()
        self.fetchers = fetchers or []
        self.sfx_dir = sfx_dir
        self.enumName: str = ""
        self.imports: List[str] = []
        super().__init__(
            target=str(outfile), files=[str(infile)], dependencies=dependencies
        )

    def build(self) -> None:
        cstr = ""
        cL = getClothingTypesFromJavaEnum(self.infile)
        for sfxfile in self.sfx_dir.rglob("*.xml"):
            tree = etree.parse(sfxfile)
            senode = tree.getroot()
            # print(sfxfile, senode)
            coreAttrs = senode.find("coreAttributes")
            if coreAttrs is None:
                coreAttrs = senode.find("coreAtributes")
            ctd = ClothingTypeData()
            ctd.baseValue = int(coreAttrs.find("value").text)
            ctd.name = coreAttrs.find("name").text
            ctd.namePlural = coreAttrs.find("namePlural").text
            ctd.isPlural = bool(coreAttrs.find("namePlural").attrib["pluralByDefault"])
            ctd.description = coreAttrs.find("description").text
            # ctd.pathName = str(sfxfile.absolute().relative_to(Path('lib/liliths-throne-public').absolute()))
            id = (
                str(
                    sfxfile.parent.absolute().relative_to(self.sfx_dir.absolute())
                ).replace(os.sep, "_")
                + "_"
                + sfxfile.stem
            )
            if (cel := coreAttrs.find("primaryColours")) is not None:
                if "values" in cel.attrib:
                    ctd.setPrimaryColorPossibilities(
                        ast.Attribute(
                            value=ast.Name(id="ColourListPresets", ctx=ast.Load()),
                            attr=cel.attrib["values"],
                            ctx=ast.Load(),
                        )
                    )
            if (cel := coreAttrs.find("secondaryColours")) is not None:
                if "values" in cel.attrib:
                    ctd.setSecondaryColorPossibilities(
                        ast.Attribute(
                            value=ast.Name(id="ColourListPresets", ctx=ast.Load()),
                            attr=cel.attrib["values"],
                            ctx=ast.Load(),
                        )
                    )
            if (cel := coreAttrs.find("tertiaryColours")) is not None:
                if "values" in cel.attrib:
                    ctd.setTertiaryColorPossibilities(
                        ast.Attribute(
                            value=ast.Name(id="ColourListPresets", ctx=ast.Load()),
                            attr=cel.attrib["values"],
                            ctx=ast.Load(),
                        )
                    )
            if (found := coreAttrs.find("attributeModifiers")) is not None:
                for modifier in found.findall("modifier"):
                    ctd.effects[
                        ast.Attribute(
                            value=ast.Name(id="EAttribute", ctx=ast.Load()),
                            attr=modifier.text,
                        )
                    ] = ast.Constant(float(modifier.attrib["value"]))
            cL[id] = ctd

        def tfast(input: List[ast.AST]) -> List[ast.AST]:
            o = []
            tfer = ClothingTransformer()
            for node in input:
                o.append(tfer.visit(node))
            return o

        # cstr += transformToPythonEnum(contents={'EKnownClothing': [(k, tfast(ased.getSimpRow())) for k, ased in cL.items()]}, attrs_for_instance=ClothingTypeData.GetAttributes())
        mod = ast.Module(
            body=[
                ast.ImportFrom(
                    module="lilitools.lazy_enum",
                    names=[
                        # ast.alias(name='auto', asname=None),
                        ast.alias(name="LazyGenericEnum", asname=None),
                    ],
                    level=0,
                ),
                # from lilitools.saves.modfiles.character.clothing.clothing_type import ClothingType
                ast.ImportFrom(
                    module="lilitools.saves.modfiles.character.clothing.clothing_type",
                    names=[
                        ast.alias(name="ClothingType", asname=None),
                    ],
                    level=0,
                ),
                ast.ImportFrom(
                    module="lilitools.saves.items.lazy_loaders.clothing_loader",
                    names=[
                        ast.alias(name="LazyClothing", asname=None),
                    ],
                    level=0,
                ),
                ast.ClassDef(
                    name="EKnownClothing",
                    bases=[
                        ast.Subscript(
                            value=ast.Name("LazyGenericEnum"),
                            slice=ast.Name("ClothingType"),
                        )
                    ],
                    keywords=[],
                    decorator_list=[],
                    body=[
                        ast.Assign(
                            targets=[ast.Name(k)],
                            value=ast.Call(
                                func=ast.Name("LazyClothing"), args=[ast.Constant(k)]
                            ),
                        )
                        for k in cL.keys()
                    ],
                ),
            ]
        )

        def pretty_source(source):
            return "".join(split_lines(source, maxline=65535))

        cstr += astor.to_source(mod, pretty_source=pretty_source)
        cfg = isort.Config(
            line_length=65355,
            add_imports=self.imports,
            ignore_whitespace=True,
            quiet=True,
            lines_after_imports=0,
            lines_before_imports=0,
            lines_between_sections=0,
            lines_between_types=0,
        )
        cstr = isort.code(cstr, config=cfg)
        cstr = autopep8.fix_code(
            cstr,
            options=dict(
                max_line_length=65535,
                ignore=[
                    "E265",  # E265 - Format block comments.
                    "E266",  # E266 - Fix too many leading '#' for block comments.
                ],
                aggressive=2,
            ),
        )
        tmpfile = self.outfile.with_suffix(".tmp")
        with tmpfile.open("w") as f:
            writeGeneratedPythonWarningHeader(f, "Built-in clothing", Path(__file__))
            f.write(cstr)
        os.replace(tmpfile, self.outfile)

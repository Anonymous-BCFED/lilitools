import ast
import collections
import keyword
from typing import Dict, FrozenSet, Iterable, List, Optional, OrderedDict, Set, cast

import astor
from astor.source_repr import split_lines
from mkast.modulefactory import ModuleFactory

RESERVED_ENUM_ATTR_NAMES: FrozenSet[str] = frozenset(
    {
        "name",
    }
)


class EnumDef:

    def __init__(
        self,
        id: str = "",
        start_at: int = 0,
        attrs_for_instance: Optional[OrderedDict[str, Optional[str]]] = None,
        additional_methods: Optional[List[ast.FunctionDef]] = None,
        bases: Optional[List[ast.AST]] = None,
    ) -> None:
        self.id: str = id
        self.startAt: int = start_at
        self.attrs: Optional[OrderedDict[str, Optional[str]]] = attrs_for_instance
        self.additionalMethods: Optional[List[ast.FunctionDef]] = additional_methods
        self.contents: OrderedDict[str, Optional[List[ast.AST]]] = (
            collections.OrderedDict()
        )
        self.value_type: Optional[str] = None
        self.argmap: Optional[Dict[str, str]] = None
        self.bases: Optional[List[ast.AST]] = bases

    def setValueType(self, value_type: str, argmap: Dict[str, str]) -> None:
        self.value_type = value_type
        self.argmap = argmap

    def addAttr(self, attrID: str, attrType: str) -> None:
        if self.attrs is None:
            self.attrs = collections.OrderedDict()
        self.attrs[attrID] = attrType

    def skipAttr(self, attrID: str) -> None:
        if self.attrs is None:
            self.attrs = collections.OrderedDict()
        self.attrs[attrID] = None

    def addValue(self, name: str, args: Optional[List[ast.AST]] = None) -> None:
        self.contents[name] = args

    def generate(self) -> ast.AST:
        clsbod: List[ast.AST] = []
        cls = ast.ClassDef(
            name=self.id,
            bases=(
                [
                    ast.Name(
                        id="Enum" if self.attrs is not None else "IntEnum",
                        ctx=ast.Load(),
                    )
                ]
                if self.bases is None
                else self.bases
            ),
            keywords=[],
            body=clsbod,
            decorator_list=[],
        )
        for i, (member, args) in enumerate(self.contents.items()):
            v: ast.AST = ast.Constant(i)
            if args is not None and self.attrs is not None:
                elts = []
                j = 0
                if self.value_type is None:
                    for attr, attrType in self.attrs.items():
                        if attrType is None:
                            continue
                        elts.append(args[j])
                        j += 1
                    v = ast.Tuple(elts=elts)
                else:
                    v = ast.Call(func=ast.Name(self.value_type), args=[], keywords=[])
                    for attr, attrType in self.attrs.items():
                        if attrType is None:
                            continue
                        v.args.append(cast(ast.expr, args[j]))
                        j += 1

            clsbod.append(
                # ast.Assign(
                #     targets=[
                #         ast.Name(id=member, ctx=ast.Load())
                #     ],
                #     value=ast.Call(
                #         func=ast.Name(id='auto', ctx=ast.Load()),
                #         args=[],
                #         keywords=[]
                #     )))
                ast.Assign(targets=[ast.Name(id=member, ctx=ast.Store())], value=v)
            )
        if self.value_type is None:
            if self.attrs is not None and len(self.attrs) > 0:
                args = [ast.arg(arg="self", annotation=None)]
                for k, t in self.attrs.items():
                    if t is None:
                        continue
                    if keyword.iskeyword(k):
                        k += "_"
                    args.append(
                        ast.arg(arg=k, annotation=ast.Name(id=t, ctx=ast.Load()))
                    )
                init = ast.FunctionDef(
                    name="__init__",
                    args=ast.arguments(
                        posonlyargs=[],
                        args=args,
                        kw_defaults=[],
                        kwonlyargs=[],
                        defaults=[],
                        vararg=None,
                        kwarg=None,
                    ),
                    returns=ast.Constant(None),
                    decorator_list=[],
                    body=[],
                )
                for k, t in self.attrs.items():
                    if t is None:
                        continue
                    ka: str = k
                    if ka in RESERVED_ENUM_ATTR_NAMES:
                        ka += "_"
                    if keyword.iskeyword(k):
                        k += "_"
                    init.body.append(
                        ast.AnnAssign(
                            target=ast.Attribute(
                                value=ast.Name(id="self", ctx=ast.Load()),
                                attr=ast.Name(id=ka, ctx=ast.Store()),
                            ),
                            annotation=ast.Name(id=t, ctx=ast.Load()),
                            value=ast.Name(id=k, ctx=ast.Load()),
                            simple=1,
                        )
                    )

                    funcName = (k[0].upper() + k[1:]).strip("_")
                    getFunc = ast.FunctionDef(
                        name=f"get{funcName}",
                        args=ast.arguments(
                            posonlyargs=[],
                            args=[ast.arg(arg="self", annotation=None)],
                            kw_defaults=[],
                            kwonlyargs=[],
                            defaults=[],
                            vararg=None,
                            kwarg=None,
                        ),
                        returns=ast.Name(id=t, ctx=ast.Load()),
                        decorator_list=[],
                        body=[
                            ast.Return(
                                value=ast.Attribute(
                                    value=ast.Name(id="self", ctx=ast.Load()),
                                    attr=ast.Name(id=ka, ctx=ast.Store()),
                                )
                            )
                        ],
                    )
                    cls.body.append(getFunc)
                clsbod.append(init)
            if self.additionalMethods is not None:
                clsbod += self.additionalMethods
        return cls


class EnumGenerator:
    def __init__(self) -> None:
        self._mod = ModuleFactory()
        # self.imports: List[Union[ast.Import, ast.ImportFrom]] = []
        self.contents: OrderedDict[str, EnumDef] = collections.OrderedDict()
        # self.expressions: List[ast.expr] = []

    def addImport(self, names: Iterable[str]) -> None:
        # if isinstance(names, str):
        #     names = [names]
        # self.imports.append(
        #     ast.Import(names=[ast.alias(name=x, asname=None) for x in names])
        # )
        self._mod.addImport(names)

    def addImportFrom(self, module: str, names: Iterable[str]) -> None:
        # if isinstance(names, str):
        #     names = [names]
        # self.imports.append(
        #     ast.ImportFrom(
        #         module=module,
        #         names=[ast.alias(name=x, asname=None) for x in names],
        #         level=0,
        #     )
        # )
        self._mod.addImportFrom(module, names)

    def addEnum(
        self,
        id: str = "",
        start_at: int = 0,
        attrs_for_instance: Optional[OrderedDict[str, Optional[str]]] = None,
        additional_methods: Optional[List[ast.FunctionDef]] = None,
        bases: Optional[List[ast.AST]] = None,
    ) -> EnumDef:
        e = EnumDef(id, start_at, attrs_for_instance, additional_methods, bases)
        self.contents[e.id] = e
        return e

    def addVariableDecl(
        self,
        name: str,
        annotation: Optional[ast.AST],
        value: Optional[ast.AST],
        final: bool = False,
    ) -> None:
        self._mod.addVariableDecl(name, annotation, value, final)

    def generateAST(self) -> ast.AST:
        allValues: List[ast.Constant] = []
        enumImports: Set[str] = set()
        for e in self.contents.values():
            if e.attrs is None:
                enumImports.add("IntEnum")
            else:
                enumImports.add("Enum")
        self._mod.addImportFrom("enum", list(enumImports))
        self._mod.addVariableDecl("__all__", None, ast.List(elts=allValues))
        for enumName, enumContents in self.contents.items():
            allValues.append(ast.Constant(enumName))
            self._mod.expressions.append(enumContents.generate())
        return self._mod.generate()

    def generate(self) -> str:
        def pretty_source(source):
            return "".join(split_lines(source, maxline=65535))

        return astor.to_source(self.generateAST(), pretty_source=pretty_source)

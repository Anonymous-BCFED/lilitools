from pathlib import Path
import javalang
from javalang.tree import CompilationUnit
class EnumScanner:
    def __init__(self, filename: Path) -> None:
        self.filename: Path = filename
    def start(self) -> None:
        java: CompilationUnit = javalang.parse.parse(self.filename.read_text())

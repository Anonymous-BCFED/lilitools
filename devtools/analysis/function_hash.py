'''
Function hashes are a quick way to determine when a chunk of code has changed.

We use them to determine when we need to update emulated code or constants based on LT code.
'''
import collections
import hashlib
import re
from binascii import b2a_base64
from typing import Any, Dict, Optional

from _dev_regex import REG_ENUM, REG_INDENT


def getFunctionBySig(filename: str, signature: str) -> Optional[str]:
    o = []
    in_func = False
    buffer = ''
    block = 0
    found = False
    endAtEOL = False
    enum_mode = False

    # print(repr(signature))
    signature = signature.replace('\t', ' ' * 4).replace('\r\n', '\n').replace('\r', '\n').strip()
    signature = REG_INDENT.sub(' ', signature)
    # print(repr(signature))

    if ' static ' in signature and ' = ' in signature:
        #log.info('Parser in endAtEOL mode.')
        endAtEOL = True

    def addToBuffer(c: str) -> None:
        nonlocal buffer
        buffer += c

    def commitBuffer(c: str) -> None:
        nonlocal buffer, o
        o += [buffer + c]
        buffer = ''
    with open(filename, 'r') as f:
        for line in f:
            if REG_ENUM.search(line) is not None:
                enum_mode = True
            l_stripped = line.strip()
            lsig = line.replace('\t', '    ').strip()
            lsig = REG_INDENT.sub(' ', lsig)
            if not in_func:
                if endAtEOL:
                    if lsig.startswith(signature):
                        lb = '' + lsig
                        while not lb.endswith(';'):
                            lb += f.read(1)
                        return lb
                else:
                    if lsig == signature:
                        in_func = True
                        found = True
                        if lsig.endswith('(' if enum_mode else '{'):
                            block += 1
            else:
                buffer = ''
                for c in l_stripped:
                    if c == ('(' if enum_mode else '{'):
                        block += 1
                    elif c == (')' if enum_mode else '}'):
                        block -= 1
                        if block <= 0:
                            commitBuffer(c)
                            return '\n'.join(o)
                    addToBuffer(c)
                commitBuffer('')
    if not found:
        return None
    return '\n'.join(o)



class FuncHash:
    def __init__(self) -> None:
        self.id: str = ''
        self.filename: str = ''
        self.signature: str = ''
        self.digest: Optional[bytes] = None

    def serialize(self) -> Dict[str, Any]:
        o = collections.OrderedDict()
        o['id'] = self.id
        o['filename'] = self.filename
        o['signature'] = self.signature.rstrip()
        if self.digest is not None:
            o['digest'] = self.digest
        return dict(o)

    def deserialize(self, data: Dict[str, Any]) -> None:
        self.id = data['id']
        self.filename = data['filename']
        self.signature = data['signature']
        self.digest = data.get('digest', None)

    def fix_sig(self) -> None:
        sig = self.signature.replace('\t', ' ' * 4).replace('\r\n', '\n').replace('\r', '\n').strip()
        self.signature = REG_INDENT.sub(' ', sig)

    def rehash(self) -> None:
        self.fix_sig()
        content = getFunctionBySig(self.filename, self.signature).encode('utf-8')
        #print(repr(content))
        self.digest = hashlib.sha512(content).digest()

    def __str__(self) -> str:
        if self.signature is None:
            return f'## from {self.filename}: {self.signature}'
        encoded_hash = b2a_base64(self.digest).decode("utf-8")
        return f'## from {self.filename}: {self.signature} @ {encoded_hash}'

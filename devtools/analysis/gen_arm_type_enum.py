import ast
import collections
import json
import os
from pathlib import Path
from typing import Dict, List, Optional, OrderedDict, Set

from buildtools.maestro.base_target import SingleBuildTarget

from _codegen import LTMinifiableCodeGenerator
from analysis.enum_generator import EnumGenerator
from analysis.utils import ProtoItemEffect

from _codegen import writeGeneratedPythonWarningHeader


def _fixSet(inp: ast.Set) -> ast.AST:
    # If ast.Set().elts.len == 0, we need to replace it with a set() call.
    if len(inp.elts) == 0:
        return ast.Call(func=ast.Name("set"), args=[], keywords=[])
    return inp


class ArmTypeData:
    def __init__(self) -> None:
        self.id: str = ""
        """
        {
            "arms": {
                "ALLIGATOR_MORPH": {
                    "coveringType": "ALLIGATOR_SCALES",
                    "flags": [],
                    "namePlural": "arms",
                    "nameSingular": "arm",
                    "race": "ALLIGATOR_MORPH",
                    "tags": [
                        "ARM_STANDARD"
                    ],
                    "transformationName": null
                },
            }
        }
        """
        self.nameSingular: str = ""
        self.namePlural: str = ""
        self.race: str = ""
        self.coveringType: str = ""
        self.transformationName: Optional[str] = ""
        self.tags: Set[str] = set()
        self.flags: Set[str] = set()

    @staticmethod
    def GetAttributes() -> OrderedDict[str, str]:
        o = collections.OrderedDict()
        o["id"] = "str"
        o["nameSingular"] = "str"
        o["namePlural"] = "str"
        o["race"] = "str"
        o["coveringType"] = "str"
        o["transformationName"] = "Optional[str]"
        o["tags"] = "Set[str]"
        o["flags"] = "Set[str]"
        return o

    def fromDict(self, data: dict) -> None:
        # self.id = data['id']
        self.nameSingular = data["nameSingular"]
        self.namePlural = data["namePlural"]
        self.race = data["race"]
        self.coveringType = data["coveringType"]
        if "transformationName" in data:
            self.transformationName = data["transformationName"]

        # self.pathName = str(Path(data['pathName']).absolute().relative_to(Path.cwd()))
        # if self.pathName.startswith('dist' + os.sep):
        #     self.pathName = self.pathName[5:]

        self.tags = set(data["tags"])
        self.flags = set(data["flags"])

    def toAST(self) -> ast.AST:
        return ast.Call(
            func=ast.Name(id="ArmType", ctx=ast.Load()), args=self.getSimpRow()
        )

    def getSimpRow(self) -> List[Optional[ast.AST]]:
        # DIST = Path.cwd() / 'dist'
        # cleaned_path = Path(self.pathName)
        # if cleaned_path.is_absolute():
        #     cleaned_path = cleaned_path.relative_to(DIST)
        return [
            ast.Constant(self.id),  # 0
            ast.Constant(self.nameSingular),  # 1
            ast.Constant(self.namePlural),  # 2
            ast.Constant(self.race),  # 3
            ast.Constant(self.coveringType),  # 4
            ast.Constant(self.transformationName),  # 5
            _fixSet(ast.Set(elts=[ast.Constant(x) for x in sorted(self.tags)])),
            _fixSet(ast.Set(elts=[ast.Constant(x) for x in sorted(self.flags)])),
        ]


class KnownArmTypeEnumGenerator(SingleBuildTarget):
    def __init__(
        self, outfile: Path, jsonFile: Path, dependencies: List[str] = []
    ) -> None:
        self.outfile = outfile
        self.json_file = jsonFile
        super().__init__(
            target=str(outfile.absolute()),
            files=[str(jsonFile.absolute())],
            dependencies=dependencies,
        )

    def build(self) -> None:
        data: dict
        with self.json_file.open("r") as f:
            data = json.load(f)

        eg = EnumGenerator()
        eg.addImportFrom("lilitools.saves.modfiles.bodyparttypes.arm_type", ["ArmType"])
        eg.addImportFrom("typing", ["List", "Set"])
        ekc = eg.addEnum("EKnownArmTypes")
        ekc.setValueType("ArmType", {})
        for k, v in ArmTypeData.GetAttributes().items():
            ekc.addAttr(k, v)
        for cid, cdata in sorted(
            data["arms"].items(), key=lambda x: str(x[0]).casefold()
        ):
            # print(cid, repr(cdata))
            ctd = ArmTypeData()
            ctd.fromDict(cdata)
            ctd.id = cid
            ekc.addValue(cid, ctd.getSimpRow())

        with self.outfile.open("w") as f:
            writeGeneratedPythonWarningHeader(f,'Built-in Arm Types', Path(__file__))
            f.write(LTMinifiableCodeGenerator().generate(eg.generateAST()))

import ast
import collections
import json
import os
from pathlib import Path
from typing import Dict, List, Optional, OrderedDict, Set

from buildtools.maestro.base_target import SingleBuildTarget
import tqdm

from _codegen import LTMinifiableCodeGenerator
from analysis.enum_generator import EnumGenerator
from analysis.utils import ProtoItemEffect

from _codegen import writeGeneratedPythonWarningHeader

class ClothingTypeData:
    def __init__(self) -> None:
        self.id: str = ""
        self.name: str = ""
        self.namePlural: str = ""
        self.description: str = ""
        self.determiner: str = ""
        # self.pathName: str = ""
        self.baseValue: int = 0
        self.isPlural: bool = False
        self.possibleGeneratedColours: List[ast.Set] = []
        self.possibleDyeColours: List[ast.Set] = []
        self.effects: Dict[ast.Attribute, ast.Constant] = {}

    @staticmethod
    def GetAttributes() -> OrderedDict[str, str]:
        o = collections.OrderedDict()
        o["id"] = "str"
        o["baseValue"] = "int"
        o["determiner"] = "str"
        o["nameSingular"] = "str"
        o["namePlural"] = "str"
        o["isPlural"] = "bool"
        # o["pathName"] = "str"
        o["description"] = "str"
        o["generatedColourPossibilities"] = "List[Set[str]]"
        o["allColourPossibilities"] = "List[Set[str]]"
        o["effects"] = "List[ItemEffect]"
        return o

    def fromDict(self, data: dict) -> None:
        self.baseValue = int(data["baseValue"])
        self.description = data["description"]
        self.determiner = data["determiner"]
        self.isPlural = bool(data["isPlural"])
        self.name = data["name"]
        self.namePlural = data["namePlural"]

        # self.pathName = str(Path(data["pathName"]).absolute().relative_to(Path.cwd()))
        # if self.pathName.startswith("dist" + os.sep):
        #     self.pathName = self.pathName[5:]

        self.possibleGeneratedColours = []
        self.possibleDyeColours = []
        for colourReplacement in data["colourReplacements"]:
            self.possibleGeneratedColours.append(
                ast.Set(elts=[ast.Constant(c) for c in colourReplacement["defaults"]])
            )
            self.possibleDyeColours.append(
                ast.Set(elts=[ast.Constant(c) for c in colourReplacement["all"]])
            )

        self.effects = []
        for effectData in data["effects"]:
            self.effects.append(
                ProtoItemEffect(
                    type=effectData["type"],
                    mod1=effectData["primary_modifier"],
                    mod2=effectData["secondary_modifier"],
                    potency=effectData["potency"],
                    limit=int(effectData["limit"]),
                    timer=0,
                )
            )

    def toAST(self) -> ast.AST:
        return ast.Call(
            func=ast.Name(id="ClothingType", ctx=ast.Load()), args=self.getSimpRow()
        )

    def getSimpRow(self) -> List[Optional[ast.AST]]:
        DIST = Path.cwd() / "dist"
        # cleaned_path = Path(self.pathName)
        # if cleaned_path.is_absolute():
        #     cleaned_path = cleaned_path.relative_to(DIST)
        return [
            ast.Constant(self.id),  # 0
            ast.Constant(self.baseValue),  # 1
            ast.Constant(self.determiner),  # 2
            ast.Constant(self.name),  # 3
            ast.Constant(self.namePlural),  # 4
            ast.Constant(self.isPlural),  # 5
            # ast.Constant(str(self.pathName)),  # 6
            ast.Constant(self.description),  # 7
            self.getPossibleGeneratedColours(),  # 8
            self.getPossibleDyeColours(),  # 9
            # ast.Constant(None),
            (
                ast.List(elts=[self._instantiate_itemeffect(x) for x in self.effects])
                if self.effects is not None
                else ast.Constant(None)
            ),
        ]

    def _instantiate_itemeffect(self, i: ProtoItemEffect) -> ast.AST:
        # print(repr(i))
        return ast.Call(
            func=ast.Name("ItemEffect"),
            args=[
                ast.Constant(i.type),
                ast.Attribute(
                    value=ast.Name("ETFModifier"),
                    attr=i.mod1,
                ),
                ast.Attribute(
                    value=ast.Name("ETFModifier"),
                    attr=i.mod2,
                ),
                ast.Attribute(
                    value=ast.Name("ETFPotency"),
                    attr=i.potency,
                ),
                ast.Constant(i.limit),
                ast.Constant(i.timer),
            ],
            keywords=[],
        )

    def getPossibleGeneratedColours(self) -> Optional[ast.List]:
        o = []
        for i, s in enumerate(self.possibleGeneratedColours):
            if s is None:
                o += [ast.Constant(None)]
            else:
                if isinstance(s, ast.Set):
                    l = []
                    for e in s.elts:
                        if isinstance(e, ast.Constant):
                            e = e.value
                        l.append(cname2EPresetColourAttr(e))
                    s = ast.Set(elts=l, ctx=ast.Load())
                # if not isinstance(s, ast.Attribute):
                #    print(ast.dump(s))
                o += [s]
        return ast.List(elts=o, ctx=ast.Load())

    def getPossibleDyeColours(self) -> Optional[ast.List]:
        o = []
        for i, s in enumerate(self.possibleDyeColours):
            if s is None:
                o += [ast.Constant(None)]
            else:
                if isinstance(s, ast.Set):
                    l = []
                    for e in s.elts:
                        if isinstance(e, ast.Constant):
                            e = e.value
                        l.append(cname2EPresetColourAttr(e))
                    s = ast.Set(elts=l, ctx=ast.Load())
                # if not isinstance(s, ast.Attribute):
                #    print(ast.dump(s))
                o += [s]
        return ast.List(elts=o, ctx=ast.Load())


def cname2EPresetColourAttr(cname: str) -> ast.AST:
    return ast.Attribute(
        value=ast.Attribute(
            value=ast.Name(id="EPresetColour"), attr=cname, ctx=ast.Load()
        ),
        attr="name",
        ctx=ast.Load(),
    )
from ruamel.yaml import YAML as Yaml
YAML=Yaml(typ='safe',pure=True)

class KnownClothingEnumGenerator(SingleBuildTarget):
    BT_TYPE='GENERATE'
    def __init__(
        self, outfile: Path, yaml_dir: Path, dependencies: List[str] = []
    ) -> None:
        self.outfile = outfile
        self.yaml_dir = yaml_dir
        self.yfiles:List[Path] = list(self.yaml_dir.rglob('*.yml'))
        super().__init__(
            target=str(outfile.absolute()),
            files=sorted(map(str, self.yfiles)),
            dependencies=dependencies,
        )

    def build(self) -> None:
        clothing: dict={}
        for yf in tqdm.tqdm(self.yfiles, unit='file',desc='Parsing files...'):
            with yf.open("r") as f:
                data = YAML.load(f)
                clothing[data['id']]=data

        eg = EnumGenerator()
        eg.addImportFrom("lilitools.saves.enums.preset_colour", ["EPresetColour"])
        eg.addImportFrom("lilitools.saves.items.item_effect", ["ItemEffect"])
        eg.addImportFrom("lilitools.saves.enums.tf_modifier", ["ETFModifier"])
        eg.addImportFrom("lilitools.saves.enums.tf_potency", ["ETFPotency"])
        eg.addImportFrom(
            "lilitools.saves.modfiles.character.clothing.clothing_type",
            ["ClothingType"],
        )
        eg.addImportFrom("typing", ["List", "Set"])
        # from lilitools.saves.items.lazy_loaders.clothing_loader import LazyClothing
        eg.addImportFrom(
            "lilitools.saves.items.lazy_loaders.clothing_loader", ["LazyClothing"]
        )
        # from lilitools.saves.modfiles.character.clothing.clothing_type import ClothingType
        eg.addImportFrom(
            "lilitools.saves.modfiles.character.clothing.clothing_type",
            ["ClothingType"],
        )
        eg.addImportFrom("lilitools.lazy_enum", ["LazyGenericEnum"])
        ekc = eg.addEnum(
            "EKnownClothing",
            bases=[
                # ast.Subscript(
                #     value=ast.Name("LazyGenericEnum"), slice=ast.Name("ClothingType")
                # )
                ast.Name('LazyGenericEnum')
            ],
        )
        # ekc.setValueType("ClothingType", {})
        ekc.setValueType("LazyClothing", {})
        ekc.addAttr("cid", str)
        # for k, v in ClothingTypeData.GetAttributes().items():
        #     ekc.addAttr(k, v)

        # colorSets: List[Set[str]] = []
        # for cid, cdata in sorted(
        #     data["clothing"].items(), key=lambda x: str(x[0]).casefold()
        # ):
        #     # print(cid, repr(cdata))
        #     ctd = ClothingTypeData()
        #     ctd.id = cid
        #     ctd.fromDict(cdata)
        #     dyeColours = ast.List(elts=[])
        #     for i, pdc_ast in enumerate(ctd.possibleDyeColours):
        #         k: Set[str] = set([x.value for x in pdc_ast.elts])
        #         if k not in colorSets:
        #             cl: List[ast.AST] = ast.Set(
        #                 elts=[ast.Constant(x.value) for x in pdc_ast.elts]
        #             )
        #             idx = len(colorSets)
        #             colorSets.append(k)
        #             # name = ast.Name(f'_CS_{idx:04X}')
        #             # csASTEntries.append(
        #             eg.addVariableDecl(
        #                 name=f"_CS_{idx:04X}",
        #                 annotation=ast.Subscript(
        #                     value=ast.Name("Set"),
        #                     slice=ast.Name("str"),
        #                 ),
        #                 value=cl,
        #                 final=True,
        #             )
        #         idx = colorSets.index(k)
        #         name = ast.Name(f"_CS_{idx:04X}")
        #         dyeColours.elts.append(name)
        #     defaultColours = ast.List(elts=[])
        #     for i, pgc_ast in enumerate(ctd.possibleGeneratedColours):
        #         k: Set[str] = set([x.value for x in pgc_ast.elts])
        #         if k not in colorSets:
        #             cl: List[ast.AST] = ast.Set(
        #                 elts=[ast.Constant(x.value) for x in pgc_ast.elts]
        #             )
        #             idx = len(colorSets)
        #             colorSets.append(k)
        #             # name = ast.Name(f'_CS_{idx:04X}')
        #             # csASTEntries.append(
        #             eg.addVariableDecl(
        #                 name=f"_CS_{idx:04X}",
        #                 annotation=ast.Subscript(
        #                     value=ast.Name("Set"),
        #                     slice=ast.Name("str"),
        #                 ),
        #                 value=cl,
        #                 final=True,
        #             )
        #         idx = colorSets.index(k)
        #         name = ast.Name(f"_CS_{idx:04X}")
        #         defaultColours.elts.append(name)
        #     row = ctd.getSimpRow()
        #     row[8] = defaultColours
        #     row[9] = dyeColours
        #     ekc.addValue(cid, row)
        for cid in sorted(clothing.keys(), key=lambda x: str(x).casefold()):
            ekc.addValue(cid, [ast.Constant(cid)])

        with self.outfile.open("w") as f:
            writeGeneratedPythonWarningHeader(f, "Built-in Clothing Types", Path(__file__))
            f.write(LTMinifiableCodeGenerator().generate(eg.generateAST()))

import ast
import collections
import json
from enum import IntFlag
from pathlib import Path
from typing import Dict, List, Optional, OrderedDict, Set, Type

from buildtools.maestro.base_target import SingleBuildTarget

from _codegen import LTMinifiableCodeGenerator
from analysis.enum_generator import EnumGenerator
from analysis.utils import ProtoItemEffect
from lilitools.saves.items.enums.item_flags import EItemFlags

from _codegen import writeGeneratedPythonWarningHeader

MODDED_ITEMS:Set[str] = set({'LEVELLERS_DRAUGHT'})
MODDED_ITEMEFFECTS:Set[str] = set({'LEVELLERS_DRAUGHT'})
def build_flags_by_or(
    flagclsname: str, flagtype: Type[IntFlag], value: IntFlag
) -> ast.AST:
    o: List[ast.Attribute] = []
    for v in flagtype:
        if (value & v) == v and v.value.bit_count() == 1:
            o.append(
                ast.Attribute(
                    value=ast.Name(id=flagclsname), attr=v.name, ctx=ast.Load()
                ),
            )
    l_o = len(o)
    if l_o == 0:
        return ast.Call(func=flagclsname, args=[ast.Constant(0)], keywords=[])
    elif l_o == 1:
        return o[0]
    else:
        return applyBinopToEntireList(o, ast.BitOr())


def applyBinopToEntireList(l: List[ast.AST], op: ast.operator) -> ast.AST:
    l_l = len(l)
    if l_l == 1:
        return l[0]
    else:
        right = l.pop()
        return ast.BinOp(applyBinopToEntireList(l, op), op, right)


class ItemTypeData:
    def __init__(self) -> None:
        self.id: str = ""
        self.authorDescription: str = ""
        self.description: str = ""
        self.determiner: str = ""
        self.flags: EItemFlags = EItemFlags.NONE
        self.nameSingular: str = ""
        self.namePlural: str = ""
        self.colours: List[str] = []
        self.effects: Dict[ast.Attribute, ast.Constant] = {}

    @staticmethod
    def GetAttributes() -> OrderedDict[str, str]:
        o = collections.OrderedDict()
        o["id"] = "str"
        o["authorDescription"] = "str"
        o["baseValue"] = "int"
        o["description"] = "str"
        o["determiner"] = "str"
        o["flags"] = "EItemFlags"
        # o['isAbleToBeDropped'] = 'bool'
        # o['isAbleToBeSold'] = 'bool'
        # o['isAbleToBeUsedFromInventory'] = 'bool'
        # o['isAbleToBeUsedInCombatAllies'] = 'bool'
        # o['isAbleToBeUsedInCombatEnemies'] = 'bool'
        # o['isAbleToBeUsedInSex'] = 'bool'
        # o['isConsumedOnUse'] = 'bool'
        # o['isFetishGiving'] = 'bool'
        # o['isFromExternalFile'] = 'bool'
        # o['isGift'] = 'bool'
        # o['isMod'] = 'bool'
        # o['isPlural'] = 'bool'
        # o['isTransformative'] = 'bool'
        o["nameSingular"] = "str"
        o["namePlural"] = "str"
        # o['pathName'] = 'str'
        o["colours"] = "List[str]"
        o["effects"] = "List[ItemEffect]"
        return o

    def fromDict(self, data: dict) -> None:
        self.id = data["id"]
        self.authorDescription = data["authorDescription"]
        self.value = int(data["value"])
        self.description = data["description"]
        self.determiner = data["determiner"]
        self.flags = EItemFlags.NONE
        if bool(data["isAbleToBeDropped"]):
            self.flags |= EItemFlags.isAbleToBeDropped
        if bool(data["isAbleToBeSold"]):
            self.flags |= EItemFlags.isAbleToBeSold
        if bool(data["isAbleToBeUsedFromInventory"]):
            self.flags |= EItemFlags.isAbleToBeUsedFromInventory
        if bool(data["isAbleToBeUsedInCombatAllies"]):
            self.flags |= EItemFlags.isAbleToBeUsedInCombatAllies
        if bool(data["isAbleToBeUsedInCombatEnemies"]):
            self.flags |= EItemFlags.isAbleToBeUsedInCombatEnemies
        if bool(data["isAbleToBeUsedInSex"]):
            self.flags |= EItemFlags.isAbleToBeUsedInSex
        if bool(data["isConsumedOnUse"]):
            self.flags |= EItemFlags.isConsumedOnUse
        if bool(data["isFetishGiving"]):
            self.flags |= EItemFlags.isFetishGiving
        if bool(data["isFromExternalFile"]):
            self.flags |= EItemFlags.isFromExternalFile
        if bool(data["isGift"]):
            self.flags |= EItemFlags.isGift
        if bool(data["isMod"]):
            self.flags |= EItemFlags.isMod
        if bool(data["isPlural"]):
            self.flags |= EItemFlags.isPlural
        if bool(data["isTransformative"]):
            self.flags |= EItemFlags.isTransformative
        self.nameSingular = data["name"]
        self.namePlural = data["namePlural"]
        # self.pathName = data['pathName']

        # self.pathName = str(Path(data['pathName']).absolute().relative_to(Path.cwd()))
        # if self.pathName.startswith('dist' + os.sep):
        #     self.pathName = self.pathName[5:]

        self.colours = data["colourShades"]

        self.effects = []
        for effectData in data["effects"]:
            self.effects.append(
                ProtoItemEffect(
                    type=effectData["type"],
                    mod1=effectData["primary_modifier"],
                    mod2=effectData["secondary_modifier"],
                    potency=effectData["potency"],
                    limit=int(effectData["limit"]),
                    timer=0,
                )
            )

    def toAST(self) -> ast.AST:
        return ast.Call(
            func=ast.Name(id="ClothingType", ctx=ast.Load()), args=self.getSimpRow()
        )

    def getSimpRow(self) -> List[Optional[ast.AST]]:
        DIST = Path.cwd() / "dist"
        # cleaned_path = Path(self.pathName)
        # if cleaned_path.is_absolute():
        #     cleaned_path = cleaned_path.relative_to(DIST)
        return [
            # o['id'] = 'str'
            # o['authorDescription'] = 'str'
            # o['baseValue'] = 'int'
            # o['description'] = 'str'
            # o['determiner'] = 'str'
            # o['flags'] = 'EItemFlags'
            ast.Constant(self.id),  # 0
            ast.Constant(self.authorDescription),  # 0
            ast.Constant(self.value),  # 0
            ast.Constant(self.description),  # 0
            ast.Constant(self.determiner),  # 0
            build_flags_by_or("EItemFlags", EItemFlags, self.flags),  # 0
            ast.Constant(self.nameSingular),
            ast.Constant(self.namePlural),
            # ast.Constant(str(self.pathName)),# 5
            # 7
            ast.List(elts=[ast.Constant(x) for x in self.colours]),  # 8
            # ast.Constant(None),
            ast.List(elts=[self._instantiate_itemeffect(x) for x in self.effects])
            if self.effects is not None
            else ast.Constant(None),
        ]

    def _instantiate_itemeffect(self, i: ProtoItemEffect) -> ast.AST:
        # print(repr(i))
        return ast.Call(
            func=ast.Name("ItemEffect"),
            args=[
                ast.Constant(i.type),
                ast.Attribute(
                    value=ast.Name("ETFModifier"),
                    attr=i.mod1,
                )
                if i.mod1
                else ast.Constant(None),
                ast.Attribute(
                    value=ast.Name("ETFModifier"),
                    attr=i.mod2,
                )
                if i.mod2
                else ast.Constant(None),
                ast.Attribute(
                    value=ast.Name("ETFPotency"),
                    attr=i.potency,
                )
                if i.potency
                else ast.Constant(None),
                ast.Constant(i.limit),
                ast.Constant(i.timer),
            ],
            keywords=[],
        )


def cname2EPresetColourAttr(cname: str) -> ast.AST:
    return ast.Attribute(
        value=ast.Attribute(
            value=ast.Name(id="EPresetColour"), attr=cname, ctx=ast.Load()
        ),
        attr="name",
        ctx=ast.Load(),
    )


class KnownItemsEnumGenerator(SingleBuildTarget):
    def __init__(
        self, outfile: Path, jsonFile: Path, dependencies: List[str] = []
    ) -> None:
        self.outfile = outfile
        self.json_file = jsonFile
        super().__init__(
            target=str(outfile.absolute()),
            files=[str(jsonFile.absolute())],
            dependencies=dependencies,
        )

    def build(self) -> None:
        data: dict
        with self.json_file.open("r") as f:
            data = json.load(f)

        eg = EnumGenerator()
        eg.addImportFrom("lilitools.saves.enums.preset_colour", ["EPresetColour"])
        eg.addImportFrom("lilitools.saves.items.item_effect", ["ItemEffect"])
        # eg.addImportFrom('lilitools.saves.items.enums.item_flags', ['ItemFlags'])
        eg.addImportFrom("lilitools.saves.enums.tf_modifier", ["ETFModifier"])
        eg.addImportFrom("lilitools.saves.enums.tf_potency", ["ETFPotency"])
        eg.addImportFrom("lilitools.saves.items.enums.item_flags", ["EItemFlags"])
        eg.addImportFrom("typing", ["List", "Set"])

        ekc = eg.addEnum("EKnownItems")
        for k, v in ItemTypeData.GetAttributes().items():
            ekc.addAttr(k, v)

        ekc.additionalMethods = []
        for m in EItemFlags:
            if m == EItemFlags.NONE:
                continue
            ekc.additionalMethods.append(
                # name: _Identifier
                # args: arguments
                # body: list[stmt]
                # decorator_list: list[expr]
                # returns: expr | None
                ast.FunctionDef(
                    name=m.name,
                    args=ast.arguments(
                        args=[ast.arg("self")],
                        vararg=None,
                        kwonlyargs=[],
                        kw_defaults=[],
                        kwarg=None,
                        defaults=[],
                    ),
                    body=[
                        ast.Return(
                            value=ast.BinOp(
                                left=ast.BinOp(
                                    left=ast.Attribute(
                                        value=ast.Name("self"), attr="flags"
                                    ),
                                    op=ast.BitAnd(),
                                    right=ast.Attribute(
                                        value=ast.Name("EItemFlags"), attr=m.name
                                    ),
                                ),
                                op=ast.Eq(),
                                right=ast.Attribute(
                                    value=ast.Name("EItemFlags"), attr=m.name
                                ),
                            )
                        )
                    ],
                    decorator_list=[ast.Name("property")],
                    returns=ast.Name("bool"),
                )
            )
        colorSets: List[List[str]] = []
        for cid, cdata in sorted(
            data["items"].items(), key=lambda x: str(x[0]).casefold()
        ):
            # print(cid, repr(cdata))
            itd = ItemTypeData()
            itd.fromDict(cdata)
            if itd.id in MODDED_ITEMS:
                continue
            row = itd.getSimpRow()
            ekc.addValue(cid, row)

        with self.outfile.open("w") as f:
            writeGeneratedPythonWarningHeader(f, "Built-in Item Types", Path(__file__))
            f.write(LTMinifiableCodeGenerator().generate(eg.generateAST()))

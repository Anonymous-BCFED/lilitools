import ast
import json
from pathlib import Path
from typing import Any, Dict, List
from buildtools.maestro.base_target import SingleBuildTarget
from _codegen import LTMinifiableCodeGenerator, writeGeneratedPythonWarningHeader
from .enum_generator import EnumGenerator

class SubSpeciesData:
    def __init__(self) -> None:
        self.id: str
        self.baseSlaveValue: int

    @classmethod
    def FromDict(cls, data: Dict[str, Any])->'SubSpeciesData':
        o = cls()
        o.id=data['id']
        o.baseSlaveValue=int(data['baseSlaveValue'])
        return o
    
    def toSimpRow(self)->List[ast.AST]:
        return [
            ast.Constant(self.id),
            ast.Constant(self.baseSlaveValue),
        ]

class KnownSubspeciesEnumGenerator(SingleBuildTarget):
    def __init__(
        self, outfile: Path, jsonFile: Path, dependencies: List[str] = []
    ) -> None:
        self.outfile = outfile
        self.json_file = jsonFile
        super().__init__(
            target=str(outfile.absolute()),
            files=[str(jsonFile.absolute())],
            dependencies=dependencies,
        )

    def build(self) -> None:
        data: dict
        with self.json_file.open("r") as f:
            data = json.load(f)

        eg = EnumGenerator()
        # eg.addImportFrom("lilitools.saves.enums.preset_colour", ["EPresetColour"])
        # eg.addImportFrom("lilitools.saves.items.item_effect", ["ItemEffect"])
        # # eg.addImportFrom('lilitools.saves.items.enums.item_flags', ['ItemFlags'])
        # eg.addImportFrom("lilitools.saves.enums.tf_modifier", ["ETFModifier"])
        # eg.addImportFrom("lilitools.saves.enums.tf_potency", ["ETFPotency"])
        # eg.addImportFrom("lilitools.saves.items.enums.item_flags", ["EItemFlags"])
        eg.addImportFrom("typing", ["List", "Set"])

        ekc = eg.addEnum("EKnownSubspecies")
        ekc.addAttr('id','str')
        ekc.addAttr('baseSlaveValue','str')

        colorSets: List[List[str]] = []
        for cid, ssdata in sorted(
            data["species"].items(), key=lambda x: str(x[0]).casefold()
        ):
            # print(cid, repr(cdata))
            itd = SubSpeciesData.FromDict(ssdata)
            # if itd.id in MODDED_ITEMS:
            #     continue
            row = itd.toSimpRow()
            ekc.addValue(cid, row)

        with self.outfile.open("w") as f:
            writeGeneratedPythonWarningHeader(f, "Built-in Subspecies Types", Path(__file__))
            f.write(LTMinifiableCodeGenerator().generate(eg.generateAST()))


import ast
from typing import Dict, Iterable, List, Set, Union


class ImportMap:
    def __init__(self) -> None:
        self.imports: Set[str] = set()
        self.importsFrom: Dict[str, Set[str]] = {}

    def addImportFrom(self, package: str, attrs: Union[str, Iterable[str]]) -> None:
        if isinstance(attrs, str):
            attrs = [attrs]
        if package not in self.importsFrom.keys():
            self.importsFrom[package] = set(attrs)
        else:
            self.importsFrom[package].update(set(attrs))

    def addImport(self, package: str) -> None:
        self.imports.add(package)

    def serialize(self) -> List[Union[str, Dict[str, List[str]]]]:
        o: List[Union[str, Dict[str, List[str]]]] = []
        pkg: str
        objs: Set[str]
        for pkg in sorted(self.imports):
            o.append(pkg)
        for pkg, objs in sorted(self.importsFrom.items()):
            o.append({pkg: list(objs)})
        return o

    def deserialize(self, l: List[Union[str, Dict[str, List[str]]]]) -> None:
        for e in l:
            # - lxml: [etree]
            if isinstance(e, dict):
                k, v = next(iter(e.items()))
                self.addImportFrom(k, v)
            # - os
            elif isinstance(e, str):
                self.addImport(e)
            else:
                raise ValueError(f'e={e!r}')

    def toAST(self) -> List[ast.AST]:
        o: List[ast.AST] = []
        pkg: str
        objs: Set[str]
        for pkg in sorted(self.imports):
            o.append(ast.Import(names=[
                ast.Name(id=pkg)
            ]))
        for pkg, objs in sorted(self.importsFrom.items()):
            o.append(ast.ImportFrom(module=pkg, names=[ast.Name(id=x) for x in objs]))
        return o

    def fromAST(self, l: Iterable[ast.AST]) -> None:
        name: ast.Name
        alias: ast.alias
        aliases: List[str]
        imp: ast.Import
        impfrom: ast.ImportFrom
        for e in l:
            match type(e):
                case ast.Import:
                    imp = e
                    for name in imp.names:
                        self.addImport(name.id)
                case ast.ImportFrom:
                    impfrom = e
                    aliases = []
                    for alias in impfrom.names:
                        aliases.append(alias.name)
                    self.addImportFrom(str(impfrom.module), aliases)

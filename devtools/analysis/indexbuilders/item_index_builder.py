import ast
import importlib.util
import os
from pathlib import Path
from typing import Dict, List, Optional, cast

from _codegen import LTMinifiableCodeGenerator
from buildtools.maestro.base_target import SingleBuildTarget
from mkast.modulefactory import ModuleFactory


class ClassInfo:
    def __init__(self, id: str, clsname: str, module: str) -> None:
        self.id: str = id
        self.class_name: str = clsname
        self.module: str = module


class ClassIndex:
    def __init__(self, varname: str, idattr: str, baseclass: str) -> None:
        self.varname: str = varname
        self.idattr: str = idattr
        self.baseclass: str = baseclass
        self.classes: Dict[str, ClassInfo] = {}
        self.imports: Dict[str, List[str]] = {}

    def addImport(self, package: str, attrs: List[str]) -> None:
        self.imports[package] = attrs

    def parseFilesIn(self, dir: Path) -> None:
        for path in dir.rglob("*.py"):
            self.parseFile(path)

    def parseFile(self, file: Path) -> None:
        try:
            tree = ast.parse(file.read_text(), str(file.absolute()))
            pkg = (
                (file.relative_to(Path.cwd()).parent / file.stem)
                .as_posix()
                .replace("/", ".")
            )
            for el in tree.body:
                if isinstance(el, ast.ClassDef):
                    clsname: str = el.name
                    clsid: Optional[str] = None
                    for clsel in el.body:
                        if isinstance(clsel, (ast.AnnAssign, ast.Assign)):
                            varname = cast(
                                ast.Name,
                                clsel.targets[0]
                                if isinstance(clsel, ast.Assign)
                                else clsel.target,
                            ).id
                            if varname == self.idattr:
                                if isinstance(clsel.value, ast.Call):
                                    if isinstance(clsel.value.func, ast.Name):
                                        if clsel.value.func.id == "frozenset":
                                            # ID_SET: FrozenSet[str] = frozenset({...})
                                            elts: List[ast.AST] = []
                                            if isinstance(clsel.value.args[0], ast.Set):
                                                elts = clsel.value.args[0].elts
                                            for const in elts:
                                                if not isinstance(const, ast.Constant):
                                                    continue
                                                if const.value is None:
                                                    continue
                                                clsid = const.value
                                                self.classes[clsid] = ClassInfo(
                                                    clsid, clsname, pkg
                                                )
                                            clsid = None
                                elif isinstance(clsel.value, ast.Constant):
                                    clsid = cast(ast.Constant, clsel.value).value
                                break
                    if clsid is not None and clsid != "":
                        self.classes[clsid] = ClassInfo(clsid, clsname, pkg)
        except SyntaxError as e:
            print(repr(e))

    def generate(self) -> str:
        mb = ModuleFactory()
        # from typing import Dict, Type
        mb.addImportFrom("typing", ["Dict", "Type"])
        for k, v in self.imports.items():
            mb.addImportFrom(k, v)
        clsByID = ast.Dict(keys=[], values=[])
        for c in sorted(self.classes.values(), key=lambda k: k.class_name):
            mb.addImportFrom(c.module, [c.class_name])
            clsByID.keys.append(ast.Constant(c.id, kind=None))
            clsByID.values.append(ast.Name(id=c.class_name, ctx=ast.Load()))
        mb.addVariableDecl(
            "__all__",
            None,
            ast.List(elts=[ast.Constant(self.varname, kind=None)], ctx=ast.Load()),
        )
        ann = ast.Subscript(
            value=ast.Name(id="Dict", ctx=ast.Load()),
            slice=ast.Index(
                value=ast.Tuple(
                    elts=[
                        ast.Name(id="str", ctx=ast.Load()),
                        ast.Subscript(
                            value=ast.Name(id="Type", ctx=ast.Load()),
                            slice=ast.Index(
                                value=ast.Name(id=self.baseclass, ctx=ast.Load())
                            ),
                            ctx=ast.Load(),
                        ),
                    ],
                    ctx=ast.Load(),
                )
            ),
            ctx=ast.Load(),
        )
        mb.addVariableDecl(self.varname, ann, clsByID)
        return LTMinifiableCodeGenerator().generate(mb.generate())

    def writeCodeTo(self, path: Path) -> None:
        tmpout = path.with_suffix(".py.tmp")
        with open(tmpout, "w") as f:
            COL_W = 79
            CNTR_W = COL_W - 2
            sep = ("#" * COL_W) + "\n"
            f.write(sep)
            f.write("#" + ("@generated by devtools/build.py".center(CNTR_W)) + "#\n")
            f.write("#" + ("DO NOT EDIT DIRECTLY".center(CNTR_W)) + "#\n")
            f.write(sep)
            f.write(self.generate())
        os.replace(tmpout, path)


class GenerateIndex(SingleBuildTarget):
    BT_LABEL = "GEN INDEX"

    def __init__(
        self,
        indir: Path,
        outfile: Path,
        varname: str,
        baseclass: str,
        idattr: str = "ID",
    ) -> None:
        self.indir: Path = indir
        self.outfile: Path = outfile
        self.index = ClassIndex(varname, idattr, baseclass)
        files = list({str(x) for x in self.indir.rglob("*.py")} - {str(self.outfile)})
        super().__init__(target=str(outfile), files=files)

    def build(self) -> None:
        self.index.parseFilesIn(self.indir)
        self.index.writeCodeTo(self.indir / "__init__.py")

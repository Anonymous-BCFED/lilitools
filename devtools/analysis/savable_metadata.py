from enum import IntEnum
from pathlib import Path
from typing import Any, Dict, List

from analysis.function_hash import FuncHash
from analysis.import_map import ImportMap
from analysis.savables.bindings.attribute import AttrBinding
from analysis.savables.bindings.base_child import BaseChildBinding, createChildBindingFrom
from ruamel.yaml import CommentedMap


class EGenerateLanguage(IntEnum):
    PYTHON = 0


class AttributeMeta:
    def __init__(self) -> None:
        pass


class SavableMetadata:
    '''
    Concrete YAML representation of a Savable.
    '''

    def __init__(self) -> None:
        self.className: str = ''
        self.tag: str = ''
        self.superclass: str = 'Savable'
        self.imports = ImportMap()
        self.generate: Dict[EGenerateLanguage, Path] = {}
        self.function_hashes: Dict[str, FuncHash] = {}
        self.attrs: List[AttrBinding] = []
        self.children: List[BaseChildBinding] = []

        self.imports.addImportFrom('typing', ['Optional'])

    def deserialize(self, data: Dict[str, Any]) -> None:
        self.className = data['class']
        self.tag = data['tag']
        self.imports.deserialize(data.get('imports', []))
        self.superclass = data.get('super', 'Savable')
        self.generate = {}
        langid: str
        outpath: str
        for langid, outpath in data['generate'].items():
            self.generate[EGenerateLanguage[langid.upper()]] = Path(outpath)
        self.function_hashes = {}

        for fhdata in data.get('function-hashes', {}):
            fh = FuncHash()
            fh.deserialize(fhdata)
            self.function_hashes[fh.id] = fh

        self.attrs = []
        #print(repr(data['attrs']))
        for e in data['attrs']:
            k, v = next(iter(e.items()))
            a = AttrBinding()
            a.deserialize(k, v)
            self.attrs.append(a)

        self.children = []
        cdata: dict
        for cdata in data['children']:
            k, v = next(iter(cdata.items()))
            c = createChildBindingFrom(k, v, False)
            self.children.append(c)

    def serialize(self) -> CommentedMap:
        cm = CommentedMap()
        cm['class'] = self.className
        if self.superclass != 'Savable':
            cm['super'] = self.superclass
        cm['tag'] = self.tag
        cm['imports'] = self.imports.serialize()
        cm['generate'] = {k.name.lower(): str(v) for k, v in self.generate.items()}
        cm['function-hashes'] = [v.serialize() for v in sorted(self.function_hashes.values(), key=lambda x: x.id)]
        cm['attrs'] = [v.serialize() for v in self.attrs]
        cm['children'] = [v.serialize() for v in self.children]
        return cm

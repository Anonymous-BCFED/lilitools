import sys
from pathlib import Path
from typing import Any, Dict, List, Optional

import javalang
from analysis.savables.bindings.base import Binding
from javalang.tree import (Assignment, BasicType, BlockStatement, ClassDeclaration, CompilationUnit, FieldDeclaration, FormalParameter, IfStatement, LocalVariableDeclaration, MemberReference,
                           MethodDeclaration, MethodInvocation, Node, ReferenceType, ReturnStatement, StatementExpression, This, TypeDeclaration, VariableDeclaration, VariableDeclarator)


class AttrInfo:
    def __init__(self) -> None:
        self.id: str = ''
        self.type: str = 'str'
        self.optional: bool = False

    def serialize(self) -> dict:
        return {self.id:{
            'type': self.type,
            'optional': self.optional
        }}
        
class ChildInfo:
    def __init__(self) -> None:
        self.id: str = ''
        self.tag: str = ''
        self.type: Optional[str] = 'str'
        self.simple: bool = True

        self.class_type: Optional[str] = None
        self.class_package: Optional[str] = None


    def fromSavable(self, si: 'SavableInfo') -> None:
        self.id = si.id
        self.tag = si.tag
        self.type = None
        self.class_type = si.tag.capitalize()
        self.class_package = 'lilitools.saves.fixme'
        self.simple = False

    def serialize(self) -> dict:
        if self.simple:
            return {self.id: self.type}
        else:
            o: Dict[str, Any] = {
                'tag': self.tag,
            }
            if self.class_package and self.class_type:
                o['class'] = {
                    'package': self.class_package,
                    'type': self.class_type,
                }
            else:
                o['type'] = self.type
            return {self.id: o}


class SavableInfo:
    def __init__(self, id: str, tag: str) -> None:
        self.id: str = id
        self.tag: str = tag
        self.attrs: List[AttrInfo] = []
        self.children: List[ChildInfo] = []
        self.knownTypes: List[SavableInfo] = []

    def toChild(self) -> ChildInfo:
        ci = ChildInfo()
        ci.id = self.id
        ci.type = self.id.capitalize()
        return ci

    def serialize(self) -> dict:
        return {
            'id': self.id,
            'tag': self.tag,
            'attrs': [a.serialize() for a in self.attrs],
            'children': [ci.serialize() for ci in self.children],
        }
        
class SavableScanner:
    def __init__(self, filename: Path) -> None:
        self.className: str = ''
        self.fieldTypes: Dict[str, str] = {}
        self.filename: Path = filename
        self.locals: Dict[str, SavableInfo] = {}
        self.parentParam: FormalParameter = None
        self.docParam: FormalParameter = None
        self.currentElements: Dict[str, Binding] = {}
        self.currentVars: Dict[str, str]={}
        self.returningElement: Optional[str] = None

        self.knownTypes: List[SavableInfo] = []

        self.iflevel: int = 0
        self.forlevel: int = 0

        self.saveAsXMLName: str = 'saveAsXML'
        self.exportGameName: str = 'exportGame'

    def detectTypeFrom(self, stmt: Node) -> str:
        '''
        <class 'javalang.tree.StatementExpression'> 
        StatementExpression(
            expression=MethodInvocation(
                arguments=[
                    MemberReference(
                        member=doc, 
                        postfix_operators=[], 
                        prefix_operators=[], 
                        qualifier=, 
                        selectors=[]
                    ), 
                    MemberReference(
                        member=element, 
                        postfix_operators=[], 
                        prefix_operators=[], 
                        qualifier=, 
                        selectors=[]
                    ), 
                    Literal(
                        postfix_operators=[], 
                        prefix_operators=[], 
                        qualifier=None, 
                        selectors=[], 
                        value="time"
                    ), 
                    MethodInvocation(
                        arguments=[
                            MemberReference(
                                member=time, 
                                postfix_operators=[], 
                                prefix_operators=[], 
                                qualifier=, 
                                selectors=[]
                            )
                        ], 
                        member=valueOf, 
                        postfix_operators=[], 
                        prefix_operators=[], 
                        qualifier=String, 
                        selectors=[], 
                        type_arguments=None
                    )], member=addAttribute, postfix_operators=[], prefix_operators=[], qualifier=XMLUtil, selectors=[], type_arguments=None), label=None)
        '''
        if isinstance(stmt, MethodInvocation):
            if stmt.member == 'valueOf' and stmt.qualifier=='String':
                if isinstance(stmt.arguments[0], MemberReference):
                    return self.fieldTypes.get(stmt.arguments[0].member, 'float')
                return 'float'
        return 'str'

    def start(self) -> Dict[str, Any]:
        java: CompilationUnit = javalang.parse.parse(self.filename.read_text())
        t = java.types[0]
        #print(repr(type(t)), t)
        self.className = t.name
        self._populateFields(t)
        method: MethodDeclaration
        for method in t.methods:
            #print(repr(method))
            print('method', method.name, method.return_type.name if method.return_type else 'void')
            # public Element saveAsXML(Element parentElement, Document doc) {
            if method.name == self.saveAsXMLName and method.return_type.name == 'Element':
                self._handle_saveAsXML(java.types[0], method)
                break
            # public static void exportGame(String exportFileName, boolean allowOverwrite) {
            elif method.name == self.exportGameName and method.return_type is None:
                self._handle_exportGame(java.types[0], method)
                break

        data = {
            'class': self.className,
            'tag': None,
            'generate': {'python': 'FIXME'}
        }
        if self.returningElement is not None:
            data.update(self.getChildByVarName(self.returningElement).serialize())
        else:
            data.update(self.currentElements['__FIXME__'].serialize())
            
        return data

    def _populateFields(self, t: ClassDeclaration) -> None:
        '''
        FieldDeclaration(
            annotations=[], 
            declarators=[
                VariableDeclarator(
                    dimensions=[], 
                    initializer=None, 
                    name=event
                )
            ], 
            documentation=None, 
            modifiers={'private'}, 
            type=ReferenceType(
                arguments=None, 
                dimensions=[], 
                name=SlaveEvent, 
                sub_type=None
            )
        ), 
        '''
        self.fieldTypes = {}
        print('Fields:')
        for stmt in t.body:
            if not isinstance(stmt, FieldDeclaration):
                break
            typeid: Optional[str] = None
            if len(stmt.type.dimensions) == 0: # No arrays
                if isinstance(stmt.type, BasicType):
                    if stmt.type.name in ('byte', 'short', 'int', 'long'):
                        typeid = 'int'
                    elif stmt.type.name in ('float', 'double'):
                        typeid = 'float'
                    elif stmt.type.name == 'boolean':
                        typeid = 'bool'
                    elif stmt.type.name in ('char', 'String'):
                        typeid = 'str'
                    else:
                        print('Unknown BasicType:', repr(stmt.type))
                elif isinstance(stmt.type, ReferenceType):
                    if stmt.type.name == 'String':
                        typeid = 'str'
                    else:
                        print('Unknown ReferenceType:', repr(stmt.type))
                else:
                    print('stmt.type == '+repr(stmt.type))
            decl: VariableDeclarator
            for decl in stmt.declarators:
                self.fieldTypes[decl.name] = typeid
                print(f'  {decl.name}: {typeid}')
        

    def _handle_saveAsXML(self, _type: TypeDeclaration, method: MethodDeclaration) -> None:
        # 	public Element saveAsXML(Element parentElement, Document doc) {
        
        self.parentParam, self.docParam = method.parameters

        self.createScalarElement('__FIXME__', self.parentParam.name)

        for stmt in method.body:
            if isinstance(stmt, ReturnStatement):
                print('RE')
                self._handle_root_return_statement(stmt)
            else:
                self._handle_statement_in_block(stmt)

    def _handle_exportGame(self, _type: TypeDeclaration, method: MethodDeclaration) -> None:
        # public static void exportGame(String exportFileName, boolean allowOverwrite) {
        
        #self.parentParam, self.docParam = method.parameters

        #self.createScalarElement('__FIXME__', self.parentParam.name)
        for stmt in method.body:
            if isinstance(stmt, ReturnStatement):
                self._handle_root_return_statement(stmt)
            else:
                self._handle_statement_in_block(stmt)

    def _handle_root_return_statement(self, ret: ReturnStatement) -> None:
        '''
        ReturnStatement(
            expression=MemberReference(
                member=element,
                postfix_operators=[],
                prefix_operators=[],
                qualifier=,
                selectors=[]
            ),
            label=None
        )
        '''
        if isinstance(ret.expression, MemberReference) and isinstance(ret.expression.member, str) and ret.expression.qualifier == '':
            print(f'return {ret.expression.member}')
            self.returningElement = ret.expression.member


    def _handle_statement_in_block(self, stmt: Node) -> None:
            #print(repr(type(stmt)), stmt)
            if isinstance(stmt, LocalVariableDeclaration):
                # Element properties = doc.createElement("character");
                self._handle_variable_decl(stmt)
            elif isinstance(stmt, StatementExpression):
                # XMLUtil.addAttribute(doc, element, "time", String.valueOf(time));
                self._handle_statement_expression(stmt)
            elif isinstance(stmt, IfStatement):
                self._handle_if_statement(stmt)
            # elif isinstance(stmt, ForStatement):
            #     self._handle_for_statement(stmt)

    # def _handle_for_statement(self, fors: ForStatement) -> None:
    #     si = ListSavable()
    #     # for(Something i : list) {
    #     #   Element itemElement = doc.createElement("li");
    #     if isinstance(fors.body, BlockStatement):
    #         lv:
    #         for stmt in fors.body.statements[1:]:
    #             self._handle_statement_in_block(stmt)
    #         else:
    #             self._handle_statement_in_block(fors.body)

    #     finally:
    #         self.forlevel -= 1

    def _handle_if_statement(self, ifs: IfStatement) -> None:
        self.iflevel += 1
        try:
            '''
            <class 'javalang.tree.IfStatement'> 
            IfStatement(
                condition=BinaryOperation(
                    operandl=This(
                        postfix_operators=[], 
                        prefix_operators=[], 
                        qualifier=None, 
                        selectors=[
                            MemberReference(
                                member=involvedSlaveIDs, 
                                postfix_operators=None, 
                                prefix_operators=None, 
                                qualifier=None, 
                                selectors=None
                            )
                        ]
                    ), 
                    operandr=Literal(
                        postfix_operators=[], 
                        prefix_operators=[], 
                        qualifier=None, 
                        selectors=[], 
                        value=null
                    ), 
                    operator=!=
                ), 
                else_statement=None, 
                label=None, 
                then_statement=BlockStatement(label=None, statements=[LocalVariableDeclaration(annotations=[], declarators=[VariableDeclarator(dimensions=[], initializer=MethodInvocation(arguments=[Literal(postfix_operators=[], prefix_operators=[], qualifier=None, selectors=[], value="involvedSlaves")], member=createElement, postfix_operators=[], prefix_operators=[], qualifier=doc, selectors=[], type_arguments=None), name=slavesNode)], modifiers=set(), type=ReferenceType(arguments=None, dimensions=[], name=Element, sub_type=None)), StatementExpression(expression=MethodInvocation(arguments=[MemberReference(member=slavesNode, postfix_operators=[], prefix_operators=[], qualifier=, selectors=[])], member=appendChild, postfix_operators=[], prefix_operators=[], qualifier=element, selectors=[], type_arguments=None), label=None), ForStatement(body=BlockStatement(label=None, statements=[LocalVariableDeclaration(annotations=[], declarators=[VariableDeclarator(dimensions=[], initializer=MethodInvocation(arguments=[Literal(postfix_operators=[], prefix_operators=[], qualifier=None, selectors=[], value="id")], member=createElement, postfix_operators=[], prefix_operators=[], qualifier=doc, selectors=[], type_arguments=None), name=idNode)], modifiers=set(), type=ReferenceType(arguments=None, dimensions=[], name=Element, sub_type=None)), StatementExpression(expression=MethodInvocation(arguments=[MemberReference(member=idNode, postfix_operators=[], prefix_operators=[], qualifier=, selectors=[])], member=appendChild, postfix_operators=[], prefix_operators=[], qualifier=slavesNode, selectors=[], type_arguments=None), label=None), StatementExpression(expression=MethodInvocation(arguments=[MemberReference(member=id, postfix_operators=[], prefix_operators=[], qualifier=, selectors=[])], member=setTextContent, postfix_operators=[], prefix_operators=[], qualifier=idNode, selectors=[], type_arguments=None), label=None)]), control=EnhancedForControl(iterable=MemberReference(member=involvedSlaveIDs, postfix_operators=[], prefix_operators=[], qualifier=, selectors=[]), var=VariableDeclaration(annotations=[], declarators=[VariableDeclarator(dimensions=None, initializer=None, name=id)], modifiers=set(), type=ReferenceType(arguments=None, dimensions=[], name=String, sub_type=None))), label=None)]))
            '''
            if isinstance(ifs.then_statement, BlockStatement):
                for stmt in ifs.then_statement.statements:
                    self._handle_statement_in_block(stmt)
            else:
                self._handle_statement_in_block(ifs.then_statement)

        finally:
            self.iflevel -= 1

   
    def _handle_statement_expression(self, stmt: StatementExpression) -> None:
        '''
        <class 'javalang.tree.StatementExpression'> 
        StatementExpression(
            expression=MethodInvocation(
                arguments=[
                    MemberReference(
                        member=doc, 
                        postfix_operators=[], 
                        prefix_operators=[], 
                        qualifier=, 
                        selectors=[]
                    ),
                    MemberReference(
                        member=element, 
                        postfix_operators=[], 
                        prefix_operators=[], 
                        qualifier=, 
                        selectors=[]
                    ), 
                    Literal(
                        postfix_operators=[], 
                        prefix_operators=[], 
                        qualifier=None, 
                        selectors=[], 
                        value="slaveID"
                    ), 
                    MemberReference(
                        member=slaveID, 
                        postfix_operators=[], 
                        prefix_operators=[], 
                        qualifier=, 
                        selectors=[]
                    )
                ], 
                member=addAttribute, 
                postfix_operators=[], 
                prefix_operators=[], 
                qualifier=XMLUtil, 
                selectors=[], 
                type_arguments=None
            ), 
            label=None
        )
        '''
        if isinstance(stmt.expression, MethodInvocation):
            self._handle_method_invocation(stmt.expression)
        elif isinstance(stmt.expression, This):
            #this.milk.saveAsXML(...)
            return
        elif isinstance(stmt.expression, Assignment):
            return
        else:
            print('WAT')
            print(repr(type(stmt.expression)), stmt.expression)
            sys.exit(1)

    def _handle_method_invocation(self, mi: MethodInvocation) -> None:
        # parentElement.appendChild(element)
        if mi.member == 'appendChild':
            self._handle_appendChild(parentvar=mi.qualifier, childvar=mi.arguments[0].member)
        # XMLUtil.addAttribute(doc, element, "time", String.valueOf(time))
        elif mi.qualifier == 'XMLUtil' and mi.member == 'addAttribute':
            self._handle_addAttribute(mi.arguments[1].member, mi.arguments[2].value, mi.arguments[3])
        # XMLUtil.createXMLElementWithValue(doc, element, "ralphDiscount", String.valueOf(ralphDiscount));
        elif mi.qualifier == 'XMLUtil' and mi.member == 'createXMLElementWithValue':
            self._handle_createXMLElementWithValue(mi.arguments[1].member, mi.arguments[2].value, mi.arguments[3])

    def _handle_addAttribute(self, parentvar: str, attrName: str, valueMember: Any) -> None:
        # # XMLUtil.addAttribute(doc, element, "time", String.valueOf(time))
        ai = AttrInfo()
        ai.id = attrName.strip('"')
        ai.type = self.detectTypeFrom(valueMember)
        ci = self.getChildByVarName(parentvar)
        print(f' + attr: {ai.id!r}')
        ci.attrs.append(ai)

    def _handle_createXMLElementWithValue(self, parentvar: str, elName: str, valueMember: Any) -> None:
        # XMLUtil.createXMLElementWithValue(doc, element, "ralphDiscount", String.valueOf(ralphDiscount));
        ci = ChildInfo()
        ci.id = elName.strip('"')
        ci.type = self.detectTypeFrom(valueMember)
        pi = self.getChildByVarName(parentvar)
        print(f' + simple: {ci.id!r}')
        pi.children.append(ci)

    def getChildByVarName(self, varname: str) -> SavableInfo:
        return self.currentElements[self.currentVars[varname]]

    def _handle_appendChild(self, parentvar: str, childvar: str) -> None:
        #print(repr(self.parentParam.name), repr(parentvar))
        # if parentvar == self.parentParam.name:
        #     print(f'Skipped appendChild: is parentParam')
        #     return
        pi = self.getChildByVarName(parentvar)
        ci = self.getChildByVarName(childvar)
        print(f'<{ci.tag}> => <{pi.tag}>')
        if isinstance(ci, SavableInfo):
            nci = ChildInfo()
            nci.fromSavable(ci)
            self.knownTypes.append(ci)
            ci = nci
        pi.children.append(ci)

    def _handle_variable_decl(self, stmt: VariableDeclaration) -> None:
        
        if len(stmt.declarators) > 0:
            decl: VariableDeclarator
            for decl in stmt.declarators:
                '''
                LocalVariableDeclaration(
                    annotations=[], 
                    declarators=[
                        VariableDeclarator(
                            dimensions=[], 
                            initializer=MethodInvocation(
                                arguments=[
                                    Literal(
                                        postfix_operators=[], 
                                        prefix_operators=[], 
                                        qualifier=None, 
                                        selectors=[], 
                                        value="character"
                                    )
                                ], 
                                member=createElement, 
                                postfix_operators=[], 
                                prefix_operators=[], 
                                qualifier=doc, 
                                selectors=[], 
                                type_arguments=None
                            ), 
                            name=properties
                        )
                    ], 
                    modifiers=set(), 
                    type=ReferenceType(
                        arguments=None, 
                        dimensions=[], 
                        name=Element, 
                        sub_type=None
                    )
                )
                '''
                if stmt.type.name == 'Element' and isinstance(decl.initializer, MethodInvocation) and decl.initializer.member == 'createElement' and decl.initializer.qualifier==self.docParam.name:
                    self._handle_createElement(decl)
    
    def _handle_createElement(self, decl: VariableDeclarator) -> None:
        tag: str = decl.initializer.arguments[0].value.strip('"')
        varname: str = decl.name
        self.createPlaceholderElement(tag, varname)

    def createPlaceholderElement(self, tag: str, varname: str) -> None:
        print(f'create: {tag!r} as {varname!r}')
        c = SavableInfo(varname, tag)
        self.currentElements[tag] = c  # SavableInfo(varname, tag)
        self.currentVars[varname] = tag

    def createScalarElement(self, tag: str, varname: str) -> None:
        print(f'create: {tag!r} as {varname!r}')
        c = SavableInfo(varname, tag)
        self.currentElements[tag] = c  # SavableInfo(varname, tag)
        self.currentVars[varname] = tag

    

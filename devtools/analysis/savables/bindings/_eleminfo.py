import logging
from typing import Callable, Optional

from analysis.savables.enums import EAttrType, EEnumStyle
from buildtools.bt_logging import IndentLogger
from buildtools.indentation import IndentWriter
from ruamel.yaml import CommentedMap

from .debug import DEBUG_ON, EDebugState, debug

log = IndentLogger(logging.getLogger(__name__))


class _ElemInfo:
    def __init__(self) -> None:
        self.attr: Optional[str] = None
        self.elem: Optional[str] = None
        self.type: Optional[EAttrType] = None
        self.class_type: Optional[str] = None
        self.class_package: Optional[str] = None
        self.enum_type: Optional[str] = None
        self.enum_package: Optional[str] = None
        self.enum_style: Optional[EEnumStyle] = None

    def buildWriteLine(self, w: IndentWriter) -> Callable[[str], IndentWriter]:
        if DEBUG_ON:
            import inspect

            frame = inspect.currentframe().f_back
            method = frame.f_code.co_name
            suffix = f" # {self.__class__.__name__}.{method}()"

            def writeline(l: str) -> IndentWriter:
                return w.writeline(l + suffix)

        else:

            def writeline(l: str) -> IndentWriter:
                return w.writeline(l)

        return writeline

    def deserialize(self, data: dict) -> None:
        self.attr = data.get("attr")
        self.elem = data.get("elem")
        self.type = None
        if (typ := data.get("type")) is not None:
            self.type = EAttrType[typ.upper()]
        if (classData := data.get("class")) is not None:
            self.class_type = classData.get("type")
            self.class_package = classData.get("package")
        if (enumData := data.get("enum")) is not None:
            self.enum_type = enumData.get("type")
            self.enum_package = enumData.get("package")
            self.enum_style = EEnumStyle[enumData.get("style", "name").upper()]

    def serialize(self) -> CommentedMap:
        cm = CommentedMap()
        if self.attr is not None:
            cm["attr"] = self.attr
        if self.elem is not None:
            cm["elem"] = self.elem
        if self.type is not None:
            cm["type"] = self.type.name.lower()
        if self.class_package is not None:
            cm["class"] = self.serialize_class()
        if self.enum_package is not None:
            cm["enum"] = self.serialize_enum()
        return cm

    def serialize_class(self) -> CommentedMap:
        cm = CommentedMap()
        cm["package"] = self.class_package
        cm["type"] = self.class_type
        return cm

    def serialize_enum(self) -> CommentedMap:
        cm = CommentedMap()
        cm["style"] = self.enum_style.name
        cm["package"] = self.enum_package
        cm["type"] = self.enum_type
        return cm

    def fromXMLCode(self, evar: str) -> str:
        if self.class_type:
            return ""
        o = ""
        if self.attr and not self.elem:
            o = f"{evar}.attrib[{self.attr!r}]"
        elif not self.attr and self.elem:
            o = f"{evar}.find({self.elem!r}).text"
        elif self.attr and self.elem:
            o = f"{evar}.find({self.elem!r}).attrib[{self.attr!r}]"
        elif not self.attr and not self.elem:
            o = f"{evar}.text"
        if self.type is not None:
            if self.type == EAttrType.BOOL:
                o = f"XML2BOOL[{o}]"
            elif self.type != EAttrType.STR:
                o = f"{self.type.name.lower()}({o})"
        if self.enum_type:
            if self.enum_style == EEnumStyle.NAME:
                o = f"{self.enum_type}[{o}]"
            if self.enum_style == EEnumStyle.VALUE:
                o = f"{self.enum_type}({o})"
        return o

    def genStr2TypeConverter(self, vvar: str) -> str:
        if self.type == EAttrType.STR:
            return vvar
        if self.type == EAttrType.BOOL:
            return f"XML2BOOL[{vvar}]"
        return f"{self.type.name.lower()}({vvar})"

    def needsChildElem(self) -> bool:
        if self.class_type:
            if not self.attr and self.attr:
                return False
        return True

    def toXMLCode(self, w: IndentWriter, evar: str, valvar: str) -> None:
        debug(__file__, self, "toXMLCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)
        assert self.class_type is None

        valvar = self.genType2StrConverter(valvar)

        if self.attr and not self.elem:
            writeline(f"{evar}.attrib[{self.attr!r}] = {valvar}")
        elif not self.attr and self.elem:
            writeline(f"etree.SubElement({evar}, {self.elem!r}).text = {valvar}")
        elif self.attr and self.elem:
            writeline(
                f"etree.SubElement({evar}, {self.elem!r}, {{{self.attr!r}: {valvar}}})"
            )
        elif not self.attr and not self.elem:
            writeline(f"{evar}.text = {valvar}")
        debug(__file__, self, "toXMLCode", EDebugState.START, locals())

    def toDictCode(self, w: IndentWriter, outvar: str, valvar: str) -> None:
        assert self.class_type is None
        debug(__file__, self, "toDictCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)

        valvar = self.genType2StrConverter4Dict(valvar)
        writeline(f"{outvar} = {valvar}")
        debug(__file__, self, "toDictCode", EDebugState.START, locals())

    def fromDictCode(self, w: IndentWriter, var: str) -> str:
        o = var
        if self.enum_type:
            if self.enum_style == EEnumStyle.NAME:
                o = f"{self.enum_type}[{o}]"
            if self.enum_style == EEnumStyle.VALUE:
                o = f"{self.enum_type}({o})"

        # valvar = self.genType2StrConverter4Dict(valvar)
        return o

    def genType2StrConverter4Dict(self, valvar: str) -> str:
        if self.enum_style == EEnumStyle.NAME:
            valvar = f"{valvar}.name"
        elif self.enum_style == EEnumStyle.VALUE:
            valvar = f"str({valvar}.value)"
        # elif self.type == EAttrType.BOOL:
        #     valvar = f'str({valvar}).lower()'
        # elif self.type != EAttrType.STR:
        #     valvar = f'str({valvar})'
        return valvar

    def genType2StrConverter(self, valvar: str) -> str:
        if self.enum_style == EEnumStyle.NAME:
            valvar = f"{valvar}.name"
        elif self.enum_style == EEnumStyle.VALUE:
            valvar = f"str({valvar}.value)"
        elif self.type == EAttrType.BOOL:
            valvar = f"str({valvar}).lower()"
        elif self.type != EAttrType.STR:
            valvar = f"str({valvar})"
        return valvar

    def addImports(self, sg: "SavableGenerator") -> None:
        if self.class_type and self.class_package:
            sg.addImportFrom(self.class_package, [self.class_type])
        if self.enum_type and self.enum_package:
            sg.addImportFrom(self.enum_package, [self.enum_type])
        if self.type == EAttrType.BOOL:
            sg.addImportFrom("lilitools.saves.savable", ["XML2BOOL"])

    def getTypeHint(self) -> str:
        if self.class_type is None and self.enum_type is None:
            if self.type == EAttrType.INT:
                return "int"
            elif self.type == EAttrType.FLOAT:
                return "float"
            elif self.type == EAttrType.BOOL:
                return "bool"
            elif self.type == EAttrType.STR:
                return "str"
        elif self.enum_type is not None:
            return self.enum_type
        elif self.class_type is not None:
            return self.class_type
        return "Any"

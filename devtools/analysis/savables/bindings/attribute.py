import logging
from typing import Any, Optional, Union

from analysis.savables.enums import EAttrType, EEnumStyle
from buildtools.bt_logging import IndentLogger
from buildtools.indentation import IndentWriter
from ruamel.yaml import CommentedMap

from .base import Binding
from .debug import EDebugState, debug

log = IndentLogger(logging.getLogger(__name__))


class AttrBinding(Binding):

    _XMLID_ATTR: str = "attribute"

    def __init__(self) -> None:
        super().__init__()
        self.xml_attr: str = ""
        self.optional: bool = False
        self.default: Optional[Any] = None
        self.type: EAttrType = EAttrType.STR
        self.null_value: Optional[Any] = None
        self.enum_style: Optional[EEnumStyle] = None
        self.enum_type: Optional[str] = None
        self.enum_package: Optional[str] = None

    def addImportsTo(self, sg: "SavableGenerator") -> None:
        # sg.addImportFrom('lilitools.saves.savable', [self.getBinderMethod()])
        sg.addImportFrom(
            "lilitools.saves.exceptions",
            ["AttributeRequiredException", "KeyRequiredException"],
        )
        if self.optional:
            sg.addImportFrom("typing", ["Optional"])
        if self.enum_package and self.enum_type:
            sg.addImportFrom(self.enum_package, [self.enum_type])
        if self.type == EAttrType.BOOL:
            sg.addImportFrom("lilitools.saves.savable", ["XML2BOOL"])

    def deserialize(self, key: str, value: Optional[Union[str, list, dict]]) -> None:
        super().deserialize(key, value)
        if value is None:
            self.xml_attr = self.cls_attr
        elif isinstance(value, str):
            self.type = EAttrType[value.upper()]
            self.xml_attr = self.cls_attr
        elif isinstance(value, list):
            self.xml_attr = value[0]
            self.type = EAttrType[value[1].upper()]
        else:
            self.xml_attr = value.get("attr", value.get("attrib", self.cls_attr))
            self.default = value.get("default", None)
            vt = value.get("type", "STR").upper()
            self.type = EAttrType[vt]
            self.optional = value.get("optional", False)
            self.null_value = value.get("null-value", None)
            if "enum" in value:
                enumvalue = value["enum"]
                self.enum_package = enumvalue["package"]
                self.enum_type = enumvalue["type"]
                self.enum_style = EEnumStyle[enumvalue["style"]]

    def serialize(self) -> CommentedMap:
        cm = CommentedMap()
        dict_reqd = False
        if self.xml_attr != self.cls_attr:
            cm["attr"] = self.xml_attr
            dict_reqd = True
        if self.enum_package is None:
            cm["type"] = self.type.name.lower()
        if self.optional:
            cm["optional"] = True
            dict_reqd = True
        if self.default is not None:
            cm["default"] = self.default
            dict_reqd = True
        if self.null_value is not None:
            cm["null-value"] = self.null_value
            dict_reqd = True
        if self.enum_package is not None:
            cm["enum"] = self.serialize_enum()
            dict_reqd = True
        if not dict_reqd:
            return self.finishSerializing(self.type.name.lower())
        else:
            return self.finishSerializing(cm)

    def serialize_enum(self) -> CommentedMap:
        cm = CommentedMap()
        assert self.enum_package is not None
        assert self.enum_style is not None
        assert self.enum_type is not None
        cm["style"] = self.enum_style.name
        cm["package"] = self.enum_package
        cm["type"] = self.enum_type
        return cm

    def getBinderMethod(self) -> str:
        if self.type == EAttrType.STR:
            return "BindAttrStr"
        elif self.type == EAttrType.INT:
            return "BindAttrInt"
        elif self.type == EAttrType.FLOAT:
            return "BindAttrFloat"
        elif self.type == EAttrType.BOOL:
            return "BindAttrBool"
        else:
            return f"UNKNOWN ({self.type!r})"

    def getAttrType(self) -> str:
        o = ""
        if self.enum_type:
            o = self.enum_type
        else:
            if self.type == EAttrType.STR:
                o = "str"
            elif self.type == EAttrType.INT:
                o = "int"
            elif self.type == EAttrType.FLOAT:
                o = "float"
            elif self.type == EAttrType.BOOL:
                o = "bool"
            else:
                return f"UNKNOWN ({self.type!r})"
        if self.optional and self.default is None:
            o = f"Optional[{o}]"
        return o

    def getAttrDefault(self) -> str:
        if self.default is not None:
            if self.enum_type:
                if isinstance(self.default, str):
                    return f"{self.enum_type}.{self.default}"
                else:
                    return f"{self.enum_type}({self.default!r})"
            else:
                return repr(self.default)
        if self.optional and not self.default:
            return "None"
        if self.enum_type:
            return f"{self.enum_type}(0)"
        if self.type == EAttrType.STR:
            return "''"
        elif self.type == EAttrType.INT:
            return "0"
        elif self.type == EAttrType.FLOAT:
            return "0."
        elif self.type == EAttrType.BOOL:
            return "False"
        else:
            return f"UNKNOWN ({self.type!r})"

    def getDictKey(self) -> str:
        return self.dict_key or self.cls_attr or self.xml_attr

    def ATTRMAPCode(self, w: IndentWriter) -> None:
        args = []
        if self.xml_attr == self.cls_attr:
            args += [repr(self.xml_attr)]
        else:
            args += [repr(self.cls_attr), repr(self.xml_attr)]
        if self.optional:
            args.append("optional=True")
        binder = self.getBinderMethod()
        w.writeline(f"#{binder}({self._args2str(args)}),")

    def initCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "initCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)
        debug(__file__, self, "initCode", EDebugState.END, locals())
        w.writeline(
            f"self.{self.cls_attr}: {self.getAttrType()} = {self.getAttrDefault()}"
        )

    def declareXMLEntities(self) -> None:
        self.declareAttr(self._XMLID_ATTR, self.xml_attr)

    def fromDictCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "fromDictCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)

        key = self.dict_key or self.cls_attr or self.xml_attr
        v = f"data[{key!r}]"
        suffix = ""
        if self.optional is False:
            with writeline(f"if {key!r} not in data:"):
                writeline(
                    f"raise KeyRequiredException(self, data, {self.cls_attr!r}, {self.getDictKey()!r})"
                )
        else:
            suffix = f" if {key!r} in data else None"
        if self.enum_type:
            if self.enum_style == EEnumStyle.NAME:
                v = f"{self.enum_type}[{v}]"
            else:
                v = f"{self.enum_type}(int({v}))"
        else:
            if self.type == EAttrType.STR:
                v = f"str({v})"
            elif self.type == EAttrType.INT:
                v = f"int({v})"
            elif self.type == EAttrType.FLOAT:
                v = f"float({v})"
            elif self.type == EAttrType.BOOL:
                v = f"bool({v})"
        writeline(f"self.{self.cls_attr} = {v}{suffix}")
        debug(__file__, self, "fromDictCode", EDebugState.END, locals())

    def fromXMLCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "fromXMLCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)

        writeline(f"value = e.get({self.genXMLAttrVar(self._XMLID_ATTR)}, None)")

        def _inner() -> None:
            v = f"value"
            if self.enum_type:
                if self.enum_style == EEnumStyle.NAME:
                    v = f"{self.enum_type}[{v}]"
                else:
                    v = f"{self.enum_type}(int({v}))"
            else:
                if self.type == EAttrType.STR:
                    v = f"str({v})"
                elif self.type == EAttrType.INT:
                    v = f"int({v})"
                elif self.type == EAttrType.FLOAT:
                    v = f"float({v})"
                elif self.type == EAttrType.BOOL:
                    v = f"XML2BOOL[{v}]"
            writeline(f"self.{self.cls_attr} = {v}")

        with writeline(f"if value is None:"):
            if self.optional:
                if self.default is not None:
                    writeline(f"self.{self.cls_attr} = {self.getAttrDefault()}")
                else:
                    writeline(f"self.{self.cls_attr} = None")
            else:
                # def __init__(self, cls: Any, element: etree._Element, xml_attr: str, cls_attr: str) -> None:
                writeline(
                    f"raise AttributeRequiredException(self, e, {self.genXMLAttrVar(self._XMLID_ATTR)}, {self.cls_attr!r})"
                )
        with writeline(f"else:"):
            if self.null_value is not None:
                with writeline(f"if value == {self.null_value!r}:"):
                    writeline(f"self.{self.cls_attr} = None")
                with writeline("else:"):
                    _inner()
            else:
                _inner()
        debug(__file__, self, "fromXMLCode", EDebugState.END, locals())

    def toDictCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "toDictCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)

        key = self.getDictKey()

        def _inner() -> None:
            v = f"self.{self.cls_attr}"
            if self.enum_type:
                match self.enum_style:
                    case EEnumStyle.NAME:
                        v += ".name"
                    case EEnumStyle.VALUE:
                        v = f"{v}.value"
                    case _:
                        v = f"str({v})"
            else:
                match self.type:
                    case EAttrType.BOOL:
                        v = f"bool({v})"
                    case EAttrType.FLOAT:
                        v = f"float({v})"
                    case EAttrType.INT:
                        v = f"int({v})"
            writeline(f"data[{key!r}] = {v}")

        if self.optional:
            if self.default is not None:
                if self.null_value is not None:
                    with writeline(f"if self.{self.cls_attr} is None:"):
                        writeline(f"data[{key!r}] = {self.null_value!r}")
                    with writeline(
                        f"elif self.{self.cls_attr} != {self.getAttrDefault()}:"
                    ):
                        _inner()
                else:
                    with writeline(
                        f"if self.{self.cls_attr} != {self.getAttrDefault()}:"
                    ):
                        _inner()
            else:
                if self.null_value is not None:
                    with writeline(f"if self.{self.cls_attr} is None:"):
                        writeline(f"data[{key!r}] = {self.null_value!r}")
                    with writeline("else:"):
                        _inner()
                else:
                    with writeline(f"if self.{self.cls_attr} is not None:"):
                        _inner()
        else:
            _inner()
        debug(__file__, self, "toDictCode", EDebugState.END, locals())

    def toXMLCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "toXMLCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)

        def _inner() -> None:
            v = f"self.{self.cls_attr}"
            if self.enum_type:
                match self.enum_style:
                    case EEnumStyle.NAME:
                        v += ".name"
                    case EEnumStyle.VALUE:
                        v = f"str({v}.value)"
                    case _:
                        v = f"str({v})"
            else:
                match self.type:
                    case EAttrType.BOOL:
                        v = f"str({v}).lower()"
                    case EAttrType.FLOAT:
                        v = f"str(float({v}))"
                    case (
                        _
                    ):  # self.type in (EAttrType.FLOAT, EAttrType.FLOAT, EAttrType.STR):
                        v = f"str({v})"
            writeline(f"e.attrib[{self.genXMLAttrVar(self._XMLID_ATTR)}] = {v}")

        if self.optional:
            if self.default is not None:
                if self.null_value is not None:
                    with writeline(f"if self.{self.cls_attr} is None:"):
                        writeline(
                            f"e.attrib[{self.genXMLAttrVar(self._XMLID_ATTR)}] = {self.null_value!r}"
                        )
                    with writeline(
                        f"elif self.{self.cls_attr} != {self.getAttrDefault()}:"
                    ):
                        _inner()
                else:
                    with writeline(
                        f"if self.{self.cls_attr} != {self.getAttrDefault()}:"
                    ):
                        _inner()
            else:
                if self.null_value is not None:
                    with writeline(f"if self.{self.cls_attr} is None:"):
                        writeline(
                            f"e.attrib[{self.genXMLAttrVar(self._XMLID_ATTR)}] = {self.null_value!r}"
                        )
                    with writeline("else:"):
                        _inner()
                else:
                    with writeline(f"if self.{self.cls_attr} is not None:"):
                        _inner()
        else:
            _inner()
        debug(__file__, self, "toXMLCode", EDebugState.END, locals())

import logging
from typing import Any, Callable, Dict, Iterable, List, Optional, Tuple, Union

from buildtools.bt_logging import IndentLogger
from buildtools.indentation import IndentWriter
from ruamel.yaml import CommentedMap

from .debug import DEBUG_ON

log = IndentLogger(logging.getLogger(__name__))


class Declarations:
    def __init__(self) -> None:
        self.varbinds: Dict[Tuple[str, str], Optional[str]] = {}
        self.iter: Optional[Any] = None

    def declare(self, name: str, sig: str, value: Optional[str] = None) -> None:
        self.varbinds[(name, sig)] = value

    def isDeclared(self, name: str, sig: str) -> bool:
        return (name, sig) in self.varbinds


class Binding:
    def __init__(self) -> None:
        self.cls_attr: Optional[str] = None
        self.dict_key: Optional[str] = None
        self.default: Optional[str] = None
        self._globals: Declarations = Declarations()
        self._tags: Dict[Tuple[str, str], str] = {}
        self._attrs: Dict[Tuple[str, str], str] = {}

    def addImportsTo(self, sg: "analysis.savable_generator.SavableGenerator") -> None:
        pass

    def ATTRMAPCode(self, w: IndentWriter) -> None:
        pass

    def initCode(self, w: IndentWriter) -> None:
        pass

    def toXMLCode(self, w: IndentWriter) -> None:
        pass

    def fromXMLCode(self, w: IndentWriter) -> None:
        pass

    def toDictCode(self, w: IndentWriter) -> None:
        pass

    def fromDictCode(self, w: IndentWriter) -> None:
        pass

    def arbitraryMethodCode(self, w: IndentWriter) -> None:
        pass

    def deserialize(self, key: str, value: Union[str, list, dict]) -> None:
        self.config = value
        self.cls_attr = key

    def serialize(self) -> CommentedMap:
        return CommentedMap()

    def finishSerializing(self, value: CommentedMap, key: str = None) -> CommentedMap:
        cm = CommentedMap()
        cm[key or self.cls_attr] = value
        return cm

    def _args2str(self, args: List[str]) -> str:
        return ", ".join(args)

    def registerAttrs(self) -> Iterable[str]:
        return []

    def registerTags(self) -> Iterable[str]:
        return []

    def _stuff_for_generator(
        self,
        globals: Declarations,
        attrmap: Dict[Tuple[str, str], str],
        tagmap: Dict[Tuple[str, str], str],
    ) -> None:
        self._globals = globals
        self._tags = tagmap
        self._attrs = attrmap

    def declareXMLEntities(self) -> None:
        pass

    def declareVarsForFromXML(self, decls: Declarations) -> None:
        pass

    def declareVarsForToXML(self, decls: Declarations) -> None:
        pass

    def declareVarsForFromDict(self, decls: Declarations) -> None:
        pass

    def declareVarsForToDict(self, decls: Declarations) -> None:
        pass

    def declareTag(self, ident: str, initial_value: Optional[str] = None) -> None:
        # if not initial_value:
        #     initial_value = ident
        self._globals.declare(self.genXMLTagVar(ident, ""), "str", initial_value)
        self._tags[(self.cls_attr, ident)] = self.genXMLTagVar(ident, "")

    def declareAttr(self, ident: str, initial_value: Optional[str] = None) -> None:
        # if not initial_value:
        #    initial_value = ident
        self._globals.declare(self.genXMLAttrVar(ident, ""), "str", initial_value)
        self._attrs[(self.cls_attr, ident)] = self.genXMLAttrVar(ident, "")

    def genXMLTagVar(self, ident: str, prefix: Optional[str] = None) -> str:
        if prefix is None:
            prefix = "self."
        return (
            prefix
            + "_"
            + "_".join(["XMLID", "TAG", self.cls_attr.upper(), ident.upper()])
        )

    def genXMLAttrVar(self, ident: str, prefix: Optional[str] = None) -> str:
        if prefix is None:
            prefix = "self."
        return (
            prefix
            + "_"
            + "_".join(["XMLID", "ATTR", self.cls_attr.upper(), ident.upper()])
        )

    def genDictKey(self, ident: str) -> str:
        return ident

    def getDictKey(self) -> str:
        return self.dict_key or self.cls_attr

    def buildWriteLine(self, w: IndentWriter) -> Callable[[str], IndentWriter]:
        if DEBUG_ON:
            import inspect

            def writeline(l: str) -> IndentWriter:
                frame = inspect.currentframe().f_back
                method = frame.f_code.co_name
                line = frame.f_lineno
                suffix = f" # {self.__class__.__name__}.{method}() line {line:,}"
                return w.writeline(l + suffix)

        else:

            def writeline(l: str) -> IndentWriter:
                return w.writeline(l)

        return writeline

import logging
from enum import IntEnum
from typing import Dict, Optional, Type, Union, cast

import click
from analysis.savables.enums import EAttrType
from buildtools.bt_logging import IndentLogger
from buildtools.indentation import IndentWriter
from ruamel.yaml import CommentedMap

from .base import Binding
from .debug import EDebugState, debug

log = IndentLogger(logging.getLogger(__name__))


class EChildBindingStyle(IntEnum):
    SCALAR = 0
    LIST = 1
    DICT = 2
    SET = 3
    PLACEHOLDER = 4
    COMMENT = 5
    CDATA = 6
    STACK = 7
    INNERTEXT = 8


CHILD_BINDING_STYLES: Dict[EChildBindingStyle, Type["BaseChildBinding"]] = {}


def register_child_binding_style(cbsCls: Type["BaseChildBinding"]) -> None:
    CHILD_BINDING_STYLES[cbsCls.STYLE] = cbsCls


def createChildBindingFrom(
    key: str, data: Optional[Union[str, dict]], preserve_save_ordering: bool = False
) -> "BaseChildBinding":
    # print(repr(key), repr(data))
    style = "SCALAR"
    binding: BaseChildBinding
    if data is None or isinstance(data, str):
        if key == "@COMMENT":
            binding = CHILD_BINDING_STYLES[EChildBindingStyle.COMMENT]()
        else:
            binding = CHILD_BINDING_STYLES[EChildBindingStyle.SCALAR]()
    else:
        style = data.get("style", "scalar").upper()
        if preserve_save_ordering:
            if style == "SET":
                click.secho(
                    "W: FIXME: OVERRIDING ALL SETS TO LISTS TEMPORARILY",
                    fg="yellow",
                    err=True,
                )
                style = "LIST"
        binding = CHILD_BINDING_STYLES[EChildBindingStyle[style]]()
        if preserve_save_ordering:
            if binding.STYLE in (EChildBindingStyle.LIST, EChildBindingStyle.STACK):
                old = data.get("sort-key", None)
                if old:
                    click.secho(f"W: Clearing sort-key: {old}", fg="yellow")
                    del data["sort-key"]
            if binding.STYLE in (EChildBindingStyle.DICT,):
                click.secho(
                    "W: FIXME: OVERRIDING ALL DICTS TO ORDERED-DICTS TEMPORARILY",
                    fg="yellow",
                    err=True,
                )
                old = data.get("sort-key", None)
                if old:
                    click.secho(f"W: Clearing sort-key: {old}", fg="yellow")
                    del data["sort-key"]
                if "imports" not in data.keys():
                    data["imports"] = {"collections": ["OrderedDict"]}
                else:
                    data["imports"]["collections"] = ["OrderedDict"]
                data["init-type"] = "OrderedDict"
                data["default"] = "OrderedDict()"
    binding.deserialize(key, data)
    # print(repr(key), repr(data), binding)
    return binding


class BaseChildBinding(Binding):
    STYLE: EChildBindingStyle

    def __init__(self) -> None:
        super().__init__()
        self.xml_element: str = ""
        self.optional: bool = False

    def addImportsTo(self, sg: "SavableGenerator") -> None:
        if self.optional:
            sg.addImportFrom("typing", ["Optional"])

    def deserialize(self, key: str, value: Optional[Union[str, list, dict]]) -> None:
        super().deserialize(key, value)

    def serialize(self) -> CommentedMap:
        cm = cast(CommentedMap, super().serialize())
        if self.STYLE != EChildBindingStyle.SCALAR:
            cm["style"] = self.STYLE.name.lower()
        return cm

    def getPyTypeFrom(self, typ: EAttrType) -> str:
        o = ""
        if typ == EAttrType.STR:
            o = "str"
        elif typ == EAttrType.INT:
            o = "int"
        elif typ == EAttrType.FLOAT:
            o = "float"
        elif typ == EAttrType.BOOL:
            o = "bool"
        else:
            return f"UNKNOWN ({typ!r})"
        return o

    def getPyDefaultFrom(self, typ: EAttrType) -> str:
        if typ == EAttrType.STR:
            return "''"
        elif typ == EAttrType.INT:
            return "0"
        elif typ == EAttrType.FLOAT:
            return "0."
        elif typ == EAttrType.BOOL:
            return "False"
        else:
            return f"UNKNOWN ({typ!r})"

    def initCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "initCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)
        writeline(
            f"self.{self.cls_attr}: {self.getAttrType()} = {self.getAttrDefault()}"
        )
        debug(__file__, self, "initCode", EDebugState.END, locals())

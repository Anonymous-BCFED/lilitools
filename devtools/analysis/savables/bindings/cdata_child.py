from __future__ import annotations

import logging
import typing
from typing import Any, Callable, Iterable, Optional, Union, cast

from analysis.savables.bindings.base_child import (
    BaseChildBinding,
    EChildBindingStyle,
    register_child_binding_style,
)
from analysis.savables.enums import EAttrType
from buildtools.bt_logging import IndentLogger
from buildtools.indentation import IndentWriter
from ruamel.yaml import CommentedMap

from .debug import EDebugState, debug

if typing.TYPE_CHECKING:
    from analysis.savable_generator import SavableGenerator

log = IndentLogger(logging.getLogger(__name__))


class CDATAChildBinding(BaseChildBinding):
    STYLE = EChildBindingStyle.CDATA

    def __init__(self) -> None:
        super().__init__()
        self.type: EAttrType = EAttrType.STR

    def addImportsTo(self, sg: SavableGenerator) -> None:
        if self.type == EAttrType.BOOL:
            sg.addImportFrom("lilitools.saves.savable", ["XML2BOOL"])

    def registerAttrs(self) -> Iterable[str]:
        return set()

    def registerTags(self) -> Iterable[str]:
        return set()

    def getAttrType(self) -> str:
        return self.getPyTypeFrom(self.type)

    def getAttrDefault(self) -> str:
        return self.getPyDefaultFrom(self.type)

    def deserialize(self, key: str, value: Optional[Union[str, list, dict]]) -> None:
        super().deserialize(key, value)
        if isinstance(value, str):
            self.type = EAttrType[value.upper()]
        elif isinstance(value, dict):
            self.type = EAttrType[value.get("type", "STR").upper()]

    def serialize(self) -> CommentedMap:
        cm = cast(CommentedMap, super().serialize())
        if self.type != EAttrType.STR:
            cm["type"] = self.type.name.lower()
        return self.finishSerializing(cm)

    def initCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "initCode", EDebugState.START, locals())
        writeline: Callable[[str], IndentWriter] = self.buildWriteLine(w)
        writeline(
            f"self.{self.cls_attr}: {self.getAttrType()} = {self.getAttrDefault()} # CDATA"
        )
        debug(__file__, self, "initCode", EDebugState.END, locals())

    def toXMLCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "toXMLCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)
        writeline(f"e.text = etree.CDATA(str(self.{self.cls_attr}))")
        debug(__file__, self, "toXMLCode", EDebugState.END, locals())

    def fromXMLCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "fromXMLCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)

        rhs = "e.text"
        if self.type == EAttrType.BOOL:
            rhs = f"XML2BOOL[{rhs}]"
        elif self.type == EAttrType.FLOAT:
            rhs = f"float({rhs})"
        elif self.type == EAttrType.INT:
            rhs = f"int({rhs})"
        writeline(f"self.{self.cls_attr} = {rhs}")
        debug(__file__, self, "fromXMLCode", EDebugState.END, locals())

    def toDictCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "toDictCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)

        rhs = f"self.{self.cls_attr}"
        if self.type == EAttrType.BOOL:
            rhs = f"bool({rhs})"
        elif self.type == EAttrType.FLOAT:
            rhs = f"float({rhs})"
        elif self.type == EAttrType.INT:
            rhs = f"int({rhs})"
        writeline(f"data[{self.getDictKey()!r}] = {rhs}")
        debug(__file__, self, "toDictCode", EDebugState.END, locals())

    def fromDictCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "fromDictCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)

        rhs = f"data[{self.getDictKey()!r}]"
        if self.type == EAttrType.BOOL:
            rhs = f"bool({rhs})"
        elif self.type == EAttrType.FLOAT:
            rhs = f"float({rhs})"
        elif self.type == EAttrType.INT:
            rhs = f"int({rhs})"
        writeline(f"self.{self.cls_attr} = {rhs}")
        debug(__file__, self, "fromDictCode", EDebugState.END, locals())


register_child_binding_style(CDATAChildBinding)

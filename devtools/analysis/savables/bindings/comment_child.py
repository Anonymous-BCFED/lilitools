from __future__ import annotations

import logging
import typing
from typing import Iterable, Optional, Union

from analysis.savables.bindings.base_child import (
    BaseChildBinding,
    EChildBindingStyle,
    register_child_binding_style,
)
from buildtools.bt_logging import IndentLogger
from buildtools.indentation import IndentWriter
from ruamel.yaml import CommentedMap

from .debug import EDebugState, debug

if typing.TYPE_CHECKING:
    from analysis.savable_generator import SavableGenerator

log = IndentLogger(logging.getLogger(__name__))


class CommentChildBinding(BaseChildBinding):
    STYLE = EChildBindingStyle.COMMENT
    CURRENT_COMMENT = 0

    def __init__(self) -> None:
        super().__init__()
        self.content: str = ""
        self.comment_id: int = CommentChildBinding.CURRENT_COMMENT
        CommentChildBinding.CURRENT_COMMENT = CommentChildBinding.CURRENT_COMMENT + 1

    def addImportsTo(self, sg: SavableGenerator) -> None:
        return

    def registerAttrs(self) -> Iterable[str]:
        return set()

    def registerTags(self) -> Iterable[str]:
        return set()

    def deserialize(self, key: str, value: Optional[Union[str, list, dict]]) -> None:
        super().deserialize(key, value)
        if isinstance(value, str):
            self.content = value
        elif isinstance(value, dict):
            self.content = value["content"]

    def serialize(self) -> CommentedMap:
        cm = CommentedMap()
        cm["@COMMENT"] = self.content
        return cm

    def initCode(self, w: IndentWriter) -> None:
        return

    def toXMLCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "toXMLCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)

        writeline(f"e.append(etree.Comment({self.content!r}))")
        debug(__file__, self, "toXMLCode", EDebugState.END, locals())

    def toDictCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "toDictCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)
        writeline(f'data["@comment-{self.comment_id:04d}"] = {self.content!r}')
        debug(__file__, self, "toDictCode", EDebugState.END, locals())


register_child_binding_style(CommentChildBinding)

import logging
from typing import Dict, Iterable, List, Optional, Union, cast

from analysis.savables.bindings.base_child import (
    BaseChildBinding,
    EChildBindingStyle,
    register_child_binding_style,
)
from analysis.savables.enums import EAttrType
from buildtools.bt_logging import IndentLogger
from buildtools.indentation import IndentWriter
from ruamel.yaml import CommentedMap
from typing import cast

from ._eleminfo import _ElemInfo

from ruamel.yaml import CommentedMap

log = IndentLogger(logging.getLogger(__name__))


class ComplexChildBinding(BaseChildBinding):
    STYLE = EChildBindingStyle.COMPLEX

    def __init__(self) -> None:
        super().__init__()
        self.parent_elem_name: str = ""
        self.child_elem_name: str = ""
        self.children: List[_ElemInfo] = []
        self.optional: bool = False

    def addImportsTo(self, sg: "SavableGenerator") -> None:
        sg.addImportFrom("typing", ["Dict"])
        for e in self.children:
            if e.class_type:
                sg.addImportFrom(e.class_package, [e.class_type])

    def deserialize(self, key: str, value: Optional[Union[str, list, dict]]) -> None:
        super().deserialize(key, value)
        self.parent_elem_name = value["parent-elem"]
        self.child_elem_name = value["child-elem"]
        for edata in value["children"]:
            e = _ElemInfo()
            e.deserialize(edata)
            assert e.attr or e.elem
            self.children.append(e)
        self.type = value.get("type", "str")
        self.optional = value.get("optional", False)

    def serialize(self) -> CommentedMap:
        cm = cast(CommentedMap, super().serialize())
        cm["parent-elem"] = self.parent_elem_name
        cm["child-elem"] = self.child_elem_name
        if self.type != EAttrType.STR:
            cm["type"] = self.type.name.lower()
        return self.finishSerializing(cm)

    def getAttrType(self) -> str:
        return self.type

    def getAttrDefault(self) -> str:
        return self.default

    def _getPyTypeFromEInfo(self, e: _ElemInfo) -> str:
        o = self.getPyTypeFrom(e.type)
        if e.class_type:
            o = e.class_type
        return o

    def initCode(self, w: IndentWriter) -> None:
        w.writeline(
            f"self.{self.cls_attr}: Dict[{self._getPyTypeFromEInfo(self.key)}, {self._getPyTypeFromEInfo(self.value)}] = {{}}"
        )

    def registerTags(self) -> Iterable[str]:
        tags = {self.parent_elem_name, self.child_elem_name}
        if self.key.elem:
            tags.add(self.key.elem)
        if self.value.elem:
            tags.add(self.value.elem)
        return tags

    def registerAttrs(self) -> Iterable[str]:
        attrs = set()
        if self.key.attr:
            attrs.add(self.key.attr)
        if self.value.attr:
            attrs.add(self.value.attr)
        return attrs

    def fromXMLCode(self, w: IndentWriter) -> None:
        w.writeline(f"ce: str = self._tag_xml2cls[{self.child_elem_name!r}]")
        if self.key.elem:
            w.writeline(f"ke: str = self._tag_xml2cls[{self.key.elem!r}]")
        if self.key.attr:
            w.writeline(f"ka: str = self._attr_xml2cls[{self.key.attr!r}]")
        if self.value.elem:
            w.writeline(f"ve: str = self._tag_xml2cls[{self.value.elem!r}]")
        if self.value.attr:
            w.writeline(f"va: str = self._attr_xml2cls[{self.value.attr!r}]")

        def _inner(var: str) -> None:
            cvar = f"{self.child_elem_name}Elem"
            # with w.writeline(f'for {cvar} in {var}.findall(ce):'):
            with w.writeline(f"for {cvar} in {var}.iterfind(ce):"):
                kc = self.key.fromXMLCode(cvar)
                if self.key.class_type:
                    w.writeline(f"k = {self.key.class_type}()")
                    if self.value.elem:
                        cvar = f"{cvar}.find(ke)"
                    w.writeline(f"k.fromXML({cvar})")
                    kc = "k"
                vc = self.value.fromXMLCode(cvar)
                if self.value.class_type:
                    w.writeline(f"v = {self.value.class_type}()")
                    if self.value.elem:
                        cvar = f"{cvar}.find(ve)"
                    w.writeline(f"v.fromXML({cvar})")
                    vc = "v"
                w.writeline(f"self.{self.cls_attr}[{kc}] = {vc}")

        if self.optional:
            pvar = f"{self.parent_elem_name}Elem"
            with w.writeline(
                f"if ({pvar} := e.find(self._tag_xml2cls[{self.parent_elem_name!r}])) is not None:"
            ):
                _inner(pvar)
        else:
            _inner(f"e.find(self._tag_xml2cls[{self.parent_elem_name!r}])")

    def toXMLCode(self, w: IndentWriter) -> None:
        pvar: str = self.parent_elem_name + "Elem"
        cvar: str = self.child_elem_name + "Elem"

        def _inner() -> None:
            w.writeline(f"{pvar} = etree.SubElement(e, {self.parent_elem_name!r})")
            with w.writeline(f"for k, v in self.{self.cls_attr}.items():"):
                w.writeline(
                    f"{cvar} = etree.SubElement({pvar}, {self.child_elem_name!r})"
                )
                self.key.toXMLCode(w, cvar, "k")
                self.value.toXMLCode(w, cvar, "v")

        if self.optional:
            with w.writeline(f"if len(self.{self.cls_attr}):"):
                _inner()
        else:
            _inner()
"""
    def fromDictCode(self, w: IndentWriter) -> None:
        w.writeline(f"ce: str = self._tag_xml2cls[{self.child_elem_name!r}]")
        if self.key.elem:
            w.writeline(f"ke: str = self._tag_xml2cls[{self.key.elem!r}]")
        if self.key.attr:
            w.writeline(f"ka: str = self._attr_xml2cls[{self.key.attr!r}]")
        if self.value.elem:
            w.writeline(f"ve: str = self._tag_xml2cls[{self.value.elem!r}]")
        if self.value.attr:
            w.writeline(f"va: str = self._attr_xml2cls[{self.value.attr!r}]")

        def _inner(var: str) -> None:
            cvar = f"{self.child_elem_name}Elem"
            # with w.writeline(f'for {cvar} in {var}.findall(ce):'):
            with w.writeline(f"for {cvar} in {var}.iterfind(ce):"):
                kc = self.key.fromXMLCode(cvar)
                if self.key.class_type:
                    w.writeline(f"k = {self.key.class_type}()")
                    if self.value.elem:
                        cvar = f"{cvar}.find(ke)"
                    w.writeline(f"k.fromXML({cvar})")
                    kc = "k"
                vc = self.value.fromXMLCode(cvar)
                if self.value.class_type:
                    w.writeline(f"v = {self.value.class_type}()")
                    if self.value.elem:
                        cvar = f"{cvar}.find(ve)"
                    w.writeline(f"v.fromXML({cvar})")
                    vc = "v"
                w.writeline(f"self.{self.cls_attr}[{kc}] = {vc}")

        if self.optional:
            pvar = f"{self.parent_elem_name}Elem"
            with w.writeline(
                f"if ({pvar} := e.find(self._tag_xml2cls[{self.parent_elem_name!r}])) is not None:"
            ):
                _inner(pvar)
        else:
            _inner(f"e.find(self._tag_xml2cls[{self.parent_elem_name!r}])")

    def toDictCode(self, w: IndentWriter) -> None:
        pvar: str = self.parent_elem_name + "Elem"
        cvar: str = self.child_elem_name + "Elem"

        def _inner() -> None:
            w.writeline(f"{pvar} = etree.SubElement(e, {self.parent_elem_name!r})")
            with w.writeline(f"for k, v in self.{self.cls_attr}.items():"):
                w.writeline(
                    f"{cvar} = etree.SubElement({pvar}, {self.child_elem_name!r})"
                )
                self.key.toXMLCode(w, cvar, "k")
                self.value.toXMLCode(w, cvar, "v")

        if self.optional:
            with w.writeline(f"if len(self.{self.cls_attr}):"):
                _inner()
        else:
            _inner()
"""

register_child_binding_style(ComplexChildBinding)

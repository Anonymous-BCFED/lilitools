from enum import IntEnum
from typing import Any, Dict, Optional, Tuple

DEBUG_ON: bool = False
DEBUG_OVERRIDES: Dict[Tuple[str, str, str], bool] = {}


class EDebugState(IntEnum):
    START = 0
    END = 1
    IDFK = 2
    ERROR = 2


def debug(
    filename: str,
    i: Optional[object],
    method: str,
    state: EDebugState,
    locals_: Dict[str, Any],
) -> None:
    clsname: str = i.__class__.__name__ if i is not None else None
    key = (filename, clsname, method)
    if DEBUG_ON or DEBUG_OVERRIDES.get((filename, clsname, method), False):
        print(filename, clsname, method, state.name, locals_)

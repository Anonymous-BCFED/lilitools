import logging
from typing import Dict, List, Optional, Set, Union, cast

from analysis.savables.bindings.base import Declarations
from analysis.savables.bindings.base_child import (
    BaseChildBinding,
    EChildBindingStyle,
    register_child_binding_style,
)
from analysis.utils import copy_ca_to
from buildtools.bt_logging import IndentLogger
from buildtools.indentation import IndentWriter
from ruamel.yaml import CommentedMap, CommentedSeq
from ruamel.yaml.comments import Comment

from ._eleminfo import _ElemInfo
from .debug import EDebugState, debug

log = IndentLogger(logging.getLogger(__name__))


class DictChildBinding(BaseChildBinding):
    STYLE = EChildBindingStyle.DICT

    _PARENT_ELEMENT_VAR: str = "dp"
    _CHILD_ELEMENT_VAR: str = "dc"
    _SUBCHILD_ELEMENT_VAR: str = "dsc"
    _FOUND_ELEMENT_VAR: str = "df"
    _KEY_SAVABLE_VAR: str = "dk"
    _VALUE_SAVABLE_VAR: str = "dv"

    # We're not going to explicitly define the type right now
    _TRUE_KEY_VAR: str = "dK"
    _TRUE_VALUE_VAR: str = "dV"

    _DICT_KEY_VAR: str = "dictK"
    _DICT_VALUE_VAR: str = "dictV"

    _SERIALIZED_KEY_VAR = "sk"
    _SERIALIZED_VALUE_VAR = "sv"

    _XMLID_PARENT: str = "parent"
    _XMLID_CHILD: str = "child"
    _XMLID_SUBCHILD: str = "subchild"
    _XMLID_KEY_ATTR: str = "keyattr"
    _XMLID_KEY_ELEM: str = "keyelem"
    _XMLID_VALUE_ATTR: str = "valueattr"
    _XMLID_VALUE_ELEM: str = "valueelem"

    def __init__(self) -> None:
        super().__init__()
        self.parent_elem_name: str = ""
        self.child_elem_name: str = ""
        self.key: _ElemInfo = _ElemInfo()
        self.value: _ElemInfo = _ElemInfo()
        self.optional: bool = False
        self.empty_if_missing: bool = True
        self.skip_write_if: Optional[str] = None
        self.sort_key: Optional[str] = None

        self._ca: Optional[Comment] = None

        self.init_type: str = "Dict"
        self.ordered: bool = False
        self.default: str = "{}"
        self.imports: Dict[str, Set[str]] = {}

    def addImportsTo(self, sg: "SavableGenerator") -> None:
        sg.addImportFrom("typing", ["Dict", "Any"])
        if self.ordered:
            sg.addImportFrom("collections", ["OrderedDict"])
        if not self.optional:
            sg.addImportFrom("lilitools.saves.exceptions", ["ElementRequiredException"])
        if self.key.class_type or self.value.class_type:
            sg.addImportFrom("lilitools.saves.savable", ["Savable"])
        if self.key.class_type:
            sg.addImportFrom(self.key.class_package, [self.key.class_type])
        elif self.key.enum_type:
            sg.addImportFrom(self.key.enum_package, [self.key.enum_type])
        if self.value.class_type:
            sg.addImportFrom(self.value.class_package, [self.value.class_type])
        elif self.value.enum_type:
            sg.addImportFrom(self.value.enum_package, [self.value.enum_type])
        for i in self.imports:
            if isinstance(i, dict):
                k, v = next(iter(i.items()))
                sg.addImportFrom(k, v)
            else:
                sg.addImport(i)

    def deserialize(self, key: str, data: Optional[Union[str, list, dict]]) -> None:
        super().deserialize(key, data)
        assert isinstance(data, dict)
        if isinstance(data, CommentedMap):
            # print(repr(data.ca))
            self._ca = data.ca
        self.parent_elem_name = data["parent-elem"]
        self.child_elem_name = data["child-elem"]
        self.key.deserialize(data["key"])
        self.value.deserialize(data["value"])
        self.optional = data.get("optional", False)
        self.empty_if_missing = data.get(
            "empty-if-missing", True
        )  # Default to cleanest XML
        self.skip_write_if = data.get("skip-write-if", None)
        self.sort_key = data.get("sort-key", None)
        self.ordered = data.get("ordered", False)
        if self.ordered:
            self.init_type = "OrderedDict"
            self.default = "OrderedDict()"
        self.init_type = data.get("init-type", self.init_type)
        self.default = data.get("default", self.default)
        self.imports = data.get("imports", {})

        if isinstance(self.imports, CommentedMap):
            self.imports = {k: v for k, v in self.imports.items()}
        elif isinstance(self.imports, CommentedSeq):
            self.imports = {k: v for k, v in self.imports}

        assert (self.key.attr or self.key.elem) or (self.value.attr or self.value.elem)

    def serialize(self) -> CommentedMap:
        cm = cast(CommentedMap, super().serialize())
        if self.optional:
            cm["optional"] = True
        if self.ordered:
            cm["ordered"] = True
        cm["parent-elem"] = self.parent_elem_name
        cm["child-elem"] = self.child_elem_name
        if not self.empty_if_missing:
            cm["empty-if-missing"] = False
        if self.skip_write_if:
            cm["skip-write-if"] = self.skip_write_if
        if self.sort_key is not None:
            cm["sort-key"] = self.sort_key
        cm["key"] = self.key.serialize()
        cm["value"] = self.value.serialize()
        cm["init-type"] = self.init_type
        cm["default"] = self.default
        cm["imports"] = self.imports
        if self._ca:
            copy_ca_to(self._ca, cm.ca)
        return self.finishSerializing(cm)

    def declareXMLEntities(self) -> None:
        self.declareTag(self._XMLID_PARENT, self.parent_elem_name)
        self.declareTag(self._XMLID_CHILD, self.child_elem_name)
        if self.key.attr:
            self.declareAttr(self._XMLID_KEY_ATTR, self.key.attr)
        if self.key.elem:
            self.declareTag(self._XMLID_KEY_ELEM, self.key.elem)
        if self.value.attr:
            self.declareAttr(self._XMLID_VALUE_ATTR, self.value.attr)
        if self.value.elem:
            self.declareTag(self._XMLID_VALUE_ELEM, self.value.elem)

    def getAttrType(self) -> str:
        return self.type

    def getAttrDefault(self) -> str:
        return self.default

    def _getPyTypeFromEInfo(self, e: _ElemInfo) -> str:
        o = self.getPyTypeFrom(e.type)
        if e.class_type:
            o = e.class_type
        if e.enum_type:
            o = e.enum_type
        return o

    def initCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "initCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)
        writeline(
            f"self.{self.cls_attr}: {self.init_type}[{self._getPyTypeFromEInfo(self.key)}, {self._getPyTypeFromEInfo(self.value)}] = {self.default}"
        )
        debug(__file__, self, "initCode", EDebugState.END, locals())

    def declareVarsForFromXML(self, decls: Declarations) -> None:
        # Dict: Child Element
        decls.declare(self._CHILD_ELEMENT_VAR, "etree._Element")
        if self.optional:
            # Found Element
            decls.declare(self._FOUND_ELEMENT_VAR, "etree._Element")
        if self.key.class_type:
            decls.declare(self._KEY_SAVABLE_VAR, "Savable")
        if self.value.class_type:
            decls.declare(self._VALUE_SAVABLE_VAR, "Savable")

    def fromXMLCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "toXMLCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)

        def _inner(var: str) -> None:
            cvar = f"{self._CHILD_ELEMENT_VAR}"
            ctag = self.genXMLTagVar(self._XMLID_CHILD)
            if self.child_elem_name is None:
                if self.key.class_type:
                    ctag = self.genXMLTagVar(self._XMLID_KEY_ELEM)
                elif self.value.class_type:
                    ctag = self.genXMLTagVar(self._XMLID_VALUE_ELEM)
            # with writeline(f'for {cvar} in {var}.findinit_typeall({ctag}):'):
            with writeline(f"for {cvar} in {var}.iterfind({ctag}):"):
                if self.key.class_type:
                    writeline(f"{self._KEY_SAVABLE_VAR} = {self.key.class_type}()")
                    kcvar = cvar
                    if self.child_elem_name is not None and self.key.elem:
                        # kcvar = f'{cvar}.find({self.key.elem!r})'
                        kcvar = (
                            f"{cvar}.find({self.genXMLTagVar(self._XMLID_KEY_ELEM)})"
                        )
                    writeline(f"{self._KEY_SAVABLE_VAR}.fromXML({kcvar})")
                    kc = self._KEY_SAVABLE_VAR + ""
                else:
                    kc = self.key.fromXMLCode(cvar)
                if self.value.class_type:
                    writeline(f"{self._VALUE_SAVABLE_VAR} = {self.value.class_type}()")
                    vcvar = cvar
                    if self.child_elem_name is not None and self.value.elem:
                        # vcvar = f'{cvar}.find({self.value.elem!r})'
                        vcvar = (
                            f"{cvar}.find({self.genXMLTagVar(self._XMLID_VALUE_ELEM)})"
                        )
                    writeline(f"{self._VALUE_SAVABLE_VAR}.fromXML({vcvar})")
                    vc = self._VALUE_SAVABLE_VAR + ""
                else:
                    vc = self.value.fromXMLCode(cvar)
                writeline(f"self.{self.cls_attr}[{kc}] = {vc}")

        pvar = f"{self._FOUND_ELEMENT_VAR}"
        with writeline(
            f"if ({pvar} := e.find({self.genXMLTagVar(self._XMLID_PARENT)})) is not None:"
        ):
            _inner(pvar)
        if not self.optional:
            with writeline("else:"):
                if self.empty_if_missing:
                    writeline(f"self.{self.cls_attr} = {self.default}")
                else:
                    writeline(
                        f"raise ElementRequiredException(self, e, {self.cls_attr!r}, {self.genXMLTagVar(self._XMLID_PARENT)})"
                    )
        debug(__file__, self, "toXMLCode", EDebugState.END, locals())

    def declareVarsForToXML(self, decls: Declarations) -> None:
        decls.declare(self._PARENT_ELEMENT_VAR, "etree._Element")
        decls.declare(self._CHILD_ELEMENT_VAR, "etree._Element")
        if self.key.class_type:
            decls.declare(self._KEY_SAVABLE_VAR, "Savable")
            # decls.declare(self._TRUE_KEY_VAR, self.key.class_type)
        decls.declare(self._TRUE_KEY_VAR, "Any")
        if self.value.class_type:
            decls.declare(self._VALUE_SAVABLE_VAR, "Savable")
            # decls.declare(self._TRUE_VALUE_VAR, self.value.class_type)
        if self.child_elem_name:
            if (
                (self.value.elem is not None or self.key.elem is not None)
                or self.key.class_type is not None
                or self.value.class_type is not None
            ):
                decls.declare(self._SUBCHILD_ELEMENT_VAR, "etree._Element")
        decls.declare(self._TRUE_VALUE_VAR, "Any")

    def declareVarsForToDict(self, decls: Declarations) -> None:
        decls.declare(self._PARENT_ELEMENT_VAR, "Dict[str, Any]")
        # decls.declare(self._CHILD_ELEMENT_VAR, "Dict[str, Any]")
        if self.key.class_type:
            decls.declare(self._KEY_SAVABLE_VAR, "Savable")
            # decls.declare(self._TRUE_KEY_VAR, self.key.class_type)
        decls.declare(self._TRUE_KEY_VAR, "Any")
        if self.value.class_type:
            decls.declare(self._VALUE_SAVABLE_VAR, "Savable")
            # decls.declare(self._TRUE_VALUE_VAR, self.value.class_type)
        if self.child_elem_name:
            if (
                (self.value.elem is not None or self.key.elem is not None)
                or self.key.class_type is not None
                or self.value.class_type is not None
            ):
                decls.declare(self._SUBCHILD_ELEMENT_VAR, "Dict[str, Any]")
        decls.declare(self._TRUE_VALUE_VAR, "Any")

    def toXMLCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "toXMLCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)

        def _inner() -> None:
            writeline(
                f"{self._PARENT_ELEMENT_VAR} = etree.SubElement(e, {self.genXMLTagVar(self._XMLID_PARENT)})"
            )
            rhs: str = f"self.{self.cls_attr}.items()"
            if self.sort_key is not None:
                rhs = f"sorted({rhs}, key={self.sort_key})"
            with writeline(
                f"for {self._TRUE_KEY_VAR}, {self._TRUE_VALUE_VAR} in {rhs}:"
            ):
                pev = self._PARENT_ELEMENT_VAR
                cev = self._CHILD_ELEMENT_VAR
                xidc = self._XMLID_CHILD
                key_is_parent = False
                value_is_parent = False
                needs_append = ""
                if self.skip_write_if is not None:
                    swif = self.skip_write_if.replace(
                        "{key}", self._TRUE_KEY_VAR
                    ).replace("{value}", self._TRUE_VALUE_VAR)
                    with writeline(f"if {swif}:"):
                        writeline("continue")
                if self.child_elem_name is not None:
                    writeline(
                        f"{cev} = etree.SubElement({pev}, {self.genXMLTagVar(xidc)})"
                    )
                    pev = self._CHILD_ELEMENT_VAR
                    cev = self._SUBCHILD_ELEMENT_VAR
                    xidc = self._XMLID_SUBCHILD
                else:
                    if (
                        self.key.class_type is not None
                        and self.value.class_type is None
                    ):
                        needs_append = pev
                        writeline(
                            f"{cev} = {self._TRUE_KEY_VAR}.toXML({self.genXMLTagVar(self._XMLID_KEY_ELEM)})"
                        )
                        pev = cev
                        key_is_parent = True
                    elif (
                        self.value.class_type is not None
                        and self.key.class_type is None
                    ):
                        needs_append = pev
                        writeline(
                            f"{cev} = {self._TRUE_VALUE_VAR}.toXML({self.genXMLTagVar(self._XMLID_KEY_ELEM)})"
                        )
                        pev = cev
                        value_is_parent = True
                    elif (
                        self.key.class_type is not None
                        and self.value.class_type is not None
                    ):
                        raise Exception(
                            "You cannot have class set on both key and value without using child-elem!"
                        )
                if not key_is_parent:
                    if self.key.class_type is not None:
                        writeline(
                            f"{pev}.append({self._TRUE_KEY_VAR}.toXML({self.genXMLTagVar(self._XMLID_KEY_ELEM)}))"
                        )
                    else:
                        self.key.toXMLCode(w, pev, self._TRUE_KEY_VAR)
                if not value_is_parent:
                    if self.value.class_type is not None:
                        writeline(
                            f"{pev}.append({self._TRUE_VALUE_VAR}.toXML({self.genXMLTagVar(self._XMLID_VALUE_ELEM)}))"
                        )
                    else:
                        self.value.toXMLCode(w, pev, self._TRUE_VALUE_VAR)
                if needs_append:
                    writeline(f"{needs_append}.append({pev})")

        if self.optional:
            conditions: List[str] = [f"self.{self.cls_attr} is not None"]
            if self.empty_if_missing:
                conditions.append(f"len(self.{self.cls_attr}) > 0")
            conditionstr: str = " and ".join(conditions)
            with writeline(f"if {conditionstr}:"):
                _inner()
        else:
            _inner()
        debug(__file__, self, "toXMLCode", EDebugState.END, locals())

    def toDictCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "toDictCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)

        def _inner() -> None:
            pev = self._PARENT_ELEMENT_VAR
            pev_value = "{}"
            if self.key.class_type is not None:
                pev_value = "[]"
            writeline(f"{self._PARENT_ELEMENT_VAR} = {pev_value}")
            rhs: str = f"self.{self.cls_attr}.items()"
            if self.sort_key is not None:
                rhs = f"sorted({rhs}, key={self.sort_key})"
            with writeline(
                f"for {self._TRUE_KEY_VAR}, {self._TRUE_VALUE_VAR} in {rhs}:"
            ):
                if self.skip_write_if is not None:
                    swif = self.skip_write_if.replace(
                        "{key}", self._TRUE_KEY_VAR
                    ).replace("{value}", self._TRUE_VALUE_VAR)
                    with writeline(f"if {swif}:"):
                        writeline("continue")
                write_as_key = self._SERIALIZED_KEY_VAR
                write_as_value = self._SERIALIZED_VALUE_VAR
                if self.key.class_type is not None:
                    write_as_key = f"{self._TRUE_KEY_VAR}.toDict()"
                else:
                    self.key.toDictCode(w, write_as_key, self._TRUE_KEY_VAR)
                if self.value.class_type is not None:
                    write_as_value = f"{self._TRUE_VALUE_VAR}.toDict()"
                else:
                    self.value.toDictCode(w, write_as_value, self._TRUE_VALUE_VAR)
                if (
                    self.key.class_type is not None
                    and self.value.class_type is not None
                ):
                    raise Exception(
                        "You cannot have class set on both key and value without using child-elem!"
                    )
                if self.key.class_type is not None:
                    writeline(f"{pev}.append([{write_as_key}, {write_as_value}])")
                else:
                    writeline(f"{pev}[{write_as_key}] = {write_as_value}")
            writeline(f"data[{self.getDictKey()!r}] = {pev}")

        if self.optional:
            conditions: List[str] = [f"self.{self.cls_attr} is not None"]
            if self.empty_if_missing:
                conditions.append(f"len(self.{self.cls_attr}) > 0")
            conditionstr: str = " and ".join(conditions)
            with writeline(f"if {conditionstr}:"):
                _inner()
        else:
            _inner()
        debug(__file__, self, "toDictCode", EDebugState.END, locals())

    def fromDictCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "fromDictCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)

        def _inner(var: str) -> None:
            read_key = self._SERIALIZED_KEY_VAR
            read_value = self._SERIALIZED_VALUE_VAR
            suffix = '.items()' if self.key.class_type is None else ''
            with writeline(f"for {read_key}, {read_value} in {var}{suffix}:"):
                if self.key.class_type:
                    writeline(f"{self._TRUE_KEY_VAR} = {self.key.class_type}()")
                    writeline(f"{self._TRUE_KEY_VAR}.fromDict({read_key})")
                    kc = self._TRUE_KEY_VAR
                else:
                    kc = self.key.fromDictCode(w, read_key)
                if self.value.class_type:
                    writeline(f"{self._TRUE_VALUE_VAR} = {self.value.class_type}()")
                    writeline(f"{self._TRUE_VALUE_VAR}.fromDict({read_value})")
                    vc = self._VALUE_SAVABLE_VAR
                else:
                    vc = self.value.fromDictCode(w, read_value)
                writeline(f"self.{self.cls_attr}[{kc}] = {vc}")

        pvar = f"{self._FOUND_ELEMENT_VAR}"
        with writeline(f"if ({pvar} := data.get({self.getDictKey()!r})) is not None:"):
            _inner(pvar)
        if not self.optional:
            with writeline("else:"):
                if self.empty_if_missing:
                    writeline(f"self.{self.cls_attr} = {self.default}")
                else:
                    writeline(
                        f"raise KeyError(self, data, {self.cls_attr!r}, {self.getDictKey()!r})"
                    )
        debug(__file__, self, "fromDictCode", EDebugState.END, locals())


register_child_binding_style(DictChildBinding)

import logging
from typing import Iterable, List, Optional, Union, cast

from analysis.savables.bindings.base import Declarations
from analysis.savables.bindings.base_child import (
    BaseChildBinding,
    EChildBindingStyle,
    register_child_binding_style,
)
from analysis.savables.enums import EAttrType, EEnumStyle
from analysis.utils import copy_ca_to
from buildtools.bt_logging import IndentLogger
from buildtools.indentation import IndentWriter
from ruamel.yaml import CommentedMap
from ruamel.yaml.comments import Comment

from ._eleminfo import _ElemInfo
from .debug import EDebugState, debug

log = IndentLogger(logging.getLogger(__name__))


class ListChildBinding(BaseChildBinding):
    STYLE = EChildBindingStyle.LIST
    LIST_TYPE: str = "List"
    LIST_CTOR: str = "list"
    LIST_APPEND: str = "append"
    TODICT_LIST_TYPE: str = "List"
    TODICT_LIST_CTOR: str = "list"
    TODICT_LIST_APPEND: str = "append"

    _PARENT_ELEMENT_VAR: str = "lp"
    _CHILD_ELEMENT_VAR: str = "lc"
    _FOUND_ELEMENT_VAR: str = "lf"
    _VALUE_SAVABLE_VAR: str = "lv"
    _KEY_ATTRIB_VAR: str = "lk"
    _INDEX_INT_VAR: str = "li"

    _XMLID_IDX: str = "index"
    _XMLID_VALUE_ATTR: str = "valueattr"
    _XMLID_VALUE_ELEM: str = "valueelem"
    _XMLID_PARENT: str = "parent"
    _XMLID_CHILD: str = "child"

    def __init__(self) -> None:
        super().__init__()
        self.type: EAttrType = EAttrType.STR
        self.parent_elem_name: Optional[str] = None
        self.child_elem_name: Optional[str] = None
        self.value: _ElemInfo = _ElemInfo()
        self.optional: bool = False
        self.index_attr_name: Optional[str] = None
        self.empty_if_missing: bool = True
        self.use_attr_ids: bool = False
        self.skip_write_if: Optional[str] = None
        self._ca: Optional[Comment] = None
        self.sort_key: Optional[str] = None

    def addImportsTo(self, sg: "SavableGenerator") -> None:
        typing_imports = [self.LIST_TYPE, "Optional"]
        if self.index_attr_name is not None:
            typing_imports.append("Tuple")
        sg.addImportFrom("typing", typing_imports)
        if self.value.class_type:
            sg.addImportFrom("lilitools.saves.savable", ["Savable"])
        self.value.addImports(sg)

    def deserialize(self, key: str, value: Optional[Union[str, list, dict]]) -> None:
        super().deserialize(key, value)
        assert isinstance(value, dict)
        self.parent_elem_name = value["parent-elem"]
        self.child_elem_name = value["child-elem"]
        self.optional = value.get("optional")
        self.index_attr_name = value.get("index-attr")
        self.use_attr_ids = value.get(
            "use-attr-ids", False
        )  # <durr herp="true" derp="true"/>
        if "empty-if-null" in value:  # old
            self.empty_if_missing = value.get("empty-if-null", True)
        else:
            self.empty_if_missing = value.get("empty-if-missing", True)

        self.value.deserialize(value["value"])
        self.skip_write_if = value.get("skip-write-if")
        self.sort_key = value.get("sort-key", None)
        if self.STYLE in (EChildBindingStyle.LIST, EChildBindingStyle.STACK):
            if self.sort_key is not None:
                print(f"W: [{self.LIST_TYPE}] {key}: sort-key is NOT None")
        else:
            if self.sort_key is None:
                print(f"W: [{self.LIST_TYPE}] {key}: sort-key is None")

        if isinstance(value, CommentedMap):
            self._ca = value.ca

    def serialize(self) -> CommentedMap:
        cm = cast(CommentedMap, super().serialize())
        if self.optional:
            cm["optional"] = True
        cm["parent-elem"] = self.parent_elem_name
        cm["child-elem"] = self.child_elem_name
        if self.index_attr_name is not None:
            cm["index-attr"] = self.index_attr_name
        if self.use_attr_ids:
            cm["use-attr-ids"] = True
        if not self.empty_if_missing:
            cm["empty-if-missing"] = False
        if self.skip_write_if:
            cm["skip-write-if"] = self.skip_write_if
        if self.sort_key is not None:
            cm["sort-key"] = self.sort_key
        cm["value"] = self.value.serialize()
        if self._ca:
            copy_ca_to(self._ca, cm.ca)
        return self.finishSerializing(cm)

    def getAttrType(self) -> str:
        o = ""
        if self.value.class_type:
            o = self.value.class_type
        elif self.value.enum_type:
            o = self.value.enum_type
        else:
            o = self.getPyTypeFrom(self.value.type)
        o = f"{self.LIST_TYPE}[{o}]"
        if self.optional:
            o = f"Optional[{o}]"
        return o

    def getInnerAttrType(self) -> str:
        o = ""
        if self.value.class_type:
            o = self.value.class_type
        elif self.value.enum_type:
            o = self.value.enum_type
        else:
            o = self.getPyTypeFrom(self.value.type)
        return o

    def getAttrDefault(self) -> str:
        return "None" if self.optional and not self.empty_if_missing else self.genCTor()

    def genCTor(self) -> str:
        return f"{self.LIST_CTOR}()"

    def initCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "toDictCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)
        writeline(
            f"self.{self.cls_attr}: {self.getAttrType()} = {self.getAttrDefault()}"
        )
        debug(__file__, self, "toDictCode", EDebugState.END, locals())

    def _todict_forloop_datavar(self, datavar: str) -> str:
        return datavar

    def toDictCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "toDictCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)

        def _forloop(datavar: str, vvar: Optional[str] = None) -> None:
            with writeline(f"for {vvar} in {self._todict_forloop_datavar(datavar)}:"):
                if self.value.class_type is not None:
                    writeline(
                        f"{self._CHILD_ELEMENT_VAR}.{self.TODICT_LIST_APPEND}({vvar}.toDict())"
                    )
                else:
                    rhs = vvar
                    if self.value.enum_type is not None:
                        match self.value.enum_style:
                            case EEnumStyle.NAME:
                                rhs += ".name"
                            case EEnumStyle.VALUE:
                                rhs += ".value"
                    else:
                        match self.value.type:
                            case EAttrType.INT:
                                rhs = f"int({rhs})"
                            case EAttrType.FLOAT:
                                rhs = f"float({rhs})"
                            case EAttrType.STR:
                                rhs = f"str({rhs})"
                            case EAttrType.BOOL:
                                rhs = f"bool({rhs})"
                    writeline(
                        f"{self._CHILD_ELEMENT_VAR}.{self.TODICT_LIST_APPEND}({rhs})"
                    )

        def _write_init_and_store(datavar: str, vvar: Optional[str] = None) -> None:
            writeline(f"{self._CHILD_ELEMENT_VAR} = {self.TODICT_LIST_CTOR}()")
            with writeline(f"if len(self.{self.cls_attr}) > 0:"):
                _forloop(self._CHILD_ELEMENT_VAR, self._VALUE_SAVABLE_VAR)
            writeline(f"data[{self.getDictKey()!r}] = {self._CHILD_ELEMENT_VAR}")

        if self.empty_if_missing:
            with writeline(
                f"if self.{self.cls_attr} is None or len(self.{self.cls_attr}) > 0:"
            ):
                writeline(f"data[{self.getDictKey()!r}] = {self.TODICT_LIST_CTOR}()")
            with writeline(f"else:"):
                _write_init_and_store(self._CHILD_ELEMENT_VAR, self._VALUE_SAVABLE_VAR)
        else:

            if self.optional:
                with writeline(f"if self.{self.cls_attr} is not None:"):
                    _write_init_and_store(
                        self._CHILD_ELEMENT_VAR, self._VALUE_SAVABLE_VAR
                    )
            else:
                _write_init_and_store(self._CHILD_ELEMENT_VAR, self._VALUE_SAVABLE_VAR)
        debug(__file__, self, "toDictCode", EDebugState.END, locals())

    def fromDictCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "fromDictCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)

        def _forloop(datavar: str, vvar: str) -> None:
            with writeline(f"for {vvar} in {datavar}:"):
                if self.value.class_type is not None:
                    writeline(
                        f"self.{self.cls_attr}.{self.LIST_APPEND}({vvar}.toDict())"
                    )
                else:
                    rhs = vvar
                    if self.value.enum_type is not None:
                        match self.value.enum_style:
                            case EEnumStyle.NAME:
                                rhs += ".name"
                            case EEnumStyle.VALUE:
                                rhs += ".value"
                    else:
                        match self.value.type:
                            case EAttrType.INT:
                                rhs = f"int({rhs})"
                            case EAttrType.FLOAT:
                                rhs = f"float({rhs})"
                            case EAttrType.STR:
                                rhs = f"str({rhs})"
                            case EAttrType.BOOL:
                                rhs = f"bool({rhs})"
                    writeline(f"self.{self.cls_attr}.{self.LIST_APPEND}({rhs})")

        with writeline(f"if (lv := self.{self.cls_attr}) is not None:"):
            _forloop("lv", "le")
        if self.optional:
            if self.empty_if_missing:
                with writeline(f"else:"):
                    writeline(f"self.{self.cls_attr} = {self.LIST_CTOR}()")
        else:
            with writeline(f"else:"):
                writeline(
                    f"raise KeyRequiredException(self, data, {self.cls_attr!r}, {self.getDictKey()!r})"
                )
        debug(__file__, self, "fromDictCode", EDebugState.END, locals())

    def declareXMLEntities(self) -> None:
        if self.index_attr_name is not None:
            self.declareAttr(self._XMLID_IDX, self.index_attr_name)
        if self.value.attr is not None:
            self.declareAttr(self._XMLID_VALUE_ATTR, self.value.attr)
        if self.value.elem is not None:
            self.declareTag(self._XMLID_VALUE_ELEM, self.value.elem)
        elif self.value.class_type is not None:
            self.declareTag(self._XMLID_VALUE_ELEM, None)

        if self.parent_elem_name is not None:
            self.declareTag(self._XMLID_PARENT, self.parent_elem_name)
        if self.child_elem_name is not None:
            self.declareTag(self._XMLID_CHILD, self.child_elem_name)

    def declareVarsForFromXML(self, decls: Declarations) -> None:
        if self.parent_elem_name is not None and self.optional:
            decls.declare(self._PARENT_ELEMENT_VAR, "etree._Element")
        if self.value.class_type:
            decls.declare(self._VALUE_SAVABLE_VAR, "Savable")
        if self.index_attr_name is not None:
            decls.declare(self._INDEX_INT_VAR, "int")
        if self.use_attr_ids is not None:
            decls.declare(self._KEY_ATTRIB_VAR, "str")

    def fromXMLCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "fromXMLCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)

        def _inner(evar: str, ovvar: Optional[str] = None):
            if ovvar is None:
                ovvar = str(evar)
            vvar: str = ovvar + ""
            if vvar == evar and self.parent_elem_name is None:
                vvar = self._PARENT_ELEMENT_VAR + ""
            appendTo: str = "self." + self.cls_attr
            if not self.empty_if_missing:
                writeline(f"{appendTo} = {self.LIST_CTOR}()")
            if self.index_attr_name is not None:
                appendTo = f"tmp_{self.cls_attr}"
                writeline(
                    f"{appendTo}: List[Tuple[int, {self.getInnerAttrType()}]] = []"
                )

            def _innerLoopLogic() -> None:
                nonlocal vvar, appendTo
                valcode = ""

                def buildvvar(
                    v: str,
                    noelem: bool = False,
                    noattr: bool = False,
                    notext: bool = False,
                ) -> str:
                    if self.value.elem and not noelem:
                        v = f"{v}.find({self.genXMLTagVar(self._XMLID_VALUE_ELEM)})"
                    if self.value.attr and not noattr:
                        v = f"{v}.attrib[{self.genXMLAttrVar(self._XMLID_VALUE_ATTR)}]"
                    if (
                        self.value.elem is None
                        and self.value.attr is None
                        and self.value.class_type is None
                    ) and not notext:
                        v = f"{v}.text"
                    return v

                # writeline(f'#elem={self.value.elem!r}, attr={self.value.attr!r}')
                if self.value.class_type:
                    writeline(f"{self._VALUE_SAVABLE_VAR} = {self.value.class_type}()")
                    writeline(
                        f"{self._VALUE_SAVABLE_VAR}.fromXML({buildvvar(vvar, noattr=True, notext=True)})"
                    )
                    valcode = self._VALUE_SAVABLE_VAR
                    if self.index_attr_name is not None:
                        writeline(
                            f"{self._INDEX_INT_VAR} = int({vvar}.attrib[{self.genXMLAttrVar(self._XMLID_IDX)}])"
                        )
                        valcode = f"({self._INDEX_INT_VAR}, {self._VALUE_SAVABLE_VAR})"
                elif self.value.enum_type is not None:
                    valcode = f"{buildvvar(vvar)}"
                    if self.value.enum_style == EEnumStyle.NAME:
                        valcode = f"{self.value.enum_type}[{valcode}]"
                    elif self.value.enum_style == EEnumStyle.VALUE:
                        valcode = self.value.genStr2TypeConverter(valcode)
                        valcode = f"{self.value.enum_type}({valcode})"
                    if self.index_attr_name is not None:
                        writeline(
                            f"{self._INDEX_INT_VAR} = int({vvar}.attrib[{self.genXMLAttrVar(self._XMLID_IDX)}])"
                        )
                        valcode = f"({self._INDEX_INT_VAR}, {self._VALUE_SAVABLE_VAR})"
                else:
                    valcode = self.value.genStr2TypeConverter(buildvvar(vvar))
                    if self.index_attr_name is not None:
                        writeline(
                            f"{self._INDEX_INT_VAR} = int({vvar}.attrib[{self.genXMLAttrVar(self._XMLID_IDX)}])"
                        )
                        valcode = f"({self._INDEX_INT_VAR}, {valcode})"
                writeline(f"{appendTo}.{self.LIST_APPEND}({valcode})")

            if self.use_attr_ids:
                kvar = self._KEY_ATTRIB_VAR
                with writeline(f"for {kvar} in {evar}.attrib.keys():"):
                    val = f"{kvar}"
                    if self.value.enum_type is not None:
                        if self.value.enum_style == EEnumStyle.NAME:
                            val = f"{self.value.enum_type}[{val}]"
                        elif self.value.enum_style == EEnumStyle.VALUE:
                            val = f"{self.value.enum_type}({val})"
                    writeline(f"{appendTo}.{self.LIST_APPEND}({val})")
            else:
                vvar = self._CHILD_ELEMENT_VAR
                if evar != "e":
                    if ".find" in evar:
                        writeline(
                            f"if ({self._PARENT_ELEMENT_VAR} := {evar}) is not None:"
                        )
                        evar = self._PARENT_ELEMENT_VAR
                    else:
                        writeline(f"if {evar} is not None:")
                    with w:
                        if self.child_elem_name:
                            # with writeline(f'for {vvar} in {evar}.findall({self.genXMLTagVar(self._XMLID_CHILD)}):'):
                            with writeline(
                                f"for {vvar} in {evar}.iterfind({self.genXMLTagVar(self._XMLID_CHILD)}):"
                            ):
                                _innerLoopLogic()
                        else:
                            with writeline(f"for {vvar} in {evar}:"):
                                _innerLoopLogic()
                else:
                    if self.child_elem_name:
                        # with writeline(f'for {vvar} in {evar}.findall({self.genXMLTagVar(self._XMLID_CHILD)}):'):
                        with writeline(
                            f"for {vvar} in {evar}.iterfind({self.genXMLTagVar(self._XMLID_CHILD)}):"
                        ):
                            _innerLoopLogic()
                    else:
                        with writeline(f"for {vvar} in {evar}:"):
                            _innerLoopLogic()
            if self.index_attr_name is not None:
                writeline(f"self.{self.cls_attr} = [None] * len({appendTo})")
                with writeline(
                    f"for {self._INDEX_INT_VAR}, {self._VALUE_SAVABLE_VAR} in {appendTo}:"
                ):
                    writeline(
                        f"self.{self.cls_attr}[{self._INDEX_INT_VAR}] = {self._VALUE_SAVABLE_VAR}"
                    )
                # writeline(f'{appendTo} = None')

        if self.parent_elem_name is None:
            _inner("e")
        else:
            with writeline(
                f"if ({self._FOUND_ELEMENT_VAR} := e.find({self.genXMLTagVar(self._XMLID_PARENT)})) is not None:"
            ):
                _inner(self._FOUND_ELEMENT_VAR)
            if not self.optional:
                with writeline(f"else:"):
                    if self.empty_if_missing:
                        writeline(f"self.{self.cls_attr} = {self.LIST_CTOR}()")
                    else:
                        writeline(
                            f"raise ElementRequiredException(self, e, {self.cls_attr!r}, {self.genXMLTagVar(self._XMLID_PARENT)})"
                        )
        debug(__file__, self, "fromXMLCode", EDebugState.END, locals())

    def declareVarsForToXML(self, decls: Declarations) -> None:
        decls.declare(self._PARENT_ELEMENT_VAR, "etree._Element")
        if self.index_attr_name is not None:
            decls.declare(self._INDEX_INT_VAR, "int")
        if not self.use_attr_ids and self.value.needsChildElem():
            decls.declare(self._CHILD_ELEMENT_VAR, "etree._Element")
        return super().declareVarsForToXML(decls)

    def toXMLCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "toXMLCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)
        if self.optional:
            conditions: List[str] = [f"self.{self.cls_attr} is not None"]
            if self.empty_if_missing:
                conditions.append(f"len(self.{self.cls_attr}) > 0")
            conditionstr: str = " and ".join(conditions)
            with writeline(f"if {conditionstr}:"):
                self._toXML_middle(w, "e")
        else:
            self._toXML_middle(w, "e")
        debug(__file__, self, "toXMLCode", EDebugState.END, locals())

    def _toXML_middle(self, w: IndentWriter, evar: str) -> None:
        debug(__file__, self, "_toXML_middle", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)
        pev = evar
        if self.parent_elem_name is not None:
            writeline(
                f"{self._PARENT_ELEMENT_VAR} = etree.SubElement({evar}, {self.genXMLTagVar(self._XMLID_PARENT)})"
            )
            pev = self._PARENT_ELEMENT_VAR
        if self.use_attr_ids:
            self._toXML_writeAttrListTo(w, pev)
        else:
            self._toXML_writeLoop(w, pev)
        debug(__file__, self, "_toXML_middle", EDebugState.END, locals())

    def _toXML_writeAttrListTo(self, w: IndentWriter, evar: str) -> None:
        debug(__file__, self, "_toXML_writeAttrListTo", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)
        # Elem and Attr are now invalid.
        assert (
            self.value.attr is None and self.value.elem is None
        ), "value.elem and value.attr must both be null."
        with writeline(f"for {self._VALUE_SAVABLE_VAR} in self.{self.cls_attr}:"):
            writeline(
                f"{evar}.attrib[{self.value.genType2StrConverter(self._VALUE_SAVABLE_VAR)}] = 'true'"
            )
        debug(__file__, self, "_toXML_writeAttrListTo", EDebugState.END, locals())

    def _toXML_writeLoop(self, w: IndentWriter, evar: str) -> None:
        debug(__file__, self, "_toXML_writeLoop", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)
        rhs: str = f"self.{self.cls_attr}"
        if self.sort_key is not None:
            rhs = f"sorted({rhs}, key={self.sort_key})"
        if self.index_attr_name is not None:
            loop = writeline(
                f"for {self._INDEX_INT_VAR}, {self._VALUE_SAVABLE_VAR} in enumerate({rhs}):"
            )
        else:
            loop = writeline(f"for {self._VALUE_SAVABLE_VAR} in {rhs}:")
        with loop:
            if self.skip_write_if is not None:
                swif = self.skip_write_if.replace(
                    "{value}", self._VALUE_SAVABLE_VAR
                ).replace("{index}", self._INDEX_INT_VAR)
                with writeline(f"if {swif}:"):
                    writeline("continue")
            self._toXML_writeValueElements(w, evar)
        debug(__file__, self, "_toXML_writeLoop", EDebugState.END, locals())

    def _toXML_writeValueElements(self, w: IndentWriter, evar: str) -> None:
        debug(__file__, self, "_toXML_writeValueElements", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)
        # Scalars
        if self.value.class_type is None:
            # child-elem: yes
            # value.elem: yes
            if self.child_elem_name is not None and self.value.elem is not None:
                # e1 = etree.SubElement(e, '')
                # etree.SubElement(e1, ...)
                writeline(
                    f"{self._CHILD_ELEMENT_VAR} = etree.SubElement({evar}, {self.genXMLTagVar(self._XMLID_CHILD)})"
                )
                self._toXML_writeElement(
                    w, evar, self.genXMLTagVar(self._XMLID_VALUE_ELEM)
                )
            # child-elem: no
            # value.elem: yes
            elif self.child_elem_name is None and self.value.elem is not None:
                # etree.SubElement(e, ...)
                self._toXML_writeElement(
                    w, evar, self.genXMLTagVar(self._XMLID_VALUE_ELEM)
                )
            # child-elem: yes
            # value.elem: no
            elif self.child_elem_name is not None and self.value.elem is None:
                # etree.SubElement(e, ...)
                self._toXML_writeElement(w, evar, self.genXMLTagVar(self._XMLID_CHILD))
            # child-elem: no
            # value.elem: no
            else:
                raise Exception("child-elem or value.elem are required!")
        # Classes
        else:
            if self.value.elem is None:
                self._toXML_writeNestedTypeAppend(w, evar, None)
            else:
                self._toXML_writeNestedTypeAppend(
                    w, evar, self.genXMLTagVar(self._XMLID_VALUE_ELEM)
                )
        debug(__file__, self, "_toXML_writeValueElements", EDebugState.END, locals())

    def _toXML_writeNestedTypeAppend(
        self, w: IndentWriter, evar: str, tag: Optional[str]
    ) -> None:
        debug(
            __file__, self, "_toXML_writeNestedTypeAppend", EDebugState.START, locals()
        )
        writeline = self.buildWriteLine(w)
        if tag is None:
            tag = ""
        if self.index_attr_name is not None:
            writeline(f"le = {self._VALUE_SAVABLE_VAR}.toXML({tag})")
            writeline(
                f"le[{self.genXMLAttrVar(self._XMLID_IDX)}] = str({self._INDEX_INT_VAR})"
            )
            writeline(f"{evar}.append(le)")
        else:
            writeline(f"{evar}.append({self._VALUE_SAVABLE_VAR}.toXML({tag}))")
        debug(__file__, self, "_toXML_writeNestedTypeAppend", EDebugState.END, locals())

    def _toXML_writeElement(self, w: IndentWriter, evar: str, tagvar: str) -> None:
        debug(__file__, self, "_toXML_writeElement", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)
        # attr is set but not elem
        if self.value.attr is not None:
            # <child attr='value'/>
            attrs = "{"
            if self.index_attr_name is not None:
                attrs += (
                    f"{self.genXMLAttrVar(self._XMLID_IDX)}: str({self._INDEX_INT_VAR})"
                )
            attrs += f"{self.genXMLAttrVar(self._XMLID_VALUE_ATTR)}: {self.value.genType2StrConverter(self._VALUE_SAVABLE_VAR)}"
            attrs += "}"
            writeline(f"etree.SubElement({evar}, {tagvar}, {attrs})")
        # attr is not set and neither is elem
        else:
            # <child>value</child>
            attrs = "{"
            if self.index_attr_name is not None:
                attrs += (
                    f"{self.genXMLAttrVar(self._XMLID_IDX)}: str({self._INDEX_INT_VAR})"
                )
            attrs += "}"
            writeline(
                f"etree.SubElement({evar}, {tagvar}, {attrs}).text = {self.value.genType2StrConverter(self._VALUE_SAVABLE_VAR)}"
            )
        debug(__file__, self, "_toXML_writeElement", EDebugState.END, locals())

    def registerAttrs(self) -> Iterable[str]:
        attrs = set()
        if self.index_attr_name:
            attrs.add(self.index_attr_name)
        if self.value.attr:
            attrs.add(self.value.attr)
        return attrs

    def __writeIndexedListItem(
        self, w: IndentWriter, tgtElem: str, valvar: str
    ) -> None:
        debug(__file__, self, "__writeIndexedListItem", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)
        pElem = tgtElem
        needs_append: bool
        if self.value.class_type is not None:
            needs_append = True
            writeline(
                f"{tgtElem} = {valvar}.toXML({self.genXMLTagVar(self._XMLID_VALUE_ELEM)}))"
            )
        else:
            needs_append = False
            assert self.value.class_type is None
            if self.value.enum_style == EEnumStyle.NAME:
                valvar = f"{valvar}.name"
            elif self.value.enum_style == EEnumStyle.VALUE:
                valvar = f"str({valvar}.value)"
            elif self.value.type == EAttrType.BOOL:
                valvar = f"str({valvar}).lower()"
            elif self.value.type != EAttrType.STR:
                valvar = f"str({valvar})"

            if self.value.attr and not self.value.elem:
                writeline(f"{tgtElem}.attrib[{self.value.attr!r}] = {valvar}")
            elif not self.value.attr and self.value.elem:
                writeline(
                    f"o = etree.SubElement({tgtElem}, {self.value.elem!r}).text = {valvar}"
                )
                pElem = tgtElem
                tgtElem = "o"
            elif self.value.attr and self.value.elem:
                writeline(
                    f"o = etree.SubElement({tgtElem}, {self.value.elem!r}, {{{self.value.attr!r}: {valvar}}})"
                )
                pElem = tgtElem
                tgtElem = "o"
            elif not self.value.attr and not self.value.elem:
                writeline(f"{tgtElem}.text = {valvar}")
        writeline(
            f"{tgtElem}.attrib[{self.genXMLAttrVar(self._XMLID_IDX)}] = str({self._INDEX_INT_VAR})"
        )
        if needs_append:
            writeline(f"{pElem}.append({tgtElem})")
        debug(__file__, self, "__writeIndexedListItem", EDebugState.END, locals())


register_child_binding_style(ListChildBinding)

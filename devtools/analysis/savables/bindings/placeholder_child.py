import logging
from typing import Dict, Optional, Set, Union, cast

from analysis.savables.bindings.base_child import (
    BaseChildBinding,
    EChildBindingStyle,
    register_child_binding_style,
)
from analysis.utils import copy_ca_to
from buildtools.bt_logging import IndentLogger
from buildtools.indentation import IndentWriter
from ruamel.yaml import CommentedMap, CommentedSeq
from ruamel.yaml.comments import Comment

from .debug import EDebugState, debug

log = IndentLogger(logging.getLogger(__name__))


class PlaceholderChildBinding(BaseChildBinding):
    STYLE = EChildBindingStyle.PLACEHOLDER

    def __init__(self) -> None:
        super().__init__()
        self.type: str = ""
        self.imports: Dict[str, Set[str]] = {}
        self.default: str = ""
        self._ca: Optional[Comment] = None

    def addImportsTo(self, sg: "SavableGenerator") -> None:
        sg.addImportFrom("lxml", ["etree"])
        for i in self.imports:
            if isinstance(i, dict):
                k, v = next(iter(i.items()))
                sg.addImportFrom(k, v)
            else:
                sg.addImport(i)

    def deserialize(self, key: str, value: Optional[Union[str, list, dict]]) -> None:
        super().deserialize(key, value)
        assert isinstance(value, dict)
        self.type = value.get("type", "str")
        self.default = value.get("default", "")
        self.imports = value.get("imports", {})

        if isinstance(value, CommentedMap):
            self._ca = value.ca

        if isinstance(self.imports, CommentedMap):
            self.imports = {k: v for k, v in self.imports.items()}
        elif isinstance(self.imports, CommentedSeq):
            self.imports = {k: v for k, v in self.imports}

    def serialize(self) -> CommentedMap:
        cm = cast(CommentedMap, super().serialize())
        cm["type"] = self.type
        cm["default"] = self.default
        cm["imports"] = self.imports
        if self._ca is not None:
            copy_ca_to(self._ca, cm.ca)
        return self.finishSerializing(cm)

    def getAttrType(self) -> str:
        return self.type

    def getAttrDefault(self) -> str:
        return self.default

    def initCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "initCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)
        writeline(
            f"self.{self.cls_attr}: {self.getAttrType()} = {self.getAttrDefault()}"
        )
        debug(__file__, self, "initCode", EDebugState.END, locals())

    def fromXMLCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "fromXMLCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)
        writeline(f"self._fromXML_{self.cls_attr}(e)")
        debug(__file__, self, "fromXMLCode", EDebugState.END, locals())

    def toXMLCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "toXMLCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)
        writeline(f"self._toXML_{self.cls_attr}(e)")
        debug(__file__, self, "toXMLCode", EDebugState.END, locals())

    def fromDictCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "fromDictCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)
        writeline(f"self._fromDict_{self.cls_attr}(data)")
        debug(__file__, self, "fromDictCode", EDebugState.END, locals())

    def toDictCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "toDictCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)
        writeline(f"self._toDict_{self.cls_attr}(data)")
        debug(__file__, self, "toDictCode", EDebugState.END, locals())

    def arbitraryMethodCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "arbitraryMethodCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)
        with writeline(
            f"def _fromXML_{self.cls_attr}(self, e: etree._Element) -> None:"
        ):
            writeline(
                f"raise NotImplementedError('Not implemented: '+self.__class__.__name__+'._fromXML_{self.cls_attr}()')"
            )
        with writeline(f"def _toXML_{self.cls_attr}(self, e: etree._Element) -> None:"):
            writeline(
                f"raise NotImplementedError('Not implemented: '+self.__class__.__name__+'._toXML_{self.cls_attr}()')"
            )
        with writeline(
            f"def _fromDict_{self.cls_attr}(self, data: Dict[str, Any]) -> None:"
        ):
            writeline(
                f"raise NotImplementedError('Not implemented: '+self.__class__.__name__+'._fromDict_{self.cls_attr}()')"
            )
        with writeline(
            f"def _toDict_{self.cls_attr}(self, data: Dict[str, Any]) -> None:"
        ):
            writeline(
                f"raise NotImplementedError('Not implemented: '+self.__class__.__name__+'._toDict_{self.cls_attr}()')"
            )
        debug(__file__, self, "arbitraryMethodCode", EDebugState.END, locals())

    def addImportsTo(self, sg: "SavableGenerator") -> None:
        # print('a', repr(self.imports))
        for k, v in self.imports.items():
            sg.addImportFrom(k, v)


register_child_binding_style(PlaceholderChildBinding)

import logging
from typing import Any, Iterable, Optional, Union, cast

from analysis.savables.bindings.base import Declarations
from analysis.savables.enums import EAttrType, EEnumStyle
from analysis.utils import copy_ca_to
from buildtools.bt_logging import IndentLogger
from buildtools.indentation import IndentWriter
from ruamel.yaml import CommentedMap
from ruamel.yaml.comments import Comment

from .base_child import (
    BaseChildBinding,
    EChildBindingStyle,
    register_child_binding_style,
)
from .debug import EDebugState, debug

log = IndentLogger(logging.getLogger(__name__))


class ScalarChildBinding(BaseChildBinding):
    STYLE = EChildBindingStyle.SCALAR

    _XMLID_ELEMENT: str = "element"
    _XMLID_SUBELEMENT: str = "subelement"
    _XMLID_ATTRIBUTE: str = "attribute"

    _VAR_ELEMENT: str = "se"
    _VAR_FOUND: str = "sf"

    def __init__(self) -> None:
        super().__init__()
        self.type: EAttrType = EAttrType.STR
        self.default: Optional[Any] = None
        self.value_attr: Optional[str] = "value"
        self.subelement: Optional[str] = None
        self.null_value: Optional[str] = None
        self.optional: bool = False
        self.class_type: Optional[str] = None
        self.class_package: Optional[str] = None
        self.enum_type: Optional[str] = None
        self.enum_package: Optional[str] = None
        self.enum_style: Optional[EEnumStyle] = None

        self._ca: Optional[Comment] = None

    def deserialize(self, key: str, value: Optional[Union[str, list, dict]]) -> None:
        super().deserialize(key, value)
        if value is None:
            self.xml_element = self.cls_attr
        elif isinstance(value, str):
            self.type = EAttrType[value.upper()]
            self.xml_element = self.cls_attr
        elif isinstance(value, list):
            self.xml_element = value[0]
            self.type = EAttrType[value[1].upper()]
        else:
            self.xml_element = value.get("tag", self.cls_attr)
            self.value_attr = value.get("attr", self.value_attr)
            self.subelement = value.get("subelement", None)
            self.default = value.get("default", None)
            self.null_value = value.get("null-value", None)
            self.optional = value.get("optional", False)
            vt = value.get("type", "STR").upper()
            self.type = EAttrType[vt]
            if (clsdata := value.get("class")) is not None:
                self.class_package = clsdata["package"]
                self.class_type = clsdata["type"]
            if (enumdata := value.get("enum")) is not None:
                self.enum_package = enumdata["package"]
                self.enum_type = enumdata["type"]
                self.enum_style = EEnumStyle[enumdata["style"].upper()]

            if isinstance(value, CommentedMap):
                self._ca = value.ca
        # if self.null_value is not None and not self.optional:
        #     sel: str = self.xml_element
        #     if self.value_attr is not None:
        #         sel += f'[{self.value_attr}]'
        #     raise SavableConfigurationError(f'{sel}: Cannot use null-value without optional')
        # print(repr({
        #     'type': self.type,
        #     'default': self.default,
        #     'value_attr': self.value_attr,
        #     'class_type': self.class_type,
        #     'class_package': self.class_package,
        #     'enum_style': self.enum_style,
        #     'enum_type': self.enum_type,
        #     'enum_package': self.enum_package,
        # }))

    def serialize(self) -> CommentedMap:
        cm = cast(CommentedMap, super().serialize())
        dict_reqd = False
        if self.enum_package is None and self.class_type is None:
            cm["type"] = self.type.name.lower()
        if self.xml_element != self.cls_attr:
            cm["tag"] = self.xml_element
            dict_reqd = True
        if self.value_attr != "value":
            cm["attr"] = self.value_attr
            dict_reqd = True
        if self.optional:
            cm["optional"] = True
            dict_reqd = True
        if self.subelement is not None:
            cm["subelement"] = self.subelement
            dict_reqd = True
        if self.default is not None:
            cm["default"] = self.default
            dict_reqd = True
        if self.null_value is not None:
            cm["null-value"] = self.null_value
            dict_reqd = True
        if self.class_package is not None:
            cm["class"] = self.serialize_class()
            dict_reqd = True
        if self.enum_package is not None:
            cm["enum"] = self.serialize_enum()
            dict_reqd = True
        if dict_reqd:
            if self._ca:
                copy_ca_to(self._ca, cm.ca)
            return self.finishSerializing(cm)
        else:
            return self.finishSerializing(self.type.name.lower())

    def serialize_class(self) -> CommentedMap:
        cm = CommentedMap()
        cm["package"] = self.class_package
        cm["type"] = self.class_type
        return cm

    def serialize_enum(self) -> CommentedMap:
        cm = CommentedMap()
        cm["style"] = self.enum_style.name
        cm["package"] = self.enum_package
        cm["type"] = self.enum_type
        return cm

    def addImportsTo(self, sg: "SavableGenerator") -> None:
        super().addImportsTo(sg)
        if not self.optional:
            sg.addImportFrom(
                "lilitools.saves.exceptions",
                [
                    "ElementRequiredException",
                    "AttributeRequiredException",
                    "KeyRequiredException",
                ],
            )
        if self.class_type:
            sg.addImportFrom(self.class_package, [self.class_type])
        if self.enum_type:
            sg.addImportFrom(self.enum_package, [self.enum_type])
        if self.type == EAttrType.BOOL:
            sg.addImportFrom("lilitools.saves.savable", ["XML2BOOL"])

    def getAttrType(self) -> str:
        o = ""
        if self.class_type:
            o = self.class_type
        elif self.enum_type:
            o = self.enum_type
        else:
            o = self.getPyTypeFrom(self.type)

        if (self.optional and self.default is None) or self.null_value is not None:
            o = f"Optional[{o}]"
        return o

    def getAttrDefault(self) -> str:
        if self.class_type is not None and not self.optional:
            return f"{self.class_type}()"
        if self.enum_type is not None and not self.optional:
            if self.default:
                return f"{self.enum_type}.{self.default}"
            return f"{self.enum_type}(0)"
        if (self.optional and self.default is None) or self.null_value is not None:
            return "None"
        if self.default is not None:
            return repr(self.default)
        return self.getPyDefaultFrom(self.type)

    def initCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "initCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)
        writeline(
            f"self.{self.cls_attr}: {self.getAttrType()} = {self.getAttrDefault()} # Element"
        )
        debug(__file__, self, "initCode", EDebugState.END, locals())

    def registerTags(self) -> Iterable[str]:
        return {self.xml_element}

    def toDictCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "toDictCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)

        def _inner(val_override: Optional[str] = None) -> None:
            if self.class_type is not None:
                writeline(f"data[{self.getDictKey()!r}] = self.{self.cls_attr}.toDict()")
                return
            rhs = f'self.{self.cls_attr}'
            if val_override is not None:
                rhs = repr(val_override)
            else:
                if self.enum_type is not None:
                    match self.enum_style:
                        case EEnumStyle.NAME:
                            rhs = f"{rhs}.name"
                        case EEnumStyle.VALUE:
                            rhs = f"{rhs}.value"
                else:
                    match self.type:
                        case EAttrType.INT:
                            rhs = f"int({rhs})"
                        case EAttrType.FLOAT:
                            rhs = f"float({rhs})"
                        case EAttrType.STR:
                            rhs = f"str({rhs})"
                        case EAttrType.BOOL:
                            rhs = f"bool({rhs})"

            writeline(f"data[{self.getDictKey()!r}] = {rhs}")

        if self.optional:
            terms = [f"self.{self.cls_attr} is not None"]
            if self.default:
                terms.append(f"self.{self.cls_attr} != {self.getAttrDefault()}")
            cond = " and ".join(terms)
            with writeline(f"if {cond}:"):
                _inner()
        else:
            _inner()
        debug(__file__, self, "toDictCode", EDebugState.END, locals())

    def fromDictCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "fromDictCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)

        def _inner() -> None:
            if self.class_type is not None:
                writeline(f"self.{self.cls_attr} = {self.class_type}()")
                writeline(f"self.{self.cls_attr}.fromDict(data[{self.getDictKey()!r}])")
            else:
                rhs = f"data[{self.getDictKey()!r}]"
                if self.enum_type is not None:
                    match self.enum_style:
                        case EEnumStyle.NAME:
                            rhs += ".name"
                        case EEnumStyle.VALUE:
                            rhs += ".value"
                else:
                    match self.enum_type:
                        case EAttrType.BOOL:
                            rhs = f"bool({rhs})"
                        case EAttrType.FLOAT:
                            rhs = f"float({rhs})"
                        case EAttrType.INT:
                            rhs = f"int({rhs})"
                        case EAttrType.STR:
                            rhs = f"str({rhs})"
                writeline(f"self.{self.cls_attr} = {rhs}")

        with writeline(
            f"if ({self._VAR_FOUND}:=data.get({self.getDictKey()!r})) is not None:"
        ):
            _inner()
        with writeline(f"else:"):
            if not self.optional:
                writeline(
                    f"raise KeyRequiredException(self, data, {self.cls_attr!r}, {self.getDictKey()!r})"
                )
            else:
                writeline(f"self.{self.cls_attr} = {self.getAttrDefault()}")
        debug(__file__, self, "fromDictCode", EDebugState.END, locals())

    def toXMLCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "toXMLCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)
        if self.optional:
            ifstmt = None
            if self.default is not None:
                ifstmt = writeline(
                    f"if self.{self.cls_attr} is not None and self.{self.cls_attr} != {self.getAttrDefault()}:"
                )
            else:
                ifstmt = writeline(f"if self.{self.cls_attr} is not None:")
            with ifstmt:
                self.__inner_toXMLCode(w)
        else:
            if self.null_value is not None:
                with writeline(f"if self.{self.cls_attr} is not None:"):
                    self.__inner_toXMLCode(w)
                with writeline(f"else:"):
                    self.__inner_toXMLCode(w, self.null_value)
            else:
                self.__inner_toXMLCode(w)
        debug(__file__, self, "toXMLCode", EDebugState.END, locals())

    def declareXMLEntities(self) -> None:
        self.declareTag(self._XMLID_ELEMENT, self.xml_element)
        if self.subelement:
            self.declareTag(self._XMLID_SUBELEMENT, self.subelement)
        if self.value_attr:
            self.declareAttr(self._XMLID_ATTRIBUTE, self.value_attr)

    def declareVarsForFromXML(self, decls: Declarations) -> None:
        decls.declare(self._VAR_FOUND, "etree._Element")
        if self.subelement:
            decls.declare(self._VAR_ELEMENT, "etree._Element")

    def __inner_toXMLCode(
        self, w: IndentWriter, val_override: Optional[str] = None
    ) -> None:
        debug(__file__, self, "__inner_toXMLCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)
        var_e = "e"
        toxml_tag = self.genXMLTagVar(self._XMLID_ELEMENT)
        if self.subelement:
            writeline(f"{self._VAR_ELEMENT} = etree.SubElement(e, {toxml_tag})")
            var_e = self._VAR_ELEMENT
            toxml_tag = self.genXMLTagVar(self._XMLID_SUBELEMENT)
        if self.class_type is not None:
            writeline(f"{var_e}.append(self.{self.cls_attr}.toXML({toxml_tag}))")
            return

        rhs = ""
        if val_override is not None:
            rhs = repr(val_override)
        else:
            if self.enum_type is not None:
                if self.enum_style == EEnumStyle.NAME:
                    rhs = f"self.{self.cls_attr}.name"
                elif self.enum_style == EEnumStyle.VALUE:
                    rhs = f"str(self.{self.cls_attr}.value)"
            else:
                rhs += f"str(self.{self.cls_attr})"
        if val_override is None and self.type == EAttrType.BOOL:
            rhs += f".lower()"
        if self.subelement:
            if self.value_attr is None:
                writeline(
                    f"etree.SubElement({var_e}, {self.genXMLTagVar(self._XMLID_SUBELEMENT)}).text = {rhs}"
                )
            else:
                writeline(
                    f"etree.SubElement({var_e}, {self.genXMLTagVar(self._XMLID_SUBELEMENT)}, {{{self.genXMLAttrVar(self._XMLID_ATTRIBUTE)}: {rhs}}})"
                )
        else:
            if self.value_attr is None:
                writeline(
                    f"etree.SubElement(e, {self.genXMLTagVar(self._XMLID_ELEMENT)}).text = {rhs}"
                )
            else:
                writeline(
                    f"etree.SubElement(e, {self.genXMLTagVar(self._XMLID_ELEMENT)}, {{{self.genXMLAttrVar(self._XMLID_ATTRIBUTE)}: {rhs}}})"
                )
        debug(__file__, self, "__inner_toXMLCode", EDebugState.END, locals())

    def fromXMLCode(self, w: IndentWriter) -> None:
        debug(__file__, self, "fromXMLCode", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)
        if self.subelement:
            with writeline(
                f"if ({self._VAR_ELEMENT} := e.find({self.genXMLTagVar(self._XMLID_ELEMENT)})) is not None:"
            ):
                self.__fromXMLCode_inner(
                    w,
                    elvar=self._VAR_ELEMENT,
                    tagvar=self.genXMLTagVar(self._XMLID_SUBELEMENT),
                )
        else:
            self.__fromXMLCode_inner(
                w, elvar="e", tagvar=self.genXMLTagVar(self._XMLID_ELEMENT)
            )
        debug(__file__, self, "fromXMLCode", EDebugState.END, locals())

    def __fromXMLCode_inner(self, w: IndentWriter, elvar: str, tagvar: str) -> None:
        debug(__file__, self, "__fromXMLCode_inner", EDebugState.START, locals())
        writeline = self.buildWriteLine(w)
        with writeline(
            f"if ({self._VAR_FOUND} := {elvar}.find({tagvar})) is not None:"
        ):
            rhs = self._VAR_FOUND
            if self.class_type:
                writeline(f"self.{self.cls_attr} = {self.class_type}()")
                writeline(f"self.{self.cls_attr}.fromXML({rhs})")
            else:
                if self.value_attr:
                    if not self.optional:
                        with writeline(
                            f"if {self.genXMLAttrVar(self._XMLID_ATTRIBUTE)} not in {self._VAR_FOUND}.attrib:"
                        ):
                            writeline(
                                f"raise AttributeRequiredException(self, {self._VAR_FOUND}, {self.genXMLAttrVar(self._XMLID_ATTRIBUTE)}, {self.cls_attr!r})"
                            )
                        rhs += f".attrib[{self.genXMLAttrVar(self._XMLID_ATTRIBUTE)}]"
                    else:
                        dstr = self.getAttrDefault()
                        if self.type == EAttrType.BOOL:
                            dstr = repr(dstr.lower())
                        elif self.type != EAttrType.STR:
                            dstr = f'"{dstr}"'
                        rhs += f".attrib.get({self.genXMLAttrVar(self._XMLID_ATTRIBUTE)}, {dstr})"
                else:
                    rhs += f".text"
                rawrhs = rhs
                if self.type == EAttrType.BOOL:
                    # rhs = f'str({rhs}).lower()'
                    rhs = f"XML2BOOL[{rhs}]"
                elif self.type != EAttrType.STR:
                    rhs = f"{self.getPyTypeFrom(self.type)}({rhs})"
                if self.enum_type:
                    if self.enum_style == EEnumStyle.NAME:
                        rhs = f"{self.enum_type}[{rhs}]"
                    elif self.enum_style == EEnumStyle.VALUE:
                        rhs = f"{self.enum_type}({rhs})"
                if self.null_value is not None:
                    with writeline(f"if {rawrhs} == {self.null_value!r}:"):
                        writeline(f"self.{self.cls_attr} = None")
                    with writeline("else:"):
                        writeline(f"self.{self.cls_attr} = {rhs}")
                else:
                    writeline(f"self.{self.cls_attr} = {rhs}")
        if not self.optional or self.default is not None:
            with writeline("else:"):
                if not self.optional:
                    writeline(
                        f"raise ElementRequiredException(self, {elvar}, {self.cls_attr!r}, {tagvar})"
                    )
                else:
                    writeline(f"self.{self.cls_attr} = {self.getAttrDefault()}")
        debug(__file__, self, "__fromXMLCode_inner", EDebugState.END, locals())


register_child_binding_style(ScalarChildBinding)

import logging

from analysis.savables.bindings.base_child import (
    BaseChildBinding, EChildBindingStyle, register_child_binding_style)
from buildtools.bt_logging import IndentLogger

from analysis.savables.enums import EAttrType, EEnumStyle

from .list_child import ListChildBinding

log = IndentLogger(logging.getLogger(__name__))


class SetChildBinding(ListChildBinding):
    STYLE = EChildBindingStyle.SET
    LIST_TYPE = 'Set'
    LIST_CTOR = 'set'
    LIST_APPEND = 'add'
    def _todict_forloop_datavar(self, datavar: str) -> str:
        if self.value.class_type is not None:
            return datavar
        rhs:str='x'
        if self.value.enum_type is not None:
            match self.value.enum_style:
                case EEnumStyle.NAME:
                    rhs += ".name"
                case EEnumStyle.VALUE:
                    rhs += ".value"
        else:
            match self.value.type:
                case EAttrType.INT:
                    rhs = f"int({rhs})"
                case EAttrType.FLOAT:
                    rhs = f"float({rhs})"
                case EAttrType.STR:
                    rhs = f"str({rhs})"
                case EAttrType.BOOL:
                    rhs = f"bool({rhs})"
        return f'sorted({datavar}, lambda x: {rhs})'


register_child_binding_style(SetChildBinding)

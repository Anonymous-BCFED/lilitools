import logging
from typing import Optional, Union
from ruamel.yaml import CommentedMap
from analysis.savables.bindings.base_child import (
    EChildBindingStyle,
    register_child_binding_style,
)
from buildtools.bt_logging import IndentLogger

from .list_child import ListChildBinding

log = IndentLogger(logging.getLogger(__name__))


class StackChildBinding(ListChildBinding):
    STYLE = EChildBindingStyle.STACK
    LIST_TYPE = "Deque"
    LIST_CTOR = "deque"
    LIST_APPEND = "append"

    def __init__(self) -> None:
        super().__init__()
        self.size: int = 0

    def deserialize(self, key: str, value: Optional[Union[str, list, dict]]) -> None:
        super().deserialize(key, value)
        if isinstance(value, dict):
            data: dict = value
            self.size = int(data["size"])

    def finishSerializing(self, value: CommentedMap, key: str = None) -> CommentedMap:
        value.insert(1, "size", self.size)
        return super().finishSerializing(value, key)

    def genCTor(self) -> str:
        if self.size > 0:
            return f"{self.LIST_CTOR}(maxlen={self.size})"
        else:
            return super().genCTor()

    def addImportsTo(self, sg: "SavableGenerator") -> None:
        typing_imports = ["Deque"]
        if self.optional:
            typing_imports.append("Optional")
        if self.index_attr_name is not None:
            typing_imports.append("Tuple")
        sg.addImportFrom("typing", typing_imports)
        if self.value.class_type:
            sg.addImportFrom("lilitools.saves.savable", ["Savable"])
        self.value.addImports(sg)
        sg.addImportFrom("collections", ["deque"])


register_child_binding_style(StackChildBinding)

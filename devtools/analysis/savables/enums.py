from enum import IntEnum

class EAttrType(IntEnum):
    STR = 0
    INT = 1
    FLOAT = 2
    BOOL = 3

class EEnumStyle(IntEnum):
    NAME = 0
    VALUE = 1

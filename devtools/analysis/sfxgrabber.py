from __future__ import annotations, absolute_import

import ast
import collections
import json
from pathlib import Path
import pprint
import string
from typing import Dict, List, Optional, OrderedDict, Set

import autopep8
import isort
import javalang
from buildtools.maestro.base_target import SingleBuildTarget
from javalang.tree import ClassCreator, FieldDeclaration
from lxml import etree
from _codegen import writeGeneratedPythonWarningHeader
from enumFrom import ArgumentFetcher, simplifyArgs, transformToPythonEnum

# from lilitools.saves.character.enums.attribute import EAttribute

SFX_LIB = (
    Path("lib")
    / "liliths-throne-public"
    / "src"
    / "com"
    / "lilithsthrone"
    / "game"
    / "character"
    / "effects"
    / "StatusEffect.java"
)
SFX_PY = Path("lilitools") / "game" / "enums" / "known_status_effect.py"


class AbstractStatusEffectData:
    def __init__(self) -> None:
        self.name: str = ""
        self.pathName: str = ""
        self.beneficial: bool = True
        self.effects: Dict[ast.Attribute, ast.Constant] = {}

    def toAST(self) -> ast.AST:
        return ast.Call(
            func=ast.Name(id="AbstractStatusEffect", ctx=ast.Load()),
            args=self.getSimpRow(),
        )

    def getSimpRow(self) -> List[ast.AST]:
        return [
            ast.Constant(self.name),
            ast.Constant(self.pathName),
            ast.Constant(self.beneficial),
            ast.Dict(keys=self.effects.keys(), values=self.effects.values()),
        ]


def getStatusEffectsFromJavaEnum(
    file: Path, ignore: Set[str]
) -> OrderedDict[str, AbstractStatusEffectData]:
    cu = javalang.parse.parse(file.read_text())
    values = collections.OrderedDict()
    for stmt in cu.types[0].body:
        if (
            not isinstance(stmt, FieldDeclaration)
            or "static" not in stmt.modifiers
            or "public" not in stmt.modifiers
        ):
            continue
        typeid: Optional[str] = None
        if len(stmt.type.dimensions) == 0:  # No arrays
            for vdec in stmt.declarators:
                if isinstance(vdec.initializer, ClassCreator):
                    simped = simplifyArgs(vdec.initializer.arguments)
                    ased = AbstractStatusEffectData()
                    """
                    0 int renderingPriority,
                    1 String name,
                    2 String pathName,
                    3 Colour colourShade,
                    4 boolean beneficial,
                    5 Map<AbstractAttribute, Float> attributeModifiers,
                    6 List<String> extraEffects
                    """
                    fxd: ast.Dict
                    if len(simped) == 7:
                        ased.name = simped[1].value
                        # ased.pathName = simped[2].value
                        ased.beneficial = simped[4].value
                        fxd = simped[5]
                    """
                    0 int renderingPriority,
                    1 String name,
                    2 String pathName,
                    3 Colour colourShade,
                    4 Colour colourShadeSecondary,
                    5 boolean beneficial,
                    6 Map<AbstractAttribute, Float> attributeModifiers,
                    7 List<String> extraEffects
                    """
                    if len(simped) == 8:
                        ased.name = simped[1].value
                        ased.pathName = simped[2].value
                        ased.beneficial = simped[5].value
                        fxd = simped[6]
                    """
                    0 int renderingPriority,
                    1 String name,
                    2 String pathName,
                    3 Colour colourShade,
                    4 Colour colourShadeSecondary,
                    5 Colour colourShadeTertiary,
                    6 boolean beneficial,
                    7 Map<AbstractAttribute, Float> attributeModifiers,
                    8 List<String> extraEffects
                    """
                    if len(simped) == 9:
                        ased.name = simped[1].value
                        # ased.pathName = simped[2].value
                        ased.beneficial = simped[6].value
                        fxd = simped[7]
                    """
                    0 StatusEffectCategory category,
                    1 int renderingPriority,
                    2 String name,
                    3 String pathName,
                    4 Colour colourShade,
                    5 Colour colourShadeSecondary,
                    6 Colour colourShadeTertiary,
                    7 boolean beneficial,
                    8 Map<AbstractAttribute, Float> attributeModifiers,
                    9 List<String> extraEffects
                    """
                    if len(simped) == 10:
                        ased.name = simped[2].value
                        # ased.pathName = simped[3].value
                        ased.beneficial = simped[7].value
                        fxd = simped[8]
                    if isinstance(fxd, ast.Dict):
                        k_: ast.Attribute
                        v: ast.Constant
                        for k_, v in dict(zip(fxd.keys, fxd.values)).items():
                            k = ast.Attribute(
                                value=ast.Name(id="EAttribute", ctx=ast.Load()),
                                attr=k_.attr,
                            )
                            ased.effects[k] = v
                    # print(ased.name, ased.pathName, ased.effects)
                    if vdec.name in ignore:
                        continue
                    values[vdec.name] = ased
                    # print(ast.dump(ast.Tuple(elts=simped)))

    return values


def fix_name(inp: str) -> str:
    o = ""
    valid = string.ascii_letters + string.digits + "_"
    under = " -'"
    for c in inp:
        if c in valid:
            o += c
        elif c in under:
            o += "_"
        else:
            continue
    return o


class GrabSFXTarget(SingleBuildTarget):
    BT_LABEL: str = "GRAB SFX"

    def __init__(
        self,
        outfile: Path,
        infile: Path,
        jsonfile: Path,
        eattrfile: Path,
        sfx_dir: Path,
        fields: Optional[collections.OrderedDict] = None,
        dependencies: List[str] = [],
    ) -> None:
        self.outfile = outfile
        self.infile = infile
        self.jsonfile = jsonfile
        self.eattrfile = eattrfile
        self.fields = fields or collections.OrderedDict()
        self.sfx_dir = sfx_dir
        self.enumName: str = ""
        self.imports: List[str] = []
        self.ignore: Set[str] = set()
        super().__init__(
            target=str(outfile), files=[str(infile)], dependencies=dependencies
        )

    def build(self) -> None:
        cstr = ""
        sfx = getStatusEffectsFromJavaEnum(self.infile, self.ignore)
        fixid2sfxid = {k: k for k in sfx.keys()}
        sfxid2fixid = {k: k for k in sfx.keys()}
        for sfxfile in self.sfx_dir.rglob("*.xml"):
            tree = etree.parse(sfxfile)
            senode = tree.getroot()
            ased = AbstractStatusEffectData()
            ased.name = senode.find("name").text
            id = ased.name.upper().replace(" ", "_").replace("'", "")
            ased.pathName = str(
                sfxfile.absolute().relative_to(
                    Path("lib/liliths-throne-public").absolute()
                )
            )
            ased.beneficial = senode.find("beneficial").text
            if (found := senode.find("attributeModifiers")) is not None:
                for modifier in found.findall("modifier"):
                    ased.effects[
                        ast.Attribute(
                            value=ast.Name(id="EAttribute", ctx=ast.Load()),
                            attr=modifier.text,
                        )
                    ] = ast.Constant(float(modifier.attrib["value"]))
            assert isinstance(ased, AbstractStatusEffectData)
            if id in self.ignore:
                continue
            sfx[id] = ased
            fixid2sfxid[id] = id
            sfxid2fixid[id] = id
        data = json.loads(self.jsonfile.read_text())
        eattrs = self.eattrfile.read_text()
        assert data["version"] == 1
        for sfxid, sd in data["sfx"].items():
            ased = AbstractStatusEffectData()
            ased.name = sd["name"]["male"]
            ased.pathName = sfxid
            ased.beneficial = sd["beneficial"]
            for k, v in sd["attributeModifiers"]["male"].items():
                if k not in eattrs:
                    continue
                ased.effects[
                    ast.Attribute(
                        value=ast.Name(id="EAttribute", ctx=ast.Load()), attr=k
                    )
                ] = ast.Constant(float(v))
            assert isinstance(ased, AbstractStatusEffectData)
            id = fix_name(sfxid.upper().replace(" ", "_").replace("'", ""))
            if id in self.ignore:
                continue
            sfx[id] = ased
            fixid2sfxid[id] = ased.pathName
            sfxid2fixid[ased.pathName] = id

        getByName = ast.FunctionDef(
            name="GetByName",
            args=ast.arguments(
                posonlyargs=[],
                args=[
                    # ast.arg(arg='self', annotation=None),
                    ast.arg(arg="name", annotation=ast.Name(id="str", ctx=ast.Load())),
                ],
                kw_defaults=[],
                kwonlyargs=[],
                defaults=[],
                vararg=None,
                kwarg=None,
            ),
            returns=ast.Name(id="EKnownStatusEffect", ctx=ast.Load()),
            decorator_list=[ast.Name(id="staticmethod", ctx=ast.Load())],
            body=[
                ast.Return(
                    value=ast.Subscript(
                        value=ast.Name(id="EKnownStatusEffect", ctx=ast.Load()),
                        slice=ast.Subscript(
                            value=ast.Attribute(
                                value='EKnownStatusEffect',
                                attr='NAME2ID'
                            ),
                            slice=ast.Name(id="name")
                        ),
                    ),
                    ctx=ast.Load(),
                )
            ],
        )

        cstr += transformToPythonEnum(
            contents={
                "EKnownStatusEffect": [
                    (k, ased.getSimpRow()) for k, ased in sfx.items()
                ]
            },
            attrs_for_instance=self.fields,
            additional_methods=[getByName],
        )
        cstr += "\nEKnownStatusEffect.ID2NAME=" + pprint.pformat(fixid2sfxid) + "\n"
        cstr += "EKnownStatusEffect.NAME2ID=" + pprint.pformat(sfxid2fixid) + "\n"

        cfg = isort.Config(
            line_length=65355,
            add_imports=self.imports
            + ["from typing import Dict", "from __future__ import annotations"],
            ignore_whitespace=True,
            quiet=True,
            lines_after_imports=0,
            lines_before_imports=0,
            lines_between_sections=0,
            lines_between_types=0,
        )
        cstr = isort.code(cstr, config=cfg)
        cstr = autopep8.fix_code(
            cstr,
            options=dict(
                max_line_length=65535,
                ignore=[
                    "E265",  # E265 - Format block comments.
                    "E266",  # E266 - Fix too many leading '#' for block comments.
                ],
                aggressive=2,
            ),
        )
        with self.outfile.open('w') as f:
            writeGeneratedPythonWarningHeader(f,'Status Effects',Path(__file__))
            f.write(cstr)


class GrabEnumWithArgsTarget(SingleBuildTarget):
    BT_LABEL: str = "ENUM"

    def __init__(
        self,
        outfile: Path,
        infile: Path,
        jsonfile: Path,
        sfx_dir: Path,
        fields: Optional[collections.OrderedDict] = None,
        fetchers: Optional[List[ArgumentFetcher]] = None,
        dependencies: List[str] = [],
    ) -> None:
        self.outfile = outfile
        self.infile = infile
        self.jsonfile = jsonfile
        self.fields = fields or collections.OrderedDict()
        self.fetchers = fetchers or []
        self.sfx_dir = sfx_dir
        self.enumName: str = ""
        self.imports: List[str] = []
        super().__init__(
            target=str(outfile), files=[str(infile)], dependencies=dependencies
        )

    def build(self) -> None:
        writingFile = Path(__file__).absolute().relative_to(Path.cwd())
        cstr = f"# auto@generated by {writingFile}\n"
        sfx = getStatusEffectsFromJavaEnum(self.infile)
        for sfxfile in self.sfx_dir.rglob("*.xml"):
            tree = etree.parse(sfxfile)
            senode = tree.getroot()
            ased = AbstractStatusEffectData()
            ased.name = senode.find("name").text
            id = ased.name.upper().replace(" ", "_").replace("'", "")
            # ased.pathName = str(sfxfile.absolute().relative_to(Path('lib/liliths-throne-public').absolute()))
            ased.beneficial = senode.find("beneficial").text
            if (found := senode.find("attributeModifiers")) is not None:
                for modifier in found.findall("modifier"):
                    ased.effects[
                        ast.Attribute(
                            value=ast.Name(id="EAttribute", ctx=ast.Load()),
                            attr=modifier.text,
                        )
                    ] = ast.Constant(float(modifier.attrib["value"]))
            sfx[id] = ased
        data = json.loads(self.jsonfile.read_text())
        assert data["version"] == 1
        for sfxid, sfx in data["sfx"].items():
            ased = AbstractStatusEffectData()
            ased.name = sfx["name"]["male"]
            # ased.pathName = sfxid
            ased.beneficial = sfx["beneficial"] == "BENEFICIAL"
            for k, v in sfx["attributeModifiers"]:
                ased.effects[
                    ast.Attribute(
                        value=ast.Name(id="EAttribute", ctx=ast.Load()), attr=k
                    )
                ] = ast.Constant(float(v))
            sfx[id] = ased
        cstr += transformToPythonEnum(
            contents={
                "EKnownStatusEffect": [
                    (k, ased.getSimpRow()) for k, ased in sfx.items()
                ]
            },
            attrs_for_instance=self.fields,
        )
        cfg = isort.Config(
            line_length=65355,
            add_imports=self.imports,
            ignore_whitespace=True,
            quiet=True,
            lines_after_imports=0,
            lines_before_imports=0,
            lines_between_sections=0,
            lines_between_types=0,
        )
        cstr = isort.code(cstr, config=cfg)
        cstr = autopep8.fix_code(
            cstr,
            options=dict(
                max_line_length=65535,
                ignore=[
                    "E265",  # E265 - Format block comments.
                    "E266",  # E266 - Fix too many leading '#' for block comments.
                ],
                aggressive=2,
            ),
        )
        self.outfile.write_text(cstr)

from typing import NamedTuple
from ruamel.yaml.comments import Comment

def copy_ca_to(src: Comment, dest: Comment) -> None:
    if src and dest:
        if src.comment is not None:
            if dest.comment is None:
                dest.comment = [None, None]
            if src.comment[0] is not None:
                dest.comment[0] = dest.comment[0]
            if src.comment[1] is not None and len(src.comment[1]) > 0:
                dest.comment[1] = dest.comment[1]
        if len(src.end) > 0:
            dest.end = src.end


class ProtoItemEffect(NamedTuple):
    type: str
    mod1: str
    mod2: str
    potency: str
    limit: int
    timer: int



import argparse
import re
from pathlib import Path

from _patchinator import Patchinator
from declarative_argparse import DeclarativeOptionParser


class Options(DeclarativeOptionParser):
    def __init__(self, argp: argparse.ArgumentParser | None = None) -> None:
        super().__init__(argp)
        self.lt_dir = self.addPath('lt_dir', description='Location on disk of liliths-throne-public')


def main() -> None:
    args = Options()
    args.parseArguments()
    LT_DIR = Path(args.lt_dir.get_value())
    LTDOTCOM = LT_DIR / 'src' / 'com' / 'lilithsthrone'

    p = Patchinator('Helpful Info')
    GAMECHAR_JAVA = LTDOTCOM / 'game' / 'character' / 'GameCharacter.java'
    with p.addFile(GAMECHAR_JAVA, Path('GameCharacter.java')) as pf:
        with pf.addPatch('Add helpfulInformation tag') as pp:
            pp.findStrippedLine(re.escape('// ************** Artwork overrides **************//'))
            pp.findUnstrippedLine(r'^\t\t\}')
            pp.injectLines([
                '',
                '\t\t/** HELPFUL INFO **/',
                '\t\tElement helpfulInfo = doc.createElement("helpfulInformation");',
                '\t\tproperties.appendChild(helpfulInfo);',
                '\t\t{',
                '\t\t\tComment hiComment = doc.createComment("Added by HelpfulInfo mod.");',
                '\t\t\thelpfulInfo.appendChild(hiComment);',
                '\t\t\tXMLUtil.createXMLElementWithValue(doc, helpfulInfo, "race", String.valueOf(this.getSubspecies().getRace().getName(false)));',
                '\t\t\tXMLUtil.createXMLElementWithValue(doc, helpfulInfo, "subspecies", String.valueOf(this.getSubspecies().getName(null)));',
                '\t\t\tString ssn = "";',
                '\t\t\tAbstractSubspecies subspecies = this.getSubspecies();',
                '\t\t\tif(subspecies!=null) {',
                '\t\t\t\tif (this.isFeminine()) {',
                '\t\t\t\t\tssn = subspecies.getSingularFemaleName(this.body);',
                '\t\t\t\t} else {',
                '\t\t\t\t\tssn = subspecies.getSingularMaleName(this.body);',
                '\t\t\t\t}',
                '\t\t\t\tssn = Util.capitaliseSentence(ssn);',
                '\t\t\t}',
                '\t\t\tXMLUtil.createXMLElementWithValue(doc, helpfulInfo, "fullSpeciesName", String.valueOf(ssn));',
                '\t\t\tXMLUtil.createXMLElementWithValue(doc, helpfulInfo, "slaveValue", String.valueOf(this.getValueAsSlave(true)));',
                '\t\t}',
                #'',
            ])
            pp.finish()
    p.patch()


if __name__ == "__main__":
    main()

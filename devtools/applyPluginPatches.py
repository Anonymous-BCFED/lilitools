import argparse
import re
from pathlib import Path

from _dev_regex import REG_FETISH_GETIDFROMFETISH, REG_JAVA_CATCH_EXCEPTION_E, REG_JAVA_PUBLIC_STATIC_LIST_ABSTRACTPERK
from _patchinator import Patchinator
from declarative_argparse import DeclarativeOptionParser


class Options(DeclarativeOptionParser):
    def __init__(self, argp: argparse.ArgumentParser | None = None) -> None:
        super().__init__(argp)
        self.lt_dir = self.addPath('lt_dir', description='Location on disk of liliths-throne-public')


def main() -> None:
    args = Options()
    args.parseArguments()
    LT_DIR = Path(args.lt_dir.get_value())
    LT_DIR = LT_DIR / 'src' / 'com' / 'lilithsthrone'

    p = Patchinator('Plugin system')

    MAINCONTROLLER = LT_DIR / 'controller' / 'MainController.java'
    with p.addFile(MAINCONTROLLER, MAINCONTROLLER) as pf:
        with pf.addPatch('Replace Fetish.getIdFromFetish(f) with f.getId().') as pp:
            pp.replaceAll(REG_FETISH_GETIDFROMFETISH, '\1.getId()')

    GAME = LT_DIR / 'game' / 'Game.java'
    with p.addFile(GAME, GAME) as pf:
        with pf.addPatch('Inject plugin loader.') as pp:
            pp.findStrippedLine(re.compile(re.escape('import com.lilithsthrone.rendering.Artwork;')))
            pp.injectLines(['import com.lilithsthrone.modding.PluginLoader;'])

            pp.findStrippedLine(re.compile(re.escape('if(!Main.game.NPCMap.containsKey(Main.game.getUniqueNPCId(Aurokaris.class))) { addNPC(new Aurokaris(), false); addedNpcs.add(Aurokaris.class); }')))
            pp.replaceLine(needle=REG_JAVA_CATCH_EXCEPTION_E,
                           replacement='\t\t\t// Let plugins know they can add stuff now.\n' +
                           '\t\t\tPluginLoader.getInstance().onInitUniqueNPCs(addedNpcs);\n' +
                           '\t\t} catch(Exception e) {\n')
            pp.finish()

    CHARACTERUTILS = LT_DIR / 'game' / 'character' / 'CharacterUtils.java'
    with p.addFile(CHARACTERUTILS, CHARACTERUTILS) as pf:
        with pf.addPatch('Inject plugin events') as pp:
            pp.findStrippedLine(re.compile(re.escape('import com.lilithsthrone.main.Main;')))
            pp.injectLines(['import com.lilithsthrone.modding.PluginLoader;'])
            pp.findStrippedLine(re.compile(re.escape('availableFetishes.remove(Fetish.FETISH_VAGINAL_RECEIVING);')))
            pp.injectLines(['\t\tPluginLoader.getInstance().onGenerateDesiresAvailableFetishesFixup(character,availableFetishes);'])
            pp.findStrippedLine(re.compile(re.escape('desiresAssigned++;')))
            pp.findStrippedLine(re.compile(re.escape('}')))
            pp.injectLines(['\t\tPluginLoader.getInstance().onAfterGenerateDesires(character,availableFetishes,desireMap,negativeMap,desiresAssigned);'])
            pp.finish()

    GAMECHARACTER = LT_DIR / 'game' / 'character' / 'GameCharacter.java'
    with p.addFile(GAMECHARACTER, GAMECHARACTER) as pf:
        with pf.addPatch('Inject plugin events') as pp:
            pp.findStrippedLine(re.compile(re.escape('import com.lilithsthrone.main.Main;')))
            pp.injectLines(['import com.lilithsthrone.modding.PluginLoader;'])
            pp.findStrippedLine(re.compile(re.escape('addSexTypeWeighting(new SexType(SexParticipantType.NORMAL, SexAreaPenetration.FINGER, SexAreaPenetration.FINGER), target, request, foreplaySexTypes, 0.5f);')))
            pp.injectLines([
                '',
                '\t\tPluginLoader.getInstance().onGenerateSexChoicesAddSexTypes(this,resetPositioningBan,target,request,foreplaySexTypes,mainSexTypes);',
            ])
            pp.finish()

    ABSTRACTPERK = LT_DIR / 'game' / 'character' / 'effects' / 'AbstractPerk.java'
    with p.addFile(ABSTRACTPERK, ABSTRACTPERK) as pf:
        with pf.addPatch('Add ID field and getter') as pp:
            pp.findStrippedLine(re.compile(re.escape('import com.lilithsthrone.game.combat.spells.SpellUpgrade;')))
            pp.injectLines(['import com.lilithsthrone.modding.BasePlugin;'])
            pp.findStrippedLine(re.compile(re.escape('public abstract class AbstractPerk {')))
            pp.injectLines(['private String id;'])
            pp.findStrippedLine(re.compile(re.escape('public SpellSchool getSchool() {')))
            pp.findStrippedLine(re.compile(re.escape('}')))
            pp.injectLines([
                '\t',
                '\tpublic final String getId() {',
                '\t\treturn this.id;',
                '\t}',
                '\t',
                '\tpublic void assignId(BasePlugin plugin, String id) {',
                '\t\tif(this.id != null)',
                '\t\t\treturn;',
                '\t\tif(plugin == null) {',
                '\t\t\tthis.id = id; // Stock',
                '\t\t} else {',
                '\t\t\tthis.id = String.format("%s_%s", plugin.metadata.id.toString().replaceAll("-", "_"), id);',
                '\t\t}',
                '\t}',
            ])
            pp.finish()

    PERK = LT_DIR / 'game' / 'character' / 'effects' / 'Perk.java'
    with p.addFile(PERK, PERK) as pf:
        with pf.addPatch('Add imports') as pp:
            pp.findStrippedLine(re.compile(re.escape('import com.lilithsthrone.game.dialogue.utils.UtilText;')))
            pp.injectLines([
                'import com.lilithsthrone.modding.BasePlugin;',
                'import com.lilithsthrone.modding.PluginLoader;',
            ])
            pp.finish()
        with pf.addPatch('Instantiate AbstractPerk lists') as pp:
            pp.replaceAll(REG_JAVA_PUBLIC_STATIC_LIST_ABSTRACTPERK, 'public static List<AbstractPerk> \1 = new AbstractList<>();')
        with pf.addPatch('Trigger plugin perk list event') as pp:
            pp.replaceAll(re.compile(re.escape('generateSubspeciesPerks();')), 'Perk.getAllPerks(); // Trigger perk list build')
        with pf.addPatch('Patch getIDFromPerk() return') as pp:
            pp.findStrippedLine(re.compile(re.escape('return perkToIdMap.get(perk);')))
            pp.injectLines(['return perk.getId();'])
            pp.finish()
        with pf.addPatch('Add new methods') as pp:
            pp.findStrippedLine(re.compile(re.escape('public static String getIdFromPerk(AbstractPerk perk) {')))
            pp.findStrippedLine(re.compile(re.escape('return perk.getId();')))
            pp.findStrippedLine(re.compile(re.escape('}')))
            pp.injectLines([
                '\tpublic static void addPerk(BasePlugin plugin, String id, AbstractPerk perk) {',
                '\t\t// I feel like this is stupid :thinking:',
                '\t\tif(!allPerks.contains(perk)) {',
                '\t\t\tperk.assignId(plugin, id);',
                '\t\t\t',
                '\t\t\tperkToIdMap.put(perk, perk.getId());',
                '\t\t\tidToPerkMap.put(perk.getId(), perk);',
                '\t',
                '\t\t\tallPerks.add(perk);',
                '\t\t\tif (perk.isHiddenPerk()) {',
                '\t\t\t\thiddenPerks.add(perk);',
                '\t\t\t}',
                '\t\t}',
                '\t}',
                '',
                '\t// Theoretically, this would work, but since you can\'t screw with an enum\'s members, it can\'t actually be used:(',
                '\t// public static void overridePerk(String id, AbstractPerk perk) {',
                '\t// \tif(idToPerkMap.containsKey(id)) {',
                '\t// \t\tAbstractPerk oldPerk = idToPerkMap.get(id);',
                '\t// \t\tallPerks.remove(oldPerk);',
                '\t// \t\tif(oldPerk.isHiddenPerk())',
                '\t// \t\t\thiddenPerks.remove(oldPerk);',
                '\t// \t}',
                '\t// \taddPerk(id, perk);',
                '\t// }',
            ])
            pp.finish()
        with pf.addPatch('Disable static constructor') as pp:
            pp.replaceLine(re.compile('^[ \t]*' + re.escape('static {')), '\t/*\n\tstatic {\n')
            pp.findUnstrippedLine(re.compile(r'\t\}'))
            pp.injectLines(['\t*/'])
            pp.finish()
        with pf.addPatch('Patch Perks.generateSubspeciesPerks()') as pp:
            pp.replaceLine(re.compile(re.escape('\tprivate static void generateSubspeciesPerks()')), '\tpublic static void generateSubspeciesPerks() {')
            INDENT = '\t' * 4
            pp.removeUnstrippedLine(INDENT + 'perkToIdMap.put(racePerk, Subspecies.getIdFromSubspecies(subToUse));')
            pp.removeUnstrippedLine(INDENT + 'idToPerkMap.put(Subspecies.getIdFromSubspecies(subToUse), racePerk);')
            pp.replaceLine(INDENT + 'allPerks.add(racePerk);', INDENT + 'Perk.addPerk(null, Subspecies.getIdFromSubspecies(subToUse), racePerk);')
            pp.findStrippedLine('subspeciesPerksGenerated = true;')
            pp.injectLines([
                '\t\t',
                '\t\tPluginLoader.getInstance().onInitPerks();',
                '\t\t',
            ])
            pp.finish()
        with pf.addPatch('Patch Perks.getAllPerks()') as pp:
            pp.findStrippedLine('public static List<AbstractPerk> getAllPerks() {')
            pp.removeUnstrippedLine('\t\tif(!subspeciesPerksGenerated) {')
            pp.removeUnstrippedLine('\t\t\tgenerateSubspeciesPerks();')
            pp.removeUnstrippedLine('\t\t}')
            pp.replaceLine('\t\treturn allPerks;', 'return PluginLoader.getInstance().getPerks().getAll();')
            pp.finish()
        with pf.addPatch('Patch Perks.getHiddenPerks()') as pp:
            pp.findStrippedLine('public static List<AbstractPerk> getHiddenPerks() {')
            pp.removeUnstrippedLine('\t\tif(!subspeciesPerksGenerated) {')
            pp.removeUnstrippedLine('\t\t\tgenerateSubspeciesPerks();')
            pp.removeUnstrippedLine('\t\t}')
            pp.replaceLine('\t\treturn hiddenPerks;', 'return PluginLoader.getInstance().getPerks().getHidden();')
            pp.finish()

    PERK = LT_DIR / 'game' / 'character' / 'fetishes' / 'AbstractFetish.java'
    with p.addFile(PERK, PERK) as pf:
        with pf.addPatch('Patch fields') as pp:
            pp.findStrippedLine('public abstract class AbstractFetish {')
            pp.injectLines([
                '\tprivate String id;',
                '\t'
            ])
            pp.replaceLine('\tprivate String pathName;', '\tprotected String pathName;')
            pp.replaceLine('\tprivate String SVGString;', '\tprotected String SVGString;')
            pp.finish()

    BRAX = LT_DIR / 'game' / 'character' / 'npc' / 'dominion' / 'Brax.java'
    with p.addFile(BRAX, BRAX) as pf:
        with pf.addPatch('Add imports') as pp:
            pp.findStrippedLine('import com.lilithsthrone.main.Main;')
            pp.injectLines(['import com.lilithsthrone.modding.PluginLoader;'])
        with pf.addPatch('Add events') as pp:
            pp.findStrippedLine('effects.addAll(maximumEffects);')
            pp.injectLines([
                '\t\t',
                '\t\tPluginLoader.getInstance().onNPCGenerateTransformativePotion(this, target, effects);',
            ])
            pp.finish()

    NATALYA = LT_DIR / 'game' / 'character' / 'npc' / 'dominion' / 'Natalya.java'
    with p.addFile(NATALYA, NATALYA) as pf:
        with pf.addPatch('Add imports') as pp:
            pp.findStrippedLine('import com.lilithsthrone.main.Main;')
            pp.injectLines(['import com.lilithsthrone.modding.PluginLoader;'])
        with pf.addPatch('Add events') as pp:
            pp.findStrippedLine('effects.add(new PossibleItemEffect(new ItemEffect(itemType.getEnchantmentEffect(), TFModifier.TF_PENIS, TFModifier.TF_MOD_WETNESS, TFPotency.MAJOR_BOOST, 1), ""));')
            pp.injectLines([
                '\t\t',
                '\t\tPluginLoader.getInstance().onNPCGenerateTransformativePotion(this, target, effects);',
            ])
            pp.finish()

    IMPATTACKER = LT_DIR / 'game' / 'character' / 'npc' / 'submission' / 'ImpAttacker.java'
    with p.addFile(IMPATTACKER, IMPATTACKER) as pf:
        with pf.addPatch('Add imports') as pp:
            pp.findStrippedLine('import com.lilithsthrone.main.Main;')
            pp.injectLines(['import com.lilithsthrone.modding.PluginLoader;'])
        with pf.addPatch('Add events') as pp:
            pp.findStrippedLine('new ItemEffect(itemType.getEnchantmentEffect(), TFModifier.TF_VAGINA, TFModifier.TF_MOD_WETNESS, TFPotency.MAJOR_BOOST, 1),')
            pp.findStrippedLine('}')
            pp.findStrippedLine('}')
            pp.injectLines([
                '\t\t',
                '\t\tPluginLoader.getInstance().onNPCGenerateTransformativePotion(this, target, effects);',
            ])
            pp.finish()

    TAKAHASHI = LT_DIR / 'game' / 'character' / 'npc' / 'submission' / 'Takahashi.java'
    with p.addFile(TAKAHASHI, TAKAHASHI) as pf:
        with pf.addPatch('Add imports') as pp:
            pp.findStrippedLine('import com.lilithsthrone.main.Main;')
            pp.injectLines(['import com.lilithsthrone.modding.PluginLoader;'])
        with pf.addPatch('Add events') as pp:
            pp.findStrippedLine('effects.addAll(maximumEffects);')
            pp.injectLines([
                '\t\t',
                '\t\tPluginLoader.getInstance().onNPCGenerateTransformativePotion(this, target, effects);',
            ])
            pp.finish()

    RACE = LT_DIR / 'game' / 'character' / 'race' / 'Race.java'
    with p.addFile(RACE, RACE) as pf:
        with pf.addPatch('Add imports') as pp:
            pp.findStrippedLine('import com.lilithsthrone.main.Main;')
            pp.injectLines(['import com.lilithsthrone.modding.PluginLoader;'])
        with pf.addPatch('Add events') as pp:
            pp.findStrippedLine('Attribute.allAttributes.add(racialAttribute);')
            pp.findStrippedLine('}')
            pp.findStrippedLine('}')
            pp.injectLines([
                '\t\t',
                '\t\tPluginLoader.getInstance().forEachPlugin(p -> p.onInitRaces());',
            ])
            pp.finish()

    SEXTYPE = LT_DIR / 'game' / 'sex' / 'SexType.java'
    with p.addFile(SEXTYPE, SEXTYPE) as pf:
        with pf.addPatch('Add imports') as pp:
            pp.findStrippedLine('import com.lilithsthrone.main.Main;')
            pp.injectLines(['import com.lilithsthrone.modding.PluginLoader;'])
        with pf.addPatch('Add events') as pp:
            pp.findStrippedLine('// Check for masturbation:')
            pp.findStrippedLine('}')
            pp.injectLines([
                '\t\t',
                '\t\tPluginLoader.getInstance().forEachPlugin(p->p.getRelatedFetishes(fetishes,characterPerforming,characterTargeted,isPenetration,isOrgasm));',
            ])
            pp.finish()

    SEXACTION = LT_DIR / 'game' / 'sex' / 'sexActions' / 'SexAction.java'
    with p.addFile(SEXACTION, SEXACTION) as pf:
        with pf.addPatch('Add imports') as pp:
            pp.findStrippedLine('import com.lilithsthrone.main.Main;')
            pp.injectLines(['import com.lilithsthrone.modding.PluginLoader;'])
        with pf.addPatch('Add events') as pp:
            pp.findStrippedLine('characterFetishesForPartner.get(characterPerformingAction).add(Fetish.FETISH_VAGINAL_RECEIVING);')
            pp.findStrippedLine('}')
            pp.findStrippedLine('}')
            pp.injectLines([
                '\t\t\t\tPluginLoader.getInstance().onSexActionFetishesForEitherPartner(this, characterPerformingAction,characterFetishes,characterFetishesForPartner,cummedOnList);',
            ])
            pp.finish()

    MAIN = LT_DIR / 'main' / 'Main.java'
    with p.addFile(MAIN, MAIN) as pf:
        with pf.addPatch('Add imports') as pp:
            pp.findStrippedLine('import com.lilithsthrone.game.sex.Sex;')
            pp.injectLines(['import com.lilithsthrone.modding.PluginLoader;'])
        with pf.addPatch('Patch Main.start()') as pp:
            pp.findStrippedLine('public void start(Stage primaryStage) throws Exception {')
            pp.findStrippedLine('e.printStackTrace();')
            pp.findStrippedLine('}')
            pp.injectLines([
                '\t\t',
                '\t\tPluginLoader.getInstance().onMainStart();',
            ])
            pp.finish()
        with pf.addPatch('Patch Main.main()') as pp:
            pp.findStrippedLine('public static void main(String[] args) {')
            pp.findStrippedLine('properties.savePropertiesAsXML();')
            pp.findStrippedLine('}')
            pp.injectLines([
                '\t\t',
                '\t\tPluginLoader.getInstance().onMainMain();',
            ])
            pp.finish()

    p.patch()


if __name__ == "__main__":
    main()

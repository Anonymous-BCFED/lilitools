import ast
import subprocess
from pathlib import Path
from typing import Any, Dict, List, Tuple, cast

import toml
from buildBins import addBinBuildSteps
from buildtools import os_utils
from buildtools.maestro import BuildMaestro
from buildtools.maestro.base_target import SingleBuildTarget
from buildtools.maestro.fileio import ReplaceTextTarget
from buildWeb import addWebBuildSteps
from buildPregex import BuildGameRegexPy
from buildConsts import BuildConsts
from genNPCIndex import GenerateNPCIndex
from genSavable import SavableGenerator

from analysis.indexbuilders.item_index_builder import GenerateIndex

from mkast.modulefactory import ModuleFactory

from _dev_regex import REG_VERSION_STR, REG_SUBMODULE_STATUS
from _codegen import LTExpandedCodeGenerator
from _codegen import writeGeneratedPythonWarningHeader

from ruamel.yaml import YAML as Yaml  # isort: skip

yaml = Yaml(typ="rt")

SCRIPT_DIR = Path(__file__).parent.parent
SAVABLES_DIR = SCRIPT_DIR / "data" / "savables"
LT_DIR = SCRIPT_DIR / "lib" / "liliths-throne-public"

UNPC_DIR = Path.cwd() / "lilitools" / "game" / "characters"
HISTORIES_DIR = Path.cwd() / "lilitools" / "game" / "histories"
ITEMTYPES_DIR = Path.cwd() / "lilitools" / "saves" / "items" / "itemtypes"

JAVA_SRC: Path = LT_DIR / "src" / "com" / "lilithsthrone"
MAIN_JAVA: Path = JAVA_SRC / "main" / "Main.java"


class GenerateSavableBuildStep(SingleBuildTarget):
    BT_LABEL = "SAVABLE"

    def __init__(
        self,
        infile: Path,
        outfile: Path,
        preserve_save_ordering: bool,
        dependencies: List[str] = [],
    ) -> None:
        super().__init__(
            target=str(outfile), files=[str(infile)], dependencies=dependencies
        )
        self.infile: Path = infile
        self.outfile: Path = outfile
        self.preserve_save_ordering: bool = preserve_save_ordering

    def get_config(self) -> Dict[str, Any]:
        o: Dict[str, Any] = super().get_config()
        o["psa"] = self.preserve_save_ordering
        return o

    def build(self):
        sg = SavableGenerator()
        sg.preserve_save_ordering = self.preserve_save_ordering
        sg.loadFrom(self.infile)
        sg.savable.className = f"Raw{sg.savable.className}"
        if not self.outfile.parent.is_dir():
            self.outfile.parent.mkdir(parents=True)
        sg.generate(self.outfile)


class BuildModule(SingleBuildTarget):
    BT_LABEL = "GEN"

    def __init__(self, targetfile: Path) -> None:
        self.targetpath = targetfile
        self.module = ModuleFactory()
        self.description: str = "FIXME (self.description)"
        super().__init__(target=str(self.targetpath), files=[])

    def build(self) -> None:
        with self.targetpath.open("w") as f:
            writeGeneratedPythonWarningHeader(f, self.description, Path(__file__))
            f.write(LTExpandedCodeGenerator().generate(self.module.generate()))


def addSavableBuildSteps(
    bm: BuildMaestro, preserve_save_order: bool, dependencies: List[str] = []
) -> List[str]:
    SAVABLES_IDX = SAVABLES_DIR / "_index.yml"

    generatedSavables: List[str] = []
    with SAVABLES_IDX.open("r") as f:
        data = yaml.load(f)
        for filepath in data["files"]:
            infile: Path = SAVABLES_DIR / filepath
            try:
                with infile.open("r") as wf:
                    wdata = yaml.load(wf)
            except Exception as e:
                raise Exception("oh god what", infile, e)
            if wdata is None:
                raise Exception("oh god what", infile)
            outfile = SCRIPT_DIR / "lilitools" / "saves" / wdata["generate"]["python"]
            generatedSavables.append(
                bm.add(
                    GenerateSavableBuildStep(
                        infile, outfile, preserve_save_order, dependencies=dependencies
                    )
                ).target
            )
    return generatedSavables


def addGenerateFiles(
    bm: BuildMaestro, preserve_save_ordering: bool, dependencies: List[str] = []
) -> List[str]:
    version = ""
    main = MAIN_JAVA.read_text()
    LILITHS_THRONE_VERSION = REG_VERSION_STR.search(main)[1]

    LILITOOLS_VERSION = ".".join(list(map(str, getLilitoolsVersion())))

    submodule_statuses: Dict[str, bytes] = {}
    o = subprocess.getoutput("git submodule status")
    for m in REG_SUBMODULE_STATUS.finditer(o):
        submodule_statuses[m["path"]] = bytes.fromhex(m["commit"])

    LILITHS_THRONE_COMMIT = submodule_statuses[
        str(Path("lib") / "liliths-throne-public")
    ]

    genned = [
        bm.add(BuildGameRegexPy(SCRIPT_DIR / "lilitools" / "regex_consts.py")).target
    ]

    ltc: BuildModule = bm.add(
        BuildModule(SCRIPT_DIR / "lilitools" / "liliths_throne_consts.py")
    )
    ltc.description = "Constants from the game itself"
    ltc.module.addVariableDecl(
        "LILITHS_THRONE_VERSION",
        ast.Name("str"),
        ast.Constant(LILITHS_THRONE_VERSION),
        final=True,
    )
    ltc.module.addVariableDecl(
        "LILITHS_THRONE_COMMIT",
        ast.Name("bytes"),
        ast.Call(
            func=ast.Name("bytes"),
            args=[
                ast.List(elts=[ast.Constant(int(x)) for x in LILITHS_THRONE_COMMIT]),
            ],
            keywords=[],
        ),
        final=True,
    )
    genned.append(ltc.target)

    genned += [
        # bm.add(ReplaceTextTarget(
        # str(SCRIPT_DIR / 'lilitools' / 'consts.py'),
        # str(SCRIPT_DIR / 'lilitools' / 'consts.py.in'),
        # replacements={
        #     '@@LILITOOLS_VERSION@@': LILITOOLS_VERSION,
        #     '@@PRESERVE_SAVE_ORDERING@@': repr(preserve_save_ordering),
        # })).target
        bm.add(
            BuildConsts(
                SCRIPT_DIR / "lilitools" / "consts.py",
                preserve_save_ordering=preserve_save_ordering,
            )
        ).target
    ]
    return genned


def getLilitoolsVersion() -> Tuple[int, int, int]:
    with open(SCRIPT_DIR / "pyproject.toml", "r") as f:
        data = toml.load(f)
    return cast(
        Tuple[int, int, int],
        tuple(map(int, data["tool"]["poetry"]["version"].split("."))),
    )


def main():
    from _utils import file2builddir

    bm = BuildMaestro(file2builddir(__file__))
    argp = bm.build_argparser()
    argp.add_argument("--skip-bins", action="store_true", default=False)
    argp.add_argument(
        "--save-comparison-mode-will-break-shit", action="store_true", default=False
    )
    args = bm.parse_args(argp)

    pso = args.save_comparison_mode_will_break_shit
    if pso:
        print("PRESERVE_SAVE_ORDERING", "=", pso)

    deps = []
    deps += addSavableBuildSteps(bm, preserve_save_order=pso)
    deps += addGenerateFiles(bm, preserve_save_ordering=pso)

    npcs: GenerateIndex = bm.add(
        GenerateIndex(
            UNPC_DIR, UNPC_DIR / "__init__.py", "NPCS_BY_PATH", "NPC", "CLASS_PATH"
        )
    )
    deps.append(npcs.target)
    npcs.index.addImport("lilitools.saves.character.npc", ["NPC"])

    histories: GenerateIndex = bm.add(
        GenerateIndex(
            HISTORIES_DIR,
            HISTORIES_DIR / "__init__.py",
            "HISTORIES_BY_ID",
            "History",
            "ID",
        )
    )
    deps.append(histories.target)
    histories.index.addImport("lilitools.game.histories.history", ["History"])

    itemtypes: GenerateIndex = bm.add(
        GenerateIndex(
            ITEMTYPES_DIR,
            ITEMTYPES_DIR / "__init__.py",
            "ITEMTYPES_BY_ID",
            "Savable",
            "ID_SET",
        )
    )
    deps.append(itemtypes.target)
    itemtypes.index.addImport("lilitools.saves.savable", ["Savable"])

    deps.append(
        addWebBuildSteps(
            bm, os_utils.assertWhich("yarn"), devmode=False, dependencies=[]
        )
    )

    if not args.skip_bins:
        addBinBuildSteps(bm, deps)

    bm.as_app(argp)


if __name__ == "__main__":
    main()

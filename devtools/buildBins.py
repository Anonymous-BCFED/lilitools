import re
from pathlib import Path
from typing import List, Set, Tuple, cast

import toml
from buildtools import os_utils
from buildtools.maestro import BuildMaestro
from buildtools.maestro.base_target import SingleBuildTarget
from buildtools.maestro.fileio import CopyFileTarget, CopyFilesTarget
from buildtools.maestro.nuitka import NuitkaTarget
from lxml import etree

from _dev_regex import REG_VERSION_SEARCH


def getGameVersion(LILITH_DIR: Path) -> Tuple[int, int, int, int]:
    filename = LILITH_DIR / "src" / "com" / "lilithsthrone" / "main" / "Main.java"
    with filename.open("r") as f:
        m = REG_VERSION_SEARCH.search(f.read())
        assert m is not None
        return int(m[1]), int(m[2]), int(m[3]), int(m[4])


def getLilitoolsVersion() -> Tuple[int, int, int]:
    with open("pyproject.toml", "r") as f:
        data = toml.load(f)
    return cast(
        Tuple[int, int, int],
        tuple(map(int, data["tool"]["poetry"]["version"].split("."))),
    )


class FixPom(SingleBuildTarget):
    BT_LABEL = "FIX POM"

    def __init__(
        self, inputfile: Path, outputfile: Path, dependencies: List[str] = []
    ) -> None:
        self.inputfile: Path = inputfile
        self.outputfile: Path = outputfile
        super().__init__(
            target=str(outputfile), files=[str(inputfile)], dependencies=dependencies
        )

    def __addDependency(
        self, deps: etree._Element, groupId: str, artifactId: str, version: str
    ) -> None:
        dep = etree.SubElement(deps, "dependency")
        etree.SubElement(dep, "groupId").text = groupId
        etree.SubElement(dep, "artifactId").text = artifactId
        etree.SubElement(dep, "version").text = version

    def build(self):
        NSPOM = "{http://maven.apache.org/POM/4.0.0}"
        tree = etree.parse(str(self.inputfile))
        root = tree.getroot()

        props = root.find(f"{NSPOM}properties")
        """
		<javafx.version>17</javafx.version>
		<javafx.maven.plugin.version>0.0.6</javafx.maven.plugin.version>
        """
        etree.SubElement(props, "javafx.version").text = "17"
        etree.SubElement(props, "javafx.maven.plugin.version").text = "0.0.6"

        if (deps := root.find(f"{NSPOM}dependencies")) is None:
            deps = etree.SubElement(root, f"{NSPOM}dependencies")
        self.__addDependency(deps, "org.openjfx", "javafx-controls", "17")
        self.__addDependency(deps, "org.openjfx", "javafx-fxml", "17")
        self.__addDependency(deps, "org.openjfx", "javafx-web", "17")

        tree.write(str(self.outputfile), encoding="utf-8", pretty_print=True)


DIST_DIR = Path("dist")

LILITH_DIR = Path("lib") / "liliths-throne-public"


def getDefaultNuitkaPlugins() -> Set[str]:
    from nuitka.plugins import Plugins as NuitkaPlugins
    from nuitka.plugins.PluginBase import NuitkaPluginBase

    o: Set[str] = set()
    NuitkaPlugins.loadPlugins()
    for plugin_name in sorted(NuitkaPlugins.plugin_name2plugin_classes):
        plugin: NuitkaPluginBase = NuitkaPlugins.plugin_name2plugin_classes[
            plugin_name
        ][0]
        if plugin.isAlwaysEnabled():
            o.add(plugin.plugin_name)
    # print(f'Default Nuitka plugins: {o!r}')
    return o


DEFAULT_NUITKA_PLUGIN_NAMES: Set[str] = getDefaultNuitkaPlugins()


def addLiliRunCompile(bm: BuildMaestro, dependencies: List[str] = []) -> str:
    n = NuitkaTarget(
        "lilitools/cli/lilirun/__main__.py",
        "__main__",
        ["lilitools/cli/lilirun/__main__.py"],
        nuitka_subdir="lilirun",
        dependencies=dependencies,
    )
    nt = bm.add(n)
    n.product_name = "LiliTools Runner"
    n.company_name = "LiliTools Contributors"
    ltv = getLilitoolsVersion()
    n.file_version = (ltv[0], ltv[1], ltv[2], 0)
    n.file_description = "Runs Lilith's Throne with JavaFX"
    n.trademarks = ""
    n.copyright = "Copyright (c)2023-2024 Lilitools Contributors."
    n.enabled_plugins.add("anti-bloat")
    n.enabled_plugins.add("pylint-warnings")
    n.enabled_plugins -= DEFAULT_NUITKA_PLUGIN_NAMES
    LILIRUN_EXE = (DIST_DIR / "bin" / "lilirun").with_suffix(
        ".exe" if os_utils.is_windows() else ""
    )
    return bm.add(
        CopyFileTarget(
            str(LILIRUN_EXE),
            str(n.executable_mangled),
            dependencies=[str(n.executable_mangled)],
        )
    ).target


def addSlavesheetCompile(bm: BuildMaestro, dependencies: List[str] = []) -> str:
    n = NuitkaTarget(
        "lilitools/cli/slavesheet/__main__.py",
        "__main__",
        ["lilitools/cli/slavesheet/__main__.py"],
        nuitka_subdir="slavesheet",
        dependencies=dependencies,
    )
    nt = bm.add(n)
    n.product_name = "LiliTools Slave Spreadsheet Generator"
    n.company_name = "LiliTools Contributors"
    ltv = getLilitoolsVersion()
    n.file_version = (ltv[0], ltv[1], ltv[2], 0)
    n.file_description = "Slave management tool for Lilith's Throne"
    n.trademarks = ""
    n.copyright = "Copyright (c)2023-2024 Lilitools Contributors."
    n.enabled_plugins.add("anti-bloat")
    n.enabled_plugins.add("pylint-warnings")
    n.enabled_plugins -= DEFAULT_NUITKA_PLUGIN_NAMES
    SLAVESHEET_EXE = (DIST_DIR / "bin" / "slavesheet").with_suffix(
        ".exe" if os_utils.is_windows() else ""
    )
    return bm.add(
        CopyFileTarget(
            str(SLAVESHEET_EXE),
            str(n.executable_mangled),
            dependencies=[str(n.executable_mangled)],
        )
    ).target


def addLiliToolCompile(bm: BuildMaestro, dependencies: List[str] = []) -> str:
    n = NuitkaTarget(
        "lilitools/cli/lilitool/__main__.py",
        "__main__",
        ["lilitools/cli/lilitool/__main__.py"],
        nuitka_subdir="lilitool",
        dependencies=dependencies,
    )
    nt = bm.add(n)
    n.product_name = "LiliTool"
    n.company_name = "LiliTools Contributors"
    ltv = getLilitoolsVersion()
    n.file_version = (ltv[0], ltv[1], ltv[2], 0)
    n.file_description = "Save Editor and Utilities for Lilith's Throne"
    n.trademarks = ""
    n.copyright = "Copyright (c)2023-2024 Lilitools Contributors."
    n.enabled_plugins.add("anti-bloat")
    n.enabled_plugins.add("pylint-warnings")
    n.enabled_plugins -= DEFAULT_NUITKA_PLUGIN_NAMES
    LILITOOL_EXE = (DIST_DIR / "bin" / "lilitool").with_suffix(
        ".exe" if os_utils.is_windows() else ""
    )
    # SRC_DATA_DUMPS_DIR = Path('data') / 'dumps'
    # DATA_DUMPS_DIR = DIST_DIR / 'data' / 'dumps'
    # dumps=bm.add(
    #     CopyFilesTarget(
    #         str(bm.generateVirtualTarget('cpdump')),
    #         str(SRC_DATA_DUMPS_DIR),
    #         str(DATA_DUMPS_DIR),
    #         dependencies=[str(n.executable_mangled)],
    #     )
    # ).target
    return bm.add(
        CopyFileTarget(
            str(LILITOOL_EXE),
            str(n.executable_mangled),
            dependencies=[str(n.executable_mangled)],
        )
    ).target


def addBinBuildSteps(bm: BuildMaestro, dependencies: List[str] = []) -> List[str]:
    SRC_DATA_DUMPS_DIR = Path("data") / "dumps"
    DATA_DUMPS_DIR = DIST_DIR / "data" / "dumps"
    dumps = bm.add(
        CopyFilesTarget(
            str(bm.generateVirtualTarget("cpdump")),
            str(SRC_DATA_DUMPS_DIR),
            str(DATA_DUMPS_DIR),
            dependencies=dependencies,
        )
    ).target
    return [
        addLiliRunCompile(bm, dependencies=dependencies + [dumps]),
        addSlavesheetCompile(bm, dependencies=dependencies + [dumps]),
        addLiliToolCompile(bm, dependencies=dependencies + [dumps]),
    ]


def main():
    from _utils import file2builddir

    bm = BuildMaestro(file2builddir(__file__))

    addBinBuildSteps(bm)

    bm.as_app()


if __name__ == "__main__":
    main()

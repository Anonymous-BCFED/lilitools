# import mkast.nodes.ast3_10 as fast
import ast as fast

import hashlib
import re
import sys
from pathlib import Path
from typing import Any, Callable, Dict, List, Optional, Set, Tuple
from _codegen import writeGeneratedPythonWarningHeader
import toml
from _codegen import LTExpandedCodeGenerator
from _dev_regex import REG_CONST_FIND_GAMECHARACTER_INTS
from buildtools.maestro.base_target import SingleBuildTarget
from mkast.modulefactory import ModuleFactory

from analysis.function_hash import FuncHash

LT_DIR = (Path("lib") / "liliths-throne-public").absolute()


def tryToFindSigRegexIn(
    filename: str, sig: re.Pattern | str
) -> Optional[Tuple[re.Match, str]]:
    if isinstance(sig, str):
        sig = re.compile(sig)
    with open(filename, "r") as f:
        for l in f:
            if (m := sig.search(l)) is not None:
                return m, l.strip()
    return None


class BuildConsts(SingleBuildTarget):
    BT_LABEL = "GEN"

    def __init__(
        self,
        target: str,
        dependencies: List[str] = [],
        preserve_save_ordering: bool = False,
    ) -> None:
        super().__init__(str(target), [], dependencies, [], "")
        self.mod: ModuleFactory
        self.all: fast.List
        self._set: Set[str] = set()
        self.funcHashes: List[FuncHash] = []
        self.preserve_save_ordering: bool = preserve_save_ordering

    def build(self) -> None:
        self.mod = ModuleFactory()
        self.mod.addImportFrom("typing", ["Final"])

        self.all = fast.List(elts=[])
        self.mod.expressions.append(
            fast.Assign(targets=[fast.Name(id="__all__")], value=self.all)
        )
        self._gen_primary_consts()
        self._analyze_charactermodificationutils()
        self._analyze_gamecharacter()
        self._analyze_renderingengine()
        self._define_computed_consts()
        self.all.elts.sort(key=lambda x: x.value)
        exprs = self.mod.expressions[1:]
        self.mod.expressions[1:] = sorted(
            exprs, key=lambda x: x.target.asname or x.target.name
        )
        with open(self.target, "w") as f:
            writeGeneratedPythonWarningHeader(f, "Constants for lilitools", Path(__file__))
            for fh in sorted(self.funcHashes, key=lambda x: (x.filename, x.signature)):
                f.write(str(fh))
            f.write(LTExpandedCodeGenerator().generate(self.mod.generate()))

    def addConst(self, name: str, value: Any) -> None:
        ann: fast.AST
        if isinstance(value, int):
            ann = fast.Name(id="int")
        if isinstance(value, float):
            ann = fast.Name(id="float")
        if isinstance(value, str):
            ann = fast.Name(id="str")
        if isinstance(value, bytes):
            ann = fast.Name(id="bytes")
        self.addVar(name, ann, fast.Constant(value))

    def addVar(self, name: str, ann: fast.AST, value: fast.AST) -> None:
        assert name not in self._set, f"{name} already set"
        self._set.add(name)
        self.mod.addVariableDecl(name, ann, value, final=True)
        self.all.elts.append(fast.Constant(name))

    def getConstValue(self, name: str) -> int | float | str | None:
        for expr in self.mod.expressions:
            # if isinstance(expr, fast.AnnAssign):
            #     print(repr(expr), repr(expr.target), repr(expr.value))
            if isinstance(expr, fast.AnnAssign) and isinstance(
                expr.value, fast.Constant
            ):
                if isinstance(expr.target, fast.Name):
                    k = expr.target.id
                elif isinstance(expr.target, fast.alias):
                    k = expr.target.asname or expr.target.name
                if k == name:
                    return expr.value.value

    def _gen_primary_consts(
        self,
    ) -> None:
        with open("pyproject.toml", "r") as f:
            pyproject = toml.load(f)

        self.addConst("VERSION", pyproject["tool"]["poetry"]["version"])

        self.addConst("APP_NAME", pyproject["tool"]["poetry"]["name"])

        self.addConst("PRESERVE_SAVE_ORDERING", self.preserve_save_ordering)

        self.addConst("SID_SYMBOL", "⛧")

        self.addConst("SECONDS_PER_DAY", 24 * 60 * 60)
        self.addConst("SECONDS_PER_HOUR", 60 * 60)
        self.addConst("SECONDS_PER_MINUTE", 60)
        self.addConst("HOURS_PER_DAY", 24)
        self.addConst("MINUTES_PER_HOUR", 60)

        self.addConst("MAX_CLOTHING_COLOUR_LEN", 3)

        # Handling bugs
        self.addConst("MAX_ITEMS_PER_STACK", 10)

        # self.addConst("MAX_PAGES_OF_INVENTORY", 5)
        # self.addConst("TILES_PER_INVENTORY_PAGE", 30)

    def addFuncHashOf(self, filepath: Path, sig: str) -> None:
        fh = FuncHash()
        fh.filename = str(filepath.absolute())
        fh.signature = sig
        fh.id = (
            filepath.absolute().relative_to(Path.cwd()).as_posix()
            + "_"
            + hashlib.blake2b(sig.encode(), digest_size=32).hexdigest()
        )
        fh.fix_sig()
        fh.rehash()
        fh.filename = filepath.absolute().as_posix().replace(LT_DIR.as_posix(), "[LT]")
        self.funcHashes.append(fh)

    def analyzeFileForSigs(
        self,
        path: Path,
        sigs: Dict[re.Pattern, Callable[[Path, int, str, re.Match], None]],
    ) -> None:
        with path.open("r") as f:
            for ln, line in enumerate(f):
                sline = line.strip()
                for sig, cb in sigs.items():
                    if (m := sig.search(sline)) is not None:
                        cb(path, ln, sline, m)

    def _addConstForInt(
        self, store_as: Optional[str] = None
    ) -> Callable[[Path, int, str, re.Match], None]:
        def _inner(path: Path, line_number: int, line: str, m: re.Match) -> None:
            self.addConst(store_as or m["name"], int(m["value"]))
            self.addFuncHashOf(path, line)

        return _inner

    def _analyze_gamecharacter(self) -> None:
        cf = (
            LT_DIR
            / "src"
            / "com"
            / "lilithsthrone"
            / "game"
            / "character"
            / "GameCharacter.java"
        )
        self.analyzeFileForSigs(
            cf, {REG_CONST_FIND_GAMECHARACTER_INTS: self._addConstForInt()}
        )

    def _analyze_charactermodificationutils(self) -> None:
        self.analyzeFileForSigs(
            LT_DIR
            / "src"
            / "com"
            / "lilithsthrone"
            / "game"
            / "dialogue"
            / "utils"
            / "CharacterModificationUtils.java",
            {
                re.compile(
                    r"public static final int (?P<name>(?:FLUID_INCREMENT_|FLUID_REGEN_INCREMENT_)(?:SMALL|AVERAGE|LARGE|HUGE)) = (?P<value>\d+);"
                ): self._addConstForInt()
            },
        )

    def _analyze_renderingengine(self) -> None:
        self.analyzeFileForSigs(
            LT_DIR
            / "src"
            / "com"
            / "lilithsthrone"
            / "rendering"
            / "RenderingEngine.java",
            {
                re.compile(
                    r"public static final int (?P<name>INVENTORY_PAGES) = (?P<value>\d+);"
                ): self._addConstForInt(store_as="MAX_PAGES_OF_INVENTORY"),
                re.compile(
                    r"public static final int (?P<name>ITEMS_PER_PAGE) *= *(?P<a>\d+) +(?P<op>\+|\-|/|\^|\*) +(?P<b>\d+);"
                ): self._addItemsPerPage(store_as="TILES_PER_INVENTORY_PAGE"),
            },
        )

    def _define_computed_consts(self) -> None:
        self.addConst(
            "MAX_STACKS_PER_SET",
            int(self.getConstValue("MAX_PAGES_OF_INVENTORY"))
            * int(self.getConstValue("TILES_PER_INVENTORY_PAGE")),
        )

    def _addItemsPerPage(self, store_as: str = "") -> None:
        def _a(path: Path, line_number: int, line: str, m: re.Match) -> None:
            v: int = 0
            a = int(m["a"])
            b = int(m["b"])
            op = m["op"]
            match op:
                case "+":
                    v = a + b
                case "-":
                    v = a - b
                case "/":
                    v = a / b
                case "*":
                    v = a * b
                case _:
                    raise Exception(
                        f"{path=}, {line_number=}, {line=}, {m=}, {a=}, {b=}, {op=}"
                    )
            self.addConst(store_as or m["name"], v)
            self.addFuncHashOf(path, line)

        return _a


def main():
    from _utils import file2builddir
    from buildtools.maestro import BuildMaestro

    bm = BuildMaestro(file2builddir(__file__))
    bm.add(BuildConsts(Path("lilitools") / "_consts.test.py"))
    bm.as_app()


if __name__ == "__main__":
    main()

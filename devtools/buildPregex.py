import json
import typing
from pathlib import Path
from typing import List, Tuple

from buildtools.maestro.base_target import SingleBuildTarget
from pregex.core.assertions import MatchAtLineStart, NotPrecededBy
from pregex.core.classes import Any, AnyBetween, AnyButFrom, AnyDigit, AnyFrom, AnyLetter, AnyUppercaseLetter, AnyWhitespace
from pregex.core.groups import Capture, Group
from pregex.core.operators import Either
from pregex.core.pre import Pregex
from pregex.core.quantifiers import AtLeast, Indefinite, OneOrMore, Optional

from devtools._utils import RegexConsts

# from mkast.nodes import ast3_12 as fast
# from regast.reparse import reparse_regex


JAVA_IDENT_START: typing.Optional[AnyFrom] = None
JAVA_IDENT_PART: typing.Optional[AnyFrom] = None


def getOrCreateJavaLetterDigits() -> None:
    global JAVA_IDENT_PART, JAVA_IDENT_START

    # ULETTERS: Set[str] = set()
    # UDIGITS: Set[str] = set()
    # UCURRENCY: Set[str] = set()
    # with tqdm.tqdm(range(0x10FFFF), 'Gathering unicode data...') as p:
    #     def updpostfix() -> None:
    #         p.postfix = {'l': len(ULETTERS), 'n': len(UDIGITS), 'c': len(UCURRENCY)}
    #     for i in p:
    #         c = chr(i)
    #         cat = unicodedata.category(c)
    #         match cat:
    #             case 'Ll', 'Lm', 'Lo', 'Lt', 'Lu':
    #                 ULETTERS.add(c)
    #                 updpostfix()
    #             case 'Nd' | 'Nl' | 'No':
    #                 UDIGITS.add(c)
    #                 updpostfix()
    #             case 'Sc':
    #                 UCURRENCY.add(c)
    #                 updpostfix()
    # print('Collecting JAVA_LETTERS...')
    # JAVA_LETTERS = AnyFrom(*(''.join(sorted(ULETTERS | UCURRENCY)) + '_'))
    # print('Collecting JAVA_LETTER_DIGITS...')
    # JAVA_LETTER_DIGITS = AnyFrom(*(''.join(sorted(ULETTERS | UDIGITS | UCURRENCY)) + '_'))
    chardata: dict
    with open("devtools/java/charinfo/app/charinfo.json", "r") as f:
        chardata = json.load(f)

    def list2precls(l: List[Tuple[int, int]]) -> AnyBetween:
        import re

        # cls: Optional[AnyBetween] = None
        s = ""
        for r in l:
            start, end = r
            if end - start == 0:
                s += re.escape(chr(start))
                # if cls is None:
                #     cls = AnyFrom(chr(start))
                # else:
                #     cls |= chr(start)
            else:
                s += f"{re.escape(chr(start))}-{re.escape(chr(end))}"
                # if cls is None:
                #     cls = AnyBetween(chr(start), chr(end))
                # else:
                #     cls |= AnyBetween(chr(start), chr(end))
        # print(s)
        return Pregex(f"[{s}]", False)

    print("Collecting JAVA_IDENT_START...")
    JAVA_IDENT_START = list2precls(chardata["start"])
    print("Collecting JAVA_IDENT_PART...")
    JAVA_IDENT_PART = list2precls(chardata["part"])


class BuildGameRegexPy(SingleBuildTarget):
    BT_LABEL = "PREGEX"

    def __init__(self, targetfile: Path) -> None:
        self.targetpath = targetfile
        super().__init__(target=str(self.targetpath), files=[])

    def build(self) -> None:
        r = RegexConsts()
        # REG_GLUON_CUSTOMJS = re.compile(r'//gluonhq\.com/wp\-content/uploads/custom\-css\-js/\d+\.js\?v=\d+')
        r.addExpr(
            "REG_GLUON_CUSTOMJS",
            Pregex("//gluonhq.com/wp-content/uploads/custom-css-js/")
            + OneOrMore(AnyDigit())
            + ".js?v="
            + OneOrMore(AnyDigit()),
        )
        # REG_VERSIONS = re.compile(r'new Version\((.+)\)')
        r.addExpr(
            "REG_GLUON_VERSIONS",
            Pregex("new Version(") + Capture(OneOrMore(Any())) + ")",
        )
        # VersionType.LTS
        r.addExpr(
            "REG_GLUON_VERSIONTYPE",
            Pregex("VersionType.") + Capture(OneOrMore(AnyUppercaseLetter() | "_")),
        )

        r.addExpr("REG_NUMBERS", Capture(OneOrMore(AnyDigit())))

        # REMEMBER TO UPDATE DOCUMENTATION IF THESE ARE CHANGED
        # docs-src/docs/commands/slavesheet/serve.src.md
        # Training Room 1
        # Training Room D-1
        # Training Room A-B101 <- current
        # {purpose} {building}-{floor}{room:02d}
        r.addExpr(
            "REG_TRAINING_ROOM",
            Pregex("Training")
            + OneOrMore(" ")
            + Either(AnyFrom("R", "r") + "oom", AnyFrom("O", "o") + "verflow")
            + OneOrMore(" ")
            + Capture(OneOrMore(AnyUppercaseLetter() | AnyDigit()))
            + Optional(Pregex("-"))
            + Optional(Capture(AnyUppercaseLetter() | AnyDigit())),
        )
        r.addExpr(
            "REG_RECOVERY_ROOM",
            Pregex("Recovery")
            + OneOrMore(" ")
            + Either(AnyFrom("R", "r") + "oom", AnyFrom("O", "o") + "verflow")
            + OneOrMore(" ")
            + Capture(OneOrMore(AnyUppercaseLetter() | AnyDigit()))
            + Optional(Pregex("-"))
            + Optional(Capture(AnyUppercaseLetter() | AnyDigit())),
        )

        r.addExpr(
            "REG_COW_STALL",
            Pregex("Cow")
            + OneOrMore(" ")
            + Either(
                (AnyFrom("S", "s") + "tall"),
                (AnyFrom("R", "r") + "oom"),
                (AnyFrom("P", "p") + "en"),
            )
            + OneOrMore(" ")
            + Capture(OneOrMore(AnyUppercaseLetter() | AnyDigit()))
            + Optional(Pregex("-"))
            + Optional(Capture(AnyUppercaseLetter() | AnyDigit())),
        )

        FLOOR_CODE = "floorcode"
        ROOM_ID = "roomid"
        r.addExpr(
            "REG_VALID_ROOMS",
            Either(
                # Garden Room FG-##
                # Garden Room GG-##
                Pregex("Garden Room ")
                + Capture(AnyUppercaseLetter())
                + Pregex("G-")
                + Capture(OneOrMore(AnyDigit())),
                # Room G-13
                # Room F-13
                Pregex("Room ")
                + Capture(AnyUppercaseLetter())
                + Pregex("-")
                + Capture(OneOrMore(AnyDigit())),
            ),
        )

        # if path.name.startswith("LilithsThrone") and path.suffix == ".jar":
        r.addExpr(
            "REG_LT_JARS",
            Pregex("LilithsThrone")
            + "_"
            + Capture(name="major", pre=OneOrMore(AnyDigit()))
            + "_"
            + Capture(name="minor", pre=OneOrMore(AnyDigit()))
            + "_"
            + Capture(name="patch", pre=OneOrMore(AnyDigit()))
            + Optional("_" + Capture(name="revision", pre=OneOrMore(AnyDigit())))
            + Pregex(".jar"),
        )
        r.to_file(self.targetpath)


class BuildDevRegexPy(SingleBuildTarget):
    BT_LABEL = "PREGEX"

    def __init__(self, targetfile: Path) -> None:
        self.targetpath = targetfile
        super().__init__(target=str(self.targetpath), files=[])

    def build(self) -> None:
        r = RegexConsts()
        # RE_VERSION_SEARCH = re.compile(r'public static final String VERSION_NUMBER = "([^"]+)";')
        r.addExpr(
            "REG_VERSION_SEARCH",
            Pregex('public static final String VERSION_NUMBER = "')
            + Capture(OneOrMore(AnyDigit()))
            + "."
            + Capture(OneOrMore(AnyDigit()))
            + "."
            + Capture(OneOrMore(AnyDigit()))
            + Optional(Pregex(".") + Capture(OneOrMore(AnyDigit())))
            + Pregex('";'),
        )
        # RE_VERSION_SEARCH = re.compile(r'public static final String VERSION_NUMBER = "([^"]+)";')
        r.addExpr(
            "REG_VERSION_STR",
            Pregex('public static final String VERSION_NUMBER = "')
            + Capture(OneOrMore(AnyButFrom('"')))
            + Pregex('";'),
        )

        # REG_SIGREF = re.compile(r'^(?P<indent>[ \t]*)## from (?P<path>[^:]+): ?(?P<signature>[^@]+) ?(@ (?P<hash>[A-Za-z0-9/\+=]+))?$')
        r.addExpr(
            "REG_SIGREF",
            MatchAtLineStart(
                Capture(Indefinite(AnyWhitespace()), "indent")
                + "##"
                + Indefinite(AnyWhitespace())
                + "from "
                + Capture(OneOrMore(AnyButFrom(":")), "path")
                + ":"
                + Indefinite(AnyWhitespace())
                + Capture(OneOrMore(AnyButFrom("@")), "signature")
                + Optional(
                    Indefinite(AnyWhitespace())
                    + "@"
                    + Indefinite(AnyWhitespace())
                    + Capture(
                        OneOrMore(AnyLetter() | AnyDigit() | "/" | "+" | "="), "hash"
                    )
                )
            ),
        )

        # REG_FLOAT = re.compile(r'(\-?\d+(\.\d+)?)f?')
        r.addExpr(
            "REG_JAVA_FLOAT",
            Capture(
                Optional("-")
                + OneOrMore(AnyDigit())
                + Optional("." + OneOrMore(AnyDigit()))
            )
            + Optional("f"),
        )
        # REG_INT = re.compile(r'(\-?[0-9_]+)')
        r.addExpr(
            "REG_JAVA_INT",
            Capture(
                Optional("-")
                + Pregex("[0-9]", False)
                + Indefinite(Pregex("[0-9_]", False))
            ),
        )
        r.addExpr("REG_JAVA_BOOL", Capture(Either("true", "false")))
        # ALL_CLOTHING = re.compile(r'CLOTHING_[A-Z_]+')
        r.addExpr(
            "REG_ALL_CLOTHING",
            Pregex("CLOTHING_")
            + AnyUppercaseLetter()
            + OneOrMore(AnyUppercaseLetter() | "_"),
        )
        r.addExpr(
            "REG_ENUM",
            Pregex("public")
            + OneOrMore(AnyWhitespace())
            + "enum"
            + OneOrMore(AnyWhitespace())
            + Capture(AnyLetter() + Indefinite(AnyLetter() | AnyDigit() | "_")),
        )

        getOrCreateJavaLetterDigits()

        # r'([A-Z0-9][a-z]*)'
        # https://docs.oracle.com/javase/specs/jls/se7/html/jls-3.html#jls-3.8
        r.addExpr(
            "REG_JAVA_IDENT", Capture(JAVA_IDENT_START + Indefinite(JAVA_IDENT_PART))
        )

        # signature = re.sub(r'[\t ]{2,}', ' ', signature)
        r.addExpr("REG_INDENT", AtLeast(Capture(AnyFrom("\t", " ")), n=2))
        r.addExpr(
            "REG_FETISH_GETIDFROMFETISH",
            Pregex("Fetish.getIdFromFetish(")
            + Capture(JAVA_IDENT_START + Indefinite(JAVA_IDENT_PART))
            + ")",
        )
        r.addExpr(
            "REG_JAVA_CATCH_EXCEPTION_E",
            Capture(pre=Indefinite(AnyFrom(" ", "\t")), name="whitespace")
            + "} catch(Exception e) {",
        )
        r.addExpr(
            "REG_JAVA_PUBLIC_STATIC_LIST_ABSTRACTPERK",
            Capture(pre=Indefinite(AnyFrom(" ", "\t")), name="whitespace")
            + "public static List<AbstractPerk> "
            + Capture(
                pre=Either("hiddenPerks", "allPerks", "subspeciesKnowledgePerks"),
                name="name",
            )
            + ";",
        )

        #  4260c19d92573aa0d34f787324a08807969897c7 lib/liliths-throne-public (remotes/origin/HEAD)
        r.addExpr(
            "REG_SUBMODULE_STATUS",
            Capture(
                pre=OneOrMore(
                    AnyBetween("a", "f") | AnyBetween("A", "F") | AnyBetween("0", "9")
                ),
                name="commit",
            )
            + OneOrMore(AnyWhitespace())
            + Capture(pre=OneOrMore(~AnyWhitespace()), name="path")
            + OneOrMore(AnyWhitespace())
            + "("
            + Capture(
                pre=OneOrMore(AnyLetter() | AnyDigit() | "_" | "/" | "-"), name="ref"
            )
            + ")",
        )

        r.addExpr(
            "REG_DOCS_CMD_CALL",
            NotPrecededBy(
                Pregex("<@cmd")
                + Capture(Pregex("{") + OneOrMore(Any()) + "}", name="json")
                + "@>",
                "`",
            ),
        )

        r.addExpr(
            "REG_DOCS_DATA",
            NotPrecededBy(
                Pregex("<@data")
                + OneOrMore(AnyWhitespace())
                + Capture(
                    OneOrMore(
                        AnyBetween("A", "Z")
                        | AnyBetween("a", "z")
                        | AnyBetween("0", "9")
                        | AnyFrom("_")
                    ),
                    name="command",
                )
                + OneOrMore(AnyWhitespace())
                + Capture(Pregex("{") + Indefinite(Any()) + "}", name="json")
                + Indefinite(AnyWhitespace())
                + Pregex("@>"),
                Pregex("`"),
            ),
        )

        r.addExpr(
            "REG_UTIL_GETEXTERNALMODFILESBYID",
            Pregex('Util.getExternalModFilesById("')
            + Capture(OneOrMore(AnyButFrom('"')))
            + Pregex('")'),
        )

        r.addExpr(
            "REG_UTIL_GETEXTERNALMODFILESBYID",
            Pregex('Util.getExternalModFilesById("')
            + Capture(OneOrMore(AnyButFrom('"')))
            + Pregex('")'),
        )
        r.addExpr(
            "REG_CONST_FIND_GAMECHARACTER_INTS",
            Pregex("public static final int ")
            + Capture(
                name="name",
                pre=Either(
                    "LEVEL_CAP", "MAX_TRAITS", "MAX_COMBAT_MOVES", "DEFAULT_COMBAT_AP"
                ),
            )
            + " = "
            + Capture(name="value", pre=OneOrMore(AnyDigit()))
            + ";",
        )
        r.addExpr(
            "REG_CONST_FIND_CHARACTERMODIFICATIONUTILS_INTS",
            Pregex("public static final int ")
            + Capture(
                name="name",
                pre=Group(
                    Either(
                        Pregex("FLUID_INCREMENT_"),
                        Pregex("FLUID_REGEN_INCREMENT_"),
                    )
                )
                + Group(
                    Either(
                        Pregex("SMALL"),
                        Pregex("AVERAGE"),
                        Pregex("LARGE"),
                        Pregex("HUGE"),
                    )
                ),
            )
            + " = "
            + Capture(name="value", pre=OneOrMore(AnyDigit()))
            + ";",
        )

        r.to_file(self.targetpath)


def main():
    from buildtools.maestro import BuildMaestro

    from devtools._utils import file2builddir

    bm = BuildMaestro(file2builddir(__file__))
    bm.add(BuildDevRegexPy(Path("devtools") / "_dev_regex.py"))
    bm.as_app()


if __name__ == "__main__":
    main()

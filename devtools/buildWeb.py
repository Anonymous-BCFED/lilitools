import os
from pathlib import Path
from typing import Dict, List, Tuple

import human_readable
import jinja2
from buildtools import log, os_utils
from buildtools.maestro import BuildMaestro
from buildtools.maestro.base_target import SingleBuildTarget
from buildtools.maestro.enumwriters.coffee import CoffeeEnumWriter
from buildtools.maestro.fileio import CopyFilesTarget
from buildtools.maestro.genenum import GenerateEnumTarget
from buildtools.maestro.jinja2 import Jinja2BuildTarget
from buildtools.maestro.package_managers import YarnBuildTarget
from buildtools.maestro.shell import CommandBuildTarget
from buildtools.maestro.web import DartSCSSBuildTarget

from enumwriters.typescript import TypeScriptEnumWriter

WEB_DIR = Path('web')
NODE_MODULES_DIR = WEB_DIR / 'node_modules'
BSICON_PATH = NODE_MODULES_DIR / 'bootstrap-icons'
BSICONS_PATH = BSICON_PATH / 'icons'
COFFEE_DIR = WEB_DIR / 'src' / 'coffee'
TYPESCRIPT_DIR = WEB_DIR / 'src' / 'typescript'
ENUM_YML_DIR = Path('data') / 'enums'
# BSICON_COFFEE = COFFEE_DIR / '_GEN_bsicons.coffee'
BSICON_TS = TYPESCRIPT_DIR / '_GEN_bsicons.ts'

DIST_DIR = Path('dist')

COFFEE_ENUMS: List[Tuple[Path, Path, Path]] = [
    (
        ENUM_YML_DIR / 'slavesheet' / 'plans.yml',
        COFFEE_DIR / 'enums' / 'plans.coffee',
        TYPESCRIPT_DIR / 'enums' / 'plans.ts',
    ),
    (
        ENUM_YML_DIR / 'slavesheet' / 'status.yml',
        COFFEE_DIR / 'enums' / 'status.coffee',
        TYPESCRIPT_DIR / 'enums' / 'status.ts',
    ),
    (
        ENUM_YML_DIR / 'slavesheet' / 'treatment.yml',
        COFFEE_DIR / 'enums' / 'treatment.coffee',
        TYPESCRIPT_DIR / 'enums' / 'treatment.ts',
    ),
    (
        ENUM_YML_DIR / 'slavesheet' / 'roompurpose.yml',
        COFFEE_DIR / 'enums' / 'roompurpose.coffee',
        TYPESCRIPT_DIR / 'enums' / 'roompurpose.ts',
    ),
]


class BaseBuildBSIcons(SingleBuildTarget):
    BT_LABEL = 'GENERATE'

    def __init__(self, target: Path, dependencies: List[str]):
        self.fileData: List[Tuple[str, str, str]] = []
        self.outfile: Path = target
        super().__init__(target=str(target), files=self.scanForIconFiles(), dependencies=dependencies)

    def scanForIconFiles(self) -> List[str]:
        files: List[Tuple[str, str, str]] = []
        #oid = 0
        path: Path
        for path in BSICONS_PATH.glob('*.svg'):
            #oid = f'bsIcon{oid:04X}'
            oid = 'BS_ICON_' + path.stem.upper().replace('-', '_')
            files += [(path.stem, str(path.relative_to(NODE_MODULES_DIR)), oid)]
            #oid += 1
        self.fileData = files
        return sorted(list(map(lambda x: str(x[0]), files)))


# class BuildBSIcons4CoffeeScript(BaseBuildBSIcons):
#     def build(self) -> None:
#         print(f'Found {len(self.fileData):,} icons')
#         with self.outfile.open('w') as f:
#             f.write('# @GENERATED\n')
#             for stem, path, oid in sorted(self.fileData, key=lambda x: x[0]):
#                 f.write(f'import {oid} from {path!r}\n')
#             f.write('export BOOTSTRAP_ICONS =\n')
#             for stem, path, oid in sorted(self.fileData, key=lambda x: x[0]):
#                 f.write(f'    "{stem}": {oid}\n')
#             f.write("export {\n")
#             for stem, path, oid in sorted(self.fileData, key=lambda x: x[0]):
#                 f.write(f'    {oid},\n')
#             f.write('}\n')
#         hrfs = human_readable.file_size(os.path.getsize(self.outfile))
#         print(f'Emitted {hrfs} to {self.outfile}')


class BuildBSIcons4TypeScript(BaseBuildBSIcons):
    def build(self) -> None:
        with self.outfile.open('w') as f:
            f.write('// @GENERATED\n')
            for stem, path, oid in sorted(self.fileData, key=lambda x: x[0]):
                #f.write(f'import {oid} from "{path}";\n')
                f.write(f'const {oid}: string = require("{path}");\n')
            f.write('export const BOOTSTRAP_ICONS: Record<string, string> = {\n')
            for stem, path, oid in sorted(self.fileData, key=lambda x: x[0]):
                f.write(f'    "{stem}": {oid},\n')
            f.write("};\n")
            f.write("export {\n")
            for stem, path, oid in sorted(self.fileData, key=lambda x: x[0]):
                f.write(f'    {oid},\n')
            f.write('};\n')
        hrfs = human_readable.file_size(os.path.getsize(self.outfile))
        print(f'Emitted {hrfs} to {self.outfile}')


def addWebBuildSteps(bm: BuildMaestro, YARN: str, dependencies=[], devmode: bool = True) -> str:
    jenv = jinja2.Environment(
        loader=jinja2.FileSystemLoader('.')
    )

    yarn = bm.add(YarnBuildTarget(yarn_path=str(YARN), working_dir=str(WEB_DIR), dependencies=dependencies))
    # bsicons_coffee = bm.add(BuildBSIcons4CoffeeScript(BSICON_COFFEE, dependencies=[yarn.target]))
    bsicons_ts = bm.add(BuildBSIcons4TypeScript(BSICON_TS, dependencies=[yarn.target]))
    SASS = NODE_MODULES_DIR / '.bin' / 'sass'
    loadingcss = bm.add(DartSCSSBuildTarget('tmp/loading.css', [str(WEB_DIR / 'src' / 'scss' / 'loading.scss')], dependencies=[yarn.target], sass_path=SASS))
    indexhtml = bm.add(Jinja2BuildTarget(str(WEB_DIR / 'src' / 'index.jinja'), str(WEB_DIR / 'src' / 'index.html'), jenv, {
        'css': {
            'loading': lambda: Path(loadingcss.target).read_text()
        }
    }, dependencies=[loadingcss.target]))
    # cw = CoffeeEnumWriter()
    tsw = TypeScriptEnumWriter()
    enums = []
    # for infile, outfile, _ in COFFEE_ENUMS:
    #     enums.append(bm.add(GenerateEnumTarget(str(outfile), str(infile), cw, dependencies=[yarn.target])).target)
    for infile, _, outfile in COFFEE_ENUMS:
        enums.append(bm.add(GenerateEnumTarget(str(outfile), str(infile), tsw, dependencies=[yarn.target])).target)
    files = []
    for glob in ('*.scss', '*.html', '*.svg', '*.jinja', '*.ts'):
        files.extend([str(x.absolute()) for x in (WEB_DIR / 'src').rglob(glob)])
    files = list(set(sorted(files)))
    webpack = bm.add(CommandBuildTarget(
        bm,
        outfiles=[
            str(WEB_DIR / 'dist' / 'index.html')
        ],
        infiles=files,
        cmd=[str(NODE_MODULES_DIR.relative_to(WEB_DIR) / '.bin' / 'webpack'),
             '--mode', 'development' if devmode else 'production',
             ],
        show_output=True,
        cwd=str(WEB_DIR),
        dependencies=enums + [str(bsicons_ts.target), indexhtml.target])).provides()[0]

    return bm.add(CopyFilesTarget(target=str(Path(bm.builddir) / '.deploy-web'), source=str(WEB_DIR / 'static'), destination=str(DIST_DIR / 'web' / 'static'), dependencies=[webpack])).target


def main():
    import argparse
    from _utils import file2builddir

    bm = BuildMaestro(file2builddir(__file__))
    argp: argparse.ArgumentParser = bm.build_argparser()
    argp.add_argument('--debug', action='store_true', default=False)
    args = bm.parse_args(argp)
    addWebBuildSteps(bm, os_utils.assertWhich('yarn'), dependencies=[], devmode=args.debug)
    bm.as_app(argp)


if __name__ == '__main__':
    main()

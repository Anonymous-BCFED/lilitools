# This script checks methods that start with '## from {filename}: {signature} [@HASH]'.
# If hash is not present, it adds it.

import hashlib
import os
import re
import shutil
import sys
from binascii import b2a_base64
from pathlib import Path
from typing import List, Optional

from analysis.function_hash import getFunctionBySig
from buildtools import log, os_utils

from _dev_regex import REG_SIGREF

log.enableANSIColors()

LT_DIR = Path('lib')/'liliths-throne-public'


EXCLUDED_FILES = [
    Path(__file__).absolute()
]

def _fixPathWith(envID: str, rootpath: str, path: str) -> str:
    prefix = f'[{envID}]'
    if path.startswith(prefix):
        subpath = path[len(prefix):]
        if subpath.startswith('/'):
            subpath = subpath[1:]
        path = os.path.join(rootpath, subpath)
    return path


def _unfixPathWith(envID: str, rootpath: str, path: str) -> str:
    prefix = f'[{envID}]'
    if path.startswith(rootpath):
        subpath = path[len(rootpath):]
        if subpath.startswith('/'):
            subpath = subpath[1:]
        path = os.path.join(prefix, subpath)
    return path


def parsePythonFile(filename: str, check_only=False, rehash=False) -> None:
    changes: int = 0
    errors: List[str] = []
    ln: int = 0
    with log.info(f'{filename}:'):
        with open(filename, 'r') as f:
            with open(filename + '.tmp', 'w') as w:
                for line in f:
                    ln += 1
                    m = REG_SIGREF.match(line.rstrip())
                    if m is None:
                        w.write(line)
                    else:
                        indent = m.group('indent')
                        path = m.group('path').strip()
                        signature = m.group('signature').strip()
                        knownhash = m.group('hash')

                        origpath = path

                        path = _fixPathWith('LT', str(LT_DIR), path)

                        fixedopath = _unfixPathWith('LT', str(LT_DIR), path)

                        logprefix = f'{origpath}: {signature}'

                        actualhash = ''
                        if not os.path.isfile(path):
                            with log.error(f'{logprefix}: <red>FAILED</red>'):
                                errors += [f'{logprefix}: Specified file {origpath!r} ({path!r}) is missing.']
                                log.error(f'Specified file {origpath!r} ({path!r}) is missing.')
                                w.write(f'{indent}## from {fixedopath}: {signature} @ !!!FILE MISSING!!!\n')
                                changes += 1
                                #errors += 1
                                continue

                        funcdata = getFunctionBySig(path, signature)
                        # print(repr(funcdata))
                        funcdata = funcdata.encode() if funcdata is not None else None #'utf-8')
                        #print(repr(funcdata))
                        if funcdata is None:
                            with log.error(f'{logprefix}: <red>FAILED</red>'):
                                errors += [f'{logprefix}: Unable to find that signature in {path!r}.']
                                log.error(f'Unable to find that signature in %r.  Copy the *entire line* and try again.', path)
                                w.write(f'{indent}## from {fixedopath}: {signature} @ !!!FUNC MISSING!!!\n')
                                changes += 1
                                #errors += 1
                                continue

                        digest = hashlib.sha512(funcdata).digest()
                        #hexhash = binascii.b2a_hex(digest).decode('ascii')
                        #print(repr(hexhash), len(hexhash))
                        actualhash = b2a_base64(digest).decode('utf-8')
                        #print(repr(actualhash), len(actualhash))
                        
                        knownhash = knownhash.strip() if knownhash else None
                        actualhash = actualhash.strip()

                        if rehash or knownhash is None:
                            w.write(f'{indent}## from {fixedopath}: {signature} @ {actualhash}\n')
                            if not check_only:
                                changes += 1
                                word = 'RE-hashed!' if rehash else 'Hashed!'
                                log.info(f'{logprefix}: <cyan>{word}</cyan>')
                            else:
                                with log.warning(f'{logprefix}: <yellow>NO HASH</yellow>'):
                                    log.warning('Run without --check-only to fix.')
                        else:
                            if actualhash == knownhash:
                                log.info(f'{logprefix}: <green>OK</green>')
                                w.write(f'{indent}## from {fixedopath}: {signature} @ {actualhash}\n')
                            else:
                                with log.error(f'{logprefix}: <red>FAILED</red>'):
                                    log.error('Known hash:  %s', knownhash)
                                    log.error('Actual hash: %s', actualhash)
                                errors += [f'{logprefix}: Hash mismatch!']
        if check_only:
            os.remove(filename + '.tmp')
            if len(errors) > 0:
                with log.error('%d errors.', len(errors)):
                    for e in errors:
                        log.error(e)
                sys.exit(1)
            return
        log.info('%d changes', changes)
        if len(errors) > 0:
            with log.error('%d errors, aborting overwrite.', len(errors)):
                for e in errors:
                    log.error(e)
            os.remove(filename + '.tmp')
            sys.exit(1)
        else:
            os.remove(filename)
            shutil.move(filename + '.tmp', filename)


def main():
    import argparse

    argp = argparse.ArgumentParser()
    argp.add_argument('--check-only', action='store_true', default=False, help='Only check if hashes are correct.  Do not overwrite.')
    argp.add_argument('--rehash', action='store_true', default=False, help='Overwrite all hashes with their new values.')
    args = argp.parse_args()

    targets = []
    for subdir in [Path('devtools'),Path('lilitools')]:
        for pyfile in subdir.rglob('*'):
            if pyfile.absolute() in EXCLUDED_FILES:
                continue
            ext = ''.join(pyfile.suffixes)
            #print(ext)
            if ext in {'.py', '.py.in'}:
                with open(pyfile, 'r') as f:
                    if '## from ' in f.read():
                        targets += [str(pyfile)]
    for pyfile in targets:
        parsePythonFile(pyfile, check_only=args.check_only, rehash=args.rehash)
    else:
        print('No targets found.')


if __name__ == '__main__':
    main()

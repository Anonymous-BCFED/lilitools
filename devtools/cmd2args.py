import argparse
import json


def main() -> None:
    argp = argparse.ArgumentParser('cmd2args',description='For converting commands to <@cmd{}@> tokens.')
    argp.add_argument("cmd", type=str, nargs="+")
    args = argp.parse_args()
    j: dict = {"args": args.cmd}
    jstr = json.dumps(j, indent=None, separators=(",", ":"))
    print(f"<@cmd{jstr}@>")


if __name__ == "__main__":
    main()

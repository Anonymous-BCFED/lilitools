import ast
from pathlib import Path
from typing import Dict, List, Optional, Set, Tuple
from ruamel.yaml import YAML as Yaml
import tqdm
from mkast.modulefactory import ModuleFactory
from mkast.nodes import ast3_10 as fast
from analysis.function_hash import FuncHash
from _codegen import LTExpandedCodeGenerator, LTMinifiableCodeGenerator

DATA_DIR = Path("data")
SAVABLES_DIR = DATA_DIR / "savables"
MODFILES_DIR = SAVABLES_DIR / "modfiles"
LT_DIR = Path("lib") / "liliths-throne-public"
LT_PKG_DIR = LT_DIR / "src" / "com" / "lilithsthrone"

YAML = Yaml(typ="rt")


def path2ast(path: Path, prefix: Optional[str] = None) -> ast.AST:
    o: List[ast.AST] = []
    if prefix is not None:
        o.append(ast.Name(id=prefix, ctx=ast.Load()))
    # p:Optional[Path] = None
    for chunk in reversed(path.parents):
        if chunk.name != "":
            o.append(ast.Constant(chunk.name, kind=None))
    o.append(ast.Constant(path.name, kind=None))
    if isinstance(o[0], ast.Constant):
        o[0] = ast.Call(func=ast.Name("Path"), args=[o[0]], keywords=[])
    # from mkast.nodes import ast3_10 as mast
    bop: ast.AST = ast.BinOp(left=o[0], op=ast.Div(), right=o[1])
    for c in o[2:]:
        bop = ast.BinOp(left=bop, op=ast.Div(), right=c)
    return bop


def fsrc(fh: FuncHash) -> Tuple[str, str]:
    """
    Translate data dictionary to a tuple.
    """
    return fh.filename, fh.id


def _build_rebuildFH(SAVABLES: Dict[Tuple[str, str], Tuple[str, Set[Path]]]) -> None:
    mf = ModuleFactory()
    mf.addImportFrom("pathlib", ["Path"])
    # from addFuncHashToSavable import addFuncHashToSavables
    mf.addImportFrom("addFuncHashToSavable", ["addFuncHashToSavables"])
    mf.addVariableDecl(
        "DATA_DIR",
        None,
        ast.Call(
            func=ast.Name(id="Path", ctx=ast.Load()),
            args=[ast.Constant(value="data")],
            keywords=[],
        ),
    )
    mf.addVariableDecl(
        "SAVABLES_DIR",
        None,
        ast.BinOp(
            left=ast.Name(id="DATA_DIR", ctx=ast.Load()),
            op=ast.Div(),
            right=ast.Constant(value="savables"),
        ),
    )
    mf.addVariableDecl(
        "MODFILES_DIR",
        None,
        ast.BinOp(
            left=ast.Name(id="SAVABLES_DIR", ctx=ast.Load()),
            op=ast.Div(),
            right=ast.Constant(value="modfiles"),
        ),
    )
    mf.addVariableDecl(
        "LT_DIR",
        None,
        ast.BinOp(
            left=ast.Call(
                func=ast.Name(id="Path", ctx=ast.Load()),
                args=[ast.Constant(value="lib")],
                keywords=[],
            ),
            op=ast.Div(),
            right=ast.Constant(value="liliths-throne-public"),
        ),
    )
    mf.addVariableDecl(
        "LT_PKG_DIR",
        None,
        path2ast(LT_PKG_DIR.relative_to(LT_DIR), "LT_DIR"),
        final=True,
    )

    mainf = ast.FunctionDef(
        name="main",
        args=ast.arguments(
            posonlyargs=[],
            args=[],
            vararg=None,
            kwonlyargs=[],
            kw_defaults=[],
            kwarg=None,
            defaults=[],
        ),
        body=[],
        decorator_list=[],
        returns=None,
    )
    mf.expressions.append(mainf)

    for (filename, id), (sig, paths) in SAVABLES.items():
        # print(repr(sig))
        mainf.body.append(
            ast.Expr(
                value=ast.Call(
                    func=ast.Name(id="addFuncHashToSavables", ctx=ast.Load()),
                    args=[
                        ast.Set(
                            elts=[
                                path2ast(x.relative_to(SAVABLES_DIR), "SAVABLES_DIR")
                                for x in sorted(paths)
                            ]
                        ),
                        path2ast(
                            Path(filename.replace("[LT]", str(LT_DIR))).relative_to(
                                LT_PKG_DIR
                            ),
                            "LT_PKG_DIR",
                        ),
                        ast.Constant(value=id),
                    ],
                    keywords=[
                        ast.keyword(
                            arg=ast.Name(id="signature", ctx=ast.Load()),
                            value=ast.Constant(value=sig),
                        )
                    ],
                )
            )
        )
    mf.expressions.append(
        ast.If(
            test=ast.BinOp(
                left=ast.Name(id="__name__", ctx=ast.Load()),
                op=ast.Eq(),
                right=ast.Constant(value="__main__"),
            ),
            body=[ast.Call(func=ast.Name(id="main"), args=[], keywords=[])],
            orelse=[],
        )
    )
    with open("devtools/rebuildFuncHashes.recc.py", "w") as f:
        f.write(LTMinifiableCodeGenerator().generate(mf.generate()))


def _build_diffFH(SAVABLES: Dict[Tuple[str, str], Tuple[str, Set[Path]]]) -> None:
    mf = ModuleFactory()
    mf.addImport(["argparse"])
    mf.addImport(["os"])
    mf.addImportFrom("pathlib", ["Path"])
    # from addFuncHashToSavable import addFuncHashToSavables
    mf.addImportFrom("subprocess", ["check_call"])
    mf.addVariableDecl(
        "DATA_DIR",
        None,
        ast.Call(
            func=ast.Name(id="Path", ctx=ast.Load()),
            args=[ast.Constant(value="data")],
            keywords=[],
        ),
    )
    mf.addVariableDecl(
        "SAVABLES_DIR",
        None,
        ast.BinOp(
            left=ast.Name(id="DATA_DIR", ctx=ast.Load()),
            op=ast.Div(),
            right=ast.Constant(value="savables"),
        ),
    )
    mf.addVariableDecl(
        "MODFILES_DIR",
        None,
        ast.BinOp(
            left=ast.Name(id="SAVABLES_DIR", ctx=ast.Load()),
            op=ast.Div(),
            right=ast.Constant(value="modfiles"),
        ),
    )
    mf.addVariableDecl(
        "LT_DIR",
        None,
        ast.BinOp(
            left=ast.Call(
                func=ast.Name(id="Path", ctx=ast.Load()),
                args=[ast.Constant(value="lib")],
                keywords=[],
            ),
            op=ast.Div(),
            right=ast.Constant(value="liliths-throne-public"),
        ),
        final=True,
    )
    mf.addVariableDecl(
        "LT_PKG_DIR",
        None,
        ast.Call(
            func=ast.Attribute(
                value=path2ast(LT_PKG_DIR.relative_to(LT_DIR), "LT_DIR"),
                attr="absolute",
            ),
            args=[],
            keywords=[],
        ),
        final=True,
    )

    mainf = ast.FunctionDef(
        name="main",
        args=ast.arguments(
            posonlyargs=[],
            args=[],
            vararg=None,
            kwonlyargs=[],
            kw_defaults=[],
            kwarg=None,
            defaults=[],
        ),
        body=[],
        decorator_list=[],
        returns=None,
    )
    mf.expressions.append(mainf)
    mainf.body.append(
        ast.Assign(
            targets=[ast.Name(id="argp", ctx=ast.Store())],
            value=ast.Call(
                func=ast.Attribute(
                    value=ast.Name(id="argparse", ctx=ast.Load()), attr="ArgumentParser"
                ),
                args=[],
                keywords=[],
            ),
        )
    )
    mainf.body.append(
        ast.Expr(
            value=ast.Call(
                func=ast.Attribute(
                    value=ast.Name(id="argp"), attr="add_argument", ctx=ast.Load()
                ),
                args=[
                    ast.Constant("commit"),
                ],
                keywords=[
                    ast.keyword(arg="type", value=ast.Name("str")),
                ],
            )
        )
    )
    mainf.body.append(
        ast.Assign(
            targets=[ast.Name(id="args", ctx=ast.Store())],
            value=ast.Call(
                func=ast.Attribute(
                    value=ast.Name(id="argp", ctx=ast.Load()), attr="parse_args"
                ),
                args=[],
                keywords=[],
            ),
        )
    )
    mainf.body.append(
        ast.Assign(
            targets=[ast.Name(id="oldcommit", ctx=ast.Store())],
            value=ast.Attribute(
                value=ast.Name(id="args", ctx=ast.Load()), attr="commit"
            ),
        )
    )
    mainf.body.append(
        ast.Assign(
            targets=[ast.Name(id="olddir", ctx=ast.Store())],
            value=ast.Call(
                func=ast.Attribute(
                    value=ast.Name(id="Path", ctx=ast.Load()), attr="cwd"
                ),
                args=[],
                keywords=[],
            ),
        )
    )
    mainf.body.append(
        ast.Assign(
            targets=[ast.Name(id="cwd", ctx=ast.Store())],
            value=path2ast(LT_DIR),
        )
    )
    # for (filename, id), (sig, paths) in SAVABLES.items():
    for filename in sorted(set([f for (f, _), (_, _) in SAVABLES.items()])):
        # print(repr(sig))
        mainf.body.append(
            ast.Expr(
                value=ast.Call(
                    func=ast.Name(id="check_call", ctx=ast.Load()),
                    args=[
                        ast.List(
                            elts=[
                                ast.Constant("git"),
                                ast.Constant("difftool"),
                                ast.Name("oldcommit"),
                                # ast.Constant('-C'),
                                # ast.Constant(str(LT_DIR)),
                                ast.Call(
                                    func=ast.Name("str"),
                                    args=[
                                        path2ast(
                                            Path(
                                                filename.replace("[LT]", str(LT_DIR))
                                            ).relative_to(LT_PKG_DIR),
                                            "LT_PKG_DIR",
                                        )
                                    ],
                                    keywords=[],
                                ),
                            ]
                        )
                    ],
                    keywords=[ast.keyword(arg="cwd", value=ast.Name("cwd"))],
                )
            )
        )
    # mainf.body.append(
    #     ast.Expr(
    #         value=ast.Call(
    #             func=ast.Attribute(
    #                 value=ast.Name(id="os"), attr="chdir", ctx=ast.Load()
    #             ),
    #             args=[ast.Name("olddir")],
    #             keywords=[],
    #         )
    #     )
    # )
    mf.expressions.append(
        ast.If(
            test=ast.BinOp(
                left=ast.Name(id="__name__", ctx=ast.Load()),
                op=ast.Eq(),
                right=ast.Constant(value="__main__"),
            ),
            body=[ast.Call(func=ast.Name(id="main"), args=[], keywords=[])],
            orelse=[],
        )
    )
    with open("devtools/diffFuncHashes.recc.py", "w") as f:
        f.write(LTExpandedCodeGenerator().generate(mf.generate()))


def main() -> None:
    SAVABLES: Dict[Tuple[str, str], Tuple[str, Set[Path]]] = {}
    with tqdm.tqdm(iterable=SAVABLES_DIR.rglob("*.yml"), unit="f") as prog:
        for yf in prog:
            data: dict = {}
            with yf.open("r") as f:
                data = YAML.load(f)
            if "function-hashes" in data.keys():
                for fhd in data["function-hashes"]:
                    fh = FuncHash()
                    fh.deserialize(fhd)
                    key = fsrc(fh)
                    if key not in SAVABLES.keys():
                        SAVABLES[key] = (fh.signature.strip(), set([yf]))
                    else:
                        SAVABLES[key][1].add(yf)
    _build_rebuildFH(SAVABLES)
    _build_diffFH(SAVABLES)


if __name__ == "__main__":
    main()

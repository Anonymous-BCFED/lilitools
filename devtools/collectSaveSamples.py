import ast
import re
import shutil
import subprocess
import sys
from pathlib import Path
from typing import Any, Dict, Final, Iterable, List, Optional, Set

import click
from _codegen import LTExpandedCodeGenerator
from _dev_regex import REG_JAVA_BOOL, REG_JAVA_FLOAT, REG_JAVA_INT
from dumpFromXMLTo import dumpFromXPath
from idiotCheck import getModsInSave, getSaveAsTree
from lxml import etree
from mkast.modulefactory import ModuleFactory

from lilitools.saves.character.enums.orifice_modifier import EOrificeModifier
from lilitools.saves.character.enums.penetrator_modifier import EPenetratorModifier
from lilitools.saves.character.fluids.enums.fluid_modifier import EFluidModifier

SAVE_PATH: Final[Path] = Path.cwd() / 'dist' / 'data' / 'saves'
SAMPLE_PATH: Final[Path] = Path.cwd() / 'data' / 'samples' / 'saves'
TEST_DATA_PATH: Final[Path] = Path.cwd() / 'tests' / 'data'
TEST_SAVABLES_PATH: Final[Path] = Path.cwd() / 'tests' / 'savables'


def info(msg: str) -> None:
    click.secho(msg)


def success(msg: str) -> None:
    click.secho(msg, fg='green')


def fatal(msg: str) -> None:
    click.secho(msg, fg='white', bg='red')
    sys.exit(1)


_CP = click.style('CP', fg='cyan')
_DUMP = click.style('DUMP', fg='cyan')
_D2X = click.style('D2X', fg='cyan')
_D2T = click.style('D2T', fg='cyan')
_SAMPLE = click.style('SAMPLE', fg='cyan')


def collectSave(orig_stem: str, raw_stem: str) -> None:
    orig = SAVE_PATH / (orig_stem + '.xml')
    raw = SAMPLE_PATH / (raw_stem + '.xml')
    cleaned = SAMPLE_PATH / (raw_stem + '.cleaned.xml')
    click.secho(f'Checking {orig}...')
    mods = getModsInSave(getSaveAsTree(orig))
    if len(mods) > 0:
        for mod in sorted(mods):
            info(f' * {mod}')
        fatal('Saves are tainted by modded information.')

    click.secho(f'{_CP}\t{orig} -> {raw}')
    shutil.copy(orig, raw)

    click.secho(f'{_DUMP}\t{orig} -> {cleaned}')
    subprocess.check_call(['lilitool', 'save', 'dump', str(orig), str(cleaned)])


def xpath2xml(inputfile: Path, xpath: str, output: Path, parsed: Optional[etree._ElementTree] = None) -> None:
    click.secho(f'{_D2X}\t{xpath} -> {output}')
    dumpFromXPath(inputfile=inputfile, xpath=xpath, output=output, parsed=parsed)


def detectAndConvertXMLString2AST(s: str) -> ast.AST:
    if (m := REG_JAVA_INT.fullmatch(s)) is not None:
        return ast.Constant(int(s))
    if (m := REG_JAVA_FLOAT.fullmatch(s)) is not None:
        return ast.Constant(float(s))
    if (m := REG_JAVA_BOOL.fullmatch(s)) is not None:
        if s == 'true':
            return ast.Constant(True)
        if s == 'false':
            return ast.Constant(False)
    return ast.Constant(s)


def is_orifice_modifier(s: str) -> bool:
    try:
        EOrificeModifier[s]
    except KeyError:
        return False
    return True


def is_penetrator_modifier(s: str) -> bool:
    try:
        EPenetratorModifier[s]
    except KeyError:
        return False
    return True


def is_fluid_modifier(s: str) -> bool:
    try:
        EFluidModifier[s]
    except KeyError:
        return False
    return True


def getEnumForModifierName(s: str) -> str:
    if is_orifice_modifier(s):
        return 'EOrificeModifier'
    if is_penetrator_modifier(s):
        return 'EPenetratorModifier'
    if is_fluid_modifier(s):
        return 'EFluidModifier'
    return 'EUnknown'


def xpath2test(inputfile: Path, xpath: str, module: str, className: str, output: Path, sample: Optional[etree._Element] = None, parsed: Optional[etree._ElementTree] = None) -> None:
    click.secho(f'{_D2T}\t{xpath} -> {output}')
    tree: etree._ElementTree
    if parsed is not None:
        tree = parsed
    else:
        tree = etree.parse(inputfile)
    if xpath.startswith('/') and not xpath.startswith('//'):
        xpath = '.' + xpath

    def _gen_frozenset(entries: Iterable[str]) -> ast.Call:
        elts = []
        if len(list(entries)) > 0:
            elts = [ast.Set(elts=list(
                map(lambda x: ast.Constant(x), sorted(entries))
            ))]
        return ast.Call(
            func=ast.Name('frozenset'),
            args=elts,
            keywords=[]
        )

    def _gen_fsass(name: str, entries: Iterable[str]) -> ast.AnnAssign:
        return ast.AnnAssign(
            target=ast.Name(name),
            annotation=ast.Subscript(value=ast.Name('AbstractSet'), slice=ast.Name('str')),
            value=_gen_frozenset(entries),
            simple=1
        )

    def _gen_selfcall(func: str, *args: ast.AST) -> ast.Call:
        return ast.Call(
            func=ast.Attribute(
                value=ast.Name('self'),
                attr=func
            ),
            args=args,
            keywords=[]
        )

    mf = ModuleFactory()
    required_attrs: Set[str] = set()
    all_attrs: Set[str] = set()
    all_attr_sets: List[Set[str]] = []
    required_elems: Set[str] = set()
    all_elems: Set[str] = set()
    all_elem_sets: List[Set[str]] = []
    last_elem: etree._Element = None
    all_elems_appearing_once: Set[str] = set()
    all_elems_appearing_more_than_once: Set[str] = set()
    entry: etree._Element
    all_samples: List[etree._Element] = []
    modifiers: Dict[str, Set[str]] = {}
    for entry in tree.getroot().xpath(xpath):
        # t = etree.ElementTree(entry)
        # etree.indent(t.getroot(), space=(' ' * 4))
        last_elem = entry
        cattrs: Set[str] = set(entry.keys())
        all_attrs |= cattrs
        all_attr_sets.append(cattrs)
        celems: Set[str] = set([e.tag for e in entry.getchildren()])
        all_elems |= celems
        all_elem_sets.append(celems)
        all_samples.append(entry)
        for c in entry.getchildren():
            if 'modifier' in c.tag.casefold():
                if c.tag not in modifiers.keys():
                    modifiers[c.tag] = set(c.attrib.keys())
                else:
                    modifiers[c.tag].update(set(c.attrib.keys()))
                for k in c.attrib.keys():
                    if is_orifice_modifier(k):
                        mf.addImportFrom('lilitools.saves.character.enums.orifice_modifier', ['EOrificeModifier'])
                    if is_penetrator_modifier(k):
                        mf.addImportFrom('lilitools.saves.character.enums.penetrator_modifier', ['EPenetratorModifier'])
                    if is_fluid_modifier(k):
                        mf.addImportFrom('lilitools.saves.character.fluids.enums.fluid_modifier', ['EFluidModifier'])

        for elem in celems:
            occurrances = sum([elem == x for x in celems])
            if occurrances == 1:
                if elem not in all_elems_appearing_more_than_once:
                    all_elems_appearing_once.add(elem)
            elif occurrances > 1:
                all_elems_appearing_more_than_once.add(elem)
                if elem not in all_elems_appearing_once:
                    all_elems_appearing_once.remove(elem)

    def intersect_all(p: List[Iterable[Any]]) -> Set[Any]:
        lenp = len(p)
        if lenp == 0:
            return set()
        if lenp == 1:
            return set(p[0])
        return set(p[0]).intersection(*p[1:])

    required_attrs = intersect_all(all_attr_sets)
    required_elems = intersect_all(all_elem_sets)
    mf.addImportFrom('lxml', ['etree'])
    mf.addImportFrom('typing', ['AbstractSet'])
    mf.addImportFrom(module, [className])
    #from tests.savables._base_savable_test import BaseSavableTest
    mf.addImportFrom('tests.savables._base_savable_test', ['BaseSavableTest'])
    cls = ast.ClassDef(
        name=f'{className}Tests',
        bases=[ast.Name('BaseSavableTest')],
        keywords=[],
        body=[],
        decorator_list=[],
    )
    mf.expressions.append(cls)
    cls.body.append(_gen_fsass('REQD_ATTRS', required_attrs))
    cls.body.append(_gen_fsass('ALLOWED_ATTRS', all_attrs))
    cls.body.append(_gen_fsass('REQD_CHILDREN', required_elems))
    cls.body.append(_gen_fsass('ALLOWED_CHILDREN', all_elems))
    sample = sample if sample is not None else last_elem
    et = etree.ElementTree(sample)
    etree.indent(et.getroot(), ' ' * 4)
    lns = etree.tostringlist(et, pretty_print=True)
    rawdat = etree.tostring(et, pretty_print=True, encoding='unicode')
    setup = ast.FunctionDef(
        name='setUp',
        args=ast.arguments(
            posonlyargs=[],
            args=[
                ast.arg('self')
            ],
            vararg=None,
            kwonlyargs=[],
            kw_defaults=None,
            kwarg=None,
            defaults=[],
        ),
        body=[
            ast.Expr(ast.Constant(rawdat.strip())),
            #ast.Constant(f'self.{sample.tag} = {className}()'),
            ast.Assign(
                targets=[ast.Attribute(
                    value=ast.Name('self'),
                    attr=sample.tag,
                )],
                value=ast.Call(
                    func=ast.Name(className),
                    args=[], keywords=[]
                )
            ),
        ],
        decorator_list=[],
        returns=ast.Constant(None)
    )
    cls.body.append(setup)
    toxml = ast.FunctionDef(
        name=f'test_{sample.tag}_toxml',
        args=ast.arguments(
            posonlyargs=[],
            args=[
                ast.arg('self')
            ],
            vararg=None,
            kwonlyargs=[],
            kw_defaults=None,
            kwarg=None,
            defaults=[],
        ),
        body=[
            ast.Assign(
                targets=[
                    ast.Name('e')
                ],
                value=ast.Call(
                    func=ast.Attribute(
                        value=ast.Attribute(
                            value=ast.Name('self'),
                            attr=sample.tag
                        ),
                        attr='toXML'
                    ),
                    args=[],
                    keywords=[]
                )
            )
        ],
        decorator_list=[],
        returns=ast.Constant(None)
    )
    cls.body.append(toxml)
    if len(required_attrs) > 0:
        toxml.body.append(ast.Expr(_gen_selfcall('assertAttrsMustExist', ast.Name('e'), ast.Attribute(value=ast.Name('self'), attr='REQD_ATTRS'))))
    if len(all_attrs) > 0:
        toxml.body.append(ast.Expr(_gen_selfcall('assertOnlyTheseAttrsMayExist', ast.Name('e'), ast.Attribute(value=ast.Name('self'), attr='ALLOWED_ATTRS'))))
    if len(required_elems) > 0:
        toxml.body.append(ast.Expr(_gen_selfcall('assertChildrenMustExist', ast.Name('e'), ast.Attribute(value=ast.Name('self'), attr='REQD_CHILDREN'))))
    if len(all_elems) > 0:
        toxml.body.append(ast.Expr(_gen_selfcall('assertOnlyTheseChildrenMayExist', ast.Name('e'), ast.Attribute(value=ast.Name('self'), attr='ALLOWED_CHILDREN'))))
    for e in all_elems_appearing_once:
        toxml.body.append(ast.Expr(_gen_selfcall('assertChildMustOccurOnlyOnce', ast.Name('e'), ast.Constant(e))))
    for k, v in sample.items():
        toxml.body.append(ast.Expr(_gen_selfcall('assertAttrEquals', ast.Name('e'), ast.Constant(k), ast.Constant(v))))
    '''
        self.assertIsNotNone(m := e.find('modifiers'))
        self.assertAttrEquals(m, 'a', 'true')
        ...
    '''
    for k, mods in modifiers.items():
        toxml.body.append(ast.Expr(_gen_selfcall('assertIsNotNone', ast.NamedExpr(
            target=ast.Name('m'),
            value=ast.Call(
                func=ast.Attribute(
                    value=ast.Name('e'),
                    attr='find'
                ),
                args=[
                    ast.Constant(k)
                ],
                keywords=[]
            )
        ))))
        for mfe in sorted(mods):
            toxml.body.append(ast.Expr(_gen_selfcall('assertAttrEquals', ast.Name('m'), ast.Constant(mfe), ast.Constant('true'))))

    fromxml = ast.FunctionDef(
        name=f'test_{sample.tag}_fromxml',
        args=ast.arguments(
            posonlyargs=[],
            args=[
                ast.arg('self')
            ],
            vararg=None,
            kwonlyargs=[],
            kw_defaults=None,
            kwarg=None,
            defaults=[],
        ),
        body=[
            # ast.Assign(
            #     targets=[ast.Name('xml')],
            #     value=ast.Constant(
            #         rawdat
            #     )
            # ),
            ast.AnnAssign(
                target=ast.Name('o'),
                annotation=ast.Name(className),
                value=_gen_selfcall('parseToSavable', ast.Constant(rawdat.strip()), ast.Name(className)),
                simple=1
            ),
        ],
        decorator_list=[],
        returns=ast.Constant(None)
    )
    cls.body.append(fromxml)
    for k, v in sample.items():
        #self.leg.configuration = ELegConfiguration.BIPEDAL
        setup.body.append(ast.Assign(
            targets=[
                ast.Attribute(
                    value=ast.Attribute(
                        value=ast.Name('self'),
                        attr=sample.tag
                    ),
                    attr=k
                )
            ],
            value=detectAndConvertXMLString2AST(v)
        ))
        fromxml.body.append(ast.Expr(_gen_selfcall('assertEqual', ast.Attribute(ast.Name('o'), k), detectAndConvertXMLString2AST(v))))
    for k, mods in modifiers.items():
        attr = ast.Attribute(
            value=ast.Attribute(
                value=ast.Name('self'),
                attr=sample.tag
            ),
            attr=k
        )
        setv = ast.Set([ast.Attribute(value=ast.Name(getEnumForModifierName(mod)), attr=mod) for mod in sorted(mods)])
        setup.body.append(ast.Assign(
            targets=[
                attr
            ],
            value=setv
        ))
        fromxml.body.append(ast.Expr(
            _gen_selfcall('assertSetEqual', attr, setv)
        ))

    # for se in all_samples:
    #     et = etree.ElementTree(se)
    #     etree.indent(et.getroot(), ' ' * 4)
    #     mf.expressions.append(ast.Expr(ast.Constant(etree.tostring(et, pretty_print=True, encoding='unicode').strip())))
    with output.open('w') as f:
        f.write(LTExpandedCodeGenerator().generate(mf.generate()))
        #f.write(astor.to_source(mf.generate()))


def generatePopulatedSampleFromXPath(inputfile: Path, xpath: str, output: Path, parsed: Optional[etree._ElementTree] = None) -> Optional[etree._Element]:
    click.secho(f'{_SAMPLE}\t{xpath} -> {output}')
    tree: etree._ElementTree
    if parsed is not None:
        tree = parsed
    else:
        tree = etree.parse(inputfile)
    if xpath.startswith('/') and not xpath.startswith('//'):
        xpath = '.' + xpath
    tag: Optional[str] = None
    attrs: Dict[str, str] = {}
    children: Dict[str, etree._Element] = {}
    modifiers: Dict[str, Set[str]] = {}
    entry: etree._Element
    for entry in tree.getroot().xpath(xpath):
        if tag is None:
            tag = entry.tag
        attrs.update(entry.attrib)
        for c in entry.getchildren():
            children[c.tag] = c
            if 'modifier' in c.tag.casefold():
                if c.tag not in modifiers.keys():
                    modifiers[c.tag] = set(c.attrib.keys())
                else:
                    modifiers[c.tag].update(set(c.attrib.keys()))
    if tag is None:
        return None
    e = etree.Element(tag, attrs)
    for c in children.values():
        e.append(c)
    for k, mods in modifiers.items():
        if (olde := e.find(k)) is not None:
            olde.getparent().remove(olde)
        etree.SubElement(e, k, {k: 'true' for k in sorted(mods)})
    return e


def camel_case(s):
    s = re.sub(r"(_|-)+", " ", s).title().replace(" ", "")
    return ''.join([s[0].lower(), s[1:]])


def main() -> None:
    collectSave('Male', 'male')
    collectSave('Female', 'female')
    xpath2xml(
        inputfile=SAVE_PATH / 'Male.xml',
        xpath='./playerCharacter/body/bodyCore',
        output=TEST_DATA_PATH / 'character' / 'body' / 'bodycore.xml'
    )
    xpath2xml(
        inputfile=SAVE_PATH / 'Male.xml',
        xpath='./playerCharacter/character/characterInventory',
        output=TEST_DATA_PATH / 'character' / 'character_inventory' / 'full.xml'
    )
    xpath2xml(
        inputfile=SAVE_PATH / 'Male.xml',
        xpath='./maps',
        output=TEST_DATA_PATH / 'maps' / 'maps.xml'
    )
    genTestAndSampleFor(
        inputfile=SAVE_PATH / 'Male.xml',
        xpath=f'./maps',
        subjectmodule='lilitools.saves.world.world_controller',
        subjectclass='WorldController',
        output_xml=TEST_DATA_PATH / 'maps' / 'maps.xml',
        output_unittest=TEST_SAVABLES_PATH / 'maps' / 'maps.py.new'
    )
    genTestAndSampleFor(
        inputfile=SAVE_PATH / 'Male.xml',
        xpath=f'./maps//world[@worldType="DADDYS_APARTMENT"]',
        subjectmodule='lilitools.saves.world.world',
        subjectclass='World',
        output_xml=TEST_DATA_PATH / 'maps' / 'world.xml',
        output_unittest=TEST_SAVABLES_PATH / 'maps' / 'world.py.new'
    )
    genTestAndSampleFor(
        inputfile=SAVE_PATH / 'Male.xml',
        xpath=f'./maps//world[@worldType="DADDYS_APARTMENT"]//cell[location[@x="2" and @y="0"]]',
        subjectmodule='lilitools.saves.world.cell',
        subjectclass='Cell',
        output_xml=TEST_DATA_PATH / 'maps' / 'cell.xml',
        output_unittest=TEST_SAVABLES_PATH / 'maps' / 'cell.py.new'
    )
    CLASSNAMES_THAT_DIFFER: Dict[str, str] = {
        'antennae': 'Antenna',
        'arm': 'Arms',
        'ear': 'Ears',
        'eye': 'Eyes',
        'girlcum': 'GirlCum',
        'horn': 'Horns',
        'leg': 'Legs',
        'tentacle': 'Tentacles',
        'wing': 'Wings',
    }
    MODULES_THAT_DIFFER: Dict[str, str] = {
        'antennae': 'lilitools.saves.character.body.antenna',
        'arm': 'lilitools.saves.character.body.arms',
        'cum': 'lilitools.saves.character.fluids.cum',
        'ear': 'lilitools.saves.character.body.ears',
        'eye': 'lilitools.saves.character.body.eyes',
        'girlcum': 'lilitools.saves.character.fluids.girlcum',
        'horn': 'lilitools.saves.character.body.horns',
        'leg': 'lilitools.saves.character.body.legs',
        'tentacle': 'lilitools.saves.character.body.tentacles',
        'wing': 'lilitools.saves.character.body.wings',
    }
    for bptag in ('antennae', 'anus', 'arm', 'ass', 'breasts', 'cum', 'ear', 'eye', 'face', 'girlcum', 'hair', 'horn', 'leg', 'mouth', 'nipples', 'penis', 'spinneret', 'tail', 'tentacle', 'testicles', 'tongue', 'torso', 'vagina', 'wing'):
        genTestAndSampleFor(
            inputfile=SAVE_PATH / 'Male.xml',
            xpath=f'//body/{bptag}',
            subjectmodule=MODULES_THAT_DIFFER.get(bptag, f'lilitools.saves.character.body.{bptag}'),
            subjectclass=CLASSNAMES_THAT_DIFFER.get(bptag, re.sub(r"(_|-)+", " ", bptag).title().replace(" ", "")),
            output_xml=TEST_DATA_PATH / 'character' / 'body' / f'{bptag}.xml',
            output_unittest=TEST_SAVABLES_PATH / 'character' / 'body' / f'{bptag}.py.new',
        )
        # sample = generatePopulatedSampleFromXPath(
        #     inputfile=SAVE_PATH / 'Male.xml',
        #     xpath=f'//body/{bptag}',
        #     output=TEST_DATA_PATH / 'character' / 'body' / f'{bptag}.xml'
        # )
        # xpath2xml(
        #     inputfile=SAVE_PATH / 'Male.xml',
        #     xpath=f'//body/{bptag}',
        #     output=TEST_DATA_PATH / 'character' / 'body' / f'{bptag}.xml'
        # )
        # xpath2test(
        #     inputfile=SAVE_PATH / 'Male.xml',
        #     xpath=f'//body/{bptag}',
        #     module=MODULES_THAT_DIFFER.get(bptag, f'lilitools.saves.character.body.{bptag}'),
        #     className=CLASSNAMES_THAT_DIFFER.get(bptag, re.sub(r"(_|-)+", " ", bptag).title().replace(" ", "")),
        #     output=TEST_SAVABLES_PATH / 'character' / 'body' / f'{bptag}.py.new',
        #     sample=sample
        # )


def genTestAndSampleFor(inputfile: Path, xpath: str, subjectmodule: str, subjectclass: str, output_xml: Path, output_unittest: Path) -> None:
    et = etree.parse(inputfile)
    sample = generatePopulatedSampleFromXPath(
        inputfile=inputfile,
        xpath=xpath,
        output=output_xml,
        parsed=et
    )
    xpath2xml(
        inputfile=inputfile,
        xpath=xpath,
        output=output_xml,
        parsed=et
    )
    xpath2test(
        inputfile=inputfile,
        xpath=xpath,
        module=subjectmodule,
        className=subjectclass,
        output=output_unittest,
        sample=sample,
        parsed=et
    )


if __name__ == "__main__":
    main()

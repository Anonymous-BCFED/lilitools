import argparse
import os
from pathlib import Path
from subprocess import call as check_call
from typing import Final
DATA_DIR = Path("data")
SAVABLES_DIR = DATA_DIR / "savables"
MODFILES_DIR = SAVABLES_DIR / "modfiles"
LT_DIR: Final = Path("lib") / "liliths-throne-public"
LT_PKG_DIR: Final = (LT_DIR / "src" / "com" / "lilithsthrone").absolute()


def main():
    argp = argparse.ArgumentParser()
    argp.add_argument("commit", type=str)
    args = argp.parse_args()
    oldcommit = args.commit
    olddir = Path.cwd()
    cwd = Path("lib") / "liliths-throne-public"
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "Game.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "Properties.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "character" / "FluidStored.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "character" / "GameCharacter.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "character" / "PlayerCharacter.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "character" / "SexCount.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "character" / "body" / "Body.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "character" / "body" / "FluidCum.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "character" / "body" / "FluidGirlCum.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "character" / "body" / "FluidMilk.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "character" / "body" / "coverings" / "Covering.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "character" / "effects" / "AbstractStatusEffect.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "character" / "effects" / "Addiction.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "character" / "markings" / "Scar.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "character" / "markings" / "Tattoo.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "character" / "markings" / "TattooCounter.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "character" / "markings" / "TattooWriting.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "character" / "npc" / "NPC.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "character" / "npc" / "misc" / "Elemental.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "character" / "npc" / "misc" / "OffspringSeed.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "character" / "npc" / "submission" / "HazmatRat.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "character" / "pregnancy" / "Litter.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "character" / "pregnancy" / "PregnancyPossibility.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "dialogue" / "DialogueFlags.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "dialogue" / "eventLog" / "EventLogEntry.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "dialogue" / "eventLog" / "SlaveryEventLogEntry.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "dialogue" / "utils" / "EnchantmentDialogue.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "inventory" / "CharacterInventory.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "inventory" / "clothing" / "AbstractClothing.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "inventory" / "clothing" / "AbstractClothingType.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "inventory" / "clothing" / "BlockedParts.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "inventory" / "enchanting" / "ItemEffect.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "inventory" / "item" / "AbstractFilledBreastPump.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "inventory" / "item" / "AbstractFilledCondom.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "inventory" / "item" / "AbstractItem.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "inventory" / "outfit" / "Outfit.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "inventory" / "weapon" / "AbstractWeapon.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "inventory" / "weapon" / "AbstractWeaponType.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "occupantManagement" / "MilkingRoom.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "game" / "occupantManagement" / "OccupancyUtil.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "world" / "Cell.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "world" / "World.java")], cwd=cwd)
    check_call(["git", "difftool", oldcommit, str(LT_PKG_DIR / "world" / "places" / "GenericPlace.java")], cwd=cwd)


if __name__ == "__main__":
    main()

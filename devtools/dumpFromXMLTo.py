'''
Used for gathering samples from saves.
'''
from typing import Optional
from lxml import etree
from pathlib import Path


def main():
    import argparse

    argp = argparse.ArgumentParser()

    argp.add_argument('infile', type=str)
    argp.add_argument('xpath', type=str)
    argp.add_argument('--output', '-o', type=Path, default=None, nargs='?')

    args = argp.parse_args()

    dumpFromXPath(args.infile, args.xpath, args.output)


def dumpFromXPath(inputfile: Path, xpath: str, output: Optional[Path] = None, parsed: Optional[etree._ElementTree] = None) -> None:
    tree: etree._ElementTree
    if parsed is not None:
        tree = parsed
    else:
        tree = etree.parse(inputfile)
    if xpath.startswith('/') and not xpath.startswith('//'):
        xpath = '.' + xpath
    for entry in tree.getroot().xpath(xpath):
        #print(entry)
        t = etree.ElementTree(entry)
        etree.indent(t, space=(' ' * 4))
        if output is None:
            etree.dump(t.getroot(), pretty_print=True)
        else:
            output.parent.mkdir(parents=True, exist_ok=True)
            t.write(output, encoding='utf-8', pretty_print=True)
        return


if __name__ == '__main__':
    main()

from __future__ import annotations

import os
import time
from enum import IntEnum, IntFlag
from pathlib import Path
from typing import Dict, Iterable, List, Optional, Set, TextIO, Tuple

from ruamel.yaml import YAML as Yaml

YAML = Yaml(typ="rt")


class EEffectFlags(IntFlag):
    NONE = 0
    NO_REPEAT = 1


class EAllowedPotency(IntFlag):
    NONE = 0
    MAJOR_DRAIN = 1
    DRAIN = 2
    MINOR_DRAIN = 4
    MINOR_BOOST = 8
    BOOST = 16
    MAJOR_BOOST = 32
    ALL = MAJOR_DRAIN | DRAIN | MINOR_DRAIN | MINOR_BOOST | BOOST | MAJOR_BOOST

    def toStrings(self) -> List[str]:
        o = []
        v: int = self.value
        for i in range(6):
            cf = 1 << i
            cn = EAllowedPotency(cf).name
            if (v & cf) == cf:
                o.append(cn)
        return o

    @classmethod
    def FromStrings(self, ss: Iterable[str]) -> EAllowedPotency:
        o = EAllowedPotency.NONE
        for s in ss:
            o |= EAllowedPotency[s]
        return o


class EffectInfo:
    def __init__(self) -> None:
        self.id: str = ""
        self.type: str = ""
        self.mod1: str = ""
        self.mod2: str = ""

        self.potencies: EAllowedPotency = EAllowedPotency.NONE
        self.description: str = ""
        self.flags: EEffectFlags = EEffectFlags.NONE

    def fromDict(self, data: dict) -> None:
        self.id = data["id"]
        self.type = data["type"]
        self.mod1 = data["mod1"]
        self.mod2 = data["mod2"]
        self.description = data["desc"]
        self.flags = EEffectFlags(int(data["flags"]))
        self.potencies = EAllowedPotency.FromStrings(data["potencies"])

    def asDict(self) -> dict:
        return {
            "id": self.id,
            "type": self.type,
            "mod1": self.mod1,
            "mod2": self.mod2,
            "desc": self.description,
            "flags": self.flags.value,
            "pot": self.potencies.toStrings(),
        }

    def toRow(self) -> List[str]:
        return [
            self.type,
            self.mod1,
            self.mod2,
            self.id,
            str(self.flags.value),
            "|".join(self.potencies.toStrings()),
            self.description,
        ]


class Effects:
    CVERSION: int = 2023_06_24_11_44

    def __init__(self) -> None:
        self.tf2id: Dict[Tuple[str, str, str], str] = {}
        self.id2tf: Dict[str, Tuple[str, str, str]] = {}
        self.effects: Dict[str, EffectInfo] = {}

    def add(
        self,
        type_: str,
        mod1: str,
        mod2: str,
        fxid: Optional[str],
        info: Optional[EffectInfo] = None,
    ) -> EffectInfo:
        if fxid is None:
            for i in range(len(self.tf2id)):
                fxid = f"UNKNOWN_{i:04X}"
                if fxid not in self.id2tf.keys():
                    break
        ei = info or EffectInfo()
        ei.type = type_
        ei.mod1 = mod1
        ei.mod2 = mod2
        ei.id = fxid
        self.tf2id[(type_, mod1, mod2)] = fxid
        self.id2tf[fxid] = (type_, mod1, mod2)
        self.effects[fxid] = ei
        return ei

    def remove(self, type_: str, mod1: str, mod2: str) -> None:
        fxid = self.tf2id[(type_, mod1, mod2)]
        del self.tf2id[(type_, mod1, mod2)]
        del self.id2tf[fxid]
        del self.effects[fxid]

    def rename(self, type_: str, mod1: str, mod2: str, newname: str) -> None:
        fxid = self.tf2id[(type_, mod1, mod2)]
        ei = self.effects[fxid]
        self.remove(type_, mod1, mod2)
        self.add(type_, mod1, mod2, newname, info=ei)

    def set(
        self,
        type_: str,
        mod1: str,
        mod2: str,
        fxid: str,
        info: Optional[EffectInfo] = None,
    ) -> None:
        old_fxid = self.tf2id[(type_, mod1, mod2)]
        ei = info or self.effects[old_fxid]
        if old_fxid in self.id2tf.keys():
            del self.id2tf[old_fxid]
        if fxid in self.id2tf.keys():
            del self.id2tf[fxid]
        if fxid in self.effects.keys():
            del self.effects[fxid]
        if old_fxid in self.effects.keys():
            del self.effects[old_fxid]
        self.tf2id[(type_, mod1, mod2)] = fxid
        self.id2tf[fxid] = (type_, mod1, mod2)
        self.effects[fxid] = ei

    def getEffectID(self, type_: str, mod1: str, mod2: str) -> str:
        return self.tf2id[(type_, mod1, mod2)]

    def getEffectByID(self, fxid: str) -> Tuple[str, str, str]:
        return self.id2tf[fxid]

    def getEffectInfoByID(self, fxid: str) -> EffectInfo:
        return self.effects[fxid]

    def getEffectInfoByTuple(self, type_: str, mod1: str, mod2: str) -> EffectInfo:
        return self.effects[self.tf2id[(type_, mod1, mod2)]]

    def from_dict(self, data: dict) -> None:
        sd = sorted(data.items(), key=lambda x: x[0])
        for k, v in sd:
            (a, b, c) = k[:3]
            self.add(a, b, c, v)

    @classmethod
    def from_yml_file(cls, path: Path) -> Effects:
        fxf = cls()
        with path.open("r") as f:
            fxf.from_yml_handle(f)
        return fxf

    def from_yml_handle(self, f_: TextIO) -> None:
        print(f"Loading {f_.name}")
        start = time.time()
        data: Dict[List[str], str] = YAML.load(f_)
        dur = time.time() - start
        print(f"YAML deserialized {len(data)} objects in {dur}s")
        start = time.time()
        sd = sorted(data.items(), key=lambda x: x[0])
        dur = time.time() - start
        print(f"Sorted in {dur}s")
        for k, v in sd:
            if isinstance(v, str):
                (t, m1, m2) = k
                v, _, _ = v
                ei = self.add(t, m1, m2, v)
                ei.id = v
                ei.type = t
                ei.mod1 = m1
                ei.mod2 = m2
                ei.flags = EEffectFlags.NONE
                ei.description = ""
            else:
                ei = EffectInfo()
                ei.fromDict(v)
            self.add(ei.type, ei.mod1, ei.mod2, ei.id, ei)

    def to_yml_file(self, path: Path) -> None:
        tmpname = path.with_suffix(".yml~")
        try:
            with open(tmpname, "w") as f:
                o = {}
                for k, e in sorted(self.effects.items(), key=lambda x: x[0]):
                    o[e.id] = e.asDict()
                YAML.dump(o, f)
            os.replace(tmpname, path)
        finally:
            if tmpname.is_file():
                tmpname.unlink()

    @classmethod
    def from_csv_file(cls, path: Path) -> Effects:
        fxf = cls()
        with path.open("r") as f:
            fxf.from_csv_handle(f)
        return fxf

    def from_csv_handle(self, f_: TextIO) -> None:
        read_the_first_line: bool = False
        for line in f_:
            if not read_the_first_line:
                read_the_first_line = True
                continue
            ld = list(map(lambda x: x.strip('"').strip(), line.strip().split(",", 6)))
            e = EffectInfo()
            if len(ld) == 4:
                e.type, e.mod1, e.mod2, e.id = ld[:4]
                e.flags = EEffectFlags.NONE
                e.description = ""
            else:
                e.type, e.mod1, e.mod2, e.id, f, p, e.description = ld[:7]
                e.potencies = EAllowedPotency.FromStrings(filter(lambda x: len(x) > 0, p.split("|")))
                e.flags = EEffectFlags(int(f))
            self.add(e.type, e.mod1, e.mod2, e.id, e)

    def to_csv_file(self, path: Path) -> None:
        tmppath = path.with_suffix(".csv~")
        try:
            with tmppath.open("w") as w:
                w.write(f"type,mod1,mod2,LiliTools ID,Flags,Description\n")
                for k, v in sorted(self.tf2id.items(), key=lambda x: x[0]):
                    ei = self.effects[v]
                    if ei.id is None:
                        ei.id = v
                    w.write(",".join(ei.toRow()) + "\n")
            os.replace(tmppath, path)
        finally:
            if tmppath.is_file():
                os.remove(tmppath)


def main() -> int:
    import argparse

    argp = argparse.ArgumentParser()
    argp.add_argument("type", type=str)
    argp.add_argument("mod1", type=str)
    argp.add_argument("mod2", type=str)
    argp.add_argument("--set-id", type=str, default=None)
    argp.add_argument("--set-no-repeat", type=bool, default=None)
    args = argp.parse_args()
    EFFECT_NAMES_YML = Path("data") / "effect_names.yml"
    EFFECT_NAMES_CSV = Path("data") / "effect_names.csv"

    effects: Effects
    if EFFECT_NAMES_CSV.is_file():
        effects = Effects.from_csv_file(EFFECT_NAMES_CSV)
    elif EFFECT_NAMES_YML.is_file():
        effects = Effects.from_yml_file(EFFECT_NAMES_YML)
    k = (args.type, args.mod1, args.mod2)
    if k not in effects.tf2id.keys():
        print(f"Key {k!r} does not exist in tf2id.")
        # with open('dump.txt', 'w') as f:
        #     f.write(repr(effects.tf2id))
        return 1
    if args.set_id is not None:
        if args.set_id in effects.id2tf.keys():
            print(repr("Specified LT ID exists already."))
            return 1
        oldfxid = effects.getEffectID(args.type, args.mod1, args.mod2)
        newfxid = args.set_id.strip()
        effects.set(args.type, args.mod1, args.mod2, newfxid)
        print(f"{args.type}/{args.mod1}/{args.mod2}: {oldfxid} -> {newfxid}")
        print("Writing CSV...")
        effects.to_csv_file(EFFECT_NAMES_CSV)
        print("Writing YAML...")
        effects.to_yml_file(EFFECT_NAMES_YML)
    elif args.set_no_repeat is not None:
        effect: EffectInfo = effects.getEffectInfoByID(args.type, args.mod1, args.mod2)
        old_flags = effect.flags
        if args.set_no_repeat:
            effect.flags |= EEffectFlags.NO_REPEAT
        else:
            effect.flags &= ~EEffectFlags.NO_REPEAT
        print(
            f"{args.type}/{args.mod1}/{args.mod2}/{effect.id}: Flags {old_flags!r} -> {effect.flags!r}"
        )
        print("Writing CSV...")
        effects.to_csv_file(EFFECT_NAMES_CSV)
        print("Writing YAML...")
        effects.to_yml_file(EFFECT_NAMES_YML)
    else:
        print(effects.getEffectID(k))
    return 0


if __name__ == "__main__":
    main()

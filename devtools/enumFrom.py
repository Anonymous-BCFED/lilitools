import ast
import collections
import json
import re
from pathlib import Path
from typing import Any, Dict, FrozenSet, Iterable, List, Optional, OrderedDict, TextIO, Tuple, Union

import astor
import javalang
from _dev_regex import REG_JAVA_FLOAT, REG_JAVA_INT
from astor.source_repr import split_lines
from javalang.tree import (BinaryOperation, ClassCreator, ClassDeclaration, CompilationUnit, EnumConstantDeclaration, EnumDeclaration, FieldDeclaration, Literal, MemberReference, MethodInvocation,
                           VariableDeclarator)

RESERVED_ENUM_ATTR_NAMES: FrozenSet[str] = frozenset({
    'name',
})


def getEnumsInEnum(filepath: Union[str, Path], passCtorArgs: bool = False) -> Dict[str, List[Tuple[str, Optional[List[ast.AST]]]]]:
    cu: CompilationUnit = javalang.parse.parse(filepath.read_text())
    o: Dict[str, List[Tuple[str, Optional[List[ast.AST]]]]] = {}
    values: List[Tuple[str, Optional[ast.AST]]] = []

    jtype: EnumDeclaration
    eVal: EnumConstantDeclaration
    for jtype in cu.types:
        if isinstance(jtype, EnumDeclaration):
            values = []
            for eVal in jtype.body.constants:
                #print(jtype.name, eVal.name, simplifyArgs(eVal.arguments))
                values.append((eVal.name, None if not passCtorArgs else simplifyArgs(eVal.arguments, constants_only=True)))
            o[jtype.name] = values
    return o


def getEnumsFromConsts(filepath: Union[str, Path], const_type: Optional[str] = None, passCtorArgs: bool = False) -> Dict[str, List[Tuple[str, Optional[List[ast.AST]]]]]:
    cu: CompilationUnit = javalang.parse.parse(filepath.read_text())
    o: Dict[str, List[Tuple[str, Optional[List[ast.AST]]]]] = {}
    values: List[Tuple[str, Optional[ast.AST]]] = []
    lists: Dict[str, List[Tuple[str, Optional[ast.AST]]]] = {}

    jtype: EnumDeclaration
    eVal: FieldDeclaration
    vdec: VariableDeclarator
    for jtype in cu.types:
        if isinstance(jtype, ClassDeclaration):
            values = []
            for eVal in jtype.body:
                #print(jtype.name, eVal.modifiers)
                if isinstance(eVal, FieldDeclaration) and 'static' in eVal.modifiers and 'public' in eVal.modifiers:
                    if const_type and eVal.type.name != const_type:
                        continue

                    #print(repr(eVal))

                    # public static final Colour[] ACTION_POINT_COLOURS = new Colour[] {
                    # eVal.type=ReferenceType(arguments=None, dimensions=[None], name=Colour, sub_type=None)
                    if len(eVal.type.dimensions) == 1:
                        for vdec in eVal.declarators:
                            #print(vdec.name)
                            lists[vdec.name] = []

                        continue

                    for vdec in eVal.declarators:
                        if isinstance(vdec.initializer, ClassCreator):
                            simped = simplifyArgs(vdec.initializer.arguments, constants_only=True)
                            values.append((vdec.name, simped))
                            #print(ast.dump(ast.Tuple(elts=simped)))
                        else:
                            values.append((vdec.name, None))
            o[jtype.name] = values
    return o


def simplifyStringIfPossible(op: BinaryOperation) -> Union[ast.Constant, ast.BinOp]:
    opmap = {
        '+': ast.Add,
        '-': ast.Sub,
        '/': ast.Div,
        '*': ast.Mult,
    }
    o = ast.BinOp(left=simplifyArgs([op.operandl]), op=opmap[op.operator](), right=simplifyArgs([op.operandr]))
    if isinstance(o.op, ast.Add) \
            and isinstance(o.left, ast.Constant) and isinstance(o.left.value, str) \
            and isinstance(o.right, ast.Constant) and isinstance(o.right.value, str):
        o = ast.Constant(o.left.value + o.right.value)
    return o


def simplifyArgs(args: Iterable[Any], constants_only: bool = False) -> List[ast.AST]:
    o: List[ast.AST] = []
    for op in args:
        if isinstance(op, Literal):
            val = str(op.value)
            neg = hasattr(op, 'prefix_operators') and '-' in op.prefix_operators
            if (m := REG_JAVA_INT.match(val)) is not None:
                #print('INT', m[0], m[1])
                o.append(ast.Constant(int(m[1]) * (-1 if neg else 1)))
            elif (m := REG_JAVA_FLOAT.match(val)) is not None:
                #print('FLOAT', m[0], m[1])
                o.append(ast.Constant(float(m[1]) * (-1 if neg else 1)))
            elif val.startswith('0x'):
                o.append(ast.Constant(int(val) * (-1 if neg else 1)))
            elif val == 'true':
                o.append(ast.Constant(True))
            elif val == 'false':
                o.append(ast.Constant(False))
            elif val == 'null':
                o.append(ast.Constant(None))
            elif val.startswith('"') and val.endswith('"'):
                o.append(ast.Constant(json.loads(val)))
            else:
                print(f'W: Unable to simplify unknown literal: {val}')
                o.append(ast.Constant(None))

        elif isinstance(op, BinaryOperation):
            if op.operator == '+' and (isinstance(op.operandr, Literal) and op.operandr.value.startswith('"') and op.operandr.value.endswith('"')):
                o.append(simplifyStringIfPossible(op))
            # else:
            #     o.append(ast.Constant('[CANNOT DETERMINE VIA STATIC ANALYSIS]'))

        elif isinstance(op, MemberReference) and not constants_only:
            v = op.qualifier
            if isinstance(op.qualifier, str):
                v = ast.Name(id=op.qualifier, ctx=ast.Load())
            o.append(ast.Attribute(value=v, attr=op.member, ctx=ast.Load()))

        elif isinstance(op, ClassCreator) and not constants_only:
            # print(repr(op))
            #if op.type.name == 'ItemEffect':
            v = ast.Call(func=ast.Name(op.type.name),
                         args=simplifyArgs(op.arguments),
                         keywords=[])
            # print(astor.to_source(v))
            o.append(v)
        elif isinstance(op, MethodInvocation) and not constants_only:
            if op.qualifier == 'Util':
                # Util.newArrayListOfValues
                # MethodInvocation(arguments=[Literal(postfix_operators=[], prefix_operators=[], qualifier=None, selectors=[], value="unarmed")], member=newArrayListOfValues, postfix_operators=[], prefix_operators=[], qualifier=Util, selectors=[], type_arguments=None)
                if op.member == 'newArrayListOfValues':
                    o.append(ast.List(elts=list(simplifyArgs(op.arguments, constants_only=constants_only)), ctx=ast.Load()))
                # Util.newHashMapOfValues
                # MethodInvocation(arguments=[Literal(postfix_operators=[], prefix_operators=[], qualifier=None, selectors=[], value="unarmed")], member=newArrayListOfValues, postfix_operators=[], prefix_operators=[], qualifier=Util, selectors=[], type_arguments=None)
                elif op.member == 'newHashMapOfValues':
                    dkeys = []
                    dvalues = []
                    # new Value<>(), ...
                    cc: ClassCreator
                    for cc in op.arguments:
                        simped = simplifyArgs(cc.arguments, constants_only=constants_only)
                        if len(simped) == 2:
                            k, v = simped
                            dkeys.append(k)
                            dvalues.append(v)
                    o.append(ast.Dict(keys=dkeys, values=dvalues))
        else:
            print('Unhandled type in simplifyArgs:', repr(op))
    return o


def transformToPythonEnum(contents: Dict[str, List[Tuple[str, Optional[List[ast.AST]]]]],
                          start_at: int = 0,
                          attrs_for_instance: Optional[OrderedDict[str, str]] = None,
                          additional_methods: Optional[List[ast.FunctionDef]] = None,
                          additional_imports: Optional[List[ast.Import | ast.ImportFrom]] = None,
                          additional_module_members: List[ast.AST]=[]) -> str:
    allValues: List[ast.Constant] = []

    mod = ast.Module(body=[
        ast.ImportFrom(module='enum',
                       names=[
                           #ast.alias(name='auto', asname=None),
                           ast.alias(name='IntEnum', asname=None),
                       ],
                       level=0),
    ])
    if additional_imports is not None:
        mod.body += additional_imports
    mod.body += [
        ast.Assign(
            targets=[
                ast.Name(id='__all__', ctx=ast.Store())
            ],
            value=ast.List(elts=allValues))
    ]
    if attrs_for_instance is not None:
        mod.body[0].names = [ast.alias(name='Enum', asname=None)]
    i: int = start_at
    for enumName, enumContents in contents.items():
        clsbod: List[ast.AST] = []
        cls = ast.ClassDef(name=enumName, bases=[ast.Name(id='Enum' if attrs_for_instance is not None else 'IntEnum', ctx=ast.Load())], keywords=[], body=clsbod, decorator_list=[])
        allValues.append(ast.Constant(value=enumName, kind=None))
        for member, args in enumContents:
            v: ast.AST = ast.Constant(i)
            if args is not None and attrs_for_instance is not None:
                elts = []
                j = 0
                try:
                    for attr, attrType in attrs_for_instance.items():
                        if attrType is None:
                            continue
                        elts.append(args[j])
                        j += 1
                except IndexError as e:
                    print(repr(args))
                    raise e
                v = ast.Tuple(elts=elts)
            clsbod.append(
                # ast.Assign(
                #     targets=[
                #         ast.Name(id=member, ctx=ast.Load())
                #     ],
                #     value=ast.Call(
                #         func=ast.Name(id='auto', ctx=ast.Load()),
                #         args=[],
                #         keywords=[]
                #     )))
                ast.Assign(
                    targets=[
                        ast.Name(id=member, ctx=ast.Load())
                    ],
                    value=v))
            i += 1
        if attrs_for_instance is not None and len(attrs_for_instance) > 0:
            args = [ast.arg(arg='self', annotation=None)]
            for k, t in attrs_for_instance.items():
                if t is None:
                    continue
                args.append(ast.arg(arg=k, annotation=ast.Name(id=t, ctx=ast.Load())))
            init = ast.FunctionDef(name='__init__',
                                   args=ast.arguments(
                                       posonlyargs=[],
                                       args=args,
                                       kw_defaults=[],
                                       kwonlyargs=[],
                                       defaults=[],
                                       vararg=None,
                                       kwarg=None),
                                   returns=ast.Constant(None),
                                   decorator_list=[],
                                   body=[])
            for k, t in attrs_for_instance.items():
                if t is None:
                    continue
                ka: str = k
                if ka in RESERVED_ENUM_ATTR_NAMES:
                    ka += '_'
                init.body.append(
                    ast.AnnAssign(
                        target=ast.Attribute(
                            value=ast.Name(id='self', ctx=ast.Load()),
                            attr=ast.Name(id=ka, ctx=ast.Store())),
                        annotation=ast.Name(id=t, ctx=ast.Load()),
                        value=ast.Name(id=k, ctx=ast.Load()),
                        simple=1)
                )

                funcName = k[0].upper() + k[1:]
                getFunc = ast.FunctionDef(name=f'get{funcName}',
                                          args=ast.arguments(
                                              posonlyargs=[],
                                              args=[ast.arg(arg='self', annotation=None)],
                                              kw_defaults=[],
                                              kwonlyargs=[],
                                              defaults=[],
                                              vararg=None,
                                              kwarg=None),
                                          returns=ast.Name(id=t, ctx=ast.Load()),
                                          decorator_list=[],
                                          body=[ast.Return(
                                              value=ast.Attribute(
                                                  value=ast.Name(id='self', ctx=ast.Load()),
                                                  attr=ast.Name(id=ka, ctx=ast.Store()))
                                          )])
                cls.body.append(getFunc)
            clsbod.append(init)
            if additional_methods is not None:
                clsbod += additional_methods
        mod.body.append(cls)
        mod.body += additional_module_members

    def pretty_source(source):
        return ''.join(split_lines(source, maxline=65535))
    return astor.to_source(mod, pretty_source=pretty_source)


def filterContents(contents: Dict[str, List[Tuple[str, Optional[List[ast.AST]]]]], force_name: Optional[str] = None, skip_members: Optional[List[str]] = None) -> Dict[str, List[Tuple[str, Optional[List[ast.AST]]]]]:
    if skip_members is None:
        skip_members = []
    newcontents: Dict[str, List[Tuple[str, Optional[List[ast.AST]]]]] = {}
    for clsName, clsMembers in contents.items():
        if force_name:
            clsName = force_name
        newMembers: List[Tuple[str, Optional[ast.AST]]] = []
        for member, aststuff in clsMembers:
            if member in skip_members:
                continue
            newMembers.append((member, aststuff))
        newcontents[clsName] = newMembers
        break
    return newcontents


class ArgumentFetcher:
    'Fetches arguments from a ClassCreator if it has the desired length.'

    def __init__(self, length: int = 0, args: Optional[List[int]] = None) -> None:
        self.length: int = length
        self.args: List[int] = args or []

    def match(self, simped_args: List[ast.AST]) -> Optional[List[ast.AST]]:
        if len(simped_args) != self.length:
            return None
        o = []
        for i in self.args:
            o.append(simped_args[i])
        return o


def getArgDictFromJavaEnum(file: Path, fetchers: List[ArgumentFetcher]) -> OrderedDict[str, List[ast.AST]]:
    cu = javalang.parse.parse(file.read_text())
    values = collections.OrderedDict()
    for stmt in cu.types[0].body:
        if not isinstance(stmt, FieldDeclaration) or 'static' not in stmt.modifiers or 'public' not in stmt.modifiers:
            continue
        typeid: Optional[str] = None
        if len(stmt.type.dimensions) == 0:  # No arrays
            for vdec in stmt.declarators:
                if isinstance(vdec.initializer, ClassCreator):
                    simped = simplifyArgs(vdec.initializer.arguments)
                    for fetcher in fetchers:
                        if (m := fetcher.match(simped)) is not None:
                            values[vdec.name] = fetcher
                            break

    return values


def getEnumDictWithArgsFromJava(file: Path, fetchers: List[ArgumentFetcher]) -> Dict[str, List[Tuple[str, Optional[List[ast.AST]]]]]:
    cu = javalang.parse.parse(file.read_text())
    o: Dict[str, List[Tuple[str, Optional[List[ast.AST]]]]] = {}
    for t in cu.types:
        values: List[Tuple[str, Optional[List[ast.AST]]]] = []
        for stmt in t.body:
            if not isinstance(stmt, FieldDeclaration) or 'static' not in stmt.modifiers or 'public' not in stmt.modifiers:
                continue
            if len(stmt.type.dimensions) == 0:  # No arrays
                for vdec in stmt.declarators:
                    if isinstance(vdec.initializer, ClassCreator):
                        simped = simplifyArgs(vdec.initializer.arguments)
                        for fetcher in fetchers:
                            if (m := fetcher.match(simped)) is not None:
                                values.append((vdec.name, m))
                                break
        o[t.name] = values
    return o


'''
LTGIT_DIR = Path.home() / 'liliths-throne-public'
JAVA_SRC_DIR = LTGIT_DIR / 'src'
LILITH_PKG_DIR = JAVA_SRC_DIR / 'com' / 'lilithsthrone'

data = getEnumsIn(LILITH_PKG_DIR / 'game' / 'inventory' / 'InventorySlot.java')
print(transformToPythonEnum(data))
'''


def filePathThatExists(s: str) -> Path:
    p = Path(s)
    if not p.is_file():
        raise FileNotFoundError(str(p))
    return p


def dirPathThatExists(s: str) -> Path:
    p = Path(s)
    if not p.is_dir():
        raise FileNotFoundError(str(p))
    return p


def main():
    import argparse
    argp = argparse.ArgumentParser()

    argp.add_argument('inputfile', type=filePathThatExists, help='Input java file')
    argp.add_argument('outputfile', type=Path, default=None, nargs='?', help='Output Python file. Outputs to stdout if not specified.')
    argp.add_argument('--force-name', type=str, default=None, help='Change name of class')
    argp.add_argument('--scanner', choices=('enum', 'consts'), default='enum', help='What kind of code scanner to use')

    argp.add_argument('--const-type', type=str, default=None, help='What type to look for')

    argp.add_argument('--skip-member', type=str, nargs='*', default=[], help='Which potential enum members to ignore')

    args = argp.parse_args()

    contents = None
    if args.scanner == 'enum':
        contents = getEnumsInEnum(args.inputfile, args)
    elif args.scanner == 'consts':
        contents = getEnumsFromConsts(args.inputfile, args.const_type)

    contents = filterContents(contents, args.force_name, args.skip_member)

    o = transformToPythonEnum(contents)
    if args.outputfile:
        with open(args.outputfile, 'w') as f:
            f.write(o)
    else:
        print(o)


if __name__ == '__main__':
    main()

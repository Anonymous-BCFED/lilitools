import json
from typing import Any, Dict, TextIO

from buildtools.indentation import IndentWriter
from buildtools.maestro.enumwriters.types import EnumDefinition, EnumValueInfo

from buildtools.maestro.enumwriters.enumwriter import EnumWriter


class TypeScriptEnumWriter(EnumWriter):
    def __init__(self):
        super().__init__()

    def write(self, _w: TextIO, ed: EnumDefinition) -> None:
        ts_conf: Dict[str, Any] = ed.langconf.get('typescript', {})

        valcount = len(ed.values.keys())

        w = IndentWriter(_w,
                         indent_chars=ts_conf.get('indent_chars', '  '))

        w.writeline('/**')
        if ed.notes:
            for line in ed.notes.split('\n'):
                w.writeline(f' * {line.strip()}')
        w.writeline(f' * @enumdef: {ed.name}')
        w.writeline(f' */')
        base_type = ts_conf.get('type', 'number')
        with w.writeline(f'export class {ed.name} {{' if ts_conf.get('export', False) else f'class {ed.name} {{'):
            w.writeline(f'static readonly _DEFAULT: {base_type} = {ed.default!r};')
            w.writeline(f'static readonly _ERROR: {base_type} = {ed.error!r};')

            if ed.is_flags:
                w.writeline(f'static readonly NONE: number = 0;')

            vpak: EnumValueInfo
            for k, vpak in ed.values.items():
                if vpak.meaning:
                    w.writeline('/**')
                    w.writeline(' * ' + json.dumps(vpak.meaning.strip()))
                    w.writeline(' */')
                w.writeline(f'static readonly {vpak.id}: {base_type} = {json.dumps(vpak.value)};')

            if ed.is_flags:
                w.writeline()
                with w.writeline(f'static ValueToStrings(val: {base_type}): string {{'):
                    w.writeline('let o: string[] = [];')
                    w.writeline('let i: number = 0;')
                    w.writeline('let bitidx: number = 0;')
                    with w.writeline(f'for(bitidx = 0; i < {valcount}; i++) {{'):
                        w.writeline('switch ((1 << bitidx) & val) {')
                        written = set()
                        for k, v in ed.values.items():
                            jk = json.dumps(k)
                            jv = json.dumps(v.value)
                            if v.value in written:
                                continue
                            written.add(v.value)
                            with w.writeline(f'case {jv}:'):
                                w.writeline(f'o.push({jk});')
                                w.writeline(f'break;')
                        w.writeline('}')
                    w.writeline('}')
                    w.writeline('return o;')
                w.writeline('}')

                w.writeline()
                with w.writeline(f'static StringsToValue(valarr: {base_type}[]) {{'):
                    w.writeline('let o = 0;')
                    w.writeline(f'let e: {base_type};')
                    with w.writeline('for (e : valarr) {'):
                        w.writeline('o |= @StringToValue flagname')
                    w.writeline('}')
                    w.writeline('return o')
                w.writeline('}')

            w.writeline()
            with w.writeline('static ValueToString('):
                w.writeline(f'val: {base_type},')
                w.writeline('sep: string = ", ",')
                w.writeline('start_end: string = ""')
            with w.writeline(f'): string {{'):
                if ed.is_flags:
                    w.writeline('return this.ValueToStrings(val).join(sep)')
                else:
                    w.writeline('let o: string = "";')
                    with w.writeline('switch (val) {'):
                        written = set()
                        for k, v in ed.values.items():
                            if v.value in written:
                                continue
                            written.add(v.value)
                            jk = json.dumps(k)
                            jv = json.dumps(v.value)
                            with w.writeline(f'case {jv}:'):
                                w.writeline(f'o = {jk};')
                                w.writeline(f'break;')
                    w.writeline('}')
                    with w.writeline('if (start_end.length == 1) {'):
                        w.writeline('return start_end + o + start_end;')
                    w.writeline('}')
                    with w.writeline('if (start_end.length == 2) {'):
                        w.writeline('return start_end[0] + o + start_end[1];')
                    w.writeline('}')
                    w.writeline('return o')
            w.writeline('}')

            w.writeline()
            with w.writeline(f'static StringToValue(key: string): {base_type} {{'):
                with w.writeline('switch (key) {'):
                    written = set()
                    for k, v in ed.values.items():
                        if k in written:
                            continue
                        written.add(k)
                        jk = json.dumps(k)
                        jv = json.dumps(v.value)
                        with w.writeline(f'case {jk}:'):
                            w.writeline(f'return {jv};')
                w.writeline('}')
                w.writeline(f'return {json.dumps(ed.error)}')
            w.writeline('}')

            w.writeline()
            keys = ', '.join([json.dumps(x) for x in ed.values.keys()])
            with w.writeline('static Keys(): string[] {'):
                w.writeline(f'return [{keys}];')
            w.writeline('}')

            w.writeline()
            values = ', '.join([json.dumps(v.value)
                               for v in ed.values.values()])
            with w.writeline(f'static Values(): {base_type}[] {{'):
                w.writeline(f'return [{values}];')
            w.writeline('}')

            w.writeline()
            with w.writeline('static Count(): number {'):
                w.writeline(f'return {valcount};')
            w.writeline('}')

            if base_type == 'number':
                w.writeline()
                with w.writeline(f'static Min(): {base_type} {{'):
                    w.writeline(f'return {json.dumps(ed.min)};')
                w.writeline('}')

                w.writeline()
                with w.writeline(f'static Max(): {base_type} {{'):
                    w.writeline(f'return {json.dumps(ed.max)};')
                w.writeline('}')

            if ed.is_flags:
                allofem = 0
                for v in ed.values.values():
                    allofem |= int(v.value)
                w.writeline()
                with w.writeline(f'static All(): {base_type} {{'):
                    w.writeline(f'return {json.dumps(allofem)};')
                w.writeline('}')

            if base_type == 'number':
                with w.writeline(f'static Width(): {base_type} {{'):
                    w.writeline(f'return {json.dumps(ed.max.bit_length())};')
                w.writeline('}')
        w.writeline('}')
        w.writeline()

import os
from pathlib import Path
from typing import Any, Dict, List
from xml.etree.ElementTree import Comment

import click

from ruamel.yaml import YAML, CommentedMap

from analysis.savable_metadata import SavableMetadata
from analysis.utils import copy_ca_to  # isort: skip
yaml = YAML(typ='rt')


SAVABLES_DIR = Path('data') / 'savables'
filelist: List[Path] = []
with open(SAVABLES_DIR / '_index.yml', 'r') as f:
    filelist = [SAVABLES_DIR / Path(p) for p in yaml.load(f)['files']]
for savableYML in filelist:
    click.echo(f'{savableYML}... ', nl=False)
    try:
        savableTmp = savableYML.with_suffix('.tmp')
        with savableYML.open('r') as f:
            data = yaml.load(f)
        ca = data.ca
        s = SavableMetadata()
        s.deserialize(data)
        data = s.serialize()
        copy_ca_to(ca, data.ca)
        with savableTmp.open('w') as f:
            yaml.dump(data, f)
        click.secho('OK', fg='green')
        os.replace(savableTmp, savableYML)
    except Exception as e:
        click.secho('FAILED', fg='red')
        click.secho(e, fg='red')
        os.remove(savableTmp)

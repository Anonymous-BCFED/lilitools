import ast
import re
from pathlib import Path
from typing import Dict, FrozenSet, List, Set
from xml.dom import NotFoundErr

import javalang

from mkast.modulefactory import ModuleFactory
from _codegen import LTExpandedCodeGenerator
from buildtools.maestro.base_target import SingleBuildTarget
from enumFrom import simplifyArgs
from javalang.tree import ClassDeclaration, CompilationUnit, FieldDeclaration
from _codegen import writeGeneratedPythonWarningHeader

from _dev_regex import REG_ALL_CLOTHING

SETS_TO_COPY: FrozenSet[str] = frozenset(
    {
        "ALL_METAL",
        "ALL_WITH_METALS",
        "ALL",
        "BLACK_OR_WHITE",
        "DARK_SHADES",
        "DENIM",
        "KIMONO",
        "LEATHER",
        "LINGERIE",
        "MAID",
        "MILK_MAID",
        "SHADES_OF_GREY",
        # 'naturalSiliconeColours',
        # 'dyeSiliconeColours',
    }
)


class ColourListGenTarget(SingleBuildTarget):
    BT_LABEL = "GENERATE"

    def __init__(
        self,
        output: Path,
        colour_presets: Path,
        colour_preset_lists: Path,
        dependencies: List[str] = [],
    ) -> None:
        self.outpath: Path = output
        self.colour_presets_file: Path = colour_presets
        self.colour_preset_lists_file: Path = colour_preset_lists
        super().__init__(
            target=str(self.outpath),
            files=[str(self.colour_preset_lists_file), str(self.colour_presets_file)],
            dependencies=dependencies,
        )

    def getColoursMatching(self, mod: ast.Module, regex: re.Pattern) -> Set[str]:
        o = set()
        for modstmt in mod.body:
            if isinstance(modstmt, ast.ClassDef):
                for clsstmt in modstmt.body:
                    if isinstance(clsstmt, ast.Assign) and isinstance(
                        clsstmt.targets[0], ast.Name
                    ):
                        if regex.match(clsstmt.targets[0].id) is not None:
                            o.add(clsstmt.targets[0].id)
        return o

    def getValuesIn(self, cu: CompilationUnit, name: str) -> Set[str]:
        for jtype in cu.types:
            if isinstance(jtype, ClassDeclaration):
                fld: FieldDeclaration
                for fld in jtype.fields:
                    if "static" in fld.modifiers and "public" in fld.modifiers:
                        # print('TYPE: ', repr(fld.type))
                        # print('DECL: ', repr(fld.declarators))
                        if fld.type.name == "ArrayList":
                            for decl in fld.declarators:
                                if decl.name != name:
                                    continue
                                o = set()
                                for lst in simplifyArgs([decl.initializer]):
                                    for e in lst.elts:
                                        v = ""
                                        # print(repr(e))
                                        if isinstance(e, ast.Attribute):
                                            v = e.attr
                                        o.add(v)
                                return o
        raise NotFoundErr(name)

    def build(self) -> None:
        colourtree = ast.parse(
            self.colour_presets_file.read_text(), str(self.colour_presets_file)
        )
        cu: CompilationUnit = javalang.parse.parse(
            self.colour_preset_lists_file.read_text()
        )

        ALL_CLOTHING_COLOURS: Set[str] = self.getColoursMatching(
            colourtree, REG_ALL_CLOTHING
        )

        sets: Dict[str, Set[str]] = {}

        for name in sorted(ALL_CLOTHING_COLOURS):
            stripped_name: str = name[9:]
            sets[f"JUST_{stripped_name}"] = {name}

        for setID in SETS_TO_COPY:
            sets[setID] = self.getValuesIn(cu, setID)

        # Calculated
        ## from [LT]/src/com/lilithsthrone/utils/colours/ColourListPresets.java: static { @ T9Urv5YAsWh0CnL2RCW3CYcFUYMMdGLy9V1qV8+vvBwyd4C+10O8WTeJjEoJwf8XYKAvJXJ17yBFSGC8NGtwCA==
        sets["NOT_WHITE"] = sets["ALL"] - {"CLOTHING_WHITE"}
        sets["NOT_BLACK"] = sets["ALL"] - {"CLOTHING_BLACK", "CLOTHING_BLACK_JET"}
        sets["DEBUG_ALL"] = sets["ALL_WITH_METALS"] | self.getValuesIn(cu, "DEBUG_ALL")

        mb = ModuleFactory()
        mb.addImportFrom("typing", ["FrozenSet"])
        mb.addImportFrom("typing", ["Set"])
        mb.addImportFrom("frozendict", ["frozendict"])
        FROZENSET_STR = ast.Subscript(
            value=ast.Name("FrozenSet"), slice=ast.Index(value=ast.Name("str"))
        )
        FROZENDICT_STR_SET_STR = ast.Subscript(
            value=ast.Name("frozendict"),
            slice=ast.Tuple(
                elts=[
                    ast.Name("str"),
                    ast.Subscript(value=ast.Name("Set"), slice=ast.Name("str")),
                ]
            ),
        )
        all_ = set()
        ast_all_list = ast.List(elts=[])
        mb.addVariableDecl("__all__", None, ast_all_list)

        setNames: Set[str] = set(sets.keys())
        for k, v in sorted(sets.items()):
            call = ast.Call(
                func=ast.Name("frozenset"),
                args=[ast.Set(elts=[ast.Constant(x) for x in sorted(v)])],
                keywords=[],
            )
            mb.addVariableDecl(k, FROZENSET_STR, call)

        mb.addVariableDecl(
            "ALL_WITH_METAL", FROZENSET_STR, ast.Name(id="ALL_WITH_METALS")
        )
        mb.addVariableDecl("ALL_METALS", FROZENSET_STR, ast.Name(id="ALL_METAL"))
        setNames |= {"ALL_WITH_METAL", "ALL_METALS"}

        ALL_COLOUR_SETS = ast.Dict(
            keys=[ast.Constant(k) for k in sorted(setNames)],
            values=[ast.Name(id=k) for k in sorted(setNames)],
        )
        all_ |= setNames
        mb.addVariableDecl("ALL_COLOUR_SETS", FROZENDICT_STR_SET_STR, ALL_COLOUR_SETS)
        all_.add("ALL_COLOUR_SETS")
        mb.expressions.append(
            ast.FunctionDef(
                name="getColoursByPresetId",
                args=ast.arguments(
                    posonlyargs=[],
                    args=[
                        ast.arg(
                            arg="id_",
                            annotation=ast.Name("str"),
                        ),
                    ],
                    vararg=None,
                    kwonlyargs=[],
                    kw_defaults=[],
                    kwarg=None,
                    defaults=[],
                ),
                decorator_list=[],
                returns=ast.Subscript(
                    value=ast.Name("Set"),
                    slice=ast.Name("str"),
                ),
                body=[
                    ast.Return(
                        value=ast.Subscript(
                            value=ast.Name("ALL_COLOUR_SETS"),
                            slice=ast.Name("id_"),
                        )
                    ),
                ],
            )
        )

        all_.add("getColoursByPresetId")
        ast_all_list.elts = [ast.Constant(x) for x in sorted(all_)]

        cstr = LTExpandedCodeGenerator().generate(mb.generate())
        with self.outpath.open("w") as f:
            writeGeneratedPythonWarningHeader(f,'Preset Colour Lists', Path(__file__))
            f.write(cstr)


def main():
    from buildtools.maestro import BuildMaestro
    from _utils import file2builddir

    bm = BuildMaestro(file2builddir(__file__))
    bm.add(
        ColourListGenTarget(
            Path("tmp/colorsets.py"),
            Path("lilitools/saves/enums/preset_colour.py"),
            Path(
                "lib/liliths-throne-public/src/com/lilithsthrone/utils/colours/ColourListPresets.java"
            ),
        )
    )
    bm.as_app()


if __name__ == "__main__":
    main()

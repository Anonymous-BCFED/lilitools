import ast
import collections
from enum import IntEnum, auto
from pathlib import Path
import re
from typing import Dict, List, Optional, OrderedDict, Tuple
from typing_extensions import deprecated

from analysis.clothinggrabber import GrabClothingTarget
from analysis.sfxgrabber import GrabSFXTarget
from buildtools.maestro import BuildMaestro
from buildtools.maestro.base_target import SingleBuildTarget
from buildtools.maestro.fileio import CopyFileTarget
from analysis.weapongrabber import GrabWeaponTarget
from analysis.gen_arm_type_enum import KnownArmTypeEnumGenerator
from analysis.gen_clothing_enum import KnownClothingEnumGenerator
from analysis.gen_items_enum import KnownItemsEnumGenerator
from analysis.gen_subspecies_enum import KnownSubspeciesEnumGenerator
from genKnownEffects import GenerateEffectList
from genPerkList import GeneratePerkList
from genSlaveJobList import GenerateSlaveJobList
from enumFrom import (
    dirPathThatExists,
    filterContents,
    getEnumsFromConsts,
    getEnumsInEnum,
    transformToPythonEnum,
)
from genColourLists import ColourListGenTarget

ENUM_DATA_DIR: Path = Path("data") / "enums"
DOCS_SRC_DIR: Path = Path("docs-src") / "docs"


class EScanner(IntEnum):
    ENUM = auto()
    CONSTS = auto()


class EnumFromJavaTarget(SingleBuildTarget):
    BT_LABEL = "ENUM FROM"

    def __init__(
        self, outputPythonFile: Path, inputJavaFile: Path, dependencies: List[str] = []
    ):
        self.outputPythonFile: Path = outputPythonFile
        self.inputJavaFile: Path = inputJavaFile
        self.scanner: EScanner = EScanner.ENUM
        self.skipMembers: List[str] = []
        self.forceName: Optional[str] = None
        self.filterConstType: Optional[str] = None
        self.generateAttrs: Optional[OrderedDict[str, Optional[str]]] = None
        self.additional_methods: Optional[List[ast.FunctionDef]] = None
        self.additional_module_members: List[ast.AST] = []
        self.additional_imports: List[ast.Import | ast.ImportFrom] = []
        super().__init__(
            target=str(outputPythonFile),
            files=[str(inputJavaFile)],
            dependencies=dependencies,
        )

    def addAttr(self, attrID: str, attrType: str) -> None:
        if self.generateAttrs is None:
            self.generateAttrs = collections.OrderedDict()
        self.generateAttrs[attrID] = attrType

    def skipAttr(self, attrID: str) -> None:
        if self.generateAttrs is None:
            self.generateAttrs = collections.OrderedDict()
        self.generateAttrs[attrID] = None

    def addMethod(
        self, name: str, args: List[ast.arg] = [], kwargs: List[ast.keyword] = []
    ) -> ast.FunctionDef:
        f = ast.FunctionDef(
            name=name,
            args=ast.arguments(
                args=args,
                vararg=None,
                kwonlyargs=[],
                kw_defaults=[],
                kwarg=None,
                defaults=[],
            ),
            body=[],
            decorator_list=[],
            returns=None,
        )
        if self.additional_methods is None:
            self.additional_methods = [f]
        else:
            self.additional_methods.append(f)
        return f

    def get_config(self):
        return {
            "outputPythonFile": str(self.outputPythonFile),
            "inputJavaFile": str(self.inputJavaFile),
            "scanner": self.scanner.name,
            "skipMembers": self.skipMembers,
            "forceName": self.forceName,
            "filterConstType": self.filterConstType,
            "generateAttrs": dict(self.generateAttrs) if self.generateAttrs else None,
        }

    def scan(self) -> Dict[str, List[Tuple[str, Optional[List[ast.AST]]]]]:
        pca = self.generateAttrs is not None and len(self.generateAttrs) > 0
        if self.scanner == EScanner.ENUM:
            return getEnumsInEnum(self.inputJavaFile, passCtorArgs=pca)
        elif self.scanner == EScanner.CONSTS:
            return getEnumsFromConsts(
                self.inputJavaFile, const_type=self.filterConstType, passCtorArgs=pca
            )
        return None

    def build(self):
        contents = self.scan()
        assert len(contents) > 0
        contents = filterContents(contents, self.forceName, self.skipMembers)
        with open(self.outputPythonFile, "w") as f:
            f.write(
                transformToPythonEnum(
                    contents,
                    attrs_for_instance=self.generateAttrs,
                    additional_methods=self.additional_methods,
                    additional_imports=self.additional_imports,
                    additional_module_members=self.additional_module_members,
                )
            )


def addMedianToEnumFromJavaTarget(
    e: EnumFromJavaTarget, attrMin: str, attrMax: str, nameOfMedianMethod:str
) -> None:
    """
    @property
    @lru_cache
    def median(self)->float:
        return self.min + (self.max - self.min) / 2
    """
    e.additional_imports.append(ast.ImportFrom(module="functools", names=[ast.Name("lru_cache")], level=0))
    median: ast.FunctionDef = e.addMethod(nameOfMedianMethod, args=[ast.arg("self")])
    median.decorator_list = [ast.Name("property"), ast.Name("lru_cache")]
    median.returns = ast.Name("float")
    median.body = [
        ast.Return(
            ast.BinOp(
                left=ast.Attribute(value=ast.Name("self"), attr=attrMin),
                op=ast.Add(),
                right=ast.BinOp(
                    left=ast.BinOp(
                        left=ast.Attribute(value=ast.Name("self"), attr=attrMax),
                        op=ast.Sub(),
                        right=ast.Attribute(value=ast.Name("self"), attr=attrMin),
                    ),
                    op=ast.Mult(),
                    right=ast.Constant(2),
                ),
            )
        )
    ]


def markMethodAsDeprecated(e: EnumFromJavaTarget, m: ast.FunctionDef, msg: str) -> None:
    e.additional_imports.append(
        ast.ImportFrom(
            module="typing_extensions", names=[ast.alias(name="deprecated")], level=0
        )
    )
    m.decorator_list.append(
        ast.Call(func=ast.Name(id="deprecated"), args=[ast.Constant(msg)], keywords=[])
    )


def genASTFromTFModListAdds(path: Path, regex: str, group: int | str) -> ast.AST:
    o: ast.List = ast.List(elts=[])
    with open(path, "r") as f:
        for line in f:
            if m := re.search(regex, line):
                o.elts.append(
                    ast.Attribute(value=ast.Name("ETFModifier"), attr=m[group])
                )

    return o


from _utils import file2builddir

bm = BuildMaestro(file2builddir(__file__))
argp = bm.build_argparser()
argp.add_argument("--liliths-throne-repo", "-L", type=dirPathThatExists)
args = bm.parse_args(argp)

LT_REPO_DIR: Path = args.liliths_throne_repo
LT_SRC_PKG = LT_REPO_DIR / "src" / "com" / "lilithsthrone"

LILIPY_DIR: Path = Path.cwd() / "lilitools"
DATA_DIR: Path = Path.cwd() / "data"

eItemTags = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "items" / "enums" / "item_tags.py",
        LT_SRC_PKG / "game" / "inventory" / "ItemTag.java",
    )
)
eItemTags.forceName = "EItemTags"

eRarity = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "items" / "enums" / "rarity.py",
        LT_SRC_PKG / "game" / "inventory" / "Rarity.java",
    )
)
eRarity.forceName = "ERarity"

eWeather = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "enums" / "weather.py",
        LT_SRC_PKG / "world" / "Weather.java",
    )
)
eWeather.forceName = "EWeather"

eWorld = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "enums" / "worlds.py",
        LT_SRC_PKG / "world" / "WorldType.java",
    )
)
eWorld.forceName = "EWorld"
eWorld.scanner = EScanner.CONSTS

# eSlaveJob = bm.add(EnumFromJavaTarget(
#     LILIPY_DIR / 'saves' / 'slavery' / 'enums' / 'slave_jobs.py',
#     LT_SRC_PKG / 'game' / 'occupantManagement' / 'slave' / 'SlaveJob.java'))
# eSlaveJob.forceName = 'ESlaveJob'

eSubspecies = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "enums" / "subspecies.py",
        LT_SRC_PKG / "game" / "character" / "race" / "Subspecies.java",
    )
)
eSubspecies.forceName = "ESubspecies"
eSubspecies.scanner = EScanner.CONSTS
eSubspecies.skipMembers = ["allSubspecies", "subspeciesToIdMap", "idToSubspeciesMap"]

eFluidType = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "enums" / "fluid_type.py",
        LT_SRC_PKG / "game" / "character" / "body" / "types" / "FluidType.java",
    )
)
eFluidType.forceName = "EFluidType"
eFluidType.scanner = EScanner.CONSTS

eDisplacementType = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "inventory" / "enums" / "displacement_type.py",
        LT_SRC_PKG / "game" / "inventory" / "clothing" / "DisplacementType.java",
    )
)
eDisplacementType.forceName = "EDisplacementType"

eClothingAccess = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "inventory" / "enums" / "clothing_access.py",
        LT_SRC_PKG / "game" / "inventory" / "clothing" / "ClothingAccess.java",
    )
)
eClothingAccess.forceName = "EClothingAccess"

eCoverableArea = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "enums" / "coverable_area.py",
        LT_SRC_PKG / "game" / "character" / "body" / "CoverableArea.java",
    )
)
eCoverableArea.forceName = "ECoverableArea"

eDamageType = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "combat" / "enums" / "damage_type.py",
        LT_SRC_PKG / "game" / "combat" / "DamageType.java",
    )
)
eDamageType.forceName = "EDamageType"

ePlaceType = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "world" / "enums" / "place_type.py",
        LT_SRC_PKG / "world" / "places" / "PlaceType.java",
    )
)
ePlaceType.forceName = "EPlaceType"
ePlaceType.scanner = EScanner.CONSTS

ePlaceUpgrade = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "world" / "enums" / "place_upgrade.py",
        LT_SRC_PKG / "world" / "places" / "PlaceUpgrade.java",
    )
)
ePlaceUpgrade.forceName = "EPlaceUpgrade"
ePlaceUpgrade.scanner = EScanner.CONSTS

eFluidType = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "fluids" / "enums" / "fluid_types.py",
        LT_SRC_PKG / "game" / "character" / "body" / "types" / "FluidType.java",
    )
)
eFluidType.forceName = "EFluidType"
eFluidType.scanner = EScanner.CONSTS

eFluidFlavour = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "fluids" / "enums" / "fluid_flavours.py",
        LT_SRC_PKG / "game" / "character" / "body" / "valueEnums" / "FluidFlavour.java",
    )
)
eFluidFlavour.forceName = "EFluidFlavour"

eFluidModifier = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "fluids" / "enums" / "fluid_modifier.py",
        LT_SRC_PKG
        / "game"
        / "character"
        / "body"
        / "valueEnums"
        / "FluidModifier.java",
    )
)
eFluidModifier.forceName = "EFluidModifier"

eWorldType = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "world" / "enums" / "world_type.py",
        LT_SRC_PKG / "world" / "WorldType.java",
    )
)
eWorldType.forceName = "EWorldType"
eWorldType.scanner = EScanner.CONSTS

eSexualOrientation = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "enums" / "sexual_orientation.py",
        LT_SRC_PKG / "game" / "character" / "persona" / "SexualOrientation.java",
    )
)
eSexualOrientation.forceName = "ESexualOrientation"

eOccupation = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "enums" / "occupation.py",
        LT_SRC_PKG / "game" / "character" / "persona" / "Occupation.java",
    )
)
eOccupation.forceName = "EOccupation"

ePersonalityTrait = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "enums" / "personality_trait.py",
        LT_SRC_PKG / "game" / "character" / "persona" / "PersonalityTrait.java",
    )
)
ePersonalityTrait.forceName = "EPersonalityTrait"

eCombatBehaviour = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "enums" / "combat_behaviour.py",
        LT_SRC_PKG / "game" / "combat" / "CombatBehaviour.java",
    )
)
eCombatBehaviour.forceName = "ECombatBehaviour"

# No
# eGender = bm.add(EnumFromJavaTarget(
#    LILIPY_DIR / 'saves' / 'character' / 'enums' / 'gender.py',
#    LT_SRC_PKG / 'game' / 'character' / 'gender' / 'Gender.java'
# ))
# eGender.forceName = 'EGender'

eAttribute: EnumFromJavaTarget = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "enums" / "attribute.py",
        LT_SRC_PKG / "game" / "character" / "attributes" / "Attribute.java",
    )
)
eAttribute.forceName = "EAttribute"
eAttribute.scanner = EScanner.CONSTS
"""public AbstractAttribute(boolean percentage,
			int baseValue,
			int lowerLimit,
			int upperLimit,
			String name,
			String nameAbbreviation,
			String pathName,
			Colour colour,
			String positiveEnchantment,
			String negativeEnchantment,
			List<String> extraEffects) {"""
eAttribute.addAttr("percentage", "bool")
eAttribute.addAttr("baseValue", "int")
eAttribute.addAttr("lowerLimit", "int")
eAttribute.addAttr("upperLimit", "int")
eAttribute.addAttr("name", "str")
eAttribute.addAttr("nameAbbreviation", "str")
# eAttribute.addAttr("pathName", "str")
eAttribute.skipAttr("pathName")
eAttribute.skipAttr("colour")
eAttribute.addAttr("positiveEnchantment", "str")
eAttribute.addAttr("negativeEnchantment", "str")
eAttribute.skipAttr("extraEffects")
eAttribute.skipMembers = [
    "attributeToIdMap",
    "idToAttributeMap",
    "allAttributes",
    "racialAttributes",
]


ePerkCategory = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "enums" / "perk_category.py",
        LT_SRC_PKG / "game" / "character" / "effects" / "PerkCategory.java",
    )
)
ePerkCategory.forceName = "EPerkCategory"

eBodyHair = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "enums" / "body_hair.py",
        LT_SRC_PKG / "game" / "character" / "body" / "valueEnums" / "BodyHair.java",
    )
)
eBodyHair.forceName = "EBodyHair"

eBodyMaterial = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "enums" / "body_material.py",
        LT_SRC_PKG / "game" / "character" / "body" / "valueEnums" / "BodyMaterial.java",
    )
)
eBodyMaterial.forceName = "EBodyMaterial"

eGenitalArrangement = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "enums" / "genital_arrangement.py",
        LT_SRC_PKG
        / "game"
        / "character"
        / "body"
        / "valueEnums"
        / "GenitalArrangement.java",
    )
)
eGenitalArrangement.forceName = "EGenitalArrangement"

eBreastShape = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "enums" / "breast_shape.py",
        LT_SRC_PKG / "game" / "character" / "body" / "valueEnums" / "BreastShape.java",
    )
)
eBreastShape.forceName = "EBreastShape"

eAreolaeShape = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "enums" / "areolae_shape.py",
        LT_SRC_PKG / "game" / "character" / "body" / "valueEnums" / "AreolaeShape.java",
    )
)
eAreolaeShape.forceName = "EAreolaeShape"

ePenetratorModifiers = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "enums" / "penetrator_modifier.py",
        LT_SRC_PKG
        / "game"
        / "character"
        / "body"
        / "valueEnums"
        / "PenetrationModifier.java",
    )
)
ePenetratorModifiers.forceName = "EPenetratorModifier"

eOrificeModifier = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "enums" / "orifice_modifier.py",
        LT_SRC_PKG
        / "game"
        / "character"
        / "body"
        / "valueEnums"
        / "OrificeModifier.java",
    )
)
eOrificeModifier.forceName = "EOrificeModifier"

# eInventorySlot = bm.add(EnumFromJavaTarget(
#     LILIPY_DIR / 'saves' / 'enums' / 'inventory_slot.py',
#     LT_SRC_PKG / 'game' / 'inventory' / 'InventorySlot.java'
# ))
# eInventorySlot.forceName = 'EInventorySlot'

eFetishDesire = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "enums" / "fetish_desire.py",
        LT_SRC_PKG / "game" / "character" / "fetishes" / "FetishDesire.java",
    )
)
eFetishDesire.forceName = "EFetishDesire"

ePronounType = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "enums" / "pronoun_type.py",
        LT_SRC_PKG / "game" / "character" / "gender" / "PronounType.java",
    )
)
ePronounType.forceName = "EPronounType"

ePresetColours = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "enums" / "preset_colour.py",
        LT_SRC_PKG / "utils" / "colours" / "PresetColour.java",
    )
)
ePresetColours.forceName = "EPresetColour"
ePresetColours.scanner = EScanner.CONSTS
ePresetColours.importLists = True
ePresetColours.skipMembers = [
    "allCoveringColours",
    "allCoveringColoursWithClear",
    "allMakeupColours",
    "allSkinColours",
    "antlerColours",
    "demonSkinColours",
    "dyeDemonIrisColours",
    "dyeFeatherColours",
    "dyeIrisColours",
    "dyePredatorIrisColours",
    "dyePupilColours",
    "dyeScleraColours",
    "dyeSiliconeColours",
    "dyeSlimeColours",
    "hornColours",
    "humanSkinColours",
    "naturalDemonIrisColours",
    "naturalFeatherColours",
    "naturalFurColours",
    "naturalHairColours",
    "naturalIrisColours",
    "naturalPredatorIrisColours",
    "naturalPupilColours",
    "naturalScaleColours",
    "naturalScleraColours",
    "naturalSiliconeColours",
    "naturalSlimeColours",
    "ratSkinColours",
]

eTFPotency = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "enums" / "tf_potency.py",
        LT_SRC_PKG / "game" / "inventory" / "enchanting" / "TFPotency.java",
    )
)
eTFPotency.forceName = "ETFPotency"

eTFModifier: EnumFromJavaTarget = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "enums" / "tf_modifier.py",
        LT_SRC_PKG / "game" / "inventory" / "enchanting" / "TFModifier.java",
    )
)
eTFModifier.additional_imports += [
    ast.ImportFrom(module="typing", names=[ast.alias("List")], level=0)
]
eTFModifier.forceName = "ETFModifier"
eTFModifier.additional_module_members += [
    ast.AnnAssign(
        target=ast.Name("RACIAL_BODY_PART_TFMODS"),
        annotation=ast.Subscript(ast.Name("List"), ast.Name("ETFModifier")),
        value=genASTFromTFModListAdds(
            LT_SRC_PKG / "game" / "inventory" / "enchanting" / "TFModifier.java",
            r"TFRacialBodyPartsList\.add\(([A-Z_]+)\);",
            1,
        ),
        simple=1,
    )
]

eAssSize = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "body" / "enums" / "ass_size.py",
        LT_SRC_PKG / "game" / "character" / "body" / "valueEnums" / "AssSize.java",
    )
)
eAssSize.forceName = "EAssSize"

eHipSize = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "body" / "enums" / "hip_size.py",
        LT_SRC_PKG / "game" / "character" / "body" / "valueEnums" / "HipSize.java",
    )
)
eHipSize.forceName = "EHipSize"

eWetness = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "body" / "enums" / "wetness.py",
        LT_SRC_PKG / "game" / "character" / "body" / "valueEnums" / "Wetness.java",
    )
)
eWetness.forceName = "EWetness"

eDepth = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "body" / "enums" / "depth.py",
        LT_SRC_PKG / "game" / "character" / "body" / "valueEnums" / "OrificeDepth.java",
    )
)
eDepth.forceName = "EDepth"

eElasticity = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "body" / "enums" / "elasticity.py",
        LT_SRC_PKG
        / "game"
        / "character"
        / "body"
        / "valueEnums"
        / "OrificeElasticity.java",
    )
)
eElasticity.forceName = "EElasticity"

ePlasticity = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "body" / "enums" / "plasticity.py",
        LT_SRC_PKG
        / "game"
        / "character"
        / "body"
        / "valueEnums"
        / "OrificePlasticity.java",
    )
)
ePlasticity.forceName = "EPlasticity"

eEyeShape = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "body" / "enums" / "eye_shape.py",
        LT_SRC_PKG / "game" / "character" / "body" / "valueEnums" / "EyeShape.java",
    )
)
eEyeShape.forceName = "EEyeShape"

eHairStyle = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "body" / "enums" / "hair_style.py",
        LT_SRC_PKG / "game" / "character" / "body" / "valueEnums" / "HairStyle.java",
    )
)
eHairStyle.forceName = "EHairStyle"

eFootStructure = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "body" / "enums" / "foot_structure.py",
        LT_SRC_PKG
        / "game"
        / "character"
        / "body"
        / "valueEnums"
        / "FootStructure.java",
    )
)
eFootStructure.forceName = "EFootStructure"


eAreolaeShape = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "body" / "enums" / "areolae_shape.py",
        LT_SRC_PKG / "game" / "character" / "body" / "valueEnums" / "AreolaeShape.java",
    )
)
eAreolaeShape.forceName = "EAreolaeShape"

eNippleShape = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "body" / "enums" / "nipple_shape.py",
        LT_SRC_PKG / "game" / "character" / "body" / "valueEnums" / "NippleShape.java",
    )
)
eNippleShape.forceName = "ENippleShape"

eTongueModifier = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "body" / "enums" / "tongue_modifier.py",
        LT_SRC_PKG
        / "game"
        / "character"
        / "body"
        / "valueEnums"
        / "TongueModifier.java",
    )
)
eTongueModifier.forceName = "ETongueModifier"

ePenetratorGirth = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "body" / "enums" / "penetrator_girth.py",
        LT_SRC_PKG
        / "game"
        / "character"
        / "body"
        / "valueEnums"
        / "PenetrationGirth.java",
    )
)
ePenetratorGirth.forceName = "EPenetratorGirth"

"""
eFluidExpulsion = bm.add(EnumFromJavaTarget(
    LILIPY_DIR / 'saves' / 'character' / 'body' / 'enums' / 'fluid_expulsion.py',
    LT_SRC_PKG / 'game' / 'character' / 'body' / 'valueEnums' / 'FluidExpulsion.java'
))
eFluidExpulsion.forceName = 'EFluidExpulsion'
"""

eIntelligenceLevel: EnumFromJavaTarget = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "enums" / "intelligence_level.py",
        LT_SRC_PKG / "game" / "character" / "attributes" / "IntelligenceLevel.java",
    )
)
eIntelligenceLevel.forceName = "EIntelligenceLevel"
# eIntelligenceLevel.scanner = EScanner.CONSTS
"(String name, int minimumValue, int maximumValue, Colour colour)"
eIntelligenceLevel.addAttr("name", "str")
eIntelligenceLevel.addAttr("minimumValue", "int")
eIntelligenceLevel.addAttr("maximumValue", "int")
eIntelligenceLevel.skipAttr("colour")
addMedianToEnumFromJavaTarget(eIntelligenceLevel,"minimumValue",'maximumValue','medianValue')

eKnownStatusEffects: GrabSFXTarget = bm.add(
    GrabSFXTarget(
        LILIPY_DIR / "game" / "enums" / "known_status_effect.py",
        LT_SRC_PKG / "game" / "character" / "effects" / "StatusEffect.java",
        Path("data") / "dumps" / "statusEffects.json",
        LILIPY_DIR / "saves" / "character" / "enums" / "attribute.py",
        LT_REPO_DIR / "res" / "statusEffects",
    )
)
eKnownStatusEffects.enumName = "EKnownStatusEffect"
eKnownStatusEffects.imports.append(
    "from lilitools.saves.character.enums.attribute import EAttribute"
)
eKnownStatusEffects.fields["name"] = "str"
eKnownStatusEffects.fields["pathName"] = "str"
eKnownStatusEffects.fields["beneficial"] = "bool"
eKnownStatusEffects.fields["effects"] = "dict"
eKnownStatusEffects.ignore.add("statusEffectToIdMap")
eKnownStatusEffects.ignore.add("idToStatusEffectMap")

eCapacity: EnumFromJavaTarget = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "body" / "enums" / "capacity.py",
        LT_SRC_PKG / "game" / "character" / "body" / "valueEnums" / "Capacity.java",
    )
)
eCapacity.forceName = "ECapacity"

eFluidRegeneration: EnumFromJavaTarget = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "enums" / "fluid_regeneration.py",
        LT_SRC_PKG
        / "game"
        / "character"
        / "body"
        / "valueEnums"
        / "FluidRegeneration.java",
    )
)
eFluidRegeneration.forceName = "EFluidRegeneration"
# eFluidRegeneration.scanner = EScanner.CONSTS
"""private FluidRegeneration(String name, int minimumValue, int maximumValue, String verb, Colour colour) {"""
eFluidRegeneration.addAttr("name", "str")
eFluidRegeneration.addAttr("min", "int")
eFluidRegeneration.addAttr("max", "int")
eFluidRegeneration.addAttr("verb", "str")
eFluidRegeneration.skipAttr("colour")
addMedianToEnumFromJavaTarget(eFluidRegeneration, "min", "max",'median')

# eKnownClothing: GrabClothingTarget = bm.add(GrabClothingTarget(
#     LILIPY_DIR / 'saves' / 'items' / 'enums' / 'known_clothing.py',
#     LT_SRC_PKG / 'game' / 'inventory' / 'clothing' / 'ClothingType.java',
#     LT_REPO_DIR / 'res' / 'clothing'
# ))
# eKnownClothing.enumName = 'EKnownClothing'
# eKnownClothing.imports.append('import lilitools.saves.colour_preset_lists as colour_preset_lists')
# eKnownClothing.imports.append('from lilitools.saves.enums.preset_colour import EPresetColour')
# eKnownClothing.imports.append('from lilitools.saves.items.item_effect import ItemEffect')
# eKnownClothing.imports.append('from typing import List,Set')
eKnownClothing = bm.add(
    KnownClothingEnumGenerator(
        LILIPY_DIR / "saves" / "items" / "enums" / "known_clothing.py",
        Path("data") / "dumps" / "clothing",
    )
)

eKnownItems = bm.add(
    KnownItemsEnumGenerator(
        LILIPY_DIR / "saves" / "items" / "enums" / "known_items.py",
        Path("data") / "dumps" / "items.json",
    )
)

eKnownArmTypes = bm.add(
    KnownArmTypeEnumGenerator(
        LILIPY_DIR / "saves" / "character" / "body" / "enums" / "known_arm_types.py",
        Path("data") / "dumps" / "bodyparts" / "arms.json",
    )
)

eKnownSubSpecies = bm.add(
    KnownSubspeciesEnumGenerator(
        LILIPY_DIR / "saves" / "character" / "body" / "enums" / "known_subspecies.py",
        Path("data") / "dumps" / "subspecies.json",
    )
)

eKnownWeapons: GrabWeaponTarget = bm.add(
    GrabWeaponTarget(
        LILIPY_DIR / "saves" / "items" / "enums" / "known_weapons.py",
        LT_SRC_PKG / "game" / "inventory" / "weapon" / "WeaponType.java",
        LT_REPO_DIR / "res" / "weapons",
    )
)
eKnownWeapons.enumName = "EKnownWeapons"
eKnownWeapons.imports.append(
    "import lilitools.saves.colour_preset_lists as colour_preset_lists"
)
eKnownWeapons.imports.append(
    "from lilitools.saves.enums.preset_colour import EPresetColour"
)
eKnownWeapons.imports.append("from lilitools.saves.items.item_effect import ItemEffect")
eKnownWeapons.imports.append("from typing import List,Set")

colourSets: ColourListGenTarget = bm.add(
    ColourListGenTarget(
        LILIPY_DIR / "saves" / "colour_preset_lists.py",
        Path(ePresetColours.target),
        LT_SRC_PKG / "utils" / "colours" / "ColourListPresets.java",
        dependencies=[ePresetColours.target],
    )
)
# eItemType = bm.add(EnumFromJavaTarget(
#     LILIPY_DIR / 'saves' / 'enums' / 'itemtype.py',
#     LT_SRC_PKG / 'game' / 'inventory' / 'item' / 'ItemType.java'))
# eItemType.forceName = 'EKnownItemType'
# eItemType.scanner = EScanner.CONSTS


eKnownPerks: GeneratePerkList = bm.add(
    GeneratePerkList(
        DATA_DIR / "dumps" / "perks.json",
        LILIPY_DIR / "saves" / "character" / "enums" / "known_perks.py",
    )
)

genEffects: GenerateEffectList = bm.add(
    GenerateEffectList(
        DATA_DIR / "dumps" / "effects.json",
        DOCS_SRC_DIR / "ref" / "lt" / "known_effects",
        LILIPY_DIR / "saves" / "enums" / "known_effects.py",
        DATA_DIR / "effect_names.yml",
        DATA_DIR / "effect_names.csv",
    )
)

"""
private SlaveJob(
    Colour colour,
    float hourlyEventChance,
    int slaveLimit,
    float hourlyStaminaDrain,
    String nameFeminine,
    String nameMasculine,
    String description,
    float affectionGain,
    float obedienceGain,
    int income,
    float affectionIncomeModifier,
    float obedienceIncomeModifier,
    List<SlaveJobSetting> mutualSettings,
    List<SlaveJobSetting> defaultMutualSettings,
    Map<String, List<SlaveJobSetting>> mutuallyExclusiveSettings,
    List<SlaveJobSetting> defaultMutuallyExclusiveSettings,
    List<SlaveJobFlag> flags,
    AbstractWorldType worldLocation,
    AbstractPlaceType placeLocation) {
"""
# eSlaveJobs: EnumFromJavaTarget = bm.add(EnumFromJavaTarget(
#     LILIPY_DIR / 'saves' / 'slavery' / 'enums' / 'slave_jobs.py',
#     LT_SRC_PKG / 'game' / 'occupantManagement' / 'slave' / 'SlaveJob.java'
# ))
# eSlaveJobs.scanner = EScanner.ENUM
# eSlaveJobs.forceName = 'ESlaveJob'
# eSlaveJobs.addAttr('colour','str')
# eSlaveJobs.addAttr('hourlyEventChance','float')
# eSlaveJobs.addAttr('slaveLimit','int')
# eSlaveJobs.addAttr('hourlyStaminaDrain','float')
# eSlaveJobs.addAttr('nameFeminine','str')
# eSlaveJobs.addAttr('nameMasculine','str')
# eSlaveJobs.addAttr('description','str')
# eSlaveJobs.addAttr('affectionGain','float')
# eSlaveJobs.addAttr('obedienceGain','float')
# eSlaveJobs.addAttr('income','int')
# eSlaveJobs.addAttr('affectionIncomeModifier','float')
# eSlaveJobs.addAttr('obedienceIncomeModifier','float')
# eSlaveJobs.skipAttr('mutualSettings')
# eSlaveJobs.skipAttr('defaultMutualSettings')
# eSlaveJobs.skipAttr('mutuallyExclusiveSettings')
# eSlaveJobs.skipAttr('defaultMutuallyExclusiveSettings')
# eSlaveJobs.skipAttr('flags')
# eSlaveJobs.skipAttr('worldLocation')
# eSlaveJobs.skipAttr('placeLocation')
eSlaveJobs: GenerateSlaveJobList = bm.add(
    GenerateSlaveJobList(
        DATA_DIR / "dumps" / "slavejobs.json",
        LILIPY_DIR / "saves" / "slavery" / "enums" / "slave_jobs.py",
        Path("web") / "src" / "coffee" / "slave_jobs.coffee",
        Path("web") / "src" / "typescript" / "slave_jobs.ts",
    )
)

eSlaveJobSetting: EnumFromJavaTarget = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "slavery" / "enums" / "slave_job_settings.py",
        LT_SRC_PKG / "game" / "occupantManagement" / "slave" / "SlaveJobSetting.java",
    )
)
eSlaveJobSetting.scanner = EScanner.ENUM
eSlaveJobSetting.forceName = "ESlaveJobSettings"

eMuscle: EnumFromJavaTarget = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "body" / "enums" / "muscle.py",
        LT_SRC_PKG / "game" / "character" / "body" / "valueEnums" / "Muscle.java",
    )
)
eMuscle.scanner = EScanner.ENUM
eMuscle.forceName = "EMuscle"
eMuscle.addAttr("name", "str")
eMuscle.addAttr("minimumMuscle", "int")
eMuscle.addAttr("maximumMuscle", "int")
eMuscle.skipAttr("colour")
addMedianToEnumFromJavaTarget(eMuscle, "minimumMuscle", "maximumMuscle",'medianMuscle')
gmv = eMuscle.addMethod(
    "getMedianValue", [ast.arg(arg=ast.Name("self"), annotation=None)]
)
markMethodAsDeprecated(eMuscle, gmv, "Use median attribute instead")
gmv.returns = ast.Name("int")
gmv.body.append(
    ast.Return(
        value=ast.BinOp(
            left=ast.Attribute(
                value=ast.Name(id="self", ctx=ast.Load()),
                attr="minimumMuscle",
                ctx=ast.Load(),
            ),
            op=ast.Add(),
            right=ast.BinOp(
                left=ast.BinOp(
                    left=ast.Attribute(
                        value=ast.Name(id="self", ctx=ast.Load()),
                        attr="maximumMuscle",
                        ctx=ast.Load(),
                    ),
                    op=ast.Sub(),
                    right=ast.Attribute(
                        value=ast.Name(id="self", ctx=ast.Load()),
                        attr="minimumMuscle",
                        ctx=ast.Load(),
                    ),
                ),
                op=ast.Div(),
                right=ast.Constant(value=2),
            ),
        )
    )
)

eBodySize: EnumFromJavaTarget = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "body" / "enums" / "bodysize.py",
        LT_SRC_PKG / "game" / "character" / "body" / "valueEnums" / "BodySize.java",
    )
)
eBodySize.scanner = EScanner.ENUM
eBodySize.forceName = "EBodySize"
# String name, int minimumBodySize, int maximumBodySize, Colour colour
eBodySize.addAttr("name", "str")
eBodySize.addAttr("minimumBodySize", "int")
eBodySize.addAttr("maximumBodySize", "int")
eBodySize.skipAttr("colour")
addMedianToEnumFromJavaTarget(eBodySize, "minimumBodySize", "maximumBodySize",'medianMuscleSize')
gmv = eBodySize.addMethod(
    "getMedianValue", [ast.arg(arg=ast.Name("self"), annotation=None)]
)
markMethodAsDeprecated(eBodySize, gmv, "Use medianMuscleSize attribute instead")
gmv.returns = ast.Name("int")
gmv.body.append(
    ast.Return(
        value=ast.BinOp(
            left=ast.Attribute(
                value=ast.Name(id="self", ctx=ast.Load()),
                attr="minimumBodySize",
                ctx=ast.Load(),
            ),
            op=ast.Add(),
            right=ast.BinOp(
                left=ast.BinOp(
                    left=ast.Attribute(
                        value=ast.Name(id="self", ctx=ast.Load()),
                        attr="maximumBodySize",
                        ctx=ast.Load(),
                    ),
                    op=ast.Sub(),
                    right=ast.Attribute(
                        value=ast.Name(id="self", ctx=ast.Load()),
                        attr="minimumBodySize",
                        ctx=ast.Load(),
                    ),
                ),
                op=ast.Div(),
                right=ast.Constant(value=2),
            ),
        )
    )
)

eFemininity: EnumFromJavaTarget = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "body" / "enums" / "femininity.py",
        LT_SRC_PKG / "game" / "character" / "body" / "valueEnums" / "Femininity.java",
    )
)
eFemininity.scanner = EScanner.ENUM
eFemininity.forceName = "EFemininity"
# List<String> names, int minimumFemininity, int maximumFemininity, Colour colour, Colour speechColour
# eFemininity.addAttr('names', 'List[str]')
eFemininity.skipAttr("names")
eFemininity.addAttr("minimumFemininity", "int")
eFemininity.addAttr("maximumFemininity", "int")
eFemininity.skipAttr("colour")
eFemininity.skipAttr("speechColour")
addMedianToEnumFromJavaTarget(
    eFemininity, "minimumFemininity", "maximumFemininity", "medianFemininity"
)
gmv = eFemininity.addMethod(
    "getMedianFemininity", [ast.arg(arg=ast.Name("self"), annotation=None)]
)
markMethodAsDeprecated(eFemininity,gmv,'Use medianFemininity attribute instead')
gmv.returns = ast.Name("int")
gmv.body.append(
    ast.Return(
        value=ast.BinOp(
            left=ast.Attribute(
                value=ast.Name(id="self", ctx=ast.Load()),
                attr="minimumFemininity",
                ctx=ast.Load(),
            ),
            op=ast.Add(),
            right=ast.BinOp(
                left=ast.BinOp(
                    left=ast.Attribute(
                        value=ast.Name(id="self", ctx=ast.Load()),
                        attr="maximumFemininity",
                        ctx=ast.Load(),
                    ),
                    op=ast.Sub(),
                    right=ast.Attribute(
                        value=ast.Name(id="self", ctx=ast.Load()),
                        attr="minimumFemininity",
                        ctx=ast.Load(),
                    ),
                ),
                op=ast.Div(),
                right=ast.Constant(value=2),
            ),
        )
    )
)

eCupSize: EnumFromJavaTarget = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "body" / "enums" / "cupsize.py",
        LT_SRC_PKG / "game" / "character" / "body" / "valueEnums" / "CupSize.java",
    )
)
eCupSize.scanner = EScanner.ENUM
eCupSize.forceName = "ECupSize"
# private CupSize(String descriptor, String cupSizeName, int measurement) {
eCupSize.addAttr("descriptor", "str")
eCupSize.addAttr("cupSizeName", "str")
eCupSize.addAttr("measurement", "int")

eHairLength: EnumFromJavaTarget = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "body" / "enums" / "hairlength.py",
        LT_SRC_PKG / "game" / "character" / "body" / "valueEnums" / "HairLength.java",
    )
)
eHairLength.scanner = EScanner.ENUM
eHairLength.forceName = "EHairLength"
# String descriptor, int minimumValue, int maximumValue, Colour colour, boolean suitableForPulling
eHairLength.addAttr("descriptor", "str")
eHairLength.addAttr("minimumValue", "int")
eHairLength.addAttr("maximumValue", "int")
eHairLength.skipAttr("colour")  # , 'int')
eHairLength.addAttr("suitableForPulling", "bool")
addMedianToEnumFromJavaTarget(eHairLength,'minimumValue','maximumValue','medianValue')
gmv = eHairLength.addMethod(
    "getMedianValue", [ast.arg(arg=ast.Name("self"), annotation=None)]
)
markMethodAsDeprecated(eHairLength,gmv,'Use medianValue attribute instead.')
gmv.returns = ast.Name("int")
gmv.body.append(
    ast.Return(
        value=ast.BinOp(
            left=ast.Attribute(
                value=ast.Name(id="self", ctx=ast.Load()),
                attr="minimumValue",
                ctx=ast.Load(),
            ),
            op=ast.Add(),
            right=ast.BinOp(
                left=ast.BinOp(
                    left=ast.Attribute(
                        value=ast.Name(id="self", ctx=ast.Load()),
                        attr="maximumValue",
                        ctx=ast.Load(),
                    ),
                    op=ast.Sub(),
                    right=ast.Attribute(
                        value=ast.Name(id="self", ctx=ast.Load()),
                        attr="minimumValue",
                        ctx=ast.Load(),
                    ),
                ),
                op=ast.Div(),
                right=ast.Constant(value=2),
            ),
        )
    )
)

eLipSize: EnumFromJavaTarget = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "body" / "enums" / "lipsize.py",
        LT_SRC_PKG / "game" / "character" / "body" / "valueEnums" / "LipSize.java",
    )
)
eLipSize.scanner = EScanner.ENUM
eLipSize.forceName = "ELipSize"
# private LipSize(int value, String descriptor, Colour colour, boolean impedesSpeech) {
eLipSize.addAttr("value_", "int")
eLipSize.addAttr("descriptor", "str")
eLipSize.skipAttr("colour")
eLipSize.addAttr("impedesSpeech", "bool")

eLegConfiguration: EnumFromJavaTarget = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "body" / "enums" / "leg_configuration.py",
        LT_SRC_PKG
        / "game"
        / "character"
        / "body"
        / "valueEnums"
        / "LegConfiguration.java",
    )
)
eLegConfiguration.scanner = EScanner.ENUM
eLegConfiguration.forceName = "ELegConfiguration"
# eLegConfiguration.addImportFrom('lilitools.saves.enums.tf_modifier',['ETFModifier'])
# private LegConfiguration(
#     String name,
eLegConfiguration.addAttr("identifier", "str")
#     int landSpeedModifier,
eLegConfiguration.addAttr("landSpeedModifier", "int")
#     int waterSpeedModifier,
eLegConfiguration.addAttr("waterSpeedModifier", "int")
#     boolean bipedalPositionedGenitals,
eLegConfiguration.addAttr("bipedalPositionedGenitals", "bool")
#     boolean bipedalPositionedCrotchBoobs,
eLegConfiguration.addAttr("bipedalPositionedCrotchBoobs", "bool")
#     boolean largeGenitals,
eLegConfiguration.addAttr("largeGenitals", "bool")
#     boolean tall,
eLegConfiguration.addAttr("tall", "bool")
#     WingSize minimumWingSizeForFlight,
eLegConfiguration.skipAttr("minimumWingSizeForFlight")
#     boolean wingsOnLegConfiguration,
eLegConfiguration.addAttr("wingsOnLegConfiguration", "bool")
#     int numberOfLegs,
eLegConfiguration.addAttr("numberOfLegs", "int")
#     String genericDescription,
# eLegConfiguration.addAttr('genericDescription', 'str')
eLegConfiguration.skipAttr("genericDescription")
#     String crotchBoobLocationDescription,
# eLegConfiguration.addAttr('crotchBoobLocationDescription', 'str')
eLegConfiguration.skipAttr("crotchBoobLocationDescription")
#     TFModifier tfModifier,
eLegConfiguration.skipAttr("tfModifier")
#     String subspeciesStatusEffectBackgroundPath) {
eLegConfiguration.skipAttr("subspeciesStatusEffectBackgroundPath")

eAndrogynousIdentification: EnumFromJavaTarget = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "enums" / "androgynous_identification.py",
        # src/com/lilithsthrone/game/character/gender/AndrogynousIdentification.java
        LT_SRC_PKG / "game" / "character" / "gender" / "AndrogynousIdentification.java",
    )
)
eAndrogynousIdentification.scanner = EScanner.ENUM
eAndrogynousIdentification.forceName = "EAndrogynousIdentification"

eDifficultyLevel: EnumFromJavaTarget = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "enums" / "difficulty_level.py",
        # src/com/lilithsthrone/game/settings/DifficultyLevel.java
        LT_SRC_PKG / "game" / "settings" / "DifficultyLevel.java",
    )
)
eDifficultyLevel.scanner = EScanner.ENUM
eDifficultyLevel.forceName = "EDifficultyLevel"
eDifficultyLevel.addAttr("name", "str")
eDifficultyLevel.addAttr("description", "str")
eDifficultyLevel.skipAttr("colour")
eDifficultyLevel.addAttr("isNPCLevelScaling", "bool")
eDifficultyLevel.addAttr("damageModifierNPC", "int")
eDifficultyLevel.addAttr("damageModifierPlayer", "int")

eForcedTFTendency: EnumFromJavaTarget = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "enums" / "forced_tf_tendency.py",
        # src/com/lilithsthrone/game/settings/ForcedTFTendency.java
        LT_SRC_PKG / "game" / "settings" / "ForcedTFTendency.java",
    )
)
eForcedTFTendency.scanner = EScanner.ENUM
eForcedTFTendency.forceName = "EForcedTFTendency"

eForcedFetishTendency: EnumFromJavaTarget = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "enums" / "forced_fetish_tendency.py",
        # src/com/lilithsthrone/game/settings/ForcedFetishTendency.java
        LT_SRC_PKG / "game" / "settings" / "ForcedFetishTendency.java",
    )
)
eForcedFetishTendency.scanner = EScanner.ENUM
eForcedFetishTendency.forceName = "EForcedFetishTendency"

eFurryPreference: EnumFromJavaTarget = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "enums" / "furry_preference.py",
        # src/com/lilithsthrone/game/character/race/FurryPreference.java
        LT_SRC_PKG / "game" / "character" / "race" / "FurryPreference.java",
    )
)
eFurryPreference.scanner = EScanner.ENUM
eFurryPreference.forceName = "EFurryPreference"

eSubspeciesPreference: EnumFromJavaTarget = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "enums" / "subspecies_preference.py",
        # src/com/lilithsthrone/game/character/race/SubspeciesPreference.java
        LT_SRC_PKG / "game" / "character" / "race" / "SubspeciesPreference.java",
    )
)
eSubspeciesPreference.scanner = EScanner.ENUM
eSubspeciesPreference.forceName = "ESubspeciesPreference"

eTesticleSize = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "body" / "enums" / "testicle_size.py",
        LT_SRC_PKG / "game" / "character" / "body" / "valueEnums" / "TesticleSize.java",
    )
)
eTesticleSize.forceName = "ETesticleSize"


eCumProduction: EnumFromJavaTarget = bm.add(
    EnumFromJavaTarget(
        LILIPY_DIR / "saves" / "character" / "cum_production.py",
        LT_SRC_PKG
        / "game"
        / "character"
        / "body"
        / "valueEnums"
        / "CumProduction.java",
    )
)
eCumProduction.forceName = "ECumProduction"
eCumProduction.scanner = EScanner.ENUM
"""private CumProduction(
    String name, 
    String descriptor, 
    int minimumValue, 
    int maximumValue, 
    int arousalNeededToStartPreCumming, 
    Wetness associatedWetness, 
    Colour colour, 
    int additionalSlotsDirtiedUponOrgasm) {"""
eCumProduction.addAttr("name", "str")
eCumProduction.addAttr("descriptor", "str")
eCumProduction.addAttr("min", "int")
eCumProduction.addAttr("max", "int")
eCumProduction.addAttr("arousalNeededToStartPreCumming", "int")
# eCumProduction.addAttr("wetness", "EWetness")
eCumProduction.skipAttr("wetness")
eCumProduction.skipAttr("colour")
# eCumProduction.skipMembers = [
#     "attributeToIdMap",
#     "idToAttributeMap",
#     "allAttributes",
#     "racialAttributes",
# ]
addMedianToEnumFromJavaTarget(eCumProduction, "min", "max",'median')

bm.as_app(argp)

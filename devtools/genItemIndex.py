import ast
import importlib
import importlib.util
import os
from pathlib import Path
from typing import Dict, Optional, cast

from buildtools.maestro import BuildMaestro
from buildtools.maestro.base_target import SingleBuildTarget

from mkast.modulefactory import ModuleFactory
from _codegen import LTMinifiableCodeGenerator

from _codegen import writeGeneratedPythonWarningHeader

class HistoryInfo:
    def __init__(self, hid: str, clsname: str, module: str) -> None:
        self.hist_id: str = hid
        self.class_name: str = clsname
        self.module: str = module


class NPCIndex:
    def __init__(self) -> None:
        self.histories: Dict[str, HistoryInfo] = {}

    def parseFilesIn(self, dir: Path) -> None:
        for path in dir.rglob("*.py"):
            self.parseFile(path)

    def parseFile(self, file: Path) -> None:
        tree = ast.parse(file.read_text(), str(file.absolute()))
        pkg = (
            (file.relative_to(Path.cwd()).parent / file.stem)
            .as_posix()
            .replace("/", ".")
        )
        for el in tree.body:
            if isinstance(el, ast.ClassDef):
                clsname: str = el.name
                hid: Optional[str] = None
                for clsel in el.body:
                    if isinstance(clsel, ast.Assign):
                        varname = cast(ast.Name, clsel.targets[0]).id
                        if varname == "ID":
                            hid = cast(ast.Constant, clsel.value).value
                if hid is not None:
                    self.histories[hid] = HistoryInfo(hid, clsname, pkg)
                    # print(f'Found {clsname} in {file}')

    def generate(self) -> str:
        mb = ModuleFactory()
        # from typing import Dict, Type
        mb.addImportFrom("typing", ["Dict", "Type"])
        # from lilitools.saves.character.npc import NPC
        mb.addImportFrom("lilitools.game.histories.history", ["History"])
        histsByID = ast.Dict(keys=[], values=[])
        for h in sorted(self.histories.values(), key=lambda k: k.class_name):
            mb.addImportFrom(h.module, [h.class_name])
            histsByID.keys.append(ast.Constant(h.hist_id, kind=None))
            histsByID.values.append(ast.Name(id=h.class_name, ctx=ast.Load()))
        mb.addVariableDecl(
            "__all__",
            None,
            ast.List(elts=[ast.Constant("HISTORIES_BY_ID", kind=None)], ctx=ast.Load()),
        )
        ann = ast.Subscript(
            value=ast.Name(id="Dict", ctx=ast.Load()),
            slice=ast.Index(
                value=ast.Tuple(
                    elts=[
                        ast.Name(id="str", ctx=ast.Load()),
                        ast.Subscript(
                            value=ast.Name(id="Type", ctx=ast.Load()),
                            slice=ast.Index(
                                value=ast.Name(id="History", ctx=ast.Load())
                            ),
                            ctx=ast.Load(),
                        ),
                    ],
                    ctx=ast.Load(),
                )
            ),
            ctx=ast.Load(),
        )
        mb.addVariableDecl("HISTORIES_BY_ID", ann, histsByID)
        return LTMinifiableCodeGenerator().generate(mb.generate())

    def writeCodeTo(self, path: Path) -> None:
        tmpout = path.with_suffix(".py.tmp")
        with open(tmpout, "w") as f:
            writeGeneratedPythonWarningHeader(f,'All Items',Path(__file__))
            f.write(self.generate())
        os.replace(tmpout, path)


class GenerateHistoryIndex(SingleBuildTarget):
    BT_LABEL = "GENERATE"

    def __init__(self, indir: Path, outfile: Path) -> None:
        self.indir: Path = indir
        self.outfile: Path = outfile
        files = list({str(x) for x in self.indir.rglob("*.py")} - {str(self.outfile)})
        super().__init__(target=str(outfile), files=files)

    def build(self) -> None:
        idx = NPCIndex()
        HISTORIES_DIR = Path.cwd() / "lilitools" / "game" / "histories"
        idx.parseFilesIn(HISTORIES_DIR)
        idx.writeCodeTo(HISTORIES_DIR / "__init__.py")


if __name__ == "__main__":
    HISTORIES_DIR = Path.cwd() / "lilitools" / "game" / "histories"
    from _utils import file2builddir

    bm = BuildMaestro(file2builddir(__file__))
    bm.add(GenerateHistoryIndex(HISTORIES_DIR, HISTORIES_DIR / "__init__.py"))
    bm.as_app()

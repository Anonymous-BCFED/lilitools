import astor.source_repr
import isort
from lxml import etree
import ast
import json, astor
from pathlib import Path
from typing import Any, Dict, List, Optional, Tuple

import tqdm
from analysis.enum_generator import EnumGenerator

from buildtools.maestro.base_target import SingleBuildTarget
from ruamel.yaml import YAML as Yaml

from editKnownEffect import EAllowedPotency, Effects, EffectInfo as StoredEffectInfo, EEffectFlags

YAML = Yaml(typ="rt")
RENAMES: Dict[Tuple[str, str, str], str] = {}


class EffectValues:
    def __init__(self, data: dict[str, Any]) -> None:
        self.limit: int = int(data["limit"])
        self.appliedStatusEffects: Dict[str, int] = {}
        self.descriptionsForSelf: Optional[List[str]] = data["descriptions"].get("self")
        self.descriptionsForOthers: Optional[List[str]] = data["descriptions"].get(
            "other"
        )
        self.descriptionsForPotions: str = data["descriptions"].get("potion")


class EffectInfo:
    def __init__(self, data: dict) -> None:
        self.type: str = data["type"]
        self.mod1: str = data["mod1"]
        self.mod2: str = data["mod2"]
        self.potency: str = data["pot"]
        self.valuesForClothing = EffectValues(data["clothing"])
        self.valuesForPotions = EffectValues(data["potion"])
        self.flags: EEffectFlags = EEffectFlags.NONE

        self.sort_key = self.type, self.mod1, self.mod2


class GenerateEffectList(SingleBuildTarget):
    BT_LABEL = "GENERATE"

    class Item:
        def __init__(self, text: str, link: str) -> None:
            self.text: str = text
            self.link: str = link

        def serialize(self) -> Dict[str, Any]:
            return {"text": self.text, "link": self.link}

    def __init__(
        self,
        effects_json: Path,
        docdir: Path,
        known_effects_py: Path,
        renames_yml: Path,
        renames_csv: Path,
    ) -> None:
        self.infile: Path = effects_json
        self.renames_yml_file: Path = renames_yml
        self.renames_csv_file: Path = renames_csv
        self.docdir: Path = docdir
        self.outfile: Path = known_effects_py
        self.items: List[self.Item] = []
        super().__init__(target=str(self.outfile), files=[str(self.infile)])

    def build(self) -> None:
        knownEffects = Effects()
        if self.renames_csv_file.is_file():
            knownEffects = Effects.from_csv_file(self.renames_csv_file)
        with self.infile.open("r") as f:
            data = json.load(f)
        assert data["version"] == 2
        hdr = [
            "LiliTools ID",
            "Type",
            "Primary",
            "Secondary",
            "Potency",
            "Limit",
            "Descriptions",
        ]
        newset = set()
        effects: Dict[Tuple[str, str, str, str], EffectInfo] = {}
        fx_tree: Dict[str, Dict[Tuple[str, str], Dict[str, EffectInfo]]] = {}
        sfxi: Dict[Tuple[str, str, str], StoredEffectInfo] = {}
        with tqdm.tqdm(desc="Reading...", total=len(data["effects"])) as prog:
            for i, row in enumerate(data["effects"]):
                fxi = EffectInfo(row)
                modt = (fxi.mod1, fxi.mod2)
                if fxi.type not in fx_tree.keys():
                    fx_tree[fxi.type] = {}
                if modt not in fx_tree[fxi.type].keys():
                    fx_tree[fxi.type][modt] = {}
                effects[fxi.type, fxi.mod1, fxi.mod2, fxi.potency] = fxi
                fx_tree[fxi.type][modt][fxi.potency] = fxi
                if (fxi.type, fxi.mod1, fxi.mod2) not in knownEffects.tf2id.keys():
                    knownEffects.add(fxi.type, fxi.mod1, fxi.mod2, None)
                newset.add((fxi.type, fxi.mod1, fxi.mod2))
                t = (fxi.type, fxi.mod1, fxi.mod2)
                if t not in sfxi.keys():
                    sfxi[t] = StoredEffectInfo()
                sfxi[t].potencies |= EAllowedPotency[fxi.potency]
                prog.update(1)

        oldset = set(knownEffects.tf2id.keys())
        for id2rm in oldset - newset:
            type_, mod1, mod2 = id2rm
            knownEffects.remove(type_, mod1, mod2)
            print(f"- {type_}, {mod1}, {mod2}")

        # print(f'{i:X}')
        for file in self.docdir.glob("*.md"):
            file.unlink()
        with (self.docdir / f"index.md").open("w") as f:
            f.write("# Effects\n\n")
            f.write(
                "This is an automatically-generated list of all known valid ItemEffect type IDs. Click on one to see known valid combinations of ETFModifiers and potencies.\n\n"
            )
            for typeid in sorted(fx_tree.keys()):
                f.write(f" * [{typeid}](./{typeid}.md)\n")

        for typeid, modtree in tqdm.tqdm(
            sorted(fx_tree.items()), desc="Writing markdown..."
        ):
            with (self.docdir / f"{typeid}.md").open("w") as f:
                f.write(f"# {typeid}\n")
                self.items.append(
                    self.Item(typeid, f"/ref/lt/known_effects/{typeid}.md")
                )
                table = etree.Element("table")
                thead = etree.SubElement(table, "thead")
                for h in hdr:
                    etree.SubElement(thead, "th", {}).text = h
                # types = [fxi.type for fxi in effects.values()]
                tbody = etree.SubElement(table, "thead")
                for _tid, mod1, mod2, pot in sorted(effects.keys()):
                    if _tid != typeid:
                        continue
                    fxi = effects[(_tid, mod1, mod2, pot)]
                    effect = knownEffects.getEffectInfoByTuple(typeid, mod1, mod2)
                    context_names = ["Clothing", "Potions"]
                    contexts = [fxi.valuesForClothing, fxi.valuesForPotions]
                    description_types = ["Self", "Others", "Potions"]
                    rowspan_primary = str(len(context_names) * len(description_types))
                    rowspan_secondary = str(len(description_types))
                    for context in range(2):
                        values = contexts[context]
                        available_descs = [
                            values.descriptionsForSelf,
                            values.descriptionsForOthers,
                            values.descriptionsForPotions,
                        ]
                        for desctype in range(3):
                            desclist = available_descs[desctype]
                            tr = etree.SubElement(tbody, "tr")
                            if context == 0 and desctype == 0:
                                etree.SubElement(
                                    tr, "td", {"rowspan": rowspan_primary}
                                ).text = effect.id
                                etree.SubElement(
                                    tr, "td", {"rowspan": rowspan_primary}
                                ).text = typeid
                                etree.SubElement(
                                    tr, "td", {"rowspan": rowspan_primary}
                                ).text = mod1
                                etree.SubElement(
                                    tr, "td", {"rowspan": rowspan_primary}
                                ).text = mod2
                                etree.SubElement(
                                    tr, "td", {"rowspan": rowspan_primary}
                                ).text = pot

                            if desctype == 0:
                                etree.SubElement(
                                    tr, "td", {"rowspan": rowspan_secondary}
                                ).text = context_names[context]
                                etree.SubElement(
                                    tr, "td", {"rowspan": rowspan_secondary}
                                ).text = str(values.limit)
                                appliedSFXTD = etree.SubElement(
                                    tr, "td", {"rowspan": rowspan_secondary}
                                )
                                appliedSFXUL = etree.SubElement(appliedSFXTD, "ul")
                                for sfxid, i in values.appliedStatusEffects.items():
                                    etree.SubElement(appliedSFXUL, "li").text = (
                                        f"{sfxid} = {i}"
                                    )
                            etree.SubElement(tr, "td").text = description_types[
                                desctype
                            ]
                            desctd = etree.SubElement(tr, "td")
                            if isinstance(desclist, str):
                                desctd.text = str(desclist)
                            else:
                                if desclist is None:
                                    etree.SubElement(desctd, "em").text = "NULL"
                                else:
                                    descul = etree.SubElement(desctd, "ul")
                                    for desc in desclist:
                                        etree.SubElement(descul, "li").text = desc
                f.write(
                    etree.tostring(
                        table, encoding="unicode", method="html", pretty_print=False
                    )
                )
        print(f"Writing {self.outfile}...")
        gen = EnumGenerator()
        gen.addImportFrom("enum", ["IntFlag"])

        flags = gen.addEnum("EEffectFlags")
        flags.bases = [ast.Name("IntFlag")]
        EFFECT_FLAGS = {"NONE": 0, "NO_REPEAT": 1}
        for k, v in EFFECT_FLAGS.items():
            flags.addValue(k, args=[ast.Constant(v)])

        def mkFlagAttr(name: str) -> ast.Attribute:
            return ast.Attribute(
                value=ast.Name("EEffectFlags"),
                attr=name,
            )

        def fxflag2ast(fv: int) -> ast.AST:
            o = None
            if fv == 0:
                return mkFlagAttr("NONE")
            for k, v in EFFECT_FLAGS.items():
                if v == fv and v.bit_count() == 1:
                    if o is None:
                        o = mkFlagAttr(k)
                    else:
                        o = ast.BinOp(left=o, op=ast.BitOr(), right=mkFlagAttr(k))
            return o

        # gen._mod.expressions.append(
        #     ast.ClassDef(
        #         name="KnownEffect",
        #         bases=[],
        #         keywords=[],
        #         decorator_list=[],
        #         type_params=[],
        #         body=[
        #             ast.FunctionDef(
        #                 name="__init__",
        #                 args=ast.arguments(
        #                     posonlyargs=[],
        #                     args=[
        #                         ast.arg("self"),
        #                         ast.arg("type", ast.Name("str")),
        #                         ast.arg("mod1", ast.Name("ETFModifier")),
        #                         ast.arg("mod2", ast.Name("ETFModifier")),
        #                         ast.arg("min_limit", ast.Name("int")),
        #                         ast.arg("max_limit", ast.Name("int")),
        #                         ast.arg("flags", ast.Name("EEffectFlags")),
        #                     ],
        #                     vararg=None,
        #                     kwonlyargs=[],
        #                     kw_defaults=[],
        #                     kwarg=None,
        #                     defaults=[],
        #                 ),
        #                 body=[
        #                     ast.AnnAssign(
        #                         target=ast.Attribute(
        #                             value=ast.Name("self"), attr="type"
        #                         ),
        #                         annotation=ast.Name("str"),
        #                         value=ast.Name("type"),
        #                         simple=1,
        #                     ),
        #                     ast.AnnAssign(
        #                         target=ast.Attribute(
        #                             value=ast.Name("self"), attr="mod1"
        #                         ),
        #                         annotation=ast.Name("ETFModifier"),
        #                         value=ast.Name("mod1"),
        #                         simple=1,
        #                     ),
        #                     ast.AnnAssign(
        #                         target=ast.Attribute(
        #                             value=ast.Name("self"), attr="mod2"
        #                         ),
        #                         annotation=ast.Name("ETFModifier"),
        #                         value=ast.Name("mod2"),
        #                         simple=1,
        #                     ),
        #                     ast.AnnAssign(
        #                         target=ast.Attribute(
        #                             value=ast.Name("self"), attr="min_limit"
        #                         ),
        #                         annotation=ast.Name("int"),
        #                         value=ast.Name("min_limit"),
        #                         simple=1,
        #                     ),
        #                     ast.AnnAssign(
        #                         target=ast.Attribute(
        #                             value=ast.Name("self"), attr="max_limit"
        #                         ),
        #                         annotation=ast.Name("int"),
        #                         value=ast.Name("max_limit"),
        #                         simple=1,
        #                     ),
        #                     ast.AnnAssign(
        #                         target=ast.Attribute(
        #                             value=ast.Name("self"), attr="flags"
        #                         ),
        #                         annotation=ast.Name("EEffectFlags"),
        #                         value=ast.Name("flags"),
        #                         simple=1,
        #                     ),
        #                 ],
        #                 decorator_list=[],
        #                 returns=None,
        #                 type_params=[],
        #             )
        #         ],
        #     )
        # )
        enum = gen.addEnum("EKnownEffects")
        # enum.setValueType("KnownEffect", {})
        enum.addAttr("type", "str")
        enum.addAttr("mod1", "ETFModifier")
        enum.addAttr("mod2", "ETFModifier")
        enum.addAttr("minLimit", "int")
        enum.addAttr("maxLimit", "int")
        enum.addAttr("flags", "EEffectFlags")
        # enum.bases=[ast.Name('Enum')]
        for v in tqdm.tqdm(
            sorted(effects.values(), key=lambda x: x.sort_key),
            unit="effect",
            desc="Building enum",
        ):
            vc = v.valuesForClothing
            eid = knownEffects.getEffectID(v.type, v.mod1, v.mod2)
            if eid.startswith('UNKNOWN_'):
                continue
            e = knownEffects.getEffectInfoByID(eid)
            e.potencies = sfxi[v.type, v.mod1, v.mod2].potencies
            postprocessEffect(v, e)
            enum.addValue(
                eid,
                [
                    ast.Constant(v.type),
                    ast.Attribute(value=ast.Name("ETFModifier"), attr=v.mod1),
                    ast.Attribute(value=ast.Name("ETFModifier"), attr=v.mod2),
                    ast.Constant(0),
                    ast.Constant(vc.limit),
                    fxflag2ast(e.flags.value),
                ],
            )
        with self.outfile.open("w") as f:
            f.write("# AUTOGENERATED. DO NOT EDIT.\n")
            f.write("from lilitools.saves.enums.tf_modifier import ETFModifier\n")
            def pretty_source(source):
                return "".join(astor.source_repr.split_lines(source, maxline=65535))
            a:ast.Module = gen.generateAST()
            for i,v in enumerate(a.body):
                print(i,v)
            # a.body[3].value.elts.append(ast.Constant('KnownEffect'))
            # a.body[2], a.body[3] = a.body[3],a.body[2]
            # a.body[4], a.body[3] = a.body[3],a.body[4]
            f.write(isort.code(astor.to_source(a, pretty_source=pretty_source)))
            # f.write(gen.generate())
        knownEffects.to_csv_file(self.renames_csv_file)
        knownEffects.to_yml_file(self.renames_yml_file)

        with open(self.docdir / "index.json", "w") as f:
            json.dump([x.serialize() for x in self.items], f, indent=2)


def pfmt(e: StoredEffectInfo, msg: str) -> None:
    # print(f"{e.type}/{e.mod1}/{e.mod2}: {msg}")
    pass


def postprocessEffect(p: EffectInfo, e: StoredEffectInfo) -> None:
    match (e.type, e.mod1, e.mod2):
        case (_, _, "CLOTHING_ENSLAVEMENT"):
            pfmt(e, "Enslavement: +NO_REPEAT")
            e.flags |= EEffectFlags.NO_REPEAT
        case (_, _, "CLOTHING_ORGASM_PREVENTION"):
            pfmt(e, "Orgasm prevention: +NO_REPEAT")
            e.flags |= EEffectFlags.NO_REPEAT
        case (_, _, "CLOTHING_SERVITUDE"):
            pfmt(e, "Servitude: +NO_REPEAT")
            e.flags |= EEffectFlags.NO_REPEAT
        case (_, _, "CLOTHING_CONDOM"):
            pfmt(e, "Condom: +NO_REPEAT")
            e.flags |= EEffectFlags.NO_REPEAT
        case (_, _, "TF_MOD_INTERNAL"):
            pfmt(e, "Internal: +NO_REPEAT")
            e.flags |= EEffectFlags.NO_REPEAT
        case (
            _,
            _,
            (
                "TF_TYPE_1"
                | "TF_TYPE_2"
                | "TF_TYPE_3"
                | "TF_TYPE_4"
                | "TF_TYPE_5"
                | "TF_TYPE_6"
                | "TF_TYPE_7"
                | "TF_TYPE_8"
                | "TF_TYPE_9"
                | "TF_TYPE_10"
            ),
        ):
            pfmt(e, "Grow organ: +NO_REPEAT")
            e.flags |= EEffectFlags.NO_REPEAT
        case (_, _, "REMOVAL"):
            pfmt(e, "Remove organ: +NO_REPEAT")
            e.flags |= EEffectFlags.NO_REPEAT
        case (_, _, "CLOTHING_CREAMPIE_RETENTION"):
            pfmt(e, "Creampie Retention: +NO_REPEAT")
            e.flags |= EEffectFlags.NO_REPEAT
        case (
            _,
            _,
            (
                "TF_MOD_ORIFICE_PUFFY"
                | "TF_MOD_ORIFICE_RIBBED"
                | "TF_MOD_ORIFICE_TENTACLED"
                | "TF_MOD_ORIFICE_MUSCLED"
                | "TF_MOD_ORIFICE_PUFFY_2"
                | "TF_MOD_ORIFICE_RIBBED_2"
                | "TF_MOD_ORIFICE_TENTACLED_2"
                | "TF_MOD_ORIFICE_MUSCLED_2"
            ),
        ):
            pfmt(e, "Orifice mod: +NO_REPEAT")
            e.flags |= EEffectFlags.NO_REPEAT
        case (
            _,
            _,
            (
                "TF_MOD_EYE_IRIS_CIRCLE"
                | "TF_MOD_EYE_IRIS_HORIZONTAL"
                | "TF_MOD_EYE_IRIS_VERTICAL"
                | "TF_MOD_EYE_IRIS_HEART"
                | "TF_MOD_EYE_IRIS_STAR"
            ),
        ):
            pfmt(e, "Iris mod: +NO_REPEAT")
            e.flags |= EEffectFlags.NO_REPEAT
        case (
            _,
            _,
            (
                "TF_MOD_EYE_PUPIL_CIRCLE"
                | "TF_MOD_EYE_PUPIL_HORIZONTAL"
                | "TF_MOD_EYE_PUPIL_VERTICAL"
                | "TF_MOD_EYE_PUPIL_HEART"
                | "TF_MOD_EYE_PUPIL_STAR"
            ),
        ):
            pfmt(e, "Pupil mod: +NO_REPEAT")
            e.flags |= EEffectFlags.NO_REPEAT
        case (
            _,
            _,
            (
                "ORIENTATION_GYNEPHILIC"
                | "ORIENTATION_AMBIPHILIC"
                | "ORIENTATION_ANDROPHILIC"
            ),
        ):
            pfmt(e, "Orientation: +NO_REPEAT")
            e.flags |= EEffectFlags.NO_REPEAT
        case (
            _,
            _,
            (
                "PERSONALITY_TRAIT_SPEECH_LISP"
                | "PERSONALITY_TRAIT_SPEECH_STUTTER"
                | "PERSONALITY_TRAIT_SPEECH_SLOVENLY"
            ),
        ):
            pfmt(e, "Personality trait: +NO_REPEAT")
            e.flags |= EEffectFlags.NO_REPEAT
        case (
            _,
            _,
            (
                "TF_MOD_FOOT_STRUCTURE_PLANTIGRADE"
                | "TF_MOD_FOOT_STRUCTURE_DIGITIGRADE"
                | "TF_MOD_FOOT_STRUCTURE_UNGULIGRADE"
            ),
        ):
            pfmt(e, "Foot structure: +NO_REPEAT")
            e.flags |= EEffectFlags.NO_REPEAT
        case (
            _,
            _,
            (
                "TF_MOD_LEG_CONFIG_BIPEDAL"
                | "TF_MOD_LEG_CONFIG_TAUR"
                | "TF_MOD_LEG_CONFIG_TAIL_LONG"
                | "TF_MOD_LEG_CONFIG_TAIL"
                | "TF_MOD_LEG_CONFIG_ARACHNID"
                | "TF_MOD_LEG_CONFIG_CEPHALOPOD"
                | "TF_MOD_LEG_CONFIG_AVIAN"
                | "TF_MOD_LEG_CONFIG_WINGED_BIPED"
            ),
        ):
            pfmt(e, "Leg structure: +NO_REPEAT")
            e.flags |= EEffectFlags.NO_REPEAT
        case (
            _,
            _,
            (
                "TF_MOD_LEG_CONFIG_BIPEDAL"
                | "TF_MOD_LEG_CONFIG_TAUR"
                | "TF_MOD_LEG_CONFIG_TAIL_LONG"
                | "TF_MOD_LEG_CONFIG_TAIL"
                | "TF_MOD_LEG_CONFIG_ARACHNID"
                | "TF_MOD_LEG_CONFIG_CEPHALOPOD"
                | "TF_MOD_LEG_CONFIG_AVIAN"
                | "TF_MOD_LEG_CONFIG_WINGED_BIPED"
            ),
        ):
            pfmt(e, "Leg structure: +NO_REPEAT")
            e.flags |= EEffectFlags.NO_REPEAT
        case (
            _,
            _,
            (
                "TF_MOD_BREAST_SHAPE_UDDERS"
                | "TF_MOD_BREAST_SHAPE_ROUND"
                | "TF_MOD_BREAST_SHAPE_NARROW"
                | "TF_MOD_BREAST_SHAPE_WIDE"
                | "TF_MOD_BREAST_SHAPE_POINTY"
                | "TF_MOD_BREAST_SHAPE_PERKY"
                | "TF_MOD_BREAST_SHAPE_SIDESET"
            ),
        ):
            pfmt(e, "Boob shape: +NO_REPEAT")
            e.flags |= EEffectFlags.NO_REPEAT
        case (
            _,
            _,
            (
                "TF_MOD_NIPPLE_NORMAL"
                | "TF_MOD_NIPPLE_INVERTED"
                | "TF_MOD_NIPPLE_VAGINA"
                | "TF_MOD_NIPPLE_LIPS"
            ),
        ):
            pfmt(e, "Nipple shape: +NO_REPEAT")
            e.flags |= EEffectFlags.NO_REPEAT
        case (
            _,
            _,
            ("TF_MOD_AREOLAE_CIRCLE" | "TF_MOD_AREOLAE_HEART" | "TF_MOD_AREOLAE_STAR"),
        ):
            pfmt(e, "Areolae shape: +NO_REPEAT")
            e.flags |= EEffectFlags.NO_REPEAT
        case (
            _,
            _,
            (
                "TF_MOD_TONGUE_RIBBED"
                | "TF_MOD_TONGUE_TENTACLED"
                | "TF_MOD_TONGUE_BIFURCATED"
                | "TF_MOD_TONGUE_WIDE"
                | "TF_MOD_TONGUE_FLAT"
                | "TF_MOD_TONGUE_STRONG"
            ),
        ):
            pfmt(e, "Tongue shape: +NO_REPEAT")
            e.flags |= EEffectFlags.NO_REPEAT
        case (
            _,
            _,
            (
                "TF_MOD_PENIS_SHEATHED"
                | "TF_MOD_PENIS_RIBBED"
                | "TF_MOD_PENIS_TENTACLED"
                | "TF_MOD_PENIS_KNOTTED"
                | "TF_MOD_PENIS_TAPERED"
                | "TF_MOD_PENIS_FLARED"
                | "TF_MOD_PENIS_BLUNT"
                | "TF_MOD_PENIS_BARBED"
                | "TF_MOD_PENIS_VEINY"
                | "TF_MOD_PENIS_PREHENSILE"
                | "TF_MOD_PENIS_OVIPOSITOR"
            ),
        ):
            pfmt(e, "Penis mod: +NO_REPEAT")
            e.flags |= EEffectFlags.NO_REPEAT
        case (
            _,
            _,
            ("TF_MOD_VAGINA_SQUIRTER" | "TF_MOD_VAGINA_EGG_LAYER"),
        ):
            pfmt(e, "Vagina mod: +NO_REPEAT")
            e.flags |= EEffectFlags.NO_REPEAT
        case (
            _,
            _,
            (
                "TF_MOD_FLUID_MUSKY"
                | "TF_MOD_FLUID_VISCOUS"
                | "TF_MOD_FLUID_STICKY"
                | "TF_MOD_FLUID_SLIMY"
                | "TF_MOD_FLUID_BUBBLING"
                | "TF_MOD_FLUID_ALCOHOLIC"
                | "TF_MOD_FLUID_ALCOHOLIC_WEAK"
                | "TF_MOD_FLUID_ADDICTIVE"
                | "TF_MOD_FLUID_HALLUCINOGENIC"
                | "TF_MOD_FLUID_MINERAL_OIL"
            ),
        ):
            pfmt(e, "Fluid mod: +NO_REPEAT")
            e.flags |= EEffectFlags.NO_REPEAT
        case (
            _,
            _,
            (
                "TF_MOD_FLAVOUR_CUM"
                | "TF_MOD_FLAVOUR_MILK"
                | "TF_MOD_FLAVOUR_GIRLCUM"
                | "TF_MOD_FLAVOUR_FLAVOURLESS"
                | "TF_MOD_FLAVOUR_BUBBLEGUM"
                | "TF_MOD_FLAVOUR_BEER"
                | "TF_MOD_FLAVOUR_VANILLA"
                | "TF_MOD_FLAVOUR_STRAWBERRY"
                | "TF_MOD_FLAVOUR_CHOCOLATE"
                | "TF_MOD_FLAVOUR_PINEAPPLE"
                | "TF_MOD_FLAVOUR_HONEY"
                | "TF_MOD_FLAVOUR_MINT"
                | "TF_MOD_FLAVOUR_CHERRY"
                | "TF_MOD_FLAVOUR_COFFEE"
                | "TF_MOD_FLAVOUR_TEA"
                | "TF_MOD_FLAVOUR_MAPLE"
                | "TF_MOD_FLAVOUR_CINNAMON"
                | "TF_MOD_FLAVOUR_LEMON"
                | "TF_MOD_FLAVOUR_ORANGE"
                | "TF_MOD_FLAVOUR_GRAPE"
                | "TF_MOD_FLAVOUR_MELON"
                | "TF_MOD_FLAVOUR_COCONUT"
                | "TF_MOD_FLAVOUR_BLUEBERRY"
                | "TF_MOD_FLAVOUR_BANANA"
            ),
        ):
            pfmt(e, "Fluid flavor: +NO_REPEAT")
            e.flags |= EEffectFlags.NO_REPEAT

    for desc in p.valuesForClothing.descriptionsForSelf or []:
        if (
            desc.startswith("In an hour,")
            or desc.startswith("In a day,")
            or desc.startswith("In a week,")
        ):
            e.flags |= EEffectFlags.NO_REPEAT

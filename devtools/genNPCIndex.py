import ast
import importlib
import importlib.util
import os
from pathlib import Path
from typing import Dict, Optional, cast

from buildtools.maestro import BuildMaestro
from buildtools.maestro.base_target import SingleBuildTarget

from mkast.modulefactory import ModuleFactory
from _codegen import LTMinifiableCodeGenerator

from _codegen import writeGeneratedPythonWarningHeader

class NPCInfo:
    def __init__(
        self,
        clspath: str,
        clsname: str,
        module: str,
        chrid: Optional[str] = None,
        chrattr: Optional[str] = None,
    ) -> None:
        self.class_path: str = clspath
        self.class_name: str = clsname
        self.char_id: Optional[str] = chrid
        self.char_attr: Optional[str] = chrattr
        self.module: str = module


class NPCIndex:
    def __init__(self) -> None:
        self.npcs: Dict[str, NPCInfo] = {}

    def parseFilesIn(self, dir: Path) -> None:
        for path in dir.rglob("*.py"):
            self.parseFile(path)

    def parseFile(self, file: Path) -> None:
        tree = ast.parse(file.read_text(), str(file.absolute()))
        pkg = (
            (file.relative_to(Path.cwd()).parent / file.stem)
            .as_posix()
            .replace("/", ".")
        )
        for el in tree.body:
            if isinstance(el, ast.ClassDef):
                clsname: str = el.name
                clspath: Optional[str] = None
                npcid: Optional[str] = None
                for clsel in el.body:
                    if isinstance(clsel, ast.Assign):
                        varname = cast(ast.Name, clsel.targets[0]).id
                        if varname == "CLASS_PATH":
                            clspath = cast(ast.Constant, clsel.value).value
                        elif varname == "NPCID":
                            npcid = cast(ast.Constant, clsel.value).value
                if clspath is not None:
                    self.npcs[clsname] = NPCInfo(
                        clspath,
                        clsname,
                        pkg,
                        npcid,
                        clsname.lower() if npcid is not None else None,
                    )
                    # print(f'Found {clsname} in {file}')

    def generate(self) -> str:
        mb = ModuleFactory()
        # from typing import Dict, Type
        mb.addImportFrom("typing", ["Dict", "Type"])
        # from lilitools.saves.character.npc import NPC
        mb.addImportFrom("lilitools.saves.character.npc", ["NPC"])
        npcsByPath = ast.Dict(keys=[], values=[])
        for npc in sorted(self.npcs.values(), key=lambda k: k.class_name):
            mb.addImportFrom(npc.module, [npc.class_name])
            npcsByPath.keys.append(ast.Constant(npc.class_path, kind=None))
            npcsByPath.values.append(ast.Name(id=npc.class_name, ctx=ast.Load()))
        mb.addVariableDecl(
            "__all__",
            None,
            ast.List(elts=[ast.Constant("NPCS_BY_PATH", kind=None)], ctx=ast.Load()),
        )
        ann = ast.Subscript(
            value=ast.Name(id="Dict", ctx=ast.Load()),
            slice=ast.Index(
                value=ast.Tuple(
                    elts=[
                        ast.Name(id="str", ctx=ast.Load()),
                        ast.Subscript(
                            value=ast.Name(id="Type", ctx=ast.Load()),
                            slice=ast.Index(value=ast.Name(id="NPC", ctx=ast.Load())),
                            ctx=ast.Load(),
                        ),
                    ],
                    ctx=ast.Load(),
                )
            ),
            ctx=ast.Load(),
        )
        mb.addVariableDecl("NPCS_BY_PATH", ann, npcsByPath)
        return LTMinifiableCodeGenerator().generate(mb.generate())

    def writeCodeTo(self, path: Path) -> None:
        tmpout = path.with_suffix(".py.tmp")
        with open(tmpout, "w") as f:
            writeGeneratedPythonWarningHeader(f,'All known NPCs',Path(__file__))
            f.write(self.generate())
        os.replace(tmpout, path)


class GenerateNPCIndex(SingleBuildTarget):
    BT_LABEL = "GEN INDEX"

    def __init__(self, indir: Path, outfile: Path) -> None:
        self.indir: Path = indir
        self.outfile: Path = outfile
        files = list({str(x) for x in self.indir.rglob("*.py")} - {str(self.outfile)})
        super().__init__(target=str(outfile), files=files)

    def build(self) -> None:
        idx = NPCIndex()
        UNPC_DIR = Path.cwd() / "lilitools" / "game" / "characters"
        idx.parseFilesIn(UNPC_DIR)
        idx.writeCodeTo(UNPC_DIR / "__init__.py")


if __name__ == "__main__":
    UNPC_DIR = Path.cwd() / "lilitools" / "game" / "characters"
    from _utils import file2builddir

    bm = BuildMaestro(file2builddir(__file__))
    bm.add(GenerateNPCIndex(UNPC_DIR, UNPC_DIR / "__init__.py"))
    bm.as_app()

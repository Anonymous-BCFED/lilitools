
import ast
import json
from pathlib import Path
from typing import List

from analysis.enum_generator import EnumGenerator
from buildtools.maestro.base_target import SingleBuildTarget


class GeneratePerkList(SingleBuildTarget):
    BT_LABEL = 'GENERATE'

    def __init__(self, perks_json: Path, perks_py: Path) -> None:
        self.infile: Path = perks_json
        self.outfile: Path = perks_py
        super().__init__(target=str(self.outfile), files=[str(self.infile)])

    def build(self) -> None:
        with self.infile.open('r') as f:
            data = json.load(f)
        eg = EnumGenerator()
        e = eg.addEnum('EKnownPerks')
        e.addAttr('id', 'str')
        e.addAttr('realname', 'str')
        e.addAttr('row', 'int')
        for k,v in data['perks'].items():
            e.addValue(k, [
                ast.Str(s=k),
                ast.Str(s=v['name']['male']),
                ast.Constant(s=v['row']['male']),
            ])
        with self.outfile.open('w') as f:
            f.write('# AUTOGENERATED. DO NOT EDIT.\n')
            f.write(eg.generate())
        
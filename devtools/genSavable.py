from pathlib import Path

from analysis.savable_generator import SavableGenerator


def main():
    import argparse
    argp = argparse.ArgumentParser()

    argp.add_argument('infile', type=Path)
    argp.add_argument('outfile', type=Path)

    args = argp.parse_args()

    sg = SavableGenerator()
    sg.loadFrom(args.infile)
    sg.generate(args.outfile)


if __name__ == '__main__':
    main()

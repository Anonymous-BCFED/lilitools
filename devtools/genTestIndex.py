import ast
import os
from pathlib import Path
from typing import Dict, Set

from _codegen import writeGeneratedPythonWarningHeader
from _codegen import LTExpandedCodeGenerator
from buildtools.maestro import BuildMaestro
from buildtools.maestro.base_target import SingleBuildTarget
from mkast.modulefactory import ModuleFactory


class UnitTestInfo:
    def __init__(self) -> None:
        self.module_name: str = ""
        self.class_name: str = ""


class UnitTestIndex:
    def __init__(self) -> None:
        self.tests: Dict[str, str] = {}

    def parseFilesIn(self, dir: Path) -> None:
        for path in dir.rglob("*.py"):
            # print(path)
            self.parseFile(path)

    def parseFile(self, file: Path) -> None:
        tree = ast.parse(file.read_text(), str(file.absolute()))
        pkg = (
            (file.relative_to(Path.cwd()).parent / file.stem)
            .as_posix()
            .replace("/", ".")
        )
        for el in tree.body:
            if isinstance(el, ast.ClassDef):
                # print('  ', pkg, el.name)
                self.tests[pkg] = el.name

    def generate(self) -> str:
        mb = ModuleFactory()
        mb.addImport("unittest")
        valid_tests: Set[str] = set()
        for module, clsname in self.tests.items():
            if clsname in ("BaseSavableTest",):
                continue
            valid_tests.add(clsname)
            mb.addImportFrom(module, [clsname])
        mb.addVariableDecl(
            "__all__",
            None,
            ast.List(
                elts=[ast.Constant(x, kind=None) for x in sorted(valid_tests)],
                ctx=ast.Load(),
            ),
        )
        mb.expressions.append(
            ast.If(
                test=ast.Compare(
                    left=ast.Name("__name__"),
                    ops=[ast.Eq()],
                    comparators=[
                        ast.Constant("__main__"),
                    ],
                ),
                body=[
                    ast.Expr(
                        ast.Call(
                            func=ast.Attribute(value=ast.Name("unittest"), attr="main"),
                            args=[],
                            keywords=[],
                        )
                    )
                ],
                orelse=[],
            )
        )
        return LTExpandedCodeGenerator().generate(mb.generate())

    def writeCodeTo(self, path: Path) -> None:
        tmpout = path.with_suffix(".py.tmp")
        with open(tmpout, "w") as f:
            writeGeneratedPythonWarningHeader(f,'All tests',Path(__file__))
            f.write(self.generate())
        os.replace(tmpout, path)


class GenerateTestIndex(SingleBuildTarget):
    BT_LABEL = "GEN INDEX"

    def __init__(self, indir: Path, outfile: Path) -> None:
        self.indir: Path = indir
        self.outfile: Path = outfile
        files = list({str(x) for x in self.indir.rglob("*.py")} - {str(self.outfile)})
        super().__init__(target=str(outfile), files=files)

    def build(self) -> None:
        idx = UnitTestIndex()
        TESTS_DIR = Path.cwd() / "tests"
        idx.parseFilesIn(TESTS_DIR)
        # print(repr(idx.tests))
        idx.writeCodeTo(TESTS_DIR / "__init__.py")


if __name__ == "__main__":
    TESTS_DIR = Path.cwd() / "tests"
    from _utils import file2builddir

    bm = BuildMaestro(file2builddir(__file__))
    bm.add(GenerateTestIndex(TESTS_DIR, TESTS_DIR / "__init__.py"))
    bm.as_app()

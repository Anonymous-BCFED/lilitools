import os
from pathlib import Path
from _dev_regex import REG_UTIL_GETEXTERNALMODFILESBYID
from rich.console import Console
o=[]
c=Console()
for root,_,filenames in os.walk('lib/liliths-throne-public/src'):
    for filename in filenames:
        p=Path(root, filename)
        for encoding in [None,'utf-8-sig']:
            try:
                with p.open('r', encoding=encoding) as f:
                    for i,line in enumerate(f):
                        if (m:=REG_UTIL_GETEXTERNALMODFILESBYID.search(line)) is not None:
                            c.print(str(p),i,line.rstrip())
                            o.append(tuple(filter(None,m[1].split('/'))))
                break
            except UnicodeDecodeError as ude:
                continue
o=set(sorted(o))
c.print(o)

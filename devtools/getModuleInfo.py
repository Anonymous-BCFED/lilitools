from __future__ import annotations
import argparse
import importlib
import importlib.util
import json
import os
from pathlib import Path
from types import ModuleType
from typing import List, Optional


def modname_from_path(path: Path) -> str:
    o: List[str] = []
    for p in str(path).split(os.sep):
        if len(p.strip()) == 0:
            continue
        if ":" in p:
            continue
        if "." in p:
            p = p.split(".")[0]
        o.append(p)
    return ".".join(o)


class ModuleInfo:
    def __init__(self) -> None:
        self.id: str = ""
        self.path: Path = Path.cwd()
        self.module: ModuleType = None

    @classmethod
    def FromPath(cls, p: Path, relative_to: Optional[Path] = None) -> ModuleInfo:
        op = p
        if relative_to is not None:
            p = p.relative_to(relative_to)
        mi = cls()
        mi.path = op.absolute()
        mi.id = modname_from_path(p)
        spec = importlib.util.spec_from_file_location(mi.id, p)
        mi.module = importlib.util.module_from_spec(spec)
        return mi

    def toDict(self) -> dict:
        return {
            "id": self.id,
            "path": str(self.path),
        }


def main() -> None:
    argp = argparse.ArgumentParser()
    argp.add_argument("modfiles", type=Path, nargs="+")
    args = argp.parse_args()
    o = {}
    p: Path
    for p in args.modfiles:
        assert p.is_file()
        o[str(p)] = ModuleInfo.FromPath(p).toDict()
    print(json.dumps(o))


if __name__ == "__main__":
    main()

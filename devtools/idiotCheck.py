import sys
from pathlib import Path
from typing import Set
from lxml import etree
from rich import get_console
from rich.console import Console
from rich.table import Table
from rich.progress import Progress


def getSaveAsTree(path: Path) -> etree._ElementTree:
    return etree.parse(str(path))


def getModsInSave(tree: etree._ElementTree) -> Set[str]:
    mods: Set[str] = set()
    if saveContainsHelpfulInfoMod(tree):
        mods.add('helpfulinfo')
    return mods


def saveContainsMods(tree: etree._ElementTree) -> bool:
    return len(getModsInSave(tree)) > 0


def saveContainsHelpfulInfoMod(tree: etree._ElementTree) -> bool:
    return len(tree.xpath('//helpfulInfo')) > 0


def checkSave(t: etree._ElementTree) -> bool:
    if saveContainsMods(t):
        return False
    return True


def main() -> None:
    c = Console()
    c.rule('Idiot Check')
    with Progress(console=c) as prog:
        for masc in sorted(['male','female']):
            p = Path("data") / "samples" / "saves" / f"{masc}.xml"
            t = prog.add_task('checking', filename=str(p))
            with prog.open(p, mode='rb', description='checking', task_id=t) as f:
                ok=checkSave(etree.parse(f))
            prog.remove_task(t)
            if ok:
                c.print(f"checking {p} [green]OK[/]")
            else:
                c.print(f"checking {p} [green]FAIL[/]")
                c.print(f"[red][!] You have mods installed. Disable mods and try again.[/]")
                sys.exit(1)
    sys.exit(0)


if __name__ == "__main__":
    main()

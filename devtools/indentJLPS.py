# Indents Javalang psuedocode

from pathlib import Path


def indent(inp: str) -> str:
    o = ''
    i = 0
    ic = '  '
    #lc = ''
    def newline() -> None:
        nonlocal o, ic, i
        o += '\n'+(ic*i)
    skipnext=False
    skipwhite=False
    for x, c in enumerate(inp):
        if c == ' ':
            if skipwhite:
                continue
        else:
            skipwhite=False
        if skipnext:
            skipnext=False
        elif c == '[' and inp[x+1] == ']':
            o += '[]'
            skipnext=True
        elif c == '(' and inp[x+1] == ')':
            o += '()'
            skipnext=True
        elif c in '([':
            i += 1
            o += c
            newline()
        elif c in ')]':
            i -= 1
            newline()
            o += c
        elif c == ',':
            o += c
            newline()
            skipwhite=True
        else:
            o += c
        #lc=c
    return o

def existingPath(inp: str) -> Path:
    p = Path(inp)
    assert p.is_file(), f'{inp} does not exist on disk'
    return p

def main():
    import argparse
    
    argp = argparse.ArgumentParser()
    
    argp.add_argument('input', type=existingPath)
    argp.add_argument('output', type=Path, default=None, nargs='?')

    args = argp.parse_args()

    o = indent(args.input.read_text())
    if args.output is None:
        print(o)
    else:
        args.output.write_text(o)
        print('Done!')

if __name__ == '__main__':
    main()

package charinfo;

import org.json.JSONArray;

public class Range {
    public int low;
    public int high;
    public Range(int low, int high) {
        this.low=low;
        this.high=high;
    }
    public JSONArray toJSONArray() {
        JSONArray a = new JSONArray();
        a.put(this.low);
        a.put(this.high);
        return a;
    }
}


from __future__ import annotations
from functools import lru_cache
import os
from pathlib import Path
from typing import Any, Dict, List

import toml


class Configuration:
    def __init__(self) -> None:
        self.cwd: Path = Path.cwd() / 'dist'
        self.lilirun: Path = self.cwd / 'bin'/'lilirun'
        self.xdotool: Path = Path('/usr/bin/xdotool')
        self.java: Path = Path('/usr/bin/java')
        self.jar: Path = None
        self.lt_version: List[int] = [0,0,0,0]
        self.version_suffix: str = 'Alpha'

    def loadFromFile(self, path: Path) -> None:
        with path.open('r') as f:
            data = toml.load(path)
        self.cwd = Path(data['paths']['work-dir']).absolute()
        self.lilirun = Path(data['paths']['lilirun']).absolute()
        self.xdotool = Path(data['paths']['xdotool']).absolute()
        self.java = Path(data['paths']['java']).absolute()
        self.jar = Path(data['paths']['jar']).absolute()
        self.lt_version = list(map(int, data.get('game', {}).get('version',{}).get('number', '0.0.0').split('.')))
        self.version_suffix = data.get('game', {}).get('version',{}).get('suffix', '')

    @classmethod
    def FromFile(cls, path: Path) -> Configuration:
        cfg = cls()
        if path.is_file():
            cfg.loadFromFile(path)
        return cfg

    def saveToFile(self, path: Path) -> None:
        data: Dict[str, Any] = {
            'version': 1,
            'paths': {
                'work-dir': str(self.cwd.absolute()),
                'lilirun': str(self.lilirun.absolute()),
                'xdotool': str(self.xdotool.absolute()),
                'java': str(self.java.absolute()),
                'jar': str(self.jar.absolute()),
            },
            'game': {
                'version': {
                    'number': '.'.join(map(str, self.lt_version)),
                    'suffix': self.version_suffix,
                }
            }
        }
        with path.open('w') as f:
            toml.dump(data, f)

    @lru_cache
    def game_version(self) -> str:
        return '.'.join(map(str, self.lt_version))

    @lru_cache
    def title_bar(self) -> str:
        vs: str = ''
        if self.version_suffix is not None:
            vs = f' {self.version_suffix}'
        return f"Lilith's Throne {self.game_version()}{vs}"

from typing import Optional

import pyautogui
from buildtools import log

from ..point import Point
from ..ltautogui import LTAutoGUI


class BaseControl:
    def __init__(self, idx: Optional[int] = None, 
                       name: Optional[str] = None, 
                       pos: Optional[Point] = None, 
                       size: Optional[Point] = None) -> None:
        self.idx: int = idx or -1
        self.name: str = name or ''
        self.pos: Point = pos or Point(0, 0)
        self.size: Point = size or Point(0, 0)

    def click(self, name: str) -> None:
        center: Point = self.center
        log.info(f'Clicking {name} @ {center}')
        pyautogui.click(center.x, center.y)  # Click center of button
        if LTAutoGUI.INSTANCE:
            LTAutoGUI.INSTANCE._stepCheck()

    @property
    def center(self) -> Point:
        return self.pos + (self.size / 2)

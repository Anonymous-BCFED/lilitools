from typing import Optional

from .basecontrol import BaseControl
from ..point import Point


class Button(BaseControl):
    def __init__(self, idx: Optional[int] = None, 
                       name: Optional[str] = None, 
                       pos: Optional[Point] = None, 
                       size: Optional[Point] = None, 
                       hotkey: Optional[str] = None) -> None:
        super().__init__(idx, name, pos, size)
        self.hotkey: Optional[str] = hotkey

    def click(self, name: Optional[str] = None) -> None:
        if name is None:
            name = self.name
        super().click(name=f'[{name}]')

    def __str__(self) -> str:
        return f'[{self.idx}] {self.name} @ {self.pos}'

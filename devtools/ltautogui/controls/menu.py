from typing import Dict, List, Optional, Union

from .basecontrol import BaseControl
from .button import Button
from ..point import Point


class BaseMenu(BaseControl):
    def __init__(self, idx: int = -1, 
                       name: str = '', 
                       pos: Optional[Point] = None, 
                       size: Optional[Point] = None) -> None:
        super().__init__(idx, name, pos, size)
        self.buttons: List[Button] = []
        self.buttonsByName: Dict[str, Button] = {}

    def add(self, label: str) -> Button:
        b = Button()
        b.name = label
        return self.addButton(b)

    def addButton(self, button: Button, quiet: bool = False) -> Button:
        button.idx = len(self.buttons)
        self.setupElement(button)
        self.buttons.append(button)
        self.buttonsByName[button.name] = button
        return button

    def setupElement(self, b: Button) -> None:
        pass

    def get(self, name: Union[int, str]) -> Button:
        if isinstance(name, str):
            return self.buttonsByName[name]
        elif isinstance(name, int):
            return self.buttons[name]

    def dump(self):
        for button in self.buttons:
            print(str(button))

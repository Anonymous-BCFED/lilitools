from __future__ import annotations

import math
import os
import subprocess
import time
from pathlib import Path
from typing import Any, List, Optional, Tuple

import pyautogui
import tqdm
from buildtools import log, os_utils
from ltautogui.config import Configuration
from PIL import ImageChops
from PIL.Image import Image

JAVAFX_VERSION: str = '17.0.2'


class LTAutoGUI:
    INSTANCE: LTAutoGUI = None

    def __init__(self, workdir: Path,
                 java: Path,
                 xdotool: Path,
                 lt_jarfile: Path,
                 title_bar: str,
                 stepfile_dir: Optional[Path] = None) -> None:
        self.work_dir: Path = workdir
        self.java: Path = java
        self.xdotool: Path = xdotool

        self.lt_jarfile: Path = lt_jarfile
        self.title_bar: str = title_bar

        self.window_id: int = -1
        self.window_size: Tuple[int, int] = (0, 0)
        self.process: subprocess.Popen = None

        self.stepCheckEnabled: bool = True
        self.step: int = 0
        self.lastCapture: Optional[Image] = None

        self.INSTANCE = self

        self.stepfile_dir: Path = stepfile_dir or (Path.cwd() / 'tmp')
        self.stepfile: Path = self.stepfile_dir / 'ltautogui_step.txt'

        if not self.stepfile_dir.is_dir():
            self.stepfile_dir.mkdir(parents=True)
        # Nuke old stepdata
        if self.stepfile.is_file():
            for step in range(int(self.stepfile.read_text())):
                step += 1
                capfile = self.stepfile_dir / f'ltautogui_step_{step}.png'
                if capfile.is_file():
                    capfile.unlink()
            self.stepfile.unlink()

    @classmethod
    def FromConfiguration(self, cfg: Configuration) -> LTAutoGUI:
        return LTAutoGUI(workdir=cfg.cwd,
                         java=cfg.java,
                         xdotool=cfg.xdotool,
                         lt_jarfile=cfg.jar,
                         title_bar=cfg.title_bar())

    def startLT(self, windowsize: Tuple[int, int], startupWait: int = 5) -> 'LTAutoGUI':
        self.window_size = windowsize
        print('work_dir', self.work_dir)
        print('java', self.java)
        print('xdotool', self.xdotool)
        print('lt_jarfile', self.lt_jarfile)
        # /usr/bin/java --module-path=javafx-sdk-17.0.2/lib --add-modules=ALL-MODULE-PATH -jar LilithsThrone_0_4_7.jar
        cmd: List[str] = [str(self.java),
                          f'--module-path=javafx-sdk-{JAVAFX_VERSION}/lib',
                          '--add-modules=ALL-MODULE-PATH',
                          '-jar', str(self.lt_jarfile.absolute())]
        log.info('$ ' + os_utils._args2str(cmd))
        self.process = subprocess.Popen(cmd)
        # Give the window a chance to render...
        self.sleep(startupWait)
        print(f'Looking for window with name {self.title_bar!r}...')
        self.window_id = self.getWindowID(self.title_bar)
        print('Window ID: ', self.window_id)
        return self

    def __enter__(self) -> Any:
        assert self.window_id is not None
        log.info('Window ID: %d', self.window_id)
        log.info('Process ID: %d', self.process.pid)
        self.focus()
        dx: int
        dy: int
        dw: int
        dh: int
        d=False
        with log.info('Dumping window geometry...'):
            o: str = os_utils.cmd_out([self.xdotool, 'getwindowgeometry', str(self.window_id)], echo=True, critical=True)
            '''
Window 119537667
  Position: 319,101 (screen: 0)
  Geometry: 1280x800
            '''
            for l in o.splitlines():
                if ':' in l:
                    k,v = l.split(':',1)
                    k=k.strip()
                    v=v.strip()
                    match k:
                        case 'Position':
                            dx,dy = map(int,v.split('0', 1)[0].split(','))
                        case 'Geometry':
                            dw,dh = map(int,v.split('x'))
        pyautogui.moveTo(dx+(dw/2), dy+(dh/2), .5)
        with log.info('Setting window size...'):
            w, h = self.window_size
            os_utils.cmd([self.xdotool, 'windowsize', '--sync', str(self.window_id), str(w), str(h)], echo=True, critical=True)
        
        self.moveWindowTo(0, 0)
        self.sleep(5)
        self.step += 1
        capfile = self.stepfile_dir / f'ltautogui_step_{self.step}.png'
        if capfile.is_file():
            capfile.unlink()
        self.lastCapture = pyautogui.screenshot(capfile, region=(0, 0, w, h))
        with self.stepfile.open('w') as f:
            f.write(str(self.step))
        return self

    #  object.__exit__(self, exc_type, exc_value, traceback)
    def __exit__(self, exc_type, exc_value, traceback) -> Any:
        if exc_value is not None:
            log.exception(exc_value)
            log.warning('Terminating process #%d...', self.process.pid)
            self.process.terminate()
        try:
            log.info('Waiting for process #%d to exit...', self.process.pid)
            self.process.wait()
        except:
            pass
        return self

    def scroll(self, y: int, mx: int, my: int) -> None:
        if os.name != 'nt':
            y = 0 - y
        pyautogui.scroll(y, mx, my)

    def clickButtonAt(self, name: str, x: int, y: int) -> None:
        log.info(f'Clicking [{name}] @ <{x}, {y}>')
        pyautogui.click(x, y)  # Click center of button
        self._stepCheck()

    def focus(self) -> None:
        with log.info('Focusing window...'):
            os_utils.cmd([self.xdotool, 'windowfocus', '--sync', str(self.window_id)], echo=True, critical=True)

    def setWindowSize(self, w: int, h: int) -> None:
        self.window_size = (w, h)
        with log.info('Setting window size...'):
            os_utils.cmd([self.xdotool, 'windowsize', '--sync', str(self.window_id), str(w), str(h)], echo=True, critical=True)

    def moveWindowTo(self, x: int, y: int) -> None:
        with log.info('Moving window to <%d, %d>...', x, y):
            os_utils.cmd([self.xdotool, 'windowmove', '--sync', str(self.window_id), str(x), str(y)], echo=True, critical=True)

    def getWindowID(self, name: str) -> int:
        window_id: int = 0
        o = os_utils.cmd_out([self.xdotool, 'search', '--name', name])
        if o.strip() == '':
            return None
        for line in o.splitlines():
            sline = line.strip()
            if sline == '':
                continue
            try:
                return int(sline)
            except ValueError:
                continue
        return None

    def getWindowPID(self, wid: int) -> int:
        window_id: int = 0
        o = os_utils.cmd_out([self.xdotool, 'getwindowpid', str(wid)])
        if o.strip() == '':
            return None
        return int(o.strip())

    def sleep(self, duration):
        with log.info('Sleeping %f seconds...', duration):
            seconds = math.floor(duration)
            leftover = duration - seconds
            for _ in tqdm.tqdm(range(seconds)):
                time.sleep(1)
            if leftover > 0.0:
                time.sleep(leftover)

    def useTextField(self, pos: Tuple[int, int], text: str, text_field: str = 'text field') -> None:
        x, y = pos
        with log.info('Entering %r into %s at <%d, %d>', text, text_field, x, y):
            log.info('Clicking @ <%d, %d>', x, y)
            pyautogui.click(x, y)
            log.info('Selecting existing text')
            pyautogui.hotkey('ctrl', 'a')
            log.info('Typing %r', list(text))
            pyautogui.typewrite(text)
            log.info('Typing \'tab\' to escape the field')
            pyautogui.typewrite(['tab'])
        self._stepCheck()

    def pressKey(self, *keys: str) -> None:
        with log.info('Pressing key(s) %r...', keys):
            pyautogui.hotkey(*keys)
        self._stepCheck()

    def useButtonByHotkey(self, name: str, *hotkeys: str) -> None:
        with log.info('Selecting button [%s] by sending keypress %r', name, hotkeys):
            pyautogui.hotkey(*hotkeys)
        self._stepCheck()

    def done(self) -> None:
        log.info('Done! Terminating process #%d...', self.process.pid)
        self.process.terminate()

    def _stepCheck(self) -> None:
        if not self.stepCheckEnabled:
            return
        w, h = self.window_size
        self.step += 1
        capfile = self.stepfile_dir / f'ltautogui_step_{self.step}.png'
        if capfile.is_file():
            capfile.unlink()
        newestCapture: Image = pyautogui.screenshot(capfile, region=(0, 0, w, h))
        with self.stepfile.open('w') as f:
            f.write(str(self.step))
        if self.lastCapture is not None:
            diff = ImageChops.difference(self.lastCapture, newestCapture)
            if not diff.getbbox():
                raise Exception(f'step check failed, no change between steps {self.step-1} and {self.step}')
        self.lastCapture = newestCapture


import click
import pyautogui
from buildtools import os_utils

from .config import Configuration
from .ltautogui import LTAutoGUI


def createMaleSave(cfg: Configuration) -> None:
    click.secho(f'!!! WARNING !!!', fg='white', bg='red')
    click.secho(f'DO NOT TOUCH YOUR KEYBOARD OR MOUSE UNTIL THIS FINISHES OR CRASHES!', fg='white', bg='red')
    g = LTAutoGUI.FromConfiguration(cfg)
    g.sleep(5.)
    MALE_XML = cfg.cwd / 'data' / 'saves' / 'Male.xml'
    if MALE_XML.is_file():
        MALE_XML.unlink()
    with os_utils.Chdir(str(cfg.cwd)):
        with g.startLT((1280, 840), 30):
            pyautogui.PAUSE = 1.
            pyautogui.moveTo(1280 / 2, 840 / 2, 1.)
            g.focus()
            g.stepCheckEnabled = False
            g.clickButtonAt('New Game', 352, 774)
            g.sleep(30.0)  # Lag
            g.stepCheckEnabled = True
            g.clickButtonAt('Agree', 352, 774)
            g.clickButtonAt('Start', 352, 774)
            # pyautogui.PAUSE=0.5
            g.clickButtonAt('October', 699, 514)
            g.clickButtonAt('Male', 400, 666)
            g.clickButtonAt('Masculine', 820, 667)
            g.pressKey('end')
            g.clickButtonAt('Ambiphilic', 637, 491)
            g.clickButtonAt('Confident', 369, 638)
            g.clickButtonAt('Kind', 585, 636)
            g.clickButtonAt('Naive', 801, 638)
            g.clickButtonAt('Brave', 370, 672)
            g.clickButtonAt('Innocent', 693, 673)
            # pyautogui.PAUSE = 1.
            g.clickButtonAt('Continue', 352, 774)

            # Name
            g.useTextField((491, 487), 'Male', 'Masculine First Name')
            g.useTextField((692, 487), 'Male', 'Androgynous First Name')
            g.useTextField((882, 487), 'Male', 'Feminine First Name')
            g.useTextField((690, 520), 'Sample', 'Surname')

            g.clickButtonAt('Continue', 352, 774)  # Confirm name

            g.clickButtonAt('Continue', 352, 774)  # Confirm body

            # Wardrobe oh fuck oh god
            g.clickButtonAt('To the stage', 352, 774)

            g.clickButtonAt('Select Job', 352, 774)

            g.clickButtonAt('Continue', 352, 774)  # We are happy with unemployed
            g.clickButtonAt('Continue', 352, 774)  # No fetishes/experience
            g.stepCheckEnabled = False
            g.clickButtonAt('Skip Prologue', 495, 782)
            g.sleep(10.0)  # Lag

            g.pressKey('esc')
            g.clickButtonAt('Save/Load', 492, 779)
            g.useTextField((565, 342), 'Male', 'New save')
            g.clickButtonAt('Save', 857, 349)

            g.clickButtonAt('Back', 926, 848)
            g.clickButtonAt('Resume', 926, 848)
            g.pressKey('f5')
            g.sleep(5.0)
            g.stepCheckEnabled = True

            g.done()

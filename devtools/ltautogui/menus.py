import json
import math
from pathlib import Path
from typing import Any, Dict, Optional
from .point import Point
from .controls.menu import BaseMenu
from .controls.button import Button


class JSONMenu(BaseMenu):
    def __init__(self, jsonFile: Path, subkey: str) -> None:
        super().__init__(-1, '', None, None)
        self.load(jsonFile, subkey)

    def load(self, path: Path, subkey: str) -> None:
        e: Dict[str, float]
        with path.open('r') as f:
            data: Dict[str, Any] = json.load(f)[subkey]
            viewport: Dict[str, float] = data['viewport'] 
            self.pos = Point(round(viewport['x']), round(viewport['y']))
            self.size = Point(round(viewport['w']), round(viewport['h']))
            for i, e in enumerate(data['items']):
                btn = Button(i, f'Response {i}', Point(round(e['x']),round(e['y'])), Point(round(e['x']), round(e['y'])))
                self.addButton(btn)
                
class ResponseMenu(JSONMenu):
    def __init__(self) -> None:
        super().__init__(Path.cwd().parent / 'data' / 'dumps' / 'menus.json', 'responses')
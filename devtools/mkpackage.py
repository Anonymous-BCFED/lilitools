import os
from pathlib import Path

def makePackage(package: str, touch_main: bool=False) -> None:
    path = Path.cwd()
    cwd = Path.cwd().resolve()
    for component in package.split('.'):
        path /= component
    for parent in list(reversed(list(path.parents)))+[path]:
        #print('\t', parent, parent.is_dir())
        if not parent.is_dir():
            print(f'Creating {parent}...')
            parent.mkdir()
        if parent == cwd:
            continue
        parent_init = parent / '__init__.py'
        if cwd in parent_init.resolve().parents and not parent_init.is_file():
            print(f'Creating {parent_init}...')
            parent_init.write_text('')
    INIT_PY = path / '__init__.py'
    MAIN_PY = path / '__main__.py'
    if not INIT_PY.is_file():
        print(f'Creating {INIT_PY}...')
        INIT_PY.write_text('')
    if touch_main and not MAIN_PY.is_file():
        print(f'Creating {MAIN_PY}...')
        MAIN_PY.write_text('')

def main():
    import argparse

    argp = argparse.ArgumentParser()

    argp.add_argument('packages', type=str, nargs='+')
    argp.add_argument('--main', action='store_true', default=False)

    args = argp.parse_args()

    for package in args.packages:
        print(f'Creating {package}...')
        makePackage(package, args.main)

if __name__=='__main__':
    main()
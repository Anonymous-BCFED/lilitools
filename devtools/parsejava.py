import sys
import javalang
import javalang.ast

cu: javalang.ast.Node
with open(sys.argv[1], 'r') as f:
    cu = javalang.parse.parse(f.read())

with open(sys.argv[2], 'w') as w:
    w.write(str(cu))
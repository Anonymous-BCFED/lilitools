import re
from typing import Callable
import html
from lxml import etree
import lilitools.regex_consts as regexps


def escape_regexp_for_md(input: str) -> str:
    return input.replace("|", "\\|")


CHARSYMBOLS = re.escape("_*[]()~`>#+-=|{}.!")
ESCAPE_SYMBOLS_MARKDOWN = re.compile(f"([{CHARSYMBOLS}])")

CHARSYMBOLS = re.escape(r"|")
ESCAPE_SYMBOLS_MDTABLE = re.compile(f"([{CHARSYMBOLS}])")


def escape_for_markdown(input: str) -> str:
    return ESCAPE_SYMBOLS_MARKDOWN.sub(r"\\\1", input)


def escape_for_markdown_table(input: str) -> str:
    return ESCAPE_SYMBOLS_MDTABLE.sub(r"\\\1", input)


def encapsulate_and_escape_code(input: str) -> str:
    # return ESCAPE_SYMBOLS_MDTABLE.sub(r"\\\1", input)
    code = etree.Element("code")
    code.text = input
    return etree.tostring(escape_for_markdown_table(code))


def encapsulate_and_escape_code(input: str) -> str:
    return f"`{escape_for_markdown_table(input)}`"


def escape_for_html(input: str) -> str:
    return html.escape(input)


import tabulate


def dump_all_regex(
    tablefmt: str = "pipe",
    escape_method: Callable[[str], str] = escape_for_markdown,
    format_code_method: Callable[[str], str] = encapsulate_and_escape_code,
) -> str:
    header = ["name", "value"]
    rows = []
    for attr in dir(regexps):
        if attr.startswith("REG_"):
            rows.append(
                [attr, encapsulate_and_escape_code(getattr(regexps, attr).pattern)]
            )
    return tabulate.tabulate(
        rows, tablefmt=tablefmt, colalign=["right", "left"], headers=header
    )


def table_main_room_regex(
    tablefmt: str = "pipe",
    escape_method: Callable[[str], str] = escape_for_markdown,
    format_code_method: Callable[[str], str] = encapsulate_and_escape_code,
) -> str:
    header = ["Type", "Name Pattern", "Purpose", "Setup"]
    colalign = ["right", "left", "left", "left"]
    rows = [
        [
            escape_method("Recovery Room"),
            format_code_method(regexps.REG_RECOVERY_ROOM.pattern),
            escape_method("Affection Training"),
            escape_method("Room Service, Upgraded Bed"),
        ],
        [
            escape_method("Training Room"),
            format_code_method(regexps.REG_TRAINING_ROOM.pattern),
            escape_method("Obedience Training"),
            escape_method("Dog Bowls, Downgraded Bed, Obedience Trainer, Chains"),
        ],
    ]
    return tabulate.tabulate(rows, tablefmt=tablefmt, colalign=colalign, headers=header)


def table_misc_room_regex(
    tablefmt: str = "pipe",
    escape_method: Callable[[str], str] = escape_for_markdown,
    format_code_method: Callable[[str], str] = encapsulate_and_escape_code,
) -> str:
    header = ["Type", "Name Pattern", "Purpose", "Setup"]
    colalign = ["right", "left", "left", "left"]
    rows = [
        [
            escape_method("Cow Stall*"),
            format_code_method(regexps.REG_COW_STALL.pattern),
            escape_method("Milking Lodging"),
            escape_method("4 Beds, Room Service, Upgraded Bed"),
        ],
    ]
    return tabulate.tabulate(rows, tablefmt=tablefmt, colalign=colalign, headers=header)


def main() -> None:
    dump_all_regex()
    print("docs-src/docs/commands/slavesheet/serve.md:")
    print(table_main_room_regex())
    print(table_misc_room_regex())


if __name__ == "__main__":
    main()

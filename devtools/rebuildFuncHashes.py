from pathlib import Path
from typing import Final
from addFuncHashToSavable import addFuncHashToSavables
DATA_DIR = Path("data")
SAVABLES_DIR = DATA_DIR / "savables"
MODFILES_DIR = SAVABLES_DIR / "modfiles"
LT_DIR = Path("lib") / "liliths-throne-public"
LT_PKG_DIR: Final = LT_DIR / "src" / "com" / "lilithsthrone"


def main():
    addFuncHashToSavables(
        {SAVABLES_DIR / "core_info.yml", SAVABLES_DIR / "date.yml", SAVABLES_DIR / "dialogue_flags.yml"},
        LT_PKG_DIR / "game" / "Game.java",
        "exportGame",
        signature="public static void exportGame(String exportFileName, boolean allowOverwrite, boolean isAutoSave) {",
    )
    addFuncHashToSavables({SAVABLES_DIR / "game.yml"}, LT_PKG_DIR / "game" / "Game.java", "saveAsXML", signature="public Element saveAsXML(Element parentElement, Document doc) {")
    addFuncHashToSavables(
        {SAVABLES_DIR / "dialogue_flags.yml"}, LT_PKG_DIR / "game" / "dialogue" / "DialogueFlags.java", "saveAsXML", signature="public Element saveAsXML(Element parentElement, Document doc) {"
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "enchantment_recipe.yml"},
        LT_PKG_DIR / "game" / "dialogue" / "utils" / "EnchantmentDialogue.java",
        "saveEnchant",
        signature="public static void saveEnchant(String name, boolean allowOverwrite, DialogueNode dialogueNode) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "items" / "item_effect.yml", SAVABLES_DIR / "modfiles" / "clothing" / "item_effect.yml"},
        LT_PKG_DIR / "game" / "inventory" / "enchanting" / "ItemEffect.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "items" / "abstract_weapon.yml"},
        LT_PKG_DIR / "game" / "inventory" / "weapon" / "AbstractWeapon.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "items" / "abstract_item.yml"},
        LT_PKG_DIR / "game" / "inventory" / "item" / "AbstractItem.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "items" / "abstract_clothing.yml"},
        LT_PKG_DIR / "game" / "inventory" / "clothing" / "AbstractClothing.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "items" / "itemtypes" / "used_condom.yml"},
        LT_PKG_DIR / "game" / "inventory" / "item" / "AbstractFilledCondom.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "items" / "itemtypes" / "used_milker.yml"},
        LT_PKG_DIR / "game" / "inventory" / "item" / "AbstractFilledBreastPump.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "character" / "player_specific.yml", SAVABLES_DIR / "quests" / "quest_updates.yml"},
        LT_PKG_DIR / "game" / "character" / "PlayerCharacter.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {
            SAVABLES_DIR / "properties" / "age_preference.yml",
            SAVABLES_DIR / "properties" / "fetish_preference.yml",
            SAVABLES_DIR / "properties" / "gender_name.yml",
            SAVABLES_DIR / "properties" / "gender_preference.yml",
            SAVABLES_DIR / "properties" / "gender_pronoun.yml",
            SAVABLES_DIR / "properties" / "keybind.yml",
            SAVABLES_DIR / "properties" / "orientation_preference.yml",
            SAVABLES_DIR / "properties" / "previous_save.yml",
            SAVABLES_DIR / "properties" / "properties.yml",
            SAVABLES_DIR / "properties" / "settings.yml",
            SAVABLES_DIR / "properties" / "skin_colour_preference.yml",
        },
        LT_PKG_DIR / "game" / "Properties.java",
        "savePropertiesAsXML",
        signature="public void savePropertiesAsXML(){",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "eventlog" / "slaveryeventlogentry.yml"},
        LT_PKG_DIR / "game" / "dialogue" / "eventLog" / "SlaveryEventLogEntry.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "eventlog" / "eventlogentry.yml"},
        LT_PKG_DIR / "game" / "dialogue" / "eventLog" / "EventLogEntry.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {
            SAVABLES_DIR / "character" / "active_status_effect.yml",
            SAVABLES_DIR / "character" / "addictions_core.yml",
            SAVABLES_DIR / "character" / "artwork_override.yml",
            SAVABLES_DIR / "character" / "companions.yml",
            SAVABLES_DIR / "character" / "core.yml",
            SAVABLES_DIR / "character" / "family.yml",
            SAVABLES_DIR / "character" / "fetish_entry.yml",
            SAVABLES_DIR / "character" / "game_character.yml",
            SAVABLES_DIR / "character" / "helpful_info.yml",
            SAVABLES_DIR / "character" / "location_information.yml",
            SAVABLES_DIR / "character" / "pregnancy" / "pregnancy.yml",
            SAVABLES_DIR / "character" / "slavery_info.yml",
            SAVABLES_DIR / "character" / "stats" / "sex_stats.yml",
            SAVABLES_DIR / "character" / "stats" / "sex_type.yml",
            SAVABLES_DIR / "slavery" / "slave_station.yml",
        },
        LT_PKG_DIR / "game" / "character" / "GameCharacter.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "character" / "outfit.yml"},
        LT_PKG_DIR / "game" / "inventory" / "outfit" / "Outfit.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "character" / "body" / "offspring_body.yml", SAVABLES_DIR / "character" / "offspring_seed.yml", SAVABLES_DIR / "character" / "offspring_seed_data.yml"},
        LT_PKG_DIR / "game" / "character" / "npc" / "misc" / "OffspringSeed.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "character" / "character_inventory.yml"},
        LT_PKG_DIR / "game" / "inventory" / "CharacterInventory.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "character" / "blocked_parts.yml"},
        LT_PKG_DIR / "game" / "inventory" / "clothing" / "BlockedParts.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "character" / "addiction.yml"},
        LT_PKG_DIR / "game" / "character" / "effects" / "Addiction.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "character" / "elemental_specific.yml"},
        LT_PKG_DIR / "game" / "character" / "npc" / "misc" / "Elemental.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "character" / "npc_specific.yml"},
        LT_PKG_DIR / "game" / "character" / "npc" / "NPC.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "character" / "mixins" / "vending_machine_entry.yml", SAVABLES_DIR / "character" / "mixins" / "vendor_vending_machines.yml"},
        LT_PKG_DIR / "game" / "character" / "npc" / "submission" / "HazmatRat.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {
            SAVABLES_DIR / "character" / "body" / "antenna.yml",
            SAVABLES_DIR / "character" / "body" / "anus.yml",
            SAVABLES_DIR / "character" / "body" / "arms.yml",
            SAVABLES_DIR / "character" / "body" / "ass.yml",
            SAVABLES_DIR / "character" / "body" / "body.yml",
            SAVABLES_DIR / "character" / "body" / "body_covering.yml",
            SAVABLES_DIR / "character" / "body" / "bodycore.yml",
            SAVABLES_DIR / "character" / "body" / "breasts.yml",
            SAVABLES_DIR / "character" / "body" / "ears.yml",
            SAVABLES_DIR / "character" / "body" / "eyes.yml",
            SAVABLES_DIR / "character" / "body" / "face.yml",
            SAVABLES_DIR / "character" / "body" / "hair.yml",
            SAVABLES_DIR / "character" / "body" / "horns.yml",
            SAVABLES_DIR / "character" / "body" / "legs.yml",
            SAVABLES_DIR / "character" / "body" / "mouth.yml",
            SAVABLES_DIR / "character" / "body" / "nipples.yml",
            SAVABLES_DIR / "character" / "body" / "penis.yml",
            SAVABLES_DIR / "character" / "body" / "spinneret.yml",
            SAVABLES_DIR / "character" / "body" / "tail.yml",
            SAVABLES_DIR / "character" / "body" / "tentacles.yml",
            SAVABLES_DIR / "character" / "body" / "testicles.yml",
            SAVABLES_DIR / "character" / "body" / "tongue.yml",
            SAVABLES_DIR / "character" / "body" / "torso.yml",
            SAVABLES_DIR / "character" / "body" / "vagina.yml",
            SAVABLES_DIR / "character" / "body" / "wings.yml",
        },
        LT_PKG_DIR / "game" / "character" / "body" / "Body.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "character" / "body" / "covering.yml"},
        LT_PKG_DIR / "game" / "character" / "body" / "coverings" / "Covering.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "character" / "body" / "scar.yml"},
        LT_PKG_DIR / "game" / "character" / "markings" / "Scar.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "character" / "body" / "tattoos" / "tattoo_writing.yml"},
        LT_PKG_DIR / "game" / "character" / "markings" / "TattooWriting.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "character" / "body" / "tattoos" / "tattoo_counter.yml"},
        LT_PKG_DIR / "game" / "character" / "markings" / "TattooCounter.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "character" / "body" / "tattoos" / "tattoo.yml"},
        LT_PKG_DIR / "game" / "character" / "markings" / "Tattoo.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "character" / "stats" / "sex_count.yml"},
        LT_PKG_DIR / "game" / "character" / "SexCount.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "character" / "fluids" / "stored_fluid.yml"},
        LT_PKG_DIR / "game" / "character" / "FluidStored.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "character" / "fluids" / "girlcum.yml"},
        LT_PKG_DIR / "game" / "character" / "body" / "FluidGirlCum.java",
        "saveAsXML",
        signature="public Element saveAsXML(String rootElementName, Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "character" / "fluids" / "cum.yml"},
        LT_PKG_DIR / "game" / "character" / "body" / "FluidCum.java",
        "saveAsXML",
        signature="public Element saveAsXML(String rootElementName, Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "character" / "fluids" / "milk.yml"},
        LT_PKG_DIR / "game" / "character" / "body" / "FluidMilk.java",
        "saveAsXML",
        signature="public Element saveAsXML(String rootElementName, Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "character" / "pregnancy" / "litter.yml"},
        LT_PKG_DIR / "game" / "character" / "pregnancy" / "Litter.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "character" / "pregnancy" / "pregnancy_possibility.yml"},
        LT_PKG_DIR / "game" / "character" / "pregnancy" / "PregnancyPossibility.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {
            SAVABLES_DIR / "modfiles" / "clothing" / "core_attributes.yml",
            SAVABLES_DIR / "modfiles" / "clothing" / "name.yml",
            SAVABLES_DIR / "modfiles" / "clothing" / "plural_name.yml",
            SAVABLES_DIR / "modfiles" / "clothing" / "type.yml",
        },
        LT_PKG_DIR / "game" / "inventory" / "clothing" / "AbstractClothingType.java",
        "ctor4xml",
        signature="public AbstractClothingType(File clothingXMLFile, String author) throws XMLLoadException { // Be sure to catch this exception correctly - if it's thrown mod is invalid and should not be continued to load",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "modfiles" / "status_effects" / "type.yml"},
        LT_PKG_DIR / "game" / "character" / "effects" / "AbstractStatusEffect.java",
        "ctor4xml",
        signature="public AbstractStatusEffect(File XMLFile, String author, boolean mod) {",
    )
    addFuncHashToSavables(
        {
            SAVABLES_DIR / "modfiles" / "weapons" / "colours.yml",
            SAVABLES_DIR / "modfiles" / "weapons" / "core_attributes.yml",
            SAVABLES_DIR / "modfiles" / "weapons" / "name.yml",
            SAVABLES_DIR / "modfiles" / "weapons" / "plural_name.yml",
            SAVABLES_DIR / "modfiles" / "weapons" / "type.yml",
        },
        LT_PKG_DIR / "game" / "inventory" / "weapon" / "AbstractWeaponType.java",
        "ctor4xml",
        signature="public AbstractWeaponType(File weaponXMLFile, String author, boolean mod) {",
    )
    addFuncHashToSavables({SAVABLES_DIR / "world" / "cell.yml"}, LT_PKG_DIR / "world" / "Cell.java", "saveAsXML", signature="public Element saveAsXML(Element parentElement, Document doc) {")
    addFuncHashToSavables({SAVABLES_DIR / "world" / "world.yml"}, LT_PKG_DIR / "world" / "World.java", "saveAsXML", signature="public Element saveAsXML(Element parentElement, Document doc) {")
    addFuncHashToSavables(
        {SAVABLES_DIR / "world" / "generic_place.yml"}, LT_PKG_DIR / "world" / "places" / "GenericPlace.java", "saveAsXML", signature="public Element saveAsXML(Element parentElement, Document doc) {"
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "slavery" / "milking_room.yml"},
        LT_PKG_DIR / "game" / "occupantManagement" / "MilkingRoom.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )
    addFuncHashToSavables(
        {SAVABLES_DIR / "slavery" / "game_slavery_controller.yml"},
        LT_PKG_DIR / "game" / "occupantManagement" / "OccupancyUtil.java",
        "saveAsXML",
        signature="public Element saveAsXML(Element parentElement, Document doc) {",
    )


if __name__ == "__main__":
    main()

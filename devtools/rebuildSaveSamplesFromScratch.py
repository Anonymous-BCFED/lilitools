from __future__ import annotations

import argparse
import os
import shutil
from pathlib import Path
import sys
from typing import Optional

from declarative_argparse import DeclarativeOptionParser
from lilitools.cli.lilirun.utils import getStoredLTVersions
from ltautogui.config import Configuration
from ltautogui.female import createFemaleSave
from ltautogui.male import createMaleSave


def whichOrNull(cmd: str) -> Optional[Path]:
    p = shutil.which(cmd)
    if p is None:
        return None
    return Path(p)


class AutoGUIDOP(DeclarativeOptionParser):
    def __init__(self) -> None:
        super().__init__(argp=argparse.ArgumentParser())
        self.configure = self.addStoreTrue(
            "--configure", description="Auto-configure .autolili.toml."
        ).setDefault(False)
        self.jar = self.addStr(
            "--set-jar", description="Manually configure jar file."
        ).setDefault(None)
        self.version = self.addStr(
            "--set-version", description="Manually configure game version."
        ).setDefault(None)
        self.char = self.addStr("char", description="Which character to use")
        self.char.choices = ["male", "female"]

        self.yiuttiabi = self.addStoreTrue(
            "--yes-i-understand-that-this-is-a-bad-idea"
        ).setDefault(False)


def main() -> int:
    args = AutoGUIDOP()
    args.parseArguments()
    cfg: Configuration
    if args.configure.get_value():
        cfg = Configuration()
        suffix = ".exe" if os.name == "nt" else ""
        cfg.lilirun = Path.cwd() / "dist" / "bin" / ("lilirun" + suffix)
        cfg.xdotool = whichOrNull("xdotool")
        cfg.java = whichOrNull("java")
        cfg.jar = args.jar.get_value() or getStoredLTVersions(Path.cwd() / "dist")[0]
        cfg.lt_version = (
            list(map(int, args.version.get_value().split(".")))
            if args.version.get_value() is not None
            else [0, 0, 0, 0]
        )
        cfg.saveToFile(Path(".autolili.toml"))
        print("Wrote .autolili.toml")
        return 0

    if not args.yiuttiabi.get_value():
        print(
            "THIS TOOL IS **ONLY** FOR USE WHEN THE SAVES NEED TO BE COMPLETELY REGENERATED FROM SCRATCH."
        )
        print(
            "IF YOU UNDERSTAND, RE-RUN COMMAND WITH --yes-i-understand-that-this-is-a-bad-idea"
        )
        return 1
    else:
        cfg = Configuration.FromFile(Path(".autolili.toml"))
        match args.char.get_value():
            case "male":
                createMaleSave(cfg)
            case "female":
                createFemaleSave(cfg)
            case other:
                print("ERROR: Please specify male or female for the char parameter.")
    return 0


if __name__ == "__main__":
    main()

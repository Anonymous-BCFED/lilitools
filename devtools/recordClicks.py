from __future__ import annotations

import argparse
import os
import shutil
from pathlib import Path
from typing import Optional, Union
from pynput import mouse, keyboard
from declarative_argparse import DeclarativeOptionParser
import pyautogui
from ltautogui.ltautogui import LTAutoGUI
from ltautogui.config import Configuration
from buildtools import os_utils
from buildtools.indentation import IndentWriter

class AutoGUIRecDOP(DeclarativeOptionParser):
    def __init__(self) -> None:
        super().__init__(argp=argparse.ArgumentParser())
        self.configure = self.addStoreTrue('--configure', description='Auto-configure .autolili.toml.').setDefault(False)
        self.jar = self.addStr('--set-jar', description='Manually configure jar file.').setDefault(None)
        self.version = self.addStr('--set-version', description='Manually configure game version.').setDefault(None)

        self.to_file = self.addStr('path')

def main() -> None:
    args = AutoGUIRecDOP()
    args.parseArguments()
    cfg: Configuration
    if args.configure.get_value():
        cfg = Configuration()
        suffix = ('.exe' if os.name == 'nt' else '')
        cfg.lilirun = Path.cwd() / 'dist' / 'bin' / ('lilirun'+suffix)
        cfg.xdotool = Path(shutil.which('xdotool'))
        cfg.java = Path(shutil.which('java'))
        cfg.jar = args.jar.get_value() or max(Path('dist').glob('*.jar'), key=os.path.getmtime)
        cfg.lt_version = list(map(int, args.version.get_value().split('.'))) if args.version.get_value() is not None else [0,0,0,0]
        cfg.saveToFile(Path('.autolili.toml'))
        print('Wrote .autolili.toml')
        return
    cfg = Configuration.FromFile(Path('.autolili.toml'))

    outfile = Path(args.to_file.get_value()).absolute()
    outfile.parent.mkdir(parents=True,exist_ok=True)
    
    g = LTAutoGUI(workdir=cfg.cwd,
                  java=cfg.java,
                  xdotool=cfg.xdotool,
                  lt_jarfile=cfg.jar,
                  title_bar=cfg.title_bar())
    with os_utils.Chdir(str(cfg.cwd)):
        with g.startLT((1280, 840), 30):
            pyautogui.PAUSE = 1.
            g.focus()
            with outfile.open('w') as f:
                w = IndentWriter(f)
                w.writeline('import os')
                w.writeline('import pyautogui')
                w.writeline('from .ltautogui.config import Configuration')
                w.writeline('from .ltautogui.ltautogui import LTAutoGUI')
                #w.writeline('from .ltautogui.menus import ResponseMenu')
                w.writeline('from buildtools import os_utils')
                with w.writeline('def main() -> None:'):
                    w.writeline('args = AutoGUIRecDOP()')
                    w.writeline('args.parseArguments()')
                    w.writeline('cfg: Configuration')
                    with w.writeline('if args.configure.get_value():'):
                        w.writeline("cfg = Configuration()")
                        w.writeline("suffix = ('.exe' if os.name == 'nt' else '')")
                        w.writeline("cfg.lilirun = Path.cwd() / 'dist' / 'bin' / ('lilirun' + suffix)")
                        w.writeline("cfg.xdotool = Path(shutil.which('xdotool'))")
                        w.writeline("cfg.java = Path(shutil.which('java'))")
                        w.writeline("cfg.jar = args.jar.get_value() or max(Path('dist').glob('*.jar'), key=os.path.getmtime)")
                        w.writeline("cfg.lt_version = list(map(int, args.version.get_value().split('.'))) if args.version.get_value() is not None else [0, 0, 0, 0]")
                        w.writeline("cfg.saveToFile(Path('.autolili.toml'))")
                        w.writeline("print('Wrote .autolili.toml')")
                        w.writeline("return")
                    w.writeline("cfg = Configuration.FromFile(Path('.autolili.toml'))")
                    w.writeline('g = LTAutoGUI.FromConfiguration(cfg)')
                    with w.writeline("with os_utils.Chdir(str(cfg.cwd)):"):
                        with w.writeline("with g.startLT((1280, 840), 30):"):
                            w.writeline("pyautogui.PAUSE = 1.")
                            w.writeline("g.focus()")
                            mlistener: mouse.Listener
                            klistener: keyboard.Listener
                            def record_click(x: int, y: int, btn: mouse.Button, down: bool) -> None:
                                if down:
                                    w.writeline(f"g.clickButtonAt('FIXME', {x}, {y})")
                            def record_keypress(key: Optional[Union[keyboard.Key, keyboard.KeyCode]]) -> None:
                                if key == keyboard.Key.esc:
                                    klistener.stop()
                                    mlistener.stop()
                                    return
                                keyid: str
                                if isinstance(key, keyboard.Key):
                                    keyid = key.name
                                elif isinstance(key, keyboard.KeyCode):
                                    keyid = key.char
                                else:
                                    return
                                    
                                w.writeline(f"g.useButtonByHotkey('FIXME', {keyid!r})")
                            mlistener = mouse.Listener(on_click=record_click)
                            mlistener.start()
                            klistener = keyboard.Listener(on_press=record_keypress)
                            klistener.start()
                            mlistener.join()

if __name__ == "__main__":
    main()
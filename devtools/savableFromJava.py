import io
import os
from pathlib import Path
import re

from ruamel.yaml import YAML

yaml = YAML(typ='rt')
from analysis.savable_scanner import SavableScanner

from _dev_regex import REG_JAVA_IDENT

class FileDoesNotExistError(Exception):
    pass


def CamelCase(s: str) -> str:
    return ''.join([str(chunk).title() for chunk in REG_JAVA_IDENT.findall(s)])

def lowerCamelCase(s: str) -> str:
    s = CamelCase(s)
    return s[0].lower()+s[1:]
    
def existingFilePath(inp: str) -> Path:
    if not (p := Path(inp)).is_file():
        raise FileDoesNotExistError(p)
    return p

def main():
    import argparse
    argp = argparse.ArgumentParser()
    argp.add_argument('javafile', type=existingFilePath)

    argp.add_argument('--target-instance-method', type=str, nargs='?', default='saveAsXML')
    argp.add_argument('--relative-to', type=Path, nargs='?', default=Path('lib') / 'liliths-throne-public' / 'src' / 'com' / 'lilithsthrone')
    
    args = argp.parse_args()

    ss = SavableScanner(args.javafile)
    ss.saveAsXMLName = args.target_instance_method
    o = ss.start()
    GAMESRC_DIR = Path('lib') / 'liliths-throne-public' / 'src' / 'com' / 'lilithsthrone'
    o['generate']['python'] = 'SUGGESTION:'+str((args.javafile.relative_to(args.relative_to).parent) / '_savables' / (lowerCamelCase(args.javafile.stem)+'.py'))
    
    print('---')
    
    f = io.StringIO()
    yaml.dump(o, f)
    print(f.getvalue())

    with open('foundtypes.yml', 'w') as f:
        ft = [x.serialize() for x in ss.knownTypes]
        yaml.dump(ft, f)
if __name__ == '__main__':
    main()

import json
import os
import shutil
import human_readable
from pathlib import Path
from ruamel.yaml import YAML

DEST_DIR = Path("data") / "dumps"
SRC_DIR = Path("dist") / "data" / "internalData"


def fmtsz(sz: int) -> str:
    return f"{sz:,} B ({human_readable.file_size(sz)})"


def copy(src: Path, dest: Path) -> None:
    print(f"JSON:\t{src} -> {dest}")
    with src.open("r") as r:
        data = json.load(r)
    with dest.open("w") as w:
        json.dump(data, w, sort_keys=True, indent="  ")
    srcsize = os.path.getsize(src)
    destsize = os.path.getsize(dest)
    print(f"  OUT: {fmtsz(srcsize)} -> {fmtsz(destsize)} = {destsize/srcsize*100:.2f}%")


BASENAMES = {
    # "clothing", Too fucking big
    "effects",
    "fetishes",
    "items",
    "menus",
    "perks",
    "places",
    "racialBodies",
    "slavejobs",
    "statusEffects",
    "subspecies",
}
BODYPARTS = {
    "arms",
}


def rec_sort(d):
    if isinstance(d, dict):
        res = dict()
        for k in sorted(d.keys()):
            res[k] = rec_sort(d[k])
        return res
    if isinstance(d, list):
        for idx, elem in enumerate(d):
            d[idx] = rec_sort(elem)
    return d


def split_to_yaml_files(
    origfile: Path, outdir: Path, content_attr: str | None, keyattr: str = "id"
) -> None:
    data: dict
    with origfile.open("r") as f:
        data = json.load(f)
    written = set()
    version = data.pop("version")
    if content_attr is not None:
        data = data[content_attr]
    for k, v in data.items():
        outfile = outdir / "vanilla" / f"{k}.yml"
        outfile.parent.mkdir(parents=True, exist_ok=True)
        newdata = {keyattr: k, "version": version}
        newdata.update(v)
        with outfile.open("w") as f:
            y = YAML(typ="safe", pure=True)
            y.indent = 2
            y.default_flow_style = False
            y.dump(rec_sort(newdata), f)
        written.add(outfile.absolute())
        destsize = os.path.getsize(outfile)
        print(f"-> {outfile} {fmtsz(destsize)}")
    for filename in outdir.rglob("*.yml"):
        if filename.absolute() not in written:
            print("rm", filename)
            os.remove(filename)


def main():
    for bn in sorted(BASENAMES):
        copy(SRC_DIR / f"{bn}.json", DEST_DIR / f"{bn}.json")
    for bn in sorted(BODYPARTS):
        copy(
            SRC_DIR / "bodyparts" / f"{bn}.json", DEST_DIR / "bodyparts" / f"{bn}.json"
        )
    split_to_yaml_files(SRC_DIR / "clothing.json", DEST_DIR / "clothing", "clothing")


if __name__ == "__main__":
    main()

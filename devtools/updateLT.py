import subprocess
import sys
from pathlib import Path

from buildtools import os_utils

MALE_SAVE_BASE_COMMIT: str = 'edf1e4cbb7871f2cb04413bf145d329b6c75b702'
MALE_SAVE_PATH: Path = Path.cwd() / 'data' / 'samples' / 'saves' / 'male.xml'
FEMALE_SAVE_BASE_COMMIT: str = 'edf1e4cbb7871f2cb04413bf145d329b6c75b702'
FEMALE_SAVE_PATH: Path = Path.cwd() / 'data' / 'samples' / 'saves' / 'female.xml'

LT_SUBMODULE_PATH = Path('lib') / 'liliths-throne-public'
BUILD_PY = Path('devtools') / 'build.py'
GEN_ENUMS_PY = Path('devtools') / 'genEnums.py'
DEPLOY_SAMPLES_SH = Path('devtools') / 'DEPLOY_SAVE_SAMPLES.sh'
GET_SAMPLES_SH = Path('devtools') / 'BUILD_TEST_XML_SAMPLES.sh'

GIT = os_utils.assertWhich('git')
BASH = os_utils.assertWhich('bash')
os_utils.cmd([sys.executable, str(BUILD_PY), '--clean'], echo=True, show_output=True, critical=True)
os_utils.cmd([sys.executable, str(GEN_ENUMS_PY), '--clean', '-L', str(LT_SUBMODULE_PATH)], echo=True, show_output=True, critical=True)
os_utils.cmd([GIT, 'submodule', 'foreach', 'git', 'clean', '-fdx'], echo=True, show_output=True, critical=True)
os_utils.cmd([GIT, 'submodule', 'foreach', 'git', 'reset', '--hard'], echo=True, show_output=True, critical=True)
os_utils.cmd([GIT, 'submodule', 'update', '--init', '--remote', str(LT_SUBMODULE_PATH)], echo=True, show_output=True, critical=True)
os_utils.cmd([sys.executable, str(GEN_ENUMS_PY), '-L', str(LT_SUBMODULE_PATH)], echo=True, show_output=True, critical=True)
#os_utils.cmd([sys.executable, str(BUILD_PY), '--rebuild'], echo=True, show_output=True, critical=True)
subprocess.check_call([sys.executable, str(BUILD_PY), '--rebuild'])
os_utils.cmd([GIT, 'checkout', MALE_SAVE_BASE_COMMIT, '--', str(MALE_SAVE_PATH)])
os_utils.cmd([GIT, 'checkout', FEMALE_SAVE_BASE_COMMIT, '--', str(FEMALE_SAVE_PATH)])
os_utils.cmd([BASH, DEPLOY_SAMPLES_SH], echo=True, show_output=True, critical=True)
#os_utils.cmd([BASH, GET_SAMPLES_SH], echo=True, show_output=True, critical=True)
print('Now run lilirun and regenerate save samples')
print('See docs')
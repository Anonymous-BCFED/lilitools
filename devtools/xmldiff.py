import difflib
from typing import BinaryIO, Dict, List, TextIO

import tqdm
from lxml import etree
import click


def genXML2OfElement(tree: etree._ElementTree, e: etree._Element, o: List[str], tq: tqdm.tqdm) -> None:
    path: str = tree.getpath(e)
    o.append(f'{path}')
    for k, v in sorted(e.attrib.items(), key=lambda x: x[0]):
        o.append(f'{path}/@{k}={v!r}')
    if e.text is not None:
        o.append(f'{path}/text()={e.text!r}')
    if e.tail is not None:
        o.append(f'{path}/tail()={e.tail!r}')
    childmap: Dict[str, List[etree._Element]] = {}
    child: etree._Element
    for child in e.getchildren():
        if child.tag not in childmap:
            childmap[child.tag] = [child]
        else:
            childmap[child.tag].append(child)
    for ctag in sorted(childmap.keys()):
        for child in childmap[ctag]:
            genXML2OfElement(tree, child, o, tq)
    tq.update(1)


def genXML2OfFile(f: BinaryIO) -> List[str]:
    xmlp = etree.XMLParser(remove_blank_text=True, remove_comments=True)
    print(f'Parsing tree of {f.name}...')
    tree: etree._ElementTree = etree.parse(f, parser=xmlp)
    nelems: int = 0
    for _ in tree.iter('*'):
        nelems += 1
    o: List[str] = []
    tq = tqdm.tqdm(total=nelems, desc=f'Processing {f.name}...')
    genXML2OfElement(tree, tree.getroot(), o, tq)
    return o


def main() -> None:
    import argparse

    argp = argparse.ArgumentParser()
    argp.add_argument('rightfile', type=argparse.FileType('rb'))
    argp.add_argument('leftfile', type=argparse.FileType('rb'))
    argp.add_argument('outfile', type=argparse.FileType('w'))

    args = argp.parse_args()

    leftlines = genXML2OfFile(args.leftfile)
    rightlines = genXML2OfFile(args.rightfile)
    click.secho(f'Writing to {args.outfile.name}...', fg='green')
    w: TextIO = args.outfile
    for ln in difflib.unified_diff(rightlines, leftlines, args.rightfile.name, args.leftfile.name):
        w.write(f'{ln}\n')
    click.secho(f'    Done!', fg='green')


if __name__ == "__main__":
    main()

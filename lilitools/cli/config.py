import os
from enum import IntEnum
from functools import lru_cache
from pathlib import Path
from typing import Optional

import human_readable
import toml
from platformdirs import PlatformDirs
from rich import get_console

from lilitools.cli.units import BaseUnitSystemProvider, getPresentingUnitSystem, setPresentingUnitSystem
from lilitools.consts import APP_NAME


class EGitLibrary(IntEnum):
    DULWICH = 0
    GIT_CLI = 1


class EUnitPresentationSystem(IntEnum):
    DETECT = 0
    UK_IMPERIAL = 1
    US_IMPERIAL = 2
    METRIC = 3


class LTConfiguration:
    VERSION: int = 1
    _I: Optional["LTConfiguration"] = None

    def __init__(self) -> None:
        self.platformdirs: PlatformDirs = PlatformDirs(APP_NAME, False)

        self.config_dir: Path = self.platformdirs.user_config_path
        self.local_dir: Path = self.platformdirs.user_data_path
        self.cache_dir: Path = self.platformdirs.user_cache_path

        self.config_file: Path = self.config_dir / "config.toml"
        self.backup_dir: Path = self.local_dir / "backup"

        self.lt_root_dir: Optional[Path] = None

        self.git_library: EGitLibrary = EGitLibrary.DULWICH
        self.git_executable: Optional[Path] = None

        self.unit_system: EUnitPresentationSystem = EUnitPresentationSystem.DETECT

    @property
    @lru_cache
    def lt_data_dir(self) -> Path:
        return self.lt_root_dir / "data"

    @property
    @lru_cache
    def lt_res_dir(self) -> Path:
        return self.lt_root_dir / "res"

    def save(self) -> None:
        self.config_dir.mkdir(parents=True, exist_ok=True)
        self.local_dir.mkdir(parents=True, exist_ok=True)
        self.cache_dir.mkdir(parents=True, exist_ok=True)

        data = {
            "VERSION": self.VERSION,
            "settings": {
                "git-library": self.git_library.name.lower(),
                "unit-system": self.unit_system.name.lower(),
            },
            "paths": {
                "backup-dir": str(self.backup_dir.resolve()),
                "git": (
                    str(self.git_executable.resolve())
                    if self.git_executable is not None
                    else None
                ),
                "lt-root-dir": str(self.lt_root_dir.resolve()),
            },
        }
        with self.config_file.open("w") as f:
            toml.dump(data, f)
        console = get_console()
        sz = human_readable.file_size(os.path.getsize(self.config_file))
        console.print(f"[green]Wrote[/] {sz} [green]to[/] {self.config_file}")

    @classmethod
    def GetInstance(cls) -> "LTConfiguration":
        if cls._I is not None:
            return cls._I
        cls._I = cls.Load()
        return cls._I

    @classmethod
    def Load(cls) -> "LTConfiguration":
        cfg: "LTConfiguration" = cls()
        try:
            if cfg.config_file.is_file():
                with cfg.config_file.open("r") as f:
                    data = toml.load(f)

                assert data["VERSION"] == cls.VERSION

                paths = dict(data["paths"])
                cfg.backup_dir = Path(
                    paths.get("backup-dir", str(cfg.backup_dir.absolute()))
                )
                if "lt-root-dir" in paths and paths["lt-root-dir"] is not None:
                    cfg.lt_root_dir = Path(paths["lt-root-dir"]).resolve()
                if "git" in paths and paths["git"] is not None:
                    cfg.git_executable = Path(paths["git"])

                settings = data["settings"]
                if "git-library" in settings and settings["git-library"] is not None:
                    cfg.git_library = EGitLibrary[settings["git-library"].upper()]
                if "unit-system" in settings and settings["unit-system"] is not None:
                    cfg.unit_system = EUnitPresentationSystem[
                        settings["unit-system"].upper()
                    ]
        except Exception as e:
            print(print(str(e)))
            print("CONFIGURATION LOAD FAILURE. Loading defaults...")

        # Post process
        match cfg.unit_system:
            # case EUnitPresentationSystem.DETECT:
            case EUnitPresentationSystem.METRIC:
                setPresentingUnitSystem("si")
            case EUnitPresentationSystem.UK_IMPERIAL:
                setPresentingUnitSystem("uk")
            case EUnitPresentationSystem.US_IMPERIAL:
                setPresentingUnitSystem("us")

        return cfg

    def getUnitSystem(self) -> BaseUnitSystemProvider:
        return getPresentingUnitSystem()

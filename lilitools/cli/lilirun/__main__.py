"""
Just launches Lilith's Throne.  

Can also install OpenJFX too.

Intended for Linux users.
"""

# nuitka-project: --python-flag=-O

import collections
import json
import os
import shlex
import shutil
import subprocess
import sys
import zipfile
from enum import Enum, IntEnum
from pathlib import Path
from typing import Final, Iterable, List, Optional, OrderedDict, Set, Tuple, Union, cast

# import jproperties
import requests
import tqdm
from rich.console import Console

from lilitools.cli.lilirun.utils import dumpStoredLTVersions, getStoredLTVersions
from lilitools.cli.utils import print_header
from lilitools.mod_tests import ModChecker
from lilitools.regex_consts import REG_GLUON_CUSTOMJS, REG_GLUON_VERSIONS, REG_GLUON_VERSIONTYPE

# Security fixes for CVE-2024-20932, CVE-2024-20918, CVE-2024-20952, CVE-2024-20919,
#  CVE-2024-20921, CVE-2024-20926, CVE-2024-20945, CVE-2024-20955
# https://adoptium.net/download/
MIN_OPENJDK_VERSION: Final[Tuple[int, int, int]] = (17, 0, 13)
MIN_ORACLE_JAVA_VERSION: Final[Tuple[int, int, int]] = (1, 8, 0, 401)

# https://gluonhq.com/products/javafx/openjfx-17-release-notes/#17.0.9
# Fixes a vulnerability in 17.0.8, borked tests fixed in 17.0.9
MIN_OPENJFX_VERSION: Final[Tuple[int, int, int]] = (17, 0, 9)
# JFX 21 causes problems
MAX_OPENJFX_VERSION: Final[Tuple[int, int, int]] = (17, 999, 999)


class EJavaFXArches(Enum):
    X64 = "x64"


class EJavaFXOS(Enum):
    LINUX = "linux"
    MACOS = "osx"
    WINDOWS = "windows"


class EJavaFXDLType(Enum):
    def __init__(
        self,
        label: str,
        name: str,
        dlLabel: str,
        dlPrefix: str,
    ) -> None:
        self.label = label
        self.name_ = name
        self.dlLabel = dlLabel
        self.dlPrefix = dlPrefix

    SDK = "sdk", "SDK", "sdk", ""
    JMODS = "jmods", "jmods", "jmods", ""
    MONOCLE = "monocle", "Monocle SDK", "sdk", "monocle-"
    JAVADOC = "javadoc", "Javadoc", "javadoc", ""


class VersionType(Enum):
    LTS = "VersionType.LTS"
    ARCHIVED = "VersionType.ARCHIVED"
    LTS_ARCHIVED = "VersionType.LTS_ARCHIVED"
    NON_LTS = "VersionType.NON_LTS"
    EARLY_ACCESS = "VersionType.EARLY_ACCESS"


class JavaFXVersion:
    def __init__(
        self,
        label: str,
        major: int,
        minor: int,
        security: int,
        patch: int,
        build: int,
        older: bool,
        hashes: Iterable[str],
    ) -> None:
        self.label: VersionType = VersionType[label]
        self.major: int = major
        self.minor: int = minor
        self.security: int = security
        self.patch: int = patch
        self.build: int = build
        self.older: bool = older
        self.hashes: Set[str] = set(hashes)

        self.isEarlyAccess = "#ea" in self.hashes
        self.isLTS = any(x.startswith("#lts") for x in self.hashes)

        self.fullVersion = ".".join(
            map(str, [self.major, self.minor, self.security, self.patch])
        )

        self.simpleVersion = ""
        if self.patch > 0:
            self.simpleVersion = ".".join(
                map(str, [self.major, self.minor, self.security, self.patch])
            )
        elif self.minor > 0 and self.security > 0:
            self.simpleVersion = ".".join(
                map(str, [self.major, self.minor, self.security])
            )
        else:
            self.simpleVersion = str(self.major)  # '.'.join(map(str, [self.major]))

        self.version = self.simpleVersion
        if self.build > 0:
            self.version += f"-ea+{self.build}"

    def toURL(
        self,
        os: EJavaFXOS,
        arch: EJavaFXArches,
        type: EJavaFXDLType = EJavaFXDLType.SDK,
    ) -> str:
        return f"https://download2.gluonhq.com/openjfx/{self.simpleVersion}/openjfx-{self.version}_{type.dlPrefix}{os.value}-{arch.value}_bin-{type.dlLabel}.zip"

    def getVersionTuple(self) -> Tuple[int, int, int]:
        return self.major, self.minor, self.security

    def __str__(self) -> str:
        return f"JavaFX {self.label!r} - {self.fullVersion} {self.hashes!r} {{older: {self.older}, ea:{self.isEarlyAccess}, lts:{self.isLTS}}}"

    @classmethod
    def Parse(
        cls,
        string: str,
    ) -> "JavaFXVersion":
        # print(repr(string))
        # wtf
        string = string.replace("\t", "")
        # constructor(label, major, minor, security, patch, build, older, hashes) {
        try:
            label, major, minor, security, patch, build, older, hashes = json.loads(
                f"[{string}]"
            )
            return cls(label, major, minor, security, patch, build, older, hashes)
        except Exception as e:
            print(f"ERROR PARSING: [{string}]")
            print(e)
            sys.exit(1)


ARCH = EJavaFXArches.X64
OS = EJavaFXOS.LINUX


def list2cmd(cmd_: List[str]) -> str:
    return " ".join([shlex.quote(x) for x in cmd_])


def cmd(cmdl: Union[str, List[str]]) -> None:
    print("$ " + list2cmd(cmdl))
    subprocess.check_call(cmdl, shell=False)


class JavaVersionFlavor(IntEnum):
    JAVASE = 0
    OPENJDK = 1


class JavaVersion:
    FLAVOR: JavaVersionFlavor
    FLAVOR_NAME: str

    def __init__(self, version: Tuple[int, int, int, int]) -> None:
        self.version: Tuple[int, int, int, int] = version


class OpenJDKVersion(JavaVersion):
    FLAVOR = JavaVersionFlavor.OPENJDK
    FLAVOR_NAME = "OpenJDK"


class JavaSEVersion(JavaVersion):
    FLAVOR = JavaVersionFlavor.JAVASE
    FLAVOR_NAME = "Oracle Java SE"


def getJREVersion() -> Optional[JavaVersion]:
    output = subprocess.getoutput("java -version")
    is_openjdk = "OpenJDK Runtime Environment" in output
    is_oraclejre = "Java(TM) SE Runtime Environment" in output
    for line in output.splitlines():
        if line.startswith("openjdk version"):
            """
            openjdk version "11.0.11" 2021-04-20
            OpenJDK Runtime Environment (build 11.0.11+9-Ubuntu-0ubuntu2.20.04)
            OpenJDK 64-Bit Server VM (build 11.0.11+9-Ubuntu-0ubuntu2.20.04, mixed mode, sharing)
            """
            _, _, version, date = line.split(" ")[:4]
            chunks = version.strip('"').split(".")
            if "_" in chunks[-1]:
                chunks = chunks[:-1] + chunks[-1].split("_")
            versionraw = list([int(x) for x in chunks])
            while len(versionraw) < 4:
                versionraw.append(0)
            vt: Tuple[int, int, int, int] = tuple(versionraw)
            return OpenJDKVersion(vt)
        elif line.startswith("java version"):
            """
            java version "1.8.0_401"
            Java(TM) SE Runtime Environment (build 1.8.0_401-b10)
            Java HotSpot(TM) 64-Bit Server VM (build 25.401-b10, mixed mode)
            """
            _, _, version = line.split(" ")
            chunks = version.strip('"').split(".")
            if "_" in chunks[-1]:
                chunks = chunks[:-1] + chunks[-1].split("_")
            versionraw = list([int(x) for x in chunks])
            while len(versionraw) < 4:
                versionraw.append(0)
            vt: Tuple[int, int, int, int] = tuple(versionraw)
            return JavaSEVersion(vt)
    return None


def getJavaFXVersions() -> List[JavaFXVersion]:
    o: List[JavaFXVersion] = []
    uri = "https://gluonhq.com/products/javafx/"
    print(f"GET {uri}")
    req = requests.get(uri)
    req.raise_for_status()
    if m := REG_GLUON_CUSTOMJS.search(req.text):
        uri = f"https:{m[0]}"
        print(f"GET {uri}")
        req = requests.get(uri)
        req.raise_for_status()
        for vm in REG_GLUON_VERSIONS.finditer(req.text):
            # print(f"Found {vm!r}")
            # for i, g in enumerate(vm.groups()):
            #    print(f' [{i+1}] {g!r}')
            s = vm[1]
            s = REG_GLUON_VERSIONTYPE.sub(r'"\1"', s)
            s = s.replace("RECENT,", "false,")
            s = s.replace("ARCHIVED,", "true,")
            # print(f'Fixed args: {s!r}')
            jfx = JavaFXVersion.Parse(s)
            # print(f'Found {jfx.simpleVersion} ({jfx.version}) - {jfx.getVersionTuple()}')
            o.append(jfx)
    return o


def getLatestJavaFXVersion() -> Optional[JavaFXVersion]:
    latestV = (0, 0, 0, 0)
    latest: Optional[JavaFXVersion] = None
    for jfx in getJavaFXVersions():
        v = jfx.getVersionTuple()
        # print(jfx, v)
        if jfx.isEarlyAccess or jfx.older or not jfx.isLTS:
            continue
        if v > latestV and v >= MIN_OPENJFX_VERSION and v <= MAX_OPENJFX_VERSION:
            # print(f'{v} > {latestV}')
            latestV = v
            latest = jfx
    return latest


def is_compiled() -> bool:
    return "__compiled__" in globals()


def b2yesno(val: bool) -> str:
    return "yes" if val else "no"


def parseDefine(val: str) -> Tuple[str, str]:
    chunks = val.split("=", 1)
    assert (
        len(chunks) == 2
    ), f"{val!r}: Each value must contain an equals sign (=) in order to split it into a key and a value."
    return cast(Tuple[str, str], tuple(chunks))


def main():
    import argparse

    # arg0 = f"{sys.executable} {__file__}"
    # if "__compiled__" in globals():
    #     arg0 = str(Path("bin") / "lilirun") + (".exe" if os.name == "nt" else "")

    argp = argparse.ArgumentParser("lilirun")

    argp.add_argument(
        "--version", action="store_true", default=False, help="Show version and exit"
    )

    argp.add_argument(
        "--clobber-javafx",
        action="store_true",
        default=False,
        help="Nuke and redownload OpenJavaFX",
    )
    argp.add_argument(
        "--no-run",
        action="store_true",
        default=False,
        help="Don't actually run Lilith's Throne. Useful for debugging.",
    )
    argp.add_argument(
        "--define",
        "-D",
        nargs="*",
        dest="defines",
        type=parseDefine,
        default=[],
        help="Specify -D* args to pass to Java. Format: -D[=]k=v",
    )
    argp.add_argument(
        "passthru", nargs=argparse.REMAINDER, help="Things to pass to LT."
    )

    args = argp.parse_args()

    defines = dict(args.defines)

    CWD = Path.cwd()

    print_header("lilirun")

    if args.version:
        sys.exit(0)
    print("-------------------------------------------------")
    print("Performing idiot checks...")

    strminjseversion = ".".join(map(str, MIN_ORACLE_JAVA_VERSION))
    strminopenjdkversion = ".".join(map(str, MIN_OPENJDK_VERSION))

    JAVA = shutil.which("java")
    JAVA_PATH: Path = Path(JAVA) if JAVA is not None else None
    failed = JAVA_PATH is None or not JAVA_PATH.is_file()
    print(f" - Is Java present? {b2yesno(not failed)} ({JAVA})")
    if failed:
        if not JAVA_PATH.is_file():
            print(
                f"   {JAVA_PATH} is present, but is not a file. This usually means you've *really* screwed up your system and need to completely reinstall Java JRE."
            )
            sys.exit(1)
        print(f"   Please install OpenJDK JRE >= {strminopenjdkversion}.")
        sys.exit(1)

    jversion = getJREVersion()
    if jversion is None:
        print(f" - I can't figure out what in the Hell kind of Java you're using.")
        print(f"   Please install OpenJDK JRE >= {strminopenjdkversion}.")
        sys.exit(1)

    flavor_name: str
    strjversion: str = ".".join(map(str, jversion.version))
    strminjversion: str
    match jversion.FLAVOR:
        case JavaVersionFlavor.JAVASE:
            flavor_name = jversion.FLAVOR_NAME
            strminjversion = strminjseversion
            failed = jversion.version < MIN_ORACLE_JAVA_VERSION
        case JavaVersionFlavor.OPENJDK:
            flavor_name = jversion.FLAVOR_NAME
            strminjversion = strminopenjdkversion
            failed = jversion.version < MIN_OPENJDK_VERSION
    print(
        f" - Java version >= {strminjversion}? {b2yesno(not failed)} ({flavor_name} {strjversion})"
    )
    if failed:
        print(f"   Please install {flavor_name} >= {strminjversion}.")
        return

    RES_DIR = Path.cwd() / "res"
    RES_DIR_STR = RES_DIR.relative_to(Path.cwd())
    print(
        f" - Is resources directory ({RES_DIR_STR}) present? {b2yesno(RES_DIR.is_dir())}"
    )
    if not RES_DIR.is_dir():
        print(
            f"   Directory {RES_DIR_STR} is missing. Please extract Lilith's Throne to {str(Path.cwd())}."
        )
        return

    DATA_DIR = Path.cwd() / "data"
    DATA_DIR_STR = Path(".") / DATA_DIR.relative_to(Path.cwd())
    print(
        f" - Is data directory ({DATA_DIR_STR}) present? {b2yesno(DATA_DIR.is_dir())}"
    )
    if not DATA_DIR.is_dir():
        print(f"   Directory {DATA_DIR_STR} is missing. Creating...")
        DATA_DIR.mkdir(parents=True)

    BASE_DIR: Path
    for p in [Path.cwd(), Path.cwd() / "dist"]:
        if (p / "res" / "mods").is_dir():
            BASE_DIR = p
            break

    print(" - Checking mods for common errors...")
    tester = ModChecker(BASE_DIR, console=Console())
    messages_present = False
    for mod_dir, test_results in tester.check_all().items():
        if test_results.has_messages:
            messages_present = True
            print(f"   - {mod_dir.relative_to(BASE_DIR)}:")
            for i in sorted(test_results.info):
                print(f"      [INFO] {i}")
            for w in sorted(test_results.warnings):
                print(f"      [WARNING] {i}")
            for e in sorted(test_results.errors):
                print(f"      [ERROR] {i}")
    if not messages_present:
        print("   - No obvious problems found!")

    FXDIR: Path
    FXLIB: Path
    FXVER: Tuple[int, int, int]

    def _find_javafx() -> Tuple[Path, Path, Tuple[int, int, int]]:
        FXDIR = next(
            path
            for path in CWD.iterdir()
            if path.is_dir() and path.name.startswith("javafx-sdk-")
        )
        FXVER = tuple(map(int, FXDIR.name[11:].split(".")))
        FXLIB = FXDIR / "lib"
        verinfo_file = FXDIR / ".lilitools-version-info.json"
        try:
            if verinfo_file.is_file():
                with verinfo_file.open("r") as f:
                    data = json.load(f)
                    if data["_"] == 10232023:
                        FXVER = tuple(data["v"])
        except:
            pass
        return FXDIR, FXLIB, FXVER

    failed = False
    try:
        FXDIR, FXLIB, FXVER = _find_javafx()
        if (
            args.clobber_javafx
            or FXVER < MIN_OPENJFX_VERSION
            or FXVER >= MAX_OPENJFX_VERSION
        ):
            while True:
                FXDIR, _, _ = _find_javafx()
                print(f"rm -rf {FXDIR}")
                shutil.rmtree(FXDIR)
    except StopIteration:
        failed = True
    strreqjfxver = ".".join(map(str, MIN_OPENJFX_VERSION))
    strjfxver = ".".join(map(str, FXVER)) if not failed else "NOT FOUND"
    print(f" - Is JavaFX version >= {strreqjfxver}? ({strjfxver})")
    if failed:
        print("   Could not find matching install of OpenJFX! Installing...")
        tried_dl = True
        jfx = getLatestJavaFXVersion()
        url = jfx.toURL(OS, ARCH)

        req = requests.get(url, stream=True)
        req.raise_for_status()
        sz = int(req.headers.get("content-length", 0))
        with open("openjfx.zip", "wb") as zf:
            pb = tqdm.tqdm(
                total=sz,
                unit="iB",
                desc="openjfx.zip",
                unit_scale=True,
                unit_divisor=1024,
            )
            for data in req.iter_content(1024):
                pb.update(len(data))
                zf.write(data)
            pb.close()
        with zipfile.ZipFile("openjfx.zip", "r") as zip:
            zip.extractall()
        os.remove("openjfx.zip")

        try:
            FXDIR, FXLIB, FXVER = _find_javafx()
            # javafx_properties = jproperties.Properties()
            # propertiesfile = FXLIB /'javafx.properties'
            # with propertiesfile.open('rb') as f:
            #     javafx_properties.load(f)
            # Since lib/javafx.properties is next to fucking useless:
            data = {
                "_": 10232023,
                "v": [
                    # jfx.label,
                    jfx.major,
                    jfx.minor,
                    jfx.security,
                    jfx.patch,
                    # jfx.build,
                    # jfx.older,
                    # list(jfx.hashes),
                ],
            }
            verinfo_file = FXDIR / ".lilitools-version-info.json"
            with verinfo_file.open("w") as f:
                json.dump(data, f, indent=None, separators=(",", ":"))
        except StopIteration:
            print("Could not find JavaFX, even after downloading.")
            return 1

    allLTVers = getStoredLTVersions(CWD)
    dumpStoredLTVersions(allLTVers, allLTVers[0])
    LTJAR, LTVER = allLTVers[0]
    LTJAR = str(LTJAR)
    LTVER = ".".join(map(str, LTVER))

    JAVA_OPTS: List[str] = []
    JAVA_OPTS += [f"-D{k}={v}" for k, v in defines.items()]
    JAVA_OPTS.append(f"--module-path={FXLIB}")
    JAVA_OPTS.append(f"--add-modules=ALL-MODULE-PATH")

    print("Environment:")
    e: OrderedDict[str, str] = collections.OrderedDict()
    if "__compiled__" in globals():
        # Nuitka stuff
        e["__compiled__"] = repr(globals()["__compiled__"])
    else:
        e["__compiled__"] = "None (not compiled by Nuitka)"

    e.update(
        {
            "CWD": CWD,
            "FXDIR": FXDIR,
            "LTJAR": LTJAR,
            "JAVA": JAVA_PATH,
            "JAVA_HOME": os.environ.get("JAVA_HOME"),
            "defines": defines,
        }
    )

    for k, v in e.items():
        print(f"  {k:.<13}: {v}")

    cmd_ = [str(JAVA_PATH)]
    cmd_ += JAVA_OPTS
    cmd_ += ["-jar", str(LTJAR)] + args.passthru
    if args.no_run:
        print(f"Would run {list2cmd(cmd_)}")
    else:
        cmd(cmd_)


if __name__ == "__main__":
    main()

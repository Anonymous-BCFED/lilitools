from operator import itemgetter
from pathlib import Path
from typing import List, Optional, Tuple, TypeAlias

from lilitools.regex_consts import REG_LT_JARS

__all__ = ["LTPathTuple", "getStoredLTVersions", "dumpStoredLTVersions"]
LTPathTuple: TypeAlias = Tuple[Path, Tuple[int, int, int, int]]


def getStoredLTVersions(lt_jar_dir: Path) -> List[LTPathTuple]:
    possibilities = []
    for path in lt_jar_dir.iterdir():
        if (m := REG_LT_JARS.match(path.name)) is not None:
            version = tuple(
                [
                    int(m["major"]),
                    int(m["minor"]),
                    int(m["patch"]),
                    int(m["revision"] or "0"),
                ]
            )
            possibilities.append((path, version))
    possibilities.sort(key=itemgetter(1), reverse=True)
    return possibilities


def dumpStoredLTVersions(
    possibilities: List[LTPathTuple],
    selected: Optional[LTPathTuple] = None,
) -> None:
    print("Installed LT Versions:")
    for i, ltvt in enumerate(possibilities):
        p = "*" if ltvt == selected else "-"
        (path, version) = ltvt
        version = ".".join(map(str, version))
        print(f" {p} {version} ({path})")

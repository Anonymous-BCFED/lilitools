import sys

import lilitools.logging
from lilitools.cli.lilitool.commands import register_cmds
from lilitools.cli.utils import handle_logging, print_header, register_logging

click = lilitools.logging.ClickWrap()


def main() -> None:
    import argparse

    argp = argparse.ArgumentParser("lilitool")

    register_logging(argp)

    argp.add_argument(
        "--version", action="store_true", default=False, help="Print version and exit."
    )

    subp = argp.add_subparsers()

    register_cmds(subp)

    args = argp.parse_args()

    if args.version:
        print_header("lilitool")
        sys.exit(0)

    handle_logging(args)

    click.debug(f"args: {args!r}")
    if not hasattr(args, "cmd"):
        argp.print_help()
    else:
        args.cmd(args)


if __name__ == "__main__":
    main()

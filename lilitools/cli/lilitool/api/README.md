# LiliTools Public API

This is a set of programming interfaces that make it easier to perform common operations to Lilith's Throne saves. This should make it easier to make user scripts that perform many operations on a single loaded save.

You are still welcome to use our more complex internal structures, but this stuff is more user-friendly and designed to be used like the `lilitool` CLI.

## Getting Started

```python3
from lilitools.cli.lilitool.api import LTSaveFactory

save = LTSaveFactory.LoadFromFile('/path/to/save.xml')
#...
save.saveToFile('/path/to/save.xml')
```


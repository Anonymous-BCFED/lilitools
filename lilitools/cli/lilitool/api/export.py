from pathlib import Path
from typing import Optional, Union
from lilitools.logging import ClickWrap

from lilitools.saves.character.game_character import GameCharacter

from lxml import etree
from lilitools.saves.character.npc import NPC

from lilitools.saves.character.player_character import PlayerCharacter
from lilitools.saves.menders import mendCharacterXML
from lilitools.saves.upgraders.npc import NPCExportUpgrader
from lilitools.saves.upgraders.playercharacter import PlayerCharacterExportUpgrader
click = ClickWrap()
class LTExportFactory:
    @staticmethod
    def FromFile(filename: Union[str, Path]) -> Optional[GameCharacter]:
        path: Path = Path(filename)
        if not path.is_file():
            click.critical('Unable to find specified export XML file.')
        doc = etree.parse(str(path), parser=etree.XMLParser(remove_comments=True))
        root = doc.getroot()
        match root.tag:
            case "playerCharacter":
                pc = PlayerCharacter()
                PlayerCharacterExportUpgrader(root)
                mendCharacterXML(root)
                pc.fromXML(root)
                return pc
            case 'exportedCharacter':
                match root.xpath('.//character/core/pathName/@value')[0]:
                    case "com.lilithsthrone.game.character.PlayerCharacter":
                        PlayerCharacterExportUpgrader(root)
                        mendCharacterXML(root)
                        pc = PlayerCharacter()
                        pc.fromXML(root)
                        return pc
                    case _:
                        NPCExportUpgrader(root)
                        mendCharacterXML(root)
                        gc = NPC()
                        gc.fromXML(root)
                        return gc
        return None

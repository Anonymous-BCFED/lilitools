from pathlib import Path
from typing import Optional, Union

from lxml import etree

from lilitools.cli.lilitool.api.inventory import _InventoryAPIWrapper
from lilitools.logging import ClickWrap
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.character.npc import NPC
from lilitools.saves.character.player_character import PlayerCharacter
from lilitools.saves.inventory.character_inventory import CharacterInventory
from lilitools.saves.menders import mendCharacterXML
from lilitools.saves.upgraders.gamecharacter import GameCharacterExportUpgrader
from lilitools.saves.upgraders.npc import NPCExportUpgrader
from lilitools.saves.upgraders.playercharacter import PlayerCharacterExportUpgrader

click = ClickWrap()


class _GameCharacterAPIWrapper:
    def __init__(self, wrap: GameCharacter) -> None:
        self._wrapped: GameCharacter = wrap

    def asGameCharacter(self) -> GameCharacter:
        return self._wrapped

    def getInventory(self) -> CharacterInventory:
        return _InventoryAPIWrapper(self._wrapped.characterInventory)


class LTNPCFactory:
    @staticmethod
    def FromExport(filename: Union[str, Path]) -> Optional[NPC]:
        path: Path = Path(filename)
        if not path.is_file():
            click.critical('Unable to find specified export XML file.')
        doc = etree.parse(str(path), parser=etree.XMLParser(remove_comments=True))
        root = doc.getroot()
        NPCExportUpgrader(root)
        mendCharacterXML(root)
        gc = NPC()
        gc.fromXML(root)
        return gc

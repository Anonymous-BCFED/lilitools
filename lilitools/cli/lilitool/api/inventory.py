import contextlib
from enum import IntEnum
from typing import (
    Callable,
    Dict,
    Generic,
    Iterable,
    List,
    Optional,
    Sequence,
    Set,
    TypeVar,
)

from lilitools.logging import ClickWrap
from lilitools.saves.inventory.character_inventory import CharacterInventory
from lilitools.saves.inventoryslot import EInventorySlot
from lilitools.saves.items.abstract_clothing import AbstractClothing
from lilitools.saves.items.abstract_item import AbstractItem
from lilitools.saves.utils import andify_str

click = ClickWrap()


class InventorySearcher:
    def __init__(
        self,
        possible_ids: Iterable[str] = [],
        possible_names: Iterable[str] = [],
        sealed: Optional[bool] = None,
        enchantment_known: Optional[bool] = None,
    ) -> None:
        self.possible_ids: Set[str] = set(possible_ids)
        self.possible_names: Set[str] = set(possible_names)
        self.sealed: Optional[bool] = sealed
        self.enchantment_known: Optional[bool] = enchantment_known

    def match_clothing(self, clothing: AbstractClothing) -> bool:
        # print(clothing.id, repr(clothing.name))
        if len(self.possible_ids) > 0:
            if clothing.id not in self.possible_ids:
                return False
        if len(self.possible_names) > 0:
            if clothing.name not in self.possible_names:
                return False
        if self.sealed is not None:
            if clothing.sealed != self.sealed:
                return False
        if self.enchantment_known is not None:
            if clothing.enchantmentKnown != self.enchantment_known:
                return False
        return True

    def match_item(self, item: AbstractItem) -> bool:
        # print(f'{item.id!r} {item.name!r}')
        if len(self.possible_ids) > 0:
            if item.id not in self.possible_ids:
                return False
        if len(self.possible_names) > 0:
            if item.name not in self.possible_names:
                return False
        return True


class EInventoryField(IntEnum):
    EQUIPPED_CLOTHING = 0
    CARRIED_CLOTHING = 1
    CARRIED_ITEMS = 2
    MAIN_WEAPONS = 3
    OFFHAND_WEAPONS = 4


class MatchHandler:
    def onMatchEquippedClothing(
        self, clothing: AbstractClothing, clothingEquipped: List[AbstractClothing]
    ) -> None:
        pass

    def onMatchCarriedClothing(
        self,
        clothing: AbstractClothing,
        count: int,
        clothingInInventory: Dict[AbstractClothing, int],
    ) -> None:
        pass

    def onMatchCarriedItem(
        self, item: AbstractItem, count: int, itemsInInventory: Dict[AbstractItem, int]
    ) -> None:
        pass

    def onMatchMainWeapon(
        self, item: AbstractItem, mainWeapons: List[AbstractItem]
    ) -> None:
        pass

    def onMatchOffhandWeapon(
        self, item: AbstractItem, offhandWeapons: List[AbstractItem]
    ) -> None:
        pass


class RemoveAllMatchHandler(MatchHandler):
    def __init__(self, dry_run: bool) -> None:
        super().__init__()
        self.dry_run: bool = dry_run
        self.found: int = 0

    def onMatchCarriedClothing(
        self,
        clothing: AbstractClothing,
        count: int,
        clothingInInventory: Dict[AbstractClothing, int],
    ) -> None:
        self.found += count
        if self.dry_run:
            click.echo(f"Would remove {clothing.id} x{count} in carried clothing")
        else:
            click.echo(f"Removed {clothing.id} x{count} in carried clothing")
            del clothingInInventory[clothing]

    def onMatchCarriedItem(
        self, item: AbstractItem, count: int, itemsInInventory: Dict[AbstractItem, int]
    ) -> None:
        self.found += count
        if self.dry_run:
            click.echo(f"Would remove {item.id} x{count} in carried items")
        else:
            click.echo(f"Removed {item.id} x{count} in carried items")
            del itemsInInventory[item]

    def onMatchEquippedClothing(
        self, clothing: AbstractClothing, clothingEquipped: List[AbstractClothing]
    ) -> None:
        self.found += 1
        if self.dry_run:
            click.echo(f"Would remove {clothing.id} x1 in equipped clothing")
        else:
            click.echo(f"Removed {clothing.id} x1 in equipped clothing")
            clothingEquipped.remove(clothing)

    def onMatchMainWeapon(
        self, item: AbstractItem, mainWeapons: List[AbstractItem]
    ) -> None:
        self.found += 1
        if self.dry_run:
            click.echo(f"Would remove {item.id} x1 in main weapons")
        else:
            click.echo(f"Removed {item.id} x1 in main weapons")
            mainWeapons.remove(item)

    def onMatchOffhandWeapon(
        self, item: AbstractItem, offhandWeapons: List[AbstractItem]
    ) -> None:
        self.found += 1
        if self.dry_run:
            click.echo(f"Would remove {item.id} x1 in offhand weapons")
        else:
            click.echo(f"Removed {item.id} x1 in offhand weapons")
            offhandWeapons.remove(item)


T = TypeVar("T")


class SearchResult(Generic[T]):
    def __init__(self, context: str, thing: T, count: Optional[int] = None) -> None:
        self.context: str = context
        self.thing: T = thing
        self.count: Optional[int] = count


class SearchResults:
    def __init__(self) -> None:
        self.found_clothing: List[SearchResult[AbstractClothing]] = []
        self.found_items: List[SearchResult[AbstractItem]] = []


class CollectAllMatchHandler(MatchHandler):
    def __init__(self) -> None:
        super().__init__()
        self.results = SearchResults()

    def onMatchCarriedClothing(
        self,
        clothing: AbstractClothing,
        count: int,
        clothingInInventory: Dict[AbstractClothing, int],
    ) -> None:
        self.results.found_clothing.append(
            SearchResult("carried_clothing", clothing, count)
        )

    def onMatchCarriedItem(
        self, item: AbstractItem, count: int, itemsInInventory: Dict[AbstractItem, int]
    ) -> None:
        self.results.found_items.append(SearchResult("carried_items", item, count))

    def onMatchEquippedClothing(
        self, clothing: AbstractClothing, clothingEquipped: List[AbstractClothing]
    ) -> None:
        self.results.found_clothing.append(SearchResult("equipped_clothing", clothing))

    def onMatchMainWeapon(
        self, item: AbstractItem, mainWeapons: List[AbstractItem]
    ) -> None:
        self.results.found_items.append(SearchResult("mainWeapons", item))

    def onMatchOffhandWeapon(
        self, item: AbstractItem, offhandWeapons: List[AbstractItem]
    ) -> None:
        self.results.found_items.append(SearchResult("offhandWeapons", item))


def search_as_english(
    id: Optional[Sequence[str]] = None,
    name: Optional[Sequence[str]] = None,
    enchantment_known: Optional[bool] = None,
    is_sealed: Optional[bool] = None,
) -> str:
    outparts: List[str] = []
    if id is not None:
        if len(id) == 1:
            outparts.append(f"id equal to {id[0]!r}")
        if len(id) > 1:
            outparts.append(f"id equal to one of {set(id)!r}")
    if name is not None:
        if len(name) == 1:
            outparts.append(f"name equal to {name[0]!r}")
        if len(name) > 1:
            outparts.append(f"name equal to one of {set(name)!r}")
    if enchantment_known is not None:
        if enchantment_known:
            outparts.append("enchantment is known")
        else:
            outparts.append("enchantment is not known")
    if is_sealed is not None:
        if is_sealed:
            outparts.append("is sealed")
        else:
            outparts.append("is not sealed")
    return andify_str(outparts)


class _InventorySearchWrapperAPI:
    def __init__(
        self, inv: CharacterInventory, name: str, cis: InventorySearcher
    ) -> None:
        self.searcher: InventorySearcher = cis
        self.inv = inv
        self.name = name
        self.mec_listeners: List[
            Callable[
                [CharacterInventory, AbstractClothing, List[AbstractClothing]], None
            ]
        ] = []
        self.mcc_listeners: List[
            Callable[
                [
                    CharacterInventory,
                    AbstractClothing,
                    int,
                    Dict[AbstractClothing, int],
                ],
                None,
            ]
        ] = []
        self.mci_listeners: List[
            Callable[
                [CharacterInventory, AbstractItem, int, Dict[AbstractItem, int]], None
            ]
        ] = []
        self.mmw_listeners: List[
            Callable[[CharacterInventory, AbstractItem, List[AbstractItem]], None]
        ] = []
        self.mow_listeners: List[
            Callable[[CharacterInventory, AbstractItem, List[AbstractItem]], None]
        ] = []

    def addMatchedEquippedClothing(
        self,
        callback: Callable[
            [CharacterInventory, AbstractClothing, List[AbstractClothing]], None
        ],
    ) -> "_InventorySearchWrapperAPI":
        """
        clothing: AbstractClothing, clothingEquipped: List[AbstractClothing]
        """
        self.mec_listeners.append(callback)
        return self

    def addMatchedCarriedClothing(
        self,
        callback: Callable[
            [CharacterInventory, AbstractClothing, int, Dict[AbstractClothing, int]],
            None,
        ],
    ) -> "_InventorySearchWrapperAPI":
        """
        clothing: AbstractClothing, count: int, clothingInInventory: Dict[AbstractClothing, int]
        """
        self.mcc_listeners.append(callback)
        return self

    def addMatchedCarriedItem(
        self,
        callback: Callable[
            [CharacterInventory, AbstractItem, int, Dict[AbstractItem, int]], None
        ],
    ) -> "_InventorySearchWrapperAPI":
        """
        item: AbstractItem, count: int, itemsInInventory: Dict[AbstractItem, int]
        """
        self.mci_listeners.append(callback)
        return self

    def addMatchedMainWeapon(
        self,
        callback: Callable[
            [CharacterInventory, AbstractItem, List[AbstractItem]], None
        ],
    ) -> "_InventorySearchWrapperAPI":
        """
        item: AbstractItem, mainWeapons: List[AbstractItem]
        """
        self.mmw_listeners.append(callback)
        return self

    def addMatchedOffhandWeapon(
        self,
        callback: Callable[
            [CharacterInventory, AbstractItem, List[AbstractItem]], None
        ],
    ) -> "_InventorySearchWrapperAPI":
        """
        item: AbstractItem, offhandWeapons: List[AbstractItem]
        """
        self.mow_listeners.append(callback)
        return self

    def execute(
        self, handler: Optional[MatchHandler] = None, verbose: bool = False
    ) -> None:
        selfinv = self.inv
        matches: int = 0
        with click.secho("Equipped Clothing:") if verbose else contextlib.nullcontext():
            for clothing in list(selfinv.clothingEquipped):
                if verbose:
                    print("", repr(clothing.name))
                if self.searcher.match_clothing(clothing):
                    if handler is None:
                        for listener in self.mec_listeners:
                            listener(selfinv, clothing, selfinv.clothingEquipped)
                    else:
                        handler.onMatchEquippedClothing(
                            clothing, selfinv.clothingEquipped
                        )
                    matches += 1
        with click.secho("Carried Clothing:") if verbose else contextlib.nullcontext():
            for clothing, count in list(selfinv.clothingInInventory.items()):
                if verbose:
                    print("", repr(clothing.name), count)
                if self.searcher.match_clothing(clothing):
                    if handler is None:
                        for listener in self.mcc_listeners:
                            listener(
                                selfinv, clothing, count, selfinv.clothingInInventory
                            )
                    else:
                        handler.onMatchCarriedClothing(
                            clothing, count, selfinv.clothingInInventory
                        )
                    matches += 1
        with click.secho("Carried Items:") if verbose else contextlib.nullcontext():
            for item, count in list(selfinv.itemsInInventory.items()):
                if verbose:
                    print("", repr(item.name), count)
                if self.searcher.match_item(item):
                    if handler is None:
                        for listener in self.mci_listeners:
                            listener(selfinv, item, count, selfinv.itemsInInventory)
                    else:
                        handler.onMatchCarriedItem(
                            item, count, selfinv.itemsInInventory
                        )
                    matches += 1
        with click.secho("Main Weapons:") if verbose else contextlib.nullcontext():
            for item in list(selfinv.mainWeapons):
                if verbose:
                    print("", repr(item.name))
                if self.searcher.match_item(item):
                    if handler is None:
                        for listener in self.mmw_listeners:
                            listener(selfinv, item, selfinv.mainWeapons)
                    else:
                        handler.onMatchMainWeapon(item, selfinv.mainWeapons)
                    matches += 1
        with click.secho("Offhand Weapons:") if verbose else contextlib.nullcontext():
            for item in list(selfinv.offhandWeapons):
                if verbose:
                    print("", repr(item.name))
                if self.searcher.match_item(item):
                    if handler is None:
                        for listener in self.mow_listeners:
                            listener(selfinv, item, selfinv.offhandWeapons)
                    else:
                        handler.onMatchOffhandWeapon(item, selfinv.offhandWeapons)
                    matches += 1
        if verbose and not matches:
            click.warn(
                "Unable to find any inventory items matching the given criteria."
            )
        return matches

    def removeAll(self, verbose: bool = True, dry_run: bool = False) -> int:
        return self.execute(RemoveAllMatchHandler(dry_run=dry_run), verbose=verbose)

    def returnAll(self, verbose: bool = True) -> SearchResults:
        camh = CollectAllMatchHandler()
        self.execute(camh, verbose=verbose)
        return camh.results


class _InventoryAPIWrapper:
    def __init__(self, inv: CharacterInventory, contextName: str) -> None:
        self.inv: CharacterInventory = inv
        self.name: str = contextName

    def buildSearch(
        self,
        possible_ids: Optional[Iterable[str]] = None,
        possible_names: Optional[Iterable[str]] = None,
        is_sealed: Optional[bool] = None,
        enchantment_known: Optional[bool] = None,
    ) -> _InventorySearchWrapperAPI:
        return _InventorySearchWrapperAPI(
            self.inv,
            self.name,
            InventorySearcher(
                possible_ids=possible_ids or [],
                possible_names=possible_names or [],
                sealed=is_sealed,
                enchantment_known=enchantment_known,
            ),
        )

    def getAllMatchingSearch(
        self,
        possible_ids: Optional[Iterable[str]] = None,
        possible_names: Optional[Iterable[str]] = None,
        is_sealed: Optional[bool] = None,
        enchantment_known: Optional[bool] = None,
        verbose: bool = True,
    ) -> SearchResult:
        searchenglish = search_as_english(
            id=possible_ids,
            name=possible_names,
            is_sealed=is_sealed,
            enchantment_known=enchantment_known,
        )
        with click.info(
            f"Attempting to remove find with {searchenglish} from {self.name}..."
        ):
            return self.buildSearch(
                possible_ids=possible_ids,
                possible_names=possible_names,
                is_sealed=is_sealed,
                enchantment_known=enchantment_known,
            ).returnAll(verbose=verbose)

    def removeAllMatchingSearch(
        self,
        possible_ids: Optional[Iterable[str]] = None,
        possible_names: Optional[Iterable[str]] = None,
        is_sealed: Optional[bool] = None,
        enchantment_known: Optional[bool] = None,
        dry_run: bool = False,
        verbose: bool = True,
    ) -> int:
        searchenglish = search_as_english(
            id=possible_ids,
            name=possible_names,
            is_sealed=is_sealed,
            enchantment_known=enchantment_known,
        )
        with click.info(
            f"Attempting to remove items with {searchenglish} from {self.name}..."
        ):
            return self.buildSearch(
                possible_ids=possible_ids,
                possible_names=possible_names,
                is_sealed=is_sealed,
                enchantment_known=enchantment_known,
            ).removeAll(dry_run=dry_run, verbose=verbose)

    def setClothingStackSize(self, clothing: AbstractClothing, count: int) -> None:
        self.inv.clothingInInventory[clothing] = count

    def unequipClothing(self, slot: EInventorySlot) -> Optional[AbstractClothing]:
        dropped: Optional[AbstractClothing] = None
        for c in self.inv.clothingEquipped:
            if c.slotEquippedTo == slot.name:
                dropped = c
                break
        if dropped is not None:
            self.inv.clothingEquipped.remove(dropped)
            dropped.slotEquippedTo = None
            if dropped in self.inv.clothingInInventory.keys():
                self.inv.clothingInInventory[dropped] += 1
            else:
                self.inv.clothingInInventory[dropped] = 1
        return dropped

    def equipClothingTo(self, clothing: AbstractClothing, slot: EInventorySlot) -> None:
        self.unequipClothing(slot)
        clothing.slotEquippedTo = slot.name
        self.inv.clothingEquipped.append(clothing)

    def setItemStackSize(self, item: AbstractItem, count: int) -> None:
        self.inv.itemsInInventory[item] = count

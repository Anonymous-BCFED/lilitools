import random
from pathlib import Path
from typing import Union

from lxml import etree

from lilitools.cli.lilitool.commands.enchant.clothing.clothing_templates import TEMPLATE_CHOICES
from lilitools.saves.character.body.tattoos.tattoo import Tattoo
from lilitools.saves.enchantment_recipe import EnchantmentRecipe
from lilitools.saves.items.abstract_clothing import AbstractClothing
from lilitools.saves.items.abstract_item import AbstractItem
from lilitools.saves.items.abstract_weapon import AbstractWeapon
from lilitools.saves.items.clothing.clothing_types import CLOTHING_TYPES
from lilitools.saves.items.enums.known_clothing import EKnownClothing
from lilitools.saves.items.enums.known_items import EKnownItems
from lilitools.saves.modfiles.character.clothing.clothing_type import ClothingType


class LTItemFactory:
    @staticmethod
    def FromClothingTypeID(type_id: str) -> AbstractClothing:
        return LTItemFactory.FromKnownClothingType(CLOTHING_TYPES.get(type_id))

    @staticmethod
    def FromClothingTemplateID(template_id: str) -> AbstractClothing:
        return LTItemFactory.FromKnownClothingType(TEMPLATE_CHOICES[template_id])

    @staticmethod
    def FromKnownClothingType(
        clothingType: Union[EKnownClothing, ClothingType]
    ) -> AbstractClothing:
        if isinstance(clothingType, EKnownClothing):
            clothingType = clothingType.value
        assert clothingType is not None
        clothing = AbstractClothing()
        clothing.id = clothingType.id
        clothing.name = clothingType.coreAttributes.nameSingular
        clothing.colours = []
        for colorset in filter(
            lambda x: x is not None, clothingType.generatedColourPossibilities
        ):
            assert colorset is not None, repr(clothingType.generatedColourPossibilities)
            clothing.colours.append(random.choice(list(colorset)))
        clothing.enchantmentKnown = True
        clothing.isDirty = False
        clothing.effects = []
        return clothing

    @staticmethod
    def FromKnownItemType(eki: EKnownItems) -> AbstractItem:
        item = AbstractItem()
        item.id = eki.name
        item.name = eki.nameSingular
        if len(eki.colours) > 0:
            item.colour = eki.colours[0]  # This is how genItem works.
        item.itemEffects = [x.clone() for x in eki.effects]
        return item

    @staticmethod
    def FromEnchantmentRecipe(
        path: Path,
    ) -> Union[AbstractClothing, AbstractItem, Tattoo, AbstractWeapon]:
        tree = etree.parse(str(path), parser=etree.XMLParser(remove_comments=True))
        r = EnchantmentRecipe()
        r.fromXML(tree.getroot())
        thing: Union[AbstractClothing, AbstractItem, Tattoo, AbstractWeapon]
        if r.clothingType is not None:
            c = LTItemFactory.FromClothingTypeID(r.clothingType)
            c.effects = r.itemEffects
            thing = c
        elif r.itemType is not None:
            i = LTItemFactory.FromKnownItemType(EKnownItems[r.itemType])
            i.itemEffects = r.itemEffects
            thing = i
        elif r.tattooType is not None:
            t = Tattoo()
            t.id = r.tattooType
            t.effects = r.itemEffects
            thing = t
        elif r.weaponType is not None:
            w = AbstractWeapon()
            w.id = r.weaponType
            w.effects = r.itemEffects
            thing = w
        thing.name = r.name
        return thing

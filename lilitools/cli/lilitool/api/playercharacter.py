from pathlib import Path
from typing import Union

from lxml import etree

from lilitools.cli.lilitool.api.inventory import _InventoryAPIWrapper
from lilitools.logging import ClickWrap
from lilitools.saves.character.player_character import PlayerCharacter
from lilitools.saves.menders import mendCharacterXML
from lilitools.saves.upgraders.playercharacter import PlayerCharacterExportUpgrader

click = ClickWrap()


class _PlayerCharacterAPIWrapper:
    def __init__(self, internal: PlayerCharacter) -> None:
        self._wrapped: PlayerCharacter = internal

    def asPlayerCharacter(self) -> PlayerCharacter:
        return self._wrapped

    def getInventory(self) -> _InventoryAPIWrapper:
        return _InventoryAPIWrapper(self._wrapped.characterInventory, self._wrapped.core.id)


class LTPlayerCharacterFactory:
    @staticmethod
    def FromExport(filename: Union[str, Path]) -> PlayerCharacter:
        path: Path = Path(filename)
        if not path.is_file():
            click.critical('Unable to find specified export XML file.')
        doc = etree.parse(str(path), parser=etree.XMLParser(remove_comments=True))
        root = doc.getroot()
        PlayerCharacterExportUpgrader(root)
        mendCharacterXML(root)
        pc = PlayerCharacter()
        pc.fromXML(root)
        return pc

from pathlib import Path
from typing import Optional, Union

from lilitools.cli.lilitool.api.inventory import _InventoryAPIWrapper
from lilitools.cli.lilitool.api.playercharacter import _PlayerCharacterAPIWrapper
from lilitools.cli.lilitool.api.world import _CellAPIWrapper, _UniverseAPIWrapper
from lilitools.saves.character.npc import NPC
from lilitools.saves.character.player_character import PlayerCharacter
from lilitools.saves.game import Game
from lilitools.saves.utils import loadSaveFrom, saveToFile

__all__ = ['LTSaveFactory']


class _SaveAPIWrapper:
    def __init__(self, internal: Game) -> None:
        self._wrapped: Game = internal

    def getPlayer(self) -> _PlayerCharacterAPIWrapper:
        return _PlayerCharacterAPIWrapper(self._wrapped.playerCharacter)

    def getNPCByID(self, npcid: str) -> Optional[NPC]:
        return self._wrapped.getNPCById(npcid)

    def getUniverse(self) -> _UniverseAPIWrapper:
        return _UniverseAPIWrapper(self._wrapped.worlds)

    def asGame(self) -> Game:
        return self._wrapped

    def saveToFile(self, filename: Union[str, Path], show_warnings: bool = True, minify: bool = True, sort_attrs: bool = False, quiet: bool = False) -> None:
        #def saveToFile(game: Game, file: Union[str, Path], show_warnings: bool = False, minify: bool = False, sort_attrs: bool = False, quiet: bool = True) -> None:
        saveToFile(self._wrapped, file=filename, show_warnings=show_warnings, minify=minify, sort_attrs=sort_attrs, quiet=quiet)


class LTSaveFactory:
    @staticmethod
    def LoadSaveFromFile(path: Union[str, Path]) -> _SaveAPIWrapper:
        savepath = Path(path)
        assert savepath.is_file(), f'{savepath} does not exist as a file'
        return _SaveAPIWrapper(loadSaveFrom(savepath))

from pathlib import Path

from lilitools.logging import ClickWrap

click = ClickWrap()
CHECKMARK = click.style('✓', fg='green')


def assertFileExists(filepath: Path) -> None:
    if not filepath.is_file():
        click.critical(f'File {filepath} does not exist.')
    click.info(f'{CHECKMARK} File {filepath} exists.')

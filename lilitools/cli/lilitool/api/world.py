from typing import Generator, Optional

from lilitools.cli.lilitool.api.gamecharacter import _GameCharacterAPIWrapper
from lilitools.cli.lilitool.api.inventory import _InventoryAPIWrapper
from lilitools.saves.character.npc import NPC
from lilitools.saves.world.cell import Cell
from lilitools.saves.world.world import World
from lilitools.saves.world.world_controller import WorldController


class _CellAPIWrapper:
    def __init__(self, world: "_WorldAPIWrapper", cell: Cell) -> None:
        self._world: "_WorldAPIWrapper" = world
        self._wrapped: Cell = cell

    def getInventory(self) -> _InventoryAPIWrapper:
        return _InventoryAPIWrapper(
            self._wrapped.getOrInstantiateInventory(),
            f"{self._wrapped.getName()} inventory",
        )

    def asCell(self) -> Cell:
        return self._wrapped

    @property
    def name(self) -> str:
        return self._wrapped.getName()

    @name.setter
    def name(self, v: str) -> None:
        if self._wrapped.place is not None:
            self._wrapped.place.name = v

    @property
    def type(self) -> str:
        return self._wrapped.place.type

    @property
    def x(self) -> int:
        return self._wrapped.location.x

    @property
    def y(self) -> int:
        return self._wrapped.location.y

    @property
    def world(self) -> str:
        return self._wrapped.worldID

    @property
    def discovered(self) -> bool:
        return self._wrapped.discovered


class _WorldAPIWrapper:
    def __init__(self, w: World) -> None:
        self._world: World = w

    def asWorld(self) -> World:
        return self._world

    def clear(self) -> None:
        self._world.clear()

    def get(self, x: int, y: Optional[int] = None) -> _CellAPIWrapper:
        return _CellAPIWrapper(self, self._world.get(x, y))

    def has(self, x: int, y: Optional[int] = None) -> bool:
        return self._world.has(x, y)

    def set(
        self, x: int, y: Optional[int] = None, cell: Optional[_CellAPIWrapper] = None
    ) -> None:
        self._world.set(x, y, cell.asCell())

    def getAllCells(self) -> Generator[_CellAPIWrapper, None, None]:
        for c in self._world.cells:
            yield _CellAPIWrapper(self, c)

    @property
    def type(self) -> str:
        return self._world.type

    @property
    def height(self) -> int:
        return self._world.height

    @property
    def width(self) -> int:
        return self._world.width


class _UniverseAPIWrapper:
    def __init__(self, internal: WorldController) -> None:
        self._wrapped: WorldController = internal

    def getWorldController(self) -> WorldController:
        return self._wrapped

    def getAllWorlds(self) -> Generator[_WorldAPIWrapper, None, None]:
        for w in self._wrapped.worlds.values():
            yield _WorldAPIWrapper(w)

    def getCellByWorldCoords(self, worldid: str, x: int, y: int) -> _CellAPIWrapper:
        return _CellAPIWrapper(self, self._wrapped.getCellByWorldCoords(worldid, x, y))

    def getHomeCellOfNPC(self, npc: NPC) -> _CellAPIWrapper:
        pos = npc.locationInformation.homeLocation
        return _CellAPIWrapper(
            self,
            self._wrapped.getCellByWorldCoords(
                npc.locationInformation.homeWorldLocation, pos.x, pos.y
            ),
        )

    def getHomeCellOfCharacter(self, char: _GameCharacterAPIWrapper) -> _CellAPIWrapper:
        npc = char.asGameCharacter()
        if isinstance(npc, NPC):
            return self.getHomeCellOfNPC(char)

    def getCurrentCellOfNPC(self, npc: NPC) -> _CellAPIWrapper:
        pos = npc.locationInformation.location
        return _CellAPIWrapper(
            self,
            self._wrapped.getCellByWorldCoords(
                npc.locationInformation.worldLocation, pos.x, pos.y
            ),
        )

    def getCurrentCellOfCharacter(
        self, char: _GameCharacterAPIWrapper
    ) -> _CellAPIWrapper:
        npc = char.asGameCharacter()
        if isinstance(npc, NPC):
            return self.getCurrentCellOfNPC(char)

    def getPlayerRoom(self) -> _CellAPIWrapper:
        return _CellAPIWrapper(self, self._wrapped.getPlayerRoom())

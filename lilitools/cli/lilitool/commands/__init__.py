from argparse import _SubParsersAction

from lilitools.cli.lilitool.commands.backup import register_backup, register_restore
from lilitools.cli.lilitool.commands.config import register_config
from lilitools.cli.lilitool.commands.enchant import register_enchant
from lilitools.cli.lilitool.commands.heal import register_heal
from lilitools.cli.lilitool.commands.inventory import register_inventory
from lilitools.cli.lilitool.commands.modcheck import register_mod_check
from lilitools.cli.lilitool.commands.npc import register_npc
from lilitools.cli.lilitool.commands.player import register_player
from lilitools.cli.lilitool.commands.cache import register_cache
from lilitools.cli.lilitool.commands.pregnancy import register_pregnancy
from lilitools.cli.lilitool.commands.room import register_room
from lilitools.cli.lilitool.commands.save import register_save
from lilitools.cli.lilitool.commands.unseal import register_unseal


def register_cmds(subp: _SubParsersAction) -> None:
    register_backup(subp)
    register_cache(subp)
    register_config(subp)
    register_enchant(subp)
    register_heal(subp)
    register_inventory(subp)
    register_mod_check(subp)
    register_npc(subp)
    register_player(subp)
    register_pregnancy(subp)
    register_restore(subp)
    register_room(subp)
    register_save(subp)
    register_unseal(subp)

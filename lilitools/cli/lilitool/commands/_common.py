import argparse
from pathlib import Path

from lilitools.saves.game import Game
from lilitools.saves.utils import saveToFile

def register_save_options(argp: argparse.ArgumentParser) -> None:
    #argp.add_argument('outfile', type=Path, help='Path to save XML to')
    argp.add_argument('--minify', action='store_true', default=False)
    argp.add_argument('--no-sort-attrs', action='store_true', default=False)

def handle_save_options(game: Game, outfile: Path, args:argparse.Namespace) -> None:
    saveToFile(game, outfile, show_warnings=True, minify=args.minify, sort_attrs=not args.no_sort_attrs)

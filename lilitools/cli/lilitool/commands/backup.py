import argparse
import hashlib
import os
import shutil
import subprocess
from datetime import datetime
from enum import IntEnum
from pathlib import Path
from typing import Any, Dict, FrozenSet, List, Optional, Tuple

from lxml import etree
from ruamel.yaml import YAML

from lilitools.cli.config import LTConfiguration
from lilitools.cli.lilitool.dulwich import doesDulwichDetectAnyStagedChanges, getDulwichRepo, initDulwichRepo, installLFSToDulwich, printDulwichStagingStatus, setDulwichConfig
from lilitools.cli.utils import existingDirPath, existingFilePath
from lilitools.consts import VERSION
from lilitools.logging import ClickWrap
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.utils import loadSaveFrom, saveToFile, saveToYAML, writeRootToFile

LFS_TRACK_GLOBS: FrozenSet[str] = frozenset({})

click = ClickWrap()

yaml = YAML(typ="rt")


class EGitLibrary(IntEnum):
    DULWICH = 0
    GIT_CLI = 1


GIT_LIB_TO_USE = EGitLibrary.DULWICH
GIT: Path


def init_repo(cwd: Path) -> None:
    match GIT_LIB_TO_USE:
        case EGitLibrary.DULWICH:
            initDulwichRepo(cwd)
        case EGitLibrary.GIT_CLI:
            subprocess.run([str(GIT), "init", "-q"], cwd=cwd, check=True)


def reset_hard(cwd: Path) -> None:
    match GIT_LIB_TO_USE:
        case EGitLibrary.DULWICH:
            getDulwichRepo(cwd).reset_index()
        case EGitLibrary.GIT_CLI:
            subprocess.run([str(GIT), "reset", "--hard"], cwd=cwd, check=True)


def git_lfs_install(cwd: Path) -> None:
    match GIT_LIB_TO_USE:
        case EGitLibrary.DULWICH:
            installLFSToDulwich(getDulwichRepo(cwd))
        case EGitLibrary.GIT_CLI:
            subprocess.run([str(GIT), "lfs", "install"], cwd=cwd, check=True)


def git_add(cwd: Path, paths: List[Path | str], verbose: bool = False) -> None:
    cwd = cwd.absolute()
    apaths = [str(Path(x).absolute()) for x in paths]
    rpaths = [str(Path(x).absolute().relative_to(cwd)) for x in paths]
    match GIT_LIB_TO_USE:
        case EGitLibrary.DULWICH:
            from dulwich import porcelain

            if verbose:
                with click.info(f"[git] Adding files:"):
                    [click.info(p) for p in rpaths]
            porcelain.add(repo=str(cwd), paths=apaths)
        case EGitLibrary.GIT_CLI:
            subprocess.run([str(GIT), "add"] + rpaths, cwd=cwd, check=True)


def git_rm(cwd: Path, paths: List[Path | str]) -> None:
    cwd = cwd.absolute()
    apaths = [str(Path(x).absolute()) for x in paths]
    rpaths = [str(Path(x).absolute().relative_to(cwd)) for x in paths]
    match GIT_LIB_TO_USE:
        case EGitLibrary.DULWICH:
            from dulwich import porcelain

            with click.info(f"[git] Removing files:"):
                [click.info(p) for p in rpaths]
            porcelain.rm(repo=str(cwd), paths=apaths)
        case EGitLibrary.GIT_CLI:
            subprocess.run([str(GIT), "rm", "-f"] + rpaths, cwd=cwd, check=True)


def git_clean_fd(cwd: Path) -> None:
    match GIT_LIB_TO_USE:
        case EGitLibrary.DULWICH:
            from dulwich import porcelain

            click.info(f"[git] Cleaning...")
            porcelain.clean(repo=str(cwd), target_dir=str(cwd))
        case EGitLibrary.GIT_CLI:
            subprocess.run([str(GIT), "clean", "-fdx"], cwd=cwd, check=True)


def git_commit(cwd: Path, message: str) -> None:
    match GIT_LIB_TO_USE:
        case EGitLibrary.DULWICH:
            from dulwich import porcelain

            repo = getDulwichRepo(cwd)
            printDulwichStagingStatus(repo)
            if doesDulwichDetectAnyStagedChanges(repo):
                with click.info(f"[git] Committing with message {message!r}..."):
                    porcelain.commit(repo=str(cwd), message=message)
                    click.success("Done!")
            else:
                click.info(f"[git] No changes.")
        case EGitLibrary.GIT_CLI:
            subprocess.run(
                [str(GIT), "commit", "-a", "-m", message],
                cwd=cwd,
                check=True,
            )


def git_checkout_blob(cwd: Path, blob: str) -> None:
    match GIT_LIB_TO_USE:
        case EGitLibrary.DULWICH:
            from dulwich import index, porcelain

            repo = getDulwichRepo(cwd)
            refs, sha = repo.refs.follow(blob.encode())
            with click.info(f"[git] Resetting index to {sha}..."):
                repo.reset_index(repo[sha].tree)
                click.success("Done!")

        case EGitLibrary.GIT_CLI:
            subprocess.run([str(GIT), "checkout", blob], cwd=cwd, check=True)


def git_set_config(cwd: Path, key: str, value: str) -> None:
    match GIT_LIB_TO_USE:
        case EGitLibrary.DULWICH:
            setDulwichConfig(getDulwichRepo(cwd), key.encode(), value.encode())
        case EGitLibrary.GIT_CLI:
            subprocess.run(
                [str(GIT), "config", "--local", key, value], cwd=cwd, check=True
            )


def register_backup(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser("backup")

    p.add_argument("--data-dir", type=existingDirPath, default=None)
    p.add_argument("--backup-dir", "--backup-to", type=Path, default=None)
    p.add_argument("--git-path", type=existingFilePath, default=None)
    p.add_argument(
        "--git-lib",
        choices=["dulwich", "git-cli"],
        default=GIT_LIB_TO_USE.name.lower().replace("_", "-"),
        help="Which git library to use. dulwich is a pure-python library implementing the git protocol, git-cli is the actual git command line interface.",
    )

    p.add_argument(
        "--message",
        "-m",
        type=str,
        default=datetime.now().strftime("%A, %B %d, %Y %I:%M:%S %p"),
    )

    p.set_defaults(cmd=cmd_backup)


def remove_empty_dirs(dirname: str) -> int:
    if ".git" in dirname:
        return 0
    removed = 1
    dirs = 0
    files = 0
    while removed > 0:
        dirs = 0
        files = 0
        removed = 0
        entries = os.listdir(dirname)
        for entryname in entries:
            entry = os.path.join(dirname, entryname)
            if os.path.isdir(entry):
                removed += remove_empty_dirs(entry)
                dirs += 1
            elif os.path.isfile(entry):
                files += 1
    if dirs + files == 0:
        # click.echo(f'rmdir {dirname} (empty)')
        os.rmdir(dirname)
        return 1
    return removed


class EFileFlags(IntEnum):
    NONE = 0
    OLD_OR_CORRUPT = 1


class FileInfo:
    def __init__(self, data: Optional[dict] = None) -> None:
        self.hash: bytes = b""
        self.flags: EFileFlags = EFileFlags.NONE
        if data is not None:
            self.hash = data["hash"]
            self.flags = EFileFlags(data["flags"])

    def serialize(self) -> dict:
        return {"hash": self.hash, "flags": self.flags.value}


class BackupMetadata:
    VERSION: int = 4

    def __init__(self, backup_dir: Path) -> None:
        self.backup_dir: Path = backup_dir.absolute()
        self.data_dir: Path = None
        self.sort_attrs: bool = True
        self.indent: str = " " * 2
        self.files: Dict[Path, FileInfo] = {}
        self.source_files: Dict[Path, FileInfo] = {}
        self.git_config: Dict[str, str] = {
            "user.name": "nobody",
            "user.email": "nobody@no.pe",
            "user.signingkey": "",
        }

    def deserialize(self, cfgdata: List[Any]) -> None:
        header: Dict[str, Any]
        data: Dict[str, Any]
        header, data = list(cfgdata)
        match header["version"]:
            case 1:
                header, data = self.upgrade_1_to_2(header, data)
            case 2:
                header, data = self.upgrade_2_to_3(header, data)
            case 3:
                header, data = self.upgrade_3_to_4(header, data)
        assert self.VERSION == header["version"]
        self.data_dir = Path(data["data-dir"])
        self.sort_attrs = data["sort-attrs"]
        self.indent = data["indent"]
        self.git_config = data["git"]
        if header["lilitools"] == VERSION:
            self.source_files = {
                (self.backup_dir / Path(k)): FileInfo(v)
                for k, v in data["source-files"].items()
            }
            self.files = {
                (self.backup_dir / Path(k)): FileInfo(v)
                for k, v in data["files"].items()
            }

    def upgrade_1_to_2(self, h: dict, b: dict) -> Tuple[dict, dict]:
        click.secho(f"Upgrade metadata v1 to v2:", nl=False)
        h["lilitools"] = VERSION
        b["files"] = {}
        h["version"] = 2
        click.secho(click.style(" DONE!", fg="green"))
        return h, b

    def upgrade_2_to_3(self, h: dict, b: dict) -> Tuple[dict, dict]:
        click.secho(f"Upgrade metadata v2 to v3:", nl=False)
        h["lilitools"] = VERSION
        h["version"] = 3
        b["files"] = {k: {"hash": v, "flags": 0} for k, v in b["files"].items()}
        click.secho(click.style(" DONE!", fg="green"))
        return h, b

    def upgrade_3_to_4(self, h: dict, b: dict) -> Tuple[dict, dict]:
        click.secho(f"Upgrade metadata v3 to v4:", nl=False)
        h["lilitools"] = VERSION
        h["version"] = 4
        b["source-files"] = {}
        click.secho(click.style(" DONE!", fg="green"))
        return h, b

    def serialize(self) -> List[Any]:
        return [
            {"version": self.VERSION, "lilitools": VERSION},
            {
                "data-dir": str(self.data_dir.absolute()),
                "sort-attrs": self.sort_attrs,
                "indent": self.indent,
                "git": self.git_config,
                "files": {
                    str(k.relative_to(self.backup_dir)): v.serialize()
                    for k, v in self.files.items()
                },
                "source-files": {
                    str(k.absolute()): v.serialize()
                    for k, v in self.source_files.items()
                },
            },
        ]

    def hash(self, file: Path) -> Optional[bytes]:
        if file.is_file():
            with file.open("rb") as f:
                return hashlib.file_digest(f, "md5").digest()
        return None

    def registerSourceFile(
        self, file: Path, flags: EFileFlags = EFileFlags.NONE
    ) -> None:
        fi = FileInfo()
        fi.hash = self.hash(file)
        fi.flags = flags
        self.source_files[file.absolute()] = fi
        self.save()

    def registerFile(self, file: Path, flags: EFileFlags = EFileFlags.NONE) -> None:
        fi = FileInfo()
        fi.hash = self.hash(file)
        fi.flags = flags
        self.files[file.absolute()] = fi
        self.save()

    def hasSourceFileChanged(self, file: Path) -> bool:
        return (
            self.hash(file) != self.source_files.get(file.absolute(), FileInfo()).hash
        )

    def didSourceFileHaveFlag(self, file: Path, flag: EFileFlags) -> bool:
        return (self.source_files.get(file.absolute(), FileInfo()).flags & flag) == flag

    def hasFileChanged(self, file: Path) -> bool:
        return self.hash(file) != self.files.get(file.absolute(), FileInfo()).hash

    def save(self) -> None:
        tmpfile = self.backup_dir / "metadata.yml.tmp"
        ymlfile = self.backup_dir / "metadata.yml"
        with open(tmpfile, "w") as f:
            yaml.dump_all(self.serialize(), f)
        os.rename(tmpfile, ymlfile)


def writeGitIgnore(basepath: Path) -> None:
    with open(basepath / ".gitignore", "w") as f:
        f.write("# This file is overwritten.  Do not edit.\n")
        f.write("*.bak\n")
        f.write("*.tmp\n")
        f.write("*~\n")
        f.write("/metadata.yml\n")


def cmd_backup(args: argparse.Namespace) -> None:
    # global GIT_LIB_TO_USE, GIT
    # GIT_LIB_TO_USE = EGitLibrary[args.git_lib.replace("-", "_").upper()]
    # if GIT_LIB_TO_USE == EGitLibrary.GIT_CLI:
    #     GIT = args.git_path or shutil.which("git")
    #     assert (
    #         GIT is not None
    #     ), "The backup subcommand requires git and git-lfs to be installed and available in $PATH."
    config = LTConfiguration.GetInstance()

    backup_dir = args.backup_dir or config.backup_dir

    # First, ensure it exists.
    backup_dir.mkdir(parents=True, exist_ok=True)

    backup_yml = backup_dir / "metadata.yml"

    meta = BackupMetadata(backup_dir)

    # print(args.data_dir, meta.data_dir, config.lt_data_dir)
    meta.data_dir = args.data_dir or meta.data_dir or config.lt_data_dir

    assert (
        meta.data_dir is not None
    ), "Please set --data-dir or update your configuration (`lilitool config setup`)."

    if config.lt_data_dir is None:
        config.lt_data_dir = meta.data_dir.resolve()
        config.save()

    if not backup_yml.is_file():
        init_repo(backup_dir)
        meta.save()
        meta.registerFile(backup_yml)
        for k, v in meta.git_config.items():
            git_set_config(backup_dir, k, v)
        git_lfs_install(backup_dir)
        writeGitIgnore(backup_dir)
        git_add(backup_dir, paths=[backup_dir])
        git_commit(backup_dir, "Initial commit")
    else:
        for k, v in meta.git_config.items():
            git_set_config(backup_dir, k, v)
        if not (backup_dir / ".git" / "lfs").is_dir():
            git_lfs_install(backup_dir)
        with open(backup_yml, "r") as f:
            meta.deserialize(yaml.load_all(f))
    assert meta.data_dir is not None, "You need to temporarily specify --data-dir."
    writeGitIgnore(backup_dir)
    meta.files.clear()
    meta.registerFile(backup_yml)
    meta.registerFile(backup_dir / ".gitignore")
    with click.echo("Saves:"):
        for savefile in (meta.data_dir / "saves").glob("*.xml"):
            file_has_changed = meta.hasSourceFileChanged(savefile)
            file_was_corrupt = meta.didSourceFileHaveFlag(
                savefile, EFileFlags.OLD_OR_CORRUPT
            )
            newpath = backup_dir / savefile.relative_to(meta.data_dir)
            newpath.parent.mkdir(parents=True, exist_ok=True)
            cleanxml = newpath.with_suffix(".clean.xml")
            cleanxml_is_file = cleanxml.is_file()
            decodedyml = newpath.with_suffix(".decoded.yml")
            decodedyml_is_file = decodedyml.is_file()
            rawxml = newpath.with_suffix(".raw.xml")
            rawxml_is_file = rawxml.is_file()
            # with click.secho(f"{savefile}:"):
            #     click.secho(f"{file_has_changed=}")
            #     click.secho(f"{file_was_corrupt=}")
            #     click.secho(f"{cleanxml_is_file=}")
            #     click.secho(f"{decodedyml_is_file=}")
            #     click.secho(f"{rawxml_is_file=}")
            if not file_has_changed and (
                not file_was_corrupt
                and cleanxml_is_file
                and decodedyml_is_file
                and rawxml_is_file
            ):
                click.secho(
                    f"Skipping {savefile.relative_to(meta.data_dir.parent)} (no changes)"
                )
                meta.registerFile(cleanxml)
                meta.registerFile(decodedyml)
                meta.registerFile(rawxml)
                meta.registerSourceFile(savefile)
                continue
            flags = EFileFlags.NONE
            meta.registerSourceFile(savefile)
            reasons = []
            if file_has_changed:
                reasons.append(
                    f"{savefile.relative_to(meta.data_dir.parent)} has changed"
                )
            else:
                if not cleanxml_is_file:
                    reasons.append(f"{cleanxml} is missing")
                if not decodedyml_is_file:
                    reasons.append(f"{decodedyml} is missing")
                if not rawxml_is_file:
                    reasons.append(f"{rawxml} is missing")

            reasoning = ", ".join(reasons)
            with click.echo(
                f"Loading {savefile.relative_to(meta.data_dir.parent)}... ({reasoning}) "
            ):
                try:
                    g = loadSaveFrom(savefile, quiet=True)
                except (
                    ElementRequiredException,
                    AttributeRequiredException,
                    KeyRequiredException,
                ) as sererr:
                    with click.secho("FAILED", fg="red"):
                        click.secho(sererr, fg="red")
                    continue
                try:
                    newpathxml = newpath.with_suffix(".clean.xml")
                    click.echo(
                        f"  -> {newpathxml.relative_to(backup_dir.parent)}... ",
                        nl=False,
                    )
                    saveToFile(
                        g,
                        newpathxml,
                        minify=False,
                        sort_attrs=meta.sort_attrs,
                        quiet=True,
                    )
                    click.secho("OK", fg="green", indent=False)
                    meta.registerFile(newpathxml)
                except NotImplementedError as nee:
                    with click.secho("FAILED", fg="red", indent=False):
                        click.secho(nee, fg="red")
                    flags = EFileFlags.OLD_OR_CORRUPT
                    raise nee
                except (
                    ElementRequiredException,
                    AttributeRequiredException,
                    KeyRequiredException,
                ) as sererr:
                    with click.secho("FAILED", fg="red", indent=False):
                        click.secho(sererr, fg="red")
                    continue
                except Exception as e:
                    with click.secho("FAILED", fg="red", indent=False):
                        click.secho(e, fg="red")
                        # traceback.print_exc()
                        # print(e)
                    raise e

                try:
                    newpathyml = newpath.with_suffix(".decoded.yml")
                    click.echo(
                        f"  -> {newpathyml.relative_to(backup_dir.parent)}... ",
                        nl=False,
                    )
                    saveToYAML(
                        g,
                        newpathyml,
                        minify=False,
                        sort_attrs=meta.sort_attrs,
                        quiet=True,
                    )
                    click.secho("OK", fg="green", indent=False)
                    meta.registerFile(newpathyml)
                except NotImplementedError as nee:
                    with click.secho("FAILED", fg="red", indent=False):
                        click.secho(nee, fg="red")
                    raise nee

                except Exception as e:
                    with click.secho("FAILED", fg="red", indent=False):
                        click.secho(e, fg="red")
                        # traceback.print_exc()
                        # print(e)
                    raise e
                try:
                    rawxml = newpath.with_suffix(".raw.xml")
                    click.echo(
                        f"  -> {rawxml.relative_to(backup_dir.parent)}... ",
                        nl=False,
                    )
                    shutil.copy(savefile, rawxml)
                    click.secho("OK", fg="green", indent=False)
                    meta.registerFile(rawxml)
                except Exception as e:
                    with click.secho("FAILED", fg="red", indent=False):
                        click.secho(e, fg="red")
                        # traceback.print_exc()
                        # print(e)
                    raise e
    mfk = meta.files.keys() | meta.source_files.keys()
    with click.echo("Misc:"):
        for datafile in meta.data_dir.rglob("*.xml"):
            newpath = backup_dir / datafile.relative_to(meta.data_dir)
            newpath.parent.mkdir(parents=True, exist_ok=True)
            if newpath.absolute() in mfk:
                # print('skip '+str(newpath))
                continue
            try:
                click.echo(
                    f"{datafile.relative_to(meta.data_dir.parent)}... ", nl=False
                )
                d = etree.parse(datafile, parser=etree.XMLParser(remove_comments=True))
                writeRootToFile(
                    newpath,
                    d.getroot(),
                    minify=False,
                    sort_attrs=meta.sort_attrs,
                    quiet=True,
                )
                click.secho("OK", fg="green")
                meta.registerFile(newpath)
            except Exception as e:
                click.secho("FAILED", fg="red")
                click.error(str(e))
        mfk = meta.files.keys() | meta.source_files.keys()
        for repofile in set(backup_dir.rglob("*.xml")) | set(backup_dir.rglob("*.yml")):
            if repofile.absolute() not in mfk:
                # subprocess.run(
                #     [
                #         str(GIT),
                #         "rm",
                #         "-f",
                #         str(repofile.absolute().relative_to(BACKUP_DIR.absolute())),
                #     ],
                #     cwd=BACKUP_DIR,
                #     check=True,
                # )
                git_rm(backup_dir, [repofile])
    remove_empty_dirs(str(backup_dir))
    git_add(
        backup_dir,
        [
            backup_dir / ".gitignore",
        ]
        + list(backup_dir.absolute().rglob("*.xml"))
        + list(backup_dir.absolute().rglob("*.yml")),
    )
    git_commit(backup_dir, args.message)
    meta.save()


def register_restore(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser("restore")

    p.add_argument("--data-dir", type=existingDirPath, default=None)
    p.add_argument("--backup-dir", "--backup-to", type=Path, default=Path("backup"))
    p.add_argument("--git-path", type=existingFilePath, default=None)

    p.add_argument("--commit", type=str, default="HEAD")
    p.add_argument("files", nargs="*", type=Path, help="Which files to restore")

    p.set_defaults(cmd=cmd_restore)


def cmd_restore(args: argparse.Namespace) -> None:
    config = LTConfiguration.GetInstance()
    backup_dir = args.backup_dir or config.backup_dir

    # First, ensure it exists.
    backup_dir.mkdir(parents=True, exist_ok=True)

    backup_yml = backup_dir / "metadata.yml"

    meta = BackupMetadata(backup_dir)
    if args.data_dir is not None:
        meta.data_dir = Path(args.data_dir).absolute()

    click.info("Resetting backup workspace...")
    # subprocess.run([str(GIT), "reset", "--hard"], cwd=BACKUP_DIR, check=True)
    reset_hard(backup_dir)
    click.info("Cleaning backup workspace...")
    git_clean_fd(backup_dir)
    click.info(f"Checking out commit blob {args.commit}...")
    # subprocess.run([str(GIT), "checkout", args.commit], cwd=BACKUP_DIR, check=True)
    git_checkout_blob(backup_dir, args.commit)
    with click.info("Copying..."):
        for filestr in args.files:
            file = Path(filestr).absolute()
            realpath = meta.data_dir / file.relative_to(meta.data_dir)
            bkppath = (backup_dir / file.relative_to(meta.data_dir)).with_suffix(
                ".raw.xml"
            )

            click.info(
                f"{bkppath.relative_to(Path.cwd())} -> {realpath.relative_to(Path.cwd())}"
            )
            shutil.copy(bkppath, realpath)

__all__ = ["register_cache"]
import argparse
import os
import time
from pathlib import Path
from typing import List

import human_readable
import tabulate

from lilitools.cli.config import LTConfiguration
from lilitools.logging import ClickWrap
from lilitools.saves.items.lazy_loaders import ALL_LAZY_LOADERS

click = ClickWrap()


def register_cache(subp: argparse._SubParsersAction) -> None:
    p: argparse._SubParsersAction = subp.add_parser(
        "cache", help="Manipulate lilitool's metadata cache"
    ).add_subparsers()
    register_cache_clear(p)
    register_cache_rebuild(p)
    register_cache_show(p)
    # register_cache_show_format(p)


def register_cache_clear(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser(
        "clear",
        description="Clear cached metadata.",
    )

    p.set_defaults(cmd=cmd_cache_clear)


def cmd_cache_clear(args: argparse.Namespace) -> None:
    with click.info("Clearing cache data..."):
        for l in ALL_LAZY_LOADERS:
            with click.info(f"Clearing {l.CACHE_DATA_TYPE_DESC}..."):
                l.clearCache(verbose=True)


def register_cache_rebuild(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser(
        "rebuild",
        help="Re-build cached metadata.",
    )

    p.set_defaults(cmd=cmd_cache_rebuild)


def cmd_cache_rebuild(args: argparse.Namespace) -> None:
    with click.info("Clearing cache data..."):
        for l in ALL_LAZY_LOADERS:
            with click.info(f"Clearing {l.CACHE_DATA_TYPE_DESC}..."):
                l.clearCache(verbose=True)
    with click.info("Rebuilding cache data..."):
        for l in ALL_LAZY_LOADERS:
            with click.info(f"Rebuilding {l.CACHE_DATA_TYPE_DESC}..."):
                t = time.time()
                l.rebuildCache()
                click.success(f"Completed in {time.time()-t}s.")


# def register_cache_show_format(subp: argparse._SubParsersAction) -> None:
#     p: argparse.ArgumentParser = subp.add_parser(
#         "show-format",
#         help="Dump cache structure.",
#     )

#     p.set_defaults(cmd=cmd_cache_show_format)


# def cmd_cache_show_format(args: argparse.Namespace) -> None:
#     for l in ALL_LAZY_LOADERS:
#         print(f'>>> {l.cache_file.name}:')
#         print(repr(l.CACHE_STRUCTURE.export_ksy()))


def register_cache_show(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser(
        "show",
        help="Show location and size of cached metadata.",
    )

    p.set_defaults(cmd=cmd_cache_show)


def cmd_cache_show(args: argparse.Namespace) -> None:
    cfg=LTConfiguration.GetInstance()
    rows: List[List[str]] = []
    headers = ["Data Type", "Size", "Location"]
    colalign = ["right", "left", "left"]
    for l in ALL_LAZY_LOADERS:
        if l.cache_file.is_file():
            cachefile_size = human_readable.file_size(os.path.getsize(l.cache_file))
            rows.append(
                [
                    l.CACHE_DATA_TYPE_DESC,
                    cachefile_size,
                    str(l.cache_file),
                ]
            )
        else:
            rows.append(
                [
                    l.CACHE_DATA_TYPE_DESC,
                    "Not Present",
                    str(l.cache_file),
                ]
            )
    print(
        tabulate.tabulate(
            headers=headers,
            tabular_data=rows,
            tablefmt="simple_outline",
            colalign=colalign,
        )
    )

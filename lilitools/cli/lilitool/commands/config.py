__all__ = ["register_cache"]
import argparse
import json
from pathlib import Path
from typing import Optional

import tabulate
from rich import get_console
from rich.prompt import Confirm, Prompt

from lilitools.cli.config import EGitLibrary, EUnitPresentationSystem, LTConfiguration
from lilitools.cli.utils import existingDirPath, existingFilePath
from lilitools.logging import ClickWrap

click = ClickWrap()


def register_config(subp: argparse._SubParsersAction) -> None:
    p: argparse._SubParsersAction = subp.add_parser(
        "config", help="Manipulate lilitool's configuration"
    ).add_subparsers()
    register_config_paths(p)
    register_config_get(p)
    register_config_set(p)
    register_config_setup(p)
    # register_config_show(p)


def register_config_paths(subp: argparse._SubParsersAction) -> None:
    p = subp.add_parser("paths", description="Dump all known paths")
    p.set_defaults(cmd=cmd_config_paths)


def cmd_config_paths(args: argparse.Namespace) -> None:
    cfg = LTConfiguration.GetInstance()
    rows = []
    rows.append(["backup_dir", str(cfg.backup_dir.absolute()) if cfg.backup_dir is not None else None])
    rows.append(["cache_dir", str(cfg.cache_dir.absolute()) if cfg.cache_dir is not None else None])
    rows.append(["config_dir", str(cfg.config_dir.absolute()) if cfg.config_dir is not None else None])
    rows.append(["config_file", str(cfg.config_file.absolute()) if cfg.config_file is not None else None])
    rows.append(["git_executable", str(cfg.git_executable.absolute()) if cfg.git_executable is not None else None])
    rows.append(["local_dir", str(cfg.local_dir.absolute()) if cfg.local_dir is not None else None])
    rows.append(["lt_data_dir", str(cfg.lt_data_dir.absolute()) if cfg.lt_data_dir is not None else None])
    print(
        tabulate.tabulate(
            tabular_data=rows,
            tablefmt="simple_outline",
        )
    )


def register_config_get(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser(
        "get",
        description="Get one or more configuration entries.",
    )
    p.add_argument("--format", choices=["text", "json"], default="text")

    p.add_argument("--all", action="store_true", default=False)

    g = p.add_argument_group("paths")
    g.add_argument("--backup-dir", action="store_true", default=False)
    g.add_argument("--cache-dir", action="store_true", default=False)
    g.add_argument("--config-dir", action="store_true", default=False)
    g.add_argument("--config-file", action="store_true", default=False)
    g.add_argument("--git-executable", action="store_true", default=False)
    g.add_argument("--local-dir", action="store_true", default=False)
    g.add_argument("--lt-data-dir", action="store_true", default=False)

    g = p.add_argument_group("enums")
    g.add_argument(
        "--git-library", "--git-lib", action="store_true", default=False
    )
    g.add_argument("--unit-system", action="store_true", default=False)

    p.set_defaults(cmd=cmd_config_get)


def cmd_config_get(args: argparse.Namespace) -> None:
    cfg = LTConfiguration.GetInstance()
    data = {}
    if args.all or args.backup_dir:
        data["backup_dir"] = (
            str(cfg.backup_dir.absolute()) if cfg.backup_dir is not None else None
        )
    if args.all or args.cache_dir:
        data["cache_dir"] = (
            str(cfg.cache_dir.absolute()) if cfg.cache_dir is not None else None
        )
    if args.all or args.config_dir:
        data["config_dir"] = (
            str(cfg.config_dir.absolute()) if cfg.config_dir is not None else None
        )
    if args.all or args.config_file:
        data["config_file"] = (
            str(cfg.config_file.absolute()) if cfg.config_file is not None else None
        )
    if args.all or args.git_executable:
        data["git_executable"] = (
            str(cfg.git_executable.absolute())
            if cfg.git_executable is not None
            else None
        )
    if args.all or args.local_dir:
        data["local_dir"] = (
            str(cfg.local_dir.absolute()) if cfg.local_dir is not None else None
        )
    if args.all or args.lt_data_dir:
        data["lt_data_dir"] = (
            str(cfg.lt_data_dir.absolute()) if cfg.lt_data_dir is not None else None
        )

    if args.all or args.git_library:
        data["git_library"] = cfg.git_library.name
    if args.all or args.unit_system:
        data["unit_system"] = cfg.unit_system.name

    match args.format:
        case "text":
            rows = []
            for k, v in data.items():
                rows.append([k, v])
            print(tabulate.tabulate(rows, tablefmt="simple_outline"))
        case "json":
            print(json.dumps(data))


def register_config_set(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser(
        "set",
        description="Get one or more configuration entries.",
    )

    g = p.add_argument_group("paths")
    g.add_argument("--backup-dir", type=existingDirPath, default=None)
    g.add_argument("--git-executable", type=existingFilePath, default=None)
    g.add_argument("--lt-data-dir", type=existingDirPath, default=None)

    g = p.add_argument_group("enums")
    g.add_argument(
        "--git-library",
        "--git-lib",
        choices=[x.name for x in EGitLibrary],
        default=None,
    )
    g.add_argument(
        "--unit-system", choices=[x.name for x in EUnitPresentationSystem], default=None
    )

    p.set_defaults(cmd=cmd_config_set)


def cmd_config_set(args: argparse.Namespace) -> None:
    backup_dir: Optional[Path] = args.backup_dir
    git_executable: Optional[Path] = args.git_executable
    lt_data_dir: Optional[Path] = args.lt_data_dir

    git_library: Optional[str] = args.git_library
    unit_system: Optional[str] = args.unit_system
    console = get_console()
    dirty: bool = False
    cfg = LTConfiguration.GetInstance()
    if backup_dir is not None:
        console.print(f"paths.backup_dir: {cfg.backup_dir} -> {backup_dir}")
        cfg.backup_dir = backup_dir
        dirty = True
    if git_executable is not None:
        console.print(f"paths.git_executable: {cfg.git_executable} -> {git_executable}")
        cfg.git_executable = git_executable
        dirty = True
    if lt_data_dir is not None:
        console.print(f"paths.lt_data_dir: {cfg.lt_data_dir} -> {lt_data_dir}")
        cfg.lt_data_dir = lt_data_dir
        dirty = True

    if git_library is not None:
        nv = EGitLibrary[git_library.upper()]
        console.print(f"settings.git_library: {cfg.git_library.name} -> {nv.name}")
        cfg.git_library = nv
        dirty = True
    if unit_system is not None:
        nv = EUnitPresentationSystem[unit_system.upper()]
        console.print(f"settings.unit_system: {cfg.unit_system.name} -> {nv.name}")
        cfg.unit_system = nv
        dirty = True
    if dirty:
        cfg.save()

def register_config_setup(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser(
        "setup",
        description="Complete configuration of lilitools.",
    )

    p.set_defaults(cmd=cmd_config_setup)


def cmd_config_setup(args: argparse.Namespace) -> None:
    console=get_console()
    if not Confirm.ask('[bold red]This will overwrite your existing configuration.[/] Are you sure you wish to proceed?'):
        return
    cfg = LTConfiguration()
    lt_dir:Optional[str]=None
    while lt_dir is None or not Path(lt_dir).is_file():
        lt_dir=Prompt.ask('Which directory is Liliths Throne in?')
        lt_dir_path=Path(lt_dir)
        if lt_dir_path.is_file():
            console.print(f'[red]{lt_dir_path} is a file, not a directory.[/]')
            continue
        if not lt_dir_path.is_dir():
            console.print(f'[red]{lt_dir_path} does not exist.[/] You need to tell lilitool where LT is stored so it can pull data from it.')
            continue
        if not (lt_dir_path/'res').is_dir():
            console.print(f'[red]{lt_dir_path/'res'} does not exist.[/] You need to tell lilitool where LT is stored so it can pull data from it.')
            continue
        cfg.lt_root_dir = lt_dir_path
        break
    # git_executable = shutil.which('git')
    # if git_executable is not None:
    #     git_executable=str(git_executable)
    # while git_executable is not None and Path(git_executable).is_file():
    #     git_executable=Prompt.ask('Where is your git executable?', default=git_executable)
    #     git_executable_path=Path(git_executable)
    #     if git_executable_path.is_dir():
    #         console.print(f'[red]{git_executable_path} is a directory, not a file.[/]')
    #         continue
    #     if not git_executable_path.is_file():
    #         console.print(f'[red]{git_executable_path} does not exist.[/] You need git for backups.')
    #         continue
    backup_dir:Optional[str]=None
    while backup_dir is None or not Path(backup_dir).is_file():
        backup_dir=Prompt.ask('Where do you want to store backups?', default=str(cfg.backup_dir.absolute()))
        backup_dir_path=Path(backup_dir)
        if backup_dir_path.is_file():
            console.print(f'[red]{backup_dir_path} is a file, not a directory.[/]')
            continue
        cfg.backup_dir = backup_dir_path
        break
    unitSystem=Prompt.ask('What unit system would you like? (UK_IMPERIAL is the imperial most of the game uses)', choices=[x.name for x in EUnitPresentationSystem], default=EUnitPresentationSystem.DETECT.name)
    cfg.unit_system=EUnitPresentationSystem[unitSystem]   

    cfg.save()

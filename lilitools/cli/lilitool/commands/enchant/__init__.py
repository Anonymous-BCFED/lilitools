import argparse

from lilitools.cli.lilitool.commands.enchant.clothing import _register_clothing
from lilitools.cli.lilitool.commands.enchant.potion import _register_potion


def register_enchant(subp: argparse._SubParsersAction) -> None:
    enchp: argparse._SubParsersAction = subp.add_parser('enchant').add_subparsers()
    _register_clothing(enchp)
    _register_potion(enchp)

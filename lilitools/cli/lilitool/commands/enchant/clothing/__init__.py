import argparse
import random
import sys
from typing import Optional

import click

from lilitools.cli.lilitool.commands.enchant.clothing.args.ass import handle_ass, register_ass
from lilitools.cli.lilitool.commands.enchant.clothing.args.breasts import handle_breasts, register_breasts
from lilitools.cli.lilitool.commands.enchant.clothing.args.core import handle_core, register_core
from lilitools.cli.lilitool.commands.enchant.clothing.args.face import register_face
from lilitools.cli.lilitool.commands.enchant.clothing.args.output import handle_output, register_output_args
from lilitools.cli.lilitool.commands.enchant.clothing.args.penis import handle_penis, register_penis
from lilitools.cli.lilitool.commands.enchant.clothing.args.standard import getCharFrom, register_standard_args
from lilitools.cli.lilitool.commands.enchant.clothing.args.vagina import handle_vagina, register_vagina
from lilitools.cli.lilitool.commands.enchant.clothing.clothing_templates import TEMPLATE_CHOICES
from lilitools.cli.lilitool.commands.enchant.common import getClothing
from lilitools.consts import MAX_CLOTHING_COLOUR_LEN
from lilitools.game.effects.enchanter import Enchanter
from lilitools.saves.items.abstract_clothing import AbstractClothing
from lilitools.saves.items.enums.known_clothing import EKnownClothing
from lilitools.saves.modfiles.character.clothing.clothing_type import ClothingType

TSubParsersAction = argparse._SubParsersAction


def _register_clothing(subp: argparse._SubParsersAction):
    p: argparse.ArgumentParser = subp.add_parser("clothing")

    register_standard_args(p)

    register_ass(p)
    register_breasts(p)
    register_core(p)
    register_penis(p)
    register_vagina(p)
    register_face(p)

    register_output_args(p)

    p.set_defaults(cmd=cmd_clothing)


def cmd_clothing(args: argparse.Namespace) -> None:
    clothing: AbstractClothing = AbstractClothing()
    ekc: Optional[EKnownClothing] = None

    if args.item_from_template is not None:
        try:
            ekc = TEMPLATE_CHOICES[args.item_from_template]
        except KeyError:
            click.secho(
                f"Unknown --item-from-template {args.item_from_template!r}",
                fg="red",
                err=True,
            )
            valid = ", ".join(sorted(TEMPLATE_CHOICES.keys()))
            click.secho(f"Valid choices: {valid!r}", fg="red", err=True)
            sys.exit(1)

    elif args.item_from_clothing_type is not None:
        try:
            ekc = EKnownClothing[args.item_from_clothing_type]
        except KeyError:
            click.secho(
                f"Unknown --item-from-clothing-type {args.item_from_template!r}",
                fg="red",
                err=True,
            )
            valid = ", ".join(sorted([kc.name for kc in EKnownClothing]))
            click.secho(f"Valid choices: {valid!r}", fg="red", err=True)
            sys.exit(1)

    elif args.item_from_scratch:
        clothing.id = args.id
        clothing.name = args.name
        clothing.colours = []
        for i in range(MAX_CLOTHING_COLOUR_LEN):
            v = getattr(args, f"colour_{i+1}", None)
            if v is not None:
                clothing.colours.append(v)

    elif args.item_from_save is not None or args.item_from_recipe is not None:
        clothing = getClothing(args)

    if ekc is not None:
        clothing.id = ekc.name
        ct: ClothingType = ekc.value
        clothing.name = args.name or ct.nameSingular
        clothing.colours = []
        for colorset in ct.getGeneratedOrAllColourPossibilities():
            clothing.colours.append(
                None if colorset is None else random.choice(list(colorset))
            )
        clothing.enchantmentKnown = True
        clothing.isDirty = False
        clothing.effects = []

    if args.clear_effects:
        clothing.effects = []

    enchanter = Enchanter(clothing)

    c = getCharFrom(args)

    handle_ass(args, c, enchanter)
    handle_breasts(args, c, enchanter)
    handle_core(args, c, enchanter)
    handle_penis(args, c, enchanter)
    handle_vagina(args, c, enchanter)

    handle_output(args, c, enchanter, clothing)

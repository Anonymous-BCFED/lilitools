import argparse
from typing import Optional

from lilitools.cli.lilitool.commands.enchant.clothing.args.standard import handle_orifice, handleIDOArgs, injectIDOArgs, register_orifice
from lilitools.game.effects.enchanter import Enchanter
from lilitools.saves.character.game_character import GameCharacter


def register_ass(argp: argparse.ArgumentParser) -> None:
    assg = argp.add_argument_group('Ass')
    # TODO
    #assg.add_argument('--ass-type', type=str)
    #assg.add_argument('--ass-type-from-char', action='store_true', default=False)

    assg.add_argument('--ass-size', type=int)
    assg.add_argument('--ass-size-from-char', action='store_true', default=False)
    injectIDOArgs(assg, 'ass-size', 'ass size')

    assg.add_argument('--hip-size', type=str)
    assg.add_argument('--hip-size-from-char', action='store_true', default=False)
    injectIDOArgs(assg, 'hip-size', 'hip size')

    register_orifice(assg, 'anus')

    assg.add_argument('--ass-from-char', action='store_true', default=False)


def handle_ass(args: argparse.Namespace, c: GameCharacter, enchanter: Enchanter) -> None:
    '''
    type: Optional[str] = None
    if args.ass_type is not None:
        type = args.ass_type
    if args.ass_from_char or args.ass_type_from_char:
        type = c.body.ass.type
    if type > 0:
        enchanter.ass(). TODO
    '''

    boost_only, drain_only = handleIDOArgs(args, 'ass_size')
    assSize: Optional[int] = None
    if args.ass_size is not None:
        assSize = args.ass_size
    if args.ass_size_from_char or args.ass_from_char:
        assSize = c.body.ass.assSize
    if assSize is not None:
        enchanter.ass().bun_size(assSize, boost_only=boost_only, drain_only=drain_only)

    boost_only, drain_only = handleIDOArgs(args, 'hip_size')
    hipSize: Optional[int] = None
    if args.hip_size is not None:
        hipSize = args.hip_size
    if args.hip_size_from_char or args.ass_from_char:
        hipSize = c.body.ass.hipSize
    if hipSize is not None:
        enchanter.ass().hip_size(hipSize, boost_only=boost_only, drain_only=drain_only)

    handle_orifice(args, 'anus', enchanter.anus(), c, args.ass_from_char)

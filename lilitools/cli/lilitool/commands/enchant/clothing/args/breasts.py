import argparse
from typing import Optional, Set

from lilitools.cli.lilitool.commands.enchant.clothing.args.standard import handleIDOArgs, injectIDOArgs
from lilitools.game.effects.enchanter import Enchanter
from lilitools.saves.character.enums.orifice_modifier import EOrificeModifier
from lilitools.saves.character.game_character import GameCharacter


def register_breasts(argp: argparse.ArgumentParser) -> None:
    g = argp.add_argument_group('Ass')
    # TODO
    #assg.add_argument('--ass-type', type=str)
    #assg.add_argument('--ass-type-from-char', action='store_true', default=False)

    g.add_argument('--breast-cup-size', type=int)
    g.add_argument('--breast-cup-size-from-char', action='store_true', default=False)
    injectIDOArgs(g, 'breast-cup-size', 'breast cup size')

    g.add_argument('--breast-nipple-size', type=int)
    g.add_argument('--breast-nipple-size-from-char', action='store_true', default=False)
    injectIDOArgs(g, 'breast-nipple-size', 'breast nipple size')

    g.add_argument('--breast-areolae-size', type=int)
    g.add_argument('--breast-areolae-size-from-char', action='store_true', default=False)
    injectIDOArgs(g, 'breast-areolae-size', 'breast areolae size')

    g.add_argument('--breast-lactation', type=int)
    g.add_argument('--breast-lactation-from-char', action='store_true', default=False)
    injectIDOArgs(g, 'breast-lactation', 'breast lactation')

    g.add_argument('--breast-regeneration', type=int)
    g.add_argument('--breast-regeneration-from-char', action='store_true', default=False)
    injectIDOArgs(g, 'breast-regeneration', 'breast regeneration')

    g.add_argument('--add-breast-modifiers', type=str, nargs='*', default=[])
    g.add_argument('--rm-breast-modifiers', type=str, nargs='*', default=[])
    g.add_argument('--set-breast-modifiers', type=str, nargs='*', default=[])
    g.add_argument('--clear-breast-modifiers', type=str, nargs='*', default=[])
    g.add_argument('--breast-modifiers-from-char', action='store_true', default=False)

    g.add_argument('--breasts-from-char', action='store_true', default=False)


def handle_breasts(args: argparse.Namespace, c: GameCharacter, enchanter: Enchanter) -> None:
    '''
    type: Optional[str] = None
    if args.ass_type is not None:
        type = args.ass_type
    if args.ass_from_char or args.ass_type_from_char:
        type = c.body.ass.type
    if type > 0:
        enchanter.ass(). TODO
    '''

    boost_only, drain_only = handleIDOArgs(args, 'breast_cup_size')
    breastSize: Optional[int] = None
    if args.breast_cup_size is not None:
        breastSize = args.breast_cup_size
    if args.breast_cup_size_from_char or args.breasts_from_char:
        breastSize = c.body.breasts.size
    if breastSize is not None:
        enchanter.breasts().of_cup_size(breastSize, boost_only=boost_only, drain_only=drain_only)

    boost_only, drain_only = handleIDOArgs(args, 'breast_nipple_size')
    breastNippleSize: Optional[int] = None
    if args.breast_nipple_size is not None:
        breastNippleSize = args.breast_nipple_size
    if args.breast_nipple_size_from_char or args.breasts_from_char:
        breastNippleSize = c.body.nipples.nippleSize
    if breastNippleSize is not None:
        enchanter.breasts().of_nipple_size(breastNippleSize, boost_only=boost_only, drain_only=drain_only)

    boost_only, drain_only = handleIDOArgs(args, 'breast_areolae_size')
    breastAreolaeSize: Optional[int] = None
    if args.breast_areolae_size is not None:
        breastAreolaeSize = args.breast_areolae_size
    if args.breast_areolae_size_from_char or args.breasts_from_char:
        breastAreolaeSize = c.body.nipples.areolaeSize
    if breastAreolaeSize is not None:
        enchanter.breasts().of_areolae_size(breastAreolaeSize, boost_only=boost_only, drain_only=drain_only)

    boost_only, drain_only = handleIDOArgs(args, 'breast_lactation')
    breastLactation: Optional[int] = None
    if args.breast_lactation is not None:
        breastLactation = args.breast_lactation
    if args.breast_lactation_from_char or args.breasts_from_char:
        breastLactation = c.body.breasts.milkStorage
    if breastLactation is not None:
        enchanter.breasts().of_lactation_amount(breastLactation, boost_only=boost_only, drain_only=drain_only)

    boost_only, drain_only = handleIDOArgs(args, 'breast_regeneration')
    breastRegeneration: Optional[int] = None
    if args.breast_regeneration is not None:
        breastRegeneration = args.breast_regeneration
    if args.breast_regeneration_from_char or args.breasts_from_char:
        breastRegeneration = c.body.breasts.milkRegeneration
    if breastRegeneration is not None:
        enchanter.breasts().of_regeneration_rate(breastRegeneration, boost_only=boost_only, drain_only=drain_only)

    breastModsToAdd: Set[EOrificeModifier] = set()
    breastModsToRemove: Set[EOrificeModifier] = set()
    if len(args.add_breast_modifiers) > 0:
        breastModsToAdd |= set(args.add_breast_modifiers)
    if len(args.rm_breast_modifiers) > 0:
        breastModsToAdd |= set(args.rm_breast_modifiers)
    if args.breast_modifiers_from_char or args.breasts_from_char:
        enchanter.breasts().the_same_orifice_mods_as(c)
    if len(breastModsToAdd) > 0:
        enchanter.breasts().using_orifice_mods(breastModsToAdd)
    if len(breastModsToRemove) > 0:
        enchanter.breasts().not_using_orifice_mods(breastModsToRemove)

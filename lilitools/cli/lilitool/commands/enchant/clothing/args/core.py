
import argparse
from typing import Optional

from lilitools.cli.lilitool.commands.enchant.clothing.args.standard import handleBodyHairArgs, handleIDOArgs, injectBodyHairArgs, injectIDOArgs
from lilitools.cli.units import UNITS, handle_length, register_length
from lilitools.game.effects.enchanter import Enchanter
from lilitools.saves.character.game_character import GameCharacter


def register_core(argp: argparse.ArgumentParser) -> None:
    bodyg = argp.add_argument_group('Core')

    register_length(bodyg, 'height', 'height', internal_units=UNITS.cm)
    injectIDOArgs(bodyg, 'height', 'height')

    bodyg.add_argument('--size', type=int, default=None, help='Set body size')
    bodyg.add_argument('--size-from-char', action='store_true', default=False, help='Copy body size from char')
    injectIDOArgs(bodyg, 'size', 'size')

    bodyg.add_argument('--muscle', type=int, default=None, help='Set muscle size')
    bodyg.add_argument('--muscle-from-char', action='store_true', default=False, help='Copy muscle size from char')
    injectIDOArgs(bodyg, 'muscle', 'muscle')

    bodyg.add_argument('--femininity', type=int, default=None, help='Set femininity (0=masculine, 100=feminine')
    bodyg.add_argument('--femininity-from-char', action='store_true', default=False, help='Copy femininity from char')
    injectIDOArgs(bodyg, 'femininity', 'femininity')

    injectBodyHairArgs(bodyg, 'pubic-hair')

    bodyg.add_argument('--core-from-char', action='store_true', default=False, help='Copy all core values from char')


def handle_core(args: argparse.Namespace, c: GameCharacter, enchanter: Enchanter) -> None:
    boost_only, drain_only = handleIDOArgs(args, 'height')
    height: Optional[int] = handle_length(args, 'height', internal_units=UNITS.cm)
    if args.height_from_char or args.core_from_char:
        height = c.body.core.height
    if height is not None and height > 0:
        enchanter.core().of_height(height, boost_only=boost_only, drain_only=drain_only)

    boost_only, drain_only = handleIDOArgs(args, 'size')
    size: Optional[int] = None
    if args.size is not None:
        size = args.size
    if args.size_from_char or args.core_from_char:
        size = c.body.core.bodySize
    if size is not None:
        enchanter.core().of_body_size(size, boost_only=boost_only, drain_only=drain_only)

    boost_only, drain_only = handleIDOArgs(args, 'muscle')
    muscle: Optional[int] = None
    if args.muscle is not None:
        muscle = args.muscle
    if args.muscle_from_char or args.core_from_char:
        muscle = c.body.core.muscle
    if muscle is not None:
        enchanter.core().of_muscle_size(muscle, boost_only=boost_only, drain_only=drain_only)

    boost_only, drain_only = handleIDOArgs(args, 'femininity')
    femininity: Optional[int] = None
    if args.femininity is not None:
        femininity = args.femininity
    if args.femininity_from_char or args.core_from_char:
        femininity = c.body.core.femininity
    if femininity is not None:
        enchanter.core().of_femininity(femininity, boost_only=boost_only, drain_only=drain_only)

    handleBodyHairArgs(args, c, enchanter, 'pubic_hair', enchanter.core(), lambda x: x.body.core.pubicHair)

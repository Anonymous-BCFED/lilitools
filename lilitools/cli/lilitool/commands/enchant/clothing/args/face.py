
import argparse
from typing import Optional, Set

from lilitools.cli.lilitool.commands.enchant.clothing.args.standard import (handle_orifice, handleBodyHairArgs, handleIDOArgs, injectBodyHairArgs, injectIDOArgs, 
                                                                       register_orifice)
from lilitools.cli.units import UNITS, handle_length, register_length
from lilitools.game.effects.enchanter import Enchanter
from lilitools.saves.character.body.enums.tongue_modifier import ETongueModifier
from lilitools.saves.character.game_character import GameCharacter


def register_face(argp: argparse.ArgumentParser) -> None:
    bodyg = argp.add_argument_group('Face')

    bodyg.add_argument('--lip-size', type=int, default=None, help='Set lip size [0,3]')
    bodyg.add_argument('--lip-size-from-char', action='store_true', default=False, help='Copy lip size from char')
    injectIDOArgs(bodyg, 'lip-size', 'lip-size')

    register_length(bodyg, 'tongue-length', 'tongue length', internal_units=UNITS.cm)
    injectIDOArgs(bodyg, 'tongue-length', 'tongue length')

    injectBodyHairArgs(bodyg, 'facial-hair')

    register_orifice(bodyg, 'mouth')

    bodyg.add_argument('--add-tongue-modifiers', type=str, nargs='*', default=[])
    bodyg.add_argument('--rm-tongue-modifiers', type=str, nargs='*', default=[])
    bodyg.add_argument('--set-tongue-modifiers', type=str, nargs='*', default=[])
    bodyg.add_argument('--clear-tongue-modifiers', type=str, nargs='*', default=[])
    bodyg.add_argument('--tongue-modifiers-from-char', action='store_true', default=False)

    bodyg.add_argument('--face-from-char', action='store_true', default=False, help='Copy all face values from char')


def handle_face(args: argparse.Namespace, c: GameCharacter, enchanter: Enchanter) -> None:
    boost_only, drain_only = handleIDOArgs(args, 'tongue_length')
    tongue_length: Optional[int] = handle_length(args, 'tongue_length', internal_units=UNITS.cm)
    if args.tongue_length_from_char or args.face_from_char:
        tongue_length = c.body.tongue.length
    if tongue_length is not None and tongue_length > 0:
        enchanter.face().of_tongue_length(tongue_length, boost_only=boost_only, drain_only=drain_only)

    boost_only, drain_only = handleIDOArgs(args, 'lipSize')
    lipSize: Optional[int] = None
    if args.lip_size is not None:
        lipSize = args.lip_size
    if args.tongue_length_from_char or args.face_from_char:
        lipSize = c.body.mouth.lipSize
    if lipSize is not None:
        enchanter.face().of_lip_size(lipSize, boost_only=boost_only, drain_only=drain_only)

    handleBodyHairArgs(args, c, enchanter, 'lip_size', enchanter.face(), lambda x: x.body.mouth.lipSize)
    handle_orifice(args, 'mouth', enchanter.face(), c, args.tongue_length_from_char or args.face_from_char)

    tongueModsToAdd: Set[ETongueModifier] = set()
    tongueModsToRemove: Set[ETongueModifier] = set()
    if len(args.add_tongue_modifiers) > 0:
        tongueModsToAdd |= set(args.add_tongue_modifiers)
    if len(args.rm_tongue_modifiers) > 0:
        tongueModsToAdd |= set(args.rm_tongue_modifiers)
    if args.tongue_modifiers_from_char or args.face_from_char:
        enchanter.face().the_same_tongue_mods_as(c)
    if len(tongueModsToAdd) > 0:
        enchanter.face().using_tongue_mods(tongueModsToAdd)
    if len(tongueModsToRemove) > 0:
        enchanter.face().not_using_tongue_mods(tongueModsToRemove)


import argparse
import sys
from pathlib import Path
from typing import Optional, cast

from lxml import etree

from lilitools.consts import MAX_CLOTHING_COLOUR_LEN
from lilitools.game.effects.enchanter import Enchanter
from lilitools.logging import ClickWrap
from lilitools.saves.character.body.tattoos.tattoo import Tattoo
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.enchantment_recipe import EnchantmentRecipe
from lilitools.saves.inventory.character_inventory import CharacterInventory
from lilitools.saves.items.abstract_clothing import AbstractClothing
from lilitools.saves.items.abstract_item import AbstractItem
from lilitools.saves.items.abstract_weapon import AbstractWeapon
from lilitools.saves.utils import loadSaveFrom, saveToFile

click = ClickWrap()


def register_output_args(p: argparse.ArgumentParser) -> None:
    rptg: argparse._MutuallyExclusiveGroup = p.add_mutually_exclusive_group()
    rptg.add_argument('--repeat-times', type=int, default=0, help='Repeat contents n times.')
    rptg.add_argument('--repeat-until-max', action='store_true', default=False, help='Repeat contents until the maximum limit of spells has been reached.')

    outg = p.add_argument_group('Output')
    outg.add_argument('--no-optimize', action='store_true', default=False)
    outg.add_argument('--no-sort', action='store_true', default=False)
    outg.add_argument('--as', choices=['clothing', 'clothing_enchantment'], default='clothing')
    outg.add_argument('--to-file', type=argparse.FileType('wb'), default=None)
    outg.add_argument('--to-save-file', type=Path, default=None)
    outg.add_argument('--count', type=int, default=1, help='Output clothing/item/weapon stack size, if applicable.')

    chartarget = outg.add_mutually_exclusive_group()
    chartarget.add_argument('--to-save-npc', type=str, default=None)
    chartarget.add_argument('--to-save-player', action='store_true', default=False)
    chartarget.add_argument('--to-save-player-room', action='store_true', default=False)


def handle_output(args: argparse.Namespace,
                  c: GameCharacter,
                  enchanter: Enchanter,
                  clothing: Optional[AbstractClothing] = None,
                  item: Optional[AbstractItem] = None,
                  weapon: Optional[AbstractWeapon] = None,
                  tattoo: Optional[Tattoo] = None) -> None:
    if not args.no_optimize:
        enchanter.optimize()
    if args.repeat_until_max or args.repeat_times > 0:
        repeated = enchanter.effects.copy()
        nPerRepeat = len(repeated)
        if args.repeat_until_max:
            i = 0
            enchanter.effects.clear()
            while nPerRepeat + len(enchanter.effects) < 100:
                enchanter.effects += repeated
                i += 1
            click.success(f'Repeated recipe {i} times.')
        else:
            for _ in range(args.repeat_times):
                enchanter.effects += repeated
            click.success(f'Repeated recipe {args.repeat_times} times.')
    if not args.no_sort:
        enchanter.sort()

    as_ = getattr(args, 'as')
    #print(repr(getattr(args, 'as')))
    if as_ == 'clothing':
        _do_new_clothing(args, enchanter, clothing)

    elif as_.endswith('_enchantment'):
        _do_enchantment_recipe(args, enchanter, clothing, as_)


def _do_new_clothing(args: argparse.Namespace, enchanter: Enchanter, clothing: Optional[AbstractClothing]) -> None:
    assert clothing is not None
    enchanter.applyToClothing(clothing)
    if any([hasattr(args, f'colour_{i+1}') for i in range(MAX_CLOTHING_COLOUR_LEN)]):
        with click.info(f'Colours:'):
            for i in range(MAX_CLOTHING_COLOUR_LEN):
                argcolour = getattr(args, f'colour_{i+1}')
                if argcolour is not None and len(clothing.colours) >= (i + 1):
                    click.secho(f'[{i}]: {clothing.colours[0]!r} -> {argcolour!r}')
                    clothing.colours[i] = argcolour

    if args.to_file is not None:
        with click.info(f'Writing to {args.to_file.name}...'):
            et: etree._ElementTree = etree.ElementTree(clothing.toXML())
            with args.to_file:
                et.write(args.to_file, encoding='utf-8', pretty_print=True)
            click.success(f'Successfully wrote clothing to {args.to_file.name}')
    elif args.to_save_file is not None:
        assert args.to_save_file.is_file(), 'Save is not a file.'
        with click.info(f'Injecting into {args.to_save_file}...'):
            g = loadSaveFrom(args.to_save_file)
            inv: CharacterInventory
            place: str = ''
            if args.to_save_npc is not None:
                if (npc := g.getNPCById(args.to_save_npc)) is None:
                    click.error(f'Could not find an NPC with ID {args.to_save_npc!r}')
                    sys.exit(1)
                inv = npc.characterInventory
                place = f'NPC {npc.getName()!r}\'s inventory'
            elif args.to_save_player:
                inv = g.playerCharacter.characterInventory
                place = 'your inventory'
            elif args.to_save_player_room:
                inv = cast(CharacterInventory, g.worlds.getPlayerRoom().getOrInstantiateInventory())
                place = 'your room\'s inventory'
            assert inv is not None, f'{place} is None!'
            inv.clothingInInventory[clothing] = args.count
            saveToFile(g, args.to_save_file)
            click.success(f'Successfully wrote clothing stack of size {args.count} to {args.to_save_file} in {place}.')
    else:
        print(etree.tostring(clothing.toXML(), pretty_print=True, encoding='unicode'))


def _do_enchantment_recipe(args: argparse.Namespace, enchanter: Enchanter, clothing: Optional[AbstractClothing], as_: str) -> None:
    assert clothing is not None
    e = EnchantmentRecipe()
    #e.clothingType = args.id or 'innoxia_finger_ring'
    e.name = args.name or clothing.name
    e.itemEffects = enchanter.effects
    if as_ == 'clothing_enchantment':
        e.clothingType = clothing.id
    elif as_ == 'item_enchantment':
        e.itemType = clothing.id
    elif as_ == 'weapon_enchantment':
        e.itemType = clothing.id
    elif as_ == 'tattoo_enchantment':
        e.tattooType = clothing.id
    if args.to_file is not None:
        et: etree._ElementTree = etree.ElementTree(e.toXML())
        with args.to_file:
            et.write(args.to_file, encoding='utf-8', pretty_print=True)
        click.secho(f'Successfully wrote enchantment recipe to {args.to_file.name}', fg='green')
    else:
        print(etree.tostring(e.toXML(), pretty_print=True, encoding='unicode'))

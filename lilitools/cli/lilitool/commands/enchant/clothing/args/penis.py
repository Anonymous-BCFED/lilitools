import argparse
from typing import Optional, Set

from lilitools.cli.lilitool.commands.enchant.clothing.args.standard import handle_liquid_volume, handle_orifice, handleIDOArgs, injectIDOArgs, register_liquid_volume, register_orifice
from lilitools.cli.units import UNITS, getVolPerDay, handle_length, register_length
from lilitools.game.effects.enchanter import Enchanter
from lilitools.logging import ClickWrap
from lilitools.saves.character.enums.fluid_regeneration import EFluidRegeneration
from lilitools.saves.character.enums.penetrator_modifier import EPenetratorModifier
from lilitools.saves.character.game_character import GameCharacter
from lilitools.utils import clampInt

click = ClickWrap()

LIQUID_STORAGE_UNIT = UNITS.milliliters


def register_penis(argp: argparse.ArgumentParser) -> None:
    g = argp.add_argument_group('Penis')

    g.add_argument('--enable-penis', action='store_true', default=False)
    g.add_argument('--disable-penis', action='store_true', default=False)

    g.add_argument('--cum-expulsion', type=int, default=None, help='Expulsion percentage')
    g.add_argument('--cum-expulsion-min', action='store_true', default=False, help='Set expulsion to 0')
    g.add_argument('--cum-expulsion-max', action='store_true', default=False, help='Set expulsion to max possible value')
    g.add_argument('--cum-expulsion-from-char', action='store_true', default=False)
    injectIDOArgs(g, 'cum-expulsion', 'cum expulsion', units=UNITS.percent)

    register_liquid_volume(g, 'cum-storage', 'Cum storage', internal_units=UNITS.milliliters)
    g.add_argument('--cum-storage-min', action='store_true', default=False, help='Set storage to 0 mL')
    g.add_argument('--cum-storage-max', action='store_true', default=False, help='Set storage to max possible value')
    g.add_argument('--cum-storage-from-char', action='store_true', default=False)
    injectIDOArgs(g, 'cum-storage', 'cum storage')

    suffix = f'Converted to {getVolPerDay():~,P} and rounded'
    g.add_argument('--cum-regeneration', type=str, default=None, help=f'Cum regeneration rate (any pint-parsable value; {suffix})')
    g.add_argument('--cum-regeneration-min', action='store_true', default=False, help=f'Set regeneration to 0 {getVolPerDay():~,P}')
    g.add_argument('--cum-regeneration-max', action='store_true', default=False, help='Set regeneration to max possible value')
    g.add_argument('--cum-regeneration-from-char', action='store_true', default=False, help='Set regeneration to the value of the selected character')
    injectIDOArgs(g, 'cum-regeneration', 'cum regeneration')

    register_length(g, 'penis-length', 'penis length', internal_units=UNITS.centimeters)
    injectIDOArgs(g, 'penis-length', 'penis length')

    register_orifice(g, 'penis-urethra')

    g.add_argument('--penis-girth', type=int, default=None)
    g.add_argument('--penis-girth-from-char', action='store_true', default=False)
    injectIDOArgs(g, 'penis-girth', 'penis girth')

    g.add_argument('--add-penis-modifiers', type=str, nargs='*', default=[])
    g.add_argument('--rm-penis-modifiers', type=str, nargs='*', default=[])
    g.add_argument('--set-penis-modifiers', type=str, nargs='*', default=[])
    g.add_argument('--clear-penis-modifiers', type=str, nargs='*', default=[])
    g.add_argument('--penis-modifiers-from-char', action='store_true', default=False)

    g.add_argument('--penis-from-char', action='store_true', default=False)


def handle_penis(args: argparse.Namespace, c: GameCharacter, enchanter: Enchanter) -> None:
    if (args.penis_from_char and c.body.penis is not None) or args.enable_penis:
        enchanter.penis().grow()
    if (args.penis_from_char and c.body.penis is None) or args.disable_penis:
        enchanter.penis().remove()
        return

    boost_only, drain_only = handleIDOArgs(args, 'cum_expulsion')
    cum_expulsion: Optional[int] = None
    if args.cum_expulsion is not None:
        cum_expulsion = args.cum_expulsion
    elif args.cum_expulsion_min:
        # cum_expulsion = 0
        vol = None
        enchanter.penis().min_cum_expulsion()
    elif args.cum_expulsion_max:
        # cum_expulsion = 100
        vol = None
        enchanter.penis().max_cum_expulsion()
    elif args.cum_expulsion_from_char or args.penis_from_char:
        if c.body.testicles:
            cum_expulsion = c.body.testicles.cumExpulsion
    if cum_expulsion is not None:
        #print('expulsion percentage', repr({'boost_only': boost_only, 'drain_only': drain_only}))
        enchanter.penis().of_expulsion_percentage(clampInt(round(cum_expulsion), 0, 100), boost_only=boost_only, drain_only=drain_only)

    boost_only, drain_only = handleIDOArgs(args, 'cum_regeneration')
    cum_regen_rate: Optional[int] = None
    if args.cum_regeneration is not None:
        # Convert to internal unit system
        cum_regen_rate = int(round(UNITS(args.cum_regeneration).to(UNITS.millilitres / UNITS.day).magnitude))
    elif args.cum_regeneration_min:
        # cum_regen_rate = EFluidRegeneration.ZERO_SLOW.min
        vol = None
        enchanter.penis().min_cum_regeneration_rate()
    elif args.cum_regeneration_max:
        # cum_regen_rate = EFluidRegeneration.FOUR_VERY_RAPID.max
        vol = None
        enchanter.penis().max_cum_regeneration_rate()
    elif args.cum_regeneration_from_char or args.penis_from_char:
        if c.body.testicles:
            cum_regen_rate = c.body.testicles.cumRegeneration
    if cum_regen_rate is not None:
        #print('cum regen',repr({'boost_only': boost_only,'drain_only':drain_only}))
        enchanter.penis().of_cum_regeneration_rate(clampInt(round(cum_regen_rate), 0, EFluidRegeneration.FOUR_VERY_RAPID.max), boost_only=boost_only, drain_only=drain_only)

    boost_only, drain_only = handleIDOArgs(args, 'cum_storage')
    vol: Optional[int] = handle_liquid_volume(args, 'cum_storage', internal_units=UNITS.milliliter)
    if args.cum_storage_from_char or args.penis_from_char:
        if c.body.testicles:
            vol = c.body.testicles.cumStorage
    elif args.cum_storage_min:
        #vol = ECumProduction.ZERO_NONE.min
        vol = None
        enchanter.penis().min_cum_storage()
    elif args.cum_storage_max:
        # vol = ECumProduction.SEVEN_MONSTROUS.max
        vol = None
        enchanter.penis().max_cum_storage()
    if vol is not None:
        #print('storage', repr({'boost_only': boost_only, 'drain_only': drain_only}))
        enchanter.penis().of_cum_storage_size(vol, boost_only=boost_only, drain_only=drain_only)

    boost_only, drain_only = handleIDOArgs(args, 'penis_length')
    penis_length: Optional[int] = handle_length(args, 'penis_length', internal_units=UNITS.centimeter)
    if args.penis_length_from_char or args.penis_from_char:
        if c.body.penis:
            penis_length = c.body.penis.penetrator.length
    if penis_length is not None:
        enchanter.penis().of_length(penis_length, boost_only=boost_only, drain_only=drain_only)

    handle_orifice(args, 'penis_urethra', enchanter.penis(), c, args.penis_from_char)

    penisModsToAdd: Set[EPenetratorModifier] = set()
    penisModsToRemove: Set[EPenetratorModifier] = set()
    if len(args.add_penis_modifiers) > 0:
        penisModsToAdd |= set(args.add_penis_modifiers)
    if len(args.rm_penis_modifiers) > 0:
        penisModsToAdd |= set(args.rm_penis_modifiers)
    if args.penis_modifiers_from_char or args.penis_from_char:
        enchanter.penis().the_same_penetrator_mods_as(c)
    if len(penisModsToAdd) > 0:
        enchanter.penis().using_penetrator_mods(penisModsToAdd)
    if len(penisModsToRemove) > 0:
        enchanter.penis().not_using_penetrator_mods(penisModsToRemove)

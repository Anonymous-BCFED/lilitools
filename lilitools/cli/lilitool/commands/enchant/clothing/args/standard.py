import argparse
from typing import Callable, Optional, Set, Tuple, cast

from pint import Quantity, Unit

from lilitools.cli.lilitool.api.export import LTExportFactory
from lilitools.cli.lilitool.commands.enchant.clothing.clothing_templates import (
    TEMPLATE_CHOICES,
)
from lilitools.cli.units import UNITS
from lilitools.cli.utils import arg2enum, existingFilePath
from lilitools.consts import MAX_CLOTHING_COLOUR_LEN
from lilitools.game.effects.enchanter import Enchanter
from lilitools.game.effects.enchanter.mixins.body_hair_mixin import BodyHairMixins
from lilitools.game.effects.enchanter.mixins.orifice import OrificeEnchantmentFactory
from lilitools.game.effects.utils import enumType
from lilitools.saves.character.body.enums.capacity import ECapacity
from lilitools.saves.character.body.enums.depth import EDepth
from lilitools.saves.character.body.enums.elasticity import EElasticity
from lilitools.saves.character.body.enums.plasticity import EPlasticity
from lilitools.saves.character.body.enums.wetness import EWetness
from lilitools.saves.character.enums.body_hair import EBodyHair
from lilitools.saves.character.enums.orifice_modifier import EOrificeModifier
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.utils import loadSaveFrom


def register_standard_args(argp: argparse.ArgumentParser) -> None:
    addCharLoadArgs(argp)
    addItemLoadArgs(argp)


def addCharLoadArgs(argp: argparse.ArgumentParser) -> None:
    savetypeg = argp.add_mutually_exclusive_group()
    savetypeg.add_argument("--from-char-xml", type=existingFilePath, default=None)
    savetypeg.add_argument("--from-save", type=existingFilePath, default=None)
    savetypeg.add_argument("--from-scratch", action="store_true", default=False)

    targetg = argp.add_argument_group("Save Character Targetting")
    targetg.add_argument("--target-player", action="store_true", default=False)
    targetg.add_argument("--target-npc", type=str, default=None)


def addItemLoadArgs(argp: argparse.ArgumentParser) -> None:
    itemg = argp.add_argument_group("Item Loading")

    itemg.add_argument(
        "--item-from-template", "-T", choices=TEMPLATE_CHOICES.keys(), default=None
    )
    itemg.add_argument("--item-from-clothing-type", "-c", type=str, default=None)
    itemg.add_argument("--item-from-recipe", type=existingFilePath, default=None)
    itemg.add_argument("--item-from-save", type=existingFilePath, default=None)
    itemg.add_argument("--item-from-scratch", action="store_true", default=False)

    itemg.add_argument("--id", "-i", type=str, default=None)
    itemg.add_argument("--name", "-n", type=str, default=None)
    for i in range(MAX_CLOTHING_COLOUR_LEN):
        itemg.add_argument(f"--colour-{i+1}", f"--color-{i+1}", type=str, default=None)

    itemg.add_argument("--item-name-in-save", type=str, default=None)

    itemg.add_argument("--clear-effects", "-C", action="store_true", default=False)


def getCharFrom(args: argparse.Namespace) -> GameCharacter:
    if args.from_char_xml is not None:
        return LTExportFactory.FromFile(args.from_char_xml)
    elif args.from_save is not None:
        save = loadSaveFrom(args.from_save)
        if args.target_player:
            return cast(GameCharacter, save.playerCharacter)
        elif args.target_npc is not None:
            return save.npcsByID[args.target_npc]
        else:
            raise Exception(
                "You must specify --target-player or --target-npc=<npcID> with --from-save."
            )
    elif args.from_scratch:
        return GameCharacter()
    raise Exception("You must specify --from-save, --from-char-xml, or --from-scratch.")


def injectIDOArgs(
    argp: argparse.ArgumentParser,
    suffix: str,
    desc: Optional[str] = None,
    units: Optional[Unit] = None,
) -> None:
    """Add --only-boost-* and --only-drain-* arguments.

    Args:
        argp (argparse.ArgumentParser):
        suffix (str): The end part of the argument (e.g. ass-size)
        desc (Optional[str], optional): what to call the attribute being changed (e.g. ass size)
    """
    if desc is None:
        desc = suffix.replace("-", " ")
    argp.add_argument(
        f"--only-boost-{suffix}",
        f"--only-increase-{suffix}",
        action="store_true",
        default=False,
        help="Do not drain " + desc,
    )
    argp.add_argument(
        f"--only-drain-{suffix}",
        f"--only-decrease-{suffix}",
        action="store_true",
        default=False,
        help="Do not boost " + desc,
    )


def handleIDOArgs(args: argparse.Namespace, suffix: str) -> Tuple[bool, bool]:
    """Translates --only-(boost|drain) arguments into a tuple.

    Args:
        args (argparse.Namespace): Argument namespace
        suffix (str): suffix_of_argparse_attributes

    Returns:
        Tuple[bool, bool]: only boost and only drain
    """
    return getattr(args, f"only_boost_{suffix}"), getattr(args, f"only_drain_{suffix}")


def injectBodyHairArgs(g: argparse._ArgumentGroup, prefix: str) -> None:
    g.add_argument(f"--{prefix}", type=enumType(EBodyHair), default=None)
    g.add_argument(f"--{prefix}-from-char", action="store_true", default=False)


def handleBodyHairArgs(
    args: argparse.Namespace,
    c: GameCharacter,
    enchanter: Enchanter,
    prefix: str,
    factory: BodyHairMixins,
    get_charval: Callable[[GameCharacter], EBodyHair],
) -> None:
    bh: Optional[EBodyHair] = None
    prefix = prefix.replace("-", "_")
    arg_bodyhair: Optional[EBodyHair] = getattr(args, prefix)
    arg_bodyhairfromchar: Optional[EBodyHair] = getattr(args, f"{prefix}_from_char")
    if arg_bodyhair is not None:
        bh = arg_bodyhair
    if arg_bodyhairfromchar:
        bh = get_charval(c)
    if bh is not None:
        factory.hair_length(bh.value)


def register_liquid_volume(
    g: argparse._ArgumentGroup, argprefix: str, subject: str, internal_units: Unit
) -> None:
    suffix = f"Internally converted to {internal_units:~,P} and rounded"
    g.add_argument(
        f"--{argprefix}-cc",
        type=float,
        default=None,
        help=f"{subject} (in cubic centimeters; {suffix})",
    )
    g.add_argument(
        f"--{argprefix}-cup",
        type=float,
        default=None,
        help=f"{subject} (in cups; {suffix})",
    )
    g.add_argument(
        f"--{argprefix}-gal",
        type=float,
        default=None,
        help=f"{subject} (in gallons; {suffix})",
    )
    g.add_argument(
        f"--{argprefix}-l",
        type=float,
        default=None,
        help=f"{subject} (in liters; {suffix})",
    )
    g.add_argument(
        f"--{argprefix}-ml",
        type=float,
        default=None,
        help=f"{subject} (in mL; {suffix})",
    )
    g.add_argument(
        f"--{argprefix}-tbsp",
        type=float,
        default=None,
        help=f"{subject} (in tablespoons; {suffix})",
    )
    g.add_argument(
        f"--{argprefix}-tsp",
        type=float,
        default=None,
        help=f"{subject} (in teaspoons; {suffix})",
    )
    g.add_argument(
        f"--{argprefix}",
        type=str,
        default=None,
        help=f"Set {subject.lower()} in arbitrary pint-parsable liquid volume units (e.g. 40cc, 5mL; {suffix})",
    )


def handle_liquid_volume(
    args: argparse.Namespace, argprefix: str, internal_units: Unit
) -> Optional[int]:
    cc: Optional[float] = getattr(args, argprefix + "_cc", None)
    cup: Optional[float] = getattr(args, argprefix + "_cup", None)
    gal: Optional[float] = getattr(args, argprefix + "_gal", None)
    l: Optional[float] = getattr(args, argprefix + "_l", None)
    ml: Optional[float] = getattr(args, argprefix + "_ml", None)
    tbsp: Optional[float] = getattr(args, argprefix + "_tbsp", None)
    tsp: Optional[float] = getattr(args, argprefix + "_tsp", None)
    arbitrary: Optional[str] = getattr(args, argprefix, None)
    vol: Optional[Quantity] = None
    if (
        cc is not None
        or cup is not None
        or gal is not None
        or l is not None
        or ml is not None
        or tbsp is not None
        or tsp is not None
        or arbitrary is not None
    ):
        vol = Quantity(0, internal_units)
    if cc is not None:
        vol += Quantity(cc, UNITS.cubic_centimeter)
    if cup is not None:
        vol += Quantity(cup, UNITS.cup)
    if gal is not None:
        vol += Quantity(gal, UNITS.gallon)
    if l is not None:
        vol += Quantity(l, UNITS.liter)
    if ml is not None:
        vol += Quantity(ml, UNITS.milliliter)
    if tbsp is not None:
        vol += Quantity(tbsp, UNITS.tablespoon)
    if tsp is not None:
        vol += Quantity(tsp, UNITS.teaspoon)
    if arbitrary is not None:
        vol += UNITS(arbitrary)
    if vol is None:
        return None
    return int(round(vol.to(internal_units).magnitude))


def register_orifice(
    g: argparse._ArgumentGroup,
    argprefix: str,
    no_wetness: bool = False,
    no_depth: bool = False,
    no_elasticity: bool = False,
    no_plasticity: bool = False,
    no_capacity: bool = False,
    no_modifiers: bool = False,
) -> None:
    if not no_modifiers:
        gmods = g.add_argument_group("Modifiers")
        gmods.add_argument(
            f"--add-{argprefix}-modifiers", type=str, nargs="*", default=[]
        )
        gmods.add_argument(
            f"--rm-{argprefix}-modifiers", type=str, nargs="*", default=[]
        )
        # gmods.add_argument(f'--set-{argprefix}-modifiers', type=str, nargs='*', default=[])
        # gmods.add_argument(f'--clear-{argprefix}-modifiers', type=str, nargs='*', default=[])
        gmods.add_argument(
            f"--{argprefix}-modifiers-from-char", action="store_true", default=False
        )

    if not no_wetness:
        g.add_argument(
            f"--{argprefix}-wetness",
            type=arg2enum(EWetness, skip_prefix=True),
            default=None,
        )
        g.add_argument(f"--same-{argprefix}-wetness", action="store_true", default=None)
        injectIDOArgs(g, f"{argprefix}-wetness")
    if not no_depth:
        g.add_argument(
            f"--{argprefix}-depth",
            type=arg2enum(EDepth, skip_prefix=True),
            default=None,
        )
        g.add_argument(f"--same-{argprefix}-depth", action="store_true", default=None)
        injectIDOArgs(g, f"{argprefix}-depth")
    if not no_elasticity:
        g.add_argument(
            f"--{argprefix}-elasticity",
            type=arg2enum(EElasticity, skip_prefix=True),
            default=None,
        )
        g.add_argument(
            f"--same-{argprefix}-elasticity", action="store_true", default=None
        )
        injectIDOArgs(g, f"{argprefix}-elasticity")
    if not no_plasticity:
        g.add_argument(
            f"--{argprefix}-plasticity",
            type=arg2enum(EPlasticity, skip_prefix=True),
            default=None,
        )
        g.add_argument(
            f"--same-{argprefix}-plasticity", action="store_true", default=None
        )
        injectIDOArgs(g, f"{argprefix}-plasticity")
    if not no_capacity:
        g.add_argument(
            f"--{argprefix}-capacity",
            type=arg2enum(ECapacity, skip_prefix=True),
            default=None,
        )
        g.add_argument(
            f"--same-{argprefix}-capacity", action="store_true", default=None
        )
        injectIDOArgs(g, f"{argprefix}-capacity")


def handle_orifice(
    args: argparse.Namespace,
    argprefix: str,
    ofc: OrificeEnchantmentFactory,
    c: GameCharacter,
    the_same_as: bool,
    no_wetness: bool = False,
    no_depth: bool = False,
    no_elasticity: bool = False,
    no_plasticity: bool = False,
    no_capacity: bool = False,
    no_modifiers: bool = False,
) -> None:
    if not no_wetness:
        boost_only, drain_only = handleIDOArgs(args, f"{argprefix}_wetness")
        if (wetness := getattr(args, f"{argprefix}_wetness")) is not None:
            ofc.wetness_of(wetness, boost_only=boost_only, drain_only=drain_only)
        if getattr(args, f"same_{argprefix}_wetness", False) or the_same_as:
            ofc.the_same_wetness_as(c, boost_only=boost_only, drain_only=drain_only)
    if not no_depth:
        boost_only, drain_only = handleIDOArgs(args, f"{argprefix}_depth")
        if (depth := getattr(args, f"{argprefix}_depth")) is not None:
            ofc.depth_of(depth, boost_only=boost_only, drain_only=drain_only)
        if getattr(args, f"same_{argprefix}_depth", False) or the_same_as:
            ofc.the_same_depth_as(c, boost_only=boost_only, drain_only=drain_only)
    if not no_plasticity:
        boost_only, drain_only = handleIDOArgs(args, f"{argprefix}_plasticity")
        if (plasticity := getattr(args, f"{argprefix}_plasticity")) is not None:
            ofc.plasticity_of(plasticity, boost_only=boost_only, drain_only=drain_only)
        if getattr(args, f"same_{argprefix}_plasticity", False) or the_same_as:
            ofc.the_same_plasticity_as(c, boost_only=boost_only, drain_only=drain_only)
    if not no_elasticity:
        boost_only, drain_only = handleIDOArgs(args, f"{argprefix}_elasticity")
        if (elasticity := getattr(args, f"{argprefix}_elasticity")) is not None:
            ofc.elasticity_of(elasticity, boost_only=boost_only, drain_only=drain_only)
        if getattr(args, f"same_{argprefix}_elasticity", False) or the_same_as:
            ofc.the_same_elasticity_as(c, boost_only=boost_only, drain_only=drain_only)
    if not no_capacity:
        boost_only, drain_only = handleIDOArgs(args, f"{argprefix}_capacity")
        if (capacity := getattr(args, f"{argprefix}_capacity")) is not None:
            ofc.capacity_of(capacity, boost_only=boost_only, drain_only=drain_only)
        if getattr(args, f"same_{argprefix}_capacity", False) or the_same_as:
            ofc.the_same_capacity_as(c, boost_only=boost_only, drain_only=drain_only)
    if not no_modifiers:
        ofcModsToAdd: Set[EOrificeModifier] = set()
        ofcModsToRemove: Set[EOrificeModifier] = set()
        addargs = [
            EOrificeModifier[x] for x in getattr(args, f"add_{argprefix}_modifiers", [])
        ]
        rmargs = [
            EOrificeModifier[x] for x in getattr(args, f"rm_{argprefix}_modifiers", [])
        ]
        mfc = getattr(args, f"{argprefix}_modifiers_from_char", False)
        if len(addargs) > 0:
            ofcModsToAdd |= set(addargs)
        if len(rmargs) > 0:
            ofcModsToAdd |= set(rmargs)
        if mfc or the_same_as:
            ofc.the_same_orifice_mods_as(c)
        if len(ofcModsToAdd) > 0:
            ofc.using_orifice_mods(ofcModsToAdd)
        if len(ofcModsToRemove) > 0:
            ofc.not_using_orifice_mods(ofcModsToRemove)

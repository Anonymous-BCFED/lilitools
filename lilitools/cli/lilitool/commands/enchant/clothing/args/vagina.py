import argparse
import sys
from typing import Optional, Set

from lilitools.cli.lilitool.commands.enchant.clothing.args.standard import handle_orifice, handleIDOArgs, injectIDOArgs, register_orifice
from lilitools.game.effects.enchanter import Enchanter
from lilitools.saves.character.game_character import GameCharacter


def register_vagina(argp: argparse.ArgumentParser) -> None:
    g = argp.add_argument_group('Vagina')
    # TODO
    #assg.add_argument('--ass-type', type=str)
    #assg.add_argument('--ass-type-from-char', action='store_true', default=False)

    g.add_argument('--enable-vagina', action='store_true', default=False)
    g.add_argument('--disable-vagina', action='store_true', default=False)

    g.add_argument('--labia-size', type=int)
    g.add_argument('--labia-size-from-char', action='store_true', default=False)
    injectIDOArgs(g, 'labia-size', 'labia size')

    #register_orifice(g, 'vagina-urethra')
    register_orifice(g, 'vagina')

    g.add_argument('--vagina-from-char', action='store_true', default=False)


def handle_vagina(args: argparse.Namespace, c: GameCharacter, enchanter: Enchanter) -> None:
    #print(repr(sys.argv))
    #print(repr(args))
    if args.vagina_from_char:
        #print('vfc')
        enchanter.vagina().the_same_vagina_as(c)

    if (args.vagina_from_char and c.body.vagina is not None) or args.enable_vagina:
        enchanter.vagina().enable()
    if (args.vagina_from_char and c.body.vagina is None) or args.disable_vagina:
        enchanter.vagina().disable()
        return

    boost_only,drain_only = handleIDOArgs(args, 'labia_size')
    labiaSize: Optional[int] = None
    if args.labia_size is not None:
        labiaSize = args.labia_size
    if args.vagina_from_char:
        labiaSize = c.body.vagina.labiaSize
    if labiaSize is not None:
        enchanter.vagina().labia_size(labiaSize, boost_only=boost_only, drain_only=drain_only)

    handle_orifice(args, 'vagina', enchanter.vagina(), c, args.vagina_from_char)


from typing import Dict

from lilitools.saves.items.enums.known_clothing import EKnownClothing

TEMPLATE_CHOICES: Dict[str, EKnownClothing] = {
    'anklet': EKnownClothing.innoxia_ankle_anklet,
    'earrings': EKnownClothing.innoxia_piercing_ear_ball_studs,
    'femininewatch': EKnownClothing.WRIST_WOMENS_WATCH,
    'masculinewatch': EKnownClothing.WRIST_MENS_WATCH,
    'nipplerings': EKnownClothing.norin_piercings_piercing_nipple_rings,
    'nosestud': EKnownClothing.innoxia_piercing_nose_ball_stud,
    'penisring': EKnownClothing.innoxia_piercing_penis_ring,
    'ring': EKnownClothing.innoxia_finger_ring,
    'ringedbarbell': EKnownClothing.innoxia_piercing_ringed_barbell,
}

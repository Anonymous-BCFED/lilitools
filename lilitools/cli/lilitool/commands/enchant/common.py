import argparse
import sys

import tqdm
from lxml import etree

from lilitools.logging import ClickWrap
from lilitools.saves.items.abstract_clothing import AbstractClothing

click = ClickWrap()
def getClothing(args: argparse.Namespace) -> AbstractClothing:
    c = AbstractClothing()
    # print(args.item_from_save, args.item_name_in_save)
    if args.item_from_save and args.item_name_in_save:
        # print(f'Attempting to find {args.item_name_in_save} in {args.item_from_save}')
        found: int = 0
        allItems = set()
        with args.item_from_save.open('r') as f:
            doc = etree.parse(f, parser=etree.XMLParser(remove_comments=True))
            click.secho(f'Finding item named {args.item_name_in_save!r} in save {args.item_from_save}...')
            cxml: etree._Element
            for cxml in tqdm.tqdm(doc.xpath(('//clothing'))):
                allItems.add(cxml.attrib['name'])
                if cxml.attrib['name'] != args.item_name_in_save:
                    continue
                found += 1
                if found == 1:
                    # print(etree.tostring(cxml))
                    c.fromXML(cxml)
                click.warn(f'Found more than one matching node.  Ignoring duplicate match: {cxml.getpath()}')
            if found == 1:
                with click.secho('COULD NOT FIND ITEM WITH THAT NAME', fg='red', err=True):
                    [print(x) for x in sorted(allItems)]
                sys.exit(1)
    elif args.item_from_save and not args.item_name_in_save:
        click.error('--item-from-save requires --item-name-in-save to be set to the name of the object you seek.')
    elif not args.item_from_save and args.item_name_in_save:
        click.error('--item-name-in-save requires --item-in-save to be set to the path of the save you wish to find the item in.')

    if args.id:
        c.id = args.id
    if args.name:
        c.name = args.name
    if args.clear_effects:
        c.effects = []
    return c

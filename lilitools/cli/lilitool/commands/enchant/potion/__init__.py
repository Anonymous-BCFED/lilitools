import argparse

from lilitools.cli.lilitool.commands.enchant.potion.args.ass import handle_ass
from lilitools.cli.lilitool.commands.enchant.potion.args.output import handle_output

from .args.arms import handle_arms
from .args.antennae import handle_antennae

from lilitools.cli.lilitool.commands.enchant.potion.args.standard import getCharFrom, getPCFrom, register_standard_args
from lilitools.game.effects.alchemist import Alchemist
from lilitools.saves.items.abstract_item import AbstractItem


TSubParsersAction = argparse._SubParsersAction


def _register_potion(subp: argparse._SubParsersAction):
    p: argparse.ArgumentParser = subp.add_parser('potion')

    register_standard_args(p)

    # register_ass(p)
    # register_breasts(p)
    # register_core(p)
    # register_penis(p)
    # register_vagina(p)
    # register_face(p)

    # register_output_args(p)

    p.set_defaults(cmd=cmd_potion)


def cmd_potion(args: argparse.Namespace) -> None:
    item: AbstractItem = AbstractItem()
    # ekc: Optional[EKnownClothing] = None

    # if args.item_from_template is not None:
    #     try:
    #         ekc = TEMPLATE_CHOICES[args.item_from_template]
    #     except KeyError:
    #         click.secho(f'Unknown --item-from-template {args.item_from_template!r}', fg='red', err=True)
    #         valid = ', '.join(sorted(TEMPLATE_CHOICES.keys()))
    #         click.secho(f'Valid choices: {valid!r}', fg='red', err=True)
    #         sys.exit(1)

    # if args.item_from_clothing_type is not None:
    #     try:
    #         ekc = EKnownClothing[args.item_from_clothing_type]
    #     except KeyError:
    #         click.secho(f'Unknown --item-from-clothing-type {args.item_from_template!r}', fg='red', err=True)
    #         valid = ', '.join(sorted([kc.name for kc in EKnownClothing]))
    #         click.secho(f'Valid choices: {valid!r}', fg='red', err=True)
    #         sys.exit(1)
    
    if True: #args.item_from_scratch:
        item.id = args.id
        item.name = args.name
        # item.colours = []
        # for i in range(MAX_CLOTHING_COLOUR_LEN):
        #     v = getattr(args, f'colour_{i+1}', None)
        #     if v is not None:
        #         item.colours.append(v)

    # elif (args.item_from_save is not None or args.item_from_recipe is not None):
    #     item = getItem(args)

    # if ekc is not None:
    #     item.id = ekc.name
    #     item.name = args.name or ekc.nameSingular
    #     # item.colours = []
    #     # for colorset in ekc.colourPossibilities:
    #     #     item.colours.append(None if colorset is None else random.choice(list(colorset)))
    #     item.enchantmentKnown = True
    #     item.isDirty = False
    #     item.effects = []

    if args.clear_effects:
        item.itemEffects = []

    baseid: str = args.potion_species
    alchemist = Alchemist(baseid, None)

    p = getPCFrom(args)
    c = getCharFrom(args)

    handle_antennae(args, p, c, alchemist)
    handle_arms(args, p, c, alchemist)
    handle_ass(args, p, c, alchemist)
    # handle_breasts(args, c, alchemist)
    # handle_core(args, c, alchemist)
    # handle_penis(args, c, alchemist)
    # handle_vagina(args, c, alchemist)

    handle_output(args, c, alchemist, item)

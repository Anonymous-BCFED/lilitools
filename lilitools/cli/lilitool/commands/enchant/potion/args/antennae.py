import argparse
from typing import Optional

from lilitools.cli.lilitool.commands.enchant.potion.args.standard import handleIDOArgs, injectIDOArgs
from lilitools.game.effects.alchemist import Alchemist
from lilitools.saves.character.game_character import GameCharacter


def register_antennae(argp: argparse.ArgumentParser) -> None:
    antennaeg = argp.add_argument_group('Antennae')

    antennaeg.add_argument('--antennae-type', type=str)
    antennaeg.add_argument('--antennae-type-from-char', action='store_true', default=False)

    antennaeg.add_argument('--antennae-size', type=int)
    antennaeg.add_argument('--antennae-size-from-char', action='store_true', default=False)
    injectIDOArgs(antennaeg, 'antennae-size', 'antennae size')

    antennaeg.add_argument('--antennae-rows', type=str)
    antennaeg.add_argument('--antennae-rows-from-char', action='store_true', default=False)
    injectIDOArgs(antennaeg, 'antennae-rows', 'antennae rows')

    antennaeg.add_argument('--antennae-count-per-row', type=str)
    antennaeg.add_argument('--antennae-count-per-row-from-char', action='store_true', default=False)
    injectIDOArgs(antennaeg, 'antennae-count-per-row', 'antennae per row')

    antennaeg.add_argument('--antennae-from-char', action='store_true', default=False)

    antennaeg.add_argument('--antennae-remove', action='store_true', default=False)


def handle_antennae(args: argparse.Namespace, p: GameCharacter, c: Optional[GameCharacter], alchemist: Alchemist) -> None:
    if args.antennae_remove:
        alchemist.antennae().remove_all()
        return

    antennaeType: Optional[str] = None
    if args.antennae_type is not None:
        antennaeType = args.antennae_type
    if args.antennae_type_from_char or args.antennae_from_char:
        antennaeType = c.body.antennae.type
    if antennaeType is not None:
        alchemist.antennae().set_type(antennaeType)

    boost_only: bool
    drain_only: bool
    boost_only, drain_only = handleIDOArgs(args, 'antennae_size')
    if args.antennae_size is not None:
        alchemist.antennae().change_size(args.antennae_size - p.body.antennae.length, boost_only=boost_only, drain_only=drain_only)
    if args.antennae_size_from_char or args.antennae_from_char:
        alchemist.antennae().the_same_size_as(p, c, boost_only=boost_only,drain_only=drain_only)

    boost_only, drain_only = handleIDOArgs(args, 'antennae_rows')
    if args.antennae_rows is not None:
        alchemist.antennae().change_row_count(args.antennae_rows - p.body.antennae.rows, boost_only=boost_only, drain_only=drain_only)
    if args.antennae_rows_from_char or args.antennae_from_char:
        alchemist.antennae().the_same_row_count_as(p, c, boost_only=boost_only, drain_only=drain_only)

    boost_only, drain_only = handleIDOArgs(args, 'antennae_count_per_row')
    if args.antennae_count_per_row is not None:
        alchemist.antennae().change_row_count(args.antennae_count_per_row - p.body.antennae.rows, boost_only=boost_only, drain_only=drain_only)
    if args.antennae_count_per_row_from_char or args.antennae_from_char:
        alchemist.antennae().the_same_row_count_as(p, c, boost_only=boost_only, drain_only=drain_only)

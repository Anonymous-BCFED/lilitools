import argparse
from typing import Optional

from lilitools.cli.lilitool.commands.enchant.potion.args.standard import handleIDOArgs, injectIDOArgs
from lilitools.game.effects.alchemist import Alchemist
from lilitools.saves.character.game_character import GameCharacter


def register_arms(argp: argparse.ArgumentParser) -> None:
    armsg = argp.add_argument_group('Arms')

    armsg.add_argument('--arms-type', type=str)
    armsg.add_argument('--arms-type-from-char', action='store_true', default=False)

    armsg.add_argument('--arms-size', type=int)
    armsg.add_argument('--arms-size-from-char', action='store_true', default=False)
    injectIDOArgs(armsg, 'arms-size', 'arms size')

    armsg.add_argument('--arms-rows', type=str)
    armsg.add_argument('--arms-rows-from-char', action='store_true', default=False)
    injectIDOArgs(armsg, 'arms-rows', 'arm rows')

    armsg.add_argument('--arms-from-char', action='store_true', default=False)

    armsg.add_argument('--arms-remove', action='store_true', default=False)


def handle_arms(args: argparse.Namespace, p: GameCharacter, c: Optional[GameCharacter], alchemist: Alchemist) -> None:
    # if args.arms_remove:
    #     alchemist.arms().remove_all()
    #     return

    armsType: Optional[str] = None
    if args.arms_type is not None:
        armsType = args.arms_type
    if args.arms_type_from_char or args.arms_from_char:
        armsType = c.body.arms.type
    if armsType is not None:
        alchemist.arms().set_type(armsType)
        
    boost_only, drain_only = handleIDOArgs(args, 'arms_rows')
    if args.arms_rows is not None:
        alchemist.arms().change_pair_count(args.arms_rows - p.body.arms.rows)
    if args.arms_rows_from_char or args.arms_from_char:
        alchemist.arms().the_same_number_of_pairs_as(p, c,boost_only=boost_only,drain_only=drain_only)

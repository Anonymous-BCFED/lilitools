import argparse
from typing import Optional

from lilitools.cli.lilitool.commands.enchant.potion.args.standard import injectIDOArgs
from lilitools.game.effects.alchemist import Alchemist
from lilitools.saves.character.game_character import GameCharacter


def register_ass(argp: argparse.ArgumentParser) -> None:
    assg = argp.add_argument_group('Arms')

    assg.add_argument('--arms-type', type=str)
    assg.add_argument('--arms-type-from-char', action='store_true', default=False)

    assg.add_argument('--arms-size', type=int)
    assg.add_argument('--arms-size-from-char', action='store_true', default=False)
    injectIDOArgs(assg, 'arms-size', 'arms size')

    assg.add_argument('--arms-rows', type=str)
    assg.add_argument('--arms-rows-from-char', action='store_true', default=False)
    injectIDOArgs(assg, 'hip-size', 'hip size')

    assg.add_argument('--arms-from-char', action='store_true', default=False)


def handle_ass(args: argparse.Namespace, p: GameCharacter, c: Optional[GameCharacter], alchemist: Alchemist) -> None:
    # if args.arms_remove:
    #     alchemist.arms().remove_all()
    #     return

    armsType: Optional[str] = None
    if args.arms_type is not None:
        armsType = args.arms_type
    if args.arms_type_from_char or args.arms_from_char:
        armsType = c.body.arms.type
    if armsType is not None:
        alchemist.arms().set_type(armsType)

    if args.arms_rows is not None:
        alchemist.arms().change_pair_count(args.arms_rows - p.body.arms.rows)
    if args.arms_rows_from_char or args.arms_from_char:
        alchemist.arms().the_same_number_of_pairs_as(p, c)

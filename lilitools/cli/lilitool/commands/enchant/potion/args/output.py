
import argparse
import sys
from pathlib import Path
from typing import Optional, cast

from lxml import etree

from lilitools.consts import MAX_CLOTHING_COLOUR_LEN
from lilitools.game.effects.alchemist import Alchemist
from lilitools.logging import ClickWrap
from lilitools.saves.character.body.tattoos.tattoo import Tattoo
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.enchantment_recipe import EnchantmentRecipe
from lilitools.saves.inventory.character_inventory import CharacterInventory
from lilitools.saves.items.abstract_clothing import AbstractClothing
from lilitools.saves.items.abstract_item import AbstractItem
from lilitools.saves.items.abstract_weapon import AbstractWeapon
from lilitools.saves.utils import loadSaveFrom, saveToFile

click = ClickWrap()


def register_output_args(p: argparse.ArgumentParser) -> None:
    rptg: argparse._MutuallyExclusiveGroup = p.add_mutually_exclusive_group()
    # rptg.add_argument('--repeat-times', type=int, default=0, help='Repeat contents n times.')
    # rptg.add_argument('--repeat-until-max', action='store_true', default=False, help='Repeat contents until the maximum limit of spells has been reached.')

    outg = p.add_argument_group('Output')
    outg.add_argument('--no-optimize', action='store_true', default=False)
    outg.add_argument('--no-sort', action='store_true', default=False)
    # outg.add_argument('--as', choices=['clothing', 'clothing_enchantment'], default='clothing')
    outg.add_argument('--to-file', type=argparse.FileType('wb'), default=None)
    outg.add_argument('--to-save-file', type=Path, default=None)
    outg.add_argument('--count', type=int, default=1, help='Output clothing/item/weapon stack size, if applicable.')

    chartarget = outg.add_mutually_exclusive_group()
    chartarget.add_argument('--to-save-npc', type=str, default=None)
    chartarget.add_argument('--to-save-player', action='store_true', default=False)
    chartarget.add_argument('--to-save-player-room', action='store_true', default=False)


def handle_output(args: argparse.Namespace,
                  p: GameCharacter,
                  c: GameCharacter,
                  alchemist: Alchemist,
                  item: Optional[AbstractItem] = None,
                  weapon: Optional[AbstractWeapon] = None,
                  tattoo: Optional[Tattoo] = None) -> None:
    # if not args.no_optimize:
    #     alchemist.optimize()

    # if args.repeat_until_max or args.repeat_times > 0:
    #     repeated = alchemist.effects.copy()
    #     nPerRepeat = len(repeated)
    #     if args.repeat_until_max:
    #         i = 0
    #         alchemist.effects.clear()
    #         while nPerRepeat + len(alchemist.effects) < 100:
    #             alchemist.effects += repeated
    #             i += 1
    #         click.success(f'Repeated recipe {i} times.')
    #     else:
    #         for _ in range(args.repeat_times):
    #             alchemist.effects += repeated
    #         click.success(f'Repeated recipe {args.repeat_times} times.')
    if not args.no_sort:
        alchemist.sort()

    _do_potion(args, alchemist, item)


def _do_potion(args: argparse.Namespace, alchemist: Alchemist, old_item: Optional[AbstractItem]) -> None:
    itm = AbstractItem()
    if old_item:
        itm.id = old_item.id
        itm.colour = old_item.colour
        itm.name = old_item.name
    itm.itemEffects = alchemist.effects.copy()
    print(etree.tostring(itm.toXML(), pretty_print=True, encoding='unicode'))

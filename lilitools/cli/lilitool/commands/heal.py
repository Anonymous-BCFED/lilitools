import argparse

from lilitools.cli.utils import bool2yn_colored, existingFilePath
from lilitools.logging import ClickWrap
from lilitools.saves.character.enums.attribute import EAttribute
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.game import Game
from lilitools.saves.utils import loadSaveFrom, saveToFile

click = ClickWrap()


def register_heal(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser('heal', description="Heal a character. Calculations of maximum values can be significantly incorrect.")

    p.add_argument('savefile', metavar='savefile.xml', type=existingFilePath)

    p.add_argument('--health', '--hp', '-H', action='store_true', default=False, help='Restore health')
    p.add_argument('--aura', '--mana', '-A', action='store_true', default=False, help='Restore Aura')
    p.add_argument('--lust', '-L', action='store_true', default=False, help='Reset Lust')
    p.add_argument('--all', action='store_true', default=False, help='Restore HP, Aura, and Lust')
    p.add_argument('--dry-run', action='store_true', default=False)

    pwho = p.add_mutually_exclusive_group()
    pwho.add_argument('--player', action='store_true', default=False)
    pwho.add_argument('--npc-id', type=str, default=None)
    pwho.title = 'Whomst'
    pwho.description = 'Who is the subject of this command'

    p.set_defaults(cmd=cmd_heal)


def cmd_heal(args: argparse.Namespace) -> None:
    g: Game = loadSaveFrom(args.savefile)
    gc: GameCharacter
    if args.player:
        gc = g.playerCharacter
    elif args.npc_id is not None:
        gc = g.getNPCById(args.npc_id)
        assert gc is not None, 'Could not find that NPC. See `npc list`.'
    assert gc is not None, 'You MUST specify either --player or --npc-id.'

    changed = False

    if args.health or args.all:
        maxhp = gc.getAttributeValue(EAttribute.HEALTH_MAXIMUM)
        if gc.core.health != maxhp:
            click.secho(f'HP: {gc.core.health} -> {maxhp}')
            gc.core.health = maxhp
            changed = True
        # else:
        #     click.secho(f'HP: {gc.core.health} -> {maxhp}')

    if args.aura or args.all:
        maxmana = gc.getAttributeValue(EAttribute.MANA_MAXIMUM)
        if gc.core.mana != maxmana:
            click.secho(f'Aura: {gc.core.mana} -> {maxmana}')
            gc.core.mana = maxmana
            changed = True
        # else:
        #     click.secho(f'Aura: {gc.core.mana} -> {maxmana}')

    if args.lust or args.all:
        lust = gc.getAttributeValue(EAttribute.LUST)
        min_lust = float(gc.getRestingLust())
        if lust > min_lust:
            click.secho(f'Lust: {lust} -> {min_lust}')
            gc.setAttributeValue(EAttribute.LUST, min_lust)
            changed = True
        # else:
        #     click.secho(f'Lust: {lust} -> 0.')
    # for k,v in gc.bonusAttributes.items():
    #     print(k.name, repr(v))
    click.secho(f'Is Pregnant: {bool2yn_colored(gc.isPregnant())}')
    
    if args.dry_run:
        return

    if changed:
        saveToFile(g, args.savefile)

import argparse

from lilitools.cli.lilitool.commands.inventory.add import register_inventory_add
from lilitools.cli.lilitool.commands.inventory.fix import register_inventory_fix
from lilitools.cli.lilitool.commands.inventory.remove import register_inventory_remove


def register_inventory(subp: argparse._SubParsersAction) -> None:
    p = subp.add_parser('inventory', aliases=['inv']).add_subparsers()

    register_inventory_add(p)
    register_inventory_fix(p)
    register_inventory_remove(p)

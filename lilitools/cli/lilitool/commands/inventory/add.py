import argparse
import sys
from typing import cast

from lilitools.cli.utils import existingFilePath, fuzzybool
from lilitools.consts import MAX_CLOTHING_COLOUR_LEN
from lilitools.logging import ClickWrap
from lilitools.saves.inventory.character_inventory import CharacterInventory
from lilitools.saves.items.abstract_clothing import AbstractClothing
from lilitools.saves.utils import loadSaveFrom, saveToFile

click = ClickWrap()
__all__ = ["register_inventory_add", "cmd_inventory_add_clothing"]


def register_inventory_add(subp: argparse._SubParsersAction) -> None:
    enchp: argparse._SubParsersAction = subp.add_parser("add").add_subparsers()
    _register_inventory_add_clothing(enchp)


def _register_inventory_add_clothing(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser("clothing", aliases=["clothes"])

    g = p.add_mutually_exclusive_group(required=True)
    g.add_argument(
        "--player",
        action="store_true",
        default=False,
        help="We place the result in the player's inventory",
    )
    g.add_argument(
        "--player-room",
        action="store_true",
        default=False,
        help="We place the result in the player's room",
    )
    g.add_argument(
        "--npc",
        type=str,
        default=None,
        help="We place the result in the NPC's inventory",
    )

    g = p.add_mutually_exclusive_group()
    p.add_argument("--id", "-I", type=str, help="Clothing ID", default=None)
    p.add_argument("--count", "-c", type=int, default=1, help="Stack size")
    p.add_argument("--name", "-n", type=str, default=None)
    for i in range(MAX_CLOTHING_COLOUR_LEN):
        p.add_argument(f"--colour-{i+1}", f"--color-{i+1}", type=str, default=None)
    p.add_argument(
        "--dirty",
        "-D",
        type=fuzzybool,
        default=False,
        help="Whether the clothing item is dirty or not",
    )
    p.add_argument(
        "--sealed",
        "-S",
        type=fuzzybool,
        default=False,
        help="Whether the clothing item is sealed or not",
    )
    p.add_argument(
        "--enchantment-known",
        "-E",
        type=fuzzybool,
        default=True,
        help="Whether the clothing item's enchantments are known or not",
    )

    p.add_argument("savefile", type=existingFilePath)
    p.set_defaults(cmd=cmd_inventory_add_clothing)


def cmd_inventory_add_clothing(args: argparse.Namespace) -> None:
    assert args.savefile.is_file(), "Save is not a file."
    clothing = AbstractClothing()
    clothing.id = args.id
    clothing.name = args.name
    clothing.isDirty = args.dirty
    clothing.sealed = args.sealed
    clothing.enchantmentKnown = args.enchantment_known
    clothing.colours = []
    for i in range(MAX_CLOTHING_COLOUR_LEN):
        v = getattr(args, f"colour_{i+1}", None)
        if v is not None:
            clothing.colours.append(v)
    with click.info(f"Injecting into {args.savefile}..."):
        g = loadSaveFrom(args.savefile)
        inv: CharacterInventory
        place: str = ""
        if args.npc is not None:
            if (npc := g.getNPCById(args.npc)) is None:
                click.error(f"Could not find an NPC with ID {args.npc!r}")
                sys.exit(1)
            inv = npc.characterInventory
            place = f"NPC {npc.getName()!r}'s inventory"
        elif args.player:
            inv = g.playerCharacter.characterInventory
            place = "your inventory"
        elif args.player_room:
            inv = cast(
                CharacterInventory, g.worlds.getPlayerRoom().getOrInstantiateInventory()
            )
            place = "your room's inventory"
        assert inv is not None, f"{place} is None!"
        inv.clothingInInventory[clothing] = args.count
        saveToFile(g, args.savefile)
        click.success(
            f"Successfully wrote clothing stack of size {args.count} to {args.savefile} in {place}."
        )

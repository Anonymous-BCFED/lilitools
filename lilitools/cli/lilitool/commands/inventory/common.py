import argparse
import contextlib

from lilitools.cli.lilitool.api.inventory import _InventoryAPIWrapper, _InventorySearchWrapperAPI, InventorySearcher, MatchHandler, search_as_english
from lilitools.cli.utils import fuzzybool
from lilitools.logging import ClickWrap
from lilitools.saves.inventory.character_inventory import CharacterInventory

click = ClickWrap()


def add_standard_inventory_search_args(p: argparse.ArgumentParser) -> None:
    sp = p.add_argument_group('Inventory Search')
    sp.add_argument('--id', '-i', type=str, nargs='*', default=None)
    sp.add_argument('--name', '-n', type=str, nargs='*', default=None)
    sp.add_argument('--sealed', type=fuzzybool, default=None)
    sp.add_argument('--enchantment-known', type=fuzzybool, default=None)


def cli_handle_inventory_search(ci: CharacterInventory,
                                args: argparse.Namespace,
                                handler: MatchHandler,
                                verbose: bool = False) -> None:
    #print(args.name)
    s = InventorySearcher(possible_ids=set(args.id) if args.id else {},
                          possible_names=set(args.name) if args.name else {},
                          enchantment_known=args.enchantment_known,
                          sealed=args.sealed)
    with (click.secho('Equipped Clothing:') if verbose else contextlib.nullcontext()):
        for clothing in list(ci.clothingEquipped):
            if s.match_clothing(clothing):
                handler.onMatchEquippedClothing(clothing, ci.clothingEquipped)
    with (click.secho('Carried Clothing:') if verbose else contextlib.nullcontext()):
        for clothing, count in list(ci.clothingInInventory.items()):
            if s.match_clothing(clothing):
                handler.onMatchCarriedClothing(clothing, count, ci.clothingInInventory)
    with (click.secho('Carried Items:') if verbose else contextlib.nullcontext()):
        for item, count in list(ci.itemsInInventory.items()):
            if s.match_item(item):
                handler.onMatchCarriedItem(item, count, ci.itemsInInventory)
    with (click.secho('Main Weapons:') if verbose else contextlib.nullcontext()):
        for item in list(ci.mainWeapons):
            if s.match_item(item):
                handler.onMatchMainWeapon(item, ci.mainWeapons)
    with (click.secho('Offhand Weapons:') if verbose else contextlib.nullcontext()):
        for item in list(ci.offhandWeapons):
            if s.match_item(item):
                handler.onMatchOffhandWeapon(item, ci.offhandWeapons)


def cli_handle_inventory_build_search(inv: _InventoryAPIWrapper,
                                      args: argparse.Namespace) -> _InventorySearchWrapperAPI:
    #print(args.name)
    return inv.buildSearch(possible_ids=set(args.id) if args.id else {},
                           possible_names=set(args.name) if args.name else {},
                           enchantment_known=args.enchantment_known,
                           is_sealed=args.sealed)


def cli_search_as_english(args: argparse.Namespace) -> str:
    return search_as_english(id=args.id, name=args.name, enchantment_known=args.enchantment_known, is_sealed=args.sealed)

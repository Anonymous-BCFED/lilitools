import argparse
from typing import List

from rich.console import Console
from rich.markdown import Markdown

from lilitools.cli.lilitool.api.inventory import _InventoryAPIWrapper
from lilitools.cli.lilitool.api.saves import LTSaveFactory, _SaveAPIWrapper
from lilitools.cli.utils import existingFilePath
from lilitools.logging import ClickWrap
from lilitools.saves.enums.tf_modifier import ETFModifier
from lilitools.saves.inventory.character_inventory import CharacterInventory
from lilitools.saves.items.abstract_clothing import AbstractClothing

click = ClickWrap()


def register_inventory_fix(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser("fix", aliases=["f"])
    whichp = p.add_argument_group()
    whichp.add_argument("--player", action="store_true", default=False)
    whichp.add_argument("--player-room", action="store_true", default=False)
    whichp.add_argument("--npcs", "--npc", type=str, nargs="*", default=[])
    whichp.add_argument("--all-npcs", action="store_true", default=False)
    whichp.add_argument("--cells", "--cell", type=str, nargs="*", default=[])
    whichp.add_argument("--all-cells", action="store_true", default=False)
    whichp.add_argument("--all", action="store_true", default=False)

    p.add_argument("savefile", type=existingFilePath)
    p.set_defaults(cmd=cmd_inventory_fix)
    p.add_argument("--commit", action="store_true", default=False)


def cmd_inventory_fix(args: argparse.Namespace) -> None:
    console = Console()
    save:_SaveAPIWrapper
    with console.status("Loading...") as status:
        save = LTSaveFactory.LoadSaveFromFile(args.savefile)
    inv: _InventoryAPIWrapper
    invs = []
    selections: int = 0
    with console.status("Collecting inventories...") as status:
        if args.player or args.all:
            status.update("Collecting player inventory...")
            invs += [("player", save.getPlayer().getInventory().inv)]
            selections+=1
        if args.player_room or args.all:
            status.update("Collecting player room inventory...")
            invs += [
                ("player room", save.getUniverse().getPlayerRoom().getInventory().inv)
            ]
            selections += 1

        if args.all or args.all_npcs:
            for npc in save.asGame().npcs:
                status.update(f"Collecting {npc.getName()}'s inventory...")
                invs += [(f"{npc.getName()}'s inventory", npc.characterInventory)]
            selections += 1
        elif len(args.npcs) > 0:
            for npc in save.asGame().npcs:
                if npc.core.id not in args.npcs:
                    continue
                status.update(f"Collecting {npc.getName()}'s inventory...")
                invs += [(f"{npc.getName()}'s inventory", npc.characterInventory)]
            selections += 1

        if args.all or args.all_cells:
            for wid, world in save.asGame().worlds.worlds.items():
                for i, cell in enumerate(world.cells):
                    status.update(f"Collecting inventory of cell {cell}...")
                    invs += [(f"Cell {wid}[{i}] inventory", cell.characterInventory)]
            selections += 1
        elif len(args.cells) > 0:
            for wid, world in save.asGame().worlds.worlds.items():
                for i, cell in enumerate(world.cells):
                    if cell.getName() not in args.cells:
                        continue
                    status.update(f"Collecting inventory of cell {cell}...")
                    invs += [(f"Cell {wid}[{i}] inventory", cell.characterInventory)]
            selections += 1

        if selections == 0:
            console.print('[red]No inventories selected.  See --help for information on how to select inventories.[/]')
            return
    changes: int = 0
    for src, inv in invs:
        if inv is None:
            continue
        with console.status(f"Attempting to repair inventory items from {src}..."):
            strchanges = []
            c = fix_inventory(strchanges, inv)
            if c > 0:
                changes += c
                report = ""
                for change in strchanges:
                    report += f"* {change}\n"
                console.rule(src)
                console.print(Markdown(report))
                console.print()
    if changes > 0:
        if not args.commit:
            console.print(
                f"[yellow]Safety: If this looks right, add --commit to the command-line arguments.[/]"
            )
        else:
            save.saveToFile(args.savefile)
            console.print(
                f"[green]Successfully fixed {changes} items and wrote changes to disk.[/]",
            )
    else:
        console.print('[green]No changes.[/]')


def fix_inventory(strchanges: List[str], inv: CharacterInventory) -> int:
    changes: int = 0
    for i, item in enumerate(inv.clothingEquipped):
        continue
    for i, item in enumerate(inv.clothingInInventory.keys()):
        if fixed_stuff_needing_seals(strchanges, i, item, inv, "clothingInInventory"):
            changes += 1
    return changes


def fixed_stuff_needing_seals(
    strchanges: List[str],
    i: int,
    item: AbstractClothing,
    inv: CharacterInventory,
    invid: str,
) -> bool:
    needs_seal: bool = False
    if item.effects is not None:
        for eff in item.effects:
            if (
                eff.type == "CLOTHING"
                and eff.mod1 == ETFModifier.CLOTHING_SPECIAL
                and eff.mod2 == ETFModifier.CLOTHING_SEALING
            ):
                needs_seal = True
    if needs_seal and not item.sealed:
        item.sealed = True
        strchanges.append(
            f"{invid}[{i}] {item.id!r} was not sealed, but it should be. Item has been re-sealed."
        )
        return True
    return False

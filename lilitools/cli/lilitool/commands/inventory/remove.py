import argparse

from lilitools.cli.lilitool.api.inventory import _InventoryAPIWrapper
from lilitools.cli.lilitool.api.saves import LTSaveFactory
from lilitools.cli.lilitool.commands.inventory.common import add_standard_inventory_search_args, cli_handle_inventory_build_search, cli_search_as_english
from lilitools.cli.utils import existingFilePath
from lilitools.logging import ClickWrap

click = ClickWrap()


def register_inventory_remove(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser('remove', aliases=['rm'])
    whichp = p.add_mutually_exclusive_group()
    whichp.add_argument('--player', action='store_true', default=False)
    whichp.add_argument('--player-room', action='store_true', default=False)
    add_standard_inventory_search_args(p)
    p.add_argument('savefile', type=existingFilePath)
    p.set_defaults(cmd=cmd_inventory_remove)
    p.add_argument('--actually-remove', action='store_true', default=False)


def cmd_inventory_remove(args: argparse.Namespace) -> None:
    save = LTSaveFactory.LoadSaveFromFile(args.savefile)
    inv: _InventoryAPIWrapper
    src = ''
    if args.player:
        inv = save.getPlayer().getInventory()
        src = 'player inventory'
    if args.player_room:
        inv = save.getUniverse().getPlayerRoom().getInventory()
        src = 'player room inventory'
    changes: int
    with click.info(f'Attempting to remove items with {cli_search_as_english(args)} from {src}...'):
        sapi = cli_handle_inventory_build_search(inv, args)
        changes = sapi.removeAll(dry_run=not args.actually_remove)
    if changes > 0:
        if not args.actually_remove:
            click.secho(f'Safety: If this looks right, add --actually-remove to the command-line arguments.', fg='yellow')
        else:
            save.saveToFile(args.savefile)
            click.secho(f'Successfully removed {changes} items and wrote changes to disk.', fg='green')

import argparse
from pathlib import Path

from rich.console import Console

from lilitools.mod_tests import ModChecker

__all__ = ["register_mod_check"]


def register_mod_check(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser(
        "mod-check",
        aliases=["modcheck", "mc"],
        description="Checks mods for common issues.",
    )

    p.set_defaults(cmd=cmd_mod_check)


def cmd_mod_check(args: argparse.Namespace) -> None:
    BASE_DIR: Path
    console = Console()
    with console.status("Finding BASE_DIR..."):
        for p in [Path.cwd(), Path.cwd() / "dist"]:
            if (p / "res" / "mods").is_dir():
                BASE_DIR = p
                break
    tester = ModChecker(BASE_DIR, console=console)
    messages_present = False
    for mod_dir, test_results in tester.check_all().items():
        if test_results.has_messages:
            messages_present = True
            console.print(f"[bold]{mod_dir.relative_to(BASE_DIR)}:[/]")
            for i in sorted(test_results.info):
                console.print(f"* {i}")
            for w in sorted(test_results.warnings):
                console.print(f"[yellow]* WARNING: {w}[/]")
            for e in sorted(test_results.errors):
                console.print(f"[red]* ERROR: {w}[/]")
    if not messages_present:
        console.print("[green]No obvious problems found.[/]")

import argparse
from lilitools.cli.lilitool.commands.npc.body import _register_npcs_body
from lilitools.cli.lilitool.commands.npc.clean import _register_npcs_clean
from lilitools.cli.lilitool.commands.npc.export import _register_npcs_export
from lilitools.cli.lilitool.commands.npc.fetishes import _register_npcs_fetishes
from lilitools.cli.lilitool.commands.npc.list import _register_npcs_list
from lilitools.cli.lilitool.commands.npc.show import _register_npcs_show


def register_npc(subp: argparse._SubParsersAction) -> None:
    savep = subp.add_parser('npc', aliases=['npcs']).add_subparsers()

    _register_npcs_body(savep)
    _register_npcs_clean(savep)
    _register_npcs_export(savep)
    _register_npcs_fetishes(savep)
    _register_npcs_list(savep)
    _register_npcs_show(savep)

import argparse
import sys
from typing import Tuple

from lilitools.cli.utils import existingFilePath
from lilitools.logging import ClickWrap
from lilitools.saves.character.npc import NPC
from lilitools.saves.game import Game
from lilitools.saves.utils import loadSaveFrom

click = ClickWrap()

__all__ = ['add_npc_args', 'handle_npc_args']

def add_npc_args(p: argparse.ArgumentParser) -> None:
    p.add_argument('path', type=existingFilePath, help='Path to the save XML')
    p.add_argument('--npc-id', type=str, help='ID of the NPC (e.g. "--npc-id=-1,FortressDemonLeader")')

def handle_npc_args(args: argparse.Namespace) -> Tuple[Game, NPC]:
    game = loadSaveFrom(args.path)
    npc = game.getNPCById(args.npc_id)
    if npc is None:
        click.secho(f'NPC {args.npc_id!r} not found. Set --npc-id', err=True, fg='red')
        sys.exit(1)
    return game, npc

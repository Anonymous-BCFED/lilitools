

import argparse
from .core import _register_npcs_body_core

def _register_npcs_body(subp: argparse._SubParsersAction) -> None:
    p = subp.add_parser('body')
    
    bp = p.add_subparsers()
    _register_npcs_body_core(bp)


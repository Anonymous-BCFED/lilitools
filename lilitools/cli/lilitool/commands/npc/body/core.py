

import argparse
from typing import Dict, Optional
from lilitools.cli.lilitool.commands.npc._utils import handle_npc_args, add_npc_args
from lilitools.cli.utils import arg2enum, fuzzybool
from lilitools.saves.character.enums.occupation import EOccupation
from lilitools.saves.character.enums.combat_behaviour import ECombatBehaviour
from lilitools.logging import ClickWrap
from lilitools.saves.character.npc import NPC
from lilitools.saves.utils import saveToFile

click = ClickWrap()

def _register_npcs_body_core(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser('core')

    add_npc_args(p)

    # Lines with a hash after them denote that it has a supporting line in cmd_npc_core.
    # Just in case I forget to remove them when this is done.
    bdp = p.add_argument_group('Birthday')
    bdp.add_argument('--set-birth-day', type=int, default=None)#
    bdp.add_argument('--set-birth-month', type=int, default=None)#
    bdp.add_argument('--set-birth-year', type=int, default=None)#

    fnp = p.add_argument_group('First Name')
    fnp.add_argument('--set-name-all', type=str, default=None, help='Set all three names')#
    fnp.add_argument('--set-name-and', type=str, default=None, help='Set androgynous first name')#
    fnp.add_argument('--set-name-fem', type=str, default=None, help='Set feminine first name')#
    fnp.add_argument('--set-name-mas', type=str, default=None, help='Set masculine first name')#

    ep = p.add_argument_group('Elemental')
    ep.add_argument('--set-elemental-id', type=str, default=None)#
    ep.add_argument('--unset-elemental-id', action='store_true', default=False)#
    ep.add_argument('--set-elemental-summoned', type=fuzzybool, default=None)#

    djp = p.add_argument_group('Desired Jobs')
    djp.add_argument('--add-desired-job', type=str, nargs='*', default=[])#
    djp.add_argument('--rm-desired-job', type=str, nargs='*', default=[])#
    djp.add_argument('--clear-desired-jobs', action='store_true', default=False)#

    p.add_argument('--set-age-appearance-difference', type=int, default=None)#
    p.add_argument('--set-captive', type=fuzzybool, default=None)#
    p.add_argument('--set-combat-behaviour', type=arg2enum(ECombatBehaviour), default=None)#
    p.add_argument('--set-description', type=str, default=None)#
    p.add_argument('--set-first-name-terms', type=fuzzybool, default=None)#
    p.add_argument('--set-generic-name', type=str, default=None, help='Set the generic name of an NPC')#
    p.add_argument('--unset-generic-name', action='store_true', default=False, help='Set the generic name of an NPC to null (None)')#
    p.add_argument('--set-history', type=arg2enum(EOccupation), default=None)#
    p.add_argument('--set-last-time-had-sex', type=int, default=None)#
    p.add_argument('--set-last-time-orgasmed', type=int, default=None)#
    p.add_argument('--set-level', type=int, default=None)#
    p.add_argument('--set-player-knows-name', type=fuzzybool, default=None)#
    p.add_argument('--set-race-concealed', type=fuzzybool, default=None)#
    p.add_argument('--set-speech-colour', type=str, default=None)
    p.add_argument('--unset-speech-colour', action='store_true', default=False)
    p.add_argument('--set-surname', type=str, default=None)#

    p.add_argument('--dry-run', action='store_true', default=False, help="Don't actually do anything.")#
    
    p.set_defaults(cmd=cmd_npc_core)

def cmd_npc_core(args: argparse.Namespace) -> None:
    game, npc = handle_npc_args(args)
    
    nchanges: int = 0

    nchanges += _cmd_npc_core__handle_birthday(args, npc)
    nchanges += _cmd_npc_core__handle_desired_jobs(args, npc)
    nchanges += _cmd_npc_core__handle_elemental(args, npc)
    nchanges += _cmd_npc_core__handle_name(args, npc)

    if args.set_description is not None:
        orig = npc.description
        npc.description = args.set_description
        click.secho(f'Description: {orig!r} -> {npc.description!r}')
        nchanges += 1

    if args.set_first_name_terms is not None:
        click.secho(f'Player On First Name Terms: {npc.player_on_first_name_terms!r} -> {args.set_first_name_terms!r}')
        npc.player_on_first_name_terms = args.set_first_name_terms
        nchanges += 1

    if args.set_generic_name is not None:
        click.secho(f'Generic Name: {npc.generic_name!r} -> {args.set_generic_name!r}')
        npc.generic_name = args.set_generic_name
        nchanges += 1
    elif args.unset_generic_name:
        click.secho(f'Generic Name: {npc.generic_name!r} -> \'\'')
        npc.generic_name = ''
        nchanges += 1

    if args.set_age_appearance_difference is not None:
        click.secho(f'Age Appearance Difference: {npc.age_appearance_difference!r} -> {args.set_age_appearance_difference!r}')
        npc.age_appearance_difference = args.set_age_appearance_difference
        nchanges += 1
        
    if args.set_captive is not None:
        click.secho(f'Captive: {npc.captive!r} -> {args.set_captive!r}')
        npc.captive = args.set_captive
        nchanges += 1

    if args.set_history is not None:
        click.secho(f'History: {npc.history!r} -> {args.set_history!r}')
        npc.history = args.set_history
        nchanges += 1

    if args.set_level is not None:
        click.secho(f'Level: {npc.level!r} -> {args.set_level!r}')
        npc.level = args.set_level
        nchanges += 1

    if args.set_player_knows_name is not None:
        click.secho(f'Player Knows Name: {npc.player_knows_name!r} -> {args.set_player_knows_name!r}')
        npc.player_knows_name = args.set_player_knows_name
        nchanges += 1

    if args.set_race_concealed is not None:
        click.secho(f'Race Concealed: {npc.race_concealed!r} -> {args.set_race_concealed!r}')
        click.warn(f'Setting race_concealed will likely not do anything for most characters.')
        npc.race_concealed = args.set_race_concealed
        nchanges += 1

    if args.set_surname is not None:
        click.secho(f'Surname: {npc.surname!r} -> {args.set_surname!r}')
        npc.surname = args.set_surname
        nchanges += 1

    if args.set_combat_behaviour is not None:
        click.secho(f'Combat Behaviour: {npc.combat_behaviour!r} -> {args.set_combat_behaviour!r}')
        npc.combat_behaviour = args.set_combat_behaviour
        nchanges += 1

    if args.set_last_time_had_sex is not None:
        click.secho(f'Last Time Had Sex: {npc.last_time_had_sex!r} -> {args.set_last_time_had_sex!r}')
        npc.last_time_had_sex = args.set_last_time_had_sex
        nchanges += 1

    if args.set_last_time_orgasmed is not None:
        click.secho(f'Last Time Orgasmed: {npc.last_time_orgasmed!r} -> {args.set_last_time_orgasmed!r}')
        npc.last_time_orgasmed = args.set_last_time_orgasmed
        nchanges += 1

    if args.set_speech_colour is not None:
        click.secho(f'Speech Colour: {npc.speech_colour!r} -> {args.set_speech_colour!r}')
        click.warn(f'Setting speech_colour will likely not do anything for most characters.')
        npc.speech_colour = args.set_speech_colour
        nchanges += 1
    elif args.unset_speech_colour:
        click.secho(f'Speech Colour: {npc.speech_colour!r} -> None')
        click.warn(f'Setting speech_colour will likely not do anything for most characters.')
        npc.speech_colour = ''
        nchanges += 1

    if nchanges == 0:
        click.secho(f'No changes made. exiting.')
        return

    if args.dry_run:
        click.warn('You specified --dry-run, exiting before we save anything.')
        return
    
    saveToFile(game, args.path)
    

def _cmd_npc_core__handle_desired_jobs(args: argparse.Namespace, npc: NPC) -> int:
    nchanges: int = 0
    job: str
    j: EOccupation
    if not args.clear_desired_jobs and len(args.rm_desired_job) == 0 and len(args.add_desired_job) == 0:
        return 0
    with click.echo('Desired Jobs:'):
        if args.clear_desired_jobs:
            for j in npc.desiredJobs:
                click.echo(f'- {j!r}')
                nchanges += 1
            npc.desiredJobs.clear()
        else:
            for job in args.rm_desired_job:
                j = EOccupation[job]
                if j in npc.desiredJobs:
                    npc.desiredJobs.remove(j)
                    click.echo(f'- {j!r}')
                    nchanges += 1
        for job in args.add_desired_job:
            j = EOccupation[job]
            if j not in npc.desiredJobs:
                npc.desiredJobs.add(j)
                click.echo(f'+ {j!r}')
                nchanges += 1
    return nchanges

def _cmd_npc_core__handle_elemental(args: argparse.Namespace, npc: NPC) -> int:
    nchanges: int = 0
    if args.set_elemental_id is not None:
        click.secho(f'Elemental ID: {npc.elementalID!r} -> {args.set_elemental_id!r}')
        npc.elementalID = args.set_elemental_id
        nchanges += 1
    elif args.unset_elemental_id:
        click.secho(f'Elemental ID: {npc.elementalID!r} -> \'\'')
        npc.elementalID = ''
        nchanges += 1
        if npc.elementalSummoned:
            click.secho(f'Elemental Summoned: {npc.elementalSummoned!r} -> False')
            npc.elementalSummoned = False
            nchanges += 1
        return nchanges
    if args.set_elemental_summoned is not None:
        click.secho(f'Elemental Summoned: {npc.elementalSummoned!r} -> {args.set_elemental_summoned!r}')
        npc.elementalSummoned = args.set_elemental_summoned
        nchanges += 1
    return nchanges

def _cmd_npc_core__handle_birthday(args: argparse.Namespace, npc: NPC) -> int:
    changes: Dict[str, int] = {}
    if args.set_birth_year is not None:
        changes['year'] = args.set_birth_year
    if args.set_birth_month is not None:
        changes['month'] = args.set_birth_month
    if args.set_birth_day is not None:
        changes['day'] = args.set_birth_day
    if len(changes) > 0:
        origbday = npc.birthday
        npc.birthday = npc.birthday.replace(**changes)
        click.secho(f'Birthday: {origbday} -> {npc.birthday}')
        return 1
    return 0


def _cmd_npc_core__handle_name(args: argparse.Namespace, npc: NPC) -> int:
    changes: Dict[str, str] = {}
    nchanges: int = 0
    if args.set_name_all is not None:
        changes = {
            'a': args.set_name_all,
            'f': args.set_name_all,
            'm': args.set_name_all,
        }
    else:
        if args.set_name_and is not None:
            changes['a'] = args.set_name_and
        if args.set_name_fem is not None:
            changes['f'] = args.set_name_fem
        if args.set_name_mas is not None:
            changes['m'] = args.set_name_mas
    if 'a' in changes:
        click.secho(f'Androgynous Name: {npc.name.androgynous!r} -> {changes["a"]!r}')
        npc.name.androgynous = changes['a']
        nchanges += 1
    if 'f' in changes:
        click.secho(f'Feminine Name: {npc.name.feminine!r} -> {changes["f"]!r}')
        npc.name.feminine = changes['f']
        nchanges += 1
    if 'm' in changes:
        click.secho(f'Masculine Name: {npc.name.masculine!r} -> {changes["m"]!r}')
        npc.name.masculine = changes['m']
        nchanges += 1
    return nchanges

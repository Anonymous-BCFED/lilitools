import argparse
import random
from typing import Dict, FrozenSet, List, Set

from lilitools.logging import ClickWrap

from lilitools.game.enums.known_npc_pathnames import KnownNPCPathNames
from lilitools.saves.character.npc import NPC

from lilitools.saves.utils import loadSaveFrom, saveToFile
from lilitools.saves.world.cell import Cell
from lilitools.saves.world.enums.place_type import EPlaceType
click = ClickWrap()

__all__ = ['_register_npcs_clean']

HISTORY_PROSTITUTE: str = 'NPC_PROSTITUTE'
PROSTITUTE_SPOTS: FrozenSet[EPlaceType] = frozenset({
    EPlaceType.DOMINION_BACK_ALLEYS,
    EPlaceType.DOMINION_ALLEYS_CANAL_CROSSING,
    EPlaceType.DOMINION_CANAL,
    EPlaceType.DOMINION_DARK_ALLEYS
})


def _register_npcs_clean(subp: argparse._SubParsersAction) -> None:
    argp = subp.add_parser('clean')
    argp.add_argument('path', type=str, help='Path to the save XML')
    argp.add_argument('--prostitutes', action='store_true', default=False)
    argp.add_argument('--muggers', action='store_true', default=False)
    argp.add_argument('--remove-all', action='store_true', default=False)
    argp.add_argument('--remove-percentage', type=float, default=None)
    argp.add_argument('--shuffle', action='store_true', default=False)
    argp.add_argument('--dry-run', action='store_true', default=False)
    argp.set_defaults(cmd=cmd_npcs_clean)


def cmd_npcs_clean(args: argparse.Namespace) -> None:
    # changed: bool = False
    game = loadSaveFrom(args.path)
    valid_prostitute_cells: Dict[str, List[Cell]] = {}
    fspots = 0
    npcsByCell: Dict[Cell, List[NPC]] = {}
    prostitutes: Set[NPC] = set()
    muggers: Set[NPC] = set()
    for cell, world in game.worlds.worlds.items():
        for cell in world.cells:
            #print(cell.place.type)
            if cell.place.type in EPlaceType._member_names_ and EPlaceType[cell.place.type] in PROSTITUTE_SPOTS:
                wid = cell.worldID
                if wid not in valid_prostitute_cells.keys():
                    valid_prostitute_cells[wid] = [cell]
                else:
                    valid_prostitute_cells[wid].append(cell)
                if cell not in npcsByCell.keys():
                    npcsByCell[cell] = []
                fspots += 1
    #print(fspots)
    removed: int = 0
    selectedNPCs: Set[NPC] = set()
    for npc in game.npcs:
        cell = npc.locationInformation.asCell(game)
        assert cell
        #cell = game.worlds.getCellByWorldCoords(npc.locationInformation.worldLocation, npc.locationInformation.location.x, npc.locationInformation.location.y)
        if npc.core.pathName in KnownNPCPathNames.POSSIBLE_RNG_PROSTITUTE_PATHNAMES and not npc.slavery.isSlave():
            match npc.core.history:
                case 'NPC_PROSTITUTE':
                    prostitutes.add(npc)
                    if args.prostitutes:
                        selectedNPCs.add(npc)
                case 'NPC_MUGGER':
                    muggers.add(npc)
                    if args.muggers:
                        selectedNPCs.add(npc)
            if args.remove_all and ((args.muggers and npc in muggers) or (args.prostitutes and npc in prostitutes)):
                game.removeNPC(npc)
                click.success(f'Removed {npc.core.id} ({npc.getName()})')
                removed += 1
                # changed = True
                continue
            if cell not in npcsByCell.keys():
                npcsByCell[cell] = [npc]
            else:
                npcsByCell[cell].append(npc)

    click.info(f'Found {len(prostitutes)} prostitutes, {len(muggers)} muggers')
    if args.remove_all:
        click.success(f'Removed {removed:,} NPCs. (')
        return

    if args.remove_percentage is not None:
        snpcs = list(selectedNPCs)
        random.shuffle(snpcs)
        remove_frac = float(args.remove_percentage)
        n2nuke = round(float(len(snpcs)) * remove_frac)
        actual_percentage = (float(n2nuke) / float(len(selectedNPCs))) * 100.
        with click.info(f'Removing {actual_percentage:.01f}% of {len(selectedNPCs)} NPCs ({n2nuke})...'):
            for _ in range(n2nuke):
                npc = snpcs.pop()
                game.removeNPC(npc)
                click.success(f'Removed {npc.core.id} ({npc.getName()})')
                removed += 1
                # changed = True
            selectedNPCs = set(snpcs)

    if args.shuffle:
        for cell, coll in npcsByCell.items():
            for npc in coll:
                if npc not in selectedNPCs:
                    continue
                if cell.worldID not in valid_prostitute_cells:
                    continue
                cells = valid_prostitute_cells[cell.worldID]
                if len(cells) > 0:
                    random.shuffle(cells)
                    oldcell = npc.locationInformation.asCell(game)
                    newcell = cells.pop()
                    npc.locationInformation.moveToCell(newcell)
                    click.success(f'Moved {npc.core.id}: {oldcell.getName()} -> {newcell.getName()}')
                    # changed = True
    click.success(f'Removed {removed} NPCs.')
    if args.dry_run:
        click.warn('You specified --dry-run, exiting before we save anything.')
        return

    saveToFile(game, args.path, quiet=False)

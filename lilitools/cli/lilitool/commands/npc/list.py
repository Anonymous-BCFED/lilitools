import argparse
from typing import Any, Callable, Optional, Set

import click
import tabulate
from tabulate import tabulate_formats
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.utils import loadSaveFrom
__all__ = ['_register_npcs_list']

def _register_npcs_list(subp: argparse._SubParsersAction) -> None:
    argp = subp.add_parser('list')
    argp.add_argument('path', type=str, help='Path to the save XML')
    argp.add_argument('--sort-by', choices=['none', 'id', 'name', 'surname'], default=None)
    argp.add_argument('--table-format', choices=tabulate_formats, default='github')
    argp.set_defaults(cmd=cmd_npcs_list)


def cmd_npcs_list(args: argparse.Namespace) -> None:
    game = loadSaveFrom(args.path)
    o = []
    sortmethod: Optional[Callable[[GameCharacter], Any]] = None
    if args.sort_by is None or args.sort_by == 'none':
        click.echo('NPCs, in save order:')
    elif args.sort_by == 'id':
        click.echo('NPCs, ordered by ID:')
        sortmethod = lambda x: x.core.id
    elif args.sort_by == 'name':
        click.echo('NPCs, ordered by first name:')
        sortmethod = lambda x: x.getName()
    elif args.sort_by == 'surname':
        click.echo('NPCs, ordered by surname:')
        sortmethod = lambda x: x.core.surname
    
    npcs = list(game.npcs)
    if sortmethod is not None:
        npcs.sort(key=sortmethod)
        
    header = ['ID', 'Name', 'Surname', 'Location']
    for npc in npcs:
        locstr = f'{npc.locationInformation.worldLocation}@({npc.locationInformation.location.x},{npc.locationInformation.location.y})'
        o.append([npc.core.id, npc.getName(), npc.core.surname, locstr])
    click.echo(tabulate.tabulate(o, headers=header, tablefmt=args.table_format))

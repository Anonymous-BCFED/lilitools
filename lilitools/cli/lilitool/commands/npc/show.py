import argparse

from lilitools.logging import ClickWrap

from ._utils import add_npc_args, handle_npc_args

__all__ = ['_register_npcs_show']

click = ClickWrap()


def _register_npcs_show(subp: argparse._SubParsersAction) -> None:
    p = subp.add_parser('show')
    add_npc_args(p)
    p.set_defaults(cmd=cmd_npc_show)


def cmd_npc_show(args: argparse.Namespace) -> None:
    _, npc = handle_npc_args(args)
    npc.show()

import argparse

from lilitools.cli.lilitool.commands.player.body import _register_player_body
from lilitools.cli.lilitool.commands.player.export import _register_player_export
from lilitools.cli.lilitool.commands.player.fetishes import _register_player_fetishes
from lilitools.cli.lilitool.commands.player.perks import _register_player_perks
from lilitools.cli.lilitool.commands.player.show import _register_player_show

# from lilitools.cli.lilitool.cmd.player.inventory import register_player_inventory


def register_player(subp: argparse._SubParsersAction) -> None:
    p = subp.add_parser("player", aliases=["pc"]).add_subparsers()

    # register_player_inventory(p)
    _register_player_body(p)
    _register_player_fetishes(p)
    _register_player_perks(p)
    _register_player_show(p)
    _register_player_export(p)

import argparse
from typing import Tuple

from lilitools.cli.utils import existingFilePath
from lilitools.logging import ClickWrap
from lilitools.saves.character.player_character import PlayerCharacter
from lilitools.saves.game import Game
from lilitools.saves.utils import loadSaveFrom

click = ClickWrap()

__all__ = ['add_player_args', 'handle_player_args']

def add_player_args(p: argparse.ArgumentParser) -> None:
    p.add_argument('path', type=existingFilePath, help='Path to the save XML')

def handle_player_args(args: argparse.Namespace) -> Tuple[Game, PlayerCharacter]:
    game = loadSaveFrom(args.path)
    pc = game.playerCharacter
    if pc is None:
        click.critical(f'Player character not found.')
    return game, pc

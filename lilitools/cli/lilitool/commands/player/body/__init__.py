import argparse

from .antenna import _register_player_body_antenna
from .core import _register_player_body_core

__all__ = ["_register_player_body"]


def _register_player_body(subp: argparse._SubParsersAction) -> None:
    p = subp.add_parser("body", help="Modify player body components.")

    bp = p.add_subparsers()

    _register_player_body_core(bp)
    _register_player_body_antenna(bp)

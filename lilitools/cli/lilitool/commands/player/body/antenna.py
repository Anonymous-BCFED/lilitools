import argparse

from lilitools.cli.lilitool.commands.player._utils import add_player_args, handle_player_args
from lilitools.cli.units import UNITS, cm2len, handle_length_as_cm, register_length
from lilitools.cli.utils import checkedInt
from lilitools.logging import ClickWrap
from lilitools.saves.utils import saveToFile

click = ClickWrap()
__all__ = ["_register_player_body_antenna"]


def _register_player_body_antenna(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser("antenna", aliases=["antennae"])

    add_player_args(p)

    bap = p.add_argument_group(
        "Actions", "Slightly more automated solutions to common problems."
    )
    bap.add_argument(
        "--remove", action="store_true", default=False, help="Remove all antennae."
    )

    vp = p.add_argument_group("Values", "Set values by hand.")
    vp.add_argument("--set-type", type=str, default=None, help="What type of antennae?")
    vp.add_argument(
        "--set-rows",
        type=checkedInt("--set-rows", minval=1, maxval=4),
        default=None,
        help="How many rows of antennae?",
    )
    register_length(vp, "set-length", "antenna length", UNITS.cm)
    vp.add_argument(
        "--set-antennae-per-row",
        type=checkedInt("--set-antennae-per-row", minval=1),
        default=None,
        help="How many antennae per row?",
    )

    p.add_argument(
        "--dry-run",
        action="store_true",
        default=False,
        help="Don't actually do anything.",
    )

    p.set_defaults(cmd=cmd_player_body_antenna)


def cmd_player_body_antenna(args: argparse.Namespace) -> None:
    game, npc = handle_player_args(args)

    # self.type: str = ''
    origType: str = npc.body.antennae.type
    # self.rows: int = 0
    origRows: str = npc.body.antennae.rows
    # self.length: int = 0
    origLength: str = npc.body.antennae.length
    # self.antennaePerRow: int = 0
    origAntennaePerRow: str = npc.body.antennae.antennaePerRow

    def anyChanges() -> bool:
        if origType != npc.body.antennae.type:
            return True
        if origRows != npc.body.antennae.rows:
            return True
        if origLength != npc.body.antennae.length:
            return True
        if origAntennaePerRow != npc.body.antennae.antennaePerRow:
            return True
        return False

    def displayChanges() -> None:
        if origType != npc.body.antennae.type:
            click.secho(f"Type: {origType!r} -> {npc.body.antennae.type!r}")
        if origRows != npc.body.antennae.rows:
            click.secho(f"Rows: {origRows!r} -> {npc.body.antennae.rows!r}")
        if origLength != npc.body.antennae.length:
            click.secho(
                f"Length: {cm2len(origLength)} -> {cm2len(npc.body.antennae.length)}"
            )
        if origAntennaePerRow != npc.body.antennae.antennaePerRow:
            click.secho(
                f"Antennae Per Row: {origAntennaePerRow!r} -> {npc.body.antennae.antennaePerRow!r}"
            )

    if args.remove is True:
        with click.secho("Removing all antennae:"):
            # Remove all antennae
            npc.body.antennae.type = "NONE"
            npc.body.antennae.rows = 1
            npc.body.antennae.length = 0  # HornLength.TINY.min
            npc.body.antennae.antennaePerRow = 2
            displayChanges()

    if args.set_type is not None:
        npc.body.antennae.type = args.set_type
        if origType != npc.body.antennae.type:
            click.secho(f"Type: {origType!r} -> {npc.body.antennae.type!r}")
    if args.set_rows is not None:
        npc.body.antennae.rows = args.rows
        if origRows != npc.body.antennae.rows:
            click.secho(f"Rows: {origRows!r} -> {npc.body.antennae.rows!r}")
    length = handle_length_as_cm(args, "set_length")
    if length is not None:
        npc.body.antennae.length = length
        if origLength != npc.body.antennae.length:
            click.secho(
                f"Length: {cm2len(origLength)} -> {cm2len(npc.body.antennae.length)}"
            )
    if args.set_antennae_per_row is not None:
        npc.body.antennae.antennaePerRow = args.set_antennae_per_row
        if origAntennaePerRow != npc.body.antennae.antennaePerRow:
            click.secho(
                f"Antennae Per Row: {origAntennaePerRow!r} -> {npc.body.antennae.antennaePerRow!r}"
            )

    if not anyChanges():
        click.secho(f"No changes made. exiting.")
        return

    if args.dry_run:
        click.warn("You specified --dry-run, exiting before we save anything.")
        return

    saveToFile(game, args.path)

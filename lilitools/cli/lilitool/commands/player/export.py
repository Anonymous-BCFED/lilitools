import argparse
from pathlib import Path
from typing import Optional

from lilitools.cli.lilitool.commands.player._utils import (
    add_player_args,
    handle_player_args,
)
from lilitools.logging import ClickWrap
from lilitools.saves.character.exported_character import ExportedCharacter
from lilitools.saves.character.transformation_preset import TransformationPreset
from lilitools.saves.utils import writeRootToFile

click = ClickWrap()

__all__ = ["_register_player_export"]


def _register_player_export(subp: argparse._SubParsersAction) -> None:
    argp: argparse.ArgumentParser = subp.add_parser(
        "export",
        description="Converts the player character into a character export or transformation preset.",
    )
    add_player_args(argp)
    argp.add_argument(
        "--as-character-export",
        "--as-char-export",
        type=Path,
        default=None,
        help="Path of the resulting character export",
    )
    argp.add_argument(
        "--as-transformation-preset",
        "--as-tf-preset",
        type=Path,
        default=None,
        help="Path of the resulting transformation preset",
    )
    argp.set_defaults(cmd=cmd_player_export)


def cmd_player_export(args: argparse.Namespace) -> None:
    char_export_path: Optional[Path] = args.as_character_export
    tf_preset_path: Optional[Path] = args.as_transformation_preset
    if tf_preset_path is None and char_export_path is None:
        click.error(
            "Need --as-character-export or --as-transformation-preset. Neither are present"
        )
        return

    _, pc = handle_player_args(args)

    if char_export_path is not None:
        with click.info(
            f"Creating exported character from player {pc.getName()} ({pc.core.id})..."
        ):
            export = ExportedCharacter.FromCharacter(pc)
            writeRootToFile((char_export_path), export.toXML(), quiet=False)

    if tf_preset_path is not None:
        with click.info(
            f"Creating tf preset from player {pc.getName()} ({pc.core.id})..."
        ):
            tfp = TransformationPreset.FromCharacter(pc)
            writeRootToFile((tf_preset_path), tfp.toXML(), quiet=False)

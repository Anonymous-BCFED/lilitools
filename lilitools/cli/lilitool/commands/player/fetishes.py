import argparse
from typing import Dict, List, Optional, Set

from lilitools.cli.utils import arg2enum, fuzzybool
from lilitools.logging import ClickWrap
from lilitools.saves.character.enums.fetish_desire import EFetishDesire
from lilitools.saves.character.fetish_entry import FetishEntry
from lilitools.saves.fetishlist import getAllKnownFetishes
from lilitools.saves.utils import saveToFile
from lilitools.utils import bool2yesno

from ._utils import add_player_args, handle_player_args

click = ClickWrap()

__all__ = ['_register_player_fetishes']


def _register_player_fetishes(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser('fetishes', aliases=['fetish'])
    add_player_args(p)
    p.add_argument('fetish_ids', type=str, nargs='*', default=[])
    p.add_argument('--all-known-fetishes', action='store_true', default=False, help='All fetishes known to the editor')
    p.add_argument('--all-player-fetishes', action='store_true', default=False, help='All fetishes known to the PC, liked or disliked')

    p.add_argument('--list', action='store_true', default=False)
    p.add_argument('--list-by-desire', action='store_true', default=False)
    p.add_argument('--dry-run', action='store_true', default=False, help='Do not save after modifying.')
    p.add_argument('--reset', action='store_true', default=False, help='Reset all desires to 0 xp, non-fetish, and NEUTRAL desire')

    p.add_argument('--set-desire', type=arg2enum(EFetishDesire), default=None)
    p.add_argument('--set-xp', type=int, default=None)
    p.add_argument('--set-is-fetish', type=fuzzybool, default=None)

    p.set_defaults(cmd=cmd_player_fetishes)


def cmd_player_fetishes(args: argparse.Namespace) -> None:
    g, pc = handle_player_args(args)

    selected_fetishes: List[str] = args.fetish_ids
    if args.all_known_fetishes:
        selected_fetishes = sorted(getAllKnownFetishes())
    elif args.all_player_fetishes:
        selected_fetishes = sorted((x.id for x in pc.fetishes.values()))
    list_only: bool = args.list

    reset: bool = args.reset
    list_only: bool = args.list
    list_by_desire: bool = args.list_by_desire

    desire: Optional[EFetishDesire] = args.set_desire
    xp: Optional[int] = args.set_xp
    is_fetish: Optional[bool] = args.set_is_fetish

    if list_only or list_by_desire:
        fetishlist = sorted(pc.fetishes.values(), key=lambda x: x.id)
        with click.info(f'[{pc.core.id}] {pc.getName()}:'):
            if list_only:
                for fetish in fetishlist:
                    click.info(f'* {fetish}')
            else:
                fetishdesires: Dict[EFetishDesire, List[FetishEntry]] = {}
                for fetish in fetishlist:
                    actual_desire: EFetishDesire = fetish.desire
                    if fetish.hasFetish and fetish.desire != EFetishDesire.FOUR_LOVE:
                        actual_desire = EFetishDesire.FOUR_LOVE
                        click.warn(f'Fetish {fetish.id} is set to desire {fetish.desire} when it should be forced to {actual_desire}.')
                    if fetish.desire not in fetishdesires.keys():
                        fetishdesires[fetish.desire] = [fetish]
                    else:
                        fetishdesires[fetish.desire].append(fetish)
                for desire in EFetishDesire:
                    fetishes = fetishdesires.get(desire, [])
                    if len(fetishes) > 0:
                        with click.info(f'{desire.name} ({desire.value}):'):
                            for fetish in fetishes:
                                click.info(f'* {fetish}')
        return
    #print(repr(fetishids))
    change = False
    with click.info(f'[{pc.core.id}] {pc.getName()}:'):
        for fetishid in selected_fetishes:
            fetish = pc.getFetishEntry(fetishid)
            changes = []
            old_has_fetish = fetish.hasFetish
            old_xp = fetish.xp
            old_desire = fetish.desire
            if reset:
                fetish.desire = EFetishDesire.TWO_NEUTRAL
                fetish.hasFetish = False
                fetish.xp = 0
            if desire is not None:
                fetish.desire = desire
            if xp is not None:
                fetish.xp = xp
            if is_fetish is not None:
                fetish.hasFetish = is_fetish
            if old_desire != fetish.desire:
                changes.append(f'Desire: {old_desire.name} -> {fetish.desire.name}')
            if old_has_fetish != fetish.hasFetish:
                changes.append(f'Is Fetish: {bool2yesno(old_has_fetish)} -> {bool2yesno(fetish.hasFetish)}')
            if old_xp != fetish.xp:
                changes.append(f'XP: {old_xp:,} -> {fetish.xp:,}')
            if len(changes) > 0:
                with click.info(f'{fetish.id}:'):
                    for chg in changes:
                        click.info(chg)
                change = True
    if args.dry_run:
        click.warn('You specified --dry-run, exiting before we save anything.')
        return
    if not change:
        click.warn('No changes.')
        return
    saveToFile(g, args.path, quiet=False)

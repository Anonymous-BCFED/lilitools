import argparse
import difflib
from typing import List, Optional, Set
from lilitools.cli.utils import arg2enum, fuzzybool

from lilitools.logging import ClickWrap
from lilitools.saves.character.enums.known_perks import EKnownPerks
from lilitools.saves.utils import saveToFile

from ._utils import add_player_args, handle_player_args
from click import style
click = ClickWrap()

__all__ = ['_register_player_perks']


def _register_player_perks(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser('perks', aliases=['perk'])
    add_player_args(p)
    p.add_argument('--clear', action='store_true', default=False)
    p.add_argument('--list', action='store_true', default=False)
    p.add_argument('--dry-run', action='store_true', default=False)
    p.add_argument('--add', type=str, nargs='*', default=None)
    p.add_argument('--remove', type=str, nargs='*', default=None)
    p.set_defaults(cmd=cmd_player_perks)


def cmd_player_perks(args: argparse.Namespace) -> None:
    g, pc = handle_player_args(args)

    clear: bool = args.clear
    list_only: bool = args.list
    addperks: Optional[Set[str]] = set(args.add) if args.add is not None else None
    rmperks: Optional[Set[str]] = set(args.remove) if args.remove is not None else None
    #print(repr(fetishids))
    change = False
    with click.info(f'[{pc.core.id}] {pc.getName()}:'):
        if list_only:
            for perkRow, perks in sorted(pc.perks.items()):
                for perk in perks:
                    click.info(f'* {perk},{perkRow}')
            return
        else:
            before = pc.allPerks.copy()
            if addperks is not None:
                for perk in addperks:
                    if ',' in perk:
                        perkName, perkRow = perk.split(',')
                        if pc.hasPerk(perkName):
                            click.warn(f'This character already has perk {perkName!r}!')
                        else:
                            pc.addPerk(perkName, int(perkRow))
                            click.success(f'Added {perkName},{perkRow}')
                            change = True
                    else:
                        if perk not in EKnownPerks._member_names_:
                            click.critical(f'Perk {perk!r} is not a known perk (see EKnownPerks), nor was the row specified.')
                        row = EKnownPerks[perk.upper()].row
                        click.warn(f'In the future, please specify the perk\'s row.  In this case: {perk},{row}')
                        if pc.hasPerk(perk):
                            click.warn(f'This character already has perk {perk!r}!')
                        else:
                            pc.addPerk(perk, row)
                            click.success(f'Added {perk},{row}')
                            change = True
            if rmperks is not None:
                for perk in rmperks:
                    if ',' in perk:
                        perkName, perkRow = perk.split(',')
                        click.warn(f'Row not needed. (Use `--remove {perkName!r}` instead.) Proceeding anyway...')
                        if not pc.hasPerk(perk):
                            click.warn(f'This character does not possess perk {perk!r}!')
                        else:
                            pc.rmPerk(perkName)
                            click.success(f'Removed {perkName}')
                            change = True
                    else:
                        if not pc.hasPerk(perk):
                            click.warn(f'This character does not possess perk {perk!r}!')
                        else:
                            pc.rmPerk(perk)
                            click.success(f'Removed {perk}')
                            change = True
        if change:
            for l in difflib.unified_diff(sorted(before), sorted(pc.allPerks), 'before', 'after'):
                click.info(l)
    if args.dry_run:
        click.warn('You specified --dry-run, exiting before we save anything.')
        return
    if not change:
        click.warn('No changes.')
        return
    saveToFile(g, args.path)

import argparse

from lilitools.logging import ClickWrap

from ._utils import add_player_args, handle_player_args

__all__ = ['_register_player_show']

click = ClickWrap()


def _register_player_show(subp: argparse._SubParsersAction) -> None:
    p = subp.add_parser('show')
    add_player_args(p)
    p.set_defaults(cmd=cmd_player_show)


def cmd_player_show(args: argparse.Namespace) -> None:
    _, pc = handle_player_args(args)
    pc.show()

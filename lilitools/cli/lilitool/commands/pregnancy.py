import argparse
from typing import Optional

from lilitools.cli.utils import bool2yn_colored, existingFilePath
from lilitools.game.enums.known_status_effect import EKnownStatusEffect
from lilitools.logging import ClickWrap
from lilitools.saves.character.enums.attribute import EAttribute
from lilitools.saves.character.game_character import PREGNANCY_STATUS_EFFECTS, GameCharacter
from lilitools.saves.game import Game
from lilitools.saves.utils import loadSaveFrom, saveToFile

click = ClickWrap()


def register_pregnancy(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser('pregnancy', description="Modify pregnancy parameters of a FICTIONAL CHARACTER (calm down texas jeez).")

    p.add_argument('savefile', metavar='savefile.xml', type=existingFilePath)
    p.add_argument('--erase', action='store_true', default=False, help='Shift into a different timeline where this never happened.')
    p.add_argument('--dry-run', action='store_true', default=False)
    p.add_argument('--possibility', action='store_true', default=False)

    pwho = p.add_mutually_exclusive_group()
    pwho.add_argument('--player', action='store_true', default=False)
    pwho.add_argument('--npc-id', type=str, default=None)
    pwho.title = 'Whomst'
    pwho.description = 'Who is the subject of this command'

    p.set_defaults(cmd=cmd_pregnancy)


def cmd_pregnancy(args: argparse.Namespace) -> None:
    g: Game = loadSaveFrom(args.savefile)
    gc: Optional[GameCharacter] = None
    if args.player:
        gc = g.playerCharacter
    elif args.npc_id is not None:
        gc = g.getNPCById(args.npc_id)
        assert gc is not None, 'Could not find that NPC. See `npc list`.'
    assert gc is not None, 'You MUST specify either --player or --npc-id.'

    with click.info('Stages:'):
        for sfx in sorted(PREGNANCY_STATUS_EFFECTS | {EKnownStatusEffect.PREGNANT_0.name}):
            if gc.hasStatusEffect(sfx):
                click.info(f'* {sfx}')

    with click.info('Pregnancy:'):
        if gc.pregnancy is None:
            click.info('None')
        else:
            with click.info('Litter:'):
                if gc.pregnancy.pregnantLitter is None:
                    click.info('None')
                else:
                    litter = gc.pregnancy.pregnantLitter
                    click.info(f'Conceived: {litter.dayOfConception}/{litter.monthOfConception}/{litter.yearOfConception}')
                    click.info(f'Birth: {litter.dayOfBirth}/{litter.monthOfBirth}/{litter.yearOfBirth}')
                    click.info(f'Incubation: {litter.dayOfIncubationStart}/{litter.monthOfIncubationStart}/{litter.yearOfIncubationStart}')
                    click.info(f'Father: {litter.fatherId}')
                    click.info(f'Mother: {litter.motherId}')
                    with click.info(f'Offspring:'):
                        for offspring in litter.offspringList:
                            click.info(f'* {offspring}')

    changed = False
    if args.erase:
        posscheck: bool = True
        if args.possibility and not gc.isPotentiallyPregnant():
            posscheck=False
        if not gc.isPregnant() and not posscheck:
            click.critical('Character is not preggers what are you doing.')
            return
        oldpv = bool2yn_colored(gc.isPregnant())
        oldpp = bool2yn_colored(gc.isPotentiallyPregnant())
        if gc.isPregnant():
            gc.terminatePregnancies(g)
        if args.possibility:
            gc.removeStatusEffect(EKnownStatusEffect.PREGNANT_0.name)
        newpv = bool2yn_colored(gc.isPregnant())
        newpp = bool2yn_colored(gc.isPotentiallyPregnant())
        click.secho(f'Is Pregnant: {oldpv} -> {newpv}')
        click.secho(f'Is Potentially Pregnant: {oldpp} -> {newpp}')
        changed=True

    if args.dry_run:
        click.warn('You specified --dry-run, exiting before we save anything.')
        return

    if changed:
        saveToFile(g, args.savefile, quiet=False)


import argparse

from lilitools.cli.lilitool.commands.room.edit import _register_room_edit
from lilitools.cli.lilitool.commands.room.find import _register_room_find
from lilitools.cli.lilitool.commands.room.show import _register_room_show


def register_room(subp: argparse._SubParsersAction) -> None:
    p = subp.add_parser('room').add_subparsers()
    _register_room_edit(p)
    _register_room_find(p)
    _register_room_show(p)
    
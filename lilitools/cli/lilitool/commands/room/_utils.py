import argparse
from typing import Tuple

from lilitools.cli.utils import existingFilePath
from lilitools.logging import ClickWrap
from lilitools.saves.character.player_character import PlayerCharacter
from lilitools.saves.game import Game
from lilitools.saves.utils import loadSaveFrom

from lilitools.saves.world.cell import Cell

click = ClickWrap()

__all__ = ['add_room_args', 'handle_room_args']


def add_room_args(p: argparse.ArgumentParser) -> None:
    p.add_argument('path', type=existingFilePath, help='Path to the save XML')
    
    wc = p.add_argument_group('By World Coordinates')
    wc.add_argument('world', type=str, help='ID of the world')
    wc.add_argument('x', type=int, help='X-coord of the cell in the given world')
    wc.add_argument('y', type=int, help='Y-coord of the cell in the given world')

    cc = p.add_argument_group('Common Cells')
    cc.add_argument('--player-room', action='store_true', default=False, help='Select player room')


def handle_room_args(args: argparse.Namespace) -> Tuple[Game, Cell]:
    game = loadSaveFrom(args.path)
    cell: Cell
    if args.player_room:
        cell = game.worlds.getPlayerRoom()
    else:
        world: str = args.world
        x: int = args.x
        y: int = args.y
        cell = game.worlds.getCellByWorldCoords(world,x,y)
    return game, cell

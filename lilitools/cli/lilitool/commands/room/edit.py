import argparse
import difflib
from pathlib import Path

from lilitools.logging import ClickWrap

from lilitools.cli.utils import bool2yn_colored, existingFilePath, fuzzybool

from lilitools.saves.utils import loadSaveFrom, saveToFile

from lilitools.saves.world.world import World

from lilitools.saves.world.cell import Cell

from lilitools.saves.world.enums.place_upgrade import EPlaceUpgrade

from lilitools.saves.game import Game

from ._utils import add_room_args, handle_room_args

__all__ = ['_register_room_edit']

click = ClickWrap()


def _register_room_edit(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser('edit')
    add_room_args(p)
    m = p.add_argument_group('Meta')
    m.add_argument('--dry-run', action='store_true', default=False)

    b = p.add_argument_group('Basics')
    b.add_argument('--set-place-name', type=str, default=None)
    b.add_argument('--unset-place-name', action='store_true', default=False)
    b.add_argument('--set-place-type', type=str, default=None)
    b.add_argument('--set-discovered', type=fuzzybool, default=None)
    b.add_argument('--set-travelled-to', type=fuzzybool, default=None)

    u = p.add_argument_group('Upgrades')
    u.add_argument('--clear-upgrades', action='store_true', default=False)
    u.add_argument('--add-upgrade', type=str, default=None, nargs='*')
    u.add_argument('--rm-upgrade', type=str, default=None, nargs='*')

    c = p.add_argument_group('Common')
    u.add_argument('--as-empty-room', action='store_true', default=False)
    u.add_argument('--as-solo-slave-room', action='store_true', default=False)
    u.add_argument('--as-double-slave-room', action='store_true', default=False)
    u.add_argument('--as-quad-slave-room', action='store_true', default=False)

    # u.add_argument('--as-empty-dungeon-cell', action='store_true', default=False)
    # u.add_argument('--as-slave-dungeon-cell', action='store_true', default=False)

    u.add_argument('--max-comfort', action='store_true', default=False)
    u.add_argument('--max-obedience', action='store_true', default=False)

    p.set_defaults(cmd=cmd_room_edit)


def _remove_milking_room(game: Game, cell: Cell) -> None:
    if cell.place.placeUpgrades is not None:
        if EPlaceUpgrade.LILAYA_MILKING_ROOM.name in cell.place.placeUpgrades:
            for mr in list(game.slavery.milkingRooms):
                if cell.worldID == mr.worldType and cell.location.x == mr.x and cell.location.y == mr.y:
                    with click.info('Removing milking room entry...'):
                        game.slavery.milkingRooms.remove(mr)
                        click.info(str(mr))


HABITABLE_LILAYA_ROOM_TYPES = {
    'LILAYA_HOME_ROOM_GARDEN_FIRST_FLOOR',
    'LILAYA_HOME_ROOM_WINDOW_FIRST_FLOOR',
    'LILAYA_HOME_ROOM_GARDEN_GROUND_FLOOR',
    'LILAYA_HOME_ROOM_WINDOW_GROUND_FLOOR',
}

HABITABLE_LILAYA_DUNGEON_TYPES = {
    'LILAYA_HOME_DUNGEON_CELL',
}

LILAYA_SLAVE_ROOM_TYPES = {
    EPlaceUpgrade.LILAYA_SLAVE_ROOM.name,
    EPlaceUpgrade.LILAYA_SLAVE_ROOM_DOUBLE.name,
    EPlaceUpgrade.LILAYA_SLAVE_ROOM_QUADRUPLE.name,
}


def cmd_room_edit(args: argparse.Namespace) -> None:
    savefile: Path = args.path
    dry_run: bool = args.dry_run
    game, ocell = handle_room_args(args)
    cell = ocell.clone()

    if bool(args.as_empty_room):
        if cell.place.type not in HABITABLE_LILAYA_ROOM_TYPES:
            click.error(f'To avoid breaking your save, you may only apply --as-empty-room to rooms of type {HABITABLE_LILAYA_ROOM_TYPES!r}.')
            return
        _remove_milking_room(game, cell)
        if cell.place.placeUpgrades is None:
            cell.place.placeUpgrades = set()
        cell.place.placeUpgrades.clear()
        cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_EMPTY_ROOM.name)
    elif bool(args.as_solo_slave_room):
        if cell.place.type not in HABITABLE_LILAYA_ROOM_TYPES:
            click.error(f'To avoid breaking your save, you may only apply --as-solo-slave-room to rooms of type {HABITABLE_LILAYA_ROOM_TYPES!r}.')
            return
        _remove_milking_room(game, cell)
        if cell.place.placeUpgrades is None:
            cell.place.placeUpgrades = set()
        cell.place.placeUpgrades.clear()
        cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_SLAVE_ROOM.name)
    elif bool(args.as_double_slave_room):
        if cell.place.type not in HABITABLE_LILAYA_ROOM_TYPES:
            click.error(f'To avoid breaking your save, you may only apply --as-double-slave-room to rooms of type {HABITABLE_LILAYA_ROOM_TYPES!r}.')
            return
        _remove_milking_room(game, cell)
        if cell.place.placeUpgrades is None:
            cell.place.placeUpgrades = set()
        cell.place.placeUpgrades.clear()
        cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_SLAVE_ROOM_DOUBLE.name)
    elif bool(args.as_quad_slave_room):
        if cell.place.type not in HABITABLE_LILAYA_ROOM_TYPES:
            click.error(f'To avoid breaking your save, you may only apply --as-quad-slave-room to rooms of type {HABITABLE_LILAYA_ROOM_TYPES!r}.')
            return
        _remove_milking_room(game, cell)
        if cell.place.placeUpgrades is None:
            cell.place.placeUpgrades = set()
        cell.place.placeUpgrades.clear()
        cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_SLAVE_ROOM_QUADRUPLE.name)

    max_comfort: bool = bool(args.max_comfort)
    max_obedience: bool = bool(args.max_obedience)
    if max_comfort or max_obedience:
        if cell.place.placeUpgrades is None:
            cell.place.placeUpgrades = set()
        if cell.place.type in HABITABLE_LILAYA_ROOM_TYPES:
            # Milking room
            if EPlaceUpgrade.LILAYA_MILKING_ROOM.name in cell.place.placeUpgrades:
                cell.place.placeUpgrades.clear()
                cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_MILKING_ROOM.name)
                if max_comfort:
                    cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_MILKING_ROOM_ARTISAN_MILKERS.name)
                    cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_MILKING_ROOM_CUM_EFFICIENCY.name)
                    cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_MILKING_ROOM_GIRLCUM_EFFICIENCY.name)
                    cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_MILKING_ROOM_MILK_EFFICIENCY.name)
                else:
                    cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_MILKING_ROOM_INDUSTRIAL_MILKERS.name)
                    cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_MILKING_ROOM_CUM_EFFICIENCY.name)
                    cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_MILKING_ROOM_GIRLCUM_EFFICIENCY.name)
                    cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_MILKING_ROOM_MILK_EFFICIENCY.name)

            if EPlaceUpgrade.LILAYA_OFFICE.name in cell.place.placeUpgrades:
                cell.place.placeUpgrades.clear()
                cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_OFFICE.name)
                if max_comfort:
                    cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_OFFICE_COFFEE_MACHINE.name)
                    cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_OFFICE_EXECUTIVE_UPGRADE.name)
                    cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_OFFICE_PARTITIONING_WALLS.name)
                elif max_obedience:
                    cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_OFFICE_COFFEE_MACHINE.name)
                    cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_OFFICE_EXECUTIVE_UPGRADE.name)

            if EPlaceUpgrade.LILAYA_SLAVE_LOUNGE.name in cell.place.placeUpgrades:
                cell.place.placeUpgrades.clear()
                cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_SLAVE_LOUNGE.name)
                #Nothing, yet

            # Slave room
            if any(x in cell.place.placeUpgrades for x in LILAYA_SLAVE_ROOM_TYPES):
                cell.place.placeUpgrades = set(filter(lambda x: x in LILAYA_SLAVE_ROOM_TYPES, cell.place.placeUpgrades))
                if max_comfort:
                    cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_SLAVE_ROOM_ROOM_SERVICE.name)
                    cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_SLAVE_ROOM_UPGRADE_BED.name)
                else:
                    cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_SLAVE_ROOM_DOG_BOWLS.name)
                    cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_SLAVE_ROOM_DOWNGRADE_BED.name)
                    cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_SLAVE_ROOM_OBEDIENCE_TRAINER.name)

        if cell.place.type in HABITABLE_LILAYA_DUNGEON_TYPES:
            if EPlaceUpgrade.LILAYA_DUNGEON_CELL.name in cell.place.placeUpgrades:
                cell.place.placeUpgrades.clear()
                cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_DUNGEON_CELL.name)
                if max_obedience:
                    cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_DUNGEON_CELL_CHAINS.name)
                    cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_DUNGEON_CELL_DOG_BOWLS.name)
                    cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_DUNGEON_CELL_DOWNGRADE_BED.name)
                    cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_SLAVE_ROOM_OBEDIENCE_TRAINER.name)
                else:  # why
                    cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_DUNGEON_CELL_DECENT_FOOD.name)
                    cell.place.placeUpgrades.add(EPlaceUpgrade.LILAYA_DUNGEON_CELL_UPGRADE_BED.name)

    if args.set_place_name is not None:
        cell.place.name = str(args.set_place_name).strip()
    if bool(args.unset_place_name):
        cell.place.name = None
    if args.set_place_type is not None:
        cell.place.type = str(args.set_place_type).strip()
    if args.set_discovered is not None:
        cell.discovered = bool(args.set_discovered)
    if args.set_travelled_to is not None:
        cell.travelledTo = bool(args.set_travelled_to)

    if bool(args.clear_upgrades):
        if cell.place.placeUpgrades is not None:
            cell.place.placeUpgrades.clear()
    if args.add_upgrade is not None:
        if cell.place.placeUpgrades is None:
            cell.place.placeUpgrades = set(args.add_upgrade)
        else:
            cell.place.placeUpgrades |= set(args.add_upgrade)
    if args.rm_upgrade is not None:
        if cell.place.placeUpgrades is not None:
            cell.place.placeUpgrades -= set(args.rm_upgrade)

    # Now for the tricky part
    if cell.place.type != ocell.place.type:
        click.info(f'Place Type: {ocell.place.type!r} -> {cell.place.type!r}')
    if cell.place.name != ocell.place.name:
        click.info(f'Place Name: {ocell.place.name!r} -> {cell.place.name!r}')
    if cell.place.placeUpgrades != ocell.place.placeUpgrades:
        if (cell.place.placeUpgrades is None) != (ocell.place.placeUpgrades is None):
            click.info(f'Place Upgrades: {ocell.place.placeUpgrades!r} -> {cell.place.placeUpgrades!r}')
        else:
            assert ocell.place.placeUpgrades is not None
            assert cell.place.placeUpgrades is not None
            with click.info('Place Upgrades:'):
                union = sorted(cell.place.placeUpgrades | ocell.place.placeUpgrades)
                added = cell.place.placeUpgrades - ocell.place.placeUpgrades
                removed = ocell.place.placeUpgrades - cell.place.placeUpgrades
                for e in union:
                    if e in added:
                        click.info(click.style('+',fg='green')+' '+e)
                    elif e in removed:
                        click.info(click.style('-',fg='red')+' '+e)
    game.worlds.setCellByWorldCoords(ocell.worldID, ocell.location.x, ocell.location.y, cell)
    if dry_run:
        click.warn(f'--dry-run is set, exiting here so we don\'t save.')
        return
    saveToFile(game, savefile)

import argparse

from lilitools.logging import ClickWrap

from lilitools.cli.utils import bool2yn_colored, existingFilePath

from lilitools.saves.utils import loadSaveFrom

from lilitools.saves.world.world import World

from lilitools.saves.world.cell import Cell

from ._utils import add_room_args, handle_room_args

__all__ = ['_register_room_find']

click = ClickWrap()


def _register_room_find(subp: argparse._SubParsersAction) -> None:
    p:argparse.ArgumentParser = subp.add_parser('find')
    p.add_argument('path', type=existingFilePath, help='Path to the save XML')
    p.add_argument('--by-npc-id',    type=str, default=None)
    p.add_argument('--by-room-name', type=str, default=None)
    p.add_argument('--by-room-type', type=str, default=None)
    p.set_defaults(cmd=cmd_room_find)


def cmd_room_find(args: argparse.Namespace) -> None:
    game = loadSaveFrom(args.path)
    def criteria_match(w: World, c:Cell) -> bool:
        a = args.by_room_type and args.by_room_type == c.place.type
        b = args.by_room_name and args.by_room_name == c.place.name
        return a or b
    for wid, w in game.worlds.worlds.items():
        for c in w.cells:
            if criteria_match(w,c):
                click.info(str(c))
import argparse

from lilitools.logging import ClickWrap

from lilitools.cli.utils import bool2yn_colored

from lilitools.saves.world.cell import Cell

from ._utils import add_room_args, handle_room_args

__all__ = ['_register_room_show']

click = ClickWrap()


def _register_room_show(subp: argparse._SubParsersAction) -> None:
    p = subp.add_parser('show')
    add_room_args(p)
    p.set_defaults(cmd=cmd_room_show)


def cmd_room_show(args: argparse.Namespace) -> None:
    _, cell = handle_room_args(args)
    show_cell(cell.clone())

def show_cell(cell:Cell)->None:
    if cell is None:
        click.error('Could not find provided cell.')
        return
    with click.info(f'Cell @ {cell.worldID}[{cell.location.x},{cell.location.y}]'):
        click.info('Name: '+cell.getName())
        with click.info('Flags:'):
            click.info(f'Discovered: {bool2yn_colored(cell.discovered)}')
            click.info(f'Travelled To: {bool2yn_colored(cell.travelledTo)}')
        with click.info('Place:'):
            click.info(f'Name: {cell.place.name}')
            click.info(f'Type: {cell.place.type}')
            if cell.place.placeUpgrades is None:
                click.info('Upgrades: (None)')
            else:
                with click.info(f'Upgrades: ({len(cell.place.placeUpgrades)})'):
                    for u in sorted(cell.place.placeUpgrades):
                        click.info(f'- {u}')

import argparse
import os
from pathlib import Path
from tempfile import NamedTemporaryFile
import time
import human_readable
from lxml import etree
from lilitools.cli.utils import existingFilePath
from lilitools.logging import ClickWrap

from lilitools.saves.utils import loadSaveFrom, saveToFile, writeRootToFile

click=ClickWrap()

def register_save(subp:argparse._SubParsersAction) -> None:
    savep = subp.add_parser('save').add_subparsers()

    _register_save_dump(savep)
    _register_save_clone(savep)
    _register_save_compress(savep)


def _register_save_compress(subp: argparse._SubParsersAction) -> None:
    argp = subp.add_parser('compress')
    argp.add_argument('savefiles', type=existingFilePath, nargs='+', help='Paths to compress')
    argp.add_argument('--dry-run', action='store_true', default=False, help='Don\'t actually save anything')
    argp.set_defaults(cmd=cmd_save_compress)


def cmd_save_compress(args: argparse.Namespace) -> None:
    subject:Path
    total_compressed_away: int=0
    for subject in args.savefiles:
        outfile:Path
        try:
            with NamedTemporaryFile(delete=False) as f:
                outfile=Path(f.name)
                old_size = os.path.getsize(subject)
                game = loadSaveFrom(subject)
                saveToFile(game, outfile, minify=True)
                new_size = os.path.getsize(outfile)
                total_compressed_away+=old_size-new_size
                old_size_hr = human_readable.file_size(old_size)
                new_size_hr = human_readable.file_size(new_size)
                click.success(f'Wrote {outfile} ({old_size_hr} => {new_size_hr}, {(new_size/old_size)*100.}% compression)')
            if not args.dry_run:
                os.replace(outfile,subject)
        finally:
            if outfile.is_file():
                outfile.unlink()
    total_compressed_away_hr = human_readable.file_size(total_compressed_away)
    click.success(f'Compressed away {total_compressed_away_hr}!')

def _register_save_clone(subp: argparse._SubParsersAction) -> None:
    argp = subp.add_parser('clone', aliases=['copy', 'cp'])
    argp.add_argument('origfile', type=existingFilePath, help='Path to copy XML from')
    argp.add_argument('clonefile', type=Path, help='Path to copy XML to')
    argp.add_argument('--reset-save-id', action='store_true', default=False, help='Set a new save ID (Sets to time.time())')
    argp.add_argument('--player-name-masculine', type=str, default=None, help='Change player name (masculine form).')
    argp.add_argument('--player-name-androgynous', type=str, default=None, help='Change player name (androgynous form).')
    argp.add_argument('--player-name-feminine', type=str, default=None, help='Change player name (feminine form).')
    argp.add_argument('--player-surname', type=str, default=None, help='Change player name (feminine form).')
    argp.add_argument('--dry-run', action='store_true', default=False, help='Don\'t actually save anything')
    argp.set_defaults(cmd=cmd_save_clone)


def cmd_save_clone(args: argparse.Namespace) -> None:
    game = loadSaveFrom(args.origfile)
    name_changed: bool=False
    if args.reset_save_id == True:
        old = game.id
        new = int(time.time())
        click.success(f'Changed save ID: {old!r} -> {new!r}')
    if args.player_name_masculine is not None:
        old = game.playerCharacter.core.name.masculine
        new = args.player_name_masculine
        game.playerCharacter.core.name.masculine = args.player_name_masculine
        click.success(f'Changed the masculine form of your name: {old!r} -> {new!r}')
        name_changed = True
    if args.player_name_androgynous is not None:
        old = game.playerCharacter.core.name.androgynous
        new = args.player_name_androgynous
        game.playerCharacter.core.name.androgynous = args.player_name_androgynous
        click.success(f'Changed the androgynous form of your name: {old!r} -> {new!r}')
        name_changed = True
    if args.player_name_feminine is not None:
        old = game.playerCharacter.core.name.feminine
        new = args.player_name_feminine
        game.playerCharacter.core.name.feminine = args.player_name_feminine
        click.success(f'Changed the feminine form of your name: {old!r} -> {new!r}')
        name_changed = True
    if args.player_surname is not None:
        old = game.playerCharacter.core.surname
        new = args.player_surname
        game.playerCharacter.core.surname = args.player_surname
        click.success(f'Changed your surname: {old!r} -> {new!r}')
        name_changed = True
    #print(game.playerCharacter.core.name.toDict())
    if name_changed:
        game.propogateNameChangeToChildren(game.playerCharacter)
    if args.dry_run:
        click.warn('You specified --dry-run, exiting before we save anything.')
        return
    saveToFile(game, args.clonefile)

def _register_save_dump(subp: argparse._SubParsersAction) -> None:
    argp = subp.add_parser('dump')
    argp.add_argument('infile', type=existingFilePath, help='Path to load XML from')
    argp.add_argument('outfile', type=Path, help='Path to save XML to')
    argp.add_argument('--minify', action='store_true', default=False)
    argp.set_defaults(cmd=cmd_save_dump)


def cmd_save_dump(args: argparse.Namespace) -> None:
    game = loadSaveFrom(args.infile)
    saveToFile(game, args.outfile, show_warnings=True, minify=args.minify, sort_attrs=True)

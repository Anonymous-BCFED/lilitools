import argparse
from pathlib import Path
from typing import Any, Optional, Set

from lilitools.cli.utils import arg2enum, bool2yn_colored, existingFilePath
from lilitools.logging import ClickWrap
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.enums.tf_modifier import ETFModifier
from lilitools.saves.game import Game
from lilitools.saves.inventoryslot import EInventorySlot
from lilitools.saves.items.abstract_clothing import AbstractClothing
from lilitools.saves.utils import loadSaveFrom, saveToFile

click = ClickWrap()


class _ArgsForUnseal(argparse.Namespace):
    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self.savefile: Path
        self.slot: Optional[EInventorySlot]
        self.all_slots: bool
        self.player: bool
        self.npc_id: Optional[str]
        self.dry_run: bool


def register_unseal(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser(
        "unseal", description="Unseal articles of clothing"
    )

    p.add_argument("savefile", metavar="savefile.xml", type=existingFilePath)
    pslot = p.add_mutually_exclusive_group()
    pslot.add_argument(
        "--slot",
        nargs="*",
        type=arg2enum(EInventorySlot),
        choices=[x.name.lower() for x in EInventorySlot],
    )
    pslot.add_argument(
        "--all-slots", action="store_true", default=False, help="all slots"
    )
    pslot.title = "Slots"
    pslot.description = "Which slot(s) to unseal"

    pwho = p.add_mutually_exclusive_group()
    pwho.add_argument("--player", action="store_true", default=False)
    pwho.add_argument("--npc-id", type=str, default=None)
    pwho.title = "Whomst"
    pwho.description = "Who is the subject of this command"

    p.add_argument("--dry-run", action="store_true", default=False)

    p.set_defaults(cmd=cmd_unseal)


def cmd_unseal(args: _ArgsForUnseal) -> None:
    g: Game = loadSaveFrom(args.savefile)
    gc: GameCharacter
    if args.player:
        gc = g.playerCharacter
    elif args.npc_id is not None:
        gc = g.getNPCById(args.npc_id)
        assert gc is not None, "Could not find that NPC. See `npc list`."
    assert gc is not None, "You MUST specify either --player or --npc-id."

    changed = False

    slot: AbstractClothing
    slots: Set[str] = set()
    if args.slot is not None:
        slots = {x.name for x in EInventorySlot if x in args.slot}
    for slot in gc.characterInventory.clothingEquipped:
        if args.all_slots or (
            slot is not None and args.slot is not None and slot.slotEquippedTo in slots
        ):
            if len(slot.effects) > 0 and any(
                x.type == "CLOTHING"
                and x.mod1 == ETFModifier.CLOTHING_SPECIAL
                and x.mod2 == ETFModifier.CLOTHING_SEALING
                for x in slot.effects
            ):
                with click.info(f"{slot.slotEquippedTo}: {slot.id}"):
                    orig: bool = slot.sealed
                    slot.sealed = False
                    if orig != slot.sealed:
                        sealed = click.style("sealed:", bold=True, fg="white", dim=True)
                        arrow = click.style("=>", bold=False, fg="white", dim=True)
                        click.secho(
                            f"{sealed} {bool2yn_colored(orig)} {arrow} {bool2yn_colored(slot.sealed)}"
                        )
                        changed = True
    if not changed:
        click.warn(
            f'Could not find any matching items in slots={"[ALL]" if args.all_slots else repr(slots)} on character {gc.core.id}'
        )

    if args.dry_run:
        click.warn("--dry-run is on.  Aborting save.")
        return

    if changed:
        saveToFile(g, args.savefile)

import os
from pathlib import Path
from typing import Dict

from dulwich import porcelain
from dulwich.lfs import LFSStore
from dulwich.repo import Repo

from lilitools.logging import ClickWrap

REPOS: Dict[Path, Repo] = {}
LFSDIRS: Dict[Path, LFSStore] = {}

click = ClickWrap()


def getDulwichRepo(cwd: Path) -> Repo:
    acwd = cwd.absolute()
    if acwd in REPOS:
        return REPOS[acwd]
    click.info(f"[git] Connecting to local git repo at {cwd} using dulwich...")
    r = Repo.discover(cwd)
    if r is None:
        return r
    REPOS[acwd] = r
    return r


def initDulwichRepo(cwd: Path) -> Repo:
    click.info(f"[git] Initializing git repo with dulwich...")
    r = Repo.init(cwd)
    REPOS[cwd.absolute()] = r
    return r


def resetDulwichRepo(repo: Repo, to: bytes | None) -> None:
    click.info(f"[git] Resetting repo (hard) to {to or 'HEAD'}...")
    repo.reset_index(tree=to)


def setDulwichConfig(repo: Repo, key: bytes, value: bytes) -> None:
    cfg = repo.get_config()
    section = tuple(key.split(b".")[:-1])
    name = key.split(b".")[-1]
    click.info(f"[git] Setting config option: {key} => {value}")
    # print("  key:", key)
    # print("    section:", section)
    # print("    name:   ", name)
    # print("  value:", value)
    cfg.set(section, name, value)
    cfg.write_to_path()


def doesDulwichDetectAnyStagedChanges(repo: Repo) -> bool:
    status = porcelain.status(repo.path)
    changes = 0
    for cat, files in status.staged.items():
        for f in files:
            if f == b"./":
                continue
            changes += 1
    return changes > 0


def printDulwichStagingStatus(repo: Repo) -> None:
    status = porcelain.status(repo.path)
    with click.info("Staged:"):
        # print(repr(status.staged))
        for cat, files in status.staged.items():
            for f in files:
                if f == b"."+os.sep.encode():
                    continue
                if isinstance(f, bytes):
                    f=f.decode()
                match cat:
                    case "add":
                        cat = click.style("A", fg="green")
                    case "modify":
                        cat = click.style("M", fg="yellow")
                    case "rm"|'delete':
                        cat = click.style("D", fg="red")
                click.info(f"{cat} {f}")
    with click.info("Unstaged:"):
        for f in status.unstaged:
            if f == b"."+os.sep.encode():
                continue
            click.info(f)
    with click.info("Untracked:"):
        for f in status.untracked:
            if f == b"."+os.sep.encode():
                continue
            click.info(f)


def installLFSToDulwich(repo: Repo) -> LFSStore:
    with click.info(f"[git] Installing LFS..."):
        lfsdir = Path(repo.controldir()) / "lfs"
        # lfs = LFSStore.from_repo(repo, create=True) Bugged
        lfs = LFSStore.create(lfsdir)
        click.success(f"Done!")
    LFSDIRS[lfsdir.absolute()] = lfs
    return lfs

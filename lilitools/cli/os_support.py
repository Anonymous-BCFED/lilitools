import sys
__all__ = ["CONSOLE_SUPPORTS_UNICODE_FRACTIONS"]

def _CONSOLE_SUPPORTS_UNICODE_FRACTIONS() -> bool:
    try:
        "\u2044".encode(sys.stdout.encoding)
        return True
    except UnicodeEncodeError:
        return False


CONSOLE_SUPPORTS_UNICODE_FRACTIONS: bool = _CONSOLE_SUPPORTS_UNICODE_FRACTIONS()

import argparse
from typing import List, Optional

from lilitools.cli.slavesheet.commands import _register_all_


def main(args: Optional[List[str]] = None) -> None:
    argp = argparse.ArgumentParser('slavesheet')

    subp = argp.add_subparsers()

    _register_all_(subp)

    _args = argp.parse_args(args)
    if not hasattr(_args, 'cmd'):
        argp.print_usage()
    else:
        _args.cmd(_args)

if __name__ == '__main__':
    main()
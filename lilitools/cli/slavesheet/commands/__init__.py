import argparse

from lilitools.cli.slavesheet.commands.import_ import _register_import
from lilitools.cli.slavesheet.commands.serve import _register_serve
from lilitools.cli.slavesheet.commands.slave import _register_slave


def _register_all_(subp: argparse._SubParsersAction) -> None:
    _register_import(subp)
    # _register_export(subp)
    _register_slave(subp)
    # _register_trainingrooms(subp)
    _register_serve(subp)

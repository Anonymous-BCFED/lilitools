
import argparse
from pathlib import Path

from lilitools.cli.slavesheet.slavery import Slavery
from lilitools.cli.utils import existingFilePath
from lilitools.saves.game import Game
from lilitools.saves.utils import loadSaveFrom


def _register_import(subp: argparse._SubParsersAction) -> None:
    argp = subp.add_parser('import')
    argp.add_argument('database', type=Path)
    argp.add_argument('savefile', type=existingFilePath)
    argp.add_argument('--clobber', action='store_true', default=False,
                      help='Ignore old data, overwrite with new. This will discard notes, etc')
    argp.set_defaults(cmd=_cmd_import)


def _cmd_import(args: argparse.Namespace) -> None:
    slaves = Slavery()
    if args.database.is_file() and not args.clobber:
        slaves.load(args.database)
    else:
        slaves.clear()
    g: Game = loadSaveFrom(args.savefile)
    slaves.importSlavesFromGame(g)
    slaves.importTrainingRoomsFromGame(g)
    slaves.save(args.database)
    print(slaves.tabulate())
    print(f' => {args.database}')

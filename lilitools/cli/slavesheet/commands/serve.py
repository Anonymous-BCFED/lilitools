import argparse
import logging
import sys
from http import HTTPStatus
from pathlib import Path
from typing import Any, Dict, List, Optional, Tuple, Union

from flask import Flask, request, send_file, send_from_directory
from flask.json import jsonify
from flask.wrappers import Response
from waitress import serve

from lilitools.cli.slavesheet.enums.plans import EPlans
from lilitools.cli.slavesheet.enums.status import EStatus
from lilitools.cli.slavesheet.enums.treatment import ETreatment
from lilitools.cli.slavesheet.room import Room
from lilitools.cli.slavesheet.slavery import Slavery
from lilitools.cli.utils import existingFilePath, find_free_port, validIP, validPort
from lilitools.saves.utils import loadSaveFrom

DEFAULT_CONFIG = Path.home() / ".config" / "lilitools" / "slavesheet.toml"


def _register_serve(subp: argparse._SubParsersAction) -> None:
    argp: argparse.ArgumentParser = subp.add_parser("serve")
    argp.add_argument("--port", "-p", type=validPort, default=None)
    argp.add_argument("--bind", "-b", type=validIP(), default=None)
    argp.add_argument("--config", "-c", type=existingFilePath, default=None)
    argp.add_argument("database", type=existingFilePath)
    argp.set_defaults(cmd=_cmd_serve)


flask: Flask = None
POSSIBLE_WEB_PATHS: List[Path] = [
    Path("web"),
    Path("dist") / "web",
]
POSSIBLE_SAVES_PATHS: List[Path] = [
    Path("data") / "saves",
    Path("dist") / "data" / "saves",
]
PATH_WEB: Path = None
PATH_STATIC: Path = None
PATH_SAVES: Path = None
log = logging.getLogger(__name__)


def _try_build_path(base: Path) -> bool:
    global PATH_WEB, PATH_STATIC
    log.info(f"Trying {base}...")
    if base.is_dir():
        PATH_WEB = base
        PATH_STATIC = PATH_WEB / "static"
        return True
    return False


def _try_build_paths() -> None:
    cwd = Path.cwd()
    for base in POSSIBLE_WEB_PATHS:
        if _try_build_path(cwd / base):
            return
    log.critical("FAILED to detect base path.")


def default_config() -> Dict[str, Dict[str, Any]]:
    return {
        "webserver": {
            "bind": "127.0.0.1",
            "port": 0,
        }
    }


def get_config(filename: Optional[Path] = None) -> dict:
    if filename is None:
        filename = DEFAULT_CONFIG
    config = default_config()
    if filename.is_file():
        import toml

        with filename.open("r") as f:
            config = toml.load(f)
    return config


def save_config(
    config: Dict[str, Dict[str, Any]], filename: Optional[Path] = None
) -> dict:
    if filename is None:
        filename = DEFAULT_CONFIG
    filename.parent.mkdir(parents=True, exist_ok=True)
    import toml

    with filename.open("w") as f:
        toml.dump(config, f)


def makeAPIResponse(
    response_ok: bool, error_message: Optional[str] = None
) -> Dict[str, Any]:
    return {"ok": response_ok, "message": error_message}


def makeAPIError(http_code: Union[int, HTTPStatus], error_msg: str) -> Response:
    from flask.json import jsonify

    response = jsonify(makeAPIResponse(False, error_msg))
    response.status_code = http_code
    return response


def _cmd_serve(args: argparse.Namespace) -> None:
    # Disable development server warning
    # cli = sys.modules['flask.cli']
    # cli.show_server_banner = lambda *x: None

    slaves = Slavery()
    slaves.load(args.database)
    print(slaves.tabulate())

    _try_build_paths()

    flask = Flask(__name__, static_folder=PATH_STATIC, static_url_path="/static")

    @flask.route("/")
    def index():
        return send_file(PATH_STATIC / "index.html", "text/html")

    @flask.route("/userstyle.css")
    def userstyle():
        cssfile = Path.cwd() / "userstyle.css"
        if not cssfile.is_file():
            cssfile.write_text(
                """/* Put your user-specified CSS styles here. */

/* EXAMPLE: Highlight "customized" tag in purple with white text.
.tag-userstyle-customized { background: purple; color: white; }
*/
"""
            )
        return send_file(cssfile, "text/css")

    @flask.route("/static/<path:path>")
    def static_file(path):
        return send_from_directory(PATH_STATIC, path)

    @flask.route("/api/slaves", methods=["GET"])
    def api_slaves():
        o: Dict[str, Any] = makeAPIResponse(True)
        o["save-file"] = str(slaves.savefile)
        o["slaves"] = [x.toDict() for x in slaves.allSlaves]
        # print(repr(o))
        return o

    @flask.route("/api/slaves/import", methods=["POST"])
    def api_slaves_import():
        if request.method != "POST":
            return makeAPIError(HTTPStatus.BAD_REQUEST, "METHOD MUST BE POST")
        if "path" not in request.form:
            return makeAPIError(HTTPStatus.BAD_REQUEST, "PATH REQUIRED")
        path = Path(request.form["path"])
        if not path.is_file():
            print(404, path, "FILE NOT FOUND")
            return makeAPIError(HTTPStatus.NOT_FOUND, "FILE NOT FOUND")

        try:
            path.absolute().relative_to(Path.cwd().absolute())
        except ValueError:
            print(403, path, "FORBIDDEN")
            return makeAPIError(
                HTTPStatus.FORBIDDEN,
                f"FILE MUST BE A SUBDIRECTORY OF {Path.cwd().absolute()}!",
            )

        g = loadSaveFrom(path)
        slaves.importSlavesFromGame(g)
        slaves.importTrainingRoomsFromGame(g)
        slaves.savefile = path.absolute()
        slaves.save(args.database)
        print(200, slaves.savefile, "OK")
        return makeAPIResponse(True)

    @flask.route("/api/slave/by-name/<name>", methods=["GET", "POST"])
    def api_slave_byname(name: str):
        slave = slaves.slavesByName.get(id)
        if slave is None:
            return makeAPIError(HTTPStatus.NOT_FOUND, "NOT FOUND")
        if request.method == "POST":
            if "notes" in request.form:
                slave.notes = request.form["notes"].strip()
            if "status" in request.form:
                slave.status = EStatus[request.form["status"].strip().upper()]
            if "treatment" in request.form:
                slave.treatment = ETreatment[request.form["treatment"].strip().upper()]
            if "plans" in request.form:
                slave.plans = EPlans[request.form["plans"].strip().upper()]
            if "tags[]" in request.form:
                slave.tags = set(
                    map(lambda x: x.strip(), request.form.getlist("tags[]"))
                )
            slaves.save(args.database)
        o: Dict[str, Any] = makeAPIResponse(True)
        o["slave"] = slave.toDict()
        return o

    @flask.route("/api/slave/by-id/<id>", methods=["GET", "POST"])
    def api_slave_byid(id: str):
        slave = slaves.slavesByID.get(id)
        if slave is None:
            return makeAPIError(HTTPStatus.NOT_FOUND, "NOT FOUND")
        if request.method == "POST":
            if "notes" in request.form:
                slave.notes = request.form["notes"].strip()
            if "status" in request.form:
                slave.status = EStatus[request.form["status"].strip().upper()]
            if "treatment" in request.form:
                slave.treatment = ETreatment[request.form["treatment"].strip().upper()]
            if "plans" in request.form:
                slave.plans = EPlans[request.form["plans"].strip().upper()]
            if "tags[]" in request.form:
                slave.tags = set(
                    map(lambda x: x.strip(), request.form.getlist("tags[]"))
                )
            slaves.save(args.database)
        o: Dict[str, Any] = makeAPIResponse(True)
        o["slave"] = slave.toDict()
        return o

    @flask.route("/api/slave/edit", methods=["POST"])
    def api_slave_edit():

        if "id" not in request.form:
            return makeAPIError(HTTPStatus.BAD_REQUEST, "BAD REQUEST")

        id = request.form["id"]
        slave = slaves.slavesByID.get(id)
        if slave is None:
            return makeAPIError(HTTPStatus.NOT_FOUND, "NOT FOUND")

        if "notes" in request.form:
            slave.notes = request.form["notes"].strip()
        if "status" in request.form:
            slave.status = EStatus[request.form["status"].strip().upper()]
        if "treatment" in request.form:
            slave.treatment = ETreatment[request.form["treatment"].strip().upper()]
        if "plans" in request.form:
            slave.plans = EPlans[request.form["plans"].strip().upper()]
        if "tags[]" in request.form:
            slave.tags = set(map(lambda x: x.strip(), request.form.getlist("tags[]")))

        slaves.save(args.database)

        o: Dict[str, Any] = makeAPIResponse(True)
        o["slave"] = slave.toDict()
        return o

    @flask.route("/api/slave/tag/delete", methods=["POST"])
    def api_slave_tag_delete():

        if "id" not in request.form:
            return makeAPIError(HTTPStatus.BAD_REQUEST, "BAD REQUEST")
        if "tag" not in request.form:
            return makeAPIError(HTTPStatus.BAD_REQUEST, "BAD REQUEST")

        id = request.form["id"]
        tag = str(request.form['tag'])
        slave = slaves.slavesByID.get(id)
        if slave is None:
            return makeAPIError(HTTPStatus.NOT_FOUND, "NOT FOUND")
        if tag not in slave.tags:
            return makeAPIError(HTTPStatus.NOT_FOUND, "NOT FOUND")
        slave.tags.remove(tag)
        slaves.save(args.database)

        o: Dict[str, Any] = makeAPIResponse(True)
        o["slave"] = slave.toDict()
        return o

    @flask.route("/api/training_rooms", methods=["GET"])
    def api_trainingrooms():
        o: Dict[str, Any] = makeAPIResponse(True)
        o["save-file"] = str(slaves.savefile)
        o["training_rooms"] = slaves.getAPITrainingRoomRows()
        return o

    @flask.route("/api/training_room/new", methods=["POST"])
    def api_trainingroom_new():
        if "id" not in request.form:
            return makeAPIError(HTTPStatus.BAD_REQUEST, "BAD REQUEST")
        if not request.form["id"].isnumeric():
            return makeAPIError(HTTPStatus.BAD_REQUEST, "BAD REQUEST")
        if "name" not in request.form:
            return makeAPIError(HTTPStatus.BAD_REQUEST, "BAD REQUEST")
        if request.form["name"] == "":
            return makeAPIError(HTTPStatus.BAD_REQUEST, "BAD REQUEST")
        if "slots" not in request.form:
            return makeAPIError(HTTPStatus.BAD_REQUEST, "BAD REQUEST")
        if not request.form["slots"].isnumeric():
            return makeAPIError(HTTPStatus.BAD_REQUEST, "BAD REQUEST")
        slots = int(request.form["slots"])
        if slots <= 0:
            return makeAPIError(HTTPStatus.BAD_REQUEST, "BAD REQUEST")

        id = int(request.form["id"])
        if id in slaves.trainingRoomsByID:
            return makeAPIError(HTTPStatus.CONFLICT, "Room by that ID already exists")
        tr = Room()
        tr.id = id
        tr.name = request.form["name"]
        tr.slots = slots
        slaves.addTrainingRoom(tr)
        slaves.save(args.database)

        o: Dict[str, Any] = makeAPIResponse(True)
        o["save-file"] = str(slaves.savefile)
        o["training_rooms"] = slaves.getAPITrainingRoomRows()
        return o

    @flask.route("/api/training_room/<int:roomid>/delete", methods=["POST"])
    def api_trainingroom_delete(roomid: int):
        if roomid not in slaves.trainingRoomsByID:
            return makeAPIError(HTTPStatus.NOT_FOUND, "Not Found")
        tr = slaves.trainingRoomsByID[roomid]
        slaves.rmTrainingRoom(tr)
        return makeAPIResponse(True)

    @flask.route("/api/training_room/<int:roomid>/edit", methods=["POST"])
    def api_trainingroom_edit(roomid: int):
        if roomid not in slaves.trainingRoomsByID:
            return makeAPIError(HTTPStatus.NOT_FOUND, "Not Found")

        if "id" not in request.form:
            return makeAPIError(HTTPStatus.BAD_REQUEST, "No ID")
        if not request.form["id"].isnumeric():
            return makeAPIError(HTTPStatus.BAD_REQUEST, "Non-numeric ID")
        newid: int = int(request.form["id"])

        if "name" not in request.form:
            return makeAPIError(HTTPStatus.BAD_REQUEST, "No Name")
        if request.form["name"].strip() == "":
            return makeAPIError(HTTPStatus.BAD_REQUEST, "Name is empty")
        newname: str = request.form["name"].strip()

        if "slots" not in request.form:
            return makeAPIError(HTTPStatus.BAD_REQUEST, "No Slots")
        if not request.form["slots"].isnumeric():
            return makeAPIError(HTTPStatus.BAD_REQUEST, "Non-numeric slots")

        slots = int(request.form["slots"])
        if slots <= 0:
            return makeAPIError(HTTPStatus.BAD_REQUEST, "Slots <= 0")

        removeAndReadd = False
        if roomid != newid:
            removeAndReadd = True
            if newid in slaves.trainingRoomsByID:
                return makeAPIError(
                    HTTPStatus.CONFLICT, "Room by that ID already exists"
                )

        tr = slaves.trainingRoomsByID[roomid]
        if tr.name != newname:
            removeAndReadd = True
            if newname in slaves.trainingRoomsByName:
                return makeAPIError(
                    HTTPStatus.CONFLICT, "Room by that name already exists"
                )

        if removeAndReadd:
            slaves.rmTrainingRoom(tr)

            tr.id = newid
            tr.name = newname
            slaves.addTrainingRoom(tr)

        tr.slots = slots

        slaves.save(args.database)
        o: Dict[str, Any] = makeAPIResponse(True)
        o["save-file"] = str(slaves.savefile)
        o["training_rooms"] = slaves.getAPITrainingRoomRows()
        return o

    @flask.route("/api/autocomplete/path", methods=["GET"])
    def api_autocomplete_path():
        def _entry(path: Path) -> Tuple[str, str]:
            type = ""
            if path.is_file():
                type = "f"
            elif path.is_dir():
                type = "d"
            return [str(path), type]

        q = request.args["q"]
        if q is None or q == "":
            return jsonify([_entry(Path.home())])
        p = Path(q)
        if p.is_file():
            return jsonify([_entry(p)])
        if p.is_dir():
            return jsonify(list(map(_entry, p.glob("*"))))
        dir = p.parent
        name = p.name
        return jsonify(list(map(_entry, dir.glob(name + "*"))))

    # Bork flask.run(args.bind, args.port)
    config = get_config(args.config)
    cfg_dirty = False
    if args.bind is not None:
        config["webserver"]["bind"] = args.bind
        cfg_dirty = True
    if args.port is not None:
        config["webserver"]["port"] = args.port
        cfg_dirty = True
    if cfg_dirty:
        save_config(config, args.config)
    ipaddr: str = config["webserver"]["bind"]
    port: int = config["webserver"]["port"]
    port_is_random: bool = False
    if port == 0:
        port = find_free_port()
        port_is_random = True
    print("Starting up lilitools slavesheet:")
    print(f"  IP:   {ipaddr}")
    print(f"  Port: {port}" + (" (Random)" if port_is_random else ""))
    print(f"Visit http://{ipaddr}:{port}/ with your browser to access slavesheet.")
    print(f"Press CTRL+C to shut down the server and exit.")
    serve(flask, host=ipaddr, port=port)

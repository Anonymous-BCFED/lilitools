import argparse
from enum import Enum
import sys
from typing import Set, Tuple, Type, TypeVar

from lilitools.cli.slavesheet.enums.plans import EPlans
from lilitools.cli.slavesheet.enums.status import EStatus
from lilitools.cli.slavesheet.enums.treatment import ETreatment
from lilitools.cli.slavesheet.slave_info import SlaveInfo
from lilitools.cli.slavesheet.slavery import Slavery
from lilitools.cli.utils import existingFilePath
from lilitools.logging import ClickWrap
from lilitools.saves.character.npc import NPC
from lilitools.saves.game import Game
from lilitools.saves.utils import loadSaveFrom

click = ClickWrap()


# __all__ = ["add_slave_args", "handle_slave_args"]
def add_slavery_yml_arg(p: argparse.ArgumentParser) -> None:
    p.add_argument("path", type=existingFilePath, help="Path to the slavery YML")


def add_slave_args(p: argparse.ArgumentParser) -> None:
    add_slavery_yml_arg(p)
    p.add_argument(
        "--npc-id",
        type=str,
        default=None,
        help='ID of the NPC (e.g. "--npc-id=-1,FortressDemonLeader")',
    )
    p.add_argument(
        "--slave-id",
        type=str,
        default=None,
        help='SID of the slave (e.g. "--slave-id=ABCD-1234")',
    )


def add_many_slave_args(p: argparse.ArgumentParser) -> None:
    add_slavery_yml_arg(p)
    p.add_argument(
        "--npc-id",
        "--npc-ids",
        type=str,
        default=None,
        nargs="*",
        help='ID of the NPC (e.g. "--npc-id=-1,FortressDemonLeader")',
    )
    p.add_argument(
        "--slave-id",
        "--slave-ids",
        type=str,
        default=None,
        nargs="*",
        help='SID of the slave (e.g. "--slave-id=ABCD-1234")',
    )


def handle_slave_args(args: argparse.Namespace) -> Tuple[Slavery, Game, NPC]:
    slavery = Slavery()
    slavery.load(args.path)
    game = loadSaveFrom(slavery.savefile)
    npc: NPC
    if args.npc_id is not None:
        npc = game.getNPCById(args.npc_id)
        if npc is None:
            click.secho(
                f"NPC {args.npc_id!r} not found. Set --npc-id or --slave-id",
                err=True,
                fg="red",
            )
            sys.exit(1)
    if args.slave_id is not None:
        npc = next(
            [x for x in game.npcs if SlaveInfo.CalculateSID(x.core.id) == args.slave_id]
        )
        if npc is None:
            click.secho(
                f"NPC {args.slave_id!r} not found. Set --npc-id or --slave-id",
                err=True,
                fg="red",
            )
            sys.exit(1)
    return slavery, game, npc


def handle_many_slave_args(args: argparse.Namespace) -> Tuple[Slavery, Game, Set[NPC]]:
    slavery = Slavery()
    slavery.load(args.path)
    game = loadSaveFrom(slavery.savefile)
    npc: NPC
    npcs: Set[NPC] = set()
    if args.npc_id is not None:
        for npc_id in args.npc_id:
            npc = game.getNPCById(npc_id)
            if npc is None:
                click.secho(
                    f"NPC {npc_id!r} not found. Set --npc-id or --slave-id",
                    err=True,
                    fg="red",
                )
                sys.exit(1)
            npcs.add(npc)
    if args.slave_id is not None:
        npcs.update(
            [x for x in game.npcs if SlaveInfo.CalculateSID(x.core.id) in args.slave_id]
        )
    return slavery, game, list(npcs)


def _register_slave(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser("slave")
    slavep = p.add_subparsers()
    __register_slave_add(slavep)
    __register_slave_list(slavep)
    __register_slave_remove(slavep)
    __register_slave_set(slavep)
    __register_slave_show(slavep)


def __register_slave_add(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser("add", help="add a slave")
    add_many_slave_args(p)
    p.add_argument("--set-tags", "--tags", "-T", type=str, nargs="*", default=[])
    p.add_argument(
        "--set-plans",
        "--plans",
        "-p",
        choices=sorted([x.name for x in EPlans]),
        default=EPlans.NONE.name,
    )
    p.add_argument(
        "--set-status",
        "--status",
        "-s",
        choices=sorted([x.name for x in EStatus]),
        default=EStatus.IDLE.name,
    )
    p.add_argument(
        "--set-treatment",
        "--treatment",
        "-t",
        choices=sorted([x.name for x in ETreatment]),
        default=ETreatment.UNDECIDED.name,
    )
    p.add_argument("--set-notes", "--notes", "-n", type=str, default="")
    p.add_argument("--dry-run", action="store_true", default=False)
    p.set_defaults(cmd=__cmd_slave_add)


def __cmd_slave_add(args: argparse.Namespace) -> None:
    slavery, save, newslaves = handle_many_slave_args(args)
    existing_slave_ids: Set[str] = set(slavery.slavesByID.keys())
    for newslave in newslaves:
        cid = newslave.core.id
        sid = SlaveInfo.CalculateSID(character_id=cid)
        with click.info(f"{newslave.getName()} ({sid}):"):
            if newslave.core.id in existing_slave_ids:
                click.warn("Already exists in slavery.")
            si = SlaveInfo.FromNPC(save, newslave)
            if si.plans != EPlans[args.set_plans]:
                click.info(f"Plans: {si.plans.name} => {EPlans[args.set_plans].name}")
                si.plans = EPlans[args.set_plans]
            if si.status != EStatus[args.set_status]:
                click.info(
                    f"Status: {si.status.name} => {EStatus[args.set_status].name}"
                )
                si.status = EStatus[args.set_status]
            if si.treatment != ETreatment[args.set_treatment]:
                click.info(
                    f"Treatment: {si.treatment.name} => {ETreatment[args.set_treatment].name}"
                )
                si.treatment = ETreatment[args.set_treatment]
            if len(args.set_tags) > 0:
                click.info(f"Tags: +{', '.join(args.set_tags)}")
                si.tags = set(args.set_tags)
            slavery.add(si)
    if args.dry_run:
        click.warn("--dry-run is set. Aborting.")
        return
    slavery.save(args.path)


def __register_slave_list(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser("list", help="list all slaves")
    add_slavery_yml_arg(p)
    p.set_defaults(cmd=__cmd_slave_list)


def __cmd_slave_list(args: argparse.ArgumentParser) -> None:
    slavery = Slavery()
    slavery.load(args.path)
    with click.info("All Slaves:"):
        for slave in slavery.allSlaves:
            click.info(f"{slave.name} ({slave.slaveID})")
            # with click.info(f'{slave.name} ({slave.slaveID}):'):
            #     with click.info('Info:'):
            #         click.info(f'Index:      {slave.index}')
            #         click.info(f'ID:         {slave.id}')
            #         click.info(f'SID:        {slave.slaveID}')
            #         click.info(f'Race:       {slave.race}')
            #         click.info(f'Species:    {slave.species}')
            #         click.info(f'Subspecies: {slave.subspecies}')
            #         with click.info(f'Names:'):
            #             click.info(f'Androgynous: {slave.names.androgynous}')
            #             click.info(f'Feminine:    {slave.names.feminine}')
            #             click.info(f'Masculine:   {slave.names.masculine}')
            #             click.info(f'Apparent:    {slave.name}')
            #     with click.info('Gender:'):
            #         click.info(f'Detected:   {slave.detectedGender.name}')
            #         click.info(f'Identity:   {slave.genderIdentity.name}')
            #         click.info(f'Femininity: {slave.femininity}')
            #     with click.info('Location:'):
            #         click.info(f'Current: {slave.currentLocName}')
            #         click.info(f'Home:    {slave.homeLocName}')
            #     with click.info('Stats:'):
            #         click.info(f'Affection: {slave.affection}')
            #         click.info(f'Obedience: {slave.obedience}')
            #     with click.info('Accounting:'):
            #         click.info(f'Plans: {slave.plans.name}')
            #         click.info(f'Status: {slave.status.name}')
            #         click.info(f'Treatment: {slave.treatment.name}')
            #         click.info(f'Tags: {slave.tags!r}')
            #         click.info(f'Notes: {slave.notes!r}')


def __register_slave_remove(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser("remove", help="remove a slave")
    add_many_slave_args(p)
    p.add_argument("--dry-run", action="store_true", default=False)
    p.set_defaults(cmd=__cmd_slave_remove)


def __cmd_slave_remove(args: argparse.Namespace) -> None:
    slavery, save, rmslaves = handle_many_slave_args(args)
    existing_slave_ids: Set[str] = set(slavery.slavesByID.keys())
    for rmslave in rmslaves:
        cid = rmslave.core.id
        sid = SlaveInfo.CalculateSID(character_id=cid)
        with click.info(f"{rmslave.getName()} ({sid}):"):
            if rmslave.core.id not in existing_slave_ids:
                click.warn("Is not in slavery.")
                continue
            si = slavery.slavesByID[rmslave.core.id]
            slavery.allSlaves.remove(si)
            slavery.slavesByID[rmslave.core.id]
            slavery.slavesByName[si.name.casefold()]
            click.success("Removed!")
    if args.dry_run:
        click.warn("--dry-run is set. Aborting.")
        return
    slavery.save(args.path)


def __register_slave_set(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser("set", help="set slave info")
    add_many_slave_args(p)
    p.add_argument("--set-tags", "--tags", "-T", type=str, nargs="*", default=[])
    p.add_argument(
        "--set-plans",
        "--plans",
        "-p",
        choices=sorted([x.name for x in EPlans]),
        default=EPlans.NONE.name,
    )
    p.add_argument(
        "--set-status",
        "--status",
        "-s",
        choices=sorted([x.name for x in EStatus]),
        default=EStatus.IDLE.name,
    )
    p.add_argument(
        "--set-treatment",
        "--treatment",
        "-t",
        choices=sorted([x.name for x in ETreatment]),
        default=ETreatment.UNDECIDED.name,
    )
    p.add_argument("--set-notes", "--notes", "-n", type=str, default="")
    p.add_argument("--dry-run", action="store_true", default=False)
    p.set_defaults(cmd=__cmd_slave_set)


E = TypeVar("E", bound=Enum)


def __set_if_arg_differs_enum(
    title: str, origval: E, newval: E | str, ET: Type[Enum]
) -> E:
    if isinstance(newval, str):
        newval = ET[newval]
    if newval is not None and origval != newval:
        click.info(f"{title}: {origval.name} -> {newval.name}")
        return newval
    return origval


def __cmd_slave_set(args: argparse.Namespace) -> None:
    slavery, save, npcs = handle_many_slave_args(args)
    existing_slave_ids: Set[str] = set(slavery.slavesByID.keys())
    for npc in npcs:
        cid = npc.core.id
        sid = SlaveInfo.CalculateSID(character_id=cid)
        with click.info(f"{npc.getName()} ({sid}):"):
            if npc.core.id not in existing_slave_ids:
                click.warn("Does not exist in slavery.")
                continue
            si = slavery.slavesByID[npc.core.id]

            si.plans = __set_if_arg_differs_enum(
                "Plans", si.plans, args.set_plans, EPlans
            )
            si.status = __set_if_arg_differs_enum(
                "Status", si.status, args.set_status, EStatus
            )
            si.treatment = __set_if_arg_differs_enum(
                "Treatment", si.treatment, args.set_treatment, ETreatment
            )
            if len(args.set_tags) > 0:
                click.info(f"Tags: +{', '.join(args.set_tags)}")
                si.tags = set(args.set_tags)
            # slavery.add(si)
    if args.dry_run:
        click.warn("--dry-run is set. Aborting.")
        return
    slavery.save(args.path)


def __register_slave_show(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser("show", help="Show information about a particular slave")
    add_slave_args(p)
    p.set_defaults(cmd=__cmd_slave_show)


def __cmd_slave_show(args: argparse.ArgumentParser) -> None:
    slavery, game, npc = handle_slave_args(args)
    slave = slavery.slavesByID[npc.core.id]
    with click.info(f"{slave.name} ({slave.slaveID}):"):
        with click.info("Info:"):
            click.info(f"Index:      {slave.index}")
            click.info(f"ID:         {slave.id}")
            click.info(f"SID:        {slave.slaveID}")
            click.info(f"Race:       {slave.race}")
            click.info(f"Species:    {slave.species}")
            click.info(f"Subspecies: {slave.subspecies}")
            with click.info(f"Names:"):
                click.info(f"Androgynous: {slave.names.androgynous}")
                click.info(f"Feminine:    {slave.names.feminine}")
                click.info(f"Masculine:   {slave.names.masculine}")
                click.info(f"Apparent:    {slave.name}")
        with click.info("Gender:"):
            click.info(f"Detected:   {slave.detectedGender.name}")
            click.info(f"Identity:   {slave.genderIdentity.name}")
            click.info(f"Femininity: {slave.femininity}")
        with click.info("Location:"):
            click.info(f"Current: {slave.currentLocName}")
            click.info(f"Home:    {slave.homeLocName}")
        with click.info("Stats:"):
            click.info(f"Affection: {slave.affection}")
            click.info(f"Obedience: {slave.obedience}")
        with click.info("Accounting:"):
            click.info(f"Plans: {slave.plans.name}")
            click.info(f"Status: {slave.status.name}")
            click.info(f"Treatment: {slave.treatment.name}")
            click.info(f"Tags: {slave.tags!r}")
            click.info(f"Notes: {slave.notes!r}")

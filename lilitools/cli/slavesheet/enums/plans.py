# @generated
import enum
class EPlans(enum.Enum):
    NONE = ''
    SELL = 's'
    KEEP = 'k'
    RELEASE = 'r'
    COW = 'c'

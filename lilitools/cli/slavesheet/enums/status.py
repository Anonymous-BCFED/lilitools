# @generated
import enum
class EStatus(enum.Enum):
    IDLE = '-'
    TRAINING = 't'
    MILKING = 'm'
    WORKING = 'w'
    REJOB = 'r'

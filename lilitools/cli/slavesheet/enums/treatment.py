from enum import Enum


class ETreatment(Enum):
    UNDECIDED='-'
    DEGRADE='d'
    UPLIFT='u'
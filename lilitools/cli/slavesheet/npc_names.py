
from typing import Any, Dict


class NPCNames:
    def __init__(self) -> None:
        self.androgynous: str = ''
        self.masculine: str = ''
        self.feminine: str = ''

    def fromDict(self, data: Dict[str, Any]) -> None:
        self.androgynous = data['androgynous']
        self.masculine = data['masculine']
        self.feminine = data['feminine']

    def toDict(self) -> Dict[str, Any]:
        return {
            'androgynous': self.androgynous,
            'masculine': self.masculine,
            'feminine': self.feminine
        }

from enum import IntEnum
from typing import Any, Dict, Set, Tuple

from lilitools.saves.math.point2i import Point2I


class ERoomPurpose(IntEnum):
    TRAINING = 0
    RECOVERY = 1
    UNKNOWN = 2
    COWSTALL = 3
    DOLLSTORAGE = 4
    COMPANIONS = 5
    GUEST = 6
    STAFF = 7


class Room:
    def __init__(self) -> None:
        self.worldID: str = ""
        self.position: Point2I = Point2I(0, 0)
        self.type: str = ""
        self.name: str = ""
        self.assigned: Set[str] = set()
        self.slots: int = 0
        self.purpose: ERoomPurpose = ERoomPurpose.TRAINING

    def toDict(self) -> Dict[str, Any]:
        return {
            "world-id": self.worldID,
            "pos": [self.position.x, self.position.y],
            "type": self.type,
            "name": self.name,
            "slots": self.slots,
            "purpose": self.purpose.value,
            "assigned": list(sorted(self.assigned)),
        }

    def fromDict(self, data: Dict[str, Any]) -> None:
        self.worldID = data["world-id"]
        self.position = Point2I(data["pos"][0], data["pos"][1])
        self.type = data["type"]
        self.name = data["name"]
        self.slots = data["slots"]
        self.purpose = ERoomPurpose(data["purpose"])
        self.assigned = set(data["assigned"])

    def key(self) -> Tuple[str, int, int]:
        return self.worldID, self.position.x, self.position.y

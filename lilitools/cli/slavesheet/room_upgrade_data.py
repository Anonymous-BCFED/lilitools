from typing import Dict, NamedTuple

__all__ = ["RoomUpgradeData", "ROOM_UPGRADES"]


class RoomUpgradeData(NamedTuple):
    id: str = ""
    slave_slots: int = 0
    guest_slots: int = 0


__RUD = RoomUpgradeData
ROOM_UPGRADES: Dict[str, RoomUpgradeData] = {
    "LILAYA_SLAVE_ROOM": __RUD("LILAYA_SLAVE_ROOM", 1, 0),
    "LILAYA_SLAVE_ROOM_DOUBLE": __RUD("LILAYA_SLAVE_ROOM_DOUBLE", 2, 0),
    "LILAYA_SLAVE_ROOM_QUADRUPLE": __RUD("LILAYA_SLAVE_ROOM_QUADRUPLE", 4, 0),
}

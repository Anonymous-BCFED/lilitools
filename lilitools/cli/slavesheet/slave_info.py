from __future__ import annotations
from functools import lru_cache
import hashlib

import statistics
import string
from typing import Any, Dict, Final, List, Optional, Set, Tuple, Type

from lilitools.cli.slavesheet.enums.plans import EPlans
from lilitools.cli.slavesheet.enums.status import EStatus
from lilitools.cli.slavesheet.enums.treatment import ETreatment
from lilitools.cli.slavesheet.npc_names import NPCNames
from lilitools.logging import ClickWrap
from lilitools.saves.character.body.body import ERaceStage
from lilitools.saves.character.gender import EGender
from lilitools.saves.character.npc import NPC
from lilitools.saves.game import Game
from lilitools.saves.location import Location

click = ClickWrap()


class SlaveInfo:
    HEADERS: Final[List[str]] = [
        # "ID",
        "Slave ID",
        "Name",
        "Species",
        "Gender Id.",
        "Calc. Gender",
        "Jobs",
        "Home Loc",
        "Current Loc",
        "Affection",
        "Obedience",
        "Value",
        "Value/Day",
        "Plans",
        "Status",
        "Treatment",
        "Tags",
        "Notes",
    ]
    MIN_SLAVEID: Final[int] = 0
    MAX_SLAVEID: Final[int] = 0xFF_FF_FF_FF_FF

    LICENSE_ALPHABET: Final[str] = string.ascii_uppercase + string.digits

    def __init__(self) -> None:
        self.index: int = 0
        self._slaveID: Optional[str] = None
        self.id: str = ""
        self.names: NPCNames = NPCNames()
        self.species: Optional[str] = None
        self.genderIdentity: EGender = EGender.N_NEUTER
        self.detectedGender: EGender = EGender.N_NEUTER
        self.jobs: List[str] = []
        self.jobHours: List[str] = ["IDLE"] * 24
        self.primaryJob: str = "IDLE"
        self.homeLoc: Location = Location()
        self.homeLocName: str = ""
        self.currentLoc: Location = Location()
        self.currentLocName: str = ""
        self.obedience: float = 0.0
        self.plans: EPlans = EPlans.NONE
        self.status: EStatus = EStatus.IDLE
        self.treatment: ETreatment = ETreatment.UNDECIDED
        self.notes: str = ""
        self.affection: float = 0.0

        self.calculatedRace: str = 'UNKNOWN'
        self.calculatedRaceStage: ERaceStage = ERaceStage.LESSER

        self.value: Optional[int] = None
        self.valuePerDay: Optional[int] = None
        self.race: Optional[str] = None
        self.subspecies: Optional[str] = None

        self.femininity: int = 0

        self.tags: Set[str] = set()

    @property
    def name(self) -> str:
        if self.femininity <= 39:
            return self.names.masculine
        elif self.femininity > 60:
            return self.names.feminine
        return self.names.androgynous

    def genSlaveID(self) -> None:
        # self._slaveID = int.from_bytes(os.urandom(32), 'little', signed=False)
        if self._slaveID is None:
            self._slaveID = self.CalculateSID(self.id)
            # print(f"{self.id} now has SID == {self._slaveID} == {self.slaveID}")

    @property
    @lru_cache
    def slaveID(self) -> str:
        self.genSlaveID()
        return self._slaveID

    @classmethod
    def CalculateSID(cls, character_id: str) -> str:
        with click.debug(f"CalculateSID: {character_id!r}"):
            i: int = int.from_bytes(
                hashlib.blake2b(character_id.encode("utf-8"), digest_size=5).digest(),
                "little",
                signed=False,
            )
            click.debug(f"i: {i}")
            l: int = len(cls.LICENSE_ALPHABET)
            o: str = ""
            assert i >= 0
            while i > 0:
                i, r = divmod(i, l)
                o = cls.LICENSE_ALPHABET[r] + o
            o = o.rjust(8, cls.LICENSE_ALPHABET[0])
            o = o[0:4] + "-" + o[4:8]
            click.debug(f"o: {o!r}")
        return o

    def toRow(
        self,
    ) -> Tuple[
        str,
        str,
        str,
        str,
        str,
        str,
        str,
        float,
        float,
        int,
        int,
        str,
        str,
        str,
        str,
        str,
    ]:
        return (
            self.slaveID,
            self.name,
            self.species,
            self.genderIdentity.slang,
            self.detectedGender.slang,
            ("-" if not len(self.jobs) else ", ".join(self.jobs)),
            self.homeLocName,
            self.currentLocName,
            float(self.affection),
            float(self.obedience),
            int(self.value) if self.value else "",
            int(self.valuePerDay) if self.valuePerDay else '',
            self.plans.name.title(),
            self.status.name.title(),
            self.treatment.name.title(),
            ", ".join(self.tags),
            self.notes,
        )

    def toDict(self) -> Dict[str, Any]:
        return {
            "index": self.index,
            "id": self.id,
            "sid": self._slaveID,
            "sidstr": self.slaveID,
            "names": self.names.toDict(),
            "species": self.species,
            "genderIdentity": self.genderIdentity.name,
            "detectedGender": self.detectedGender.name,
            "jobs": self.jobs,
            "homeLoc": {"name": self.homeLocName, **self.homeLoc.toDict()},
            "currentLoc": {"name": self.currentLocName, **self.currentLoc.toDict()},
            "obedience": self.obedience,
            "plans": self.plans.value,
            "status": self.status.value,
            "notes": self.notes,
            "femininity": self.femininity,
            "affection": self.affection,
            "value": self.value,
            'vpd': self.valuePerDay,
            "race": self.race,
            "subspecies": self.subspecies,
            "tags": list(self.tags),
            "jobHours": list(self.jobHours),
            "primaryJob": self.primaryJob,
            "treatment": self.treatment.value,
            'calculatedRace':self.calculatedRace,
            'calculatedRaceStage':self.calculatedRaceStage.name,
        }

    def fromDict(self, data: Dict[str, Any]) -> None:
        self.index = int(data["index"])
        self._slaveID = data["sid"]
        self.id = data["id"]
        # self.sid = data["sid"]
        self.names.fromDict(data["names"])
        self.species = data["species"]
        self.genderIdentity = EGender[data["genderIdentity"]]
        self.detectedGender = EGender[data["detectedGender"]]
        self.jobs = data["jobs"]
        self.affection = float(data["affection"])
        self.obedience = data["obedience"]
        self.plans = EPlans(data["plans"])
        self.notes = data["notes"]
        self.femininity = int(data["femininity"])
        self.status = EStatus(data["status"])

        self.race = data.get("race")
        self.subspecies = data.get("subspecies")
        self.value = data.get("value")
        self.valuePerDay = data.get("vpd")

        self.tags = set(data.get("tags", []))
        self.jobHours = list(data.get("jobHours", []))
        self.primaryJob = data.get("primaryJob", "IDLE")
        self.treatment = ETreatment(data.get("treatment", ETreatment.UNDECIDED.value))

        self.calculatedRace = data.get('calculatedRace','HUMAN')
        self.calculatedRaceStage = ERaceStage[data.get("calculatedRaceStage", "LESSER")]

        d: Optional[dict[str, Any]]
        if (d := data.get("homeLoc")) is not None and isinstance(d, dict):
            self.homeLocName = d.get("name")
            self.homeLoc = Location()
            self.homeLoc.fromDict(d)
        if (d := data.get("currentLoc")) is not None and isinstance(d, dict):
            self.currentLocName = d.get("name")
            self.currentLoc = Location()
            self.currentLoc.fromDict(d)

    @classmethod
    def FromNPC(cls: Type[SlaveInfo], g: Game, npc: NPC) -> SlaveInfo:
        si = cls()
        si.index = g.playerCharacter.slavery.slavesOwned.index(npc.core.id)
        si.id = npc.core.id
        si.affection = npc.relationships.get("PlayerCharacter", 0.0)

        si.femininity = npc.body.core.femininity

        si.names.androgynous = npc.core.name.androgynous
        si.names.masculine = npc.core.name.masculine
        si.names.feminine = npc.core.name.feminine
        si.species = ""
        si.genderIdentity = npc.core.genderIdentity or EGender.N_NEUTER
        si.detectedGender = npc.getCalculatedGender(ignore_visibility=True)
        si.jobHours = npc.slavery.slaveAssignedJobs
        fj = list(filter(lambda x: x != "IDLE", npc.slavery.slaveAssignedJobs))
        if len(fj) > 0:
            si.primaryJob = statistics.mode(fj)
        else:
            si.primaryJob = "IDLE"
        si.jobs = list(filter(lambda x: x != "IDLE", set(si.jobHours)))
        c = g.worlds.getCellByWorldCoords(
            npc.locationInformation.homeWorldLocation,
            npc.locationInformation.homeLocation.x,
            npc.locationInformation.homeLocation.y,
        )
        si.homeLoc = Location.FromCell(c)
        si.homeLocName = c.getName()
        c = g.worlds.getCellByWorldCoords(
            npc.locationInformation.worldLocation,
            npc.locationInformation.location.x,
            npc.locationInformation.location.y,
        )
        si.currentLoc = Location.FromCell(c)
        si.currentLocName = c.getName()
        si.obedience = npc.core.obedience
        si.valuePerDay = npc.getSlaveIncomePerDay(g)
        if npc.helpfulInformation is not None:
            si.value = npc.helpfulInformation.slaveValue
            si.race = npc.helpfulInformation.race
            si.subspecies = npc.helpfulInformation.subspecies
            si.species = npc.helpfulInformation.fullSpeciesName

        si.calculatedRace = npc.body.getRaceFromPartWeighting(demon_override=True)
        si.calculatedRaceStage = npc.body.getRaceStageFromPartWeighting()

        if si._slaveID is not None:
            si.genSlaveID()

        return si

import os
from pathlib import Path
from typing import Any, Dict, List, Optional, Set, Tuple

import tabulate
from ruamel.yaml import YAML as Yaml

from lilitools.cli.slavesheet.enums.plans import EPlans
from lilitools.cli.slavesheet.enums.status import EStatus
from lilitools.cli.slavesheet.enums.treatment import ETreatment
from lilitools.cli.slavesheet.room import ERoomPurpose, Room
from lilitools.cli.slavesheet.room_upgrade_data import ROOM_UPGRADES
from lilitools.cli.slavesheet.slave_info import SlaveInfo
from lilitools.regex_consts import REG_RECOVERY_ROOM, REG_TRAINING_ROOM
from lilitools.saves.enums.worlds import EWorld
from lilitools.saves.game import Game
from lilitools.saves.utils import loadSaveFrom
from lilitools.saves.world.cell import Cell

yaml = Yaml(typ="rt")
SLAVEFILE = Path("slaves.yml")


class Slavery:
    def __init__(self) -> None:
        self.savefile: Path = SLAVEFILE
        self.assignedSlaveIDs: Set[int] = set()
        self.allSlaves: List[SlaveInfo] = []
        self.slavesByID: Dict[str, SlaveInfo] = {}
        self.slavesByName: Dict[str, SlaveInfo] = {}
        self.allTrainingRooms: List[Room] = []
        self.trainingRoomsByID: Dict[Tuple[str, int, int], Room] = {}
        self.trainingRoomsByName: Dict[str, Room] = {}

    def _report_version_upgrade(self, start: int, end: int) -> None:
        print(f"Upgrading slavedata: {start} -> {end}")

    def load(self, filename: Path) -> None:
        with filename.open("r") as f:
            data = yaml.load(f)
            self._upgrade_1_to_2(data)
            self._upgrade_2_to_3(data)
            self._upgrade_3_to_4(data)
            self._upgrade_4_to_5(data)
            self._upgrade_5_to_6(data)
            self._upgrade_6_to_7(data)
            self._upgrade_7_to_8(data)

            assert data["version"] == 8

            self.savefile = Path(data.get("save-file", str(SLAVEFILE)))
            rooms = data.get("training-rooms", [])
            if isinstance(rooms, dict):
                rooms=[x for x in rooms.values()]
            for rdata in rooms:
                r = Room()
                # print(repr(rdata))
                r.fromDict(rdata)
                self.addTrainingRoom(r)
            for sdata in data["slaves"]:
                s = SlaveInfo()
                s.fromDict(sdata)
                self.add(s)

    def _upgrade_7_to_8(self, data: dict) -> None:
        v: dict
        if data["version"] == 7:
            self._report_version_upgrade(7, 8)
            data["slaves"] = [s for s in data['slaves'].values()]
            data["version"] = 8

    def _upgrade_6_to_7(self, data: dict) -> None:
        v: dict
        if data["version"] == 6:
            self._report_version_upgrade(6, 7)
            data["training-rooms"] = []
            data["version"] = 7

    def _upgrade_5_to_6(self, data: dict) -> None:
        v: dict
        if data["version"] == 5:
            self._report_version_upgrade(5, 6)
            newslaves = {}
            for k, v in data["training-rooms"].items():
                if "type" not in v:
                    v["type"] = ""
                if "purpose" not in v:
                    v["purpose"] = ERoomPurpose.TRAINING
                newslaves[k] = v
            data["training-rooms"] = newslaves
            data["version"] = 6

    def _upgrade_4_to_5(self, data: dict) -> None:
        v: dict
        if data["version"] == 4:
            self._report_version_upgrade(4, 5)
            newslaves = {}
            for k, v in data["slaves"].items():
                if "sid" not in v:
                    v["sid"] = 0
                newslaves[k] = v
            data["slaves"] = newslaves
            data["version"] = 5

    def _upgrade_3_to_4(self, data: dict) -> None:
        v: dict
        if data["version"] == 3:
            self._report_version_upgrade(3, 4)
            newslaves = {}
            for k, v in data["slaves"].items():
                if "index" not in v:
                    v["index"] = -1
                newslaves[k] = v
            data["slaves"] = newslaves
            data["version"] = 4

    def _upgrade_2_to_3(self, data: dict) -> None:
        v: dict
        if data["version"] == 2:
            self._report_version_upgrade(2, 3)
            newslaves = {}
            for k, v in data["slaves"].items():
                if "home" in v:
                    del v["home"]
                newslaves[k] = v
            data["slaves"] = newslaves
            data["version"] = 3

    def _upgrade_1_to_2(self, data: dict) -> None:
        v: dict
        if data["version"] == 1:
            self._report_version_upgrade(1, 2)
            newslaves = {}
            for k, v in data["slaves"].items():
                v["genderIdentity"] = v["gender"]
                v["detectedGender"] = v["gender"]
                del v["gender"]
                v["affection"] = float(v.get("affection", 0.0))
                v["plans"] = EPlans(v.get("plans", EPlans.NONE.value)).value
                v["status"] = EStatus(v.get("status", EStatus.IDLE.value)).value
                v["treatment"] = ETreatment(
                    v.get("treatment", ETreatment.UNDECIDED.value)
                ).value
                newslaves[k] = v
            data["slaves"] = newslaves
            data["version"] = 2

    def addTrainingRoom(self, r: Room) -> None:
        self.allTrainingRooms.append(r)
        self.trainingRoomsByName[r.name.casefold()] = r
        self.trainingRoomsByID[r.key()] = r

    def rmTrainingRoom(self, r: Room) -> None:
        self.allTrainingRooms.remove(r)
        del self.trainingRoomsByName[r.name.casefold()]
        del self.trainingRoomsByID[r.key()]

    def getTRByName(self, name: str) -> Room:
        return self.trainingRoomsByName[name.casefold()]

    def clear(self, slaves: bool = True, training_rooms: bool = True) -> None:
        if slaves:
            self.allSlaves.clear()
            self.slavesByID.clear()
            self.slavesByName.clear()

        if training_rooms:
            self.allTrainingRooms.clear()
            self.trainingRoomsByName.clear()
            self.trainingRoomsByID.clear()

    def add(self, slave: SlaveInfo) -> None:
        self.allSlaves.append(slave)
        self.slavesByID[slave.id] = slave
        self.slavesByName[slave.name.casefold()] = slave

    def getByName(self, name: str) -> SlaveInfo:
        return self.slavesByName[name.casefold()]

    def save(self, filename: Path) -> None:
        data = {
            "version": 8,
            "save-file": str(self.savefile.absolute()),
            "training-rooms": [r.toDict() for r in self.allTrainingRooms],
            "slaves": [s.toDict() for s in self.allSlaves],
        }
        tmpf = filename.with_suffix(".yml~")
        try:
            with tmpf.open("w") as f:
                yaml.dump(data, f)
            os.replace(tmpf, filename)
        finally:
            if tmpf.is_file():
                os.unlink(tmpf)

    def importSlavesFromSaveFile(self, filename: Path) -> None:
        self.savefile = filename.absolute()
        self.importSlavesFromGame(loadSaveFrom(filename))

    def importSlavesFromGame(self, g: Game) -> None:
        oldslaves: Dict[str, SlaveInfo] = dict(self.slavesByID)
        newslaves: List[SlaveInfo] = []

        workingSlaves: Set[str] = g.slavery.getAllWorkingSlaves()
        allMySlaves: Set[str] = set()
        # print('working', len(workingSlaves), repr(workingSlaves))
        if g.playerCharacter.slavery.slavesOwned is not None:
            allMySlaves = set(g.playerCharacter.slavery.slavesOwned)
            # print('all?', len(allMySlaves), repr(allMySlaves))
            nonworking = allMySlaves - workingSlaves
            # print("NONWORKING", len(nonworking), repr(nonworking))
            # all = (allMySlaves|workingSlaves)
            # print('ALL', len(all), repr(all))
        for npc in g.npcs:
            if npc is None:
                continue
            if npc.core.id not in allMySlaves:
                continue
            si = SlaveInfo.FromNPC(g, npc)
            newslaves.append(si)

        oldslave: SlaveInfo
        newslave: SlaveInfo
        self.clear(training_rooms=False)
        for newslave in newslaves:
            if newslave.id in oldslaves.keys():
                oldslave = oldslaves[newslave.id]
                newslave.notes = oldslave.notes
                newslave.plans = oldslave.plans
                newslave.status = oldslave.status
                newslave.treatment = oldslave.treatment
                newslave.tags = oldslave.tags
                if oldslave.species is not None and oldslave.species != "":
                    newslave.species = oldslave.species
            else:
                newslave.notes = ""
                newslave.plans = EPlans.NONE
                newslave.status = EStatus.IDLE
                newslave.treatment = ETreatment.UNDECIDED
                newslave.tags = set()
            newslave.tags = set(filter(lambda x: x != "", newslave.tags))
            self.add(newslave)

    def importTrainingRoomsFromSaveFile(self, filename: Path) -> None:
        self.importTrainingRoomsFromGame(loadSaveFrom(filename))

    def importTrainingRoomsFromGame(self, g: Game) -> None:
        cellRenames: Dict[str, str] = {}
        currentTrainingRoomID: Dict[ERoomPurpose, int] = {}
        for trp in ERoomPurpose:
            currentTrainingRoomID[trp] = 1

        def _handleTrainingRoom(
            cell: Cell, slots: int, purpose: ERoomPurpose
        ) -> None:
            tr = Room()
            tr.worldID = cell.worldID
            tr.position.x = cell.location.x
            tr.position.y = cell.location.y
            # tr.place_counter_position = cell.place_counter_position
            tr.slots = slots
            tr.name = cell.place.name
            tr.type = cell.place.type
            tr.purpose = purpose
            if tr.name.casefold() not in self.trainingRoomsByName.keys():
                self.addTrainingRoom(tr)
                print(
                    f"Discovered Training Room {tr.name!r} ({tr.slots} slots, {tr.purpose})"
                )
            else:
                etr = self.trainingRoomsByName[tr.name.casefold()]
                etr.slots = tr.slots
                etr.type = tr.type
            # rn2=''
            # match tr.purpose:
            #     case ETrainingRoomPurpose.TRAINING:
            #         rn2 = f"Training Room {currentTrainingRoomID[tr.purpose]}"
            #     case ETrainingRoomPurpose.RECOVERY:
            #         rn2 = f"Recovery Room {currentTrainingRoomID[tr.purpose]}"
            # if rn2!=tr.name:
            #     print(f'RENAME: {tr.name!r} => {rn2!r}')
            currentTrainingRoomID[tr.purpose] += 1

        for worldID in [
            EWorld.LILAYAS_HOUSE_FIRST_FLOOR.name,
            EWorld.LILAYAS_HOUSE_GROUND_FLOOR.name,
            "acexp_dungeon",
        ]:
            for cell in g.worlds.worlds[worldID].cells:
                if (
                    cell.place is not None
                    and cell.place.name is not None
                    and cell.place.placeUpgrades is not None
                ):

                    for upgradeName, upgradeInfo in ROOM_UPGRADES.items():
                        if upgradeName in cell.place.placeUpgrades:
                            if (
                                m := REG_TRAINING_ROOM.search(cell.place.name)
                            ) is not None:
                                _handleTrainingRoom(
                                    cell,
                                    upgradeInfo.slave_slots,
                                    ERoomPurpose.TRAINING,
                                )
                            elif (
                                m := REG_RECOVERY_ROOM.search(cell.place.name)
                            ) is not None:
                                _handleTrainingRoom(
                                    cell,
                                    upgradeInfo.slave_slots,
                                    ERoomPurpose.RECOVERY,
                                )

                    if cell.place.type == "LILAYA_HOME_DUNGEON_CELL":
                        if (m := REG_TRAINING_ROOM.search(cell.place.name)) is not None:
                            _handleTrainingRoom(cell, 4, ERoomPurpose.TRAINING)
                        elif (
                            m := REG_RECOVERY_ROOM.search(cell.place.name)
                        ) is not None:
                            _handleTrainingRoom(cell, 4, ERoomPurpose.RECOVERY)

    def buildHeader(self) -> List[str]:
        return SlaveInfo.HEADERS

    def buildRows(self, slaves: List[SlaveInfo]) -> List[List[str]]:
        rows = []
        rows.append(list(self.buildHeader()))
        # print(len(slaves))
        for slave in slaves:
            rows.append(list(map(str, slave.toRow())))
        return rows

    def buildTrainingReport(self) -> List[List[str]]:
        rooms: Dict[str, List[Optional[SlaveInfo]]] = {}
        slave: Optional[SlaveInfo]

        for room in self.allTrainingRooms:
            rooms[room.name] = [None] * room.slots
        for slave in self.allSlaves:
            # print(slave.name, slave.home)
            if slave.homeLocName in rooms.keys():
                for i in range(len(rooms[slave.homeLocName])):
                    if rooms[slave.homeLocName][i] is None:
                        rooms[slave.homeLocName][i] = slave
                        break
        # print(repr(rooms))
        rows = []
        hdr: List[str] = self.buildHeader()
        hdrlen: int = len(hdr)
        roomColumn: int = hdr.index("Home")
        rows.append(hdr)
        for room in sorted(self.allTrainingRooms, key=lambda r: r.id):
            for i in range(room.slots):
                if (slave := rooms[room.name][i]) is None:
                    row = [""] * hdrlen
                    row[roomColumn] = room.name
                    rows.append(row)
                else:
                    rows.append(list(map(str, slave.toRow())))
        return rows

    def getAPISlaveRows(self) -> List[Dict[str, Any]]:
        o = []
        for slave in self.allSlaves:
            o.append(slave.toDict())
        return o

    def getAPITrainingRoomRows(self) -> List[Dict[str, Any]]:
        o = []
        for tr in self.allTrainingRooms:
            o.append(tr.toDict())
        return o

    def tabulate(self, format: str = "github") -> str:
        return tabulate.tabulate(
            self.buildRows(self.allSlaves), headers="firstrow", tablefmt=format
        )

# THESE SHOULD ONLY BE USED FOR PRESENTATION!
# Internally, we use basic python types to avoid issues,
# but carefully document units where applicable.
#
# Unfortunately, LT uses a mix of unit systems internally

import argparse
import locale
import os
from fractions import Fraction
from typing import Dict, List, Optional

from pint import Quantity, Unit, UnitRegistry

from lilitools.cli.os_support import CONSOLE_SUPPORTS_UNICODE_FRACTIONS
from lilitools.cli.utils import arg_to_optional_float
from lilitools.logging import ClickWrap
from lilitools.saves.character.body.enums.cupsize import ECupSize

click = ClickWrap()

__all__ = [
    "centimeterToLength",
    "cm2len",
    "CUP_SIZE_MEASUREMENT",
    "f2pct",
    "floatToPercent",
    "formatAsFraction",
    "getPresentingUnitSystem",
    "handle_unit_args",
    "in2cupsize",
    "inchToCupSize",
    "InvalidUnitSystemException",
    "millilitresToVolumePerDay",
    "ml2vol",
    "ml2volday",
    "q2str",
    "QuantityOrUnitLike",
    "quantityToShortString",
    "register_unit_args",
    "setPresentingUnitSystem",
    "SI",
    "UNITS",
    "US",
]

UNITS = UnitRegistry()
ALL_UNIT_SUFFIXES = dict()

QuantityOrUnitLike = Quantity | Unit


class BaseUnitSystemProvider:
    ID: str = ""
    VOLUME_UNITS: List[QuantityOrUnitLike] = []
    LENGTH_UNITS: List[QuantityOrUnitLike] = []
    MASS_UNITS: List[QuantityOrUnitLike] = []
    UNIT_SUFFIXES: Dict[QuantityOrUnitLike, str] = {}

    def __init__(self) -> None:
        pass

    def _selectedPreferredUnitFrom(
        self, v: Quantity, units: List[QuantityOrUnitLike]
    ) -> Quantity:
        for unit in units:
            cv = v.to(unit)
            if abs(cv.magnitude) >= 1:
                return cv
        return v.to(units[-1])

    def _asStr(self, quantity: Quantity, unit_override: Optional[str] = None) -> str:
        if quantity is None:
            return None
        return q2str(
            quantity, unit_override or self.UNIT_SUFFIXES.get(UNITS(str(quantity)))
        )

    def asLength(
        self, value: float | int | None, origin_unit: QuantityOrUnitLike
    ) -> Optional[Quantity]:
        if value is None:
            return None
        return self._selectedPreferredUnitFrom(
            Quantity(value, origin_unit), self.LENGTH_UNITS
        )

    def asLengthStr(
        self,
        value: float | int | None,
        origin_unit: QuantityOrUnitLike,
        unit_override: Optional[str] = None,
    ) -> str:
        return self._asStr(self.asLength(value, origin_unit), unit_override)

    def asVolume(
        self, value: float | int | None, origin_unit: QuantityOrUnitLike
    ) -> Optional[Quantity]:
        if value is None:
            return None
        return self._selectedPreferredUnitFrom(
            Quantity(value, origin_unit), self.VOLUME_UNITS
        )

    def asVolumeStr(
        self,
        value: float | int | None,
        origin_unit: QuantityOrUnitLike,
        unit_override: Optional[str] = None,
    ) -> str:
        return self._asStr(self.asVolume(value, origin_unit), unit_override)

    def asVolumePerDay(
        self, value: float | int | None, origin_unit: QuantityOrUnitLike
    ) -> Optional[Quantity]:
        if value is None:
            return None
        return self._selectedPreferredUnitFrom(
            Quantity(value, origin_unit / UNITS.day),
            [x / UNITS.day for x in self.VOLUME_UNITS],
        )

    def asVolumePerDayStr(
        self,
        value: float | int | None,
        origin_unit: QuantityOrUnitLike,
        unit_override: Optional[str] = None,
    ) -> str:
        return self._asStr(self.asVolumePerDay(value, origin_unit), unit_override)

    def asMass(
        self, value: float | int | None, origin_unit: QuantityOrUnitLike
    ) -> Optional[Quantity]:
        if value is None:
            return None
        return self._selectedPreferredUnitFrom(
            Quantity(value, origin_unit), self.MASS_UNITS
        )

    def asMassStr(
        self,
        value: float | int | None,
        origin_unit: QuantityOrUnitLike,
        unit_override: Optional[str] = None,
    ) -> str:
        return self._asStr(self.asMass(value, origin_unit), unit_override)


class SIUnitSystemProvider(BaseUnitSystemProvider):
    ID = "SI"
    VOLUME_UNITS = [
        UNITS.litre,
        UNITS.millilitre,
    ]
    LENGTH_UNITS = [
        UNITS.kilometer,
        UNITS.meter,
        UNITS.centimeter,
        UNITS.millimeter,
    ]
    MASS_UNITS = [
        UNITS.metric_ton,
        UNITS.kilogram,
        UNITS.gram,
        UNITS.milligram,
    ]


SI = SIUnitSystemProvider()
ALL_UNIT_SUFFIXES.update(SI.UNIT_SUFFIXES)


class USUnitSystemProvider(BaseUnitSystemProvider):
    ID = "US"
    VOLUME_UNITS = [
        UNITS.gallon,
        UNITS.pint,
        UNITS.quart,
        UNITS.floz,
    ]
    LENGTH_UNITS = [
        UNITS.mile,
        UNITS.yard,
        UNITS.foot,
        UNITS.inch,
    ]
    MASS_UNITS = [
        UNITS.ton,
        UNITS.pound,
        UNITS.ounce,
    ]

    UNIT_SUFFIXES = {
        UNITS.gallon: "gal",
        UNITS.gallon / UNITS.day: "gal/day",
        UNITS.pint: "pt",
        UNITS.pint / UNITS.day: "pt/day",
        UNITS.quart: "qt",
        UNITS.quart / UNITS.day: "qt/day",
        UNITS.floz: "fl oz",
        UNITS.floz / UNITS.day: "fl oz/day",
        UNITS.mile: "mi",
        UNITS.mile / UNITS.day: "mi/day",
        UNITS.yard: "yd",
        UNITS.yard / UNITS.day: "yd/day",
        UNITS.foot: "ft",
        UNITS.foot / UNITS.day: "ft/day",
        UNITS.inch: "in",
        UNITS.inch / UNITS.day: "in/day",
        UNITS.UK_ton: "ton",
        UNITS.UK_ton / UNITS.day: "ton/day",
        UNITS.UK_hundredweight: "cwt",
        UNITS.UK_hundredweight / UNITS.day: "cwt/day",
        UNITS.pound: "lb",
        UNITS.pound / UNITS.day: "lb/day",
        UNITS.ounce: "oz",
        UNITS.ounce / UNITS.day: "oz/day",
    }

    def asLengthStr(
        self,
        value: float | int | None,
        origin_unit: Quantity | Unit,
        unit_override: str | None = None,
    ) -> str:
        l = Quantity(value, origin_unit)
        # print(repr(l))
        inch = l.to(UNITS.inch)
        # print(repr(inch))
        INCHES_PER_FOOT = (1 * UNITS.foot).to(UNITS.inch)
        # print(repr(INCHES_PER_FOOT))
        feet, inch = divmod(inch.m, INCHES_PER_FOOT.m)
        if feet > 0 and inch > 0:
            return f"{feet:,.0f} ft {inch:,.0f} in"
        return super().asLengthStr(value, origin_unit, unit_override)


US = USUnitSystemProvider()
ALL_UNIT_SUFFIXES.update(US.UNIT_SUFFIXES)


class UKUnitSystemProvider(BaseUnitSystemProvider):
    ID = "UK"
    VOLUME_UNITS = [
        UNITS.imperial_gallon,
        UNITS.imperial_pint,
        UNITS.imperial_quart,
        UNITS.imperial_floz,
    ]
    LENGTH_UNITS = USUnitSystemProvider.LENGTH_UNITS  # The saaaame
    asLengthStr = USUnitSystemProvider.asLengthStr

    # AVDP with some fuckery
    MASS_UNITS = [
        UNITS.UK_ton,
        UNITS.UK_hundredweight,
        UNITS.pound,
        UNITS.ounce,
    ]

    UNIT_SUFFIXES = {
        UNITS.imperial_gallon: "gal",
        UNITS.imperial_gallon / UNITS.day: "gal/day",
        UNITS.imperial_pint: "pt",
        UNITS.imperial_pint / UNITS.day: "pt/day",
        UNITS.imperial_quart: "qt",
        UNITS.imperial_quart / UNITS.day: "qt/day",
        UNITS.imperial_floz: "fl oz",
        UNITS.imperial_floz / UNITS.day: "fl oz/day",
        UNITS.mile: "mi",
        UNITS.mile / UNITS.day: "mi/day",
        UNITS.yard: "yd",
        UNITS.yard / UNITS.day: "yd/day",
        UNITS.foot: "ft",
        UNITS.foot / UNITS.day: "ft/day",
        UNITS.inch: "in",
        UNITS.inch / UNITS.day: "in/day",
        UNITS.UK_ton: "ton",
        UNITS.UK_ton / UNITS.day: "ton/day",
        UNITS.UK_hundredweight: "cwt",
        UNITS.UK_hundredweight / UNITS.day: "cwt/day",
        UNITS.pound: "lb",
        UNITS.pound / UNITS.day: "lb/day",
        UNITS.ounce: "oz",
        UNITS.ounce / UNITS.day: "oz/day",
    }


UK = UKUnitSystemProvider()
ALL_UNIT_SUFFIXES.update(UK.UNIT_SUFFIXES)

_PRESENTING_UNITS: Optional[BaseUnitSystemProvider] = None


class InvalidUnitSystemException(Exception):
    pass


def setPresentingUnitSystem(unit_system_id: str) -> BaseUnitSystemProvider:
    global _PRESENTING_UNITS, SI, US
    match unit_system_id.casefold():
        case "us":
            _PRESENTING_UNITS = US
        case "uk" | "imperial":
            _PRESENTING_UNITS = UK
        case "si" | "metric":
            _PRESENTING_UNITS = SI
        case _:
            raise InvalidUnitSystemException()
    return getPresentingUnitSystem()


def getPresentingUnitSystem() -> BaseUnitSystemProvider:
    global _PRESENTING_UNITS
    if _PRESENTING_UNITS is None:
        locale.setlocale(locale.LC_ALL, "")
        # Roughly do what this post did. https://stackoverflow.com/a/7860788
        if "LILITOOLS_MEASUREMENT_SYSTEM" in os.environ:
            setPresentingUnitSystem(os.environ["LILITOOLS_MEASUREMENT_SYSTEM"])
        elif "LC_MEASUREMENT" in os.environ:
            # LC_MEASUREMENT = "en_US.UTF-8"
            _LC_MEASUREMENT = os.environ["LC_MEASUREMENT"]
            _setPresentingUnitSystemBasedOnCountryCode(
                "LC_MEASUREMENT", _LC_MEASUREMENT.split(".")[0].split("_")[-1]
            )
        elif "LC_CTYPE" in os.environ:
            _LC_CTYPE = os.environ["LC_CTYPE"]
            _setPresentingUnitSystemBasedOnCountryCode(
                "LC_CTYPE", _LC_CTYPE.split(".")[0].split("_")[-1]
            )
        else:
            _LC_CTYPE_TUPLE = locale.getlocale(locale.LC_CTYPE)
            if _LC_CTYPE_TUPLE[0] is None:
                _PRESENTING_UNITS = SI
                click.warn(
                    f"We are automatically choosing to use {_PRESENTING_UNITS} (--unit-system=si) due to a lack of locale information from your OS.  To change this, set --unit-system to either si or us, or fix your OS locale."
                )
            else:
                _setPresentingUnitSystemBasedOnCountryCode(
                    "getlocale(LC_CTYPE)",
                    _LC_CTYPE_TUPLE[0].split(".")[0].split("_")[-1],
                )
    assert _PRESENTING_UNITS is not None
    return _PRESENTING_UNITS


def _setPresentingUnitSystemBasedOnCountryCode(source: str, cc: str) -> None:
    global _PRESENTING_UNITS
    sysname: str
    match cc:
        case "US" | "LR" | "MM":
            _PRESENTING_UNITS = US
            sysname = "us"
        case _:
            _PRESENTING_UNITS = SI
            sysname = "si"
    click.warn(
        f"We are automatically choosing to use {_PRESENTING_UNITS} (--unit-system={sysname!r}) based on the country code in {source} ({cc!r}).  To change this, set --unit-system to either si, us, or uk; or fix your OS locale."
    )
    if sysname == "us":
        click.warn(
            f"The US customary system (us) is *not* the imperial system used in the game. You will see slight differences compared to what is seen in-game. To change this, set --unit-system=uk to change to the UK imperial system."
        )


def quantizeRound(value: float, numerator: int, denominator: int):
    ratio = float(numerator) / denominator
    return (value + ratio / 2.0) // ratio * ratio


def formatAsFraction(
    v: float,
    quantize_numerator: int = 1,
    quantize_denominator: int = 4,
    ascii_slash: Optional[bool] = None,
) -> str:
    if ascii_slash is None:
        ascii_slash = not CONSOLE_SUPPORTS_UNICODE_FRACTIONS
    slash = "/" if ascii_slash else "\u2044"
    f = Fraction(
        quantizeRound(v, quantize_numerator, quantize_denominator)
    ).limit_denominator()
    w, l = divmod(f.numerator, f.denominator)
    # print(v,f,w,l)
    f = Fraction(l / f.denominator).limit_denominator()
    o: str = ""
    if w > 0 or w < 0:
        o += f"{w:,}"
    if f.numerator > 0 and f.numerator != f.denominator:
        if len(o) > 0:
            o += " "
        o += f"{f.numerator}{slash}{f.denominator}"
    if len(o) == 0:
        return "0"
    return o


def quantityToShortString(
    q: Quantity,
    unit_override: Optional[str] = None,
    dfmt: str = ",.0f",
    fractional: bool = True,
    quantize_numerator: int = 1,
    quantize_denominator: int = 4,
    ascii_slash: Optional[bool] = None,
) -> str:
    # return f'{q:~,P}'

    u = unit_override or ALL_UNIT_SUFFIXES.get(UNITS(str(q))) or f"{q.units:P~}"
    if fractional:
        return f"{formatAsFraction(q.magnitude,quantize_numerator,quantize_denominator,ascii_slash)} {u}"
    return f"{q.magnitude:{dfmt}} {u}"


q2str = quantityToShortString


def floatToPercent(f: float) -> str:
    return q2str(Quantity(f * 100.0, UNITS.percent))


f2pct = floatToPercent


def getVolPerDay() -> Quantity:
    return UNITS.millilitre / UNITS.day


def vol2ml(a: str) -> int:
    """
    Convert arbitrary volume (15gal, 1pint) to millilitres
    """
    return int(round(UNITS(a).to(UNITS.millilitre).magnitude))


def millilitresToVolumePerDay(i: int | float) -> str:
    pu = getPresentingUnitSystem()
    return q2str(pu.asVolumePerDay(i, UNITS.millilitre))


ml2volday = millilitresToVolumePerDay


def millilitresToVolume(i: int | float) -> str:
    pu = getPresentingUnitSystem()
    return q2str(pu.asVolume(i, UNITS.millilitre))


ml2vol = millilitresToVolume


def inchToCupSize(i: int | float) -> str:
    for cs in ECupSize:
        if cs.getMeasurement() == i:
            return f'{cs.getCupSizeName()} ({i}")'
    return f'UNKNOWN ({i}")'


in2cup = inchToCupSize


def centimeterToLength(i: int, base_unit: Optional[QuantityOrUnitLike] = None) -> str:
    pu = getPresentingUnitSystem()
    base_unit = base_unit or UNITS.centimeter
    return q2str(pu.asLength(i, base_unit))


cm2len = centimeterToLength


def register_unit_args(argp: argparse.ArgumentParser) -> None:
    g = argp.add_argument_group(
        "Unit Conversion Options", "Options for changing the output unit system"
    )
    g.add_argument(
        "--unit-system",
        "--sys",
        choices=["auto", "us", "si", "uk"],
        default="auto",
        help="What unit system to display with? (us=US imperial, si=Système International)",
    )


def handle_unit_args(args: argparse.Namespace) -> None:
    if args.unit_system != "auto":
        setPresentingUnitSystem(args.unit_system)
    else:
        getPresentingUnitSystem()


def register_length(
    g: argparse._ArgumentGroup, argprefix: str, subject: str, internal_units: Unit
) -> None:
    suffix = f"converted to {internal_units:~P} and rounded"
    g.add_argument(
        f"--{argprefix}-cm",
        type=float,
        default=0.0,
        help=f"Set {subject} in centimeters ({suffix})",
    )
    g.add_argument(
        f"--{argprefix}-ft",
        type=float,
        default=0.0,
        help=f"Set {subject} in inches (Combine with --{argprefix}-in; {suffix})",
    )
    g.add_argument(
        f"--{argprefix}-in",
        type=float,
        default=0.0,
        help=f"Set {subject} in inches (Combine with --{argprefix}-ft; {suffix})",
    )
    g.add_argument(
        f"--{argprefix}-m",
        type=float,
        default=0.0,
        help=f"Set {subject} in meters ({suffix})",
    )
    g.add_argument(
        f"--{argprefix}",
        type=str,
        default=None,
        help=f"Set {subject} in arbitrary pint-parsable units (e.g. 256cm, 5in; {suffix})",
    )
    g.add_argument(
        f"--{argprefix}-from-char",
        action="store_true",
        default=False,
        help=f"Copy {subject} from char",
    )


def handle_length(
    args: argparse.Namespace,
    argprefix: str,
    internal_units: Unit,
) -> Optional[int]:
    m: Optional[float] = arg_to_optional_float(args, argprefix + "_m")
    cm: Optional[float] = arg_to_optional_float(args, argprefix + "_cm")
    inches: Optional[float] = arg_to_optional_float(args, argprefix + "_in")
    ft: Optional[float] = arg_to_optional_float(args, argprefix + "_ft")
    parsable: Optional[str] = getattr(args, argprefix, None)
    if parsable is not None:
        return int(round(UNITS(parsable).to(internal_units).magnitude))
    if m or cm or inches or ft:
        o = Quantity(0, internal_units)
        if m > 0:
            o += Quantity(m, UNITS.meter)
        if cm > 0:
            o += Quantity(cm, UNITS.centimeter)
        if inches > 0:
            o += Quantity(inches, UNITS.inch)
        if ft > 0:
            o += Quantity(ft, UNITS.inch)
        return int(round(o.magnitude))


def handle_length_as_cm(args: argparse.Namespace, argprefix: str) -> Optional[int]:
    return handle_length(args, argprefix, internal_units=UNITS.centimeter)

import argparse
import ipaddress
import socket
from argparse import ArgumentError
from contextlib import closing
from enum import Enum
from pathlib import Path
from typing import Callable, Iterable, Optional, Set, Type, Union
from lilitools.consts import VERSION
from lilitools.liliths_throne_consts import (
    LILITHS_THRONE_COMMIT,
    LILITHS_THRONE_VERSION,
)

from lilitools.logging import ClickWrap

click = ClickWrap()


def existingFilePath(input: str) -> Path:
    p = Path(input)
    if not p.is_file():
        click.critical(f"{p} does not exist as a file")
    return p


def existingDirPath(input: str) -> Path:
    p = Path(input)
    if not p.is_dir():
        click.critical(f"{p} does not exist as a directory")
    return p


def fuzzybool(input: str) -> bool:
    input = input.lower()
    if input in ("y", "yes", "t", "true"):
        return True
    if input in ("n", "no", "f", "false"):
        return False
    raise ArgumentError(None, "Invalid boolean.")


def validPort(strin: str) -> int:
    o = int(strin)
    if o > 65535 or o <= 80:
        click.critical(f"invalid port number, range=(80, 65535]")
    return o


def find_free_port() -> int:
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as s:
        s.bind(("", 0))
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        return s.getsockname()[1]


IP4Or6Address = Union[ipaddress.IPv4Address, ipaddress.IPv6Address]


def validIP(
    invalid_format_error: str = "Invalid format: {ADDRESS}",
) -> Callable[[str], Optional[str]]:
    def _wrapped(strin: str) -> Optional[str]:
        try:
            addr: IP4Or6Address = ipaddress.ip_address(strin)
        except:
            raise Exception(invalid_format_error.format(ADDRESS=repr(strin)))
        o = str(addr.compressed)
        if o != strin:
            click.warn(f"WARNING: parsed {strin!r} as {o!r}")
        return o

    return _wrapped


def arg2enum(T: Type[Enum], skip_prefix: bool = False) -> Callable[[str], Enum]:
    def _inner(input: str) -> Enum:
        if input.isnumeric():
            return T(int(input))
        uinput = input.upper()
        if skip_prefix:
            for e in T:
                ename = "_".join(e.name.split("_")[1:])
                if ename == uinput or e.name == uinput:
                    return e
        return T[input.upper()]

    return _inner


def register_logging(argp: argparse.ArgumentParser) -> None:
    argp.add_argument(
        "--debug",
        action="store_true",
        default=False,
        help="Enable debug logging in console.",
    )
    argp.add_argument(
        "--log-level",
        choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"],
        default=None,
        help="Explicitly set logging level.",
    )


def handle_logging(args: argparse.Namespace) -> None:
    import logging as builtin_logging
    import lilitools.logging

    if args.debug:
        lilitools.logging.DEBUG = True
        if args.log_level is not None:
            click.warn(
                f"--log-level and --debug are both set. --log-level will be overridden with DEBUG."
            )
        args.log_level = "DEBUG"
        click.debug("--debug set: Debugging enabled")

    if args.log_level is not None:
        level = builtin_logging._nameToLevel[args.log_level]
        builtin_logging.root.setLevel(level)
        click.debug(f"--log-level set: Logging level set to {level}")


def _b2tf_c(
    value: bool,
    truestr: str = "True",
    falsestr: str = "False",
    reverse_colours: bool = False,
) -> str:
    return (
        click.style(truestr, fg="green")
        if reverse_colours ^ value
        else click.style(falsestr, fg="red")
    )


def bool2tf_colored(value: bool, reverse_colours: bool = False) -> str:
    return _b2tf_c(value, "True", "False", reverse_colours)


def bool2yn_colored(value: bool, reverse_colours: bool = False) -> str:
    return _b2tf_c(value, "Yes", "No", reverse_colours)


def checkedInt(
    argname: str,
    minval: Optional[int] = None,
    maxval: Optional[int] = None,
    badvals: Optional[Iterable[int]] = None,
) -> Callable[[str], int]:
    def _wrapped_check_int(inp: str) -> int:
        o: int = int(inp)
        if minval is not None and o < minval:
            raise ValueError(
                f"{argname}: Value {o} is less than minimum allowed value {minval}!"
            )
        if maxval is not None and o > maxval:
            raise ValueError(
                f"{argname}: Value {o} is greater than maximum allowed value {maxval}!"
            )

        if badvals is not None and o in badvals:
            raise ValueError(f"{argname}: Value {o} is not permitted!")
        return o

    return _wrapped_check_int


def get_header_lines(app_name: str) -> Iterable[str]:
    yield f"LiliTools v{VERSION} - {app_name}"
    yield "Copyright (c)2021-2024 LiliTools Contributors."
    yield "Available under the MIT Open Source License."
    yield f"Compiled against Lilith's Throne {LILITHS_THRONE_VERSION} (commit {LILITHS_THRONE_COMMIT.hex()})"


def print_header(app_name: str) -> None:
    for ln in get_header_lines(app_name=app_name):
        click.echo(ln)


def arg_to_optional_float(args: argparse.Namespace, attr: str) -> Optional[float]:
    v = getattr(args, attr, None)
    if v is None:
        return None
    return float(v)

#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#           BY devtools/buildConsts.py          #
#            Constants for lilitools            #
#################################################
## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public static final int DEFAULT_COMBAT_AP = 3; @ eewx+fuZg8Z6jWs8kZGPwF1FJCGJBIkz7Xm1qjgxzLSw9o7NL48dIvTu/zDV5AMo++7AaJa/Jk+SAzT1R4pASw==
## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public static final int LEVEL_CAP = 50; @ exoR4LqJHkf9i9QNCNKJHPrOyUr88hJDeYZImHODiq6Al2Op/ptzdx6ZHwu0JdNQ8czmfrDXiWV/fpIK+syGsQ==
## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public static final int MAX_COMBAT_MOVES = 8; @ czzJ1RplrsJfbBN140jiOXinIgchOfbWIzprp4nM8CbEDs6aupa3HYkePCRk0m7gV9UE+7b6tP/fcBSzdqP/jw==
## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public static final int MAX_TRAITS = 6; @ 3z+lcqzu4d/cx4ALRJOY9xRuqkqK9AqI+NPMbdYAiksdUjnNLSAG0DGmF/p/6imUX2tYDifU4w+veP43j6n+Bw==
## from [LT]/src/com/lilithsthrone/game/dialogue/utils/CharacterModificationUtils.java: public static final int FLUID_INCREMENT_AVERAGE = 50; @ VFMHMwiYDDlwnHe8IV8RvsOlQoPTF7fPaIw56LCBNS1IUjQ6yMSTAgNu7Rhn/m9+DXk+nOLbj+dH1HtvTdjhHw==
## from [LT]/src/com/lilithsthrone/game/dialogue/utils/CharacterModificationUtils.java: public static final int FLUID_INCREMENT_HUGE = 5000; @ pz0QiNUi6rRtzgN1twqMMT+yP3Ut7FZ0nv2zL7cinSlHWvBMSmb628eHkyzs4E/e2PT8fuWuy4AOvovbiQ9w4w==
## from [LT]/src/com/lilithsthrone/game/dialogue/utils/CharacterModificationUtils.java: public static final int FLUID_INCREMENT_LARGE = 500; @ cou5sbHoDFcfKzRCuQeoinNub5g91vT2ikjSfsHX41bfBTXjXY8bjjQPlYy6mdel3SVqRKBggkfw43ZdAmy5Ew==
## from [LT]/src/com/lilithsthrone/game/dialogue/utils/CharacterModificationUtils.java: public static final int FLUID_INCREMENT_SMALL = 5; @ JO3Uh3p3DgA6GY+F7AL+uQzNGBPRxFFYGnjfHvctEhLvyJHo88WgsgArKkYK7jEspTECec5QBNxLaFFASlzyhg==
## from [LT]/src/com/lilithsthrone/game/dialogue/utils/CharacterModificationUtils.java: public static final int FLUID_REGEN_INCREMENT_AVERAGE = 1000; @ ZWqnqbD8lZe182kSkvajoaEwxDBuxl1o47T8IIJ3BKQGO5fT1QxlEwx6PF57TUP9Rs7/zgv8CUZEzKx698uHhA==
## from [LT]/src/com/lilithsthrone/game/dialogue/utils/CharacterModificationUtils.java: public static final int FLUID_REGEN_INCREMENT_HUGE = 100000; @ xoETlR7tJalkbiirTyAE4JdNT5qonwdrLQPBMKTL3wKQwz0FYTctYJzBQFYKsakJZikN4af7YDsOMjNJ5NALPA==
## from [LT]/src/com/lilithsthrone/game/dialogue/utils/CharacterModificationUtils.java: public static final int FLUID_REGEN_INCREMENT_LARGE = 10000; @ RTRvFzHt/R6r7Kpl+cfKRDgukOBrwSaGi4vRRUhTENeTBLGmH/2TKGKABj9nuFzuhB59QMZZ8FtP/CSNdUv6Eg==
## from [LT]/src/com/lilithsthrone/game/dialogue/utils/CharacterModificationUtils.java: public static final int FLUID_REGEN_INCREMENT_SMALL = 100; @ izS/HEo5c1CDKORsHrlVMe2DHhP8povLeLqwM9hNP6cGfouGKejwVQ41sWm2ekO8dQ/7cw4ZP+sDpAH6aV0B0A==
## from [LT]/src/com/lilithsthrone/rendering/RenderingEngine.java: public static final int INVENTORY_PAGES = 5; @ Uw7L3VQ90Ivl38X33sdK/edYDfaMXV6HZpk0naw8dPc57rjXp9dwVad3lHhMSTsNSekK18cMZNQ3whpX921B5g==
## from [LT]/src/com/lilithsthrone/rendering/RenderingEngine.java: public static final int ITEMS_PER_PAGE = 6 * 5; // 6 items per row @ 8eU9oP5ocHyeGqFB7dZE3t/TjmcvimmAUPV5vnxJaUuOaMSEVbJJQ24qmQwaz7xtuogP3DlZx1xBamckDwvjng==
from typing import Final
__all__ = [
    "APP_NAME",
    "DEFAULT_COMBAT_AP",
    "FLUID_INCREMENT_AVERAGE",
    "FLUID_INCREMENT_HUGE",
    "FLUID_INCREMENT_LARGE",
    "FLUID_INCREMENT_SMALL",
    "FLUID_REGEN_INCREMENT_AVERAGE",
    "FLUID_REGEN_INCREMENT_HUGE",
    "FLUID_REGEN_INCREMENT_LARGE",
    "FLUID_REGEN_INCREMENT_SMALL",
    "HOURS_PER_DAY",
    "LEVEL_CAP",
    "MAX_CLOTHING_COLOUR_LEN",
    "MAX_COMBAT_MOVES",
    "MAX_ITEMS_PER_STACK",
    "MAX_PAGES_OF_INVENTORY",
    "MAX_STACKS_PER_SET",
    "MAX_TRAITS",
    "MINUTES_PER_HOUR",
    "PRESERVE_SAVE_ORDERING",
    "SECONDS_PER_DAY",
    "SECONDS_PER_HOUR",
    "SECONDS_PER_MINUTE",
    "SID_SYMBOL",
    "TILES_PER_INVENTORY_PAGE",
    "VERSION",
]
APP_NAME: Final[str] = "lilitools"
DEFAULT_COMBAT_AP: Final[int] = 3
FLUID_INCREMENT_AVERAGE: Final[int] = 50
FLUID_INCREMENT_HUGE: Final[int] = 5000
FLUID_INCREMENT_LARGE: Final[int] = 500
FLUID_INCREMENT_SMALL: Final[int] = 5
FLUID_REGEN_INCREMENT_AVERAGE: Final[int] = 1000
FLUID_REGEN_INCREMENT_HUGE: Final[int] = 100000
FLUID_REGEN_INCREMENT_LARGE: Final[int] = 10000
FLUID_REGEN_INCREMENT_SMALL: Final[int] = 100
HOURS_PER_DAY: Final[int] = 24
LEVEL_CAP: Final[int] = 50
MAX_CLOTHING_COLOUR_LEN: Final[int] = 3
MAX_COMBAT_MOVES: Final[int] = 8
MAX_ITEMS_PER_STACK: Final[int] = 10
MAX_PAGES_OF_INVENTORY: Final[int] = 5
MAX_STACKS_PER_SET: Final[int] = 150
MAX_TRAITS: Final[int] = 6
MINUTES_PER_HOUR: Final[int] = 60
PRESERVE_SAVE_ORDERING: Final[int] = False
SECONDS_PER_DAY: Final[int] = 86400
SECONDS_PER_HOUR: Final[int] = 3600
SECONDS_PER_MINUTE: Final[int] = 60
SID_SYMBOL: Final[str] = "⛧"
TILES_PER_INVENTORY_PAGE: Final[int] = 30
VERSION: Final[str] = "1.0.0"

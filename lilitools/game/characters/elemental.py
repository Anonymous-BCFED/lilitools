from typing import Any, Dict
from lxml import etree

from lilitools.saves.character.elemental_specific import ElementalSpecific
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.character.npc import NPC
from lilitools.saves.exceptions import ElementRequiredException


class Elemental(NPC):
    CLASS_PATH = 'com.lilithsthrone.game.character.npc.misc.Elemental'

    def __init__(self) -> None:
        super().__init__()
        self.elemental: ElementalSpecific = ElementalSpecific()

    def isElemental(self) -> bool:
        return True

    def on_fromXML_character(self, e: etree._Element) -> None:
        if (ee := e.find('elementalSpecial')) is not None:
            self.elemental = ElementalSpecific()
            self.elemental.fromXML(ee)
        else:
            raise ElementRequiredException(self, e, 'elemental', 'elementalSpecial')

    def on_toXML_character(self, e: etree._Element) -> None:
        e.append(self.elemental.toXML())

    def on_fromDict_character(self, char: Dict[str, Any]) -> None:
        self.elemental = ElementalSpecific()
        self.elemental.fromDict(char['elemental'])
    def on_toDict_character(self, char: Dict[str, Any]) -> None:
        char['elemental']=self.elemental.toDict()

    def getSummoner(self) -> GameCharacter:
        from lilitools.saves.game import Game
        return Game.getAnInstance().getNPCById(self.elemental.summoner)

    def getCharacterForExport(self) -> GameCharacter:
        return super().getCharacterForExport()

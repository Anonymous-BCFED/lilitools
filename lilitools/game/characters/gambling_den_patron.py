from typing import Any, Dict
from lxml import etree

from lilitools.saves.character.npc import NPC
from lilitools.saves.exceptions import (
    AttributeRequiredException,
    ElementRequiredException,
)


class GamblingDenPatron(NPC):
    CLASS_PATH = "com.lilithsthrone.game.character.npc.submission.GamblingDenPatron"

    def __init__(self) -> None:
        super().__init__()
        self.table: str = ""

    def on_toXML_character(self, e: etree._Element) -> None:
        etree.SubElement(e, "table", {"value": self.table})

    def on_fromXML_character(self, e: etree._Element) -> None:
        if (te := e.find("table")) is not None:
            if "value" in te.attrib:
                self.table = te.attrib["value"]
            else:
                raise AttributeRequiredException(self, te, "value", "table")
        else:
            raise ElementRequiredException(self, e, "table", "table")

    def on_toDict_character(self, char: Dict[str, Any]) -> None:
        char["table"] = self.table

    def on_fromDict_character(self, char: Dict[str, Any]) -> None:
        self.table = char["table"]

from typing import Any, Dict
from lxml import etree
from lilitools.game.characters.mixins.vendor_with_multiple_vending_machines import (
    VendorWithMultipleVendingMachinesMixin,
)
from lilitools.saves.character.npc import NPC


class HazmatRat(VendorWithMultipleVendingMachinesMixin, NPC):
    NPCID = "-1,HazmatRat"
    CLASS_PATH = "com.lilithsthrone.game.character.npc.submission.HazmatRat"

    def __init__(self) -> None:
        super().__init__()
        self.itemsAtVendingMachine = self.init_itemsAtVendingMachine()

    def on_toXML_character(self, e: etree._Element) -> None:
        self.toXML_itemsAtVendingMachine(
            e, "itemsAtVendingMachine", self.itemsAtVendingMachine
        )

    def on_fromXML_character(self, e: etree._Element) -> None:
        self.itemsAtVendingMachine = self.fromXML_itemsAtVendingMachine(
            e.find("itemsAtVendingMachine")
        )

    def on_toDict_character(self, data: Dict[str, Any]) -> None:
        data["itemsAtVendingMachine"] = self.toDict_itemsAtVendingMachine(
            self.itemsAtVendingMachine
        )

    def on_fromDict_character(self, data: Dict[str, Any]) -> None:
        self.itemsAtVendingMachine = self.fromDict_itemsAtVendingMachine(
            data.get("itemsAtVendingMachine", [])
        )

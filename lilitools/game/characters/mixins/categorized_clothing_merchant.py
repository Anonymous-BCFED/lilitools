from collections import OrderedDict
from typing import Any, Dict, Final, FrozenSet, List, Optional

from lxml import etree

from lilitools.consts import MAX_ITEMS_PER_STACK, MAX_STACKS_PER_SET, PRESERVE_SAVE_ORDERING
from lilitools.logging import ClickWrap
from lilitools.saves.character.npc import NPC
from lilitools.saves.items.abstract_clothing import AbstractClothing

log = ClickWrap()


class CategorizedClothingMerchantMixin(NPC):
    CLOTHING_SETS: FrozenSet[str]

    def __init__(self) -> None:
        super().__init__()
        self.clothingMap: OrderedDict[str, List[AbstractClothing]] = OrderedDict()

    # -/game/NPC[35]/character/commonAndrogynousAccessories/clothing[1]
    def fromXML_catClothingMerchant(self, e: etree._Element) -> None:
        stacksInSet: List[AbstractClothing] = []
        c: etree._Element
        cset: etree._Element
        stackTracking: Dict[str, int] = {}
        if PRESERVE_SAVE_ORDERING:
            for cset in e:
                if cset.tag not in self.CLOTHING_SETS:
                    continue
                stacksInSet = []
                stackTracking = {}
                for c in cset.iterfind('clothing'):
                    k = c.attrib['id']
                    if k not in stackTracking:
                        stackTracking[k] = 1
                    else:
                        stackTracking[k] += 1
                    if len(stackTracking) > MAX_STACKS_PER_SET:
                        log.warn(f'[{self.NPCID}] Clothing set {clothing_set!r} too long at {len(cset)} items; truncated to {len(stackTracking)}.')
                        break
                    if stackTracking[k] > MAX_ITEMS_PER_STACK:
                        continue
                    ac = AbstractClothing()
                    ac.fromXML(c)
                    stacksInSet.append(ac)
                self.clothingMap[str(cset.tag)] = stacksInSet
        else:
            for clothing_set in self.CLOTHING_SETS:
                if (cset := e.find(clothing_set)) is not None:
                    stacksInSet = []
                    stackTracking = {}
                    for c in cset.iterfind('clothing'):
                        k = c.attrib['id']
                        if k not in stackTracking:
                            stackTracking[k] = 1
                        else:
                            stackTracking[k] += 1
                        if len(stackTracking) > MAX_STACKS_PER_SET:
                            log.warn(f'[{self.NPCID}] Clothing set {clothing_set!r} too long at {len(cset)} items; truncated to {len(stackTracking)}.')
                            break
                        if stackTracking[k] > MAX_ITEMS_PER_STACK:
                            continue
                        ac = AbstractClothing()
                        ac.fromXML(c)
                        stacksInSet.append(ac)
                    self.clothingMap[clothing_set] = stacksInSet

    def toXML_catClothingMerchant(self, e: etree._Element) -> None:
        if PRESERVE_SAVE_ORDERING:
            for clothing_set in self.clothingMap.keys():
                cset = etree.SubElement(e, clothing_set)
                for clothing in self.clothingMap[clothing_set]:
                    c = clothing.toXML()
                    cset.append(c)
        else:
            for clothing_set in sorted(self.CLOTHING_SETS):
                cset = etree.SubElement(e, clothing_set)
                for clothing in sorted(self.clothingMap[clothing_set], key=lambda x: (x.id, x.name, x.colours)):
                    c = clothing.toXML()
                    cset.append(c)

    # -/game/NPC[35]/character/commonAndrogynousAccessories/clothing[1]
    def fromDict_catClothingMerchant(self, data: Dict[str, Any]) -> None:
        sets:Dict[str, Any] = data['clothingSets']
        stacksInSet: List[AbstractClothing] = []
        c: etree._Element
        cset: etree._Element
        stackTracking: Dict[str, int] = {}
        for tag, cset in sets.items():
            if tag not in self.CLOTHING_SETS:
                continue
            stacksInSet = []
            stackTracking = {}
            for c in cset:
                k = c['id']
                if k not in stackTracking:
                    stackTracking[k] = 1
                else:
                    stackTracking[k] += 1
                if len(stackTracking) > MAX_STACKS_PER_SET:
                    log.warn(f'[{self.NPCID}] Clothing set {tag!r} too long at {len(cset)} items; truncated to {len(stackTracking)}.')
                    break
                if stackTracking[k] > MAX_ITEMS_PER_STACK:
                    continue
                ac = AbstractClothing()
                ac.fromXML(c)
                stacksInSet.append(ac)
            self.clothingMap[str(tag)] = stacksInSet
        

    def toDict_catClothingMerchant(self, data:Dict[str,Any]) -> None:
        sets:Dict[str,Any]={}
        for clothing_set in self.clothingMap.keys():
            cset = []
            for clothing in self.clothingMap[clothing_set]:
                c = clothing.toDict()
                cset.append(c)
            sets[clothing_set]=cset
        data['clothingSets']=sets

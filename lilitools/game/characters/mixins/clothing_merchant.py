from typing import Any, Dict, List, Tuple

from lxml import etree

from lilitools.saves.character.npc import NPC
from lilitools.saves.items.abstract_clothing import AbstractClothing


# Mixin
class ClothingMerchantMixin(NPC):
    # -/game/NPC[35]/character/commonAndrogynousAccessories/clothing[1]
    def fromXML_clothingMerchant(
        self, e: etree._Element
    ) -> List[Tuple[AbstractClothing, int]]:
        o: List[Tuple[AbstractClothing, int]] = []
        ce: etree._Element
        for ce in e.getchildren():
            count = int(ce.attrib["count"])
            c = AbstractClothing()
            c.fromXML(ce)
            o.append((c, count))
        return o

    def toXML_clothingMerchant(
        self, e: etree._Element, tag: str, data: List[Tuple[AbstractClothing, int]]
    ):
        pe = etree.SubElement(e, tag)
        for ac, count in data:
            ce = ac.toXML()
            ce.attrib["count"] = str(count)
            pe.append(ce)

    def fromDict_clothingMerchant(
        self, data: List[Dict[str, Any]]
    ) -> List[Tuple[AbstractClothing, int]]:
        o: List[Tuple[AbstractClothing, int]] = []
        for cdata in data:
            count = int(cdata["count"])
            c = AbstractClothing()
            c.fromDict(cdata)
            o.append((c, count))
        return o

    def toDict_clothingMerchant(
        self, clothing: List[Tuple[AbstractClothing, int]]
    ) -> List[Dict[str, Any]]:
        o: List[Dict[str, Any]] = []
        for ac, count in clothing:
            ce = ac.toDict()
            ce["count"] = str(count)
            o.append(ce)
        return o

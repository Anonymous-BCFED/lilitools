from typing import Any, Dict, List, Tuple

from lxml import etree

from lilitools.saves.character.npc import NPC
from lilitools.saves.items.abstract_item import AbstractItem


# Mixin
class ItemMerchantMixin(NPC):

    def fromXML_itemMerchant(self, e: etree._Element) -> List[Tuple[AbstractItem, int]]:
        o: List[Tuple[AbstractItem, int]] = []
        ie: etree._Element
        for ie in e.getchildren():
            count = int(ie.attrib["count"])
            i = AbstractItem()
            i.fromXML(ie)
            o.append((i, count))
        return o

    def toXML_itemMerchant(
        self, e: etree._Element, tag: str, data: List[Tuple[AbstractItem, int]]
    ):
        pe = etree.SubElement(e, tag)
        for i, count in data:
            ie = i.toXML()
            ie.attrib["count"] = str(count)
            pe.append(ie)

    def fromDict_itemMerchant(
        self, data: List[Dict[str, Any]]
    ) -> List[Tuple[AbstractItem, int]]:
        o: List[Tuple[AbstractItem, int]] = []
        ie: Dict[str, Any]
        for ie in data:
            count = int(ie["count"])
            i = AbstractItem()
            i.fromDict(ie)
            o.append((i, count))
        return o

    def toDict_itemMerchant(
        self, items: List[Tuple[AbstractItem, int]]
    ) -> List[Dict[str, Any]]:
        o: List[Dict[str, Any]] = []
        for i, count in items:
            ie = i.toDict()
            ie["count"] = str(count)
            o.append(ie)
        return o

from collections import OrderedDict
from typing import Any, Dict, List, Tuple
from lxml import etree

from lilitools.saves.items.abstract_item import AbstractItem
from lilitools.saves.math.point2i import Point2I

# -/game/NPC[18]/character/itemsAtVendingMachine
# -/game/NPC[18]/character/itemsAtVendingMachine/location[1]
# -/game/NPC[18]/character/itemsAtVendingMachine/location[1]/@x='4'
# -/game/NPC[18]/character/itemsAtVendingMachine/location[1]/@y='5'
# -/game/NPC[18]/character/itemsAtVendingMachine/location[1]/item[1]
# -/game/NPC[18]/character/itemsAtVendingMachine/location[1]/item[1]/@colour='ATTRIBUTE_CORRUPTION'
# -/game/NPC[18]/character/itemsAtVendingMachine/location[1]/item[1]/@count='10'
# -/game/NPC[18]/character/itemsAtVendingMachine/location[1]/item[1]/@id='innoxia_race_imp_impish_brew'
# -/game/NPC[18]/character/itemsAtVendingMachine/location[1]/item[1]/@name='Impish Brew'
# private static Map<Vector2i, Map<AbstractItem, Integer>> itemsAtVendingMachine = new HashMap<>();

__all__ = ["VendorWithMultipleVendingMachinesMixin"]


class VendorWithMultipleVendingMachinesMixin:
    def init_itemsAtVendingMachine(
        self,
    ) -> OrderedDict[Point2I, OrderedDict[AbstractItem, int]]:
        return OrderedDict()

    def fromXML_itemsAtVendingMachine(
        self, e: etree._Element
    ) -> OrderedDict[Point2I, OrderedDict[AbstractItem, int]]:
        o: OrderedDict[Point2I, OrderedDict[AbstractItem, int]] = OrderedDict()
        for location in e.findall("location"):
            pos = Point2I(
                x=int(location.attrib["x"]),
                y=int(location.attrib["y"]),
            )
            things: OrderedDict[AbstractItem, int] = OrderedDict()
            for thing in location:
                if isinstance(thing, etree._Element):
                    i = AbstractItem()
                    i.fromXML(thing)
                    things[i] = int(location.attrib.get("count", "1"))

            o[pos] = things
        return o

    def toXML_itemsAtVendingMachine(
        self,
        e: etree._Element,
        tag: str,
        data: OrderedDict[Point2I, OrderedDict[AbstractItem, int]],
    ) -> None:
        itemsAtVendingMachine = etree.SubElement(e, tag)
        for pos, things in data.items():
            location = etree.SubElement(
                itemsAtVendingMachine,
                "location",
                {
                    "x": str(pos.x),
                    "y": str(pos.y),
                },
            )
            for i, count in things.items():
                item = i.toXML()
                item.attrib["count"] = str(count)
                location.append(item)

    def fromDict_itemsAtVendingMachine(
        self, data: list
    ) -> OrderedDict[Point2I, OrderedDict[AbstractItem, int]]:
        o: OrderedDict[Point2I, OrderedDict[AbstractItem, int]] = OrderedDict()
        for location, items in data:
            pos = Point2I()
            pos.fromDict(location)
            things: OrderedDict[AbstractItem, int] = OrderedDict()
            for thing, count in items:
                i = AbstractItem()
                i.fromXML(thing)
                things[i] = int(count)

            o[pos] = things
        return o

    def toDict_itemsAtVendingMachine(
        self, info: OrderedDict[Point2I, OrderedDict[AbstractItem, int]]
    ) -> List[Tuple[Dict[str, Any], List[Tuple[Dict[str, Any], int]]]]:
        o: List[Tuple[Dict[str, Any], List[Tuple[Dict[str, Any], int]]]] = []
        for pos, things in info.items():
            o.append((pos.toDict(), [(i.toDict(), c) for i, c in things.items()]))
        return o

from typing import Any, Dict, List, Tuple

from lxml import etree

from lilitools.saves.character.npc import NPC
from lilitools.saves.items.abstract_weapon import AbstractWeapon


# Mixin
class WeaponsMerchantMixin(NPC):

    def fromXML_weaponsMerchant(
        self, e: etree._Element
    ) -> List[Tuple[AbstractWeapon, int]]:
        o: List[Tuple[AbstractWeapon, int]] = []
        we: etree._Element
        for we in e.getchildren():
            count = int(we.attrib["count"])
            w = AbstractWeapon()
            w.fromXML(we)
            o.append((w, count))
        return o

    def toXML_weaponsMerchant(
        self, e: etree._Element, tag: str, data: List[Tuple[AbstractWeapon, int]]
    ):
        pe = etree.SubElement(e, tag)
        for w, count in data:
            we = w.toXML()
            we.attrib["count"] = str(count)
            pe.append(we)

    def fromDict_weaponsMerchant(
        self, data: List[Dict[str, Any]]
    ) -> List[Tuple[AbstractWeapon, int]]:
        o: List[Tuple[AbstractWeapon, int]] = []
        we: Dict[str, Any]
        for we in data:
            count = int(we["count"])
            w = AbstractWeapon()
            w.fromDict(we)
            o.append((w, count))
        return o

    def toDict_weaponsMerchant(
        self, weapons: List[Tuple[AbstractWeapon, int]]
    ) -> List[Dict[str, Any]]:
        o: List[Dict[str, Any]] = []
        for w, count in weapons:
            we = w.toDict()
            we["count"] = str(count)
            o.append(we)
        return o

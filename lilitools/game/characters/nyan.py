from typing import Any, Dict, FrozenSet

from lxml import etree

from lilitools.game.characters.mixins.categorized_clothing_merchant import CategorizedClothingMerchantMixin
from lilitools.saves.character.npc import NPC


class Nyan(CategorizedClothingMerchantMixin, NPC):
    NPCID = '-1,Nyan'
    CLASS_PATH = 'com.lilithsthrone.game.character.npc.dominion.Nyan'
    CLOTHING_SETS: FrozenSet[str] = frozenset(
        {
            "commonFemaleClothing",
            "commonFemaleUnderwear",
            "commonFemaleAccessories",
            "commonMaleClothing",
            "commonMaleLingerie",
            "commonMaleAccessories",
            "commonAndrogynousClothing",
            "commonAndrogynousLingerie",
            "commonAndrogynousAccessories",
            "specials",
        }
    )

    def on_toXML_character(self, e: etree._Element) -> None:
        self.toXML_catClothingMerchant(e)

    def on_fromXML_character(self, e: etree._Element) -> None:
        self.fromXML_catClothingMerchant(e)

    def on_toDict_character(self, data: Dict[str, Any]) -> None:
        self.toDict_catClothingMerchant(data)

    def on_fromDict_character(self, data: Dict[str, Any]) -> None:
        self.fromDict_catClothingMerchant(data)

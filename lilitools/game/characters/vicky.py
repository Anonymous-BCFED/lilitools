from typing import Any, Dict, List, Tuple

from lxml import etree

from lilitools.game.characters.mixins.clothing_merchant import ClothingMerchantMixin
from lilitools.game.characters.mixins.item_merchant import ItemMerchantMixin
from lilitools.game.characters.mixins.weapons_merchant import WeaponsMerchantMixin
from lilitools.saves.character.npc import NPC
from lilitools.saves.items.abstract_clothing import AbstractClothing
from lilitools.saves.items.abstract_item import AbstractItem
from lilitools.saves.items.abstract_weapon import AbstractWeapon


class Vicky(ClothingMerchantMixin, ItemMerchantMixin, WeaponsMerchantMixin, NPC):
    NPCID = '-1,Vicky'
    CLASS_PATH = 'com.lilithsthrone.game.character.npc.dominion.Vicky'

    def __init__(self) -> None:
        super().__init__()
        self.weaponsForSale: List[Tuple[AbstractWeapon, int]] = []
        self.itemsForSale: List[Tuple[AbstractItem, int]] = []
        self.clothingForSale: List[Tuple[AbstractClothing, int]] = []

    def on_fromXML_character(self, char: etree._Element) -> None:
        self.clothingForSale = self.fromXML_clothingMerchant(char.find('clothingForSale'))
        self.itemsForSale = self.fromXML_itemMerchant(char.find('itemsForSale'))
        self.weaponsForSale = self.fromXML_weaponsMerchant(char.find('weaponsForSale'))

    def on_toXML_character(self, char: etree._Element) -> None:
        self.toXML_clothingMerchant(char, 'clothingForSale', self.clothingForSale)
        self.toXML_itemMerchant(char, 'itemsForSale', self.itemsForSale)
        self.toXML_weaponsMerchant(char, 'weaponsForSale', self.weaponsForSale)

    def on_fromDict_character(self, data:Dict[str,Any]) -> None:
        self.clothingForSale = self.fromDict_clothingMerchant(data.get('clothingForSale'))
        self.itemsForSale = self.fromDict_itemMerchant(data.get('itemsForSale'))
        self.weaponsForSale = self.fromDict_weaponsMerchant(data.get('weaponsForSale'))

    def on_toDict_character(self, data:Dict[str,Any]) -> None:
        data['clothingForSale']=self.toDict_clothingMerchant(self.clothingForSale)
        data['itemsForSale']=self.toDict_itemMerchant(self.itemsForSale)
        data['weaponsForSale']=self.toDict_weaponsMerchant(self.weaponsForSale)

from __future__ import annotations

from typing import List, Optional

from lilitools.game.effects.alchemist.antennae import AntennaePotionFactory
from lilitools.game.effects.alchemist.arms import ArmsPotionFactory
from lilitools.game.effects.alchemist.ass import AssPotionFactory
from lilitools.game.effects.alchemist.penis import PenisPotionFactory
from lilitools.game.effects.alchemist.vagina import VaginaPotionFactory
from lilitools.saves.character.body.penis import Penis
from lilitools.saves.character.body.vagina import Vagina
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.enums.tf_modifier import ETFModifier
from lilitools.saves.enums.tf_potency import ETFPotency
from lilitools.saves.items.item_effect import ItemEffect


class Alchemist:
    def __init__(self, baseid: str, existingeffects: Optional[List[ItemEffect]]) -> None:
        self.effects: List[ItemEffect] = []
        if existingeffects is not None:
            self.effects += existingeffects
        self.base: str = baseid

        self._antennae: AntennaePotionFactory = AntennaePotionFactory(self)
        self._arms: ArmsPotionFactory = ArmsPotionFactory(self)
        self._ass: AssPotionFactory = AssPotionFactory(self)
        self._breasts: BreastsPotionFactory = BreastsPotionFactory(self)
        self._penis: PenisPotionFactory = PenisPotionFactory(self)
        self._vagina: VaginaPotionFactory = VaginaPotionFactory(self)

    def sort(self, reverse: bool = False) -> None:
        self.effects.sort(key=lambda e: (e.mod1.name, e.mod2.name, e.potency.name), reverse=reverse)

    def addEffect(self, type: str, mod1: Optional[ETFModifier], mod2: Optional[ETFModifier], potency: Optional[ETFPotency], limit: Optional[int], timer: Optional[int] = None, repeatable: bool = True) -> Alchemist:
        assert mod1 is None or isinstance(mod1, ETFModifier)
        assert mod2 is None or isinstance(mod2, ETFModifier)
        if timer is None:
            timer = 0  # TODO: Game.time.secondsElapsed + (60*60)
        self.effects.append(ItemEffect(type, mod1, mod2, potency, limit, timer, repeatable))
        return self

    def addAtomic(self, mod1: Optional[ETFModifier], mod2: Optional[ETFModifier], type: Optional[str] = None, limit: Optional[int] = None, timer: Optional[int] = None) -> Alchemist:
        self.addEffect(type or self.base, mod1, mod2, ETFPotency.BOOST, limit or 1, timer, repeatable=False)
        return self

    def addMajorDrain(self, mod1: Optional[ETFModifier], mod2: Optional[ETFModifier], type: Optional[str] = None, limit: Optional[int] = None, timer: Optional[int] = None) -> Alchemist:
        self.addEffect(type or self.base, mod1, mod2, ETFPotency.MAJOR_DRAIN, limit or 1, timer, repeatable=True)
        return self

    def addMinorDrain(self, mod1: Optional[ETFModifier], mod2: Optional[ETFModifier], type: Optional[str] = None, limit: Optional[int] = None, timer: Optional[int] = None) -> Alchemist:
        self.addEffect(type or self.base, mod1, mod2, ETFPotency.MINOR_DRAIN, limit or 1, timer, repeatable=True)
        return self

    def addDrain(self, mod1: Optional[ETFModifier], mod2: Optional[ETFModifier], type: Optional[str] = None, limit: Optional[int] = None, timer: Optional[int] = None) -> Alchemist:
        self.addEffect(type or self.base, mod1, mod2, ETFPotency.DRAIN, limit or 1, timer, repeatable=True)
        return self

    def addBoost(self, mod1: Optional[ETFModifier], mod2: Optional[ETFModifier], type: Optional[str] = None, limit: Optional[int] = None, timer: Optional[int] = None) -> Alchemist:
        self.addEffect(type or self.base, mod1, mod2, ETFPotency.BOOST, limit or 1, timer, repeatable=True)
        return self

    def addMinorBoost(self, mod1: Optional[ETFModifier], mod2: Optional[ETFModifier], type: Optional[str] = None, limit: Optional[int] = None, timer: Optional[int] = None) -> Alchemist:
        self.addEffect(type or self.base, mod1, mod2, ETFPotency.MINOR_BOOST, limit or 1, timer, repeatable=True)
        return self

    def addMajorBoost(self, mod1: Optional[ETFModifier], mod2: Optional[ETFModifier], type: Optional[str] = None, limit: Optional[int] = None, timer: Optional[int] = None) -> Alchemist:
        self.addEffect(type or self.base, mod1, mod2, ETFPotency.MAJOR_BOOST, limit or 1, timer, repeatable=True)
        return self

    def addDelta(self, mod1: Optional[ETFModifier], mod2: Optional[ETFModifier], delta: int, type: Optional[str] = None, limit: Optional[int] = None, timer: Optional[int] = None, delta_major: int = 3, delta_minor: int = 2, delta_normal: int = 1, boost_only: bool = False, drain_only: bool = False) -> Alchemist:
        if delta > 0:
            if not drain_only:
                return self.boostBy(mod1, mod2, delta, type, limit, timer, delta_major, delta_minor, delta_normal)
        elif delta < 0:
            if not boost_only:
                return self.drainBy(mod1, mod2, -delta, type, limit, timer, delta_major, delta_minor, delta_normal)
        return self

    def addDiff(self, mod1: Optional[ETFModifier], mod2: Optional[ETFModifier], mine: int, theirs: int, type: Optional[str] = None, limit: Optional[int] = None, timer: Optional[int] = None, delta_major: int = 3, delta_minor: int = 2, delta_normal: int = 1, boost_only: bool = False, drain_only: bool = False) -> Alchemist:
        self.addDelta(mod1, mod2, theirs - mine, type, limit, timer, delta_major, delta_minor, delta_normal, boost_only, drain_only)
        return self

    def boostBy(self, mod1: Optional[ETFModifier], mod2: Optional[ETFModifier], delta: int, type: Optional[str] = None, limit: Optional[int] = None, timer: Optional[int] = None, delta_major: int = 3, delta_minor: int = 2, delta_normal=1) -> Alchemist:
        dM, delta = divmod(delta, delta_major)
        dm, delta = divmod(delta, delta_minor)
        dn = delta
        if delta_normal > 1:
            dn, delta = divmod(delta, delta_normal)
        for _ in range(dM):
            self.addMajorBoost(mod1, mod2, type, limit, timer)
        for _ in range(dm):
            self.addMinorBoost(mod1, mod2, type, limit, timer)
        for _ in range(dn):
            self.addBoost(mod1, mod2, type, limit, timer)
        return self

    def drainBy(self, mod1: Optional[ETFModifier], mod2: Optional[ETFModifier], delta: int, type: Optional[str] = None, limit: Optional[int] = None, timer: Optional[int] = None, delta_major: int = 3, delta_minor: int = 2, delta_normal=1) -> Alchemist:
        dM, delta = divmod(delta, delta_major)
        dm, delta = divmod(delta, delta_minor)
        dn = delta
        if delta_normal > 1:
            dn, delta = divmod(delta, delta_normal)
        for _ in range(dM):
            self.addMajorDrain(mod1, mod2, type, limit, timer)
        for _ in range(dm):
            self.addMinorDrain(mod1, mod2, type, limit, timer)
        for _ in range(dn):
            self.addDrain(mod1, mod2, type, limit, timer)
        return self

    def generateLinearEffectDiff(self, myval: int, targetval: int, mod1: Optional[ETFModifier], mod2: Optional[ETFModifier], type: Optional[str] = None, limit: Optional[int] = None, timer: Optional[int] = None, delta_major: int = 3, delta_minor: int = 2, delta_normal: int = 1, boost_only: bool = False, drain_only: bool = False) -> Alchemist:
        if targetval == myval:
            return self
        elif targetval > myval and not drain_only:
            self.boostBy(mod1, mod2, targetval - myval, type, limit, timer, delta_major, delta_minor, delta_normal)
        elif targetval < myval and not boost_only:
            self.drainBy(mod1, mod2, myval - targetval, type, limit, timer, delta_major, delta_minor, delta_normal)
        return self

    def the_same_body_as(self, mychar: GameCharacter, targetchar: GameCharacter, skip_penis: bool = False, skip_vagina: bool = False) -> Alchemist:
        # This must be done in a certain way or shit will break.
        # Genitals first.
        if not skip_penis:
            if targetchar.body.penis is None and mychar.body.penis is not None:
                # Goodbye peepee
                self.addDrain(mod1=ETFModifier.TF_PENIS, mod2=ETFModifier.REMOVAL)
            elif targetchar.body.penis is not None and mychar.body.penis is None:
                # Grow one
                self.addBoost(mod1=ETFModifier.TF_PENIS, mod2=ETFModifier.NONE, type=targetchar.body.penis.type)
                mychar.body.penis = Penis()
                #mychar.body.penis.type = targetchar.body.penis.type
                self.penis().the_same_penis_as(mychar, targetchar)
                mychar.body.penis = None
            elif targetchar.body.penis is not None and mychar.body.penis is not None:
                self.penis().the_same_penis_as(mychar, targetchar)

        if not skip_vagina:
            if targetchar.body.vagina is None and mychar.body.vagina is not None:
                self.addDrain(mod1=ETFModifier.TF_VAGINA, mod2=ETFModifier.REMOVAL)
            elif targetchar.body.vagina is not None and mychar.body.vagina is None:
                self.addBoost(mod1=ETFModifier.TF_VAGINA, mod2=ETFModifier.NONE, type=targetchar.body.vagina.type)
                mychar.body.vagina = Vagina()
                #mychar.body.vagina.type = targetchar.body.vagina.type
                self.vagina().the_same_vagina_as(mychar, targetchar)
                mychar.body.vagina = None
            elif targetchar.body.vagina is not None and mychar.body.vagina is not None:
                self.vagina().the_same_vagina_as(mychar, targetchar)

        self.antenna().the_same_antennae_as(mychar, targetchar)
        self.arms().the_same_arms_as(mychar, targetchar)
        self.ass().the_same_ass_as(mychar, targetchar)
        # self.ass().the_same_ass_as(mychar, targetchar)
        return self

    def antenna(self) -> AntennaePotionFactory:
        return self._antennae

    def antennae(self) -> AntennaePotionFactory:
        return self._antennae

    def arms(self) -> ArmsPotionFactory:
        return self._arms

    def ass(self) -> AssPotionFactory:
        return self._ass

    def breasts(self) -> BreastsPotionFactory:
        return self._breasts

    def penis(self) -> PenisPotionFactory:
        return self._penis

    def vagina(self) -> VaginaPotionFactory:
        return self._vagina

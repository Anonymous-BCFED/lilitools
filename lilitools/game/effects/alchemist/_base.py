from __future__ import annotations

import typing
if typing.TYPE_CHECKING:
    from lilitools.game.effects.alchemist import Alchemist

class BasePotionFactory:
    def __init__(self, alchemist: 'Alchemist') -> None:
        self.alchemist: 'Alchemist' = alchemist

    # ass().has().flat_buns().and_().is_().dry().and_().the_same_as(char.body.ass)
    def has(self) -> BasePotionFactory:
        return self

    def is_(self) -> BasePotionFactory:
        return self

    def and_(self) -> BasePotionFactory:
        return self

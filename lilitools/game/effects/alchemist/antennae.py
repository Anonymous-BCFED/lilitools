from __future__ import annotations

from typing import Optional

from lilitools.game.effects.alchemist.tfsubtypes import TFSUBTYPES
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.enums.tf_modifier import ETFModifier

from ._base import BasePotionFactory


class AntennaePotionFactory(BasePotionFactory):
    MOD1: ETFModifier = ETFModifier.TF_ANTENNA

    # TF_MOD_SIZE
    def change_size(self, delta: int, boost_only: bool = False, drain_only: bool = False) -> AntennaePotionFactory:
        self.alchemist.addDelta(self.MOD1, ETFModifier.TF_MOD_SIZE, delta, boost_only=boost_only,drain_only=drain_only)
        return self

    def boost_size_by(self, amount: int) -> AntennaePotionFactory:
        self.alchemist.addBoost(self.MOD1, ETFModifier.TF_MOD_SIZE, limit=amount)
        return self

    def drain_size_by(self, amount: int) -> AntennaePotionFactory:
        self.alchemist.addDrain(self.MOD1, ETFModifier.TF_MOD_SIZE, limit=amount)
        return self

    def the_same_size_as(self, me: GameCharacter, other: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> AntennaePotionFactory:
        self.alchemist.addDiff(self.MOD1, ETFModifier.TF_MOD_SIZE, me.body.antennae.length, other.body.antennae.length, boost_only=boost_only, drain_only=drain_only)
        return self

    # TF_MOD_COUNT
    def change_row_count(self, delta: int, boost_only: bool = False, drain_only: bool = False) -> AntennaePotionFactory:
        self.alchemist.addDelta(self.MOD1, ETFModifier.TF_MOD_COUNT, delta, boost_only=boost_only, drain_only=drain_only)
        return self

    def boost_row_count_by(self, amount: int) -> AntennaePotionFactory:
        self.alchemist.addBoost(self.MOD1, ETFModifier.TF_MOD_COUNT, limit=amount)
        return self

    def drain_row_count_by(self, amount: int) -> AntennaePotionFactory:
        self.alchemist.addDrain(self.MOD1, ETFModifier.TF_MOD_COUNT, limit=amount)
        return self

    def the_same_row_count_as(self, me: GameCharacter, other: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> AntennaePotionFactory:
        self.alchemist.addDiff(self.MOD1, ETFModifier.TF_MOD_COUNT, me.body.antennae.rows, other.body.antennae.rows, boost_only=boost_only, drain_only=drain_only)
        return self

    # TF_MOD_COUNT_SECONDARY
    def change_count_per_row(self, delta: int, boost_only: bool = False, drain_only: bool = False) -> AntennaePotionFactory:
        self.alchemist.addDelta(self.MOD1, ETFModifier.TF_MOD_COUNT_SECONDARY, delta, boost_only=boost_only, drain_only=drain_only)
        return self

    def boost_count_per_row_by(self, amount: int) -> AntennaePotionFactory:
        self.alchemist.addBoost(self.MOD1, ETFModifier.TF_MOD_COUNT, limit=amount)
        return self

    def drain_count_per_row_by(self, amount: int) -> AntennaePotionFactory:
        self.alchemist.addDrain(self.MOD1, ETFModifier.TF_MOD_COUNT, limit=amount)
        return self

    def the_same_count_per_row_as(self, me: GameCharacter, other: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> AntennaePotionFactory:
        self.alchemist.addDiff(self.MOD1, ETFModifier.TF_MOD_COUNT_SECONDARY, me.body.antennae.antennaePerRow, other.body.antennae.antennaePerRow, boost_only=boost_only, drain_only=drain_only)
        return self

    # TF_REMOVE
    def remove_all(self) -> AntennaePotionFactory:
        self.alchemist.addAtomic(self.MOD1, ETFModifier.REMOVAL)
        return self

    # TF_TYPE_X
    def set_type(self, typeid: Optional[str] = None, subtype: int = 0) -> AntennaePotionFactory:
        self.alchemist.addAtomic(self.MOD1, TFSUBTYPES[subtype], type=typeid)
        return self

    def set_same_type_as(self, me: GameCharacter, other:GameCharacter) -> AntennaePotionFactory:
        if other.body.antennae.type == 'NONE':
            self.remove_all()
        else:
            self.set_type(other.body.antennae.type) # ???
        return self

    def the_same_antennae_as(self, me: GameCharacter, other:GameCharacter) -> AntennaePotionFactory:
        self.set_same_type_as(me, other)
        self.the_same_size_as(me, other)
        self.the_same_row_count_as(me, other)
        self.the_same_count_per_row_as(me, other)
        return self

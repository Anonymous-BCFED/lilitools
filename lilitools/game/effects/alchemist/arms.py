from __future__ import annotations
from typing import Optional, Union

from lilitools.game.effects.alchemist.body_hair_mixin import BodyHairMixins
from lilitools.game.effects.alchemist.tfsubtypes import TFSUBTYPES
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.enums.tf_modifier import ETFModifier


class ArmsPotionFactory(BodyHairMixins):
    MOD1 = ETFModifier.TF_ARMS
    
    BODYHAIR_PRIMARY_MOD = ETFModifier.TF_ARMS
    BODYHAIR_BODY_ATTR = 'arms'
    BODYHAIR_BODY_SUBATTR = 'underarmHair'


    # TF_MOD_TYPE
    def set_type(self, typeid: Optional[str] = None, subtype: int = 0) -> ArmsPotionFactory:
        self.alchemist.addAtomic(self.MOD1, TFSUBTYPES[subtype], type=typeid)
        return self
    def the_same_arm_type_as(self, targetchar:GameCharacter) -> ArmsPotionFactory:
        self.set_type(targetchar.body.arms.type)
        return self

    # TF_MOD_COUNT

    ####################
    # Rows
    ####################
    def change_rows_by(self, delta: int, boost_only: bool = False, drain_only: bool = False) -> ArmsPotionFactory:
        self.alchemist.addDelta(self.MOD1, ETFModifier.TF_MOD_COUNT, delta, boost_only=boost_only, drain_only=drain_only)
        return self

    def boost_rows_by(self, amount: int) -> ArmsPotionFactory:
        self.alchemist.boostBy(self.MOD1, ETFModifier.TF_MOD_COUNT, amount)
        return self

    def drain_rows_by(self, amount: int) -> ArmsPotionFactory:
        self.alchemist.drainBy(self.MOD1, ETFModifier.TF_MOD_COUNT, amount)
        return self

    def the_same_number_of_rows_as(self, mychar: GameCharacter, targetchar: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> ArmsPotionFactory:
        self.alchemist.addDiff(self.MOD1, ETFModifier.TF_MOD_COUNT, mychar.body.arms.rows, targetchar.body.arms.rows, boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_arms_as(self, mychar: GameCharacter, targetchar: GameCharacter) -> ArmsPotionFactory:
        self.the_same_arm_type_as(targetchar)
        self.the_same_number_of_rows_as(mychar, targetchar)
        self.the_same_body_hair_as(mychar, targetchar)
        return self

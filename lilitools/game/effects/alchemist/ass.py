from __future__ import annotations
from typing import Optional, Union

from lilitools.game.effects.alchemist.body_hair_mixin import BodyHairMixins
from lilitools.game.effects.alchemist.orifice_mixin import OrificePotionMixins
from lilitools.game.effects.alchemist.tfsubtypes import TFSUBTYPES
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.enums.tf_modifier import ETFModifier


class AssPotionFactory(OrificePotionMixins, BodyHairMixins):
    MOD1 = ETFModifier.TF_ASS

    ORIFICE_PRIMARY_MOD: ETFModifier = ETFModifier.TF_ASS
    ORIFICE_BODY_ATTR: str = 'anus'
    ORIFICE_ORIFICE_ATTR: str = 'orifice'

    BODYHAIR_PRIMARY_MOD: ETFModifier = ETFModifier.TF_ASS
    BODYHAIR_BODY_ATTR: str = 'ass'
    BODYHAIR_BODY_SUBATTR: str = 'assHair'

    ####################
    # Type
    ####################
    def set_type(self, typeid: Optional[str] = None, subtype: int = 0) -> AssPotionFactory:
        self.alchemist.addAtomic(self.MOD1, TFSUBTYPES[subtype], type=typeid)
        return self

    def the_same_type_as(self, targetchar: GameCharacter) -> AssPotionFactory:
        self.set_type(targetchar.body.ass.type)
        return self

    ####################
    # Bun Size
    ####################
    def change_bun_size_by(self, delta: int, boost_only: bool = False, drain_only: bool = False) -> AssPotionFactory:
        self.alchemist.addDelta(self.MOD1, ETFModifier.TF_MOD_SIZE, delta, boost_only=boost_only, drain_only=drain_only)
        return self

    def boost_bun_size_by(self, amount: int) -> AssPotionFactory:
        self.alchemist.boostBy(self.MOD1, ETFModifier.TF_MOD_SIZE, amount)
        return self

    def drain_bun_size_by(self, amount: int) -> AssPotionFactory:
        self.alchemist.drainBy(self.MOD1, ETFModifier.TF_MOD_SIZE, amount)
        return self

    def the_same_bun_size_as(self, mychar: GameCharacter, targetchar: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> AssPotionFactory:
        self.alchemist.addDiff(self.MOD1, ETFModifier.TF_MOD_SIZE, mychar.body.ass.assSize, targetchar.body.ass.assSize, boost_only=boost_only, drain_only=drain_only)
        return self

    ####################
    # Hip Size
    ####################
    def change_hip_size_by(self, delta: int, boost_only: bool = False, drain_only: bool = False) -> AssPotionFactory:
        self.alchemist.addDelta(self.MOD1, ETFModifier.TF_MOD_SIZE_SECONDARY, delta, boost_only=boost_only, drain_only=drain_only)
        return self

    def boost_hip_size_by(self, amount: int) -> AssPotionFactory:
        self.alchemist.boostBy(self.MOD1, ETFModifier.TF_MOD_SIZE_SECONDARY, amount)
        return self

    def drain_hip_size_by(self, amount: int) -> AssPotionFactory:
        self.alchemist.drainBy(self.MOD1, ETFModifier.TF_MOD_SIZE_SECONDARY, amount)
        return self

    def the_same_hip_size_as(self, mychar: GameCharacter, targetchar: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> AssPotionFactory:
        self.alchemist.addDiff(self.MOD1, ETFModifier.TF_MOD_SIZE_SECONDARY, mychar.body.ass.hipSize, targetchar.body.ass.hipSize, boost_only=boost_only, drain_only=drain_only)
        return self

    ####################
    # Same
    ####################
    def the_same_ass_as(self, mychar: GameCharacter, targetchar: GameCharacter) -> AssPotionFactory:
        self.the_same_type_as(targetchar)
        self.the_same_body_hair_as(mychar, targetchar)
        self.the_same_bun_size_as(mychar, targetchar)
        self.the_same_hip_size_as(mychar, targetchar)
        self.the_same_orifice_as(mychar, targetchar)
        return self

from __future__ import annotations

from typing import Optional, cast

from lilitools.saves.character.enums.body_hair import EBodyHair
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.enums.tf_modifier import ETFModifier
from lilitools.saves.enums.tf_potency import ETFPotency

from ._base import BasePotionFactory


class BodyHairMixins(BasePotionFactory):
    BODYHAIR_PRIMARY_MOD: ETFModifier
    BODYHAIR_BODY_ATTR: str
    BODYHAIR_BODY_SUBATTR: Optional[str] = None

    def _getBodyHairValue(self, char: GameCharacter) -> EBodyHair:
        src = getattr(char.body, self.BODYHAIR_BODY_ATTR)
        if self.BODYHAIR_BODY_SUBATTR is not None:
            src = getattr(src, self.BODYHAIR_BODY_SUBATTR)
        return cast(EBodyHair, src)

    def add_body_hair(self, strength: ETFPotency = ETFPotency.BOOST) -> BodyHairMixins:
        self.alchemist.addEffect(self.alchemist.base, self.BODYHAIR_PRIMARY_MOD, ETFModifier.TF_MOD_BODY_HAIR, strength, None)
        return self
        
    def remove_body_hair(self, strength: ETFPotency = ETFPotency.DRAIN) -> BodyHairMixins:
        self.alchemist.addEffect(self.alchemist.base, self.BODYHAIR_PRIMARY_MOD, ETFModifier.TF_MOD_BODY_HAIR, strength, None)
        return self

    def the_same_body_hair_as(self, mychar: GameCharacter, targetchar: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> BodyHairMixins:
        self.alchemist.generateLinearEffectDiff(self._getBodyHairValue(mychar).value, self._getBodyHairValue(targetchar).value, self.BODYHAIR_PRIMARY_MOD, ETFModifier.TF_MOD_BODY_HAIR, self.alchemist.base, boost_only=boost_only, drain_only=drain_only)
        return self

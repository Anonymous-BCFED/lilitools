from __future__ import annotations
from typing import Optional, Union

from lilitools.game.effects.alchemist.body_hair_mixin import BodyHairMixins
from lilitools.game.effects.alchemist.orifice_mixin import OrificePotionMixins
from lilitools.game.effects.alchemist.tfsubtypes import TFSUBTYPES
from lilitools.saves.character.body.enums.cupsize import ECupSize
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.enums.tf_modifier import ETFModifier


class BreastsPotionFactory(OrificePotionMixins, BodyHairMixins):
    MOD1 = ETFModifier.TF_BREASTS

    ORIFICE_PRIMARY_MOD: ETFModifier = ETFModifier.TF_BREASTS
    ORIFICE_BODY_ATTR: str = 'nipples'
    ORIFICE_ORIFICE_ATTR: str = 'orifice'

    ####################
    # Type
    ####################
    def set_type(self, typeid: Optional[str] = None, subtype: int = 0) -> BreastsPotionFactory:
        self.alchemist.addAtomic(self.MOD1, TFSUBTYPES[subtype], type=typeid)
        return self

    def the_same_type_as(self, targetchar: GameCharacter) -> BreastsPotionFactory:
        self.set_type(targetchar.body.breasts.type)
        return self

    #######################
    # Cup Size
    #######################
    # TF_MOD_SIZE
    def of_cup_size(self, limit: Union[int, ECupSize], boost_only: bool = False, drain_only: bool = False) -> BreastsPotionFactory:
        if isinstance(limit, ECupSize):
            limit = limit.getMeasurement()
        self.enchanter.addClampMods(ETFModifier.TF_BREASTS, ETFModifier.TF_MOD_SIZE, limit, boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_cup_size_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> BreastsPotionFactory:
        self.of_cup_size(char.body.breasts.size, boost_only=boost_only, drain_only=drain_only)
        return self

    #######################
    # Nipple Size
    #######################
    # TF_MOD_SIZE_SECONDARY
    def of_nipple_size(self, limit: int, boost_only: bool = False, drain_only: bool = False) -> BreastsPotionFactory:
        self.enchanter.addClampMods(ETFModifier.TF_BREASTS, ETFModifier.TF_MOD_SIZE_SECONDARY, limit, boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_nipple_size_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> BreastsPotionFactory:
        self.of_nipple_size(char.body.nipples.nippleSize, boost_only=boost_only, drain_only=drain_only)
        return self

    #######################
    # Areolae Size
    #######################
    # TF_MOD_SIZE_TERTIARY
    def of_areolae_size(self, limit: int, boost_only: bool = False, drain_only: bool = False) -> BreastsPotionFactory:
        self.enchanter.addClampMods(ETFModifier.TF_BREASTS, ETFModifier.TF_MOD_SIZE_TERTIARY, limit, boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_areolae_size_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> BreastsPotionFactory:
        self.of_areolae_size(char.body.nipples.areolaeSize, boost_only=boost_only, drain_only=drain_only)
        return self

    #######################
    # Lactation
    #######################
    # TF_MOD_WETNESS
    def of_lactation_amount(self, limit: int, boost_only: bool = False, drain_only: bool = False) -> BreastsPotionFactory:
        self.enchanter.addClampMods(ETFModifier.TF_BREASTS, ETFModifier.TF_MOD_WETNESS, limit, boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_lactation_amount_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> BreastEnchanterFactory:
        self.of_lactation_amount(char.body.breasts.milkStorage, boost_only=boost_only, drain_only=drain_only)
        return self

    #######################
    # Regeneration
    #######################
    # TF_MOD_REGENERATION
    def of_regeneration_rate(self, limit: int, boost_only: bool = False, drain_only: bool = False) -> BreastsPotionFactory:
        self.enchanter.addClampMods(ETFModifier.TF_BREASTS, ETFModifier.TF_MOD_REGENERATION, limit, boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_regeneration_rate_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> BreastsPotionFactory:
        self.of_regeneration_rate(char.body.breasts.milkRegeneration, boost_only=boost_only, drain_only=drain_only)
        return self

    ####################
    # Same
    ####################
    def the_same_ass_as(self, mychar: GameCharacter, targetchar: GameCharacter) -> BreastsPotionFactory:
        self.the_same_type_as(targetchar)
        self.the_same_body_hair_as(mychar, targetchar)
        self.the_same_orifice_as(mychar, targetchar)
        return self

from __future__ import annotations

from typing import Dict, Iterable, Optional

from lilitools.game.effects.alchemist._base import BasePotionFactory
from lilitools.saves.character.body.orifice import Orifice
from lilitools.saves.character.enums.orifice_modifier import EOrificeModifier
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.enums.tf_modifier import ETFModifier


class OrificePotionMixins(BasePotionFactory):
    ORIFICE_PRIMARY_MOD: ETFModifier
    ORIFICE_BODY_ATTR: str
    ORIFICE_ORIFICE_ATTR: str
    ORIFICE_OFCMOD2TFMOD: Dict[EOrificeModifier, ETFModifier] = {
        EOrificeModifier.MUSCLE_CONTROL: ETFModifier.TF_MOD_ORIFICE_MUSCLED,
        EOrificeModifier.PUFFY: ETFModifier.TF_MOD_ORIFICE_PUFFY,
        EOrificeModifier.RIBBED: ETFModifier.TF_MOD_ORIFICE_RIBBED,
        EOrificeModifier.TENTACLED: ETFModifier.TF_MOD_ORIFICE_TENTACLED,
    }

    ORIFICE_WETNESS_TFMOD: Optional[ETFModifier] = ETFModifier.TF_MOD_WETNESS

    def _getOrifice(self, char: GameCharacter) -> Orifice:
        return getattr(getattr(char.body, self.ORIFICE_BODY_ATTR), self.ORIFICE_ORIFICE_ATTR)

    ####################
    # Wetness
    ####################
    def boost_wetness_by(self, by: int) -> OrificePotionMixins:
        self.alchemist.boostBy(self.ORIFICE_PRIMARY_MOD, ETFModifier.TF_MOD_WETNESS, by)
        return self

    def drain_wetness_by(self, by: int) -> OrificePotionMixins:
        self.alchemist.boostBy(self.ORIFICE_PRIMARY_MOD, ETFModifier.TF_MOD_WETNESS, by)
        return self

    def the_same_wetness_as(self, mychar: GameCharacter, targetchar: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> OrificePotionMixins:
        self.alchemist.generateLinearEffectDiff(self._getOrifice(mychar).wetness, self._getOrifice(targetchar).wetness, self.ORIFICE_PRIMARY_MOD, ETFModifier.TF_MOD_WETNESS, boost_only=boost_only, drain_only=drain_only)
        return self

    ####################
    # Depth
    ####################
    def boost_depth_by(self, by: int) -> OrificePotionMixins:
        self.alchemist.boostBy(self.ORIFICE_PRIMARY_MOD, ETFModifier.TF_MOD_DEPTH, by)
        return self

    def drain_depth_by(self, by: int) -> OrificePotionMixins:
        self.alchemist.drainBy(self.ORIFICE_PRIMARY_MOD, ETFModifier.TF_MOD_DEPTH, by)
        return self

    def the_same_depth_as(self, mychar: GameCharacter, targetchar: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> OrificePotionMixins:
        self.alchemist.generateLinearEffectDiff(self._getOrifice(mychar).depth, self._getOrifice(targetchar).depth, self.ORIFICE_PRIMARY_MOD, ETFModifier.TF_MOD_DEPTH, boost_only=boost_only, drain_only=drain_only)
        return self

    ####################
    # Elasticity
    ####################
    def boost_elasticity_by(self, by: int) -> OrificePotionMixins:
        self.alchemist.boostBy(self.ORIFICE_PRIMARY_MOD, ETFModifier.TF_MOD_ELASTICITY, by)
        return self

    def drain_elasticity_by(self, by: int) -> OrificePotionMixins:
        self.alchemist.drainBy(self.ORIFICE_PRIMARY_MOD, ETFModifier.TF_MOD_ELASTICITY, by)
        return self

    def the_same_elasticity_as(self, mychar: GameCharacter, targetchar: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> OrificePotionMixins:
        self.alchemist.generateLinearEffectDiff(self._getOrifice(mychar).elasticity, self._getOrifice(targetchar).elasticity, self.ORIFICE_PRIMARY_MOD, ETFModifier.TF_MOD_ELASTICITY, boost_only=boost_only, drain_only=drain_only)
        return self

    ####################
    # Plasticity
    ####################
    def boost_plasticity_by(self, by: int) -> OrificePotionMixins:
        self.alchemist.boostBy(self.ORIFICE_PRIMARY_MOD, ETFModifier.TF_MOD_PLASTICITY, by)
        return self

    def drain_plasticity_by(self, by: int) -> OrificePotionMixins:
        self.alchemist.drainBy(self.ORIFICE_PRIMARY_MOD, ETFModifier.TF_MOD_PLASTICITY, by)
        return self

    def the_same_plasticity_as(self, mychar: GameCharacter, targetchar: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> OrificePotionMixins:
        self.alchemist.generateLinearEffectDiff(self._getOrifice(mychar).plasticity, self._getOrifice(targetchar).plasticity, self.ORIFICE_PRIMARY_MOD, ETFModifier.TF_MOD_PLASTICITY, boost_only=boost_only, drain_only=drain_only)
        return self

    ####################
    # Capacity
    ####################
    def boost_capacity_by(self, by: int) -> OrificePotionMixins:
        self.alchemist.boostBy(self.ORIFICE_PRIMARY_MOD, ETFModifier.TF_MOD_CAPACITY, by)
        return self

    def drain_capacity_by(self, by: int) -> OrificePotionMixins:
        self.alchemist.drainBy(self.ORIFICE_PRIMARY_MOD, ETFModifier.TF_MOD_CAPACITY, by)
        return self

    def the_same_capacity_as(self, mychar: GameCharacter, targetchar: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> OrificePotionMixins:
        self.alchemist.generateLinearEffectDiff(self._getOrifice(mychar).capacity, self._getOrifice(targetchar).capacity, self.ORIFICE_PRIMARY_MOD, ETFModifier.TF_MOD_CAPACITY, boost_only=boost_only, drain_only=drain_only)
        return self

    ####################
    # Mods
    #####################

    def set_orifice_mod(self, mod: EOrificeModifier, val: bool) -> OrificePotionMixins:
        if val:
            self.alchemist.addMajorBoost(self.ORIFICE_PRIMARY_MOD, self.ORIFICE_OFCMOD2TFMOD[mod])
        else:
            self.alchemist.addMajorDrain(self.ORIFICE_PRIMARY_MOD, self.ORIFICE_OFCMOD2TFMOD[mod])
        return self

    def using_orifice_mods(self, mods: Iterable[EOrificeModifier]) -> OrificePotionMixins:
        for mod in mods:
            self.set_orifice_mod(mod, True)
        return self

    def not_using_orifice_mods(self, mods: Iterable[EOrificeModifier]) -> OrificePotionMixins:
        for mod in mods:
            self.set_orifice_mod(mod, False)
        return self

    def the_same_orifice_mods_as(self, mychar: GameCharacter, targetchar: GameCharacter) -> OrificePotionMixins:
        for mod in EOrificeModifier:
            theirs = mod in self._getOrifice(targetchar).orificeModifiers
            mine = mod in self._getOrifice(mychar).orificeModifiers
            if theirs != mine:
                self.set_orifice_mod(mod, theirs)
        return self

    ################
    # Same
    ################
    def the_same_orifice_as(self, mychar: GameCharacter, targetchar: GameCharacter) -> OrificePotionMixins:
        self.the_same_wetness_as(mychar, targetchar)
        self.the_same_depth_as(mychar, targetchar)
        self.the_same_elasticity_as(mychar, targetchar)
        self.the_same_plasticity_as(mychar, targetchar)
        self.the_same_capacity_as(mychar, targetchar)
        self.the_same_orifice_mods_as(mychar, targetchar)
        return self

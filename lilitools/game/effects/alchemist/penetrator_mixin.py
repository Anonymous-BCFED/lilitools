from __future__ import annotations

from typing import Iterable

from lilitools.game.effects.alchemist._base import BasePotionFactory
from lilitools.saves.character.body.penetrator import Penetrator
from lilitools.saves.character.enums.penetrator_modifier import EPenetratorModifier
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.enums.tf_modifier import ETFModifier
from lilitools.tfmodmaps import PEN2TF


class PenetratorPotionMixins(BasePotionFactory):
    PENETRATOR_PRIMARY_MOD: ETFModifier
    PENETRATOR_BODY_ATTR: str
    PENETRATOR_PENETRATOR_ATTR: str

    def _getPenetrator(self, char: GameCharacter) -> Penetrator:
        return getattr(getattr(char.body, self.PENETRATOR_BODY_ATTR), self.PENETRATOR_PENETRATOR_ATTR)

    ####################
    # Length
    ####################
    def boost_length_by(self, by: int) -> PenetratorPotionMixins:
        self.alchemist.boostBy(self.PENETRATOR_PRIMARY_MOD, ETFModifier.TF_MOD_SIZE, by)
        return self

    def drain_length_by(self, by: int) -> PenetratorPotionMixins:
        self.alchemist.boostBy(self.PENETRATOR_PRIMARY_MOD, ETFModifier.TF_MOD_SIZE, by)
        return self

    def the_same_length_as(self, mychar: GameCharacter, targetchar: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> PenetratorPotionMixins:
        self.alchemist.generateLinearEffectDiff(self._getPenetrator(mychar).length, self._getPenetrator(targetchar).length, self.PENETRATOR_PRIMARY_MOD, ETFModifier.TF_MOD_SIZE,boost_only=boost_only,drain_only=drain_only)
        return self

    ####################
    # Girth
    ####################
    def boost_girth_by(self, by: int) -> PenetratorPotionMixins:
        self.alchemist.boostBy(self.PENETRATOR_PRIMARY_MOD, ETFModifier.TF_MOD_SIZE_SECONDARY, by)
        return self

    def drain_girth_by(self, by: int) -> PenetratorPotionMixins:
        self.alchemist.boostBy(self.PENETRATOR_PRIMARY_MOD, ETFModifier.TF_MOD_SIZE_SECONDARY, by)
        return self

    def the_same_girth_as(self, mychar: GameCharacter, targetchar: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> PenetratorPotionMixins:
        self.alchemist.generateLinearEffectDiff(self._getPenetrator(mychar).girth, self._getPenetrator(targetchar).girth, self.PENETRATOR_PRIMARY_MOD, ETFModifier.TF_MOD_SIZE_SECONDARY, boost_only=boost_only, drain_only=drain_only)
        return self

    ####################
    # Mods
    #####################

    def set_penetrator_mod(self, mod: EPenetratorModifier, val: bool) -> PenetratorPotionMixins:
        if val:
            self.alchemist.addBoost(self.PENETRATOR_PRIMARY_MOD, PEN2TF[mod])
        else:
            self.alchemist.addDrain(self.PENETRATOR_PRIMARY_MOD, PEN2TF[mod])
        return self

    def using_penetrator_mods(self, mods: Iterable[EPenetratorModifier]) -> PenetratorPotionMixins:
        for mod in mods:
            self.set_penetrator_mod(mod, True)
        return self

    def not_using_penetrator_mods(self, mods: Iterable[EPenetratorModifier]) -> PenetratorPotionMixins:
        for mod in mods:
            self.set_penetrator_mod(mod, False)
        return self

    def the_same_penetrator_mods_as(self, mychar: GameCharacter, targetchar: GameCharacter) -> PenetratorPotionMixins:
        for mod in EPenetratorModifier:
            theirs = mod in self._getPenetrator(targetchar).penetratorModifiers
            mine = mod in self._getPenetrator(mychar).penetratorModifiers
            if theirs != mine:
                self.set_penetrator_mod(mod, theirs)
        return self

    #####################
    # Same
    #####################
    def the_same_penetrator_as(self, mychar: GameCharacter, targetchar: GameCharacter) -> PenetratorPotionMixins:
        self.the_same_girth_as(mychar, targetchar)
        self.the_same_length_as(mychar, targetchar)
        self.the_same_penetrator_mods_as(mychar, targetchar)
        return self

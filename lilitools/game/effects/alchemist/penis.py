from __future__ import annotations

from typing import Optional

from lilitools.game.effects.alchemist.orifice_mixin import OrificePotionMixins
from lilitools.game.effects.alchemist.penetrator_mixin import PenetratorPotionMixins
from lilitools.game.effects.alchemist.tfsubtypes import TFSUBTYPES
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.enums.tf_modifier import ETFModifier
from lilitools.utils import clampInt


class PenisPotionFactory(OrificePotionMixins, PenetratorPotionMixins):
    MOD1 = ETFModifier.TF_PENIS

    ORIFICE_PRIMARY_MOD: ETFModifier = ETFModifier.TF_PENIS
    ORIFICE_BODY_ATTR: str = 'penis'
    ORIFICE_ORIFICE_ATTR: str = 'urethra'

    PENETRATOR_PRIMARY_MOD: ETFModifier = ETFModifier.TF_PENIS
    PENETRATOR_BODY_ATTR = 'penis'
    PENETRATOR_PENETRATOR_ATTR = 'penetrator'

    ####################
    # TYpe
    ####################
    def set_type(self, typeid: Optional[str] = None, subtype: int = 0) -> PenisPotionFactory:
        self.alchemist.addAtomic(self.MOD1, TFSUBTYPES[subtype], type=typeid)
        return self

    def the_same_type_as(self, targetchar: GameCharacter) -> PenisPotionFactory:
        self.set_type(targetchar.body.penis.type)
        return self

    #################
    # Enable/Disable
    #################
    def has_a_penis(self) -> PenisPotionFactory:
        self.alchemist.addAtomic(ETFModifier.TF_PENIS,ETFModifier.TF_TYPE_1)
        return self
    def has_no_penis(self) -> PenisPotionFactory:
        self.alchemist.addAtomic(ETFModifier.TF_PENIS, ETFModifier.REMOVAL)
        return self

    #################
    # Cum Expulsion
    #################
    def boost_cum_expulsion_by(self, by: int) -> PenisPotionFactory:
        self.alchemist.boostBy(ETFModifier.TF_PENIS, ETFModifier.TF_MOD_CUM_EXPULSION, clampInt(by, 0, 100))
        return self
    def drain_cum_expulsion_by(self, by: int) -> PenisPotionFactory:
        self.alchemist.drainBy(ETFModifier.TF_PENIS, ETFModifier.TF_MOD_CUM_EXPULSION, clampInt(by, 0, 100))
        return self

    def the_same_cum_expulsion_as(self, mychar: GameCharacter, targetchar: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> PenisPotionFactory:
        self.alchemist.generateLinearEffectDiff(mychar.body.testicles.cumExpulsion, targetchar.body.testicles.cumExpulsion, ETFModifier.TF_PENIS, ETFModifier.TF_MOD_CUM_EXPULSION, boost_only=boost_only, drain_only=drain_only)
        return self

    #################ass
    # Cum Storage
    #################
    def boost_cum_storage_by(self, by: int) -> PenisPotionFactory:
        self.alchemist.boostBy(ETFModifier.TF_CUM, ETFModifier.TF_MOD_WETNESS, by)
        return self
    def drain_cum_storage_by(self, by: int) -> PenisPotionFactory:
        self.alchemist.drainBy(ETFModifier.TF_CUM, ETFModifier.TF_MOD_WETNESS, by)
        return self

    def the_same_cum_storage_as(self, mychar: GameCharacter, targetchar: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> PenisPotionFactory:
        self.alchemist.generateLinearEffectDiff(mychar.body.testicles.cumStorage, targetchar.body.testicles.cumStorage, ETFModifier.TF_CUM, ETFModifier.TF_MOD_WETNESS, boost_only=boost_only, drain_only=drain_only)
        return self

    #################
    # Cum Regeneration
    #################
    def boost_cum_regeneration_by(self, by: int) -> PenisPotionFactory:
        self.alchemist.boostBy(ETFModifier.TF_CUM, ETFModifier.TF_MOD_REGENERATION, by)
        return self
    def drain_cum_regeneration_by(self, by: int) -> PenisPotionFactory:
        self.alchemist.drainBy(ETFModifier.TF_CUM, ETFModifier.TF_MOD_REGENERATION, by)
        return self

    def the_same_cum_regeneration_as(self, mychar: GameCharacter, targetchar: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> PenisPotionFactory:
        self.alchemist.generateLinearEffectDiff(mychar.body.testicles.cumRegeneration, targetchar.body.testicles.cumRegeneration, ETFModifier.TF_CUM, ETFModifier.TF_MOD_REGENERATION, boost_only=boost_only, drain_only=drain_only)
        return self

    #################
    # Ball Size
    #################
    def boost_ball_size_by(self, by: int) -> PenisPotionFactory:
        self.alchemist.boostBy(ETFModifier.TF_PENIS, ETFModifier.TF_MOD_SIZE_TERTIARY, by)
        return self
    def drain_ball_size_by(self, by: int) -> PenisPotionFactory:
        self.alchemist.drainBy(ETFModifier.TF_PENIS, ETFModifier.TF_MOD_SIZE_TERTIARY, by)
        return self

    def the_same_ball_size_as(self, mychar: GameCharacter, targetchar: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> PenisPotionFactory:
        self.alchemist.generateLinearEffectDiff(mychar.body.testicles.testicleSize, targetchar.body.testicles.testicleSize, ETFModifier.TF_PENIS, ETFModifier.TF_MOD_SIZE_TERTIARY, boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_penis_as(self, mychar: GameCharacter, targetchar:GameCharacter) -> PenisPotionFactory:
        if targetchar.body.penis is None:
            self.has_no_penis()
        else:
            self.set_type(targetchar.body.penis.type)
            self.the_same_penetrator_as(mychar, targetchar)
            self.the_same_orifice_as(mychar, targetchar)

        if targetchar.body.testicles is not None:
            self.the_same_ball_size_as(mychar, targetchar)
            self.the_same_cum_storage_as(mychar, targetchar)
            self.the_same_cum_expulsion_as(mychar, targetchar)
            self.the_same_cum_regeneration_as(mychar, targetchar)

        return self



from typing import Final, List

from lilitools.saves.enums.tf_modifier import ETFModifier

## from [LT]/src/com/lilithsthrone/game/character/body/types/BodyPartTypeInterface.java: public default TFModifier getTFTypeModifier(List<? extends BodyPartTypeInterface> types) { @ mlvxwDMR+3R93bS5toniy2CeFPQPeG38MebtfioSe/x8W0NjboKO6DWVhiqrAvQDJO3f9uczSseSurw2gq3Q2g==
TFSUBTYPES: Final[List[ETFModifier]] = [
    ETFModifier.TF_TYPE_1,
    ETFModifier.TF_TYPE_2,
    ETFModifier.TF_TYPE_3,
    ETFModifier.TF_TYPE_4,
    ETFModifier.TF_TYPE_5,
    ETFModifier.TF_TYPE_6,
    ETFModifier.TF_TYPE_7,
    ETFModifier.TF_TYPE_8,
    ETFModifier.TF_TYPE_9,
    ETFModifier.TF_TYPE_10,
]

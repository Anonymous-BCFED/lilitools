from __future__ import annotations

from typing import Optional

from lilitools.game.effects.alchemist.orifice_mixin import OrificePotionMixins
from lilitools.game.effects.alchemist.penetrator_mixin import PenetratorPotionMixins
from lilitools.game.effects.alchemist.tfsubtypes import TFSUBTYPES
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.enums.tf_modifier import ETFModifier
from lilitools.utils import clampInt


class VaginaPotionFactory(OrificePotionMixins, PenetratorPotionMixins):
    MOD1 = ETFModifier.TF_VAGINA

    ORIFICE_PRIMARY_MOD: ETFModifier = ETFModifier.TF_VAGINA
    ORIFICE_BODY_ATTR: str = 'vagina'
    ORIFICE_ORIFICE_ATTR: str = 'urethra'

    PENETRATOR_PRIMARY_MOD: ETFModifier = ETFModifier.TF_VAGINA
    PENETRATOR_BODY_ATTR = 'vagina'
    PENETRATOR_PENETRATOR_ATTR = 'penetrator'

    ####################
    # TYpe
    ####################
    def set_type(self, typeid: Optional[str] = None, subtype: int = 0) -> VaginaPotionFactory:
        self.alchemist.addAtomic(self.MOD1, TFSUBTYPES[subtype], type=typeid)
        return self

    def the_same_type_as(self, targetchar: GameCharacter) -> VaginaPotionFactory:
        self.set_type(targetchar.body.vagina.type)
        return self

    #################
    # Enable/Disable
    #################
    def has_a_vagina(self) -> VaginaPotionFactory:
        self.alchemist.addAtomic(ETFModifier.TF_VAGINA,ETFModifier.TF_TYPE_1)
        return self
    def has_no_vagina(self) -> VaginaPotionFactory:
        self.alchemist.addAtomic(ETFModifier.TF_VAGINA, ETFModifier.REMOVAL)
        return self

    def the_same_vagina_as(self, mychar: GameCharacter, targetchar:GameCharacter) -> VaginaPotionFactory:
        if targetchar.body.vagina is None:
            self.has_no_vagina()
        else:
            self.set_type(targetchar.body.vagina.type)
            self.the_same_penetrator_as(mychar, targetchar)
            self.the_same_orifice_as(mychar, targetchar)

        return self

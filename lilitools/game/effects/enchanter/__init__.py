from __future__ import annotations

import contextlib
from typing import Dict, List, Optional, Set, Tuple, Union

from lxml import etree

from lilitools.game.effects.enchanter.arms import ArmsEnchantmentFactory
from lilitools.game.effects.enchanter.ass import AssEnchantmentFactory
from lilitools.game.effects.enchanter.breasts import BreastEnchanterFactory
from lilitools.game.effects.enchanter.core import CoreEnchantmentFactory
from lilitools.game.effects.enchanter.crotch_breasts import CrotchBreastEnchanterFactory
from lilitools.game.effects.enchanter.ears import EarsEnchantmentFactory
from lilitools.game.effects.enchanter.eyes import EyesEnchantmentFactory
from lilitools.game.effects.enchanter.face import FaceEnchantmentFactory
from lilitools.game.effects.enchanter.hair import HairEnchantmentFactory
from lilitools.game.effects.enchanter.horns import HornsEnchantmentFactory
from lilitools.game.effects.enchanter.legs import LegsEnchantmentFactory
from lilitools.game.effects.enchanter.penis import PenisEnchantmentFactory
from lilitools.game.effects.enchanter.special import SpecialEnchantmentFactory
from lilitools.game.effects.enchanter.vagina import VaginaEnchantmentFactory
from lilitools.game.effects.enchanter.vaginal_urethra import (
    VaginalUrethraEnchantmentFactory,
)
from lilitools.game.effects.utils import bool2pot
from lilitools.logging import ClickWrap
from lilitools.saves.enums.known_effects import EEffectFlags, EKnownEffects
from lilitools.saves.enums.tf_modifier import ETFModifier
from lilitools.saves.enums.tf_potency import ETFPotency
from lilitools.saves.items.abstract_clothing import AbstractClothing
from lilitools.saves.items.abstract_item import AbstractItem
from lilitools.saves.items.item_effect import ItemEffect

from lilitools.game.effects.enchanter.tail import TailEnchantmentFactory

click = ClickWrap()


EXCLUDE_FROM_BOOST_TO_ZERO_CHECK = frozenset(
    [
        ETFModifier.TF_TYPE_1,
        ETFModifier.TF_TYPE_2,
        ETFModifier.TF_TYPE_3,
        ETFModifier.TF_TYPE_4,
        ETFModifier.TF_TYPE_5,
        ETFModifier.TF_TYPE_6,
        ETFModifier.TF_TYPE_7,
        ETFModifier.TF_TYPE_8,
        ETFModifier.TF_TYPE_9,
        ETFModifier.TF_TYPE_10,
        ETFModifier.TF_MOD_ORIFICE_PUFFY,
        ETFModifier.TF_MOD_ORIFICE_RIBBED,
        ETFModifier.TF_MOD_ORIFICE_TENTACLED,
        ETFModifier.TF_MOD_ORIFICE_MUSCLED,
        ETFModifier.TF_MOD_ORIFICE_PUFFY_2,
        ETFModifier.TF_MOD_ORIFICE_RIBBED_2,
        ETFModifier.TF_MOD_ORIFICE_TENTACLED_2,
        ETFModifier.TF_MOD_ORIFICE_MUSCLED_2,
        ETFModifier.TF_MOD_EYE_IRIS_CIRCLE,
        ETFModifier.TF_MOD_EYE_IRIS_HORIZONTAL,
        ETFModifier.TF_MOD_EYE_IRIS_VERTICAL,
        ETFModifier.TF_MOD_EYE_IRIS_HEART,
        ETFModifier.TF_MOD_EYE_IRIS_STAR,
        ETFModifier.TF_MOD_EYE_PUPIL_CIRCLE,
        ETFModifier.TF_MOD_EYE_PUPIL_HORIZONTAL,
        ETFModifier.TF_MOD_EYE_PUPIL_VERTICAL,
        ETFModifier.TF_MOD_EYE_PUPIL_HEART,
        ETFModifier.TF_MOD_EYE_PUPIL_STAR,
        ETFModifier.TF_MOD_BREAST_SHAPE_UDDERS,
        ETFModifier.TF_MOD_BREAST_SHAPE_ROUND,
        ETFModifier.TF_MOD_BREAST_SHAPE_NARROW,
        ETFModifier.TF_MOD_BREAST_SHAPE_WIDE,
        ETFModifier.TF_MOD_BREAST_SHAPE_POINTY,
        ETFModifier.TF_MOD_BREAST_SHAPE_PERKY,
        ETFModifier.TF_MOD_BREAST_SHAPE_SIDESET,
        ETFModifier.TF_MOD_NIPPLE_NORMAL,
        ETFModifier.TF_MOD_NIPPLE_INVERTED,
        ETFModifier.TF_MOD_NIPPLE_VAGINA,
        ETFModifier.TF_MOD_NIPPLE_LIPS,
        ETFModifier.TF_MOD_AREOLAE_CIRCLE,
        ETFModifier.TF_MOD_AREOLAE_HEART,
        ETFModifier.TF_MOD_AREOLAE_STAR,
        ETFModifier.TF_MOD_TONGUE_RIBBED,
        ETFModifier.TF_MOD_TONGUE_TENTACLED,
        ETFModifier.TF_MOD_TONGUE_BIFURCATED,
        ETFModifier.TF_MOD_TONGUE_WIDE,
        ETFModifier.TF_MOD_TONGUE_FLAT,
        ETFModifier.TF_MOD_TONGUE_STRONG,
        ETFModifier.TF_MOD_PENIS_SHEATHED,
        ETFModifier.TF_MOD_PENIS_RIBBED,
        ETFModifier.TF_MOD_PENIS_TENTACLED,
        ETFModifier.TF_MOD_PENIS_KNOTTED,
        ETFModifier.TF_MOD_PENIS_TAPERED,
        ETFModifier.TF_MOD_PENIS_FLARED,
        ETFModifier.TF_MOD_PENIS_BLUNT,
        ETFModifier.TF_MOD_PENIS_BARBED,
        ETFModifier.TF_MOD_PENIS_VEINY,
        ETFModifier.TF_MOD_PENIS_PREHENSILE,
        ETFModifier.TF_MOD_PENIS_OVIPOSITOR,
        ETFModifier.REMOVAL,
        ETFModifier.CLOTHING_ORGASM_PREVENTION,
    ]
)


class Enchanter:
    def __init__(
        self, item: Optional[Union[AbstractItem, AbstractClothing]] = None
    ) -> None:
        self.effects: List[ItemEffect] = []

        self._arms: ArmsEnchantmentFactory = ArmsEnchantmentFactory(self)
        self._ass: AssEnchantmentFactory = AssEnchantmentFactory(self)
        self._breasts: BreastEnchanterFactory = BreastEnchanterFactory(self)
        self._core: CoreEnchantmentFactory = CoreEnchantmentFactory(self)
        self._crotchBreasts: CrotchBreastEnchanterFactory = (
            CrotchBreastEnchanterFactory(self)
        )
        self._ears: EarsEnchantmentFactory = EarsEnchantmentFactory(self)
        self._eyes: EyesEnchantmentFactory = EyesEnchantmentFactory(self)
        self._face: FaceEnchantmentFactory = FaceEnchantmentFactory(self)
        self._hair: HairEnchantmentFactory = HairEnchantmentFactory(self)
        self._horns: HornsEnchantmentFactory = HornsEnchantmentFactory(self)
        self._legs: LegsEnchantmentFactory = LegsEnchantmentFactory(self)
        self._penis: PenisEnchantmentFactory = PenisEnchantmentFactory(self)
        self._special: SpecialEnchantmentFactory = SpecialEnchantmentFactory(self)
        self._tail: TailEnchantmentFactory = TailEnchantmentFactory(self)
        self._vagina: VaginaEnchantmentFactory = VaginaEnchantmentFactory(self)
        self._vaginalUrethra: VaginalUrethraEnchantmentFactory = (
            VaginalUrethraEnchantmentFactory(self)
        )

        if item is not None:
            self.appendEffects(item)

    def applyToClothing(self, item: AbstractClothing) -> Enchanter:
        item.effects.clear()
        for effect in self.effects:
            if effect.type != "CLOTHING":
                click.warn(
                    f"Dropped non-clothing effect {effect} - [BUG] Causes crashes when used in clothing."
                )
                continue
            item.effects.append(effect)
        return self

    def applyToItem(self, item: AbstractItem) -> Enchanter:
        item.itemEffects.clear()
        for effect in self.effects:
            if effect.type == "CLOTHING":
                click.warn(
                    f"Dropped clothing effect {effect} - [BUG] Undefined behaviour in items"
                )
                continue
            item.itemEffects.append(effect)
        return self

    def appendEffects(
        self, item: Optional[Union[AbstractItem, AbstractClothing]]
    ) -> Enchanter:
        if isinstance(item, AbstractClothing) and item.effects is not None:
            self.effects += item.effects
        elif isinstance(item, AbstractItem) and item.itemEffects is not None:
            self.effects += item.itemEffects
        return self

    def sort(self, reverse: bool = False) -> None:
        self.effects.sort(
            key=lambda e: (e.mod1.name, e.mod2.name, e.potency.name), reverse=reverse
        )

    def remove(
        self,
        type_: Optional[str] = None,
        mod1: Optional[ETFModifier] = None,
        mod2: Optional[ETFModifier] = None,
    ) -> None:
        def matcher(fx: ItemEffect) -> bool:
            return not (fx.type == type_ and fx.mod1 == mod1 and fx.mod2 == mod2)

        self.effects = list(filter(matcher, self.effects))

    def addEffect(
        self,
        type: str,
        mod1: Optional[ETFModifier],
        mod2: Optional[ETFModifier],
        potency: Optional[ETFPotency],
        limit: Optional[int],
        timer: Optional[int],
    ) -> Enchanter:
        assert mod1 is None or isinstance(mod1, ETFModifier)
        assert mod2 is None or isinstance(mod2, ETFModifier)
        assert isinstance(mod2, ETFModifier)
        self.effects.append(
            ItemEffect(
                type=type,
                mod1=mod1,
                mod2=mod2,
                potency=potency,
                limit=limit,
                timer=timer,
            )
        )
        return self

    def addClothingMod(
        self, mod1: ETFModifier, mod2: ETFModifier, potency: ETFPotency, limit: int
    ) -> Enchanter:
        self.addEffect(
            type="CLOTHING", mod1=mod1, mod2=mod2, potency=potency, limit=limit, timer=0
        )
        return self

    def addClampMods(
        self,
        mod1: ETFModifier,
        mod2: ETFModifier,
        limit: int,
        boost_only: bool = False,
        drain_only: bool = False,
    ) -> Enchanter:
        both = not boost_only and not drain_only
        if both or boost_only:
            self.addClothingMod(mod1, mod2, ETFPotency.MAJOR_BOOST, limit)
        if both or drain_only:
            self.addClothingMod(mod1, mod2, ETFPotency.MAJOR_DRAIN, limit)
        return self

    def addBodyPartMod(
        self, mod1: ETFModifier, mod2: ETFModifier, enabled: bool
    ) -> Enchanter:
        self.addClothingMod(mod1, mod2, bool2pot(enabled), 0)
        return self

    def optimizeForClothing(self, verbose: bool = False) -> None:
        self.optimize(verbose=verbose, for_clothing=True)

    def getKnownEffect(self, effect: ItemEffect) -> Optional[EKnownEffects]:
        for ke in EKnownEffects:
            if (
                ke.type == effect.type
                and ke.mod1 == effect.mod1
                and ke.mod2 == effect.mod2
            ):
                return ke
        return None

    def optimize(self, verbose: bool = False, for_clothing: bool = False) -> None:
        key: Tuple[ETFModifier, ETFModifier, str]
        enchantments: Dict[Tuple[ETFModifier, ETFModifier, str], ItemEffect] = {}
        effect: ItemEffect

        ctx = contextlib.nullcontext()
        if verbose:
            ctx = click.echo(f"Optimizing {len(self.effects)}...")
        with ctx:
            setEffects: Set[EKnownEffects] = set()
            for effect in self.effects:
                effect_as_str = etree.tostring(effect.toXML(), encoding="unicode")
                knownEffect = self.getKnownEffect(effect)
                if knownEffect is not None:
                    if (
                        knownEffect.flags & EEffectFlags.NO_REPEAT
                    ) == EEffectFlags.NO_REPEAT:
                        if knownEffect in setEffects:
                            click.warn(
                                f"Dropped effect {effect} - Duplicate of a non-repeatable effect. This effect will do nothing if it's applied more than once."
                            )
                            continue
                        setEffects.add(knownEffect)

                if for_clothing and effect.type != "CLOTHING":
                    click.warn(
                        f"Dropped non-clothing effect {effect} - [BUG] Causes crashes when used in clothing."
                    )
                    continue
                if (
                    effect.potency is None
                    or effect.mod1 is None
                    and effect.mod2 is None
                ):
                    click.warn(f"Dropped invalid effect {effect}")
                    continue
                simplepot = effect.potency.name.split("_")[-1][0]
                mod1 = effect.mod1
                mod2 = effect.mod2
                key = (mod1, mod2, simplepot)
                if key in enchantments:
                    if verbose:
                        old_effect_as_str = etree.tostring(
                            enchantments[key].toXML(), encoding="unicode"
                        )
                        click.warn(
                            f"Overwriting {old_effect_as_str} with {effect_as_str}"
                        )
                enchantments[key] = effect

            pot: str
            m2col: Dict[ETFModifier, Dict[str, ItemEffect]]
            pots: Dict[str, ItemEffect]
            newfx = []
            for key, effect in enchantments.items():
                effect_as_str = etree.tostring(effect.toXML(), encoding="unicode")
                if (
                    effect.mod2 not in EXCLUDE_FROM_BOOST_TO_ZERO_CHECK
                    and effect.limit == 0
                    and effect.potency
                    in (
                        ETFPotency.BOOST,
                        ETFPotency.MINOR_BOOST,
                        ETFPotency.MAJOR_BOOST,
                    )
                ):
                    click.warn(
                        f"Dropped nonsensical effect {effect_as_str} - Effect attempts to boost to zero."
                    )
                    continue
                newfx.append(effect)
            self.effects = newfx

    def ensure(self) -> Enchanter:
        return self

    def anus(self) -> AssEnchantmentFactory:
        return self._ass

    def arms(self) -> ArmsEnchantmentFactory:
        return self._arms

    def ass(self) -> AssEnchantmentFactory:
        return self._ass

    def core(self) -> CoreEnchantmentFactory:
        return self._core

    def penis(self) -> PenisEnchantmentFactory:
        return self._penis

    def vagina(self) -> VaginaEnchantmentFactory:
        return self._vagina

    def vaginalUrethra(self) -> VaginalUrethraEnchantmentFactory:
        return self._vaginalUrethra

    def breasts(self) -> BreastEnchanterFactory:
        return self._breasts

    def crotchBreasts(self) -> CrotchBreastEnchanterFactory:
        return self._crotchBreasts

    def face(self) -> FaceEnchantmentFactory:
        return self._face

    def special(self) -> SpecialEnchantmentFactory:
        return self._special

    def ears(self) -> EarsEnchantmentFactory:
        return self._ears

    def eyes(self) -> EyesEnchantmentFactory:
        return self._eyes

    def horns(self) -> HornsEnchantmentFactory:
        return self._horns

    def legs(self) -> LegsEnchantmentFactory:
        return self._legs

    def tail(self) -> TailEnchantmentFactory:
        return self._tail

    def _effectCanRepeat(self, e: ItemEffect) -> bool:
        if (knownEffect := self.getKnownEffect(e)) is not None:
            if (knownEffect.flags & EEffectFlags.NO_REPEAT) == EEffectFlags.NO_REPEAT:
                return False
        return True

    def _effectCannotRepeat(self, e: ItemEffect) -> bool:
        return not self._effectCanRepeat(e)

    def repeatUntilMax(self) -> None:
        if len(self.effects) == 0:
            return
        i = 0
        szOriginal = len(self.effects)
        repeating = list(filter(self._effectCanRepeat, self.effects.copy()))
        if len(repeating) == 0:
            return
        nonRepeating = list(filter(self._effectCannotRepeat, self.effects.copy()))
        nPerRepeat = len(repeating)
        self.effects.clear()
        self.effects += nonRepeating
        while nPerRepeat + len(self.effects) < 100:
            self.effects += repeating
            i += 1
        click.success(
            f"Repeated {nPerRepeat}/{szOriginal} effects of recipe {i} times."
        )

    def repeatTimes(self, count: int) -> None:
        szOriginal = len(self.effects)
        repeating = list(filter(self._effectCanRepeat, self.effects.copy()))
        if len(repeating) == 0:
            return
        nonRepeating = list(filter(self._effectCannotRepeat, self.effects.copy()))
        nPerRepeat = len(repeating)
        self.effects.clear()
        self.effects += nonRepeating
        for _ in range(count):
            self.effects += repeating
        click.success(
            f"Repeated {nPerRepeat}/{szOriginal} effects of recipe {count} times."
        )

from __future__ import annotations

from lilitools.game.effects.enchanter.mixins.body_hair_mixin import BodyHairMixins
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.enums.tf_modifier import ETFModifier

__all__ = ['ArmsEnchantmentFactory']


class ArmsEnchantmentFactory(BodyHairMixins):
    BODYHAIR_PRIMARY_MOD = ETFModifier.TF_ARMS
    BODYHAIR_BODY_ATTR = 'arms'
    BODYHAIR_BODY_SUBATTR = 'underarmHair'

    def the_same_arms_as(self, char: GameCharacter) -> ArmsEnchantmentFactory:
        self.the_same_hair_as(char)
        return self

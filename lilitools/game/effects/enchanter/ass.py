from __future__ import annotations

from lilitools.game.effects.enchanter.mixins.body_hair_mixin import BodyHairMixins
from lilitools.game.effects.enchanter.mixins.orifice import OrificeEnchantmentFactory
from lilitools.saves.character.body.enums.ass_size import EAssSize
from lilitools.saves.character.body.enums.hip_size import EHipSize
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.enums.tf_modifier import ETFModifier

__all__ = ['AssEnchantmentFactory']


class AssEnchantmentFactory(OrificeEnchantmentFactory, BodyHairMixins):
    ORIFICE_PRIMARY_MOD: ETFModifier = ETFModifier.TF_ASS
    ORIFICE_BODY_ATTR: str = 'anus'
    ORIFICE_ORIFICE_ATTR: str = 'orifice'

    BODYHAIR_PRIMARY_MOD: ETFModifier = ETFModifier.TF_ASS
    BODYHAIR_BODY_ATTR: str = 'anus'
    BODYHAIR_BODY_SUBATTR: str = 'assHair'

    ###################
    # Bun Size
    ###################
    def bun_size(self, limit: int, boost_only: bool = False, drain_only: bool = False) -> AssEnchantmentFactory:
        self.enchanter.addClampMods(ETFModifier.TF_ASS, ETFModifier.TF_MOD_SIZE, limit, boost_only=boost_only, drain_only=drain_only)
        return self

    def flat_buns(self) -> AssEnchantmentFactory:
        return self.bun_size(EAssSize.ZERO_FLAT.value)

    def tiny_buns(self) -> AssEnchantmentFactory:
        return self.bun_size(EAssSize.ONE_TINY.value)

    def small_buns(self) -> AssEnchantmentFactory:
        return self.bun_size(EAssSize.TWO_SMALL.value)

    def normal_buns(self) -> AssEnchantmentFactory:
        return self.bun_size(EAssSize.THREE_NORMAL.value)

    def large_buns(self) -> AssEnchantmentFactory:
        return self.bun_size(EAssSize.FOUR_LARGE.value)

    def huge_buns(self) -> AssEnchantmentFactory:
        return self.bun_size(EAssSize.FIVE_HUGE.value)

    def massive_buns(self) -> AssEnchantmentFactory:
        return self.bun_size(EAssSize.SIX_MASSIVE.value)

    def gigantic_buns(self) -> AssEnchantmentFactory:
        return self.bun_size(EAssSize.SEVEN_GIGANTIC.value)

    def the_same_buns_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> AssEnchantmentFactory:
        return self.bun_size(char.body.ass.assSize, boost_only=boost_only, drain_only=drain_only)

    ###################
    # Hip Size
    ###################
    def hip_size(self, limit: int, boost_only: bool = False, drain_only: bool = False) -> AssEnchantmentFactory:
        self.enchanter.addClampMods(ETFModifier.TF_ASS, ETFModifier.TF_MOD_SIZE_SECONDARY, limit, boost_only=boost_only, drain_only=drain_only)
        return self

    def no_hips(self) -> AssEnchantmentFactory:
        return self.hip_size(EHipSize.ZERO_NO_HIPS.value)

    def very_narrow_hips(self) -> AssEnchantmentFactory:
        return self.hip_size(EHipSize.ONE_VERY_NARROW.value)

    def narrow_hips(self) -> AssEnchantmentFactory:
        return self.hip_size(EHipSize.TWO_NARROW.value)

    def girly_hips(self) -> AssEnchantmentFactory:
        return self.hip_size(EHipSize.THREE_GIRLY.value)

    def womanly_hips(self) -> AssEnchantmentFactory:
        return self.hip_size(EHipSize.FOUR_WOMANLY.value)

    def very_wide_hips(self) -> AssEnchantmentFactory:
        return self.hip_size(EHipSize.FIVE_VERY_WIDE.value)

    def extremely_wide_hips(self) -> AssEnchantmentFactory:
        return self.hip_size(EHipSize.SIX_EXTREMELY_WIDE.value)

    def absurdly_wide_hips(self) -> AssEnchantmentFactory:
        return self.hip_size(EHipSize.SEVEN_ABSURDLY_WIDE.value)

    def the_same_hips_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> AssEnchantmentFactory:
        return self.hip_size(char.body.ass.hipSize, boost_only=boost_only, drain_only=drain_only)

    def the_same_ass_as(self, char: GameCharacter) -> AssEnchantmentFactory:
        self.the_same_orifice_as(char)  # anus
        self.the_same_buns_as(char)
        self.the_same_hair_as(char)
        self.the_same_hips_as(char)
        return self


from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from lilitools.game.effects.enchanter import Enchanter
    
__all__ = ['BaseEnchantmentFactory']


class BaseEnchantmentFactory:
    def __init__(self, enchanter: 'Enchanter') -> None:
        self.enchanter: 'Enchanter' = enchanter

    # ass().has().flat_buns().and_().is_().dry().and_().the_same_as(char.body.ass)
    def has(self) -> BaseEnchantmentFactory:
        return self

    def is_(self) -> BaseEnchantmentFactory:
        return self

    def and_(self) -> BaseEnchantmentFactory:
        return self

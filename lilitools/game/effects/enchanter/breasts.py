from __future__ import annotations

from typing import Union

from lilitools.game.effects.enchanter.mixins.orifice import OrificeEnchantmentFactory
from lilitools.saves.character.body.enums.cupsize import ECupSize
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.enums.tf_modifier import ETFModifier

__all__ = ['BreastEnchanterFactory']


class BreastEnchanterFactory(OrificeEnchantmentFactory):
    ORIFICE_PRIMARY_MOD: ETFModifier = ETFModifier.TF_BREASTS
    ORIFICE_BODY_ATTR: str = 'nipples'
    ORIFICE_ORIFICE_ATTR: str = 'orifice'

    #######################
    # Cup Size
    #######################
    # TF_MOD_SIZE
    def of_cup_size(self, limit: Union[int, ECupSize], boost_only: bool = False, drain_only: bool = False) -> BreastEnchanterFactory:
        if isinstance(limit, ECupSize):
            limit = limit.getMeasurement()
        self.enchanter.addClampMods(ETFModifier.TF_BREASTS, ETFModifier.TF_MOD_SIZE, limit, boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_cup_size_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> BreastEnchanterFactory:
        self.of_cup_size(char.body.breasts.size, boost_only=boost_only, drain_only=drain_only)
        return self

    #######################
    # Nipple Size
    #######################
    # TF_MOD_SIZE_SECONDARY
    def of_nipple_size(self, limit: int, boost_only: bool = False, drain_only: bool = False) -> BreastEnchanterFactory:
        self.enchanter.addClampMods(ETFModifier.TF_BREASTS, ETFModifier.TF_MOD_SIZE_SECONDARY, limit, boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_nipple_size_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> BreastEnchanterFactory:
        self.of_nipple_size(char.body.nipples.nippleSize, boost_only=boost_only, drain_only=drain_only)
        return self

    #######################
    # Areolae Size
    #######################
    # TF_MOD_SIZE_TERTIARY
    def of_areolae_size(self, limit: int, boost_only: bool = False, drain_only: bool = False) -> BreastEnchanterFactory:
        self.enchanter.addClampMods(ETFModifier.TF_BREASTS, ETFModifier.TF_MOD_SIZE_TERTIARY, limit, boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_areolae_size_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> BreastEnchanterFactory:
        self.of_areolae_size(char.body.nipples.areolaeSize, boost_only=boost_only, drain_only=drain_only)
        return self

    #######################
    # Lactation
    #######################
    # TF_MOD_WETNESS
    def of_lactation_amount(self, limit: int, boost_only: bool = False, drain_only: bool = False) -> BreastEnchanterFactory:
        self.enchanter.addClampMods(ETFModifier.TF_BREASTS, ETFModifier.TF_MOD_WETNESS, limit, boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_lactation_amount_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> BreastEnchanterFactory:
        self.of_lactation_amount(char.body.breasts.milkStorage, boost_only=boost_only, drain_only=drain_only)
        return self

    #######################
    # Regeneration
    #######################
    # TF_MOD_REGENERATION
    def of_regeneration_rate(self, limit: int, boost_only: bool = False, drain_only: bool = False) -> BreastEnchanterFactory:
        self.enchanter.addClampMods(ETFModifier.TF_BREASTS, ETFModifier.TF_MOD_REGENERATION, limit, boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_regeneration_rate_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> BreastEnchanterFactory:
        self.of_regeneration_rate(char.body.breasts.milkRegeneration, boost_only=boost_only, drain_only=drain_only)
        return self

    #######################
    # Same
    #######################
    def the_same_breasts_as(self, char: GameCharacter) -> BreastEnchanterFactory:
        self.the_same_orifice_as(char)
        self.the_same_areolae_size_as(char)
        self.the_same_cup_size_as(char)
        self.the_same_lactation_amount_as(char)
        self.the_same_regeneration_rate_as(char)
        return self

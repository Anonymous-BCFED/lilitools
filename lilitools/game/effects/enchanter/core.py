from __future__ import annotations

from lilitools.game.effects.enchanter.mixins.body_hair_mixin import BodyHairMixins
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.character.height import EHeight
from lilitools.saves.enums.tf_modifier import ETFModifier

__all__ = ['CoreEnchantmentFactory']


class CoreEnchantmentFactory(BodyHairMixins):
    BODYHAIR_PRIMARY_MOD = ETFModifier.TF_CORE
    BODYHAIR_BODY_ATTR = 'core'
    BODYHAIR_BODY_SUBATTR = 'pubicHair'

    ######################
    # Height
    ######################
    def of_height(self, height_in_cm: int, boost_only: bool = False, drain_only: bool = False) -> CoreEnchantmentFactory:
        '''
        in cm
        max: 244
        '''
        self.enchanter.addClampMods(ETFModifier.TF_CORE, ETFModifier.TF_MOD_SIZE, EHeight.height2enchantmentLimit(height_in_cm), boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_height_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> CoreEnchantmentFactory:
        return self.of_height(char.body.core.height, boost_only=boost_only, drain_only=drain_only)

    ######################
    # Muscles
    ######################
    def of_muscle_size(self, limit: int, boost_only: bool = False, drain_only: bool = False) -> CoreEnchantmentFactory:
        '''
        min: 0 - no muscles
        max: 100 - meat beast
        '''
        self.enchanter.addClampMods(ETFModifier.TF_CORE, ETFModifier.TF_MOD_SIZE_SECONDARY, limit, boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_muscle_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> CoreEnchantmentFactory:
        return self.of_muscle_size(char.body.core.muscle, boost_only=boost_only, drain_only=drain_only)

    ######################
    # Build
    ######################
    def of_body_size(self, limit: int, boost_only: bool = False, drain_only: bool = False) -> CoreEnchantmentFactory:
        '''
        min: 0 - emaciated
        max: 100 - fat as fuck
        '''
        self.enchanter.addClampMods(ETFModifier.TF_CORE, ETFModifier.TF_MOD_SIZE_TERTIARY, limit, boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_body_size_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> CoreEnchantmentFactory:
        return self.of_body_size(char.body.core.bodySize, boost_only=boost_only, drain_only=drain_only)

    ######################
    # Femininity
    ######################
    def of_femininity(self, limit: int, boost_only: bool = False, drain_only: bool = False) -> CoreEnchantmentFactory:
        '''
        min: 0 - gigachad
        max: 100 - bimbo
        '''
        self.enchanter.addClampMods(ETFModifier.TF_CORE, ETFModifier.TF_MOD_FEMININITY, limit, boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_femininity_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> CoreEnchantmentFactory:
        return self.of_femininity(char.body.core.femininity, boost_only=boost_only, drain_only=drain_only)

    #####################
    # Copier
    #####################
    def the_same_core_as(self, char: GameCharacter) -> CoreEnchantmentFactory:
        self.the_same_hair_as(char)
        self.the_same_body_size_as(char)
        self.the_same_femininity_as(char)
        self.the_same_height_as(char)
        self.the_same_muscle_as(char)
        return self

from __future__ import annotations

from typing import Union

from lilitools.game.effects.enchanter.mixins.orifice import OrificeEnchantmentFactory
# from lilitools.game.effects.enchanter.mixins.removable import RemovableMixins
from lilitools.saves.character.body.enums.cupsize import ECupSize
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.enums.tf_modifier import ETFModifier

__all__ = ["CrotchBreastEnchanterFactory"]


# As of 10.12.2024, no longer removable.
class CrotchBreastEnchanterFactory(OrificeEnchantmentFactory):
    ORIFICE_PRIMARY_MOD: ETFModifier = ETFModifier.TF_BREASTS_CROTCH
    ORIFICE_BODY_ATTR: str = "crotchNipples"
    ORIFICE_ORIFICE_ATTR: str = "orifice"

    REMOVABLE_PRIMARY_MOD = ETFModifier.TF_BREASTS_CROTCH

    #######################
    # Cup Size
    #######################
    # TF_MOD_SIZE
    def of_cup_size(
        self,
        limit: Union[int, ECupSize],
        boost_only: bool = False,
        drain_only: bool = False,
    ) -> CrotchBreastEnchanterFactory:
        if isinstance(limit, ECupSize):
            limit = limit.getMeasurement()
        self.enchanter.addClampMods(
            ETFModifier.TF_BREASTS_CROTCH,
            ETFModifier.TF_MOD_SIZE,
            limit,
            boost_only=boost_only,
            drain_only=drain_only,
        )
        return self

    def the_same_cup_size_as(
        self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False
    ) -> CrotchBreastEnchanterFactory:
        self.of_cup_size(
            char.body.crotchBreasts.size, boost_only=boost_only, drain_only=drain_only
        )
        return self

    #######################
    # Nipple Size
    #######################
    # TF_MOD_SIZE_SECONDARY
    def of_nipple_size(
        self, limit: int, boost_only: bool = False, drain_only: bool = False
    ) -> CrotchBreastEnchanterFactory:
        self.enchanter.addClampMods(
            ETFModifier.TF_BREASTS_CROTCH,
            ETFModifier.TF_MOD_SIZE_SECONDARY,
            limit,
            boost_only=boost_only,
            drain_only=drain_only,
        )
        return self

    def the_same_nipple_size_as(
        self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False
    ) -> CrotchBreastEnchanterFactory:
        self.of_nipple_size(
            char.body.crotchNipples.nippleSize,
            boost_only=boost_only,
            drain_only=drain_only,
        )
        return self

    #######################
    # Areolae Size
    #######################
    # TF_MOD_SIZE_TERTIARY
    def of_areolae_size(
        self, limit: int, boost_only: bool = False, drain_only: bool = False
    ) -> CrotchBreastEnchanterFactory:
        self.enchanter.addClampMods(
            ETFModifier.TF_BREASTS_CROTCH,
            ETFModifier.TF_MOD_SIZE_TERTIARY,
            limit,
            boost_only=boost_only,
            drain_only=drain_only,
        )
        return self

    def the_same_areolae_size_as(
        self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False
    ) -> CrotchBreastEnchanterFactory:
        self.of_areolae_size(
            char.body.crotchNipples.areolaeSize,
            boost_only=boost_only,
            drain_only=drain_only,
        )
        return self

    #######################
    # Lactation
    #######################
    # TF_MOD_WETNESS
    def of_lactation_amount(
        self, limit: int, boost_only: bool = False, drain_only: bool = False
    ) -> CrotchBreastEnchanterFactory:
        self.enchanter.addClampMods(
            ETFModifier.TF_BREASTS_CROTCH,
            ETFModifier.TF_MOD_WETNESS,
            limit,
            boost_only=boost_only,
            drain_only=drain_only,
        )
        return self

    def the_same_lactation_amount_as(
        self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False
    ) -> CrotchBreastEnchanterFactory:
        self.of_lactation_amount(
            char.body.crotchBreasts.milkStorage,
            boost_only=boost_only,
            drain_only=drain_only,
        )
        return self

    #######################
    # Regeneration
    #######################
    # TF_MOD_REGENERATION
    def of_regeneration_rate(
        self, limit: int, boost_only: bool = False, drain_only: bool = False
    ) -> CrotchBreastEnchanterFactory:
        self.enchanter.addClampMods(
            ETFModifier.TF_BREASTS_CROTCH,
            ETFModifier.TF_MOD_REGENERATION,
            limit,
            boost_only=boost_only,
            drain_only=drain_only,
        )
        return self

    def the_same_regeneration_rate_as(
        self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False
    ) -> CrotchBreastEnchanterFactory:
        self.of_regeneration_rate(
            char.body.crotchBreasts.milkRegeneration,
            boost_only=boost_only,
            drain_only=drain_only,
        )
        return self

    #######################
    # Same
    #######################
    def the_same_crotch_breasts_as(
        self, char: GameCharacter
    ) -> CrotchBreastEnchanterFactory:
        if char.body.crotchBreasts is None:
            # self.remove()
            pass
        else:
            # self.grow()
            self.the_same_orifice_as(char)
            self.the_same_areolae_size_as(char)
            self.the_same_cup_size_as(char)
            self.the_same_lactation_amount_as(char)
            self.the_same_regeneration_rate_as(char)
        return self

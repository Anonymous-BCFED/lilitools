from __future__ import annotations
from lilitools.game.effects.enchanter.base import BaseEnchantmentFactory
from lilitools.game.effects.enchanter.mixins.removable import RemovableMixins
from lilitools.saves.enums.tf_modifier import ETFModifier

from lilitools.saves.character.game_character import GameCharacter

__all__ = ['EarsEnchantmentFactory']
class EarsEnchantmentFactory(BaseEnchantmentFactory):
    #####################
    # Copier
    #####################

    def the_same_ears_as(self, char: GameCharacter) -> EarsEnchantmentFactory:
        # Nothing available for clothing
        return self
    
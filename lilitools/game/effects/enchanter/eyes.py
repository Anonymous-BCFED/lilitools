from __future__ import annotations
from typing import Dict

from lilitools.game.effects.enchanter.base import BaseEnchantmentFactory
from lilitools.game.effects.enchanter.mixins.removable import RemovableMixins
from lilitools.saves.enums.tf_modifier import ETFModifier
from lilitools.saves.enums.tf_potency import ETFPotency

from lilitools.saves.character.game_character import GameCharacter

from lilitools.saves.character.body.enums.eye_shape import EEyeShape

__all__ = ['EyesEnchantmentFactory']

EYE_SHAPE_TO_TFMOD_IRIS: Dict[EEyeShape, ETFModifier] = {
    EEyeShape.HEART: ETFModifier.TF_MOD_EYE_IRIS_CIRCLE,
    EEyeShape.HORIZONTAL: ETFModifier.TF_MOD_EYE_IRIS_HORIZONTAL,
    EEyeShape.ROUND: ETFModifier.TF_MOD_EYE_IRIS_CIRCLE,
    EEyeShape.STAR: ETFModifier.TF_MOD_EYE_IRIS_STAR,
    EEyeShape.VERTICAL: ETFModifier.TF_MOD_EYE_IRIS_VERTICAL,
}
TFMOD_IRIS_TO_EYE_SHAPE: Dict[ETFModifier, EEyeShape] = {
    ETFModifier.TF_MOD_EYE_IRIS_HEART: EEyeShape.HEART,
    ETFModifier.TF_MOD_EYE_IRIS_HORIZONTAL: EEyeShape.HORIZONTAL,
    ETFModifier.TF_MOD_EYE_IRIS_CIRCLE: EEyeShape.ROUND,
    ETFModifier.TF_MOD_EYE_IRIS_STAR: EEyeShape.STAR,
    ETFModifier.TF_MOD_EYE_IRIS_VERTICAL: EEyeShape.VERTICAL,
}

EYE_SHAPE_TO_TFMOD_PUPIL: Dict[EEyeShape, ETFModifier] = {
    EEyeShape.HEART: ETFModifier.TF_MOD_EYE_PUPIL_CIRCLE,
    EEyeShape.HORIZONTAL: ETFModifier.TF_MOD_EYE_PUPIL_HORIZONTAL,
    EEyeShape.ROUND: ETFModifier.TF_MOD_EYE_PUPIL_CIRCLE,
    EEyeShape.STAR: ETFModifier.TF_MOD_EYE_PUPIL_STAR,
    EEyeShape.VERTICAL: ETFModifier.TF_MOD_EYE_PUPIL_VERTICAL,
}
TFMOD_PUPIL_TO_EYE_SHAPE: Dict[ETFModifier, EEyeShape] = {
    ETFModifier.TF_MOD_EYE_PUPIL_HEART: EEyeShape.HEART,
    ETFModifier.TF_MOD_EYE_PUPIL_HORIZONTAL: EEyeShape.HORIZONTAL,
    ETFModifier.TF_MOD_EYE_PUPIL_CIRCLE: EEyeShape.ROUND,
    ETFModifier.TF_MOD_EYE_PUPIL_STAR: EEyeShape.STAR,
    ETFModifier.TF_MOD_EYE_PUPIL_VERTICAL: EEyeShape.VERTICAL,
}


class EyesEnchantmentFactory(BaseEnchantmentFactory):

    #############
    # TYPE
    #############
    # def of_type(self, typeid: int) -> EyesEnchantmentFactory:
    #     self.enchanter.addClothingMod(ETFModifier.TF_EYES, ETFModifier[f'TF_MOD_{typeid}'], ETFPotency.MAJOR_BOOST, 1)
    #     return self
    # def the_same_type_as(self, char:GameCharacter) -> EyesEnchantmentFactory:
    #     self.of_type(char.body.eyes.type)
    #     return self

    #############
    # COUNT
    #############
    def of_count(self, limit: int, boost_only: bool = False, drain_only: bool = False) -> EyesEnchantmentFactory:
        self.enchanter.addClampMods(ETFModifier.TF_EYES, ETFModifier.TF_MOD_COUNT, limit, boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_count_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> EyesEnchantmentFactory:
        self.of_count(char.body.eyes.eyePairs, boost_only=boost_only, drain_only=drain_only)
        return self

    #############
    # IRIS SHAPE
    #############
    def of_iris_shape(self, shape: EEyeShape) -> EyesEnchantmentFactory:
        self.enchanter.addClothingMod(ETFModifier.TF_EYES, EYE_SHAPE_TO_TFMOD_IRIS[shape], ETFPotency.MINOR_BOOST, limit=0)
        return self

    def the_same_iris_as(self, char: GameCharacter) -> EyesEnchantmentFactory:
        self.of_iris_shape(char.body.eyes.irisShape)
        return self

    #############
    # IRIS SHAPE
    #############
    def of_pupil_shape(self, shape: EEyeShape) -> EyesEnchantmentFactory:
        self.enchanter.addClothingMod(ETFModifier.TF_EYES, EYE_SHAPE_TO_TFMOD_IRIS[shape], ETFPotency.MINOR_BOOST, limit=0)
        return self

    def the_same_pupil_as(self, char: GameCharacter) -> EyesEnchantmentFactory:
        self.of_pupil_shape(char.body.eyes.pupilShape)
        return self

    #############
    # Copy
    #############
    def the_same_eyes_as(self, char: GameCharacter) -> None:
        self.the_same_count_as(char)
        self.the_same_iris_as(char)
        self.the_same_pupil_as(char)
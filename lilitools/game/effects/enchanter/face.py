from __future__ import annotations

from typing import Union

from lilitools.game.effects.enchanter.mixins.body_hair_mixin import BodyHairMixins
from lilitools.game.effects.enchanter.mixins.orifice import OrificeEnchantmentFactory
from lilitools.game.effects.enchanter.mixins.tongue_penetrator import TonguePenetratorEnchantmentFactory
from lilitools.saves.character.body.enums.lipsize import ELipSize
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.enums.tf_modifier import ETFModifier
from lilitools.utils import clampInt

__all__ = ['FaceEnchantmentFactory']


class FaceEnchantmentFactory(OrificeEnchantmentFactory, TonguePenetratorEnchantmentFactory, BodyHairMixins):
    # ORIFICE_PRIMARY_MOD = ETFModifier.TF_FACE
    # ORIFICE_BODY_ATTR: str = 'face'
    # ORIFICE_ORIFICE_ATTR: str = 'mouth'

    BODYHAIR_PRIMARY_MOD = ETFModifier.TF_FACE
    BODYHAIR_BODY_ATTR = 'face'
    BODYHAIR_BODY_SUBATTR = 'facialHair'

    PENETRATOR_PRIMARY_MOD = ETFModifier.TF_FACE
    PENETRATOR_BODY_ATTR = 'face'
    PENETRATOR_PENETRATOR_ATTR = 'tongue'

    ######################
    # Lip Size
    ######################
    def of_lip_size(self, lipSize: Union[int, ELipSize], boost_only: bool = False, drain_only: bool = False) -> FaceEnchantmentFactory:
        if isinstance(lipSize, ELipSize):
            lipSize = lipSize.getValue_()
        self.enchanter.addClampMods(ETFModifier.TF_FACE, ETFModifier.TF_MOD_SIZE, clampInt(lipSize, 0, 3), boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_lip_size_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> FaceEnchantmentFactory:
        return self.of_lip_size(char.body.mouth.lipSize, boost_only=boost_only, drain_only=drain_only)

    #####################
    # Copier
    #####################

    def the_same_face_as(self, char: GameCharacter) -> FaceEnchantmentFactory:
        self.the_same_hair_as(char)
        self.the_same_lip_size_as(char)
        #self.the_same_orifice_as(char)
        #self.the_same_penetrator_as(char)
        return self

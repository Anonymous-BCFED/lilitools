from abc import ABC, abstractmethod

from lilitools.saves.items.abstract_clothing import AbstractClothing
from lilitools.saves.items.abstract_item import AbstractItem
from lilitools.saves.items.item_effect import ItemEffect


class BaseEnchantmentFixup(ABC):
    def __init__(self) -> None:
        pass

    @abstractmethod
    def canRunOnClothing(self, clothing: AbstractClothing) -> bool: ...

    @abstractmethod
    def checkClothingEffect(self, clothing: AbstractClothing, effect: ItemEffect) -> bool: ...

    @abstractmethod
    def canRunOnItem(self, item: AbstractItem) -> bool: ...

    @abstractmethod
    def checkItemEffect(self, item: AbstractItem, effect: ItemEffect) -> bool: ...

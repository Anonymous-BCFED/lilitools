from __future__ import annotations
from typing import Union
from lilitools.game.effects.enchanter.base import BaseEnchantmentFactory

from lilitools.game.effects.enchanter.mixins.body_hair_mixin import BodyHairMixins
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.character.height import EHeight
from lilitools.saves.enums.tf_modifier import ETFModifier

from lilitools.saves.character.body.enums.hairlength import EHairLength

from lilitools.saves.enums.tf_potency import ETFPotency

__all__ = ['HairEnchantmentFactory']


class HairEnchantmentFactory(BaseEnchantmentFactory):

    ######################
    # Length
    ######################
    def of_length(self, length: Union[int, EHairLength], boost_only: bool = False, drain_only: bool = False) -> HairEnchantmentFactory:
        '''
        in cm
        '''
        if isinstance(length,EHairLength):
            length=length.getMedianValue()
        self.enchanter.addClampMods(ETFModifier.TF_HAIR, ETFModifier.TF_MOD_SIZE, length, boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_length_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> HairEnchantmentFactory:
        return self.of_length(char.body.hair.length, boost_only=boost_only, drain_only=drain_only)

    ######################
    # Length
    ######################
    def has_neck_fluff(self) -> HairEnchantmentFactory:
        self.enchanter.addClothingMod(ETFModifier.TF_HAIR, ETFModifier.TF_MOD_SIZE_SECONDARY, ETFPotency.MAJOR_BOOST, limit=1)
        return self
    def has_no_neck_fluff(self) -> HairEnchantmentFactory:
        self.enchanter.addClothingMod(ETFModifier.TF_HAIR, ETFModifier.TF_MOD_SIZE_SECONDARY, ETFPotency.MAJOR_DRAIN, limit=1)
        return self
    def set_neck_fluff(self, has: bool) -> HairEnchantmentFactory:
        if has:
            self.has_neck_fluff()
        else:
            self.has_no_neck_fluff()
        return self

    def the_same_neck_fluff_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> HairEnchantmentFactory:
        return self.set_neck_fluff(char.body.hair.neckFluff, boost_only=boost_only, drain_only=drain_only)

    #######################
    # Copy
    #######################
    def the_same_hair_as(self, char:GameCharacter) ->HairEnchantmentFactory:
        self.the_same_hair_as(char)
        self.the_same_neck_fluff_as(char)

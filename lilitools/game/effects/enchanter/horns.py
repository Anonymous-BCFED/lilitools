from __future__ import annotations

from lilitools.game.effects.enchanter.mixins.removable import RemovableMixins
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.enums.tf_modifier import ETFModifier

__all__ = ['HornsEnchantmentFactory']


class HornsEnchantmentFactory(RemovableMixins):
    REMOVABLE_PRIMARY_MOD = ETFModifier.TF_HORNS

    ######################
    # Size
    ######################
    def of_size(self, size: int, boost_only: bool = False, drain_only: bool = False) -> HornsEnchantmentFactory:
        self.enchanter.addClampMods(ETFModifier.TF_HORNS, ETFModifier.TF_MOD_SIZE, size, boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_size_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> HornsEnchantmentFactory:
        return self.of_size(char.body.horns.length, boost_only=boost_only, drain_only=drain_only)

    #############
    # ROWS
    #############
    def of_rows(self, limit: int, boost_only: bool = False, drain_only: bool = False) -> HornsEnchantmentFactory:
        self.enchanter.addClampMods(ETFModifier.TF_HORNS, ETFModifier.TF_MOD_COUNT, limit, boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_rows_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> HornsEnchantmentFactory:
        self.of_rows(char.body.horns.rows, boost_only=boost_only, drain_only=drain_only)
        return self

    #############
    # PER ROW
    #############
    def of_horns_per_row(self, limit: int, boost_only: bool = False, drain_only: bool = False) -> HornsEnchantmentFactory:
        self.enchanter.addClampMods(ETFModifier.TF_HORNS, ETFModifier.TF_MOD_COUNT_SECONDARY, limit, boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_horns_per_row_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> HornsEnchantmentFactory:
        self.of_horns_per_row(char.body.horns.hornsPerRow, boost_only=boost_only, drain_only=drain_only)
        return self

    #############
    # COPY
    #############
    def the_same_horns_as(self, char: GameCharacter) -> None:
        self.the_same_size_as(char)
        self.the_same_rows_as(char)
        self.the_same_horns_per_row_as(char)

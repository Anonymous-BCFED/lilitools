from __future__ import annotations

from typing import Dict
from lilitools.game.effects.enchanter.base import BaseEnchantmentFactory
from lilitools.game.effects.enchanter.mixins.orifice import OrificeEnchantmentFactory

from lilitools.game.effects.enchanter.mixins.removable import RemovableMixins
from lilitools.saves.character.body.enums.foot_structure import EFootStructure
from lilitools.saves.character.body.enums.leg_configuration import ELegConfiguration
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.enums.tf_modifier import ETFModifier
from lilitools.saves.enums.tf_potency import ETFPotency

__all__ = ['LegsEnchantmentFactory']

LEGCONFIG2TFMOD: Dict[ELegConfiguration, ETFModifier] = {
    ELegConfiguration.BIPEDAL: ETFModifier.TF_MOD_LEG_CONFIG_BIPEDAL,
    ELegConfiguration.QUADRUPEDAL: ETFModifier.TF_MOD_LEG_CONFIG_TAUR,
    ELegConfiguration.TAIL_LONG: ETFModifier.TF_MOD_LEG_CONFIG_TAIL_LONG,
    ELegConfiguration.TAIL: ETFModifier.TF_MOD_LEG_CONFIG_TAIL,
    ELegConfiguration.ARACHNID: ETFModifier.TF_MOD_LEG_CONFIG_ARACHNID,
    ELegConfiguration.CEPHALOPOD: ETFModifier.TF_MOD_LEG_CONFIG_CEPHALOPOD,
    ELegConfiguration.AVIAN: ETFModifier.TF_MOD_LEG_CONFIG_AVIAN,
    ELegConfiguration.WINGED_BIPED: ETFModifier.TF_MOD_LEG_CONFIG_WINGED_BIPED,
}
TFMOD2LEGCONFIG: Dict[ETFModifier, ELegConfiguration] = {
    ETFModifier.TF_MOD_LEG_CONFIG_BIPEDAL: ELegConfiguration.BIPEDAL,
    ETFModifier.TF_MOD_LEG_CONFIG_TAUR: ELegConfiguration.QUADRUPEDAL,
    ETFModifier.TF_MOD_LEG_CONFIG_TAIL_LONG: ELegConfiguration.TAIL_LONG,
    ETFModifier.TF_MOD_LEG_CONFIG_TAIL: ELegConfiguration.TAIL,
    ETFModifier.TF_MOD_LEG_CONFIG_ARACHNID: ELegConfiguration.ARACHNID,
    ETFModifier.TF_MOD_LEG_CONFIG_CEPHALOPOD: ELegConfiguration.CEPHALOPOD,
    ETFModifier.TF_MOD_LEG_CONFIG_AVIAN: ELegConfiguration.AVIAN,
    ETFModifier.TF_MOD_LEG_CONFIG_WINGED_BIPED: ELegConfiguration.WINGED_BIPED,
}

TFMOD2FOOTSTRUCTURE: Dict[ETFModifier, EFootStructure] = {
    # ETFModifier.TF_MOD_FOOT_STRUCTURE_NONE: EFootStructure.NONE,
    ETFModifier.TF_MOD_FOOT_STRUCTURE_PLANTIGRADE: EFootStructure.PLANTIGRADE,
    ETFModifier.TF_MOD_FOOT_STRUCTURE_DIGITIGRADE: EFootStructure.DIGITIGRADE,
    ETFModifier.TF_MOD_FOOT_STRUCTURE_UNGULIGRADE: EFootStructure.UNGULIGRADE,
    # ETFModifier.TF_MOD_FOOT_STRUCTURE_ARACHNOID: EFootStructure.ARACHNOID,
    # ETFModifier.TF_MOD_FOOT_STRUCTURE_TENTACLED: EFootStructure.TENTACLED,
}
FOOTSTRUCTURE2TFMOD: Dict[EFootStructure, ETFModifier] = {
    # EFootStructure.NONE: ETFModifier.TF_MOD_FOOT_STRUCTURE_NONE,
    EFootStructure.PLANTIGRADE: ETFModifier.TF_MOD_FOOT_STRUCTURE_PLANTIGRADE,
    EFootStructure.DIGITIGRADE: ETFModifier.TF_MOD_FOOT_STRUCTURE_DIGITIGRADE,
    EFootStructure.UNGULIGRADE: ETFModifier.TF_MOD_FOOT_STRUCTURE_UNGULIGRADE,
    # EFootStructure.ARACHNOID: ETFModifier.TF_MOD_FOOT_STRUCTURE_ARACHNOID,
    # EFootStructure.TENTACLED: ETFModifier.TF_MOD_FOOT_STRUCTURE_TENTACLED,
}

class LegsEnchantmentFactory(BaseEnchantmentFactory):
    # ORIFICE_PRIMARY_MOD = ETFModifier.TF_LEGS
    # ORIFICE_BODY_ATTR = 'spinneret'
    # ORIFICE_ORIFICE_ATTR = None

    ######################
    # Configuration
    ######################
    def of_configuration(self, lc: ELegConfiguration) -> LegsEnchantmentFactory:
        self.enchanter.addClothingMod(ETFModifier.TF_LEGS, LEGCONFIG2TFMOD[lc], ETFPotency.MAJOR_BOOST, 1)
        return self

    def the_same_configuration_as(self, char: GameCharacter) -> LegsEnchantmentFactory:
        self.of_configuration(char.body.legs.configuration)
        return self

    ######################
    # Foot Structure
    ######################
    def of_foot_structure(self, fs: EFootStructure) -> LegsEnchantmentFactory:
        self.enchanter.addClothingMod(ETFModifier.TF_LEGS, FOOTSTRUCTURE2TFMOD[fs], ETFPotency.MAJOR_BOOST, 1)
        return self

    def the_same_foot_structure_as(self, char: GameCharacter) -> LegsEnchantmentFactory:
        self.of_foot_structure(char.body.legs.footStructure)
        return self

    ######################
    # Size
    ######################
    def of_tail_length(self, size: int, boost_only: bool = False, drain_only: bool = False) -> LegsEnchantmentFactory:
        self.enchanter.addClampMods(ETFModifier.TF_LEGS, ETFModifier.TF_MOD_SIZE, size, boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_tail_length_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> LegsEnchantmentFactory:
        return self.of_tail_length(int(char.body.legs.tailLength), boost_only=boost_only, drain_only=drain_only)

    #############
    # COPY
    #############

    def the_same_legs_as(self, char: GameCharacter) -> None:
        self.the_same_configuration_as(char)
        self.the_same_foot_structure_as(char)
        self.the_same_tail_length_as(char)
        # self.the_same_orifice_as(char)

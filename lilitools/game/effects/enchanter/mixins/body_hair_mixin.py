
from __future__ import annotations
from typing import Optional

from lilitools.saves.character.enums.body_hair import EBodyHair
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.enums.tf_modifier import ETFModifier

from ..base import BaseEnchantmentFactory


class BodyHairMixins(BaseEnchantmentFactory):
    BODYHAIR_PRIMARY_MOD: ETFModifier
    BODYHAIR_BODY_ATTR: str
    BODYHAIR_BODY_SUBATTR: Optional[str] = None

    #####################
    # Hair
    #####################
    def hair_length(self, limit: int, boost_only: bool = False, drain_only: bool = False) -> BodyHairMixins:
        self.enchanter.addClampMods(self.BODYHAIR_PRIMARY_MOD, ETFModifier.TF_MOD_BODY_HAIR, limit, boost_only=boost_only, drain_only=drain_only)
        return self

    def no_hair(self) -> BodyHairMixins:
        return self.hair_length(EBodyHair.ZERO_NONE.value)

    def stubbly_hair(self) -> BodyHairMixins:
        return self.hair_length(EBodyHair.ONE_STUBBLE.value)

    def manicured_hair(self) -> BodyHairMixins:
        return self.hair_length(EBodyHair.TWO_MANICURED.value)

    def trimmed_hair(self) -> BodyHairMixins:
        return self.hair_length(EBodyHair.THREE_TRIMMED.value)

    def natural_hair(self) -> BodyHairMixins:
        return self.hair_length(EBodyHair.FOUR_NATURAL.value)

    def unkempt_hair(self) -> BodyHairMixins:
        return self.hair_length(EBodyHair.FIVE_UNKEMPT.value)

    def bushy_hair(self) -> BodyHairMixins:
        return self.hair_length(EBodyHair.SIX_BUSHY.value)

    def wild_hair(self) -> BodyHairMixins:
        return self.hair_length(EBodyHair.SEVEN_WILD.value)

    def the_same_hair_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> BodyHairMixins:
        src = getattr(char.body, self.BODYHAIR_BODY_ATTR)
        if self.BODYHAIR_BODY_SUBATTR is not None:
            src = getattr(src, self.BODYHAIR_BODY_SUBATTR)
        return self.hair_length(src.value, boost_only=boost_only, drain_only=drain_only)

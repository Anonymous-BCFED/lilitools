from __future__ import annotations

from typing import Iterable, Optional, Union

from lilitools.saves.character.body.enums.depth import EDepth
from lilitools.saves.character.body.enums.elasticity import EElasticity
from lilitools.saves.character.body.enums.plasticity import EPlasticity
from lilitools.saves.character.body.enums.wetness import EWetness
from lilitools.saves.character.body.orifice import Orifice
from lilitools.saves.character.enums.orifice_modifier import EOrificeModifier
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.enums.tf_modifier import ETFModifier

from ..base import BaseEnchantmentFactory

__all__ = ['OrificeEnchantmentFactory']


class OrificeEnchantmentFactory(BaseEnchantmentFactory):
    ORIFICE_PRIMARY_MOD: ETFModifier
    ORIFICE_BODY_ATTR: str
    ORIFICE_ORIFICE_ATTR: Optional[str]

    ORIFICE_TFMOD1_WETNESS: Optional[ETFModifier] = None
    ORIFICE_TFMOD2_WETNESS: Optional[ETFModifier] = ETFModifier.TF_MOD_WETNESS

    ORIFICE_TFMOD2_DEPTH: ETFModifier = ETFModifier.TF_MOD_DEPTH
    ORIFICE_TFMOD2_ELASTICITY: ETFModifier = ETFModifier.TF_MOD_ELASTICITY
    ORIFICE_TFMOD2_PLASTICITY: ETFModifier = ETFModifier.TF_MOD_PLASTICITY
    ORIFICE_TFMOD2_CAPACITY: ETFModifier = ETFModifier.TF_MOD_CAPACITY

    ORIFICE_TFMOD2_MUSCLED: ETFModifier = ETFModifier.TF_MOD_ORIFICE_MUSCLED
    ORIFICE_TFMOD2_PUFFY: ETFModifier = ETFModifier.TF_MOD_ORIFICE_PUFFY
    ORIFICE_TFMOD2_RIBBED: ETFModifier = ETFModifier.TF_MOD_ORIFICE_RIBBED
    ORIFICE_TFMOD2_TENTACLED: ETFModifier = ETFModifier.TF_MOD_ORIFICE_TENTACLED

    def _getOrifice(self, char: GameCharacter) -> Orifice:
        base = getattr(char.body, self.ORIFICE_BODY_ATTR)
        if self.ORIFICE_ORIFICE_ATTR:
            return getattr(base, self.ORIFICE_ORIFICE_ATTR)
        return base

    ####################
    # Wetness
    ####################
    if ORIFICE_TFMOD2_WETNESS is not None:
        def wetness_of(self, value: Union[int, EWetness], boost_only: bool = False, drain_only: bool = False) -> OrificeEnchantmentFactory:
            if isinstance(value, EWetness):
                value = value.value
            if self.ORIFICE_TFMOD2_WETNESS is not None:
                self.enchanter.addClampMods(self.ORIFICE_TFMOD1_WETNESS or self.ORIFICE_PRIMARY_MOD, self.ORIFICE_TFMOD2_WETNESS, value, boost_only=boost_only, drain_only=drain_only)
            return self

        def dry(self) -> OrificeEnchantmentFactory:
            return self.wetness_of(EWetness.ZERO_DRY)

        def slightly_moist(self) -> OrificeEnchantmentFactory:
            return self.wetness_of(EWetness.ONE_SLIGHTLY_MOIST)

        def moist(self) -> OrificeEnchantmentFactory:
            return self.wetness_of(EWetness.TWO_MOIST)

        def wet(self) -> OrificeEnchantmentFactory:
            return self.wetness_of(EWetness.THREE_WET)

        def slimy(self) -> OrificeEnchantmentFactory:
            return self.wetness_of(EWetness.FOUR_SLIMY)

        def sloppy(self) -> OrificeEnchantmentFactory:
            return self.wetness_of(EWetness.FIVE_SLOPPY)

        def sopping_wet(self) -> OrificeEnchantmentFactory:
            return self.wetness_of(EWetness.SIX_SOPPING_WET)

        def drooling(self) -> OrificeEnchantmentFactory:
            return self.wetness_of(EWetness.SEVEN_DROOLING)

        def the_same_wetness_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> OrificeEnchantmentFactory:
            return self.wetness_of(self._getOrifice(char).wetness, boost_only=boost_only, drain_only=drain_only)

    ####################
    # Depth
    ####################
    def depth_of(self, value: Union[int, EDepth], boost_only: bool = False, drain_only: bool = False) -> OrificeEnchantmentFactory:
        if isinstance(value, EDepth):
            value = value.value
        self.enchanter.addClampMods(self.ORIFICE_PRIMARY_MOD, self.ORIFICE_TFMOD2_DEPTH, value, boost_only=boost_only, drain_only=drain_only)
        return self

    def extremely_shallow(self) -> OrificeEnchantmentFactory:
        return self.depth_of(EDepth.ZERO_EXTREMELY_SHALLOW.value)

    def shallow(self) -> OrificeEnchantmentFactory:
        return self.depth_of(EDepth.ONE_SHALLOW.value)

    def average(self) -> OrificeEnchantmentFactory:
        return self.depth_of(EDepth.TWO_AVERAGE.value)

    def spacious(self) -> OrificeEnchantmentFactory:
        return self.depth_of(EDepth.THREE_SPACIOUS.value)

    def deep(self) -> OrificeEnchantmentFactory:
        return self.depth_of(EDepth.FOUR_DEEP.value)

    def very_deep(self) -> OrificeEnchantmentFactory:
        return self.depth_of(EDepth.FIVE_VERY_DEEP.value)

    def cavernous(self) -> OrificeEnchantmentFactory:
        return self.depth_of(EDepth.SIX_CAVERNOUS.value)

    def fathomless(self) -> OrificeEnchantmentFactory:
        return self.depth_of(EDepth.SEVEN_FATHOMLESS.value)

    def the_same_depth_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> OrificeEnchantmentFactory:
        return self.depth_of(self._getOrifice(char).depth, boost_only=boost_only, drain_only=drain_only)

    ####################
    # Elasticity
    ####################
    def elasticity_of(self, value: Union[int, EElasticity], boost_only: bool = False, drain_only: bool = False) -> OrificeEnchantmentFactory:
        if isinstance(value, EElasticity):
            value = value.value
        self.enchanter.addClampMods(self.ORIFICE_PRIMARY_MOD, self.ORIFICE_TFMOD2_ELASTICITY, value, boost_only=boost_only, drain_only=drain_only)
        return self

    def unyielding(self) -> OrificeEnchantmentFactory:
        return self.elasticity_of(EElasticity.ZERO_UNYIELDING.value)

    def rigid(self) -> OrificeEnchantmentFactory:
        return self.elasticity_of(EElasticity.ONE_RIGID.value)

    def firm(self) -> OrificeEnchantmentFactory:
        return self.elasticity_of(EElasticity.TWO_FIRM.value)

    def flexible(self) -> OrificeEnchantmentFactory:
        return self.elasticity_of(EElasticity.THREE_FLEXIBLE.value)

    def limber(self) -> OrificeEnchantmentFactory:
        return self.elasticity_of(EElasticity.FOUR_LIMBER.value)

    def stretchy(self) -> OrificeEnchantmentFactory:
        return self.elasticity_of(EElasticity.FIVE_STRETCHY.value)

    def supple(self) -> OrificeEnchantmentFactory:
        return self.elasticity_of(EElasticity.SIX_SUPPLE.value)

    def elastic(self) -> OrificeEnchantmentFactory:
        return self.elasticity_of(EElasticity.SEVEN_ELASTIC.value)

    def the_same_elasticity_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> OrificeEnchantmentFactory:
        return self.elasticity_of(self._getOrifice(char).elasticity, boost_only=boost_only, drain_only=drain_only)

    ####################
    # Plasticity
    ####################
    def plasticity_of(self, value: Union[int, EPlasticity], boost_only: bool = False, drain_only: bool = False) -> OrificeEnchantmentFactory:
        if isinstance(value, EPlasticity):
            value = value.value
        self.enchanter.addClampMods(self.ORIFICE_PRIMARY_MOD, self.ORIFICE_TFMOD2_PLASTICITY, value, boost_only=boost_only, drain_only=drain_only)
        return self

    def rubbery(self) -> OrificeEnchantmentFactory:
        return self.plasticity_of(EPlasticity.ZERO_RUBBERY.value)

    def springy(self) -> OrificeEnchantmentFactory:
        return self.plasticity_of(EPlasticity.ONE_SPRINGY.value)

    def tensile(self) -> OrificeEnchantmentFactory:
        return self.plasticity_of(EPlasticity.TWO_TENSILE.value)

    def resilient(self) -> OrificeEnchantmentFactory:
        return self.plasticity_of(EPlasticity.THREE_RESILIENT.value)

    def accommodating(self) -> OrificeEnchantmentFactory:
        return self.plasticity_of(EPlasticity.FOUR_ACCOMMODATING.value)

    def yielding(self) -> OrificeEnchantmentFactory:
        return self.plasticity_of(EPlasticity.FIVE_YIELDING.value)

    def malleable(self) -> OrificeEnchantmentFactory:
        return self.plasticity_of(EPlasticity.SIX_MALLEABLE.value)

    def mouldable(self) -> OrificeEnchantmentFactory:
        return self.plasticity_of(EPlasticity.SEVEN_MOULDABLE.value)

    def the_same_plasticity_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> OrificeEnchantmentFactory:
        return self.plasticity_of(self._getOrifice(char).plasticity, boost_only=boost_only, drain_only=drain_only)

    ####################
    # Capacity
    ####################
    def capacity_of(self, value: int, boost_only: bool = False, drain_only: bool = False) -> OrificeEnchantmentFactory:
        self.enchanter.addClampMods(self.ORIFICE_PRIMARY_MOD, self.ORIFICE_TFMOD2_CAPACITY, value, boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_capacity_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> OrificeEnchantmentFactory:
        return self.capacity_of(int(round(self._getOrifice(char).capacity)), boost_only=boost_only, drain_only=drain_only)

    def the_same_orifice_as(self, char: GameCharacter) -> OrificeEnchantmentFactory:
        self.the_same_capacity_as(char)
        self.the_same_depth_as(char)
        self.the_same_elasticity_as(char)
        self.the_same_plasticity_as(char)
        self.the_same_wetness_as(char)
        self.the_same_orifice_mods_as(char)
        return self

    #####################
    # Quicker mods setup
    #####################

    if ORIFICE_TFMOD2_MUSCLED is not None:
        def set_muscled(self, value: bool) -> OrificeEnchantmentFactory:
            self.enchanter.addBodyPartMod(self.ORIFICE_PRIMARY_MOD, self.ORIFICE_TFMOD2_MUSCLED, value)
            return self

        def is_muscled(self) -> OrificeEnchantmentFactory:
            return self.set_muscled(True)

        def not_muscled(self) -> OrificeEnchantmentFactory:
            return self.set_muscled(False)

        def the_same_orifice_muscles_as(self, char: GameCharacter) -> OrificeEnchantmentFactory:
            return self.set_muscled(EOrificeModifier.MUSCLE_CONTROL in self._getOrifice(char).orificeModifiers)


    if ORIFICE_TFMOD2_PUFFY is not None:
        def set_puffy(self, value: bool) -> OrificeEnchantmentFactory:
            self.enchanter.addBodyPartMod(self.ORIFICE_PRIMARY_MOD, self.ORIFICE_TFMOD2_PUFFY, value)
            return self

        def is_puffy(self) -> OrificeEnchantmentFactory:
            return self.set_puffy(True)

        def not_puffy(self) -> OrificeEnchantmentFactory:
            return self.set_puffy(False)

        def the_same_orifice_puffiness_as(self, char: GameCharacter) -> OrificeEnchantmentFactory:
            return self.set_puffy(EOrificeModifier.PUFFY in self._getOrifice(char).orificeModifiers)

    if ORIFICE_TFMOD2_RIBBED is not None:
        def set_ribbed(self, value: bool) -> OrificeEnchantmentFactory:
            self.enchanter.addBodyPartMod(self.ORIFICE_PRIMARY_MOD, self.ORIFICE_TFMOD2_RIBBED, value)
            return self

        def is_ribbed(self) -> OrificeEnchantmentFactory:
            return self.set_ribbed(True)

        def not_ribbed(self) -> OrificeEnchantmentFactory:
            return self.set_ribbed(False)

        def the_same_orifice_ribbing_as(self, char: GameCharacter) -> OrificeEnchantmentFactory:
            return self.set_ribbed(EOrificeModifier.RIBBED in self._getOrifice(char).orificeModifiers)

    if ORIFICE_TFMOD2_TENTACLED is not None:
        def set_tentacled(self, value: bool) -> OrificeEnchantmentFactory:
            self.enchanter.addBodyPartMod(self.ORIFICE_PRIMARY_MOD, self.ORIFICE_TFMOD2_TENTACLED, value)
            return self

        def is_tentacled(self) -> OrificeEnchantmentFactory:
            return self.set_tentacled(True)

        def not_tentacled(self) -> OrificeEnchantmentFactory:
            return self.set_tentacled(False)

        def the_same_orifice_tentacles_as(self, char: GameCharacter) -> OrificeEnchantmentFactory:
            return self.set_tentacled(EOrificeModifier.TENTACLED in self._getOrifice(char).orificeModifiers)

    def set_orifice_mod(self, mod: EOrificeModifier, val: bool) -> None:
        match mod:
            case EOrificeModifier.MUSCLE_CONTROL:
                if self.ORIFICE_TFMOD2_MUSCLED is not None:
                    self.set_muscled(val)
            case EOrificeModifier.PUFFY:
                if self.ORIFICE_TFMOD2_PUFFY is not None:
                    self.set_puffy(val)
            case EOrificeModifier.RIBBED:
                if self.ORIFICE_TFMOD2_RIBBED is not None:
                    self.set_ribbed(val)
            case EOrificeModifier.TENTACLED:
                if self.ORIFICE_TFMOD2_TENTACLED is not None:
                    self.set_tentacled(val)

    def using_orifice_mods(self, mods: Iterable[EOrificeModifier]) -> None:
        for mod in mods:
            self.set_orifice_mod(mod, True)

    def not_using_orifice_mods(self, mods: Iterable[EOrificeModifier]) -> None:
        for mod in mods:
            self.set_orifice_mod(mod, False)

    def the_same_orifice_mods_as(self, char: GameCharacter) -> None:
        for mod in EOrificeModifier:
            self.set_orifice_mod(mod, mod in self._getOrifice(char).orificeModifiers)

from __future__ import annotations

from typing import Dict, Iterable, Union

from lilitools.game.effects.enchanter.base import BaseEnchantmentFactory
from lilitools.saves.character.body.enums.penetrator_girth import EPenetratorGirth
from lilitools.saves.character.body.penetrator import Penetrator
from lilitools.saves.character.enums.penetrator_modifier import EPenetratorModifier
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.enums.tf_modifier import ETFModifier
from lilitools.saves.enums.tf_potency import ETFPotency
from lilitools.tfmodmaps import PEN2TF

__all__ = ['PenetratorEnchantmentFactory']


class PenetratorEnchantmentFactory(BaseEnchantmentFactory):
    PENETRATOR_PRIMARY_MOD: ETFModifier
    PENETRATOR_BODY_ATTR: str
    PENETRATOR_PENETRATOR_ATTR: str

    def _getPenetrator(self, char: GameCharacter) -> Penetrator:
        return getattr(getattr(char.body, self.PENETRATOR_BODY_ATTR), self.PENETRATOR_PENETRATOR_ATTR)

    def of_length(self, limit: int, boost_only: bool = False, drain_only: bool = False) -> PenetratorEnchantmentFactory:
        '''
        in cm
        max: 244
        '''
        self.enchanter.addClampMods(self.PENETRATOR_PRIMARY_MOD, ETFModifier.TF_MOD_SIZE, limit, boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_length_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> PenetratorEnchantmentFactory:
        return self.of_length(self._getPenetrator(char).length, boost_only=boost_only, drain_only=drain_only)

    def of_girth(self, limit: Union[int, EPenetratorGirth], boost_only: bool = False, drain_only: bool = False) -> PenetratorEnchantmentFactory:
        if isinstance(limit, EPenetratorGirth):
            limit = limit.value
        self.enchanter.addClampMods(self.PENETRATOR_PRIMARY_MOD, ETFModifier.TF_MOD_SIZE_SECONDARY, limit, boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_girth_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> PenetratorEnchantmentFactory:
        return self.of_girth(self._getPenetrator(char).girth, boost_only=boost_only, drain_only=drain_only)

    ########################
    # MODS
    ########################

    def set_penetrator_mod(self, mod: EPenetratorModifier, val: bool) -> None:
        self.enchanter.addBodyPartMod(self.PENETRATOR_PRIMARY_MOD, PEN2TF[mod], val)

    def using_penetrator_mods(self, mods: Iterable[EPenetratorModifier]) -> None:
        for mod in mods:
            self.set_penetrator_mod(mod, True)

    def not_using_penetrator_mods(self, mods: Iterable[EPenetratorModifier]) -> None:
        for mod in mods:
            self.set_penetrator_mod(mod, False)

    def the_same_penetrator_mods_as(self, char: GameCharacter) -> None:
        for mod in EPenetratorModifier:
            self.set_penetrator_mod(mod, mod in self._getPenetrator(char).penetratorModifiers)

    def the_same_penetrator_as(self, char: GameCharacter) -> PenetratorEnchantmentFactory:
        self.the_same_girth_as(char)
        self.the_same_length_as(char)
        self.the_same_penetrator_mods_as(char)
        return self

from __future__ import annotations

from typing_extensions import deprecated

from lilitools.game.effects.enchanter.base import BaseEnchantmentFactory
from lilitools.saves.enums.tf_modifier import ETFModifier

from lilitools.saves.enums.tf_potency import ETFPotency

__all__ = ['RemovableMixins']


class RemovableMixins(BaseEnchantmentFactory):
    REMOVABLE_PRIMARY_MOD: ETFModifier

    def grow(self, type: int = 1) -> RemovableMixins:
        self.enchanter.addClothingMod(self.REMOVABLE_PRIMARY_MOD, ETFModifier[f'TF_TYPE_{type}'], ETFPotency.MAJOR_BOOST, 0)
        return self

    @deprecated('Use grow instead.')
    def enable(self, type: int = 1) -> RemovableMixins:
        self.grow(type=type)
        return self

    def remove(self) -> RemovableMixins:
        self.enchanter.addClothingMod(self.REMOVABLE_PRIMARY_MOD, ETFModifier.REMOVAL, ETFPotency.MAJOR_BOOST, 0)
        return self

    @deprecated('Use remove instead.')
    def disable(self) -> RemovableMixins:
        self.remove()
        return self

from __future__ import annotations

from lilitools.game.effects.enchanter.mixins.body_hair_mixin import BodyHairMixins
from lilitools.game.effects.enchanter.mixins.orifice import OrificeEnchantmentFactory
from lilitools.game.effects.enchanter.mixins.penetrator import PenetratorEnchantmentFactory
from lilitools.game.effects.enchanter.mixins.removable import RemovableMixins
from lilitools.saves.character.cum_production import ECumProduction
from lilitools.saves.character.enums.fluid_regeneration import EFluidRegeneration
from lilitools.saves.character.fluid_expulsion import EFluidExpulsion
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.enums.tf_modifier import ETFModifier
from lilitools.utils import clampInt

__all__ = ['PenisEnchantmentFactory']


class PenisEnchantmentFactory(OrificeEnchantmentFactory, PenetratorEnchantmentFactory, BodyHairMixins, RemovableMixins):
    ORIFICE_PRIMARY_MOD = ETFModifier.TF_PENIS
    ORIFICE_BODY_ATTR: str = 'penis'
    ORIFICE_ORIFICE_ATTR: str = 'urethra'
    # Disable wetness, it's used for cum storage.
    ORIFICE_TFMOD1_WETNESS = None
    ORIFICE_TFMOD2_WETNESS = None

    PENETRATOR_PRIMARY_MOD = ETFModifier.TF_PENIS
    PENETRATOR_BODY_ATTR = 'penis'
    PENETRATOR_PENETRATOR_ATTR = 'penetrator'

    REMOVABLE_PRIMARY_MOD = ETFModifier.TF_PENIS

    ####################
    # Enable/Disable
    ####################
    def has_penis(self) -> PenisEnchantmentFactory:
        self.grow()
        return self

    def has_no_penis(self) -> PenisEnchantmentFactory:
        self.remove()
        return self

    ####################
    # Cum Expulsion
    ####################
    def of_expulsion_percentage(self, val: int, boost_only: bool = False, drain_only: bool = False) -> PenisEnchantmentFactory:
        self.enchanter.addClampMods(ETFModifier.TF_PENIS, ETFModifier.TF_MOD_CUM_EXPULSION, clampInt(val, 0, 100), boost_only=boost_only, drain_only=drain_only)
        return self

    def no_cum_expulsion(self) -> PenisEnchantmentFactory:
        self.of_expulsion_percentage(0, drain_only=True)
        return self

    def min_cum_expulsion(self) -> PenisEnchantmentFactory:
        self.of_expulsion_percentage(0, drain_only=True)
        return self

    def max_cum_expulsion(self) -> PenisEnchantmentFactory:
        self.of_expulsion_percentage(EFluidExpulsion.FOUR_HUGE.max, boost_only=True)
        return self

    def the_same_expulsion_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> PenisEnchantmentFactory:
        if char.body.testicles:
            self.of_expulsion_percentage(char.body.testicles.cumExpulsion, boost_only=boost_only, drain_only=drain_only)
        return self

    ####################
    # Cum Storage
    ####################
    def of_cum_storage_size(self, mL: int, boost_only: bool = False, drain_only: bool = False) -> PenisEnchantmentFactory:
        #print('of_cum_storage_size', repr({'boost_only': boost_only, 'drain_only': drain_only}))
        self.enchanter.addClampMods(ETFModifier.TF_PENIS, ETFModifier.TF_MOD_WETNESS, mL, boost_only=boost_only, drain_only=drain_only)
        return self

    def min_cum_storage(self) -> PenisEnchantmentFactory:
        self.of_cum_storage_size(0, drain_only=True)
        return self

    def max_cum_storage(self) -> PenisEnchantmentFactory:
        self.of_cum_storage_size(ECumProduction.SEVEN_MONSTROUS.max, boost_only=True)
        return self

    def the_same_cum_storage_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> PenisEnchantmentFactory:
        if char.body.testicles:
            self.of_cum_storage_size(char.body.testicles.cumStorage, boost_only=boost_only, drain_only=drain_only)
        return self

    ####################
    # Cum Regeneration
    ####################

    def of_cum_regeneration_rate(self, mLPerDay: int, boost_only: bool = False, drain_only: bool = False) -> PenisEnchantmentFactory:
        self.enchanter.addClampMods(ETFModifier.TF_PENIS, ETFModifier.TF_MOD_REGENERATION, clampInt(mLPerDay, 0, 500_000), boost_only=boost_only, drain_only=drain_only)
        return self

    def min_cum_regeneration_rate(self) -> PenisEnchantmentFactory:
        self.of_cum_regeneration_rate(EFluidRegeneration.ZERO_SLOW.min, drain_only=True)
        return self

    def max_cum_regeneration_rate(self) -> PenisEnchantmentFactory:
        self.of_cum_regeneration_rate(EFluidRegeneration.FOUR_VERY_RAPID.max, boost_only=True)
        return self

    def the_same_regeneration_rate_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> PenisEnchantmentFactory:
        if char.body.testicles:
            self.of_cum_regeneration_rate(char.body.testicles.cumRegeneration, boost_only=boost_only, drain_only=drain_only)
        return self

    ####################
    # Ball Size
    ####################

    def of_ball_size(self, limit: int, boost_only: bool = False, drain_only: bool = False) -> PenisEnchantmentFactory:
        self.enchanter.addClampMods(ETFModifier.TF_PENIS, ETFModifier.TF_MOD_SIZE_TERTIARY, limit, boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_ball_size_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> PenisEnchantmentFactory:
        return self.of_ball_size(char.body.testicles.testicleSize, boost_only=boost_only, drain_only=drain_only)

    def the_same_penis_as(self, char: GameCharacter) -> PenisEnchantmentFactory:
        if char.body.penis is None:
            self.has_no_penis()
        else:
            self.has_penis()
            self.the_same_penetrator_as(char)
            self.the_same_orifice_as(char)

        if char.body.testicles is not None:
            self.the_same_ball_size_as(char)
            self.the_same_cum_storage_as(char)
            self.the_same_expulsion_as(char)
            self.the_same_regeneration_rate_as(char)

        return self

from __future__ import annotations

from lilitools.game.effects.enchanter.base import BaseEnchantmentFactory
from lilitools.saves.enums.tf_modifier import ETFModifier
from lilitools.saves.enums.tf_potency import ETFPotency

__all__ = ["SpecialEnchantmentFactory"]


class SpecialEnchantmentFactory(BaseEnchantmentFactory):
    ######################
    # Orgasm Denial
    ######################
    def deny_orgasms(self) -> SpecialEnchantmentFactory:
        # <effect limit="0" mod1="CLOTHING_SPECIAL" mod2="CLOTHING_ORGASM_PREVENTION" potency="MINOR_BOOST" timer="72215" type="CLOTHING"/>
        self.enchanter.addClothingMod(
            ETFModifier.CLOTHING_SPECIAL,
            ETFModifier.CLOTHING_ORGASM_PREVENTION,
            ETFPotency.MINOR_BOOST,
            0,
        )
        return self

    ######################
    # Sealing
    ######################
    def seal(self, potency: ETFPotency = ETFPotency.BOOST) -> SpecialEnchantmentFactory:
        # <effect limit="0" mod1="CLOTHING_SPECIAL" mod2="CLOTHING_ORGASM_PREVENTION" potency="MINOR_BOOST" timer="72215" type="CLOTHING"/>
        # enchanter.addEffect(type="CLOTHING", mod1=ETFModifier.CLOTHING_SPECIAL, mod2=ETFModifier.CLOTHING_ORGASM_PREVENTION, potency=ETFPotency.MINOR_BOOST, limit=0, timer=2518)
        assert ETFPotency.MAJOR_DRAIN <= potency <= ETFPotency.MINOR_BOOST
        self.enchanter.addClothingMod(
            ETFModifier.CLOTHING_SPECIAL,
            ETFModifier.CLOTHING_SEALING,
            potency,
            0,
        )
        return self

    def seal_weakly(self) -> SpecialEnchantmentFactory:
        return self.seal(ETFPotency.MINOR_BOOST)

    def seal_moderately(self) -> SpecialEnchantmentFactory:
        return self.seal(ETFPotency.MINOR_DRAIN)

    def seal_strongly(self) -> SpecialEnchantmentFactory:
        return self.seal(ETFPotency.DRAIN)

    def seal_very_strongly(self) -> SpecialEnchantmentFactory:
        return self.seal(ETFPotency.MAJOR_DRAIN)

    ######################
    # Vibration
    ######################
    def vibrate(
        self, potency: ETFPotency = ETFPotency.BOOST
    ) -> SpecialEnchantmentFactory:
        # <effect limit="0" mod1="CLOTHING_SPECIAL" mod2="CLOTHING_ORGASM_PREVENTION" potency="MINOR_BOOST" timer="72215" type="CLOTHING"/>
        # enchanter.addEffect(type="CLOTHING", mod1=ETFModifier.CLOTHING_SPECIAL, mod2=ETFModifier.CLOTHING_ORGASM_PREVENTION, potency=ETFPotency.MINOR_BOOST, limit=0, timer=2518)
        assert potency >= ETFPotency.MINOR_BOOST
        self.enchanter.addClothingMod(
            ETFModifier.CLOTHING_SPECIAL,
            ETFModifier.CLOTHING_VIBRATION,
            potency,
            0,
        )
        return self

    def vibrate_weakly(self) -> SpecialEnchantmentFactory:
        self.vibrate(ETFPotency.MINOR_BOOST)

    def vibrate_moderately(self) -> SpecialEnchantmentFactory:
        self.vibrate(ETFPotency.BOOST)

    def vibrate_strongly(self) -> SpecialEnchantmentFactory:
        self.vibrate(ETFPotency.MAJOR_BOOST)

    ######################
    # Servitude
    ######################
    def apply_servitude(self) -> SpecialEnchantmentFactory:
        self.enchanter.addClothingMod(
            ETFModifier.CLOTHING_SPECIAL,
            ETFModifier.CLOTHING_SERVITUDE,
            ETFPotency.MINOR_BOOST,
            0,
        )
        return self

    ######################
    # Enslave
    ######################
    def enslave(self) -> SpecialEnchantmentFactory:
        self.enchanter.addClothingMod(
            ETFModifier.CLOTHING_SPECIAL,
            ETFModifier.CLOTHING_ENSLAVEMENT,
            ETFPotency.MINOR_BOOST,
            0,
        )
        return self

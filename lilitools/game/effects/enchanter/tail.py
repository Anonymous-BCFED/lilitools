from __future__ import annotations

from typing import Dict
from lilitools.game.effects.alchemist.body_hair_mixin import BodyHairMixins
from lilitools.game.effects.enchanter.mixins.orifice import OrificeEnchantmentFactory
from lilitools.game.effects.enchanter.mixins.penetrator import PenetratorEnchantmentFactory

from lilitools.game.effects.enchanter.mixins.removable import RemovableMixins
from lilitools.saves.character.body.enums.foot_structure import EFootStructure
from lilitools.saves.character.body.enums.leg_configuration import ELegConfiguration
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.enums.tf_modifier import ETFModifier
from lilitools.saves.enums.tf_potency import ETFPotency

__all__ = ['TailEnchantmentFactory']


class TailEnchantmentFactory(OrificeEnchantmentFactory, PenetratorEnchantmentFactory, BodyHairMixins, RemovableMixins):
    ORIFICE_PRIMARY_MOD = ETFModifier.TF_TAIL
    ORIFICE_BODY_ATTR = 'spinneret'
    ORIFICE_ORIFICE_ATTR = None

    PENETRATOR_PRIMARY_MOD = ETFModifier.TF_TAIL
    PENETRATOR_BODY_ATTR = 'penis'
    PENETRATOR_PENETRATOR_ATTR = 'penetrator'

    REMOVABLE_PRIMARY_MOD = ETFModifier.TF_TAIL

    #############
    # COPY
    #############
    def the_same_tail_as(self, char: GameCharacter) -> TailEnchantmentFactory:
        if char.body.penis is None:
            self.remove()
        else:
            self.grow()
            self.the_same_penetrator_as(char)
            self.the_same_orifice_as(char)
        return self

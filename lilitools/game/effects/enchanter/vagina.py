from __future__ import annotations

from typing import Set

from lilitools.game.effects.enchanter.mixins.penetrator import PenetratorEnchantmentFactory
from lilitools.game.effects.enchanter.mixins.removable import RemovableMixins
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.enums.tf_modifier import ETFModifier

from .mixins.body_hair_mixin import BodyHairMixins
from .mixins.orifice import OrificeEnchantmentFactory

__all__ = ['VaginaEnchantmentFactory']


class VaginaEnchantmentFactory(OrificeEnchantmentFactory, PenetratorEnchantmentFactory, BodyHairMixins, RemovableMixins):
    ORIFICE_PRIMARY_MOD = ETFModifier.TF_VAGINA
    ORIFICE_BODY_ATTR: str = 'vagina'
    ORIFICE_ORIFICE_ATTR: str = 'orifice'

    PENETRATOR_PRIMARY_MOD = ETFModifier.TF_VAGINA
    PENETRATOR_BODY_ATTR = 'vagina'
    PENETRATOR_PENETRATOR_ATTR = 'clit'

    REMOVABLE_PRIMARY_MOD = ETFModifier.TF_VAGINA

    ####################
    # Enable/Disable
    ####################
    def has_vagina(self) -> VaginaEnchantmentFactory:
        self.grow()
        return self

    def has_no_vagina(self) -> VaginaEnchantmentFactory:
        self.remove()
        return self

    ###################
    # Labia Size
    ###################
    def labia_size(self, limit: int, boost_only: bool = False, drain_only: bool = False) -> VaginaEnchantmentFactory:
        self.enchanter.addClampMods(ETFModifier.TF_VAGINA, ETFModifier.TF_MOD_SIZE_SECONDARY, limit, boost_only=boost_only, drain_only=drain_only)
        return self

    def the_same_labia_size_as(self, char: GameCharacter, boost_only: bool = False, drain_only: bool = False) -> VaginaEnchantmentFactory:
        self.enchanter.addClampMods(ETFModifier.TF_VAGINA, ETFModifier.TF_MOD_SIZE_SECONDARY, char.body.vagina.labiaSize, boost_only=boost_only, drain_only=drain_only)
        return self

    # Sameness
    def the_same_vagina_as(self, char: GameCharacter) -> VaginaEnchantmentFactory:
        if char.body.vagina is None:
            self.has_no_vagina()
        else:
            self.has_vagina()
            self.the_same_penetrator_as(char)
            self.the_same_orifice_as(char)
            self.the_same_labia_size_as(char)
        return self

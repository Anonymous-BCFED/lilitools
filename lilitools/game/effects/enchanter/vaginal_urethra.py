from __future__ import annotations


from lilitools.game.effects.enchanter.mixins.body_hair_mixin import BodyHairMixins
from lilitools.game.effects.enchanter.mixins.orifice import OrificeEnchantmentFactory
from lilitools.game.effects.enchanter.mixins.penetrator import PenetratorEnchantmentFactory
from lilitools.game.effects.enchanter.mixins.removable import RemovableMixins
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.enums.tf_modifier import ETFModifier

__all__ = ['VaginalUrethraEnchantmentFactory']


class VaginalUrethraEnchantmentFactory(OrificeEnchantmentFactory, PenetratorEnchantmentFactory, BodyHairMixins, RemovableMixins):
    ORIFICE_PRIMARY_MOD = ETFModifier.TF_VAGINA
    ORIFICE_BODY_ATTR: str = 'vagina'
    ORIFICE_ORIFICE_ATTR: str = 'urethra'

    # Sameness
    def the_same_vagina_as(self, char: GameCharacter) -> VaginalUrethraEnchantmentFactory:
        if char.body.vagina is None:
            #self.has_no_vagina()
            pass
        else:
            self.the_same_orifice_as(char)
        return self


from typing import Dict

from lilitools.saves.enums.tf_potency import ETFPotency


def genPotencyValuesIsNegative(ifneg: int, ifpos: int, drain: ETFPotency = ETFPotency.MAJOR_DRAIN, boost: ETFPotency = ETFPotency.MAJOR_BOOST) -> Dict[ETFPotency, int]:
    return {
        drain: ifneg,
        boost: ifpos,
    }
DEFAULT_POTENCY_VALUES: Dict[ETFPotency, int] = genPotencyValuesIsNegative(-1, 1)
# int depthIncrement = (potency.isNegative()?-1:1);
# 	int capacityIncrement = (potency.isNegative()?-2:2);
CAPACITY_POTENCY_VALUES: Dict[ETFPotency, int] = genPotencyValuesIsNegative(-2, 2)
# 	int elasticityIncrement = (potency.isNegative()?-1:1);
# 	int plasticityIncrement = (potency.isNegative()?-1:1);
# 	int wetnessIncrement = (potency.isNegative()?-1:1);
# 	int assSizeIncrement = (potency.isNegative()?-1:1);
# 	int hipSizeIncrement = (potency.isNegative()?-1:1);

# 	int breastSizeIncrement = (potency.isNegative()?-1:1);
# 	int nippleSizeIncrement = (potency.isNegative()?-1:1);
# 	int areolaeSizeIncrement = (potency.isNegative()?-1:1);
# 	int lactationIncrement = (potency.isNegative()?-50:50);
LACTATION_POTENCY_VALUES: Dict[ETFPotency, int] = genPotencyValuesIsNegative(-50, 50)

# 	int fluidRegenerationIncrement = (potency.isNegative()?-250:250);
REGENERATION_POTENCY_VALUES: Dict[ETFPotency, int] = genPotencyValuesIsNegative(-250, 250)

# 	int heightIncrement = (potency.isNegative()?-1:1);
# 	int muscleIncrement = (potency.isNegative()?-1:1);
# 	int bodySizeIncrement = (potency.isNegative()?-1:1);
# 	int femininityIncrement = (potency.isNegative()?-1:1);

# 	int lipSizeIncrement = (potency.isNegative()?-1:1);

# 	int hairLengthIncrement = (potency.isNegative()?-1:1);

# 	int penisSizeIncrement = (potency.isNegative()?-1:1);
# 	int testicleSizeIncrement = (potency.isNegative()?-1:1);
CUMSTORAGE_POTENCY_VALUES: Dict[ETFPotency, int] = genPotencyValuesIsNegative(-5, 5)
# 	int cumStorageIncrement = (potency.isNegative()?-5:5);
CUMEXPULSION_POTENCY_VALUES: Dict[ETFPotency, int] = genPotencyValuesIsNegative(-5, 5)
# 	int cumExpulsionIncrement = (potency.isNegative()?-5:5);

# 	int clitorisSizeIncrement = (potency.isNegative()?-1:1);
# 	int labiaSizeIncrement = (potency.isNegative()?-1:1);

# 	int bodyHairIncrement = (potency.isNegative()?-1:1);


from enum import Enum
from typing import Callable, Type

from lilitools.saves.enums.tf_potency import ETFPotency


def bool2pot(present: bool) -> ETFPotency:
    return ETFPotency.MAJOR_BOOST if present else ETFPotency.MAJOR_DRAIN

def str2e(s: str, enumType: Type[Enum]) -> Enum:
    if s.isnumeric():
        return enumType(int(s))
    else:
        return enumType[s]

def enumType(enumType: Type[Enum]) -> Callable[[str], Enum]:
    def _wrapped(s: str) -> Enum:
        return str2e(s, enumType)
    return _wrapped

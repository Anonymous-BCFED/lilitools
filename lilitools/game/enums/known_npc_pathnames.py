from __future__ import annotations
from dataclasses import dataclass
from typing import FrozenSet

@dataclass(frozen=True)
class KnownNPCPathNames:
    PLAYER: str = "com.lilithsthrone.game.character.PlayerCharacter"

    IMP_ATTACKER: str = "com.lilithsthrone.game.character.npc.submission.ImpAttacker"
    DOMINION_ALLEYWAY_ATTACKER: str = "com.lilithsthrone.game.character.npc.dominion.DominionAlleywayAttacker"
    ERIS_ALLEYWAY_ATTACKER: str = "com.lilithsthrone.game.character.npc.fields.ErisAlleywayAttacker"


    POSSIBLE_RNG_PROSTITUTE_PATHNAMES: FrozenSet[str] = frozenset({
        IMP_ATTACKER,
        DOMINION_ALLEYWAY_ATTACKER,
        ERIS_ALLEYWAY_ATTACKER,
    })

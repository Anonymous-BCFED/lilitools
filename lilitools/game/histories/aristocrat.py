from lilitools.game.histories.history import History
from lilitools.saves.character.enums.attribute import EAttribute

## from [LT]/src/com/lilithsthrone/game/character/persona/Occupation.java: ARISTOCRAT(Perk.JOB_ARISTOCRAT, @ dB/BnzBOQHANAPRx+2UkVmF0hwv6ofEjhR5cfc6bMCz+RMr0ga6sYFBqG7L+7LUeLlc9rslXZo+W7tXOQPceJQ==
## from [LT]/src/com/lilithsthrone/game/character/effects/Perk.java: public static AbstractPerk JOB_ARISTOCRAT = new AbstractPerk(20, @ h3BJ2LmCHZtTDcsAhknKcOoRY4AK3jtAwZN0ke/KtQEeaeKO2GB1QV/AMVXzZboRDJwaVdje9isV3UItPfUT3A==
class AristocratHistory(History):
    ID = 'ARISTOCRAT'

    def __init__(self) -> None:
        super().__init__()
        self.bonusEffects = {
            EAttribute.HEALTH_MAXIMUM: 10.,
            EAttribute.DAMAGE_MELEE_WEAPON: 25.,
            EAttribute.DAMAGE_RANGED_WEAPON: 25.,
        }

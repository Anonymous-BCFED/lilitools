from lilitools.game.histories.history import History
from lilitools.saves.character.enums.attribute import EAttribute

## from [LT]/src/com/lilithsthrone/game/character/persona/Occupation.java: ATHLETE(Perk.JOB_ATHLETE, @ jhuYBy0JT6kfunXZCaEIe4ClZZZVMFyFKw9Ix0j1s6wCL7ZL54GHA41unQREqcdV+eNzwEg97hvOHR41XDmbyw==
## from [LT]/src/com/lilithsthrone/game/character/effects/Perk.java: public static AbstractPerk JOB_ATHLETE = new AbstractPerk(20, @ rttCRJEuorx+iZGyk6NtS8hebL1VsXg5G609zcSW3Uc+QOZiCuJELRrAeJGmMWzo09TZRU9+NjnB8nMkbnu7iw==
class AthleteHistory(History):
    ID = 'ATHLETE'

    def __init__(self) -> None:
        super().__init__()
        self.bonusEffects = {
            EAttribute.MAJOR_PHYSIQUE: 10.,
        }

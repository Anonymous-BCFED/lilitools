from lilitools.game.histories.history import History
from lilitools.saves.character.enums.attribute import EAttribute

## from [LT]/src/com/lilithsthrone/game/character/persona/Occupation.java: BUTLER(Perk.JOB_BUTLER, @ dt6lukROHCt/bDvv/Z/vozz7E7CIdLCdgcHHkn0fv5VEvpVWidWszbKhc40EaJ6Qp5uf0uZzRQAl8BIDkUGz8w==
## from [LT]/src/com/lilithsthrone/game/character/effects/Perk.java: public static AbstractPerk JOB_BUTLER = new AbstractPerk(20, @ 6pXVm2dnAaOkAXELezRFW5Fhy+M2ipFsEH1ZfhwwuYFguYgI8u/l/bk2+eaDw7m9XS2HjqxDIB4SmpSw2pAtZg==
class ButlerHistory(History):
    ID = 'BUTLER'

    def __init__(self) -> None:
        super().__init__()
        self.bonusEffects = {
            EAttribute.MAJOR_PHYSIQUE: 5.,
            EAttribute.RESISTANCE_LUST: 25.,
        }

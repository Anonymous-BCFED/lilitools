from lilitools.game.histories.history import History
from lilitools.saves.character.enums.attribute import EAttribute

## from [LT]/src/com/lilithsthrone/game/character/persona/Occupation.java: CHEF(Perk.JOB_CHEF, @ byz+yscqam2lo4ibLFouJ0O321+8YoJRbtgAHAkLZixDz4rGB2GN0CiuYo7/RThQk8GHGTqb7VFv1ppeRaWJFA==
## from [LT]/src/com/lilithsthrone/game/character/effects/Perk.java: public static AbstractPerk JOB_CHEF = new AbstractPerk(20, @ 7Ll/FWxyLWAuhMZ4Nky3tT+D7SP8Jl2U/YYLjN10q1LJpV0EHTZqwzgVTp0R+Q6+Xz6SkIiRP85m7ySmH1Ohug==
class ChefHistory(History):
    ID = 'CHEF'

    def __init__(self) -> None:
        super().__init__()
        self.bonusEffects = {
            EAttribute.RESISTANCE_POISON: 25.,
        }

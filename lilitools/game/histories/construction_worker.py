from lilitools.game.histories.history import History
from lilitools.saves.character.enums.attribute import EAttribute

## from [LT]/src/com/lilithsthrone/game/character/persona/Occupation.java: CONSTRUCTION_WORKER(Perk.JOB_PLAYER_CONSTRUCTION_WORKER, @ bdopRwXlm8zK7sQhwj+j5JUV4HH3wtvV8IoaElxIZGuxMDbsuGjTRRDJZa1KUmRkiEVbK/eOqXDQNq49/R4n+w==
## from [LT]/src/com/lilithsthrone/game/character/effects/Perk.java: public static AbstractPerk JOB_PLAYER_CONSTRUCTION_WORKER = new AbstractPerk(20, @ wx5mr5mD4AStc0fJKtTP68MxUcq3gRy7Xw2IAnDGnJWRk60t97FthejeABECPLkX/p8r+ciAeKxFrzloT+jybw==
class ConstructionWorkerHistory(History):
    ID = 'CONSTRUCTION_WORKER'

    def __init__(self) -> None:
        super().__init__()
        self.bonusEffects = {
            EAttribute.MAJOR_PHYSIQUE:  5.,
            EAttribute.HEALTH_MAXIMUM:  10.,
            EAttribute.DAMAGE_PHYSICAL: 5.,
        }

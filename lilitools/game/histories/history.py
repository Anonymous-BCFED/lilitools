import typing
from typing import Dict

from lilitools.saves.character.enums.attribute import EAttribute

if typing.TYPE_CHECKING:
    from lilitools.saves.character.game_character import GameCharacter


class History:
    ID: str = ''
    def __init__(self) -> None:
        self.bonusEffects: Dict[EAttribute, float] = {}

    ## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public void setHistory(Occupation history) { @ LCvshH/WlVxKI+nuKvSZT71YliV+daRF2F3ARxt6s5zK3+SirP8ELkEpNJ5x7Ad1cQ7afg7aGwfWnVaYAJuBIg==
    def applyToCharacter(self, c: 'GameCharacter') -> None:
        for k, v in self.bonusEffects.items():
            #print(self.ID, k, v)
            c.incrementBonusAttribute(k, v)
        self.applyExtraEffects(c)
        
    ## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public void setHistory(Occupation history) { @ LCvshH/WlVxKI+nuKvSZT71YliV+daRF2F3ARxt6s5zK3+SirP8ELkEpNJ5x7Ad1cQ7afg7aGwfWnVaYAJuBIg==
    def removeFromCharacter(self, c: 'GameCharacter') -> None:
        for k, v in self.bonusEffects.items():
            #print(self.ID, k, -v)
            c.incrementBonusAttribute(k, -v)
        self.revertExtraEffects(c)

    def applyExtraEffects(self, c: 'GameCharacter') -> None:
        pass
    def revertExtraEffects(self, c: 'GameCharacter') -> None:
        pass

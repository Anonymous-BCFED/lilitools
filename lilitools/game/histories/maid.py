from lilitools.game.histories.history import History
from lilitools.saves.character.enums.attribute import EAttribute

## from [LT]/src/com/lilithsthrone/game/character/persona/Occupation.java: MAID(Perk.JOB_MAID, @ 86O1sf9xY/FVZom2E8CYaBZKSuh86jpb8wjuqYh+ur7pIFLD0IdLFM/vwiBqKI7lupFCJDUzV4WulPd81XrgmQ==
## from [LT]/src/com/lilithsthrone/game/character/effects/Perk.java: public static AbstractPerk JOB_MAID = new AbstractPerk(20, @ 2o3eSGy2qpJLAv77h9XgAqKslXvtNBonwHOLt9rI9K4YWFPJtVj1QGs/F5nre1/Z99ZyNkVpAYrEI2hhWRu2Gg==
class MaidHistory(History):
    ID = 'MAID'

    def __init__(self) -> None:
        super().__init__()
        self.bonusEffects = {
            EAttribute.DAMAGE_LUST: 5.,
            EAttribute.MAJOR_PHYSIQUE: 5.,
        }

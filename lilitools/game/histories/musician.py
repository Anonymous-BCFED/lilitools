from lilitools.game.histories.history import History
from lilitools.saves.character.enums.attribute import EAttribute

## from [LT]/src/com/lilithsthrone/game/character/persona/Occupation.java: MUSICIAN(Perk.JOB_MUSICIAN, @ rkma/TSJ2gxKlpvkyE5TjJBbLIpxhlZKbJhWbTjQJCYDWTyRBRi910xgRydowYMQvIDqp3V2slH7u/wrHLwPhw==
## from [LT]/src/com/lilithsthrone/game/character/effects/Perk.java: public static AbstractPerk JOB_MUSICIAN = new AbstractPerk(20, @ XXE2lcv7bxrYIIirBBhLUFp7DJlJALuo48UIgt7G24NeYfXBxCdKe0Lwap6yHyr6YV/3qBb2Q84g3yYtXqJGbg==
class MusicianHistory(History):
    ID = 'MUSICIAN'

    def __init__(self) -> None:
        super().__init__()
        self.bonusEffects = {
            EAttribute.DAMAGE_LUST: 25.,
        }

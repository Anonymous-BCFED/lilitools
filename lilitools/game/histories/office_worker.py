from lilitools.game.histories.history import History
from lilitools.saves.character.enums.attribute import EAttribute

## from [LT]/src/com/lilithsthrone/game/character/persona/Occupation.java: OFFICE_WORKER(Perk.JOB_OFFICE_WORKER, @ UHrEV62G9t3wKC9eFAJJmImOAoO//Un3PlCGU+Qg8vPyDO3eR4itkmwiz33hHEVf0etAwG6ZakpA/xpDFrKTEQ==
## from [LT]/src/com/lilithsthrone/game/character/effects/Perk.java: public static AbstractPerk JOB_OFFICE_WORKER = new AbstractPerk(20, @ 0WOqcAfy1CT7hyY/Nzi0TK4IGUBtX+bYoiS/vfyLMZ7e2yohAUBcfCcl9EbGwkD8FNMVSLR6Mv5C7nLkMohsWA==
class OfficeWorkerHistory(History):
    ID = 'OFFICE_WORKER'

    def __init__(self) -> None:
        super().__init__()
        self.bonusEffects = {
            EAttribute.CRITICAL_DAMAGE: 50.,
        }

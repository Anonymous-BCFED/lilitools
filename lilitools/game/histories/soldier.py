from lilitools.game.histories.history import History
from lilitools.saves.character.enums.attribute import EAttribute

## from [LT]/src/com/lilithsthrone/game/character/persona/Occupation.java: SOLDIER(Perk.JOB_SOLDIER, @ zBOX7JxtuSRI12q3dllUmoz6OquCOS+VJFYPRt6P4Nzw1WSmP2aCQYQrB8Xb+SAcKoOemNY+2pmb9CzNdlSGvg==
## from [LT]/src/com/lilithsthrone/game/character/effects/Perk.java: public static AbstractPerk JOB_SOLDIER = new AbstractPerk(20, @ vzFQajRhwjn6HckNac79zO/QE6ceLi+1qKFWp8Nci4c1FwbE5zrqK+Lz/4zp83sEpZ7TsfJRyF82Bz2kW81ywg==
class SoldierHistory(History):
    ID = 'SOLDIER'

    def __init__(self) -> None:
        super().__init__()
        self.bonusEffects = {
            EAttribute.MAJOR_PHYSIQUE: 5.,
            EAttribute.HEALTH_MAXIMUM: 25.,
            EAttribute.RESISTANCE_PHYSICAL: 5.,
            EAttribute.DAMAGE_PHYSICAL: 10.,
        }

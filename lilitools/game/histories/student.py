from lilitools.game.histories.history import History
from lilitools.saves.character.enums.attribute import EAttribute

## from [LT]/src/com/lilithsthrone/game/character/persona/Occupation.java: OFFICE_WORKER(Perk.JOB_OFFICE_WORKER, @ UHrEV62G9t3wKC9eFAJJmImOAoO//Un3PlCGU+Qg8vPyDO3eR4itkmwiz33hHEVf0etAwG6ZakpA/xpDFrKTEQ==
## from [LT]/src/com/lilithsthrone/game/character/effects/Perk.java: public static AbstractPerk JOB_STUDENT = new AbstractPerk(20, @ FezJjM9B77zpgklU0Ugifw2VHNbi8SeAY0ysWRqcq0vGk8zmRTKSZmT31tuE+ccwcy6nl8FSBRTqF4NSHevpVQ==
class StudentHistory(History):
    def __init__(self) -> None:
        super().__init__()
        self.bonusEffects = {
            EAttribute.MAJOR_ARCANE: 10.,
        }
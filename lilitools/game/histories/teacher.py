from lilitools.game.histories.history import History
from lilitools.saves.character.enums.attribute import EAttribute

## from [LT]/src/com/lilithsthrone/game/character/persona/Occupation.java: TEACHER(Perk.JOB_TEACHER, @ MoIJlMDvyao9PtbtcNEKUzaB3iB5n/8hBuoED58wApf55kcxJdHQqOmAoU3CQCejIYsCB8+ptb+u5qDQbSL6oA==
## from [LT]/src/com/lilithsthrone/game/character/effects/Perk.java: public static AbstractPerk JOB_TEACHER = new AbstractPerk(20, @ G3M9si4IzSgit0MDMP2+EiHEFDgkEOuPmKmEtt4CIazf5clApd4T1mamuQQ2Cgfaz1qg/X3k8TlScJdZ5FitKg==
class TeacherHistory(History):
    ID = 'TEACHER'

    def __init__(self) -> None:
        super().__init__()
        self.bonusEffects = {
            EAttribute.SPELL_COST_MODIFIER: 10.,
        }

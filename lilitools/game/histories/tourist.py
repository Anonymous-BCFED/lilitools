from lilitools.game.histories.history import History
from lilitools.saves.character.enums.attribute import EAttribute

## from [LT]/src/com/lilithsthrone/game/character/persona/Occupation.java: TOURIST(Perk.JOB_TOURIST, @ +CA4SAb+VuaqusBWKoEtElupHDhujJCail2oE6g52VeafnUiPycLKimRYsB5xlc9uhz4lvnJHvTkdPZ9xXGAYA==
## from [LT]/src/com/lilithsthrone/game/character/effects/Perk.java: public static AbstractPerk JOB_TOURIST = new AbstractPerk(20, @ 7ctm/YM2Aa9dao5VTM2It2dBbBSVVmeep1MTWoFKfb5nTYpnMLOyxnKQcpnZkxEpnGx4a2jLRFKmfh74JdRaXg==
class TouristHistory(History):
    ID = 'TOURIST'

    def __init__(self) -> None:
        super().__init__()
        '''Util.newHashMapOfValues(
					new Value<>(Attribute.HEALTH_MAXIMUM, 25)),'''
        self.bonusEffects = {
            EAttribute.HEALTH_MAXIMUM: 25.,
        }

from lilitools.game.histories.history import History
from lilitools.saves.character.enums.attribute import EAttribute

## from [LT]/src/com/lilithsthrone/game/character/persona/Occupation.java: UNEMPLOYED(Perk.JOB_UNEMPLOYED, @ HOPONqZzh7kEyIS53z9WyqEZHYO6Z64OjtnF/BXZrjCFMznyu+PP5ufDFGcj3OcKzugoGNTPTK37GrXj9om6Yw==
## from [LT]/src/com/lilithsthrone/game/character/effects/Perk.java: public static AbstractPerk JOB_UNEMPLOYED = new AbstractPerk(20, @ IZDsAVpjWn1fxTZUS8CyOjucTXWKN3eH/YhEPfFNbz0AH3dTfygZuKczhDeLzGa0EKVHgLTg/DLUMn5AnXcllQ==
class UnemployedHistory(History):
    ID = 'UNEMPLOYED'

    def __init__(self) -> None:
        super().__init__()
        self.bonusEffects = {
            EAttribute.MAJOR_PHYSIQUE: 2.,
            EAttribute.DAMAGE_UNARMED: 5.,
            EAttribute.DAMAGE_PHYSICAL: 5.,
        }

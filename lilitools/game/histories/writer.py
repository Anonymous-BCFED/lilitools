from lilitools.game.histories.history import History
from lilitools.saves.character.enums.attribute import EAttribute

## from [LT]/src/com/lilithsthrone/game/character/persona/Occupation.java: WRITER(Perk.JOB_WRITER, @ 1XXEB/AQwkLRLolsNRmCICX/vsf2sPW3iRq2tTA6prbwAkJHdhOvlsLh2MJvi73FqS96wzvhQozaJYSKdaaIEQ==
## from [LT]/src/com/lilithsthrone/game/character/effects/Perk.java: public static AbstractPerk JOB_WRITER = new AbstractPerk(20, @ Z+RmYggAIWptZdra1HZkD/itbqHlJ0l6DYS5oFERlc53wasFatAqEm4Ep+AMbCXX+CKl6jXG+p9NSPdZdC2oCA==
class WriterHistory(History):
    ID = 'WRITER'

    def __init__(self) -> None:
        super().__init__()
        self.bonusEffects = {
            EAttribute.DAMAGE_SPELLS: 25.,
        }

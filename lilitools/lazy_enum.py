from abc import ABCMeta
from enum import Enum, EnumMeta
from typing import TYPE_CHECKING, Generic, TypeVar


class AbstractEnumMeta(EnumMeta, ABCMeta):
    pass


T = TypeVar("T")

if TYPE_CHECKING:
    # HACK: Trick mypy and pylance
    def LazyValueProvider(*args, **kwargs) -> T:
        return T()
    class LazyGenericEnum(Generic[T]):
        pass
else:
    class LazyValueProvider(Generic[T]):
        def __init__(_: T, self) -> None:
            super().__init__()

        def get_value(self) -> T:
            pass


    class LazyGenericEnum(Generic[T], Enum, metaclass=AbstractEnumMeta):
        def __getattribute__(self, name) -> T:
            result = super().__getattribute__(name)
            if name == "value" and isinstance(result, LazyValueProvider):
                result = self._lazy_init(result)
                setattr(self, "_value_", result)
            return result

        @classmethod
        # @abstractmethod
        def _lazy_init(cls, initializer: LazyValueProvider):
            return initializer.get_value()

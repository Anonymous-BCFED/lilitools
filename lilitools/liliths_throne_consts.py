#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#              BY devtools/build.py             #
#         Constants from the game itself        #
#################################################
from typing import Final
LILITHS_THRONE_VERSION: Final[str] = "0.4.10"
LILITHS_THRONE_COMMIT: Final[bytes] = bytes([198, 123, 175, 75, 132, 63, 206, 197, 16, 28, 204, 39, 245, 127, 154, 242, 7, 236, 130, 169])

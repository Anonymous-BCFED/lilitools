from __future__ import annotations

import os
import sys
from typing import IO, Any, AnyStr, Optional, Tuple, Union

import click

DEBUG = False


class ClickWrap:
    INDENT: int = 0

    def __init__(self, context: str = "") -> None:
        self.context: str = context

    def __enter__(self):
        self.indent()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.dedent()
        return False

    def indent(self) -> ClickWrap:
        ClickWrap.INDENT += 1
        return self

    def dedent(self) -> ClickWrap:
        ClickWrap.INDENT -= 1
        return self

    def _getIndent(self) -> str:
        return "  " * self.INDENT

    def _getPrefix(self) -> str:
        return f"[{self.context}] " if self.context else ""

    def echo(
        self,
        message: Optional[Any] = None,
        file: Optional[IO[Any]] = None,
        nl: bool = True,
        err: bool = False,
        color: Optional[bool] = None,
        prefix: bool = True,
        indent: bool = True,
    ) -> ClickWrap:
        # https://github.com/pallets/click/blob/14472ffcd80dd86d47ddc08341168152540ee6f2/src/click/utils.py#L256
        if message is not None and not isinstance(message, (str, bytes, bytearray)):
            message = str(message)
        click.echo(
            (self._getIndent() if indent else "")
            + (self._getPrefix() if prefix else "")
            + str(message),
            file=file,
            nl=nl,
            err=err,
            color=color,
        )
        return self

    def secho(
        self,
        message: Optional[Any] = None,
        file: Optional[IO[AnyStr]] = None,
        nl: bool = True,
        err: bool = False,
        color: Optional[bool] = None,
        prefix: bool = True,
        indent: bool = True,
        **styles: Any,
    ) -> ClickWrap:
        # https://github.com/pallets/click/blob/main/src/click/termui.py#L634
        if message is not None and not isinstance(message, (bytes, bytearray)):
            message = click.style(message, **styles)
        self.echo(
            message,
            file=file,
            nl=nl,
            err=err,
            color=color,
            prefix=prefix,
            indent=indent,
        )
        return self

    def style(
        self,
        text: Any,
        fg: Optional[Union[int, Tuple[int, int, int], str]] = None,
        bg: Optional[Union[int, Tuple[int, int, int], str]] = None,
        bold: Optional[bool] = None,
        dim: Optional[bool] = None,
        underline: Optional[bool] = None,
        overline: Optional[bool] = None,
        italic: Optional[bool] = None,
        blink: Optional[bool] = None,
        reverse: Optional[bool] = None,
        strikethrough: Optional[bool] = None,
        reset: bool = True,
    ) -> str:
        # https://github.com/pallets/click/blob/f6a681adc13d5961073c00e57044e3855c3b76bd/src/click/termui.py#L465
        return click.style(
            text,
            fg=fg,
            bg=bg,
            bold=bold,
            dim=dim,
            underline=underline,
            overline=overline,
            italic=italic,
            blink=blink,
            reverse=reverse,
            strikethrough=strikethrough,
            reset=reset,
        )

    # Standardized logging stuff.
    ########################

    def debug(self, message: Optional[Any] = None) -> ClickWrap:
        if DEBUG:
            self.secho(f"D: {message}", fg="white", dim=True, err=True)
        return self

    def info(self, message: Optional[Any] = None) -> ClickWrap:
        self.secho(message, fg="white", err=False)
        return self

    def warn(self, message: Optional[Any] = None) -> ClickWrap:
        self.secho(f"W: {message}", fg="yellow", err=True)
        return self

    def error(self, message: Optional[Any] = None) -> ClickWrap:
        self.secho(f"ERROR: {message}", fg="red", err=True)
        return self

    def critical(self, message: Optional[Any] = None) -> ClickWrap:
        self.secho(f"CRITICAL: {message}", bg="red", fg="white", err=True)
        sys.exit(1)

    def success(self, message: Optional[Any] = None) -> ClickWrap:
        self.secho(message, fg="green", err=False)
        return self


ClickWrap.INDENT = (
    int(os.environ["LOGGING_INDENT_LEVEL"])
    if "LOGGING_INDENT_LEVEL" in os.environ
    else 0
)


def getLogger(context: str = "") -> ClickWrap:
    return ClickWrap(context)

import os
from pathlib import Path, PurePath
from typing import Dict, Optional, Set

from rich.console import Console

IS_NT = os.name == "nt"
# CaseSensitivePathType = PosixPath if not IS_NT else CaseSensitiveWindowsPath
# CaseInsensitivePathType = CaseInsensitivePosixPath if not IS_NT else WindowsPath
MOD_DIRS: Set[tuple[str, ...]] = {
    ("colours",),
    ("combatMove",),
    ("dialogue",),
    ("encounters",),
    ("items", "clothing"),
    ("items", "items"),
    ("items", "patterns"),
    ("items", "tattoos"),
    ("items", "weapons"),
    ("outfits",),
    ("setBonuses",),
    ("sex", "actions"),
    ("sex", "managers"),
    ("statusEffects",),
}


class ModCheckResults:
    def __init__(self) -> None:
        self.info: Set[str] = set()
        self.warnings: Set[str] = set()
        self.errors: Set[str] = set()

    @property
    def has_messages(self) -> bool:
        return any([self.info, self.warnings, self.errors])


class ModChecker:
    def __init__(self, game_dir: Path, console: Console) -> None:
        self.console: Optional[Console] = console
        self.game_dir = game_dir
        self.mods_dir = game_dir / "res" / "mods"

    def check_all(self) -> Dict[Path, ModCheckResults]:
        mod_results: Dict[Path, ModCheckResults] = {}

        def do_check(mod_dir: Path):
            if mod_dir.is_dir():
                mod_results[mod_dir] = self.check_one(mod_dir)

        for mod_dir in self.mods_dir.iterdir():
            if self.console is not None:
                with self.console.status(f"Checking mod dir {mod_dir}..."):
                    do_check(mod_dir)
            else:
                do_check(mod_dir)
        return mod_results

    def check_one(self, mod_dir: Path) -> ModCheckResults:
        mcr = ModCheckResults()
        self._check_dir_names(mod_dir, mcr)
        return mcr

    def _check_dir_names(self, modPath: Path, mcr: ModCheckResults) -> None:
        for pathParts in MOD_DIRS:
            # caseSensitivePath = CaseSensitivePathType(*(modPath.parts + pathParts))
            # caseInsensitivePath = CaseInsensitivePathType(*(modPath.parts + pathParts))
            # if caseSensitivePath.is_dir() == caseInsensitivePath.is_dir():
            #     print(modPath,pathParts,caseSensitivePath.is_dir(),caseInsensitivePath.is_dir())
            #     continue
            desiredPath = PurePath(*modPath.parts)
            actualPath = Path(*modPath.parts)
            for part in pathParts:
                desiredPath = desiredPath / part
                found = next(
                    filter(
                        lambda x: str(x.name).lower() == str(part).lower(),
                        actualPath.iterdir(),
                    ),
                    None,
                )
                if found is None:
                    actualPath = None
                    break
                actualPath = actualPath / found
            if actualPath is None:
                # warnings.add(f"Could not find path {pathParts}!")
                continue
            else:
                desiredSubdir = desiredPath.relative_to(modPath)
                actualSubdir = actualPath.relative_to(modPath)
                if desiredSubdir.parts != actualSubdir.parts:
                    desiredSubdir = "." + os.sep + str(desiredSubdir) + os.sep
                    actualSubdir = "." + os.sep + str(actualSubdir) + os.sep
                    mcr.errors.add(
                        f"Directory {actualSubdir} should be named {desiredSubdir}"
                    )

import fnmatch
import re
from pathlib import Path, PurePath, _PosixFlavour, _WindowsFlavour

__all__ = ["CaseInsensitivePurePosixPath"]


class _CaseInsensitivePosixFlavour(_PosixFlavour):
    def casefold(self, s):
        return s.lower()

    def casefold_parts(self, parts):
        return [p.lower() for p in parts]

    def compile_pattern(self, pattern):
        return re.compile(fnmatch.translate(pattern), re.IGNORECASE).fullmatch


_case_insensitive_posix_flavour = _CaseInsensitivePosixFlavour()


class CaseInsensitivePurePosixPath(PurePath):
    """PurePath subclass for non-Windows systems.

    On a POSIX system, instantiating a PurePath should return this object.
    However, you can also instantiate it directly on any system.
    """

    _flavour = _case_insensitive_posix_flavour
    __slots__ = ()
class CaseInsensitivePosixPath(Path, CaseInsensitivePurePosixPath):
    """Path subclass for non-Windows systems.

    On a POSIX system, instantiating a Path should return this object.
    """
    __slots__ = ()

class _CaseSensitiveWindowsFlavour(_WindowsFlavour):
    def casefold(self, s):
        return s

    def casefold_parts(self, parts):
        return parts

    def compile_pattern(self, pattern):
        return re.compile(fnmatch.translate(pattern)).fullmatch


_case_sensitive_windows_flavour = _CaseSensitiveWindowsFlavour()


class CaseSensitivePureWindowsPath(PurePath):
    _flavour = _case_sensitive_windows_flavour
    __slots__ = ()


class CaseSensitiveWindowsPath(Path, CaseSensitivePureWindowsPath):
    __slots__ = ()

    def is_mount(self):
        raise NotImplementedError("Path.is_mount() is unsupported on this system")

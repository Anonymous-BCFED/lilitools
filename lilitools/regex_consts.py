#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#             BY devtools/_utils.py             #
#        Pre-compiled regular expressions       #
#################################################
import re
from typing import Callable,Final,Pattern,TypeAlias
__all__=['REG_COW_STALL','REG_GLUON_CUSTOMJS','REG_GLUON_VERSIONS','REG_GLUON_VERSIONTYPE','REG_LT_JARS','REG_NUMBERS','REG_RECOVERY_ROOM','REG_TRAINING_ROOM','REG_VALID_ROOMS']
_C=re.compile
_P=Pattern
REG_COW_STALL=_C('Cow +(?:[Ss]tall|[Rr]oom|[Pp]en) +([\\dA-Z]+)-?([\\dA-Z])?',0)
REG_GLUON_CUSTOMJS=_C('//gluonhq\\.com/wp-content/uploads/custom-css-js/\\d+\\.js\\?v=\\d+',0)
REG_GLUON_VERSIONS=_C('new Version\\((.+)\\)',0)
REG_GLUON_VERSIONTYPE=_C('VersionType\\.([_A-Z]+)',0)
REG_LT_JARS=_C('LilithsThrone_(?P<major>\\d+)_(?P<minor>\\d+)_(?P<patch>\\d+)(?:_(?P<revision>\\d+))?\\.jar',0)
REG_NUMBERS=_C('(\\d+)',0)
REG_RECOVERY_ROOM=_C('Recovery +(?:[Rr]oom|[Oo]verflow) +([\\dA-Z]+)-?([\\dA-Z])?',0)
REG_TRAINING_ROOM=_C('Training +(?:[Rr]oom|[Oo]verflow) +([\\dA-Z]+)-?([\\dA-Z])?',0)
REG_VALID_ROOMS=_C('(?:Garden Room ([A-Z])G-(\\d+)|Room ([A-Z])-(\\d+))',0)
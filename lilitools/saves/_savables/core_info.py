#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.date import Date
from lilitools.saves.enums.weather import EWeather
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawCoreInfo']
## from [LT]/src/com/lilithsthrone/game/Game.java: public static void exportGame(String exportFileName, boolean allowOverwrite, boolean isAutoSave) { @ XlY51VAk6J5oRXtpX1P5kFr0uGcEbeit57Nvol/4ZoAAEI91lSBjkQO3PM2BpecHpoWer94eBIo8gYEpqr5P1A==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawCoreInfo(Savable):
    TAG = 'coreInfo'
    _XMLID_ATTR_DATE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_GATHERINGSTORMDURATIONINSECONDS_ATTRIBUTE: str = 'gatheringStormDurationInSeconds'
    _XMLID_ATTR_ID_ATTRIBUTE: str = 'id'
    _XMLID_ATTR_LASTAUTOSAVETIME_ATTRIBUTE: str = 'lastAutoSaveTime'
    _XMLID_ATTR_NEXTSTORMTIMEINSECONDS_ATTRIBUTE: str = 'nextStormTimeInSeconds'
    _XMLID_ATTR_SECONDSPASSED_ATTRIBUTE: str = 'secondsPassed'
    _XMLID_ATTR_VERSION_ATTRIBUTE: str = 'version'
    _XMLID_ATTR_WEATHERTIMEREMAININGINSECONDS_ATTRIBUTE: str = 'weatherTimeRemainingInSeconds'
    _XMLID_ATTR_WEATHER_ATTRIBUTE: str = 'weather'
    _XMLID_TAG_DATE_ELEMENT: str = 'date'

    def __init__(self) -> None:
        super().__init__()
        self.version: str = ''
        self.id: int = 0
        self.lastAutoSaveTime: int = 0
        self.secondsPassed: int = 0
        self.weather: EWeather = EWeather(0)
        self.nextStormTimeInSeconds: int = 0
        self.gatheringStormDurationInSeconds: int = 0
        self.weatherTimeRemainingInSeconds: int = 0
        self.date: Date = Date()  # Element

    def overrideAttrs(self, date_attribute: _Optional_str = None, gatheringStormDurationInSeconds_attribute: _Optional_str = None, id_attribute: _Optional_str = None, lastAutoSaveTime_attribute: _Optional_str = None, nextStormTimeInSeconds_attribute: _Optional_str = None, secondsPassed_attribute: _Optional_str = None, version_attribute: _Optional_str = None, weather_attribute: _Optional_str = None, weatherTimeRemainingInSeconds_attribute: _Optional_str = None) -> None:
        if date_attribute is not None:
            self._XMLID_ATTR_DATE_ATTRIBUTE = date_attribute
        if gatheringStormDurationInSeconds_attribute is not None:
            self._XMLID_ATTR_GATHERINGSTORMDURATIONINSECONDS_ATTRIBUTE = gatheringStormDurationInSeconds_attribute
        if id_attribute is not None:
            self._XMLID_ATTR_ID_ATTRIBUTE = id_attribute
        if lastAutoSaveTime_attribute is not None:
            self._XMLID_ATTR_LASTAUTOSAVETIME_ATTRIBUTE = lastAutoSaveTime_attribute
        if nextStormTimeInSeconds_attribute is not None:
            self._XMLID_ATTR_NEXTSTORMTIMEINSECONDS_ATTRIBUTE = nextStormTimeInSeconds_attribute
        if secondsPassed_attribute is not None:
            self._XMLID_ATTR_SECONDSPASSED_ATTRIBUTE = secondsPassed_attribute
        if version_attribute is not None:
            self._XMLID_ATTR_VERSION_ATTRIBUTE = version_attribute
        if weather_attribute is not None:
            self._XMLID_ATTR_WEATHER_ATTRIBUTE = weather_attribute
        if weatherTimeRemainingInSeconds_attribute is not None:
            self._XMLID_ATTR_WEATHERTIMEREMAININGINSECONDS_ATTRIBUTE = weatherTimeRemainingInSeconds_attribute

    def overrideTags(self, date_element: _Optional_str = None) -> None:
        if date_element is not None:
            self._XMLID_TAG_DATE_ELEMENT = date_element

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        sf: etree._Element
        value = e.get(self._XMLID_ATTR_VERSION_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_VERSION_ATTRIBUTE, 'version')
        else:
            self.version = str(value)
        value = e.get(self._XMLID_ATTR_ID_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_ID_ATTRIBUTE, 'id')
        else:
            self.id = int(value)
        value = e.get(self._XMLID_ATTR_LASTAUTOSAVETIME_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_LASTAUTOSAVETIME_ATTRIBUTE, 'lastAutoSaveTime')
        else:
            self.lastAutoSaveTime = int(value)
        value = e.get(self._XMLID_ATTR_SECONDSPASSED_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_SECONDSPASSED_ATTRIBUTE, 'secondsPassed')
        else:
            self.secondsPassed = int(value)
        value = e.get(self._XMLID_ATTR_WEATHER_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_WEATHER_ATTRIBUTE, 'weather')
        else:
            self.weather = EWeather[value]
        value = e.get(self._XMLID_ATTR_NEXTSTORMTIMEINSECONDS_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_NEXTSTORMTIMEINSECONDS_ATTRIBUTE, 'nextStormTimeInSeconds')
        else:
            self.nextStormTimeInSeconds = int(value)
        value = e.get(self._XMLID_ATTR_GATHERINGSTORMDURATIONINSECONDS_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_GATHERINGSTORMDURATIONINSECONDS_ATTRIBUTE, 'gatheringStormDurationInSeconds')
        else:
            self.gatheringStormDurationInSeconds = int(value)
        value = e.get(self._XMLID_ATTR_WEATHERTIMEREMAININGINSECONDS_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_WEATHERTIMEREMAININGINSECONDS_ATTRIBUTE, 'weatherTimeRemainingInSeconds')
        else:
            self.weatherTimeRemainingInSeconds = int(value)
        if (sf := e.find(self._XMLID_TAG_DATE_ELEMENT)) is not None:
            self.date = Date()
            self.date.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'date', self._XMLID_TAG_DATE_ELEMENT)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_VERSION_ATTRIBUTE] = str(self.version)
        e.attrib[self._XMLID_ATTR_ID_ATTRIBUTE] = str(self.id)
        e.attrib[self._XMLID_ATTR_LASTAUTOSAVETIME_ATTRIBUTE] = str(self.lastAutoSaveTime)
        e.attrib[self._XMLID_ATTR_SECONDSPASSED_ATTRIBUTE] = str(self.secondsPassed)
        e.attrib[self._XMLID_ATTR_WEATHER_ATTRIBUTE] = self.weather.name
        e.attrib[self._XMLID_ATTR_NEXTSTORMTIMEINSECONDS_ATTRIBUTE] = str(self.nextStormTimeInSeconds)
        e.attrib[self._XMLID_ATTR_GATHERINGSTORMDURATIONINSECONDS_ATTRIBUTE] = str(self.gatheringStormDurationInSeconds)
        e.attrib[self._XMLID_ATTR_WEATHERTIMEREMAININGINSECONDS_ATTRIBUTE] = str(self.weatherTimeRemainingInSeconds)
        e.append(self.date.toXML(self._XMLID_TAG_DATE_ELEMENT))
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['version'] = self.version
        data['id'] = int(self.id)
        data['lastAutoSaveTime'] = int(self.lastAutoSaveTime)
        data['secondsPassed'] = int(self.secondsPassed)
        data['weather'] = self.weather.name
        data['nextStormTimeInSeconds'] = int(self.nextStormTimeInSeconds)
        data['gatheringStormDurationInSeconds'] = int(self.gatheringStormDurationInSeconds)
        data['weatherTimeRemainingInSeconds'] = int(self.weatherTimeRemainingInSeconds)
        data['date'] = self.date.toDict()
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'version' not in data:
            raise KeyRequiredException(self, data, 'version', 'version')
        self.version = str(data['version'])
        if 'id' not in data:
            raise KeyRequiredException(self, data, 'id', 'id')
        self.id = int(data['id'])
        if 'lastAutoSaveTime' not in data:
            raise KeyRequiredException(self, data, 'lastAutoSaveTime', 'lastAutoSaveTime')
        self.lastAutoSaveTime = int(data['lastAutoSaveTime'])
        if 'secondsPassed' not in data:
            raise KeyRequiredException(self, data, 'secondsPassed', 'secondsPassed')
        self.secondsPassed = int(data['secondsPassed'])
        if 'weather' not in data:
            raise KeyRequiredException(self, data, 'weather', 'weather')
        self.weather = EWeather[data['weather']]
        if 'nextStormTimeInSeconds' not in data:
            raise KeyRequiredException(self, data, 'nextStormTimeInSeconds', 'nextStormTimeInSeconds')
        self.nextStormTimeInSeconds = int(data['nextStormTimeInSeconds'])
        if 'gatheringStormDurationInSeconds' not in data:
            raise KeyRequiredException(self, data, 'gatheringStormDurationInSeconds', 'gatheringStormDurationInSeconds')
        self.gatheringStormDurationInSeconds = int(data['gatheringStormDurationInSeconds'])
        if 'weatherTimeRemainingInSeconds' not in data:
            raise KeyRequiredException(self, data, 'weatherTimeRemainingInSeconds', 'weatherTimeRemainingInSeconds')
        self.weatherTimeRemainingInSeconds = int(data['weatherTimeRemainingInSeconds'])
        if (sf := data.get('date')) is not None:
            self.date = Date()
            self.date.fromDict(data['date'])
        else:
            raise KeyRequiredException(self, data, 'date', 'date')

#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.game.enums.month import EMonth
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawDate']
## from [LT]/src/com/lilithsthrone/game/Game.java: public static void exportGame(String exportFileName, boolean allowOverwrite, boolean isAutoSave) { @ XlY51VAk6J5oRXtpX1P5kFr0uGcEbeit57Nvol/4ZoAAEI91lSBjkQO3PM2BpecHpoWer94eBIo8gYEpqr5P1A==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawDate(Savable):
    TAG = 'date'
    _XMLID_ATTR_DAYOFMONTH_ATTRIBUTE: str = 'dayOfMonth'
    _XMLID_ATTR_HOUR_ATTRIBUTE: str = 'hour'
    _XMLID_ATTR_MINUTE_ATTRIBUTE: str = 'minute'
    _XMLID_ATTR_MONTH_ATTRIBUTE: str = 'month'
    _XMLID_ATTR_YEAR_ATTRIBUTE: str = 'year'

    def __init__(self) -> None:
        super().__init__()
        self.year: int = 0
        self.month: EMonth = EMonth(0)
        self.dayOfMonth: int = 0
        self.hour: int = 0
        self.minute: int = 0

    def overrideAttrs(self, dayOfMonth_attribute: _Optional_str = None, hour_attribute: _Optional_str = None, minute_attribute: _Optional_str = None, month_attribute: _Optional_str = None, year_attribute: _Optional_str = None) -> None:
        if dayOfMonth_attribute is not None:
            self._XMLID_ATTR_DAYOFMONTH_ATTRIBUTE = dayOfMonth_attribute
        if hour_attribute is not None:
            self._XMLID_ATTR_HOUR_ATTRIBUTE = hour_attribute
        if minute_attribute is not None:
            self._XMLID_ATTR_MINUTE_ATTRIBUTE = minute_attribute
        if month_attribute is not None:
            self._XMLID_ATTR_MONTH_ATTRIBUTE = month_attribute
        if year_attribute is not None:
            self._XMLID_ATTR_YEAR_ATTRIBUTE = year_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_YEAR_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_YEAR_ATTRIBUTE, 'year')
        else:
            self.year = int(value)
        value = e.get(self._XMLID_ATTR_MONTH_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_MONTH_ATTRIBUTE, 'month')
        else:
            self.month = EMonth(int(value))
        value = e.get(self._XMLID_ATTR_DAYOFMONTH_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_DAYOFMONTH_ATTRIBUTE, 'dayOfMonth')
        else:
            self.dayOfMonth = int(value)
        value = e.get(self._XMLID_ATTR_HOUR_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_HOUR_ATTRIBUTE, 'hour')
        else:
            self.hour = int(value)
        value = e.get(self._XMLID_ATTR_MINUTE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_MINUTE_ATTRIBUTE, 'minute')
        else:
            self.minute = int(value)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_YEAR_ATTRIBUTE] = str(self.year)
        e.attrib[self._XMLID_ATTR_MONTH_ATTRIBUTE] = str(self.month.value)
        e.attrib[self._XMLID_ATTR_DAYOFMONTH_ATTRIBUTE] = str(self.dayOfMonth)
        e.attrib[self._XMLID_ATTR_HOUR_ATTRIBUTE] = str(self.hour)
        e.attrib[self._XMLID_ATTR_MINUTE_ATTRIBUTE] = str(self.minute)
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['year'] = int(self.year)
        data['month'] = self.month.value
        data['dayOfMonth'] = int(self.dayOfMonth)
        data['hour'] = int(self.hour)
        data['minute'] = int(self.minute)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'year' not in data:
            raise KeyRequiredException(self, data, 'year', 'year')
        self.year = int(data['year'])
        if 'month' not in data:
            raise KeyRequiredException(self, data, 'month', 'month')
        self.month = EMonth(int(data['month']))
        if 'dayOfMonth' not in data:
            raise KeyRequiredException(self, data, 'dayOfMonth', 'dayOfMonth')
        self.dayOfMonth = int(data['dayOfMonth'])
        if 'hour' not in data:
            raise KeyRequiredException(self, data, 'hour', 'hour')
        self.hour = int(data['hour'])
        if 'minute' not in data:
            raise KeyRequiredException(self, data, 'minute', 'minute')
        self.minute = int(data['minute'])

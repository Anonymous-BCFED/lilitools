#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional, Set
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawDialogueFlags']
## from [LT]/src/com/lilithsthrone/game/Game.java: public static void exportGame(String exportFileName, boolean allowOverwrite, boolean isAutoSave) { @ XlY51VAk6J5oRXtpX1P5kFr0uGcEbeit57Nvol/4ZoAAEI91lSBjkQO3PM2BpecHpoWer94eBIo8gYEpqr5P1A==
## from [LT]/src/com/lilithsthrone/game/dialogue/DialogueFlags.java: public Element saveAsXML(Element parentElement, Document doc) { @ k2wXHyqORyara08KDowdhiemlLOc2m2LNmuQgC2njZ/qwvALj/v5zd/KUEJajI4Hs2Y/v4qHMeMHP2emxM7Pvg==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawDialogueFlags(Savable):
    TAG = 'dialogueFlags'
    _XMLID_ATTR_DIALOGUEVALUES_VALUEATTR: str = 'value'
    _XMLID_ATTR_EPONASTAMPS_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_HELENACONVERSATIONTOPICS_VALUEATTR: str = 'value'
    _XMLID_ATTR_HELENASLAVEORDERDAY_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_IMPCITADELIMPWAVE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_MURKCOMPANIONTFSTAGE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_MURKPLAYERTFSTAGE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_NATALYACOLLARCOLOUR_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_NATALYAPOINTS_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_OFFSPRINGDIALOGUETOKENS_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_RALPHDISCOUNT_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_REINDEERENCOUNTEREDIDS_VALUEATTR: str = 'value'
    _XMLID_ATTR_REINDEERFUCKEDIDS_VALUEATTR: str = 'value'
    _XMLID_ATTR_REINDEERWORKEDFORIDS_VALUEATTR: str = 'value'
    _XMLID_ATTR_SADISTNATALYASLAVE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_SAVEDLONGS_KEYATTR: str = 'id'
    _XMLID_ATTR_SCARLETTPRICE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_SLAVERYMANAGERSLAVESELECTED_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_SLAVETRADER_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_WAREHOUSEDEFEATEDIDS_VALUEATTR: str = 'value'
    _XMLID_TAG_DIALOGUEVALUES_CHILD: str = 'dialogueValue'
    _XMLID_TAG_DIALOGUEVALUES_PARENT: str = 'dialogueValues'
    _XMLID_TAG_EPONASTAMPS_ELEMENT: str = 'eponaStamps'
    _XMLID_TAG_HELENACONVERSATIONTOPICS_CHILD: str = 'value'
    _XMLID_TAG_HELENACONVERSATIONTOPICS_PARENT: str = 'helenaConversationTopics'
    _XMLID_TAG_HELENASLAVEORDERDAY_ELEMENT: str = 'helenaSlaveOrderDay'
    _XMLID_TAG_IMPCITADELIMPWAVE_ELEMENT: str = 'impCitadelImpWave'
    _XMLID_TAG_MURKCOMPANIONTFSTAGE_ELEMENT: str = 'murkCompanionTfStage'
    _XMLID_TAG_MURKPLAYERTFSTAGE_ELEMENT: str = 'murkPlayerTfStage'
    _XMLID_TAG_NATALYACOLLARCOLOUR_ELEMENT: str = 'natalyaCollarColour'
    _XMLID_TAG_NATALYAPOINTS_ELEMENT: str = 'natalyaPoints'
    _XMLID_TAG_OFFSPRINGDIALOGUETOKENS_ELEMENT: str = 'offspringDialogueTokens'
    _XMLID_TAG_RALPHDISCOUNT_ELEMENT: str = 'ralphDiscount'
    _XMLID_TAG_REINDEERENCOUNTEREDIDS_CHILD: str = 'value'
    _XMLID_TAG_REINDEERENCOUNTEREDIDS_PARENT: str = 'reindeerEncounteredIDs'
    _XMLID_TAG_REINDEERFUCKEDIDS_CHILD: str = 'value'
    _XMLID_TAG_REINDEERFUCKEDIDS_PARENT: str = 'reindeerFuckedIDs'
    _XMLID_TAG_REINDEERWORKEDFORIDS_CHILD: str = 'value'
    _XMLID_TAG_REINDEERWORKEDFORIDS_PARENT: str = 'reindeerWorkedForIDs'
    _XMLID_TAG_SADISTNATALYASLAVE_ELEMENT: str = 'sadistNatalyaSlave'
    _XMLID_TAG_SAVEDLONGS_CHILD: str = 'save'
    _XMLID_TAG_SAVEDLONGS_PARENT: str = 'savedLongs'
    _XMLID_TAG_SCARLETTPRICE_ELEMENT: str = 'scarlettPrice'
    _XMLID_TAG_SLAVERYMANAGERSLAVESELECTED_ELEMENT: str = 'slaveryManagerSlaveSelected'
    _XMLID_TAG_SLAVETRADER_ELEMENT: str = 'slaveTrader'
    _XMLID_TAG_WAREHOUSEDEFEATEDIDS_CHILD: str = 'value'
    _XMLID_TAG_WAREHOUSEDEFEATEDIDS_PARENT: str = 'warehouseDefeatedIDs'

    def __init__(self) -> None:
        super().__init__()
        self.ralphDiscount: int = 0  # Element
        self.scarlettPrice: int = 0  # Element
        self.eponaStamps: int = 0  # Element
        self.helenaSlaveOrderDay: int = 0  # Element
        self.impCitadelImpWave: int = 0  # Element
        self.murkPlayerTfStage: int = 0  # Element
        self.murkCompanionTfStage: int = 0  # Element
        self.offspringDialogueTokens: int = 0  # Element
        self.slaveTrader: str = ''  # Element
        self.slaveryManagerSlaveSelected: str = ''  # Element
        self.natalyaCollarColour: str = ''  # Element
        self.natalyaPoints: int = 0  # Element
        self.sadistNatalyaSlave: str = ''  # Element
        self.savedLongs: Dict[str, int] = {}
        self.dialogueValues: Set[str] = set()
        self.helenaConversationTopics: Set[str] = set()
        self.reindeerEncounteredIDs: Set[str] = set()
        self.reindeerWorkedForIDs: Set[str] = set()
        self.reindeerFuckedIDs: Set[str] = set()
        self.warehouseDefeatedIDs: Set[str] = set()

    def overrideAttrs(self, dialogueValues_valueattr: _Optional_str = None, eponaStamps_attribute: _Optional_str = None, helenaConversationTopics_valueattr: _Optional_str = None, helenaSlaveOrderDay_attribute: _Optional_str = None, impCitadelImpWave_attribute: _Optional_str = None, murkCompanionTfStage_attribute: _Optional_str = None, murkPlayerTfStage_attribute: _Optional_str = None, natalyaCollarColour_attribute: _Optional_str = None, natalyaPoints_attribute: _Optional_str = None, offspringDialogueTokens_attribute: _Optional_str = None, ralphDiscount_attribute: _Optional_str = None, reindeerEncounteredIDs_valueattr: _Optional_str = None, reindeerFuckedIDs_valueattr: _Optional_str = None, reindeerWorkedForIDs_valueattr: _Optional_str = None, sadistNatalyaSlave_attribute: _Optional_str = None, savedLongs_keyattr: _Optional_str = None, scarlettPrice_attribute: _Optional_str = None, slaveTrader_attribute: _Optional_str = None, slaveryManagerSlaveSelected_attribute: _Optional_str = None, warehouseDefeatedIDs_valueattr: _Optional_str = None) -> None:
        if dialogueValues_valueattr is not None:
            self._XMLID_ATTR_DIALOGUEVALUES_VALUEATTR = dialogueValues_valueattr
        if eponaStamps_attribute is not None:
            self._XMLID_ATTR_EPONASTAMPS_ATTRIBUTE = eponaStamps_attribute
        if helenaConversationTopics_valueattr is not None:
            self._XMLID_ATTR_HELENACONVERSATIONTOPICS_VALUEATTR = helenaConversationTopics_valueattr
        if helenaSlaveOrderDay_attribute is not None:
            self._XMLID_ATTR_HELENASLAVEORDERDAY_ATTRIBUTE = helenaSlaveOrderDay_attribute
        if impCitadelImpWave_attribute is not None:
            self._XMLID_ATTR_IMPCITADELIMPWAVE_ATTRIBUTE = impCitadelImpWave_attribute
        if murkCompanionTfStage_attribute is not None:
            self._XMLID_ATTR_MURKCOMPANIONTFSTAGE_ATTRIBUTE = murkCompanionTfStage_attribute
        if murkPlayerTfStage_attribute is not None:
            self._XMLID_ATTR_MURKPLAYERTFSTAGE_ATTRIBUTE = murkPlayerTfStage_attribute
        if natalyaCollarColour_attribute is not None:
            self._XMLID_ATTR_NATALYACOLLARCOLOUR_ATTRIBUTE = natalyaCollarColour_attribute
        if natalyaPoints_attribute is not None:
            self._XMLID_ATTR_NATALYAPOINTS_ATTRIBUTE = natalyaPoints_attribute
        if offspringDialogueTokens_attribute is not None:
            self._XMLID_ATTR_OFFSPRINGDIALOGUETOKENS_ATTRIBUTE = offspringDialogueTokens_attribute
        if ralphDiscount_attribute is not None:
            self._XMLID_ATTR_RALPHDISCOUNT_ATTRIBUTE = ralphDiscount_attribute
        if reindeerEncounteredIDs_valueattr is not None:
            self._XMLID_ATTR_REINDEERENCOUNTEREDIDS_VALUEATTR = reindeerEncounteredIDs_valueattr
        if reindeerFuckedIDs_valueattr is not None:
            self._XMLID_ATTR_REINDEERFUCKEDIDS_VALUEATTR = reindeerFuckedIDs_valueattr
        if reindeerWorkedForIDs_valueattr is not None:
            self._XMLID_ATTR_REINDEERWORKEDFORIDS_VALUEATTR = reindeerWorkedForIDs_valueattr
        if sadistNatalyaSlave_attribute is not None:
            self._XMLID_ATTR_SADISTNATALYASLAVE_ATTRIBUTE = sadistNatalyaSlave_attribute
        if savedLongs_keyattr is not None:
            self._XMLID_ATTR_SAVEDLONGS_KEYATTR = savedLongs_keyattr
        if scarlettPrice_attribute is not None:
            self._XMLID_ATTR_SCARLETTPRICE_ATTRIBUTE = scarlettPrice_attribute
        if slaveTrader_attribute is not None:
            self._XMLID_ATTR_SLAVETRADER_ATTRIBUTE = slaveTrader_attribute
        if slaveryManagerSlaveSelected_attribute is not None:
            self._XMLID_ATTR_SLAVERYMANAGERSLAVESELECTED_ATTRIBUTE = slaveryManagerSlaveSelected_attribute
        if warehouseDefeatedIDs_valueattr is not None:
            self._XMLID_ATTR_WAREHOUSEDEFEATEDIDS_VALUEATTR = warehouseDefeatedIDs_valueattr

    def overrideTags(self, dialogueValues_child: _Optional_str = None, dialogueValues_parent: _Optional_str = None, eponaStamps_element: _Optional_str = None, helenaConversationTopics_child: _Optional_str = None, helenaConversationTopics_parent: _Optional_str = None, helenaSlaveOrderDay_element: _Optional_str = None, impCitadelImpWave_element: _Optional_str = None, murkCompanionTfStage_element: _Optional_str = None, murkPlayerTfStage_element: _Optional_str = None, natalyaCollarColour_element: _Optional_str = None, natalyaPoints_element: _Optional_str = None, offspringDialogueTokens_element: _Optional_str = None, ralphDiscount_element: _Optional_str = None, reindeerEncounteredIDs_child: _Optional_str = None, reindeerEncounteredIDs_parent: _Optional_str = None, reindeerFuckedIDs_child: _Optional_str = None, reindeerFuckedIDs_parent: _Optional_str = None, reindeerWorkedForIDs_child: _Optional_str = None, reindeerWorkedForIDs_parent: _Optional_str = None, sadistNatalyaSlave_element: _Optional_str = None, savedLongs_child: _Optional_str = None, savedLongs_parent: _Optional_str = None, scarlettPrice_element: _Optional_str = None, slaveTrader_element: _Optional_str = None, slaveryManagerSlaveSelected_element: _Optional_str = None, warehouseDefeatedIDs_child: _Optional_str = None, warehouseDefeatedIDs_parent: _Optional_str = None) -> None:
        if dialogueValues_child is not None:
            self._XMLID_TAG_DIALOGUEVALUES_CHILD = dialogueValues_child
        if dialogueValues_parent is not None:
            self._XMLID_TAG_DIALOGUEVALUES_PARENT = dialogueValues_parent
        if eponaStamps_element is not None:
            self._XMLID_TAG_EPONASTAMPS_ELEMENT = eponaStamps_element
        if helenaConversationTopics_child is not None:
            self._XMLID_TAG_HELENACONVERSATIONTOPICS_CHILD = helenaConversationTopics_child
        if helenaConversationTopics_parent is not None:
            self._XMLID_TAG_HELENACONVERSATIONTOPICS_PARENT = helenaConversationTopics_parent
        if helenaSlaveOrderDay_element is not None:
            self._XMLID_TAG_HELENASLAVEORDERDAY_ELEMENT = helenaSlaveOrderDay_element
        if impCitadelImpWave_element is not None:
            self._XMLID_TAG_IMPCITADELIMPWAVE_ELEMENT = impCitadelImpWave_element
        if murkCompanionTfStage_element is not None:
            self._XMLID_TAG_MURKCOMPANIONTFSTAGE_ELEMENT = murkCompanionTfStage_element
        if murkPlayerTfStage_element is not None:
            self._XMLID_TAG_MURKPLAYERTFSTAGE_ELEMENT = murkPlayerTfStage_element
        if natalyaCollarColour_element is not None:
            self._XMLID_TAG_NATALYACOLLARCOLOUR_ELEMENT = natalyaCollarColour_element
        if natalyaPoints_element is not None:
            self._XMLID_TAG_NATALYAPOINTS_ELEMENT = natalyaPoints_element
        if offspringDialogueTokens_element is not None:
            self._XMLID_TAG_OFFSPRINGDIALOGUETOKENS_ELEMENT = offspringDialogueTokens_element
        if ralphDiscount_element is not None:
            self._XMLID_TAG_RALPHDISCOUNT_ELEMENT = ralphDiscount_element
        if reindeerEncounteredIDs_child is not None:
            self._XMLID_TAG_REINDEERENCOUNTEREDIDS_CHILD = reindeerEncounteredIDs_child
        if reindeerEncounteredIDs_parent is not None:
            self._XMLID_TAG_REINDEERENCOUNTEREDIDS_PARENT = reindeerEncounteredIDs_parent
        if reindeerFuckedIDs_child is not None:
            self._XMLID_TAG_REINDEERFUCKEDIDS_CHILD = reindeerFuckedIDs_child
        if reindeerFuckedIDs_parent is not None:
            self._XMLID_TAG_REINDEERFUCKEDIDS_PARENT = reindeerFuckedIDs_parent
        if reindeerWorkedForIDs_child is not None:
            self._XMLID_TAG_REINDEERWORKEDFORIDS_CHILD = reindeerWorkedForIDs_child
        if reindeerWorkedForIDs_parent is not None:
            self._XMLID_TAG_REINDEERWORKEDFORIDS_PARENT = reindeerWorkedForIDs_parent
        if sadistNatalyaSlave_element is not None:
            self._XMLID_TAG_SADISTNATALYASLAVE_ELEMENT = sadistNatalyaSlave_element
        if savedLongs_child is not None:
            self._XMLID_TAG_SAVEDLONGS_CHILD = savedLongs_child
        if savedLongs_parent is not None:
            self._XMLID_TAG_SAVEDLONGS_PARENT = savedLongs_parent
        if scarlettPrice_element is not None:
            self._XMLID_TAG_SCARLETTPRICE_ELEMENT = scarlettPrice_element
        if slaveTrader_element is not None:
            self._XMLID_TAG_SLAVETRADER_ELEMENT = slaveTrader_element
        if slaveryManagerSlaveSelected_element is not None:
            self._XMLID_TAG_SLAVERYMANAGERSLAVESELECTED_ELEMENT = slaveryManagerSlaveSelected_element
        if warehouseDefeatedIDs_child is not None:
            self._XMLID_TAG_WAREHOUSEDEFEATEDIDS_CHILD = warehouseDefeatedIDs_child
        if warehouseDefeatedIDs_parent is not None:
            self._XMLID_TAG_WAREHOUSEDEFEATEDIDS_PARENT = warehouseDefeatedIDs_parent

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        dc: etree._Element
        lk: str
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_RALPHDISCOUNT_ELEMENT)) is not None:
            if self._XMLID_ATTR_RALPHDISCOUNT_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_RALPHDISCOUNT_ATTRIBUTE, 'ralphDiscount')
            self.ralphDiscount = int(sf.attrib[self._XMLID_ATTR_RALPHDISCOUNT_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'ralphDiscount', self._XMLID_TAG_RALPHDISCOUNT_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_SCARLETTPRICE_ELEMENT)) is not None:
            if self._XMLID_ATTR_SCARLETTPRICE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_SCARLETTPRICE_ATTRIBUTE, 'scarlettPrice')
            self.scarlettPrice = int(sf.attrib[self._XMLID_ATTR_SCARLETTPRICE_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'scarlettPrice', self._XMLID_TAG_SCARLETTPRICE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_EPONASTAMPS_ELEMENT)) is not None:
            if self._XMLID_ATTR_EPONASTAMPS_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_EPONASTAMPS_ATTRIBUTE, 'eponaStamps')
            self.eponaStamps = int(sf.attrib[self._XMLID_ATTR_EPONASTAMPS_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'eponaStamps', self._XMLID_TAG_EPONASTAMPS_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_HELENASLAVEORDERDAY_ELEMENT)) is not None:
            if self._XMLID_ATTR_HELENASLAVEORDERDAY_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_HELENASLAVEORDERDAY_ATTRIBUTE, 'helenaSlaveOrderDay')
            self.helenaSlaveOrderDay = int(sf.attrib[self._XMLID_ATTR_HELENASLAVEORDERDAY_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'helenaSlaveOrderDay', self._XMLID_TAG_HELENASLAVEORDERDAY_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_IMPCITADELIMPWAVE_ELEMENT)) is not None:
            if self._XMLID_ATTR_IMPCITADELIMPWAVE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_IMPCITADELIMPWAVE_ATTRIBUTE, 'impCitadelImpWave')
            self.impCitadelImpWave = int(sf.attrib[self._XMLID_ATTR_IMPCITADELIMPWAVE_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'impCitadelImpWave', self._XMLID_TAG_IMPCITADELIMPWAVE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_MURKPLAYERTFSTAGE_ELEMENT)) is not None:
            if self._XMLID_ATTR_MURKPLAYERTFSTAGE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_MURKPLAYERTFSTAGE_ATTRIBUTE, 'murkPlayerTfStage')
            self.murkPlayerTfStage = int(sf.attrib[self._XMLID_ATTR_MURKPLAYERTFSTAGE_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'murkPlayerTfStage', self._XMLID_TAG_MURKPLAYERTFSTAGE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_MURKCOMPANIONTFSTAGE_ELEMENT)) is not None:
            if self._XMLID_ATTR_MURKCOMPANIONTFSTAGE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_MURKCOMPANIONTFSTAGE_ATTRIBUTE, 'murkCompanionTfStage')
            self.murkCompanionTfStage = int(sf.attrib[self._XMLID_ATTR_MURKCOMPANIONTFSTAGE_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'murkCompanionTfStage', self._XMLID_TAG_MURKCOMPANIONTFSTAGE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_OFFSPRINGDIALOGUETOKENS_ELEMENT)) is not None:
            if self._XMLID_ATTR_OFFSPRINGDIALOGUETOKENS_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_OFFSPRINGDIALOGUETOKENS_ATTRIBUTE, 'offspringDialogueTokens')
            self.offspringDialogueTokens = int(sf.attrib[self._XMLID_ATTR_OFFSPRINGDIALOGUETOKENS_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'offspringDialogueTokens', self._XMLID_TAG_OFFSPRINGDIALOGUETOKENS_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_SLAVETRADER_ELEMENT)) is not None:
            if self._XMLID_ATTR_SLAVETRADER_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_SLAVETRADER_ATTRIBUTE, 'slaveTrader')
            self.slaveTrader = sf.attrib[self._XMLID_ATTR_SLAVETRADER_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'slaveTrader', self._XMLID_TAG_SLAVETRADER_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_SLAVERYMANAGERSLAVESELECTED_ELEMENT)) is not None:
            if self._XMLID_ATTR_SLAVERYMANAGERSLAVESELECTED_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_SLAVERYMANAGERSLAVESELECTED_ATTRIBUTE, 'slaveryManagerSlaveSelected')
            self.slaveryManagerSlaveSelected = sf.attrib[self._XMLID_ATTR_SLAVERYMANAGERSLAVESELECTED_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'slaveryManagerSlaveSelected', self._XMLID_TAG_SLAVERYMANAGERSLAVESELECTED_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_NATALYACOLLARCOLOUR_ELEMENT)) is not None:
            if self._XMLID_ATTR_NATALYACOLLARCOLOUR_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_NATALYACOLLARCOLOUR_ATTRIBUTE, 'natalyaCollarColour')
            self.natalyaCollarColour = sf.attrib[self._XMLID_ATTR_NATALYACOLLARCOLOUR_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'natalyaCollarColour', self._XMLID_TAG_NATALYACOLLARCOLOUR_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_NATALYAPOINTS_ELEMENT)) is not None:
            if self._XMLID_ATTR_NATALYAPOINTS_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_NATALYAPOINTS_ATTRIBUTE, 'natalyaPoints')
            self.natalyaPoints = int(sf.attrib[self._XMLID_ATTR_NATALYAPOINTS_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'natalyaPoints', self._XMLID_TAG_NATALYAPOINTS_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_SADISTNATALYASLAVE_ELEMENT)) is not None:
            if self._XMLID_ATTR_SADISTNATALYASLAVE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_SADISTNATALYASLAVE_ATTRIBUTE, 'sadistNatalyaSlave')
            self.sadistNatalyaSlave = sf.attrib[self._XMLID_ATTR_SADISTNATALYASLAVE_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'sadistNatalyaSlave', self._XMLID_TAG_SADISTNATALYASLAVE_ELEMENT)
        if (df := e.find(self._XMLID_TAG_SAVEDLONGS_PARENT)) is not None:
            for dc in df.iterfind(self._XMLID_TAG_SAVEDLONGS_CHILD):
                self.savedLongs[dc.attrib['id']] = int(dc.text)
        else:
            self.savedLongs = {}
        if (lf := e.find(self._XMLID_TAG_DIALOGUEVALUES_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_DIALOGUEVALUES_CHILD):
                    self.dialogueValues.add(lc.attrib[self._XMLID_ATTR_DIALOGUEVALUES_VALUEATTR])
        else:
            self.dialogueValues = set()
        if (lf := e.find(self._XMLID_TAG_HELENACONVERSATIONTOPICS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_HELENACONVERSATIONTOPICS_CHILD):
                    self.helenaConversationTopics.add(lc.attrib[self._XMLID_ATTR_HELENACONVERSATIONTOPICS_VALUEATTR])
        else:
            self.helenaConversationTopics = set()
        if (lf := e.find(self._XMLID_TAG_REINDEERENCOUNTEREDIDS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_REINDEERENCOUNTEREDIDS_CHILD):
                    self.reindeerEncounteredIDs.add(lc.attrib[self._XMLID_ATTR_REINDEERENCOUNTEREDIDS_VALUEATTR])
        else:
            self.reindeerEncounteredIDs = set()
        if (lf := e.find(self._XMLID_TAG_REINDEERWORKEDFORIDS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_REINDEERWORKEDFORIDS_CHILD):
                    self.reindeerWorkedForIDs.add(lc.attrib[self._XMLID_ATTR_REINDEERWORKEDFORIDS_VALUEATTR])
        else:
            self.reindeerWorkedForIDs = set()
        if (lf := e.find(self._XMLID_TAG_REINDEERFUCKEDIDS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_REINDEERFUCKEDIDS_CHILD):
                    self.reindeerFuckedIDs.add(lc.attrib[self._XMLID_ATTR_REINDEERFUCKEDIDS_VALUEATTR])
        else:
            self.reindeerFuckedIDs = set()
        if (lf := e.find(self._XMLID_TAG_WAREHOUSEDEFEATEDIDS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_WAREHOUSEDEFEATEDIDS_CHILD):
                    self.warehouseDefeatedIDs.add(lc.attrib[self._XMLID_ATTR_WAREHOUSEDEFEATEDIDS_VALUEATTR])
        else:
            self.warehouseDefeatedIDs = set()

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        dK: Any
        dV: Any
        dc: etree._Element
        dp: etree._Element
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        etree.SubElement(e, self._XMLID_TAG_RALPHDISCOUNT_ELEMENT, {self._XMLID_ATTR_RALPHDISCOUNT_ATTRIBUTE: str(self.ralphDiscount)})
        etree.SubElement(e, self._XMLID_TAG_SCARLETTPRICE_ELEMENT, {self._XMLID_ATTR_SCARLETTPRICE_ATTRIBUTE: str(self.scarlettPrice)})
        etree.SubElement(e, self._XMLID_TAG_EPONASTAMPS_ELEMENT, {self._XMLID_ATTR_EPONASTAMPS_ATTRIBUTE: str(self.eponaStamps)})
        etree.SubElement(e, self._XMLID_TAG_HELENASLAVEORDERDAY_ELEMENT, {self._XMLID_ATTR_HELENASLAVEORDERDAY_ATTRIBUTE: str(self.helenaSlaveOrderDay)})
        etree.SubElement(e, self._XMLID_TAG_IMPCITADELIMPWAVE_ELEMENT, {self._XMLID_ATTR_IMPCITADELIMPWAVE_ATTRIBUTE: str(self.impCitadelImpWave)})
        etree.SubElement(e, self._XMLID_TAG_MURKPLAYERTFSTAGE_ELEMENT, {self._XMLID_ATTR_MURKPLAYERTFSTAGE_ATTRIBUTE: str(self.murkPlayerTfStage)})
        etree.SubElement(e, self._XMLID_TAG_MURKCOMPANIONTFSTAGE_ELEMENT, {self._XMLID_ATTR_MURKCOMPANIONTFSTAGE_ATTRIBUTE: str(self.murkCompanionTfStage)})
        etree.SubElement(e, self._XMLID_TAG_OFFSPRINGDIALOGUETOKENS_ELEMENT, {self._XMLID_ATTR_OFFSPRINGDIALOGUETOKENS_ATTRIBUTE: str(self.offspringDialogueTokens)})
        etree.SubElement(e, self._XMLID_TAG_SLAVETRADER_ELEMENT, {self._XMLID_ATTR_SLAVETRADER_ATTRIBUTE: str(self.slaveTrader)})
        etree.SubElement(e, self._XMLID_TAG_SLAVERYMANAGERSLAVESELECTED_ELEMENT, {self._XMLID_ATTR_SLAVERYMANAGERSLAVESELECTED_ATTRIBUTE: str(self.slaveryManagerSlaveSelected)})
        etree.SubElement(e, self._XMLID_TAG_NATALYACOLLARCOLOUR_ELEMENT, {self._XMLID_ATTR_NATALYACOLLARCOLOUR_ATTRIBUTE: str(self.natalyaCollarColour)})
        etree.SubElement(e, self._XMLID_TAG_NATALYAPOINTS_ELEMENT, {self._XMLID_ATTR_NATALYAPOINTS_ATTRIBUTE: str(self.natalyaPoints)})
        etree.SubElement(e, self._XMLID_TAG_SADISTNATALYASLAVE_ELEMENT, {self._XMLID_ATTR_SADISTNATALYASLAVE_ATTRIBUTE: str(self.sadistNatalyaSlave)})
        dp = etree.SubElement(e, self._XMLID_TAG_SAVEDLONGS_PARENT)
        for dK, dV in sorted(self.savedLongs.items(), key=lambda t: t[0]):
            dc = etree.SubElement(dp, self._XMLID_TAG_SAVEDLONGS_CHILD)
            dc.attrib['id'] = dK
            dc.text = str(dV)
        lp = etree.SubElement(e, self._XMLID_TAG_DIALOGUEVALUES_PARENT)
        for lv in sorted(self.dialogueValues, key=lambda x: x):
            etree.SubElement(lp, self._XMLID_TAG_DIALOGUEVALUES_CHILD, {self._XMLID_ATTR_DIALOGUEVALUES_VALUEATTR: lv})
        lp = etree.SubElement(e, self._XMLID_TAG_HELENACONVERSATIONTOPICS_PARENT)
        for lv in sorted(self.helenaConversationTopics, key=lambda x: x):
            etree.SubElement(lp, self._XMLID_TAG_HELENACONVERSATIONTOPICS_CHILD, {self._XMLID_ATTR_HELENACONVERSATIONTOPICS_VALUEATTR: lv})
        lp = etree.SubElement(e, self._XMLID_TAG_REINDEERENCOUNTEREDIDS_PARENT)
        for lv in sorted(self.reindeerEncounteredIDs, key=lambda x: x):
            etree.SubElement(lp, self._XMLID_TAG_REINDEERENCOUNTEREDIDS_CHILD, {self._XMLID_ATTR_REINDEERENCOUNTEREDIDS_VALUEATTR: lv})
        lp = etree.SubElement(e, self._XMLID_TAG_REINDEERWORKEDFORIDS_PARENT)
        for lv in sorted(self.reindeerWorkedForIDs, key=lambda x: x):
            etree.SubElement(lp, self._XMLID_TAG_REINDEERWORKEDFORIDS_CHILD, {self._XMLID_ATTR_REINDEERWORKEDFORIDS_VALUEATTR: lv})
        lp = etree.SubElement(e, self._XMLID_TAG_REINDEERFUCKEDIDS_PARENT)
        for lv in sorted(self.reindeerFuckedIDs, key=lambda x: x):
            etree.SubElement(lp, self._XMLID_TAG_REINDEERFUCKEDIDS_CHILD, {self._XMLID_ATTR_REINDEERFUCKEDIDS_VALUEATTR: lv})
        lp = etree.SubElement(e, self._XMLID_TAG_WAREHOUSEDEFEATEDIDS_PARENT)
        for lv in sorted(self.warehouseDefeatedIDs, key=lambda x: x):
            etree.SubElement(lp, self._XMLID_TAG_WAREHOUSEDEFEATEDIDS_CHILD, {self._XMLID_ATTR_WAREHOUSEDEFEATEDIDS_VALUEATTR: lv})
        return e

    def toDict(self) -> Dict[str, Any]:
        dK: Any
        dV: Any
        data: Dict[str, Any] = super().toDict()
        dp: Dict[str, Any]
        data['ralphDiscount'] = int(self.ralphDiscount)
        data['scarlettPrice'] = int(self.scarlettPrice)
        data['eponaStamps'] = int(self.eponaStamps)
        data['helenaSlaveOrderDay'] = int(self.helenaSlaveOrderDay)
        data['impCitadelImpWave'] = int(self.impCitadelImpWave)
        data['murkPlayerTfStage'] = int(self.murkPlayerTfStage)
        data['murkCompanionTfStage'] = int(self.murkCompanionTfStage)
        data['offspringDialogueTokens'] = int(self.offspringDialogueTokens)
        data['slaveTrader'] = str(self.slaveTrader)
        data['slaveryManagerSlaveSelected'] = str(self.slaveryManagerSlaveSelected)
        data['natalyaCollarColour'] = str(self.natalyaCollarColour)
        data['natalyaPoints'] = int(self.natalyaPoints)
        data['sadistNatalyaSlave'] = str(self.sadistNatalyaSlave)
        dp = {}
        for dK, dV in sorted(self.savedLongs.items(), key=lambda t: t[0]):
            sk = dK
            sv = dV
            dp[sk] = sv
        data['savedLongs'] = dp
        if self.dialogueValues is None or len(self.dialogueValues) > 0:
            data['dialogueValues'] = list()
        else:
            lc = list()
            if len(self.dialogueValues) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['dialogueValues'] = lc
        if self.helenaConversationTopics is None or len(self.helenaConversationTopics) > 0:
            data['helenaConversationTopics'] = list()
        else:
            lc = list()
            if len(self.helenaConversationTopics) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['helenaConversationTopics'] = lc
        if self.reindeerEncounteredIDs is None or len(self.reindeerEncounteredIDs) > 0:
            data['reindeerEncounteredIDs'] = list()
        else:
            lc = list()
            if len(self.reindeerEncounteredIDs) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['reindeerEncounteredIDs'] = lc
        if self.reindeerWorkedForIDs is None or len(self.reindeerWorkedForIDs) > 0:
            data['reindeerWorkedForIDs'] = list()
        else:
            lc = list()
            if len(self.reindeerWorkedForIDs) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['reindeerWorkedForIDs'] = lc
        if self.reindeerFuckedIDs is None or len(self.reindeerFuckedIDs) > 0:
            data['reindeerFuckedIDs'] = list()
        else:
            lc = list()
            if len(self.reindeerFuckedIDs) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['reindeerFuckedIDs'] = lc
        if self.warehouseDefeatedIDs is None or len(self.warehouseDefeatedIDs) > 0:
            data['warehouseDefeatedIDs'] = list()
        else:
            lc = list()
            if len(self.warehouseDefeatedIDs) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['warehouseDefeatedIDs'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('ralphDiscount')) is not None:
            self.ralphDiscount = data['ralphDiscount']
        else:
            raise KeyRequiredException(self, data, 'ralphDiscount', 'ralphDiscount')
        if (sf := data.get('scarlettPrice')) is not None:
            self.scarlettPrice = data['scarlettPrice']
        else:
            raise KeyRequiredException(self, data, 'scarlettPrice', 'scarlettPrice')
        if (sf := data.get('eponaStamps')) is not None:
            self.eponaStamps = data['eponaStamps']
        else:
            raise KeyRequiredException(self, data, 'eponaStamps', 'eponaStamps')
        if (sf := data.get('helenaSlaveOrderDay')) is not None:
            self.helenaSlaveOrderDay = data['helenaSlaveOrderDay']
        else:
            raise KeyRequiredException(self, data, 'helenaSlaveOrderDay', 'helenaSlaveOrderDay')
        if (sf := data.get('impCitadelImpWave')) is not None:
            self.impCitadelImpWave = data['impCitadelImpWave']
        else:
            raise KeyRequiredException(self, data, 'impCitadelImpWave', 'impCitadelImpWave')
        if (sf := data.get('murkPlayerTfStage')) is not None:
            self.murkPlayerTfStage = data['murkPlayerTfStage']
        else:
            raise KeyRequiredException(self, data, 'murkPlayerTfStage', 'murkPlayerTfStage')
        if (sf := data.get('murkCompanionTfStage')) is not None:
            self.murkCompanionTfStage = data['murkCompanionTfStage']
        else:
            raise KeyRequiredException(self, data, 'murkCompanionTfStage', 'murkCompanionTfStage')
        if (sf := data.get('offspringDialogueTokens')) is not None:
            self.offspringDialogueTokens = data['offspringDialogueTokens']
        else:
            raise KeyRequiredException(self, data, 'offspringDialogueTokens', 'offspringDialogueTokens')
        if (sf := data.get('slaveTrader')) is not None:
            self.slaveTrader = data['slaveTrader']
        else:
            raise KeyRequiredException(self, data, 'slaveTrader', 'slaveTrader')
        if (sf := data.get('slaveryManagerSlaveSelected')) is not None:
            self.slaveryManagerSlaveSelected = data['slaveryManagerSlaveSelected']
        else:
            raise KeyRequiredException(self, data, 'slaveryManagerSlaveSelected', 'slaveryManagerSlaveSelected')
        if (sf := data.get('natalyaCollarColour')) is not None:
            self.natalyaCollarColour = data['natalyaCollarColour']
        else:
            raise KeyRequiredException(self, data, 'natalyaCollarColour', 'natalyaCollarColour')
        if (sf := data.get('natalyaPoints')) is not None:
            self.natalyaPoints = data['natalyaPoints']
        else:
            raise KeyRequiredException(self, data, 'natalyaPoints', 'natalyaPoints')
        if (sf := data.get('sadistNatalyaSlave')) is not None:
            self.sadistNatalyaSlave = data['sadistNatalyaSlave']
        else:
            raise KeyRequiredException(self, data, 'sadistNatalyaSlave', 'sadistNatalyaSlave')
        if (df := data.get('savedLongs')) is not None:
            for sk, sv in df.items():
                self.savedLongs[sk] = sv
        else:
            self.savedLongs = {}
        if (lv := self.dialogueValues) is not None:
            for le in lv:
                self.dialogueValues.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'dialogueValues', 'dialogueValues')
        if (lv := self.helenaConversationTopics) is not None:
            for le in lv:
                self.helenaConversationTopics.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'helenaConversationTopics', 'helenaConversationTopics')
        if (lv := self.reindeerEncounteredIDs) is not None:
            for le in lv:
                self.reindeerEncounteredIDs.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'reindeerEncounteredIDs', 'reindeerEncounteredIDs')
        if (lv := self.reindeerWorkedForIDs) is not None:
            for le in lv:
                self.reindeerWorkedForIDs.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'reindeerWorkedForIDs', 'reindeerWorkedForIDs')
        if (lv := self.reindeerFuckedIDs) is not None:
            for le in lv:
                self.reindeerFuckedIDs.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'reindeerFuckedIDs', 'reindeerFuckedIDs')
        if (lv := self.warehouseDefeatedIDs) is not None:
            for le in lv:
                self.warehouseDefeatedIDs.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'warehouseDefeatedIDs', 'warehouseDefeatedIDs')

#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, List, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.items.item_effect import ItemEffect
from lilitools.saves.savable import Savable
__all__ = ['RawEnchantmentRecipe']
## from [LT]/src/com/lilithsthrone/game/dialogue/utils/EnchantmentDialogue.java: public static void saveEnchant(String name, boolean allowOverwrite, DialogueNode dialogueNode) { @ jUK9KqpK88Wnprs2cHD+cOeZ6WvU5l/I63pYIBB4XdsnfU9lyr9eCDpuOfxZIaoHrewesEsSdrpufmm1il5gVA==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawEnchantmentRecipe(Savable):
    TAG = 'enchantment'
    _XMLID_TAG_CLOTHINGTYPE_ELEMENT: str = 'clothingType'
    _XMLID_TAG_ITEMEFFECTS_CHILD: str = 'effect'
    _XMLID_TAG_ITEMEFFECTS_PARENT: str = 'itemEffects'
    _XMLID_TAG_ITEMEFFECTS_VALUEELEM: Optional[str] = None
    _XMLID_TAG_ITEMTYPE_ELEMENT: str = 'itemType'
    _XMLID_TAG_NAME_ELEMENT: str = 'name'
    _XMLID_TAG_TATTOOTYPE_ELEMENT: str = 'tattooType'
    _XMLID_TAG_WEAPONTYPE_ELEMENT: str = 'weaponType'

    def __init__(self) -> None:
        super().__init__()
        self.itemType: Optional[str] = None  # Element
        self.clothingType: Optional[str] = None  # Element
        self.weaponType: Optional[str] = None  # Element
        self.tattooType: Optional[str] = None  # Element
        self.name: str = ''  # Element
        self.itemEffects: List[ItemEffect] = list()

    def overrideTags(self, clothingType_element: _Optional_str = None, itemEffects_child: _Optional_str = None, itemEffects_parent: _Optional_str = None, itemEffects_valueelem: _Optional_str = None, itemType_element: _Optional_str = None, name_element: _Optional_str = None, tattooType_element: _Optional_str = None, weaponType_element: _Optional_str = None) -> None:
        if clothingType_element is not None:
            self._XMLID_TAG_CLOTHINGTYPE_ELEMENT = clothingType_element
        if itemEffects_child is not None:
            self._XMLID_TAG_ITEMEFFECTS_CHILD = itemEffects_child
        if itemEffects_parent is not None:
            self._XMLID_TAG_ITEMEFFECTS_PARENT = itemEffects_parent
        if itemEffects_valueelem is not None:
            self._XMLID_TAG_ITEMEFFECTS_VALUEELEM = itemEffects_valueelem
        if itemType_element is not None:
            self._XMLID_TAG_ITEMTYPE_ELEMENT = itemType_element
        if name_element is not None:
            self._XMLID_TAG_NAME_ELEMENT = name_element
        if tattooType_element is not None:
            self._XMLID_TAG_TATTOOTYPE_ELEMENT = tattooType_element
        if weaponType_element is not None:
            self._XMLID_TAG_WEAPONTYPE_ELEMENT = weaponType_element

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        lv: Savable
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_ITEMTYPE_ELEMENT)) is not None:
            self.itemType = sf.text
        if (sf := e.find(self._XMLID_TAG_CLOTHINGTYPE_ELEMENT)) is not None:
            self.clothingType = sf.text
        if (sf := e.find(self._XMLID_TAG_WEAPONTYPE_ELEMENT)) is not None:
            self.weaponType = sf.text
        if (sf := e.find(self._XMLID_TAG_TATTOOTYPE_ELEMENT)) is not None:
            self.tattooType = sf.text
        if (sf := e.find(self._XMLID_TAG_NAME_ELEMENT)) is not None:
            self.name = sf.text
        else:
            raise ElementRequiredException(self, e, 'name', self._XMLID_TAG_NAME_ELEMENT)
        if (lf := e.find(self._XMLID_TAG_ITEMEFFECTS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_ITEMEFFECTS_CHILD):
                    lv = ItemEffect()
                    lv.fromXML(lc)
                    self.itemEffects.append(lv)
        else:
            self.itemEffects = list()

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        if self.itemType is not None:
            etree.SubElement(e, self._XMLID_TAG_ITEMTYPE_ELEMENT).text = str(self.itemType)
        if self.clothingType is not None:
            etree.SubElement(e, self._XMLID_TAG_CLOTHINGTYPE_ELEMENT).text = str(self.clothingType)
        if self.weaponType is not None:
            etree.SubElement(e, self._XMLID_TAG_WEAPONTYPE_ELEMENT).text = str(self.weaponType)
        if self.tattooType is not None:
            etree.SubElement(e, self._XMLID_TAG_TATTOOTYPE_ELEMENT).text = str(self.tattooType)
        etree.SubElement(e, self._XMLID_TAG_NAME_ELEMENT).text = str(self.name)
        lp = etree.SubElement(e, self._XMLID_TAG_ITEMEFFECTS_PARENT)
        for lv in self.itemEffects:
            lp.append(lv.toXML())
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        if self.itemType is not None:
            data['itemType'] = str(self.itemType)
        if self.clothingType is not None:
            data['clothingType'] = str(self.clothingType)
        if self.weaponType is not None:
            data['weaponType'] = str(self.weaponType)
        if self.tattooType is not None:
            data['tattooType'] = str(self.tattooType)
        data['name'] = str(self.name)
        if self.itemEffects is None or len(self.itemEffects) > 0:
            data['itemEffects'] = list()
        else:
            lc = list()
            if len(self.itemEffects) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['itemEffects'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('itemType')) is not None:
            self.itemType = data['itemType']
        else:
            self.itemType = None
        if (sf := data.get('clothingType')) is not None:
            self.clothingType = data['clothingType']
        else:
            self.clothingType = None
        if (sf := data.get('weaponType')) is not None:
            self.weaponType = data['weaponType']
        else:
            self.weaponType = None
        if (sf := data.get('tattooType')) is not None:
            self.tattooType = data['tattooType']
        else:
            self.tattooType = None
        if (sf := data.get('name')) is not None:
            self.name = data['name']
        else:
            raise KeyRequiredException(self, data, 'name', 'name')
        if (lv := self.itemEffects) is not None:
            for le in lv:
                self.itemEffects.append(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'itemEffects', 'itemEffects')

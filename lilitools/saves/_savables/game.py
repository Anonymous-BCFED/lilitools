#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from collections import OrderedDict, deque
from typing import Any, Deque, Dict, List, Optional, Set
from lxml import etree
from lilitools.saves.character.npc import NPC
from lilitools.saves.character.offspring_seed import OffspringSeed
from lilitools.saves.character.player_character import PlayerCharacter
from lilitools.saves.core_info import CoreInfo
from lilitools.saves.dialogue_flags import DialogueFlags
from lilitools.saves.eventlog.event_log_entry import EventLogEntry
from lilitools.saves.eventlog.slavery_event_log_entry import SlaveryEventLogEntry
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.inventory.character_inventory import CharacterInventory
from lilitools.saves.savable import Savable
from lilitools.saves.slavery.game_slavery_controller import GameSlaveryController
from lilitools.saves.world.world_controller import WorldController
__all__ = ['RawGame']
## from [LT]/src/com/lilithsthrone/game/Game.java: public Element saveAsXML(Element parentElement, Document doc) { @ 1h8c3kTs1ZdA8AoTEH/EovXtdqjWmgkIy6ZmchXyvB2ttDx7NlKbI2U8aZ/0qkPn+IHu+4l6P7DY9TGjtt27tQ==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawGame(Savable):
    TAG = 'game'
    _XMLID_ATTR_COREINFO_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_DIALOGUEFLAGS_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_PLAYERCHARACTER_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_SAVEDINVENTORIES_KEYATTR: str = 'character'
    _XMLID_ATTR_SLAVERY_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_WORLDS_ATTRIBUTE: str = 'value'
    _XMLID_TAG_COREINFO_ELEMENT: str = 'coreInfo'
    _XMLID_TAG_DIALOGUEFLAGS_ELEMENT: str = 'dialogueFlags'
    _XMLID_TAG_EVENTLOG_PARENT: str = 'eventLog'
    _XMLID_TAG_EVENTLOG_VALUEELEM: Optional[str] = None
    _XMLID_TAG_OFFSPRINGSEED_CHILD: str = 'OffspringSeed'
    _XMLID_TAG_OFFSPRINGSEED_VALUEELEM: Optional[str] = None
    _XMLID_TAG_PLAYERCHARACTER_ELEMENT: str = 'playerCharacter'
    _XMLID_TAG_SAVEDINVENTORIES_CHILD: str = 'savedInventory'
    _XMLID_TAG_SAVEDINVENTORIES_PARENT: str = 'savedInventories'
    _XMLID_TAG_SLAVERY_ELEMENT: str = 'slavery'
    _XMLID_TAG_WORLDS_ELEMENT: str = 'maps'

    def __init__(self) -> None:
        super().__init__()
        self.coreInfo: CoreInfo = CoreInfo()  # Element
        self.savedInventories: Dict[str, CharacterInventory] = {}
        self.savedEnforcers: OrderedDict[str, Set[Set[str]]] = OrderedDict()
        self.slavery: GameSlaveryController = GameSlaveryController()  # Element
        self.dialogueFlags: DialogueFlags = DialogueFlags()  # Element
        self.eventLog: Deque[EventLogEntry] = deque(maxlen=50)
        self.slaveryEventLog: OrderedDict[int, List[SlaveryEventLogEntry]] = OrderedDict()
        self.worlds: WorldController = WorldController()  # Element
        self.playerCharacter: PlayerCharacter = PlayerCharacter()  # Element
        self.npcs: List[NPC] = []
        self.offspringSeed: Set[OffspringSeed] = set()

    def overrideAttrs(self, coreInfo_attribute: _Optional_str = None, dialogueFlags_attribute: _Optional_str = None, playerCharacter_attribute: _Optional_str = None, savedInventories_keyattr: _Optional_str = None, slavery_attribute: _Optional_str = None, worlds_attribute: _Optional_str = None) -> None:
        if coreInfo_attribute is not None:
            self._XMLID_ATTR_COREINFO_ATTRIBUTE = coreInfo_attribute
        if dialogueFlags_attribute is not None:
            self._XMLID_ATTR_DIALOGUEFLAGS_ATTRIBUTE = dialogueFlags_attribute
        if playerCharacter_attribute is not None:
            self._XMLID_ATTR_PLAYERCHARACTER_ATTRIBUTE = playerCharacter_attribute
        if savedInventories_keyattr is not None:
            self._XMLID_ATTR_SAVEDINVENTORIES_KEYATTR = savedInventories_keyattr
        if slavery_attribute is not None:
            self._XMLID_ATTR_SLAVERY_ATTRIBUTE = slavery_attribute
        if worlds_attribute is not None:
            self._XMLID_ATTR_WORLDS_ATTRIBUTE = worlds_attribute

    def overrideTags(self, coreInfo_element: _Optional_str = None, dialogueFlags_element: _Optional_str = None, eventLog_parent: _Optional_str = None, eventLog_valueelem: _Optional_str = None, offspringSeed_child: _Optional_str = None, offspringSeed_valueelem: _Optional_str = None, playerCharacter_element: _Optional_str = None, savedInventories_child: _Optional_str = None, savedInventories_parent: _Optional_str = None, slavery_element: _Optional_str = None, worlds_element: _Optional_str = None) -> None:
        if coreInfo_element is not None:
            self._XMLID_TAG_COREINFO_ELEMENT = coreInfo_element
        if dialogueFlags_element is not None:
            self._XMLID_TAG_DIALOGUEFLAGS_ELEMENT = dialogueFlags_element
        if eventLog_parent is not None:
            self._XMLID_TAG_EVENTLOG_PARENT = eventLog_parent
        if eventLog_valueelem is not None:
            self._XMLID_TAG_EVENTLOG_VALUEELEM = eventLog_valueelem
        if offspringSeed_child is not None:
            self._XMLID_TAG_OFFSPRINGSEED_CHILD = offspringSeed_child
        if offspringSeed_valueelem is not None:
            self._XMLID_TAG_OFFSPRINGSEED_VALUEELEM = offspringSeed_valueelem
        if playerCharacter_element is not None:
            self._XMLID_TAG_PLAYERCHARACTER_ELEMENT = playerCharacter_element
        if savedInventories_child is not None:
            self._XMLID_TAG_SAVEDINVENTORIES_CHILD = savedInventories_child
        if savedInventories_parent is not None:
            self._XMLID_TAG_SAVEDINVENTORIES_PARENT = savedInventories_parent
        if slavery_element is not None:
            self._XMLID_TAG_SLAVERY_ELEMENT = slavery_element
        if worlds_element is not None:
            self._XMLID_TAG_WORLDS_ELEMENT = worlds_element

    def _fromXML_savedEnforcers(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromXML_savedEnforcers()')

    def _toXML_savedEnforcers(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toXML_savedEnforcers()')

    def _fromDict_savedEnforcers(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromDict_savedEnforcers()')

    def _toDict_savedEnforcers(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toDict_savedEnforcers()')

    def _fromXML_slaveryEventLog(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromXML_slaveryEventLog()')

    def _toXML_slaveryEventLog(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toXML_slaveryEventLog()')

    def _fromDict_slaveryEventLog(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromDict_slaveryEventLog()')

    def _toDict_slaveryEventLog(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toDict_slaveryEventLog()')

    def _fromXML_npcs(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromXML_npcs()')

    def _toXML_npcs(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toXML_npcs()')

    def _fromDict_npcs(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromDict_npcs()')

    def _toDict_npcs(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toDict_npcs()')

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        dc: etree._Element
        dv: Savable
        lk: str
        lv: Savable
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_COREINFO_ELEMENT)) is not None:
            self.coreInfo = CoreInfo()
            self.coreInfo.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'coreInfo', self._XMLID_TAG_COREINFO_ELEMENT)
        if (df := e.find(self._XMLID_TAG_SAVEDINVENTORIES_PARENT)) is not None:
            for dc in df.iterfind(self._XMLID_TAG_SAVEDINVENTORIES_CHILD):
                dv = CharacterInventory()
                dv.fromXML(dc)
                self.savedInventories[dc.attrib['character']] = dv
        else:
            self.savedInventories = {}
        self._fromXML_savedEnforcers(e)
        if (sf := e.find(self._XMLID_TAG_SLAVERY_ELEMENT)) is not None:
            self.slavery = GameSlaveryController()
            self.slavery.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'slavery', self._XMLID_TAG_SLAVERY_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_DIALOGUEFLAGS_ELEMENT)) is not None:
            self.dialogueFlags = DialogueFlags()
            self.dialogueFlags.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'dialogueFlags', self._XMLID_TAG_DIALOGUEFLAGS_ELEMENT)
        if (lf := e.find(self._XMLID_TAG_EVENTLOG_PARENT)) is not None:
            if lf is not None:
                for lc in lf:
                    lv = EventLogEntry()
                    lv.fromXML(lc)
                    self.eventLog.append(lv)
        else:
            self.eventLog = deque()
        self._fromXML_slaveryEventLog(e)
        if (sf := e.find(self._XMLID_TAG_WORLDS_ELEMENT)) is not None:
            self.worlds = WorldController()
            self.worlds.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'worlds', self._XMLID_TAG_WORLDS_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_PLAYERCHARACTER_ELEMENT)) is not None:
            self.playerCharacter = PlayerCharacter()
            self.playerCharacter.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'playerCharacter', self._XMLID_TAG_PLAYERCHARACTER_ELEMENT)
        self._fromXML_npcs(e)
        for lc in e.iterfind(self._XMLID_TAG_OFFSPRINGSEED_CHILD):
            lv = OffspringSeed()
            lv.fromXML(lc)
            self.offspringSeed.add(lv)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        dK: Any
        dV: Any
        dc: etree._Element
        dp: etree._Element
        dsc: etree._Element
        dv: Savable
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        e.append(self.coreInfo.toXML(self._XMLID_TAG_COREINFO_ELEMENT))
        dp = etree.SubElement(e, self._XMLID_TAG_SAVEDINVENTORIES_PARENT)
        for dK, dV in sorted(self.savedInventories.items(), key=lambda t: t[0]):
            dc = etree.SubElement(dp, self._XMLID_TAG_SAVEDINVENTORIES_CHILD)
            dc.attrib['character'] = dK
            dc.append(dV.toXML(self._XMLID_TAG_SAVEDINVENTORIES_VALUEELEM))
        self._toXML_savedEnforcers(e)
        e.append(self.slavery.toXML(self._XMLID_TAG_SLAVERY_ELEMENT))
        e.append(self.dialogueFlags.toXML(self._XMLID_TAG_DIALOGUEFLAGS_ELEMENT))
        lp = etree.SubElement(e, self._XMLID_TAG_EVENTLOG_PARENT)
        for lv in self.eventLog:
            lp.append(lv.toXML())
        self._toXML_slaveryEventLog(e)
        e.append(self.worlds.toXML(self._XMLID_TAG_WORLDS_ELEMENT))
        e.append(self.playerCharacter.toXML(self._XMLID_TAG_PLAYERCHARACTER_ELEMENT))
        self._toXML_npcs(e)
        for lv in sorted(self.offspringSeed, key=lambda x: x.data.id):
            e.append(lv.toXML())
        return e

    def toDict(self) -> Dict[str, Any]:
        dK: Any
        dV: Any
        data: Dict[str, Any] = super().toDict()
        dp: Dict[str, Any]
        dsc: Dict[str, Any]
        dv: Savable
        data['coreInfo'] = self.coreInfo.toDict()
        dp = {}
        for dK, dV in sorted(self.savedInventories.items(), key=lambda t: t[0]):
            sk = dK
            dp[sk] = dV.toDict()
        data['savedInventories'] = dp
        self._toDict_savedEnforcers(data)
        data['slavery'] = self.slavery.toDict()
        data['dialogueFlags'] = self.dialogueFlags.toDict()
        if self.eventLog is None or len(self.eventLog) > 0:
            data['eventLog'] = list()
        else:
            lc = list()
            if len(self.eventLog) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['eventLog'] = lc
        self._toDict_slaveryEventLog(data)
        data['worlds'] = self.worlds.toDict()
        data['playerCharacter'] = self.playerCharacter.toDict()
        self._toDict_npcs(data)
        if self.offspringSeed is None or len(self.offspringSeed) > 0:
            data['offspringSeed'] = list()
        else:
            lc = list()
            if len(self.offspringSeed) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['offspringSeed'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('coreInfo')) is not None:
            self.coreInfo = CoreInfo()
            self.coreInfo.fromDict(data['coreInfo'])
        else:
            raise KeyRequiredException(self, data, 'coreInfo', 'coreInfo')
        if (df := data.get('savedInventories')) is not None:
            for sk, sv in df.items():
                dV = CharacterInventory()
                dV.fromDict(sv)
                self.savedInventories[sk] = dv
        else:
            self.savedInventories = {}
        self._fromDict_savedEnforcers(data)
        if (sf := data.get('slavery')) is not None:
            self.slavery = GameSlaveryController()
            self.slavery.fromDict(data['slavery'])
        else:
            raise KeyRequiredException(self, data, 'slavery', 'slavery')
        if (sf := data.get('dialogueFlags')) is not None:
            self.dialogueFlags = DialogueFlags()
            self.dialogueFlags.fromDict(data['dialogueFlags'])
        else:
            raise KeyRequiredException(self, data, 'dialogueFlags', 'dialogueFlags')
        if (lv := self.eventLog) is not None:
            for le in lv:
                self.eventLog.append(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'eventLog', 'eventLog')
        self._fromDict_slaveryEventLog(data)
        if (sf := data.get('worlds')) is not None:
            self.worlds = WorldController()
            self.worlds.fromDict(data['worlds'])
        else:
            raise KeyRequiredException(self, data, 'worlds', 'worlds')
        if (sf := data.get('playerCharacter')) is not None:
            self.playerCharacter = PlayerCharacter()
            self.playerCharacter.fromDict(data['playerCharacter'])
        else:
            raise KeyRequiredException(self, data, 'playerCharacter', 'playerCharacter')
        self._fromDict_npcs(data)
        if (lv := self.offspringSeed) is not None:
            for le in lv:
                self.offspringSeed.add(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'offspringSeed', 'offspringSeed')

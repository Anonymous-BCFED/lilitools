#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL, Savable
__all__ = ['RawActiveStatusEffect']
## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public Element saveAsXML(Element parentElement, Document doc) { @ Ra1rfw7eKd5WT2JXoGa/zLFvb/XgZlpPNZbY2BZwgdTA/t0DkASy5ADAtBbX2DvSTxI43cQAGTPbPsMVcAHyRQ==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawActiveStatusEffect(Savable):
    TAG = 'statusEffect'
    _XMLID_ATTR_FORCELOAD_ATTRIBUTE: str = 'fl'
    _XMLID_ATTR_LASTTIMEAPPLIED_ATTRIBUTE: str = 'lta'
    _XMLID_ATTR_SECONDSPASSED_ATTRIBUTE: str = 'sp'
    _XMLID_ATTR_SECONDSREMAINING_ATTRIBUTE: str = 'sr'
    _XMLID_ATTR_TYPE_ATTRIBUTE: str = 'type'

    def __init__(self) -> None:
        super().__init__()
        self.type: str = ''
        self.secondsRemaining: int = 0
        self.secondsPassed: int = 0
        self.lastTimeApplied: int = 0
        self.forceLoad: bool = False

    def overrideAttrs(self, forceLoad_attribute: _Optional_str = None, lastTimeApplied_attribute: _Optional_str = None, secondsPassed_attribute: _Optional_str = None, secondsRemaining_attribute: _Optional_str = None, type_attribute: _Optional_str = None) -> None:
        if forceLoad_attribute is not None:
            self._XMLID_ATTR_FORCELOAD_ATTRIBUTE = forceLoad_attribute
        if lastTimeApplied_attribute is not None:
            self._XMLID_ATTR_LASTTIMEAPPLIED_ATTRIBUTE = lastTimeApplied_attribute
        if secondsPassed_attribute is not None:
            self._XMLID_ATTR_SECONDSPASSED_ATTRIBUTE = secondsPassed_attribute
        if secondsRemaining_attribute is not None:
            self._XMLID_ATTR_SECONDSREMAINING_ATTRIBUTE = secondsRemaining_attribute
        if type_attribute is not None:
            self._XMLID_ATTR_TYPE_ATTRIBUTE = type_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_TYPE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_TYPE_ATTRIBUTE, 'type')
        else:
            self.type = str(value)
        value = e.get(self._XMLID_ATTR_SECONDSREMAINING_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_SECONDSREMAINING_ATTRIBUTE, 'secondsRemaining')
        else:
            self.secondsRemaining = int(value)
        value = e.get(self._XMLID_ATTR_SECONDSPASSED_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_SECONDSPASSED_ATTRIBUTE, 'secondsPassed')
        else:
            self.secondsPassed = int(value)
        value = e.get(self._XMLID_ATTR_LASTTIMEAPPLIED_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_LASTTIMEAPPLIED_ATTRIBUTE, 'lastTimeApplied')
        else:
            self.lastTimeApplied = int(value)
        value = e.get(self._XMLID_ATTR_FORCELOAD_ATTRIBUTE, None)
        if value is None:
            self.forceLoad = False
        else:
            self.forceLoad = XML2BOOL[value]

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_TYPE_ATTRIBUTE] = str(self.type)
        e.attrib[self._XMLID_ATTR_SECONDSREMAINING_ATTRIBUTE] = str(self.secondsRemaining)
        e.attrib[self._XMLID_ATTR_SECONDSPASSED_ATTRIBUTE] = str(self.secondsPassed)
        e.attrib[self._XMLID_ATTR_LASTTIMEAPPLIED_ATTRIBUTE] = str(self.lastTimeApplied)
        if self.forceLoad != False:
            e.attrib[self._XMLID_ATTR_FORCELOAD_ATTRIBUTE] = str(self.forceLoad).lower()
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['type'] = self.type
        data['secondsRemaining'] = int(self.secondsRemaining)
        data['secondsPassed'] = int(self.secondsPassed)
        data['lastTimeApplied'] = int(self.lastTimeApplied)
        if self.forceLoad != False:
            data['forceLoad'] = bool(self.forceLoad)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'type' not in data:
            raise KeyRequiredException(self, data, 'type', 'type')
        self.type = str(data['type'])
        if 'secondsRemaining' not in data:
            raise KeyRequiredException(self, data, 'secondsRemaining', 'secondsRemaining')
        self.secondsRemaining = int(data['secondsRemaining'])
        if 'secondsPassed' not in data:
            raise KeyRequiredException(self, data, 'secondsPassed', 'secondsPassed')
        self.secondsPassed = int(data['secondsPassed'])
        if 'lastTimeApplied' not in data:
            raise KeyRequiredException(self, data, 'lastTimeApplied', 'lastTimeApplied')
        self.lastTimeApplied = int(data['lastTimeApplied'])
        self.forceLoad = bool(data['forceLoad']) if 'forceLoad' in data else None

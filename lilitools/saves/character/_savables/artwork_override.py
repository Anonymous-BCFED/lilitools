#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.savable import Savable
__all__ = ['RawArtworkOverride']
## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public Element saveAsXML(Element parentElement, Document doc) { @ Ra1rfw7eKd5WT2JXoGa/zLFvb/XgZlpPNZbY2BZwgdTA/t0DkASy5ADAtBbX2DvSTxI43cQAGTPbPsMVcAHyRQ==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawArtworkOverride(Savable):
    TAG = 'artwork'
    _XMLID_ATTR_OVERRIDEARTIST_ATTRIBUTE: str = 'index'
    _XMLID_ATTR_OVERRIDEIMAGE_ATTRIBUTE: str = 'index'
    _XMLID_TAG_OVERRIDEARTIST_ELEMENT: str = 'overrideArtist'
    _XMLID_TAG_OVERRIDEIMAGE_ELEMENT: str = 'overrideImage'

    def __init__(self) -> None:
        super().__init__()
        self.overrideArtist: Optional[int] = None  # Element
        self.overrideImage: Optional[int] = None  # Element

    def overrideAttrs(self, overrideArtist_attribute: _Optional_str = None, overrideImage_attribute: _Optional_str = None) -> None:
        if overrideArtist_attribute is not None:
            self._XMLID_ATTR_OVERRIDEARTIST_ATTRIBUTE = overrideArtist_attribute
        if overrideImage_attribute is not None:
            self._XMLID_ATTR_OVERRIDEIMAGE_ATTRIBUTE = overrideImage_attribute

    def overrideTags(self, overrideArtist_element: _Optional_str = None, overrideImage_element: _Optional_str = None) -> None:
        if overrideArtist_element is not None:
            self._XMLID_TAG_OVERRIDEARTIST_ELEMENT = overrideArtist_element
        if overrideImage_element is not None:
            self._XMLID_TAG_OVERRIDEIMAGE_ELEMENT = overrideImage_element

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_OVERRIDEARTIST_ELEMENT)) is not None:
            self.overrideArtist = int(sf.attrib.get(self._XMLID_ATTR_OVERRIDEARTIST_ATTRIBUTE, "None"))
        if (sf := e.find(self._XMLID_TAG_OVERRIDEIMAGE_ELEMENT)) is not None:
            self.overrideImage = int(sf.attrib.get(self._XMLID_ATTR_OVERRIDEIMAGE_ATTRIBUTE, "None"))

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        if self.overrideArtist is not None:
            etree.SubElement(e, self._XMLID_TAG_OVERRIDEARTIST_ELEMENT, {self._XMLID_ATTR_OVERRIDEARTIST_ATTRIBUTE: str(self.overrideArtist)})
        if self.overrideImage is not None:
            etree.SubElement(e, self._XMLID_TAG_OVERRIDEIMAGE_ELEMENT, {self._XMLID_ATTR_OVERRIDEIMAGE_ATTRIBUTE: str(self.overrideImage)})
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        if self.overrideArtist is not None:
            data['overrideArtist'] = int(self.overrideArtist)
        if self.overrideImage is not None:
            data['overrideImage'] = int(self.overrideImage)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('overrideArtist')) is not None:
            self.overrideArtist = data['overrideArtist']
        else:
            self.overrideArtist = None
        if (sf := data.get('overrideImage')) is not None:
            self.overrideImage = data['overrideImage']
        else:
            self.overrideImage = None

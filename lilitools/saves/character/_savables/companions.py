#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional, Set
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawCompanions']
## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public Element saveAsXML(Element parentElement, Document doc) { @ Ra1rfw7eKd5WT2JXoGa/zLFvb/XgZlpPNZbY2BZwgdTA/t0DkASy5ADAtBbX2DvSTxI43cQAGTPbPsMVcAHyRQ==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawCompanions(Savable):
    TAG = 'companions'
    _XMLID_ATTR_FOLLOWING_VALUEATTR: str = 'id'
    _XMLID_ATTR_MAXCOMPANIONS_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_PARTYLEADER_ATTRIBUTE: str = 'value'
    _XMLID_TAG_FOLLOWING_CHILD: str = 'companion'
    _XMLID_TAG_FOLLOWING_PARENT: str = 'companionsFollowing'
    _XMLID_TAG_MAXCOMPANIONS_ELEMENT: str = 'maxCompanions'
    _XMLID_TAG_PARTYLEADER_ELEMENT: str = 'partyLeader'

    def __init__(self) -> None:
        super().__init__()
        self.following: Set[str] = set()
        self.partyLeader: Optional[str] = None  # Element
        self.maxCompanions: int = 0  # Element

    def overrideAttrs(self, following_valueattr: _Optional_str = None, maxCompanions_attribute: _Optional_str = None, partyLeader_attribute: _Optional_str = None) -> None:
        if following_valueattr is not None:
            self._XMLID_ATTR_FOLLOWING_VALUEATTR = following_valueattr
        if maxCompanions_attribute is not None:
            self._XMLID_ATTR_MAXCOMPANIONS_ATTRIBUTE = maxCompanions_attribute
        if partyLeader_attribute is not None:
            self._XMLID_ATTR_PARTYLEADER_ATTRIBUTE = partyLeader_attribute

    def overrideTags(self, following_child: _Optional_str = None, following_parent: _Optional_str = None, maxCompanions_element: _Optional_str = None, partyLeader_element: _Optional_str = None) -> None:
        if following_child is not None:
            self._XMLID_TAG_FOLLOWING_CHILD = following_child
        if following_parent is not None:
            self._XMLID_TAG_FOLLOWING_PARENT = following_parent
        if maxCompanions_element is not None:
            self._XMLID_TAG_MAXCOMPANIONS_ELEMENT = maxCompanions_element
        if partyLeader_element is not None:
            self._XMLID_TAG_PARTYLEADER_ELEMENT = partyLeader_element

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        sf: etree._Element
        if (lf := e.find(self._XMLID_TAG_FOLLOWING_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_FOLLOWING_CHILD):
                    self.following.add(lc.attrib[self._XMLID_ATTR_FOLLOWING_VALUEATTR])
        else:
            self.following = set()
        if (sf := e.find(self._XMLID_TAG_PARTYLEADER_ELEMENT)) is not None:
            if self._XMLID_ATTR_PARTYLEADER_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_PARTYLEADER_ATTRIBUTE, 'partyLeader')
            if sf.attrib[self._XMLID_ATTR_PARTYLEADER_ATTRIBUTE] == '':
                self.partyLeader = None
            else:
                self.partyLeader = sf.attrib[self._XMLID_ATTR_PARTYLEADER_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'partyLeader', self._XMLID_TAG_PARTYLEADER_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_MAXCOMPANIONS_ELEMENT)) is not None:
            if self._XMLID_ATTR_MAXCOMPANIONS_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_MAXCOMPANIONS_ATTRIBUTE, 'maxCompanions')
            self.maxCompanions = int(sf.attrib[self._XMLID_ATTR_MAXCOMPANIONS_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'maxCompanions', self._XMLID_TAG_MAXCOMPANIONS_ELEMENT)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        lp = etree.SubElement(e, self._XMLID_TAG_FOLLOWING_PARENT)
        for lv in sorted(self.following, key=str):
            etree.SubElement(lp, self._XMLID_TAG_FOLLOWING_CHILD, {self._XMLID_ATTR_FOLLOWING_VALUEATTR: lv})
        if self.partyLeader is not None:
            etree.SubElement(e, self._XMLID_TAG_PARTYLEADER_ELEMENT, {self._XMLID_ATTR_PARTYLEADER_ATTRIBUTE: str(self.partyLeader)})
        else:
            etree.SubElement(e, self._XMLID_TAG_PARTYLEADER_ELEMENT, {self._XMLID_ATTR_PARTYLEADER_ATTRIBUTE: ''})
        etree.SubElement(e, self._XMLID_TAG_MAXCOMPANIONS_ELEMENT, {self._XMLID_ATTR_MAXCOMPANIONS_ATTRIBUTE: str(self.maxCompanions)})
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        if self.following is None or len(self.following) > 0:
            data['following'] = list()
        else:
            lc = list()
            if len(self.following) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['following'] = lc
        data['partyLeader'] = str(self.partyLeader)
        data['maxCompanions'] = int(self.maxCompanions)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (lv := self.following) is not None:
            for le in lv:
                self.following.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'following', 'following')
        if (sf := data.get('partyLeader')) is not None:
            self.partyLeader = data['partyLeader']
        else:
            raise KeyRequiredException(self, data, 'partyLeader', 'partyLeader')
        if (sf := data.get('maxCompanions')) is not None:
            self.maxCompanions = data['maxCompanions']
        else:
            raise KeyRequiredException(self, data, 'maxCompanions', 'maxCompanions')

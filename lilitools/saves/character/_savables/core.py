#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from collections import OrderedDict
from typing import Any, Dict, Optional, Set
from lxml import etree
from lilitools.game.enums.month import EMonth
from lilitools.saves.character.character_names import CharacterNames
from lilitools.saves.character.enums.sexual_orientation import ESexualOrientation
from lilitools.saves.character.gender import EGender
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL, Savable
__all__ = ['RawCore']
## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public Element saveAsXML(Element parentElement, Document doc) { @ Ra1rfw7eKd5WT2JXoGa/zLFvb/XgZlpPNZbY2BZwgdTA/t0DkASy5ADAtBbX2DvSTxI43cQAGTPbPsMVcAHyRQ==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawCore(Savable):
    TAG = 'core'
    _XMLID_ATTR_AGEAPPEARANCEABSOLUTE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_AGEAPPEARANCEDIFFERENCE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_CAPTIVE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_COMBATBEHAVIOUR_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_DAYOFBIRTH_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_DESCRIPTION_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_ELEMENTALSUMMONED_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_ELEMENTAL_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_EXPERIENCE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_FOUGHTPLAYERCOUNT_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_GENDERIDENTITY_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_GENERICNAME_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_HEALTH_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_HISTORY_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_ID_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_LASTTIMEHADSEX_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_LASTTIMEORGASMED_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_LEVEL_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_LOSTCOMBATCOUNT_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_MANA_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_MONTHOFBIRTH_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_MUSKMARKER_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_NAME_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_OBEDIENCE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_PARSERTARGET_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_PATHNAME_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_PERKCATEGORYPOINTS_KEYATTR: str = 'category'
    _XMLID_ATTR_PERKPOINTS_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_PETNAMES_KEYATTR: str = 'id'
    _XMLID_ATTR_PETNAMES_VALUEATTR: str = 'petName'
    _XMLID_ATTR_PLAYERKNOWSNAME_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_PLAYERONFIRSTNAMETERMS_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_RACECONCEALED_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_SEXUALORIENTATION_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_SPEECHCOLOUR_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_SURNAME_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_VERSION_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_WONCOMBATCOUNT_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_YEAROFBIRTH_ATTRIBUTE: str = 'value'
    _XMLID_TAG_AGEAPPEARANCEABSOLUTE_ELEMENT: str = 'ageAppearanceAbsolute'
    _XMLID_TAG_AGEAPPEARANCEDIFFERENCE_ELEMENT: str = 'ageAppearanceDifference'
    _XMLID_TAG_CAPTIVE_ELEMENT: str = 'captive'
    _XMLID_TAG_COMBATBEHAVIOUR_ELEMENT: str = 'combatBehaviour'
    _XMLID_TAG_CREAMPIERETENTIONAREAS_CHILD: str = 'area'
    _XMLID_TAG_CREAMPIERETENTIONAREAS_PARENT: str = 'creampieRetentionAreas'
    _XMLID_TAG_DAYOFBIRTH_ELEMENT: str = 'dayOfBirth'
    _XMLID_TAG_DESCRIPTION_ELEMENT: str = 'description'
    _XMLID_TAG_DESIREDJOBS_CHILD: str = 'job'
    _XMLID_TAG_DESIREDJOBS_PARENT: str = 'desiredJobs'
    _XMLID_TAG_ELEMENTALSUMMONED_ELEMENT: str = 'elementalSummoned'
    _XMLID_TAG_ELEMENTAL_ELEMENT: str = 'elemental'
    _XMLID_TAG_EXPERIENCE_ELEMENT: str = 'experience'
    _XMLID_TAG_FOUGHTPLAYERCOUNT_ELEMENT: str = 'foughtPlayerCount'
    _XMLID_TAG_GENDERIDENTITY_ELEMENT: str = 'genderIdentity'
    _XMLID_TAG_GENERICNAME_ELEMENT: str = 'genericName'
    _XMLID_TAG_HEALTH_ELEMENT: str = 'health'
    _XMLID_TAG_HISTORY_ELEMENT: str = 'history'
    _XMLID_TAG_ID_ELEMENT: str = 'id'
    _XMLID_TAG_LASTTIMEHADSEX_ELEMENT: str = 'lastTimeHadSex'
    _XMLID_TAG_LASTTIMEORGASMED_ELEMENT: str = 'lastTimeOrgasmed'
    _XMLID_TAG_LEVEL_ELEMENT: str = 'level'
    _XMLID_TAG_LOSTCOMBATCOUNT_ELEMENT: str = 'lostCombatCount'
    _XMLID_TAG_MANA_ELEMENT: str = 'mana'
    _XMLID_TAG_MONTHOFBIRTH_ELEMENT: str = 'monthOfBirth'
    _XMLID_TAG_MUSKMARKER_ELEMENT: str = 'muskMarker'
    _XMLID_TAG_NAME_ELEMENT: str = 'name'
    _XMLID_TAG_OBEDIENCE_ELEMENT: str = 'obedience'
    _XMLID_TAG_PARSERTARGET_ELEMENT: str = 'parserTarget'
    _XMLID_TAG_PATHNAME_ELEMENT: str = 'pathName'
    _XMLID_TAG_PERKCATEGORYPOINTS_CHILD: str = 'points'
    _XMLID_TAG_PERKCATEGORYPOINTS_PARENT: str = 'perkCategoryPoints'
    _XMLID_TAG_PERKPOINTS_ELEMENT: str = 'perkPoints'
    _XMLID_TAG_PERSONALITY_CHILD: str = 'trait'
    _XMLID_TAG_PERSONALITY_PARENT: str = 'personality'
    _XMLID_TAG_PETNAMES_CHILD: str = 'petNameEntry'
    _XMLID_TAG_PETNAMES_PARENT: str = 'petNames'
    _XMLID_TAG_PLAYERKNOWSNAME_ELEMENT: str = 'playerKnowsName'
    _XMLID_TAG_PLAYERONFIRSTNAMETERMS_ELEMENT: str = 'playerOnFirstNameTerms'
    _XMLID_TAG_RACECONCEALED_ELEMENT: str = 'raceConcealed'
    _XMLID_TAG_SEXUALORIENTATION_ELEMENT: str = 'sexualOrientation'
    _XMLID_TAG_SPEECHCOLOUR_ELEMENT: str = 'speechColour'
    _XMLID_TAG_SURNAME_ELEMENT: str = 'surname'
    _XMLID_TAG_VERSION_ELEMENT: str = 'version'
    _XMLID_TAG_WONCOMBATCOUNT_ELEMENT: str = 'wonCombatCount'
    _XMLID_TAG_YEAROFBIRTH_ELEMENT: str = 'yearOfBirth'

    def __init__(self) -> None:
        super().__init__()
        self.id: str = ''  # Element
        self.parserTarget: Optional[str] = None  # Element
        self.pathName: str = ''  # Element
        self.name: CharacterNames = CharacterNames()  # Element
        self.surname: str = ''  # Element
        self.genericName: str = ''  # Element
        self.description: str = ''  # Element
        self.playerKnowsName: bool = False  # Element
        self.playerOnFirstNameTerms: bool = False  # Element
        self.raceConcealed: bool = False  # Element
        self.captive: bool = False  # Element
        self.level: int = 0  # Element
        self.ageAppearanceDifference: int = 0  # Element
        self.ageAppearanceAbsolute: Optional[int] = None  # Element
        self.yearOfBirth: int = 0  # Element
        self.monthOfBirth: EMonth = EMonth.JANUARY  # Element
        self.dayOfBirth: int = 0  # Element
        self.version: str = ''  # Element
        self.history: str = ''  # Element
        self.elemental: str = ''  # Element
        self.elementalSummoned: bool = False  # Element
        self.combatBehaviour: str = ''  # Element
        self.lastTimeHadSex: int = 0  # Element
        self.lastTimeOrgasmed: int = 0  # Element
        self.muskMarker: Optional[str] = None  # Element
        self.speechColour: Optional[str] = None  # Element
        self.desiredJobs: Set[str] = set()
        self.petNames: Dict[str, str] = {}
        self.personality: Set[str] = set()
        self.sexualOrientation: ESexualOrientation = ESexualOrientation(0)  # Element
        self.obedience: float = 0.  # Element
        self.genderIdentity: Optional[EGender] = None  # Element
        self.foughtPlayerCount: int = 0  # Element
        self.lostCombatCount: int = 0  # Element
        self.wonCombatCount: int = 0  # Element
        self.experience: int = 0  # Element
        self.perkPoints: int = 0  # Element
        self.perkCategoryPoints: Dict[str, int] = {}
        self.health: float = 0.  # Element
        self.mana: float = 0.  # Element
        self.areasKnownByCharacters: OrderedDict[str, Set[str]] = OrderedDict()
        self.creampieRetentionAreas: Set[str] = set()

    def overrideAttrs(self, ageAppearanceAbsolute_attribute: _Optional_str = None, ageAppearanceDifference_attribute: _Optional_str = None, captive_attribute: _Optional_str = None, combatBehaviour_attribute: _Optional_str = None, dayOfBirth_attribute: _Optional_str = None, description_attribute: _Optional_str = None, elemental_attribute: _Optional_str = None, elementalSummoned_attribute: _Optional_str = None, experience_attribute: _Optional_str = None, foughtPlayerCount_attribute: _Optional_str = None, genderIdentity_attribute: _Optional_str = None, genericName_attribute: _Optional_str = None, health_attribute: _Optional_str = None, history_attribute: _Optional_str = None, id_attribute: _Optional_str = None, lastTimeHadSex_attribute: _Optional_str = None, lastTimeOrgasmed_attribute: _Optional_str = None, level_attribute: _Optional_str = None, lostCombatCount_attribute: _Optional_str = None, mana_attribute: _Optional_str = None, monthOfBirth_attribute: _Optional_str = None, muskMarker_attribute: _Optional_str = None, name_attribute: _Optional_str = None, obedience_attribute: _Optional_str = None, parserTarget_attribute: _Optional_str = None, pathName_attribute: _Optional_str = None, perkCategoryPoints_keyattr: _Optional_str = None, perkPoints_attribute: _Optional_str = None, petNames_keyattr: _Optional_str = None, petNames_valueattr: _Optional_str = None, playerKnowsName_attribute: _Optional_str = None, playerOnFirstNameTerms_attribute: _Optional_str = None, raceConcealed_attribute: _Optional_str = None, sexualOrientation_attribute: _Optional_str = None, speechColour_attribute: _Optional_str = None, surname_attribute: _Optional_str = None, version_attribute: _Optional_str = None, wonCombatCount_attribute: _Optional_str = None, yearOfBirth_attribute: _Optional_str = None) -> None:
        if ageAppearanceAbsolute_attribute is not None:
            self._XMLID_ATTR_AGEAPPEARANCEABSOLUTE_ATTRIBUTE = ageAppearanceAbsolute_attribute
        if ageAppearanceDifference_attribute is not None:
            self._XMLID_ATTR_AGEAPPEARANCEDIFFERENCE_ATTRIBUTE = ageAppearanceDifference_attribute
        if captive_attribute is not None:
            self._XMLID_ATTR_CAPTIVE_ATTRIBUTE = captive_attribute
        if combatBehaviour_attribute is not None:
            self._XMLID_ATTR_COMBATBEHAVIOUR_ATTRIBUTE = combatBehaviour_attribute
        if dayOfBirth_attribute is not None:
            self._XMLID_ATTR_DAYOFBIRTH_ATTRIBUTE = dayOfBirth_attribute
        if description_attribute is not None:
            self._XMLID_ATTR_DESCRIPTION_ATTRIBUTE = description_attribute
        if elemental_attribute is not None:
            self._XMLID_ATTR_ELEMENTAL_ATTRIBUTE = elemental_attribute
        if elementalSummoned_attribute is not None:
            self._XMLID_ATTR_ELEMENTALSUMMONED_ATTRIBUTE = elementalSummoned_attribute
        if experience_attribute is not None:
            self._XMLID_ATTR_EXPERIENCE_ATTRIBUTE = experience_attribute
        if foughtPlayerCount_attribute is not None:
            self._XMLID_ATTR_FOUGHTPLAYERCOUNT_ATTRIBUTE = foughtPlayerCount_attribute
        if genderIdentity_attribute is not None:
            self._XMLID_ATTR_GENDERIDENTITY_ATTRIBUTE = genderIdentity_attribute
        if genericName_attribute is not None:
            self._XMLID_ATTR_GENERICNAME_ATTRIBUTE = genericName_attribute
        if health_attribute is not None:
            self._XMLID_ATTR_HEALTH_ATTRIBUTE = health_attribute
        if history_attribute is not None:
            self._XMLID_ATTR_HISTORY_ATTRIBUTE = history_attribute
        if id_attribute is not None:
            self._XMLID_ATTR_ID_ATTRIBUTE = id_attribute
        if lastTimeHadSex_attribute is not None:
            self._XMLID_ATTR_LASTTIMEHADSEX_ATTRIBUTE = lastTimeHadSex_attribute
        if lastTimeOrgasmed_attribute is not None:
            self._XMLID_ATTR_LASTTIMEORGASMED_ATTRIBUTE = lastTimeOrgasmed_attribute
        if level_attribute is not None:
            self._XMLID_ATTR_LEVEL_ATTRIBUTE = level_attribute
        if lostCombatCount_attribute is not None:
            self._XMLID_ATTR_LOSTCOMBATCOUNT_ATTRIBUTE = lostCombatCount_attribute
        if mana_attribute is not None:
            self._XMLID_ATTR_MANA_ATTRIBUTE = mana_attribute
        if monthOfBirth_attribute is not None:
            self._XMLID_ATTR_MONTHOFBIRTH_ATTRIBUTE = monthOfBirth_attribute
        if muskMarker_attribute is not None:
            self._XMLID_ATTR_MUSKMARKER_ATTRIBUTE = muskMarker_attribute
        if name_attribute is not None:
            self._XMLID_ATTR_NAME_ATTRIBUTE = name_attribute
        if obedience_attribute is not None:
            self._XMLID_ATTR_OBEDIENCE_ATTRIBUTE = obedience_attribute
        if parserTarget_attribute is not None:
            self._XMLID_ATTR_PARSERTARGET_ATTRIBUTE = parserTarget_attribute
        if pathName_attribute is not None:
            self._XMLID_ATTR_PATHNAME_ATTRIBUTE = pathName_attribute
        if perkCategoryPoints_keyattr is not None:
            self._XMLID_ATTR_PERKCATEGORYPOINTS_KEYATTR = perkCategoryPoints_keyattr
        if perkPoints_attribute is not None:
            self._XMLID_ATTR_PERKPOINTS_ATTRIBUTE = perkPoints_attribute
        if petNames_keyattr is not None:
            self._XMLID_ATTR_PETNAMES_KEYATTR = petNames_keyattr
        if petNames_valueattr is not None:
            self._XMLID_ATTR_PETNAMES_VALUEATTR = petNames_valueattr
        if playerKnowsName_attribute is not None:
            self._XMLID_ATTR_PLAYERKNOWSNAME_ATTRIBUTE = playerKnowsName_attribute
        if playerOnFirstNameTerms_attribute is not None:
            self._XMLID_ATTR_PLAYERONFIRSTNAMETERMS_ATTRIBUTE = playerOnFirstNameTerms_attribute
        if raceConcealed_attribute is not None:
            self._XMLID_ATTR_RACECONCEALED_ATTRIBUTE = raceConcealed_attribute
        if sexualOrientation_attribute is not None:
            self._XMLID_ATTR_SEXUALORIENTATION_ATTRIBUTE = sexualOrientation_attribute
        if speechColour_attribute is not None:
            self._XMLID_ATTR_SPEECHCOLOUR_ATTRIBUTE = speechColour_attribute
        if surname_attribute is not None:
            self._XMLID_ATTR_SURNAME_ATTRIBUTE = surname_attribute
        if version_attribute is not None:
            self._XMLID_ATTR_VERSION_ATTRIBUTE = version_attribute
        if wonCombatCount_attribute is not None:
            self._XMLID_ATTR_WONCOMBATCOUNT_ATTRIBUTE = wonCombatCount_attribute
        if yearOfBirth_attribute is not None:
            self._XMLID_ATTR_YEAROFBIRTH_ATTRIBUTE = yearOfBirth_attribute

    def overrideTags(self, ageAppearanceAbsolute_element: _Optional_str = None, ageAppearanceDifference_element: _Optional_str = None, captive_element: _Optional_str = None, combatBehaviour_element: _Optional_str = None, creampieRetentionAreas_child: _Optional_str = None, creampieRetentionAreas_parent: _Optional_str = None, dayOfBirth_element: _Optional_str = None, description_element: _Optional_str = None, desiredJobs_child: _Optional_str = None, desiredJobs_parent: _Optional_str = None, elemental_element: _Optional_str = None, elementalSummoned_element: _Optional_str = None, experience_element: _Optional_str = None, foughtPlayerCount_element: _Optional_str = None, genderIdentity_element: _Optional_str = None, genericName_element: _Optional_str = None, health_element: _Optional_str = None, history_element: _Optional_str = None, id_element: _Optional_str = None, lastTimeHadSex_element: _Optional_str = None, lastTimeOrgasmed_element: _Optional_str = None, level_element: _Optional_str = None, lostCombatCount_element: _Optional_str = None, mana_element: _Optional_str = None, monthOfBirth_element: _Optional_str = None, muskMarker_element: _Optional_str = None, name_element: _Optional_str = None, obedience_element: _Optional_str = None, parserTarget_element: _Optional_str = None, pathName_element: _Optional_str = None, perkCategoryPoints_child: _Optional_str = None, perkCategoryPoints_parent: _Optional_str = None, perkPoints_element: _Optional_str = None, personality_child: _Optional_str = None, personality_parent: _Optional_str = None, petNames_child: _Optional_str = None, petNames_parent: _Optional_str = None, playerKnowsName_element: _Optional_str = None, playerOnFirstNameTerms_element: _Optional_str = None, raceConcealed_element: _Optional_str = None, sexualOrientation_element: _Optional_str = None, speechColour_element: _Optional_str = None, surname_element: _Optional_str = None, version_element: _Optional_str = None, wonCombatCount_element: _Optional_str = None, yearOfBirth_element: _Optional_str = None) -> None:
        if ageAppearanceAbsolute_element is not None:
            self._XMLID_TAG_AGEAPPEARANCEABSOLUTE_ELEMENT = ageAppearanceAbsolute_element
        if ageAppearanceDifference_element is not None:
            self._XMLID_TAG_AGEAPPEARANCEDIFFERENCE_ELEMENT = ageAppearanceDifference_element
        if captive_element is not None:
            self._XMLID_TAG_CAPTIVE_ELEMENT = captive_element
        if combatBehaviour_element is not None:
            self._XMLID_TAG_COMBATBEHAVIOUR_ELEMENT = combatBehaviour_element
        if creampieRetentionAreas_child is not None:
            self._XMLID_TAG_CREAMPIERETENTIONAREAS_CHILD = creampieRetentionAreas_child
        if creampieRetentionAreas_parent is not None:
            self._XMLID_TAG_CREAMPIERETENTIONAREAS_PARENT = creampieRetentionAreas_parent
        if dayOfBirth_element is not None:
            self._XMLID_TAG_DAYOFBIRTH_ELEMENT = dayOfBirth_element
        if description_element is not None:
            self._XMLID_TAG_DESCRIPTION_ELEMENT = description_element
        if desiredJobs_child is not None:
            self._XMLID_TAG_DESIREDJOBS_CHILD = desiredJobs_child
        if desiredJobs_parent is not None:
            self._XMLID_TAG_DESIREDJOBS_PARENT = desiredJobs_parent
        if elemental_element is not None:
            self._XMLID_TAG_ELEMENTAL_ELEMENT = elemental_element
        if elementalSummoned_element is not None:
            self._XMLID_TAG_ELEMENTALSUMMONED_ELEMENT = elementalSummoned_element
        if experience_element is not None:
            self._XMLID_TAG_EXPERIENCE_ELEMENT = experience_element
        if foughtPlayerCount_element is not None:
            self._XMLID_TAG_FOUGHTPLAYERCOUNT_ELEMENT = foughtPlayerCount_element
        if genderIdentity_element is not None:
            self._XMLID_TAG_GENDERIDENTITY_ELEMENT = genderIdentity_element
        if genericName_element is not None:
            self._XMLID_TAG_GENERICNAME_ELEMENT = genericName_element
        if health_element is not None:
            self._XMLID_TAG_HEALTH_ELEMENT = health_element
        if history_element is not None:
            self._XMLID_TAG_HISTORY_ELEMENT = history_element
        if id_element is not None:
            self._XMLID_TAG_ID_ELEMENT = id_element
        if lastTimeHadSex_element is not None:
            self._XMLID_TAG_LASTTIMEHADSEX_ELEMENT = lastTimeHadSex_element
        if lastTimeOrgasmed_element is not None:
            self._XMLID_TAG_LASTTIMEORGASMED_ELEMENT = lastTimeOrgasmed_element
        if level_element is not None:
            self._XMLID_TAG_LEVEL_ELEMENT = level_element
        if lostCombatCount_element is not None:
            self._XMLID_TAG_LOSTCOMBATCOUNT_ELEMENT = lostCombatCount_element
        if mana_element is not None:
            self._XMLID_TAG_MANA_ELEMENT = mana_element
        if monthOfBirth_element is not None:
            self._XMLID_TAG_MONTHOFBIRTH_ELEMENT = monthOfBirth_element
        if muskMarker_element is not None:
            self._XMLID_TAG_MUSKMARKER_ELEMENT = muskMarker_element
        if name_element is not None:
            self._XMLID_TAG_NAME_ELEMENT = name_element
        if obedience_element is not None:
            self._XMLID_TAG_OBEDIENCE_ELEMENT = obedience_element
        if parserTarget_element is not None:
            self._XMLID_TAG_PARSERTARGET_ELEMENT = parserTarget_element
        if pathName_element is not None:
            self._XMLID_TAG_PATHNAME_ELEMENT = pathName_element
        if perkCategoryPoints_child is not None:
            self._XMLID_TAG_PERKCATEGORYPOINTS_CHILD = perkCategoryPoints_child
        if perkCategoryPoints_parent is not None:
            self._XMLID_TAG_PERKCATEGORYPOINTS_PARENT = perkCategoryPoints_parent
        if perkPoints_element is not None:
            self._XMLID_TAG_PERKPOINTS_ELEMENT = perkPoints_element
        if personality_child is not None:
            self._XMLID_TAG_PERSONALITY_CHILD = personality_child
        if personality_parent is not None:
            self._XMLID_TAG_PERSONALITY_PARENT = personality_parent
        if petNames_child is not None:
            self._XMLID_TAG_PETNAMES_CHILD = petNames_child
        if petNames_parent is not None:
            self._XMLID_TAG_PETNAMES_PARENT = petNames_parent
        if playerKnowsName_element is not None:
            self._XMLID_TAG_PLAYERKNOWSNAME_ELEMENT = playerKnowsName_element
        if playerOnFirstNameTerms_element is not None:
            self._XMLID_TAG_PLAYERONFIRSTNAMETERMS_ELEMENT = playerOnFirstNameTerms_element
        if raceConcealed_element is not None:
            self._XMLID_TAG_RACECONCEALED_ELEMENT = raceConcealed_element
        if sexualOrientation_element is not None:
            self._XMLID_TAG_SEXUALORIENTATION_ELEMENT = sexualOrientation_element
        if speechColour_element is not None:
            self._XMLID_TAG_SPEECHCOLOUR_ELEMENT = speechColour_element
        if surname_element is not None:
            self._XMLID_TAG_SURNAME_ELEMENT = surname_element
        if version_element is not None:
            self._XMLID_TAG_VERSION_ELEMENT = version_element
        if wonCombatCount_element is not None:
            self._XMLID_TAG_WONCOMBATCOUNT_ELEMENT = wonCombatCount_element
        if yearOfBirth_element is not None:
            self._XMLID_TAG_YEAROFBIRTH_ELEMENT = yearOfBirth_element

    def _fromXML_areasKnownByCharacters(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromXML_areasKnownByCharacters()')

    def _toXML_areasKnownByCharacters(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toXML_areasKnownByCharacters()')

    def _fromDict_areasKnownByCharacters(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromDict_areasKnownByCharacters()')

    def _toDict_areasKnownByCharacters(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toDict_areasKnownByCharacters()')

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        dc: etree._Element
        lk: str
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_ID_ELEMENT)) is not None:
            if self._XMLID_ATTR_ID_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_ID_ATTRIBUTE, 'id')
            self.id = sf.attrib[self._XMLID_ATTR_ID_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'id', self._XMLID_TAG_ID_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_PARSERTARGET_ELEMENT)) is not None:
            self.parserTarget = sf.attrib.get(self._XMLID_ATTR_PARSERTARGET_ATTRIBUTE, None)
        if (sf := e.find(self._XMLID_TAG_PATHNAME_ELEMENT)) is not None:
            if self._XMLID_ATTR_PATHNAME_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_PATHNAME_ATTRIBUTE, 'pathName')
            self.pathName = sf.attrib[self._XMLID_ATTR_PATHNAME_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'pathName', self._XMLID_TAG_PATHNAME_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_NAME_ELEMENT)) is not None:
            self.name = CharacterNames()
            self.name.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'name', self._XMLID_TAG_NAME_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_SURNAME_ELEMENT)) is not None:
            if self._XMLID_ATTR_SURNAME_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_SURNAME_ATTRIBUTE, 'surname')
            self.surname = sf.attrib[self._XMLID_ATTR_SURNAME_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'surname', self._XMLID_TAG_SURNAME_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_GENERICNAME_ELEMENT)) is not None:
            if self._XMLID_ATTR_GENERICNAME_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_GENERICNAME_ATTRIBUTE, 'genericName')
            self.genericName = sf.attrib[self._XMLID_ATTR_GENERICNAME_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'genericName', self._XMLID_TAG_GENERICNAME_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_DESCRIPTION_ELEMENT)) is not None:
            if self._XMLID_ATTR_DESCRIPTION_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_DESCRIPTION_ATTRIBUTE, 'description')
            self.description = sf.attrib[self._XMLID_ATTR_DESCRIPTION_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'description', self._XMLID_TAG_DESCRIPTION_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_PLAYERKNOWSNAME_ELEMENT)) is not None:
            if self._XMLID_ATTR_PLAYERKNOWSNAME_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_PLAYERKNOWSNAME_ATTRIBUTE, 'playerKnowsName')
            self.playerKnowsName = XML2BOOL[sf.attrib[self._XMLID_ATTR_PLAYERKNOWSNAME_ATTRIBUTE]]
        else:
            raise ElementRequiredException(self, e, 'playerKnowsName', self._XMLID_TAG_PLAYERKNOWSNAME_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_PLAYERONFIRSTNAMETERMS_ELEMENT)) is not None:
            if self._XMLID_ATTR_PLAYERONFIRSTNAMETERMS_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_PLAYERONFIRSTNAMETERMS_ATTRIBUTE, 'playerOnFirstNameTerms')
            self.playerOnFirstNameTerms = XML2BOOL[sf.attrib[self._XMLID_ATTR_PLAYERONFIRSTNAMETERMS_ATTRIBUTE]]
        else:
            raise ElementRequiredException(self, e, 'playerOnFirstNameTerms', self._XMLID_TAG_PLAYERONFIRSTNAMETERMS_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_RACECONCEALED_ELEMENT)) is not None:
            if self._XMLID_ATTR_RACECONCEALED_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_RACECONCEALED_ATTRIBUTE, 'raceConcealed')
            self.raceConcealed = XML2BOOL[sf.attrib[self._XMLID_ATTR_RACECONCEALED_ATTRIBUTE]]
        else:
            raise ElementRequiredException(self, e, 'raceConcealed', self._XMLID_TAG_RACECONCEALED_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_CAPTIVE_ELEMENT)) is not None:
            self.captive = XML2BOOL[sf.attrib.get(self._XMLID_ATTR_CAPTIVE_ATTRIBUTE, 'false')]
        else:
            self.captive = False
        if (sf := e.find(self._XMLID_TAG_LEVEL_ELEMENT)) is not None:
            if self._XMLID_ATTR_LEVEL_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_LEVEL_ATTRIBUTE, 'level')
            self.level = int(sf.attrib[self._XMLID_ATTR_LEVEL_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'level', self._XMLID_TAG_LEVEL_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_AGEAPPEARANCEDIFFERENCE_ELEMENT)) is not None:
            if self._XMLID_ATTR_AGEAPPEARANCEDIFFERENCE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_AGEAPPEARANCEDIFFERENCE_ATTRIBUTE, 'ageAppearanceDifference')
            self.ageAppearanceDifference = int(sf.attrib[self._XMLID_ATTR_AGEAPPEARANCEDIFFERENCE_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'ageAppearanceDifference', self._XMLID_TAG_AGEAPPEARANCEDIFFERENCE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_AGEAPPEARANCEABSOLUTE_ELEMENT)) is not None:
            if sf.attrib.get(self._XMLID_ATTR_AGEAPPEARANCEABSOLUTE_ATTRIBUTE, "None") == 'null':
                self.ageAppearanceAbsolute = None
            else:
                self.ageAppearanceAbsolute = int(sf.attrib.get(self._XMLID_ATTR_AGEAPPEARANCEABSOLUTE_ATTRIBUTE, "None"))
        else:
            self.ageAppearanceAbsolute = None
        if (sf := e.find(self._XMLID_TAG_YEAROFBIRTH_ELEMENT)) is not None:
            if self._XMLID_ATTR_YEAROFBIRTH_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_YEAROFBIRTH_ATTRIBUTE, 'yearOfBirth')
            self.yearOfBirth = int(sf.attrib[self._XMLID_ATTR_YEAROFBIRTH_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'yearOfBirth', self._XMLID_TAG_YEAROFBIRTH_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_MONTHOFBIRTH_ELEMENT)) is not None:
            if self._XMLID_ATTR_MONTHOFBIRTH_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_MONTHOFBIRTH_ATTRIBUTE, 'monthOfBirth')
            self.monthOfBirth = EMonth[sf.attrib[self._XMLID_ATTR_MONTHOFBIRTH_ATTRIBUTE]]
        else:
            raise ElementRequiredException(self, e, 'monthOfBirth', self._XMLID_TAG_MONTHOFBIRTH_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_DAYOFBIRTH_ELEMENT)) is not None:
            if self._XMLID_ATTR_DAYOFBIRTH_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_DAYOFBIRTH_ATTRIBUTE, 'dayOfBirth')
            self.dayOfBirth = int(sf.attrib[self._XMLID_ATTR_DAYOFBIRTH_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'dayOfBirth', self._XMLID_TAG_DAYOFBIRTH_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_VERSION_ELEMENT)) is not None:
            if self._XMLID_ATTR_VERSION_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_VERSION_ATTRIBUTE, 'version')
            self.version = sf.attrib[self._XMLID_ATTR_VERSION_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'version', self._XMLID_TAG_VERSION_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_HISTORY_ELEMENT)) is not None:
            if self._XMLID_ATTR_HISTORY_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_HISTORY_ATTRIBUTE, 'history')
            self.history = sf.attrib[self._XMLID_ATTR_HISTORY_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'history', self._XMLID_TAG_HISTORY_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_ELEMENTAL_ELEMENT)) is not None:
            if self._XMLID_ATTR_ELEMENTAL_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_ELEMENTAL_ATTRIBUTE, 'elemental')
            self.elemental = sf.attrib[self._XMLID_ATTR_ELEMENTAL_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'elemental', self._XMLID_TAG_ELEMENTAL_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_ELEMENTALSUMMONED_ELEMENT)) is not None:
            if self._XMLID_ATTR_ELEMENTALSUMMONED_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_ELEMENTALSUMMONED_ATTRIBUTE, 'elementalSummoned')
            self.elementalSummoned = XML2BOOL[sf.attrib[self._XMLID_ATTR_ELEMENTALSUMMONED_ATTRIBUTE]]
        else:
            raise ElementRequiredException(self, e, 'elementalSummoned', self._XMLID_TAG_ELEMENTALSUMMONED_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_COMBATBEHAVIOUR_ELEMENT)) is not None:
            if self._XMLID_ATTR_COMBATBEHAVIOUR_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_COMBATBEHAVIOUR_ATTRIBUTE, 'combatBehaviour')
            self.combatBehaviour = sf.attrib[self._XMLID_ATTR_COMBATBEHAVIOUR_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'combatBehaviour', self._XMLID_TAG_COMBATBEHAVIOUR_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_LASTTIMEHADSEX_ELEMENT)) is not None:
            if self._XMLID_ATTR_LASTTIMEHADSEX_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_LASTTIMEHADSEX_ATTRIBUTE, 'lastTimeHadSex')
            self.lastTimeHadSex = int(sf.attrib[self._XMLID_ATTR_LASTTIMEHADSEX_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'lastTimeHadSex', self._XMLID_TAG_LASTTIMEHADSEX_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_LASTTIMEORGASMED_ELEMENT)) is not None:
            if self._XMLID_ATTR_LASTTIMEORGASMED_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_LASTTIMEORGASMED_ATTRIBUTE, 'lastTimeOrgasmed')
            self.lastTimeOrgasmed = int(sf.attrib[self._XMLID_ATTR_LASTTIMEORGASMED_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'lastTimeOrgasmed', self._XMLID_TAG_LASTTIMEORGASMED_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_MUSKMARKER_ELEMENT)) is not None:
            self.muskMarker = sf.attrib.get(self._XMLID_ATTR_MUSKMARKER_ATTRIBUTE, None)
        if (sf := e.find(self._XMLID_TAG_SPEECHCOLOUR_ELEMENT)) is not None:
            self.speechColour = sf.attrib.get(self._XMLID_ATTR_SPEECHCOLOUR_ATTRIBUTE, None)
        if (lf := e.find(self._XMLID_TAG_DESIREDJOBS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_DESIREDJOBS_CHILD):
                    self.desiredJobs.add(lc.text)
        else:
            self.desiredJobs = set()
        if (df := e.find(self._XMLID_TAG_PETNAMES_PARENT)) is not None:
            for dc in df.iterfind(self._XMLID_TAG_PETNAMES_CHILD):
                self.petNames[dc.attrib['id']] = dc.attrib['petName']
        else:
            self.petNames = {}
        if (lf := e.find(self._XMLID_TAG_PERSONALITY_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_PERSONALITY_CHILD):
                    self.personality.add(lc.text)
        else:
            self.personality = set()
        if (sf := e.find(self._XMLID_TAG_SEXUALORIENTATION_ELEMENT)) is not None:
            if self._XMLID_ATTR_SEXUALORIENTATION_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_SEXUALORIENTATION_ATTRIBUTE, 'sexualOrientation')
            self.sexualOrientation = ESexualOrientation[sf.attrib[self._XMLID_ATTR_SEXUALORIENTATION_ATTRIBUTE]]
        else:
            raise ElementRequiredException(self, e, 'sexualOrientation', self._XMLID_TAG_SEXUALORIENTATION_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_OBEDIENCE_ELEMENT)) is not None:
            if self._XMLID_ATTR_OBEDIENCE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_OBEDIENCE_ATTRIBUTE, 'obedience')
            self.obedience = float(sf.attrib[self._XMLID_ATTR_OBEDIENCE_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'obedience', self._XMLID_TAG_OBEDIENCE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_GENDERIDENTITY_ELEMENT)) is not None:
            if sf.attrib.get(self._XMLID_ATTR_GENDERIDENTITY_ATTRIBUTE, None) == 'null':
                self.genderIdentity = None
            else:
                self.genderIdentity = EGender[sf.attrib.get(self._XMLID_ATTR_GENDERIDENTITY_ATTRIBUTE, None)]
        if (sf := e.find(self._XMLID_TAG_FOUGHTPLAYERCOUNT_ELEMENT)) is not None:
            if self._XMLID_ATTR_FOUGHTPLAYERCOUNT_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_FOUGHTPLAYERCOUNT_ATTRIBUTE, 'foughtPlayerCount')
            self.foughtPlayerCount = int(sf.attrib[self._XMLID_ATTR_FOUGHTPLAYERCOUNT_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'foughtPlayerCount', self._XMLID_TAG_FOUGHTPLAYERCOUNT_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_LOSTCOMBATCOUNT_ELEMENT)) is not None:
            if self._XMLID_ATTR_LOSTCOMBATCOUNT_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_LOSTCOMBATCOUNT_ATTRIBUTE, 'lostCombatCount')
            self.lostCombatCount = int(sf.attrib[self._XMLID_ATTR_LOSTCOMBATCOUNT_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'lostCombatCount', self._XMLID_TAG_LOSTCOMBATCOUNT_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_WONCOMBATCOUNT_ELEMENT)) is not None:
            if self._XMLID_ATTR_WONCOMBATCOUNT_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_WONCOMBATCOUNT_ATTRIBUTE, 'wonCombatCount')
            self.wonCombatCount = int(sf.attrib[self._XMLID_ATTR_WONCOMBATCOUNT_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'wonCombatCount', self._XMLID_TAG_WONCOMBATCOUNT_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_EXPERIENCE_ELEMENT)) is not None:
            if self._XMLID_ATTR_EXPERIENCE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_EXPERIENCE_ATTRIBUTE, 'experience')
            self.experience = int(sf.attrib[self._XMLID_ATTR_EXPERIENCE_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'experience', self._XMLID_TAG_EXPERIENCE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_PERKPOINTS_ELEMENT)) is not None:
            if self._XMLID_ATTR_PERKPOINTS_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_PERKPOINTS_ATTRIBUTE, 'perkPoints')
            self.perkPoints = int(sf.attrib[self._XMLID_ATTR_PERKPOINTS_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'perkPoints', self._XMLID_TAG_PERKPOINTS_ELEMENT)
        if (df := e.find(self._XMLID_TAG_PERKCATEGORYPOINTS_PARENT)) is not None:
            for dc in df.iterfind(self._XMLID_TAG_PERKCATEGORYPOINTS_CHILD):
                self.perkCategoryPoints[dc.attrib['category']] = int(dc.text)
        else:
            self.perkCategoryPoints = {}
        if (sf := e.find(self._XMLID_TAG_HEALTH_ELEMENT)) is not None:
            if self._XMLID_ATTR_HEALTH_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_HEALTH_ATTRIBUTE, 'health')
            self.health = float(sf.attrib[self._XMLID_ATTR_HEALTH_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'health', self._XMLID_TAG_HEALTH_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_MANA_ELEMENT)) is not None:
            if self._XMLID_ATTR_MANA_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_MANA_ATTRIBUTE, 'mana')
            self.mana = float(sf.attrib[self._XMLID_ATTR_MANA_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'mana', self._XMLID_TAG_MANA_ELEMENT)
        self._fromXML_areasKnownByCharacters(e)
        if (lf := e.find(self._XMLID_TAG_CREAMPIERETENTIONAREAS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_CREAMPIERETENTIONAREAS_CHILD):
                    self.creampieRetentionAreas.add(lc.text)
        else:
            self.creampieRetentionAreas = set()

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        dK: Any
        dV: Any
        dc: etree._Element
        dp: etree._Element
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        e.append(etree.Comment('If you want to edit any of these values, just be warned that it might break the game...'))
        etree.SubElement(e, self._XMLID_TAG_ID_ELEMENT, {self._XMLID_ATTR_ID_ATTRIBUTE: str(self.id)})
        if self.parserTarget is not None:
            etree.SubElement(e, self._XMLID_TAG_PARSERTARGET_ELEMENT, {self._XMLID_ATTR_PARSERTARGET_ATTRIBUTE: str(self.parserTarget)})
        etree.SubElement(e, self._XMLID_TAG_PATHNAME_ELEMENT, {self._XMLID_ATTR_PATHNAME_ATTRIBUTE: str(self.pathName)})
        e.append(self.name.toXML(self._XMLID_TAG_NAME_ELEMENT))
        etree.SubElement(e, self._XMLID_TAG_SURNAME_ELEMENT, {self._XMLID_ATTR_SURNAME_ATTRIBUTE: str(self.surname)})
        etree.SubElement(e, self._XMLID_TAG_GENERICNAME_ELEMENT, {self._XMLID_ATTR_GENERICNAME_ATTRIBUTE: str(self.genericName)})
        etree.SubElement(e, self._XMLID_TAG_DESCRIPTION_ELEMENT, {self._XMLID_ATTR_DESCRIPTION_ATTRIBUTE: str(self.description)})
        etree.SubElement(e, self._XMLID_TAG_PLAYERKNOWSNAME_ELEMENT, {self._XMLID_ATTR_PLAYERKNOWSNAME_ATTRIBUTE: str(self.playerKnowsName).lower()})
        etree.SubElement(e, self._XMLID_TAG_PLAYERONFIRSTNAMETERMS_ELEMENT, {self._XMLID_ATTR_PLAYERONFIRSTNAMETERMS_ATTRIBUTE: str(self.playerOnFirstNameTerms).lower()})
        etree.SubElement(e, self._XMLID_TAG_RACECONCEALED_ELEMENT, {self._XMLID_ATTR_RACECONCEALED_ATTRIBUTE: str(self.raceConcealed).lower()})
        if self.captive is not None and self.captive != False:
            etree.SubElement(e, self._XMLID_TAG_CAPTIVE_ELEMENT, {self._XMLID_ATTR_CAPTIVE_ATTRIBUTE: str(self.captive).lower()})
        etree.SubElement(e, self._XMLID_TAG_LEVEL_ELEMENT, {self._XMLID_ATTR_LEVEL_ATTRIBUTE: str(self.level)})
        etree.SubElement(e, self._XMLID_TAG_AGEAPPEARANCEDIFFERENCE_ELEMENT, {self._XMLID_ATTR_AGEAPPEARANCEDIFFERENCE_ATTRIBUTE: str(self.ageAppearanceDifference)})
        if self.ageAppearanceAbsolute is not None and self.ageAppearanceAbsolute != None:
            etree.SubElement(e, self._XMLID_TAG_AGEAPPEARANCEABSOLUTE_ELEMENT, {self._XMLID_ATTR_AGEAPPEARANCEABSOLUTE_ATTRIBUTE: str(self.ageAppearanceAbsolute)})
        etree.SubElement(e, self._XMLID_TAG_YEAROFBIRTH_ELEMENT, {self._XMLID_ATTR_YEAROFBIRTH_ATTRIBUTE: str(self.yearOfBirth)})
        etree.SubElement(e, self._XMLID_TAG_MONTHOFBIRTH_ELEMENT, {self._XMLID_ATTR_MONTHOFBIRTH_ATTRIBUTE: self.monthOfBirth.name})
        etree.SubElement(e, self._XMLID_TAG_DAYOFBIRTH_ELEMENT, {self._XMLID_ATTR_DAYOFBIRTH_ATTRIBUTE: str(self.dayOfBirth)})
        etree.SubElement(e, self._XMLID_TAG_VERSION_ELEMENT, {self._XMLID_ATTR_VERSION_ATTRIBUTE: str(self.version)})
        etree.SubElement(e, self._XMLID_TAG_HISTORY_ELEMENT, {self._XMLID_ATTR_HISTORY_ATTRIBUTE: str(self.history)})
        etree.SubElement(e, self._XMLID_TAG_ELEMENTAL_ELEMENT, {self._XMLID_ATTR_ELEMENTAL_ATTRIBUTE: str(self.elemental)})
        etree.SubElement(e, self._XMLID_TAG_ELEMENTALSUMMONED_ELEMENT, {self._XMLID_ATTR_ELEMENTALSUMMONED_ATTRIBUTE: str(self.elementalSummoned).lower()})
        etree.SubElement(e, self._XMLID_TAG_COMBATBEHAVIOUR_ELEMENT, {self._XMLID_ATTR_COMBATBEHAVIOUR_ATTRIBUTE: str(self.combatBehaviour)})
        etree.SubElement(e, self._XMLID_TAG_LASTTIMEHADSEX_ELEMENT, {self._XMLID_ATTR_LASTTIMEHADSEX_ATTRIBUTE: str(self.lastTimeHadSex)})
        etree.SubElement(e, self._XMLID_TAG_LASTTIMEORGASMED_ELEMENT, {self._XMLID_ATTR_LASTTIMEORGASMED_ATTRIBUTE: str(self.lastTimeOrgasmed)})
        if self.muskMarker is not None:
            etree.SubElement(e, self._XMLID_TAG_MUSKMARKER_ELEMENT, {self._XMLID_ATTR_MUSKMARKER_ATTRIBUTE: str(self.muskMarker)})
        if self.speechColour is not None:
            etree.SubElement(e, self._XMLID_TAG_SPEECHCOLOUR_ELEMENT, {self._XMLID_ATTR_SPEECHCOLOUR_ATTRIBUTE: str(self.speechColour)})
        lp = etree.SubElement(e, self._XMLID_TAG_DESIREDJOBS_PARENT)
        for lv in sorted(self.desiredJobs, key=str):
            etree.SubElement(lp, self._XMLID_TAG_DESIREDJOBS_CHILD, {}).text = lv
        dp = etree.SubElement(e, self._XMLID_TAG_PETNAMES_PARENT)
        for dK, dV in sorted(self.petNames.items(), key=lambda t: t[0]):
            dc = etree.SubElement(dp, self._XMLID_TAG_PETNAMES_CHILD)
            dc.attrib['id'] = dK
            dc.attrib['petName'] = dV
        lp = etree.SubElement(e, self._XMLID_TAG_PERSONALITY_PARENT)
        for lv in sorted(self.personality, key=str):
            etree.SubElement(lp, self._XMLID_TAG_PERSONALITY_CHILD, {}).text = lv
        etree.SubElement(e, self._XMLID_TAG_SEXUALORIENTATION_ELEMENT, {self._XMLID_ATTR_SEXUALORIENTATION_ATTRIBUTE: self.sexualOrientation.name})
        etree.SubElement(e, self._XMLID_TAG_OBEDIENCE_ELEMENT, {self._XMLID_ATTR_OBEDIENCE_ATTRIBUTE: str(self.obedience)})
        if self.genderIdentity is not None:
            etree.SubElement(e, self._XMLID_TAG_GENDERIDENTITY_ELEMENT, {self._XMLID_ATTR_GENDERIDENTITY_ATTRIBUTE: self.genderIdentity.name})
        etree.SubElement(e, self._XMLID_TAG_FOUGHTPLAYERCOUNT_ELEMENT, {self._XMLID_ATTR_FOUGHTPLAYERCOUNT_ATTRIBUTE: str(self.foughtPlayerCount)})
        etree.SubElement(e, self._XMLID_TAG_LOSTCOMBATCOUNT_ELEMENT, {self._XMLID_ATTR_LOSTCOMBATCOUNT_ATTRIBUTE: str(self.lostCombatCount)})
        etree.SubElement(e, self._XMLID_TAG_WONCOMBATCOUNT_ELEMENT, {self._XMLID_ATTR_WONCOMBATCOUNT_ATTRIBUTE: str(self.wonCombatCount)})
        etree.SubElement(e, self._XMLID_TAG_EXPERIENCE_ELEMENT, {self._XMLID_ATTR_EXPERIENCE_ATTRIBUTE: str(self.experience)})
        etree.SubElement(e, self._XMLID_TAG_PERKPOINTS_ELEMENT, {self._XMLID_ATTR_PERKPOINTS_ATTRIBUTE: str(self.perkPoints)})
        dp = etree.SubElement(e, self._XMLID_TAG_PERKCATEGORYPOINTS_PARENT)
        for dK, dV in sorted(self.perkCategoryPoints.items(), key=lambda t: t[0]):
            dc = etree.SubElement(dp, self._XMLID_TAG_PERKCATEGORYPOINTS_CHILD)
            dc.attrib['category'] = dK
            dc.text = str(dV)
        etree.SubElement(e, self._XMLID_TAG_HEALTH_ELEMENT, {self._XMLID_ATTR_HEALTH_ATTRIBUTE: str(self.health)})
        etree.SubElement(e, self._XMLID_TAG_MANA_ELEMENT, {self._XMLID_ATTR_MANA_ATTRIBUTE: str(self.mana)})
        self._toXML_areasKnownByCharacters(e)
        lp = etree.SubElement(e, self._XMLID_TAG_CREAMPIERETENTIONAREAS_PARENT)
        for lv in sorted(self.creampieRetentionAreas, key=str):
            etree.SubElement(lp, self._XMLID_TAG_CREAMPIERETENTIONAREAS_CHILD, {}).text = lv
        return e

    def toDict(self) -> Dict[str, Any]:
        dK: Any
        dV: Any
        data: Dict[str, Any] = super().toDict()
        dp: Dict[str, Any]
        data["@comment-0001"] = 'If you want to edit any of these values, just be warned that it might break the game...'
        data['id'] = str(self.id)
        if self.parserTarget is not None:
            data['parserTarget'] = str(self.parserTarget)
        data['pathName'] = str(self.pathName)
        data['name'] = self.name.toDict()
        data['surname'] = str(self.surname)
        data['genericName'] = str(self.genericName)
        data['description'] = str(self.description)
        data['playerKnowsName'] = bool(self.playerKnowsName)
        data['playerOnFirstNameTerms'] = bool(self.playerOnFirstNameTerms)
        data['raceConcealed'] = bool(self.raceConcealed)
        if self.captive is not None:
            data['captive'] = bool(self.captive)
        data['level'] = int(self.level)
        data['ageAppearanceDifference'] = int(self.ageAppearanceDifference)
        if self.ageAppearanceAbsolute is not None:
            data['ageAppearanceAbsolute'] = int(self.ageAppearanceAbsolute)
        data['yearOfBirth'] = int(self.yearOfBirth)
        data['monthOfBirth'] = self.monthOfBirth.name
        data['dayOfBirth'] = int(self.dayOfBirth)
        data['version'] = str(self.version)
        data['history'] = str(self.history)
        data['elemental'] = str(self.elemental)
        data['elementalSummoned'] = bool(self.elementalSummoned)
        data['combatBehaviour'] = str(self.combatBehaviour)
        data['lastTimeHadSex'] = int(self.lastTimeHadSex)
        data['lastTimeOrgasmed'] = int(self.lastTimeOrgasmed)
        if self.muskMarker is not None:
            data['muskMarker'] = str(self.muskMarker)
        if self.speechColour is not None:
            data['speechColour'] = str(self.speechColour)
        if self.desiredJobs is None or len(self.desiredJobs) > 0:
            data['desiredJobs'] = list()
        else:
            lc = list()
            if len(self.desiredJobs) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['desiredJobs'] = lc
        dp = {}
        for dK, dV in sorted(self.petNames.items(), key=lambda t: t[0]):
            sk = dK
            sv = dV
            dp[sk] = sv
        data['petNames'] = dp
        if self.personality is None or len(self.personality) > 0:
            data['personality'] = list()
        else:
            lc = list()
            if len(self.personality) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['personality'] = lc
        data['sexualOrientation'] = self.sexualOrientation.name
        data['obedience'] = float(self.obedience)
        if self.genderIdentity is not None:
            data['genderIdentity'] = self.genderIdentity.name
        data['foughtPlayerCount'] = int(self.foughtPlayerCount)
        data['lostCombatCount'] = int(self.lostCombatCount)
        data['wonCombatCount'] = int(self.wonCombatCount)
        data['experience'] = int(self.experience)
        data['perkPoints'] = int(self.perkPoints)
        dp = {}
        for dK, dV in sorted(self.perkCategoryPoints.items(), key=lambda t: t[0]):
            sk = dK
            sv = dV
            dp[sk] = sv
        data['perkCategoryPoints'] = dp
        data['health'] = float(self.health)
        data['mana'] = float(self.mana)
        self._toDict_areasKnownByCharacters(data)
        if self.creampieRetentionAreas is None or len(self.creampieRetentionAreas) > 0:
            data['creampieRetentionAreas'] = list()
        else:
            lc = list()
            if len(self.creampieRetentionAreas) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['creampieRetentionAreas'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('id')) is not None:
            self.id = data['id']
        else:
            raise KeyRequiredException(self, data, 'id', 'id')
        if (sf := data.get('parserTarget')) is not None:
            self.parserTarget = data['parserTarget']
        else:
            self.parserTarget = None
        if (sf := data.get('pathName')) is not None:
            self.pathName = data['pathName']
        else:
            raise KeyRequiredException(self, data, 'pathName', 'pathName')
        if (sf := data.get('name')) is not None:
            self.name = CharacterNames()
            self.name.fromDict(data['name'])
        else:
            raise KeyRequiredException(self, data, 'name', 'name')
        if (sf := data.get('surname')) is not None:
            self.surname = data['surname']
        else:
            raise KeyRequiredException(self, data, 'surname', 'surname')
        if (sf := data.get('genericName')) is not None:
            self.genericName = data['genericName']
        else:
            raise KeyRequiredException(self, data, 'genericName', 'genericName')
        if (sf := data.get('description')) is not None:
            self.description = data['description']
        else:
            raise KeyRequiredException(self, data, 'description', 'description')
        if (sf := data.get('playerKnowsName')) is not None:
            self.playerKnowsName = data['playerKnowsName']
        else:
            raise KeyRequiredException(self, data, 'playerKnowsName', 'playerKnowsName')
        if (sf := data.get('playerOnFirstNameTerms')) is not None:
            self.playerOnFirstNameTerms = data['playerOnFirstNameTerms']
        else:
            raise KeyRequiredException(self, data, 'playerOnFirstNameTerms', 'playerOnFirstNameTerms')
        if (sf := data.get('raceConcealed')) is not None:
            self.raceConcealed = data['raceConcealed']
        else:
            raise KeyRequiredException(self, data, 'raceConcealed', 'raceConcealed')
        if (sf := data.get('captive')) is not None:
            self.captive = data['captive']
        else:
            self.captive = False
        if (sf := data.get('level')) is not None:
            self.level = data['level']
        else:
            raise KeyRequiredException(self, data, 'level', 'level')
        if (sf := data.get('ageAppearanceDifference')) is not None:
            self.ageAppearanceDifference = data['ageAppearanceDifference']
        else:
            raise KeyRequiredException(self, data, 'ageAppearanceDifference', 'ageAppearanceDifference')
        if (sf := data.get('ageAppearanceAbsolute')) is not None:
            self.ageAppearanceAbsolute = data['ageAppearanceAbsolute']
        else:
            self.ageAppearanceAbsolute = None
        if (sf := data.get('yearOfBirth')) is not None:
            self.yearOfBirth = data['yearOfBirth']
        else:
            raise KeyRequiredException(self, data, 'yearOfBirth', 'yearOfBirth')
        if (sf := data.get('monthOfBirth')) is not None:
            self.monthOfBirth = data['monthOfBirth'].name
        else:
            raise KeyRequiredException(self, data, 'monthOfBirth', 'monthOfBirth')
        if (sf := data.get('dayOfBirth')) is not None:
            self.dayOfBirth = data['dayOfBirth']
        else:
            raise KeyRequiredException(self, data, 'dayOfBirth', 'dayOfBirth')
        if (sf := data.get('version')) is not None:
            self.version = data['version']
        else:
            raise KeyRequiredException(self, data, 'version', 'version')
        if (sf := data.get('history')) is not None:
            self.history = data['history']
        else:
            raise KeyRequiredException(self, data, 'history', 'history')
        if (sf := data.get('elemental')) is not None:
            self.elemental = data['elemental']
        else:
            raise KeyRequiredException(self, data, 'elemental', 'elemental')
        if (sf := data.get('elementalSummoned')) is not None:
            self.elementalSummoned = data['elementalSummoned']
        else:
            raise KeyRequiredException(self, data, 'elementalSummoned', 'elementalSummoned')
        if (sf := data.get('combatBehaviour')) is not None:
            self.combatBehaviour = data['combatBehaviour']
        else:
            raise KeyRequiredException(self, data, 'combatBehaviour', 'combatBehaviour')
        if (sf := data.get('lastTimeHadSex')) is not None:
            self.lastTimeHadSex = data['lastTimeHadSex']
        else:
            raise KeyRequiredException(self, data, 'lastTimeHadSex', 'lastTimeHadSex')
        if (sf := data.get('lastTimeOrgasmed')) is not None:
            self.lastTimeOrgasmed = data['lastTimeOrgasmed']
        else:
            raise KeyRequiredException(self, data, 'lastTimeOrgasmed', 'lastTimeOrgasmed')
        if (sf := data.get('muskMarker')) is not None:
            self.muskMarker = data['muskMarker']
        else:
            self.muskMarker = None
        if (sf := data.get('speechColour')) is not None:
            self.speechColour = data['speechColour']
        else:
            self.speechColour = None
        if (lv := self.desiredJobs) is not None:
            for le in lv:
                self.desiredJobs.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'desiredJobs', 'desiredJobs')
        if (df := data.get('petNames')) is not None:
            for sk, sv in df.items():
                self.petNames[sk] = sv
        else:
            self.petNames = {}
        if (lv := self.personality) is not None:
            for le in lv:
                self.personality.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'personality', 'personality')
        if (sf := data.get('sexualOrientation')) is not None:
            self.sexualOrientation = data['sexualOrientation'].name
        else:
            raise KeyRequiredException(self, data, 'sexualOrientation', 'sexualOrientation')
        if (sf := data.get('obedience')) is not None:
            self.obedience = data['obedience']
        else:
            raise KeyRequiredException(self, data, 'obedience', 'obedience')
        if (sf := data.get('genderIdentity')) is not None:
            self.genderIdentity = data['genderIdentity'].name
        else:
            self.genderIdentity = None
        if (sf := data.get('foughtPlayerCount')) is not None:
            self.foughtPlayerCount = data['foughtPlayerCount']
        else:
            raise KeyRequiredException(self, data, 'foughtPlayerCount', 'foughtPlayerCount')
        if (sf := data.get('lostCombatCount')) is not None:
            self.lostCombatCount = data['lostCombatCount']
        else:
            raise KeyRequiredException(self, data, 'lostCombatCount', 'lostCombatCount')
        if (sf := data.get('wonCombatCount')) is not None:
            self.wonCombatCount = data['wonCombatCount']
        else:
            raise KeyRequiredException(self, data, 'wonCombatCount', 'wonCombatCount')
        if (sf := data.get('experience')) is not None:
            self.experience = data['experience']
        else:
            raise KeyRequiredException(self, data, 'experience', 'experience')
        if (sf := data.get('perkPoints')) is not None:
            self.perkPoints = data['perkPoints']
        else:
            raise KeyRequiredException(self, data, 'perkPoints', 'perkPoints')
        if (df := data.get('perkCategoryPoints')) is not None:
            for sk, sv in df.items():
                self.perkCategoryPoints[sk] = sv
        else:
            self.perkCategoryPoints = {}
        if (sf := data.get('health')) is not None:
            self.health = data['health']
        else:
            raise KeyRequiredException(self, data, 'health', 'health')
        if (sf := data.get('mana')) is not None:
            self.mana = data['mana']
        else:
            raise KeyRequiredException(self, data, 'mana', 'mana')
        self._fromDict_areasKnownByCharacters(data)
        if (lv := self.creampieRetentionAreas) is not None:
            for le in lv:
                self.creampieRetentionAreas.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'creampieRetentionAreas', 'creampieRetentionAreas')

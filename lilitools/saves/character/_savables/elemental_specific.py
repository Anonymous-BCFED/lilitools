#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawElementalSpecific']
## from [LT]/src/com/lilithsthrone/game/character/npc/misc/Elemental.java: public Element saveAsXML(Element parentElement, Document doc) { @ YBSQ1qsXF/SSNNzGt78qdk3z7oJIWFcUR0ZepbKciyJgOLrsPxRS9+uJwMn5GUZYWYNIsiD3xu0/QsB3kw/QZw==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawElementalSpecific(Savable):
    TAG = 'elementalSpecial'
    _XMLID_ATTR_PASSIVEFORM_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_SUMMONER_ATTRIBUTE: str = 'value'
    _XMLID_TAG_PASSIVEFORM_ELEMENT: str = 'passiveForm'
    _XMLID_TAG_SUMMONER_ELEMENT: str = 'summoner'

    def __init__(self) -> None:
        super().__init__()
        self.summoner: str = ''  # Element
        self.passiveForm: Optional[str] = None  # Element

    def overrideAttrs(self, passiveForm_attribute: _Optional_str = None, summoner_attribute: _Optional_str = None) -> None:
        if passiveForm_attribute is not None:
            self._XMLID_ATTR_PASSIVEFORM_ATTRIBUTE = passiveForm_attribute
        if summoner_attribute is not None:
            self._XMLID_ATTR_SUMMONER_ATTRIBUTE = summoner_attribute

    def overrideTags(self, passiveForm_element: _Optional_str = None, summoner_element: _Optional_str = None) -> None:
        if passiveForm_element is not None:
            self._XMLID_TAG_PASSIVEFORM_ELEMENT = passiveForm_element
        if summoner_element is not None:
            self._XMLID_TAG_SUMMONER_ELEMENT = summoner_element

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_SUMMONER_ELEMENT)) is not None:
            if self._XMLID_ATTR_SUMMONER_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_SUMMONER_ATTRIBUTE, 'summoner')
            self.summoner = sf.attrib[self._XMLID_ATTR_SUMMONER_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'summoner', self._XMLID_TAG_SUMMONER_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_PASSIVEFORM_ELEMENT)) is not None:
            self.passiveForm = sf.attrib.get(self._XMLID_ATTR_PASSIVEFORM_ATTRIBUTE, None)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        etree.SubElement(e, self._XMLID_TAG_SUMMONER_ELEMENT, {self._XMLID_ATTR_SUMMONER_ATTRIBUTE: str(self.summoner)})
        if self.passiveForm is not None:
            etree.SubElement(e, self._XMLID_TAG_PASSIVEFORM_ELEMENT, {self._XMLID_ATTR_PASSIVEFORM_ATTRIBUTE: str(self.passiveForm)})
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['summoner'] = str(self.summoner)
        if self.passiveForm is not None:
            data['passiveForm'] = str(self.passiveForm)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('summoner')) is not None:
            self.summoner = data['summoner']
        else:
            raise KeyRequiredException(self, data, 'summoner', 'summoner')
        if (sf := data.get('passiveForm')) is not None:
            self.passiveForm = data['passiveForm']
        else:
            self.passiveForm = None

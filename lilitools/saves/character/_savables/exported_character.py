#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawExportedCharacter']
## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public Element saveAsXML(Element parentElement, Document doc) { @ Ra1rfw7eKd5WT2JXoGa/zLFvb/XgZlpPNZbY2BZwgdTA/t0DkASy5ADAtBbX2DvSTxI43cQAGTPbPsMVcAHyRQ==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawExportedCharacter(Savable):
    TAG = 'exportedCharacter'
    _XMLID_ATTR_CHARACTER_ATTRIBUTE: str = 'value'
    _XMLID_TAG_CHARACTER_ELEMENT: str = 'character'

    def __init__(self) -> None:
        super().__init__()
        self.character: GameCharacter = GameCharacter()  # Element

    def overrideAttrs(self, character_attribute: _Optional_str = None) -> None:
        if character_attribute is not None:
            self._XMLID_ATTR_CHARACTER_ATTRIBUTE = character_attribute

    def overrideTags(self, character_element: _Optional_str = None) -> None:
        if character_element is not None:
            self._XMLID_TAG_CHARACTER_ELEMENT = character_element

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_CHARACTER_ELEMENT)) is not None:
            self.character = GameCharacter()
            self.character.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'character', self._XMLID_TAG_CHARACTER_ELEMENT)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.append(self.character.toXML(self._XMLID_TAG_CHARACTER_ELEMENT))
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['character'] = self.character.toDict()
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('character')) is not None:
            self.character = GameCharacter()
            self.character.fromDict(data['character'])
        else:
            raise KeyRequiredException(self, data, 'character', 'character')

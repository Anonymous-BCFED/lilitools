#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawFamily']
## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public Element saveAsXML(Element parentElement, Document doc) { @ Ra1rfw7eKd5WT2JXoGa/zLFvb/XgZlpPNZbY2BZwgdTA/t0DkASy5ADAtBbX2DvSTxI43cQAGTPbPsMVcAHyRQ==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawFamily(Savable):
    TAG = 'family'
    _XMLID_ATTR_DAYOFCONCEPTION_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_FATHERFEMININITY_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_FATHERID_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_FATHERNAME_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_FATHERSUBSPECIES_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_INCUBATORFEMININITY_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_INCUBATORID_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_INCUBATORNAME_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_INCUBATORSUBSPECIES_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_MONTHOFCONCEPTION_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_MOTHERFEMININITY_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_MOTHERID_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_MOTHERNAME_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_MOTHERSUBSPECIES_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_YEAROFCONCEPTION_ATTRIBUTE: str = 'value'
    _XMLID_TAG_DAYOFCONCEPTION_ELEMENT: str = 'dayOfConception'
    _XMLID_TAG_FATHERFEMININITY_ELEMENT: str = 'fatherFemininity'
    _XMLID_TAG_FATHERID_ELEMENT: str = 'fatherId'
    _XMLID_TAG_FATHERNAME_ELEMENT: str = 'fatherName'
    _XMLID_TAG_FATHERSUBSPECIES_ELEMENT: str = 'fatherSubspecies'
    _XMLID_TAG_INCUBATORFEMININITY_ELEMENT: str = 'incubatorFemininity'
    _XMLID_TAG_INCUBATORID_ELEMENT: str = 'incubatorId'
    _XMLID_TAG_INCUBATORNAME_ELEMENT: str = 'incubatorName'
    _XMLID_TAG_INCUBATORSUBSPECIES_ELEMENT: str = 'incubatorSubspecies'
    _XMLID_TAG_MONTHOFCONCEPTION_ELEMENT: str = 'monthOfConception'
    _XMLID_TAG_MOTHERFEMININITY_ELEMENT: str = 'motherFemininity'
    _XMLID_TAG_MOTHERID_ELEMENT: str = 'motherId'
    _XMLID_TAG_MOTHERNAME_ELEMENT: str = 'motherName'
    _XMLID_TAG_MOTHERSUBSPECIES_ELEMENT: str = 'motherSubspecies'
    _XMLID_TAG_YEAROFCONCEPTION_ELEMENT: str = 'yearOfConception'

    def __init__(self) -> None:
        super().__init__()
        self.motherId: str = ''  # Element
        self.motherName: str = ''  # Element
        self.motherFemininity: str = ''  # Element
        self.motherSubspecies: str = ''  # Element
        self.fatherId: str = ''  # Element
        self.fatherName: str = ''  # Element
        self.fatherFemininity: str = ''  # Element
        self.fatherSubspecies: str = ''  # Element
        self.incubatorId: Optional[str] = None  # Element
        self.incubatorName: Optional[str] = None  # Element
        self.incubatorFemininity: Optional[str] = None  # Element
        self.incubatorSubspecies: Optional[str] = None  # Element
        self.yearOfConception: int = 0  # Element
        self.monthOfConception: str = ''  # Element
        self.dayOfConception: int = 0  # Element

    def overrideAttrs(self, dayOfConception_attribute: _Optional_str = None, fatherFemininity_attribute: _Optional_str = None, fatherId_attribute: _Optional_str = None, fatherName_attribute: _Optional_str = None, fatherSubspecies_attribute: _Optional_str = None, incubatorFemininity_attribute: _Optional_str = None, incubatorId_attribute: _Optional_str = None, incubatorName_attribute: _Optional_str = None, incubatorSubspecies_attribute: _Optional_str = None, monthOfConception_attribute: _Optional_str = None, motherFemininity_attribute: _Optional_str = None, motherId_attribute: _Optional_str = None, motherName_attribute: _Optional_str = None, motherSubspecies_attribute: _Optional_str = None, yearOfConception_attribute: _Optional_str = None) -> None:
        if dayOfConception_attribute is not None:
            self._XMLID_ATTR_DAYOFCONCEPTION_ATTRIBUTE = dayOfConception_attribute
        if fatherFemininity_attribute is not None:
            self._XMLID_ATTR_FATHERFEMININITY_ATTRIBUTE = fatherFemininity_attribute
        if fatherId_attribute is not None:
            self._XMLID_ATTR_FATHERID_ATTRIBUTE = fatherId_attribute
        if fatherName_attribute is not None:
            self._XMLID_ATTR_FATHERNAME_ATTRIBUTE = fatherName_attribute
        if fatherSubspecies_attribute is not None:
            self._XMLID_ATTR_FATHERSUBSPECIES_ATTRIBUTE = fatherSubspecies_attribute
        if incubatorFemininity_attribute is not None:
            self._XMLID_ATTR_INCUBATORFEMININITY_ATTRIBUTE = incubatorFemininity_attribute
        if incubatorId_attribute is not None:
            self._XMLID_ATTR_INCUBATORID_ATTRIBUTE = incubatorId_attribute
        if incubatorName_attribute is not None:
            self._XMLID_ATTR_INCUBATORNAME_ATTRIBUTE = incubatorName_attribute
        if incubatorSubspecies_attribute is not None:
            self._XMLID_ATTR_INCUBATORSUBSPECIES_ATTRIBUTE = incubatorSubspecies_attribute
        if monthOfConception_attribute is not None:
            self._XMLID_ATTR_MONTHOFCONCEPTION_ATTRIBUTE = monthOfConception_attribute
        if motherFemininity_attribute is not None:
            self._XMLID_ATTR_MOTHERFEMININITY_ATTRIBUTE = motherFemininity_attribute
        if motherId_attribute is not None:
            self._XMLID_ATTR_MOTHERID_ATTRIBUTE = motherId_attribute
        if motherName_attribute is not None:
            self._XMLID_ATTR_MOTHERNAME_ATTRIBUTE = motherName_attribute
        if motherSubspecies_attribute is not None:
            self._XMLID_ATTR_MOTHERSUBSPECIES_ATTRIBUTE = motherSubspecies_attribute
        if yearOfConception_attribute is not None:
            self._XMLID_ATTR_YEAROFCONCEPTION_ATTRIBUTE = yearOfConception_attribute

    def overrideTags(self, dayOfConception_element: _Optional_str = None, fatherFemininity_element: _Optional_str = None, fatherId_element: _Optional_str = None, fatherName_element: _Optional_str = None, fatherSubspecies_element: _Optional_str = None, incubatorFemininity_element: _Optional_str = None, incubatorId_element: _Optional_str = None, incubatorName_element: _Optional_str = None, incubatorSubspecies_element: _Optional_str = None, monthOfConception_element: _Optional_str = None, motherFemininity_element: _Optional_str = None, motherId_element: _Optional_str = None, motherName_element: _Optional_str = None, motherSubspecies_element: _Optional_str = None, yearOfConception_element: _Optional_str = None) -> None:
        if dayOfConception_element is not None:
            self._XMLID_TAG_DAYOFCONCEPTION_ELEMENT = dayOfConception_element
        if fatherFemininity_element is not None:
            self._XMLID_TAG_FATHERFEMININITY_ELEMENT = fatherFemininity_element
        if fatherId_element is not None:
            self._XMLID_TAG_FATHERID_ELEMENT = fatherId_element
        if fatherName_element is not None:
            self._XMLID_TAG_FATHERNAME_ELEMENT = fatherName_element
        if fatherSubspecies_element is not None:
            self._XMLID_TAG_FATHERSUBSPECIES_ELEMENT = fatherSubspecies_element
        if incubatorFemininity_element is not None:
            self._XMLID_TAG_INCUBATORFEMININITY_ELEMENT = incubatorFemininity_element
        if incubatorId_element is not None:
            self._XMLID_TAG_INCUBATORID_ELEMENT = incubatorId_element
        if incubatorName_element is not None:
            self._XMLID_TAG_INCUBATORNAME_ELEMENT = incubatorName_element
        if incubatorSubspecies_element is not None:
            self._XMLID_TAG_INCUBATORSUBSPECIES_ELEMENT = incubatorSubspecies_element
        if monthOfConception_element is not None:
            self._XMLID_TAG_MONTHOFCONCEPTION_ELEMENT = monthOfConception_element
        if motherFemininity_element is not None:
            self._XMLID_TAG_MOTHERFEMININITY_ELEMENT = motherFemininity_element
        if motherId_element is not None:
            self._XMLID_TAG_MOTHERID_ELEMENT = motherId_element
        if motherName_element is not None:
            self._XMLID_TAG_MOTHERNAME_ELEMENT = motherName_element
        if motherSubspecies_element is not None:
            self._XMLID_TAG_MOTHERSUBSPECIES_ELEMENT = motherSubspecies_element
        if yearOfConception_element is not None:
            self._XMLID_TAG_YEAROFCONCEPTION_ELEMENT = yearOfConception_element

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_MOTHERID_ELEMENT)) is not None:
            if self._XMLID_ATTR_MOTHERID_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_MOTHERID_ATTRIBUTE, 'motherId')
            self.motherId = sf.attrib[self._XMLID_ATTR_MOTHERID_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'motherId', self._XMLID_TAG_MOTHERID_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_MOTHERNAME_ELEMENT)) is not None:
            if self._XMLID_ATTR_MOTHERNAME_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_MOTHERNAME_ATTRIBUTE, 'motherName')
            self.motherName = sf.attrib[self._XMLID_ATTR_MOTHERNAME_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'motherName', self._XMLID_TAG_MOTHERNAME_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_MOTHERFEMININITY_ELEMENT)) is not None:
            if self._XMLID_ATTR_MOTHERFEMININITY_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_MOTHERFEMININITY_ATTRIBUTE, 'motherFemininity')
            self.motherFemininity = sf.attrib[self._XMLID_ATTR_MOTHERFEMININITY_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'motherFemininity', self._XMLID_TAG_MOTHERFEMININITY_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_MOTHERSUBSPECIES_ELEMENT)) is not None:
            if self._XMLID_ATTR_MOTHERSUBSPECIES_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_MOTHERSUBSPECIES_ATTRIBUTE, 'motherSubspecies')
            self.motherSubspecies = sf.attrib[self._XMLID_ATTR_MOTHERSUBSPECIES_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'motherSubspecies', self._XMLID_TAG_MOTHERSUBSPECIES_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_FATHERID_ELEMENT)) is not None:
            if self._XMLID_ATTR_FATHERID_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_FATHERID_ATTRIBUTE, 'fatherId')
            self.fatherId = sf.attrib[self._XMLID_ATTR_FATHERID_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'fatherId', self._XMLID_TAG_FATHERID_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_FATHERNAME_ELEMENT)) is not None:
            if self._XMLID_ATTR_FATHERNAME_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_FATHERNAME_ATTRIBUTE, 'fatherName')
            self.fatherName = sf.attrib[self._XMLID_ATTR_FATHERNAME_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'fatherName', self._XMLID_TAG_FATHERNAME_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_FATHERFEMININITY_ELEMENT)) is not None:
            if self._XMLID_ATTR_FATHERFEMININITY_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_FATHERFEMININITY_ATTRIBUTE, 'fatherFemininity')
            self.fatherFemininity = sf.attrib[self._XMLID_ATTR_FATHERFEMININITY_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'fatherFemininity', self._XMLID_TAG_FATHERFEMININITY_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_FATHERSUBSPECIES_ELEMENT)) is not None:
            if self._XMLID_ATTR_FATHERSUBSPECIES_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_FATHERSUBSPECIES_ATTRIBUTE, 'fatherSubspecies')
            self.fatherSubspecies = sf.attrib[self._XMLID_ATTR_FATHERSUBSPECIES_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'fatherSubspecies', self._XMLID_TAG_FATHERSUBSPECIES_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_INCUBATORID_ELEMENT)) is not None:
            self.incubatorId = sf.attrib.get(self._XMLID_ATTR_INCUBATORID_ATTRIBUTE, None)
        if (sf := e.find(self._XMLID_TAG_INCUBATORNAME_ELEMENT)) is not None:
            self.incubatorName = sf.attrib.get(self._XMLID_ATTR_INCUBATORNAME_ATTRIBUTE, None)
        if (sf := e.find(self._XMLID_TAG_INCUBATORFEMININITY_ELEMENT)) is not None:
            self.incubatorFemininity = sf.attrib.get(self._XMLID_ATTR_INCUBATORFEMININITY_ATTRIBUTE, None)
        if (sf := e.find(self._XMLID_TAG_INCUBATORSUBSPECIES_ELEMENT)) is not None:
            self.incubatorSubspecies = sf.attrib.get(self._XMLID_ATTR_INCUBATORSUBSPECIES_ATTRIBUTE, None)
        if (sf := e.find(self._XMLID_TAG_YEAROFCONCEPTION_ELEMENT)) is not None:
            if self._XMLID_ATTR_YEAROFCONCEPTION_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_YEAROFCONCEPTION_ATTRIBUTE, 'yearOfConception')
            self.yearOfConception = int(sf.attrib[self._XMLID_ATTR_YEAROFCONCEPTION_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'yearOfConception', self._XMLID_TAG_YEAROFCONCEPTION_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_MONTHOFCONCEPTION_ELEMENT)) is not None:
            if self._XMLID_ATTR_MONTHOFCONCEPTION_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_MONTHOFCONCEPTION_ATTRIBUTE, 'monthOfConception')
            self.monthOfConception = sf.attrib[self._XMLID_ATTR_MONTHOFCONCEPTION_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'monthOfConception', self._XMLID_TAG_MONTHOFCONCEPTION_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_DAYOFCONCEPTION_ELEMENT)) is not None:
            if self._XMLID_ATTR_DAYOFCONCEPTION_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_DAYOFCONCEPTION_ATTRIBUTE, 'dayOfConception')
            self.dayOfConception = int(sf.attrib[self._XMLID_ATTR_DAYOFCONCEPTION_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'dayOfConception', self._XMLID_TAG_DAYOFCONCEPTION_ELEMENT)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        etree.SubElement(e, self._XMLID_TAG_MOTHERID_ELEMENT, {self._XMLID_ATTR_MOTHERID_ATTRIBUTE: str(self.motherId)})
        etree.SubElement(e, self._XMLID_TAG_MOTHERNAME_ELEMENT, {self._XMLID_ATTR_MOTHERNAME_ATTRIBUTE: str(self.motherName)})
        etree.SubElement(e, self._XMLID_TAG_MOTHERFEMININITY_ELEMENT, {self._XMLID_ATTR_MOTHERFEMININITY_ATTRIBUTE: str(self.motherFemininity)})
        etree.SubElement(e, self._XMLID_TAG_MOTHERSUBSPECIES_ELEMENT, {self._XMLID_ATTR_MOTHERSUBSPECIES_ATTRIBUTE: str(self.motherSubspecies)})
        etree.SubElement(e, self._XMLID_TAG_FATHERID_ELEMENT, {self._XMLID_ATTR_FATHERID_ATTRIBUTE: str(self.fatherId)})
        etree.SubElement(e, self._XMLID_TAG_FATHERNAME_ELEMENT, {self._XMLID_ATTR_FATHERNAME_ATTRIBUTE: str(self.fatherName)})
        etree.SubElement(e, self._XMLID_TAG_FATHERFEMININITY_ELEMENT, {self._XMLID_ATTR_FATHERFEMININITY_ATTRIBUTE: str(self.fatherFemininity)})
        etree.SubElement(e, self._XMLID_TAG_FATHERSUBSPECIES_ELEMENT, {self._XMLID_ATTR_FATHERSUBSPECIES_ATTRIBUTE: str(self.fatherSubspecies)})
        if self.incubatorId is not None:
            etree.SubElement(e, self._XMLID_TAG_INCUBATORID_ELEMENT, {self._XMLID_ATTR_INCUBATORID_ATTRIBUTE: str(self.incubatorId)})
        if self.incubatorName is not None:
            etree.SubElement(e, self._XMLID_TAG_INCUBATORNAME_ELEMENT, {self._XMLID_ATTR_INCUBATORNAME_ATTRIBUTE: str(self.incubatorName)})
        if self.incubatorFemininity is not None:
            etree.SubElement(e, self._XMLID_TAG_INCUBATORFEMININITY_ELEMENT, {self._XMLID_ATTR_INCUBATORFEMININITY_ATTRIBUTE: str(self.incubatorFemininity)})
        if self.incubatorSubspecies is not None:
            etree.SubElement(e, self._XMLID_TAG_INCUBATORSUBSPECIES_ELEMENT, {self._XMLID_ATTR_INCUBATORSUBSPECIES_ATTRIBUTE: str(self.incubatorSubspecies)})
        etree.SubElement(e, self._XMLID_TAG_YEAROFCONCEPTION_ELEMENT, {self._XMLID_ATTR_YEAROFCONCEPTION_ATTRIBUTE: str(self.yearOfConception)})
        etree.SubElement(e, self._XMLID_TAG_MONTHOFCONCEPTION_ELEMENT, {self._XMLID_ATTR_MONTHOFCONCEPTION_ATTRIBUTE: str(self.monthOfConception)})
        etree.SubElement(e, self._XMLID_TAG_DAYOFCONCEPTION_ELEMENT, {self._XMLID_ATTR_DAYOFCONCEPTION_ATTRIBUTE: str(self.dayOfConception)})
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['motherId'] = str(self.motherId)
        data['motherName'] = str(self.motherName)
        data['motherFemininity'] = str(self.motherFemininity)
        data['motherSubspecies'] = str(self.motherSubspecies)
        data['fatherId'] = str(self.fatherId)
        data['fatherName'] = str(self.fatherName)
        data['fatherFemininity'] = str(self.fatherFemininity)
        data['fatherSubspecies'] = str(self.fatherSubspecies)
        if self.incubatorId is not None:
            data['incubatorId'] = str(self.incubatorId)
        if self.incubatorName is not None:
            data['incubatorName'] = str(self.incubatorName)
        if self.incubatorFemininity is not None:
            data['incubatorFemininity'] = str(self.incubatorFemininity)
        if self.incubatorSubspecies is not None:
            data['incubatorSubspecies'] = str(self.incubatorSubspecies)
        data['yearOfConception'] = int(self.yearOfConception)
        data['monthOfConception'] = str(self.monthOfConception)
        data['dayOfConception'] = int(self.dayOfConception)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('motherId')) is not None:
            self.motherId = data['motherId']
        else:
            raise KeyRequiredException(self, data, 'motherId', 'motherId')
        if (sf := data.get('motherName')) is not None:
            self.motherName = data['motherName']
        else:
            raise KeyRequiredException(self, data, 'motherName', 'motherName')
        if (sf := data.get('motherFemininity')) is not None:
            self.motherFemininity = data['motherFemininity']
        else:
            raise KeyRequiredException(self, data, 'motherFemininity', 'motherFemininity')
        if (sf := data.get('motherSubspecies')) is not None:
            self.motherSubspecies = data['motherSubspecies']
        else:
            raise KeyRequiredException(self, data, 'motherSubspecies', 'motherSubspecies')
        if (sf := data.get('fatherId')) is not None:
            self.fatherId = data['fatherId']
        else:
            raise KeyRequiredException(self, data, 'fatherId', 'fatherId')
        if (sf := data.get('fatherName')) is not None:
            self.fatherName = data['fatherName']
        else:
            raise KeyRequiredException(self, data, 'fatherName', 'fatherName')
        if (sf := data.get('fatherFemininity')) is not None:
            self.fatherFemininity = data['fatherFemininity']
        else:
            raise KeyRequiredException(self, data, 'fatherFemininity', 'fatherFemininity')
        if (sf := data.get('fatherSubspecies')) is not None:
            self.fatherSubspecies = data['fatherSubspecies']
        else:
            raise KeyRequiredException(self, data, 'fatherSubspecies', 'fatherSubspecies')
        if (sf := data.get('incubatorId')) is not None:
            self.incubatorId = data['incubatorId']
        else:
            self.incubatorId = None
        if (sf := data.get('incubatorName')) is not None:
            self.incubatorName = data['incubatorName']
        else:
            self.incubatorName = None
        if (sf := data.get('incubatorFemininity')) is not None:
            self.incubatorFemininity = data['incubatorFemininity']
        else:
            self.incubatorFemininity = None
        if (sf := data.get('incubatorSubspecies')) is not None:
            self.incubatorSubspecies = data['incubatorSubspecies']
        else:
            self.incubatorSubspecies = None
        if (sf := data.get('yearOfConception')) is not None:
            self.yearOfConception = data['yearOfConception']
        else:
            raise KeyRequiredException(self, data, 'yearOfConception', 'yearOfConception')
        if (sf := data.get('monthOfConception')) is not None:
            self.monthOfConception = data['monthOfConception']
        else:
            raise KeyRequiredException(self, data, 'monthOfConception', 'monthOfConception')
        if (sf := data.get('dayOfConception')) is not None:
            self.dayOfConception = data['dayOfConception']
        else:
            raise KeyRequiredException(self, data, 'dayOfConception', 'dayOfConception')

#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.character.enums.fetish_desire import EFetishDesire
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL, Savable
__all__ = ['RawFetishEntry']
## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public Element saveAsXML(Element parentElement, Document doc) { @ Ra1rfw7eKd5WT2JXoGa/zLFvb/XgZlpPNZbY2BZwgdTA/t0DkASy5ADAtBbX2DvSTxI43cQAGTPbPsMVcAHyRQ==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawFetishEntry(Savable):
    TAG = 'f'
    _XMLID_ATTR_DESIRE_ATTRIBUTE: str = 'desire'
    _XMLID_ATTR_HASFETISH_ATTRIBUTE: str = 'o'
    _XMLID_ATTR_XP_ATTRIBUTE: str = 'xp'

    def __init__(self) -> None:
        super().__init__()
        self.hasFetish: bool = False
        self.desire: EFetishDesire = EFetishDesire(2)
        self.xp: int = 0
        self.id: str = ''  # INNERTEXT

    def overrideAttrs(self, desire_attribute: _Optional_str = None, hasFetish_attribute: _Optional_str = None, xp_attribute: _Optional_str = None) -> None:
        if desire_attribute is not None:
            self._XMLID_ATTR_DESIRE_ATTRIBUTE = desire_attribute
        if hasFetish_attribute is not None:
            self._XMLID_ATTR_HASFETISH_ATTRIBUTE = hasFetish_attribute
        if xp_attribute is not None:
            self._XMLID_ATTR_XP_ATTRIBUTE = xp_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_HASFETISH_ATTRIBUTE, None)
        if value is None:
            self.hasFetish = False
        else:
            self.hasFetish = XML2BOOL[value]
        value = e.get(self._XMLID_ATTR_DESIRE_ATTRIBUTE, None)
        if value is None:
            self.desire = EFetishDesire(2)
        else:
            self.desire = EFetishDesire(int(value))
        value = e.get(self._XMLID_ATTR_XP_ATTRIBUTE, None)
        if value is None:
            self.xp = 0
        else:
            self.xp = int(value)
        self.id = e.text

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        if self.hasFetish != False:
            e.attrib[self._XMLID_ATTR_HASFETISH_ATTRIBUTE] = str(self.hasFetish).lower()
        if self.desire != EFetishDesire(2):
            e.attrib[self._XMLID_ATTR_DESIRE_ATTRIBUTE] = str(self.desire.value)
        if self.xp != 0:
            e.attrib[self._XMLID_ATTR_XP_ATTRIBUTE] = str(self.xp)
        e.text = str(self.id)
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        if self.hasFetish != False:
            data['hasFetish'] = bool(self.hasFetish)
        if self.desire != EFetishDesire(2):
            data['desire'] = self.desire.value
        if self.xp != 0:
            data['xp'] = int(self.xp)
        data = str(self.id)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        self.hasFetish = bool(data['hasFetish']) if 'hasFetish' in data else None
        self.desire = EFetishDesire(int(data['desire'])) if 'desire' in data else None
        self.xp = int(data['xp']) if 'xp' in data else None
        self.id = data

#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from collections import OrderedDict
from typing import Any, Dict, List, Optional, Set
from lxml import etree
from lilitools.saves.character.active_status_effect import ActiveStatusEffect
from lilitools.saves.character.addiction.addictions_core import AddictionsCore
from lilitools.saves.character.artwork_override import ArtworkOverride
from lilitools.saves.character.body.body import Body
from lilitools.saves.character.body.covering import Covering
from lilitools.saves.character.body.scar import Scar
from lilitools.saves.character.body.tattoos.tattoo import Tattoo
from lilitools.saves.character.companions import Companions
from lilitools.saves.character.core import Core
from lilitools.saves.character.enums.attribute import EAttribute
from lilitools.saves.character.family import Family
from lilitools.saves.character.fetish_entry import FetishEntry
from lilitools.saves.character.location_information import LocationInformation
from lilitools.saves.character.mods.helpful_info import HelpfulCharacterInformation
from lilitools.saves.character.outfit import Outfit
from lilitools.saves.character.pregnancy.pregnancy import Pregnancy
from lilitools.saves.character.slavery_info import SlaveryInfo
from lilitools.saves.character.stats.sex_stats import SexStats
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.inventory.character_inventory import CharacterInventory
from lilitools.saves.items.abstract_clothing import AbstractClothing
from lilitools.saves.savable import Savable
__all__ = ['RawGameCharacter']
## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public Element saveAsXML(Element parentElement, Document doc) { @ Ra1rfw7eKd5WT2JXoGa/zLFvb/XgZlpPNZbY2BZwgdTA/t0DkASy5ADAtBbX2DvSTxI43cQAGTPbPsMVcAHyRQ==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawGameCharacter(Savable):
    TAG = 'character'
    _XMLID_ATTR_ADDICTIONSCORE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_ARTWORK_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_ATTRIBUTES_KEYATTR: str = 'type'
    _XMLID_ATTR_ATTRIBUTES_VALUEATTR: str = 'value'
    _XMLID_ATTR_BODY_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_CHARACTERINVENTORY_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_COMPANIONS_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_CORE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_EQUIPPEDMOVES_VALUEATTR: str = 'type'
    _XMLID_ATTR_FAMILY_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_HELPFULINFORMATION_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_KNOWNMOVES_VALUEATTR: str = 'type'
    _XMLID_ATTR_KNOWNSPELLS_VALUEATTR: str = 'type'
    _XMLID_ATTR_LIPSTICKMARKS_KEYATTR: str = 'slot'
    _XMLID_ATTR_LOCATIONINFORMATION_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_POTIONATTRIBUTES_KEYATTR: str = 'type'
    _XMLID_ATTR_POTIONATTRIBUTES_VALUEATTR: str = 'value'
    _XMLID_ATTR_PREGNANCY_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_RELATIONSHIPS_KEYATTR: str = 'character'
    _XMLID_ATTR_RELATIONSHIPS_VALUEATTR: str = 'value'
    _XMLID_ATTR_SCARS_KEYATTR: str = 'slot'
    _XMLID_ATTR_SEXSTATS_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_SLAVERY_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_SPECIALPERKS_VALUEATTR: str = 'type'
    _XMLID_ATTR_SPELLUPGRADEPOINTS_KEYATTR: str = 'school'
    _XMLID_ATTR_SPELLUPGRADEPOINTS_VALUEATTR: str = 'points'
    _XMLID_ATTR_SPELLUPGRADES_VALUEATTR: str = 'type'
    _XMLID_ATTR_TATTOOS_KEYATTR: str = 'slot'
    _XMLID_ATTR_TRAITS_VALUEATTR: str = 'type'
    _XMLID_TAG_ADDICTIONSCORE_ELEMENT: str = 'addictionsCore'
    _XMLID_TAG_ARTWORK_ELEMENT: str = 'artwork'
    _XMLID_TAG_ATTRIBUTES_CHILD: str = 'attribute'
    _XMLID_TAG_ATTRIBUTES_PARENT: str = 'attributes'
    _XMLID_TAG_BODY_ELEMENT: str = 'body'
    _XMLID_TAG_CHARACTERINVENTORY_ELEMENT: str = 'characterInventory'
    _XMLID_TAG_COMPANIONS_ELEMENT: str = 'companions'
    _XMLID_TAG_CORE_ELEMENT: str = 'core'
    _XMLID_TAG_EQUIPPEDMOVES_CHILD: str = 'move'
    _XMLID_TAG_EQUIPPEDMOVES_PARENT: str = 'equippedMoves'
    _XMLID_TAG_FAMILY_ELEMENT: str = 'family'
    _XMLID_TAG_HELPFULINFORMATION_ELEMENT: str = 'helpfulInformation'
    _XMLID_TAG_HOLDINGCLOTHING_PARENT: str = 'holdingClothing'
    _XMLID_TAG_HOLDINGCLOTHING_VALUEELEM: Optional[str] = None
    _XMLID_TAG_KNOWNMOVES_CHILD: str = 'move'
    _XMLID_TAG_KNOWNMOVES_PARENT: str = 'knownMoves'
    _XMLID_TAG_KNOWNSPELLS_CHILD: str = 'spell'
    _XMLID_TAG_KNOWNSPELLS_PARENT: str = 'knownSpells'
    _XMLID_TAG_LIPSTICKMARKS_CHILD: str = 'lipstickEntry'
    _XMLID_TAG_LIPSTICKMARKS_PARENT: str = 'lipstickMarks'
    _XMLID_TAG_LIPSTICKMARKS_VALUEELEM: str = 'covering'
    _XMLID_TAG_LOCATIONINFORMATION_ELEMENT: str = 'locationInformation'
    _XMLID_TAG_POTIONATTRIBUTES_CHILD: str = 'attribute'
    _XMLID_TAG_POTIONATTRIBUTES_PARENT: str = 'potionAttributes'
    _XMLID_TAG_PREGNANCY_ELEMENT: str = 'pregnancy'
    _XMLID_TAG_RELATIONSHIPS_CHILD: str = 'relationship'
    _XMLID_TAG_RELATIONSHIPS_PARENT: str = 'characterRelationships'
    _XMLID_TAG_SAVEDOUTFITS_PARENT: str = 'savedOutfits'
    _XMLID_TAG_SAVEDOUTFITS_VALUEELEM: Optional[str] = None
    _XMLID_TAG_SCARS_CHILD: str = 'scarEntry'
    _XMLID_TAG_SCARS_PARENT: str = 'scars'
    _XMLID_TAG_SCARS_VALUEELEM: str = 'scar'
    _XMLID_TAG_SEXSTATS_ELEMENT: str = 'sexStats'
    _XMLID_TAG_SLAVERY_ELEMENT: str = 'slavery'
    _XMLID_TAG_SPECIALPERKS_CHILD: str = 'perk'
    _XMLID_TAG_SPECIALPERKS_PARENT: str = 'specialPerks'
    _XMLID_TAG_SPELLUPGRADEPOINTS_CHILD: str = 'upgradeEntry'
    _XMLID_TAG_SPELLUPGRADEPOINTS_PARENT: str = 'spellUpgradePoints'
    _XMLID_TAG_SPELLUPGRADES_CHILD: str = 'upgrade'
    _XMLID_TAG_SPELLUPGRADES_PARENT: str = 'spellUpgrades'
    _XMLID_TAG_STATUSEFFECTS_CHILD: str = 'statusEffect'
    _XMLID_TAG_STATUSEFFECTS_PARENT: str = 'statusEffects'
    _XMLID_TAG_STATUSEFFECTS_VALUEELEM: Optional[str] = None
    _XMLID_TAG_TATTOOS_CHILD: str = 'tattooEntry'
    _XMLID_TAG_TATTOOS_PARENT: str = 'tattoos'
    _XMLID_TAG_TATTOOS_VALUEELEM: str = 'tattoo'
    _XMLID_TAG_TRAITS_CHILD: str = 'perk'
    _XMLID_TAG_TRAITS_PARENT: str = 'traits'

    def __init__(self) -> None:
        super().__init__()
        self.core: Core = Core()  # Element
        self.locationInformation: LocationInformation = LocationInformation()  # Element
        self.body: Body = Body()  # Element
        self.characterInventory: CharacterInventory = CharacterInventory()  # Element
        self.holdingClothing: Optional[List[AbstractClothing]] = list()
        self.savedOutfits: Optional[List[Outfit]] = list()
        self.tattoos: Dict[str, Tattoo] = {}
        self.scars: Dict[str, Scar] = {}
        self.lipstickMarks: Dict[str, Covering] = {}
        self.attributes: Dict[EAttribute, float] = {}
        self.potionAttributes: Dict[str, float] = {}
        self.traits: Set[str] = set()
        self.specialPerks: Set[str] = set()
        self.perks: OrderedDict[int, List[str]] = OrderedDict()
        self.knownSpells: Set[str] = set()
        self.spellUpgrades: Set[str] = set()
        self.spellUpgradePoints: Dict[str, int] = {}
        self.fetishes: OrderedDict[str, FetishEntry] = OrderedDict()
        self.statusEffects: Set[ActiveStatusEffect] = set()
        self.knownMoves: Set[str] = set()
        self.equippedMoves: List[str] = list()
        self.relationships: Dict[str, float] = {}
        self.pregnancy: Pregnancy = Pregnancy()  # Element
        self.family: Family = Family()  # Element
        self.slavery: SlaveryInfo = SlaveryInfo()  # Element
        self.companions: Companions = Companions()  # Element
        self.sexStats: SexStats = SexStats()  # Element
        self.addictionsCore: AddictionsCore = AddictionsCore()  # Element
        self.artwork: Optional[ArtworkOverride] = None  # Element
        self.helpfulInformation: Optional[HelpfulCharacterInformation] = None  # Element

    def overrideAttrs(self, addictionsCore_attribute: _Optional_str = None, artwork_attribute: _Optional_str = None, attributes_keyattr: _Optional_str = None, attributes_valueattr: _Optional_str = None, body_attribute: _Optional_str = None, characterInventory_attribute: _Optional_str = None, companions_attribute: _Optional_str = None, core_attribute: _Optional_str = None, equippedMoves_valueattr: _Optional_str = None, family_attribute: _Optional_str = None, helpfulInformation_attribute: _Optional_str = None, knownMoves_valueattr: _Optional_str = None, knownSpells_valueattr: _Optional_str = None, lipstickMarks_keyattr: _Optional_str = None, locationInformation_attribute: _Optional_str = None, potionAttributes_keyattr: _Optional_str = None, potionAttributes_valueattr: _Optional_str = None, pregnancy_attribute: _Optional_str = None, relationships_keyattr: _Optional_str = None, relationships_valueattr: _Optional_str = None, scars_keyattr: _Optional_str = None, sexStats_attribute: _Optional_str = None, slavery_attribute: _Optional_str = None, specialPerks_valueattr: _Optional_str = None, spellUpgradePoints_keyattr: _Optional_str = None, spellUpgradePoints_valueattr: _Optional_str = None, spellUpgrades_valueattr: _Optional_str = None, tattoos_keyattr: _Optional_str = None, traits_valueattr: _Optional_str = None) -> None:
        if addictionsCore_attribute is not None:
            self._XMLID_ATTR_ADDICTIONSCORE_ATTRIBUTE = addictionsCore_attribute
        if artwork_attribute is not None:
            self._XMLID_ATTR_ARTWORK_ATTRIBUTE = artwork_attribute
        if attributes_keyattr is not None:
            self._XMLID_ATTR_ATTRIBUTES_KEYATTR = attributes_keyattr
        if attributes_valueattr is not None:
            self._XMLID_ATTR_ATTRIBUTES_VALUEATTR = attributes_valueattr
        if body_attribute is not None:
            self._XMLID_ATTR_BODY_ATTRIBUTE = body_attribute
        if characterInventory_attribute is not None:
            self._XMLID_ATTR_CHARACTERINVENTORY_ATTRIBUTE = characterInventory_attribute
        if companions_attribute is not None:
            self._XMLID_ATTR_COMPANIONS_ATTRIBUTE = companions_attribute
        if core_attribute is not None:
            self._XMLID_ATTR_CORE_ATTRIBUTE = core_attribute
        if equippedMoves_valueattr is not None:
            self._XMLID_ATTR_EQUIPPEDMOVES_VALUEATTR = equippedMoves_valueattr
        if family_attribute is not None:
            self._XMLID_ATTR_FAMILY_ATTRIBUTE = family_attribute
        if helpfulInformation_attribute is not None:
            self._XMLID_ATTR_HELPFULINFORMATION_ATTRIBUTE = helpfulInformation_attribute
        if knownMoves_valueattr is not None:
            self._XMLID_ATTR_KNOWNMOVES_VALUEATTR = knownMoves_valueattr
        if knownSpells_valueattr is not None:
            self._XMLID_ATTR_KNOWNSPELLS_VALUEATTR = knownSpells_valueattr
        if lipstickMarks_keyattr is not None:
            self._XMLID_ATTR_LIPSTICKMARKS_KEYATTR = lipstickMarks_keyattr
        if locationInformation_attribute is not None:
            self._XMLID_ATTR_LOCATIONINFORMATION_ATTRIBUTE = locationInformation_attribute
        if potionAttributes_keyattr is not None:
            self._XMLID_ATTR_POTIONATTRIBUTES_KEYATTR = potionAttributes_keyattr
        if potionAttributes_valueattr is not None:
            self._XMLID_ATTR_POTIONATTRIBUTES_VALUEATTR = potionAttributes_valueattr
        if pregnancy_attribute is not None:
            self._XMLID_ATTR_PREGNANCY_ATTRIBUTE = pregnancy_attribute
        if relationships_keyattr is not None:
            self._XMLID_ATTR_RELATIONSHIPS_KEYATTR = relationships_keyattr
        if relationships_valueattr is not None:
            self._XMLID_ATTR_RELATIONSHIPS_VALUEATTR = relationships_valueattr
        if scars_keyattr is not None:
            self._XMLID_ATTR_SCARS_KEYATTR = scars_keyattr
        if sexStats_attribute is not None:
            self._XMLID_ATTR_SEXSTATS_ATTRIBUTE = sexStats_attribute
        if slavery_attribute is not None:
            self._XMLID_ATTR_SLAVERY_ATTRIBUTE = slavery_attribute
        if specialPerks_valueattr is not None:
            self._XMLID_ATTR_SPECIALPERKS_VALUEATTR = specialPerks_valueattr
        if spellUpgradePoints_keyattr is not None:
            self._XMLID_ATTR_SPELLUPGRADEPOINTS_KEYATTR = spellUpgradePoints_keyattr
        if spellUpgradePoints_valueattr is not None:
            self._XMLID_ATTR_SPELLUPGRADEPOINTS_VALUEATTR = spellUpgradePoints_valueattr
        if spellUpgrades_valueattr is not None:
            self._XMLID_ATTR_SPELLUPGRADES_VALUEATTR = spellUpgrades_valueattr
        if tattoos_keyattr is not None:
            self._XMLID_ATTR_TATTOOS_KEYATTR = tattoos_keyattr
        if traits_valueattr is not None:
            self._XMLID_ATTR_TRAITS_VALUEATTR = traits_valueattr

    def overrideTags(self, addictionsCore_element: _Optional_str = None, artwork_element: _Optional_str = None, attributes_child: _Optional_str = None, attributes_parent: _Optional_str = None, body_element: _Optional_str = None, characterInventory_element: _Optional_str = None, companions_element: _Optional_str = None, core_element: _Optional_str = None, equippedMoves_child: _Optional_str = None, equippedMoves_parent: _Optional_str = None, family_element: _Optional_str = None, helpfulInformation_element: _Optional_str = None, holdingClothing_parent: _Optional_str = None, holdingClothing_valueelem: _Optional_str = None, knownMoves_child: _Optional_str = None, knownMoves_parent: _Optional_str = None, knownSpells_child: _Optional_str = None, knownSpells_parent: _Optional_str = None, lipstickMarks_child: _Optional_str = None, lipstickMarks_parent: _Optional_str = None, lipstickMarks_valueelem: _Optional_str = None, locationInformation_element: _Optional_str = None, potionAttributes_child: _Optional_str = None, potionAttributes_parent: _Optional_str = None, pregnancy_element: _Optional_str = None, relationships_child: _Optional_str = None, relationships_parent: _Optional_str = None, savedOutfits_parent: _Optional_str = None, savedOutfits_valueelem: _Optional_str = None, scars_child: _Optional_str = None, scars_parent: _Optional_str = None, scars_valueelem: _Optional_str = None, sexStats_element: _Optional_str = None, slavery_element: _Optional_str = None, specialPerks_child: _Optional_str = None, specialPerks_parent: _Optional_str = None, spellUpgradePoints_child: _Optional_str = None, spellUpgradePoints_parent: _Optional_str = None, spellUpgrades_child: _Optional_str = None, spellUpgrades_parent: _Optional_str = None, statusEffects_child: _Optional_str = None, statusEffects_parent: _Optional_str = None, statusEffects_valueelem: _Optional_str = None, tattoos_child: _Optional_str = None, tattoos_parent: _Optional_str = None, tattoos_valueelem: _Optional_str = None, traits_child: _Optional_str = None, traits_parent: _Optional_str = None) -> None:
        if addictionsCore_element is not None:
            self._XMLID_TAG_ADDICTIONSCORE_ELEMENT = addictionsCore_element
        if artwork_element is not None:
            self._XMLID_TAG_ARTWORK_ELEMENT = artwork_element
        if attributes_child is not None:
            self._XMLID_TAG_ATTRIBUTES_CHILD = attributes_child
        if attributes_parent is not None:
            self._XMLID_TAG_ATTRIBUTES_PARENT = attributes_parent
        if body_element is not None:
            self._XMLID_TAG_BODY_ELEMENT = body_element
        if characterInventory_element is not None:
            self._XMLID_TAG_CHARACTERINVENTORY_ELEMENT = characterInventory_element
        if companions_element is not None:
            self._XMLID_TAG_COMPANIONS_ELEMENT = companions_element
        if core_element is not None:
            self._XMLID_TAG_CORE_ELEMENT = core_element
        if equippedMoves_child is not None:
            self._XMLID_TAG_EQUIPPEDMOVES_CHILD = equippedMoves_child
        if equippedMoves_parent is not None:
            self._XMLID_TAG_EQUIPPEDMOVES_PARENT = equippedMoves_parent
        if family_element is not None:
            self._XMLID_TAG_FAMILY_ELEMENT = family_element
        if helpfulInformation_element is not None:
            self._XMLID_TAG_HELPFULINFORMATION_ELEMENT = helpfulInformation_element
        if holdingClothing_parent is not None:
            self._XMLID_TAG_HOLDINGCLOTHING_PARENT = holdingClothing_parent
        if holdingClothing_valueelem is not None:
            self._XMLID_TAG_HOLDINGCLOTHING_VALUEELEM = holdingClothing_valueelem
        if knownMoves_child is not None:
            self._XMLID_TAG_KNOWNMOVES_CHILD = knownMoves_child
        if knownMoves_parent is not None:
            self._XMLID_TAG_KNOWNMOVES_PARENT = knownMoves_parent
        if knownSpells_child is not None:
            self._XMLID_TAG_KNOWNSPELLS_CHILD = knownSpells_child
        if knownSpells_parent is not None:
            self._XMLID_TAG_KNOWNSPELLS_PARENT = knownSpells_parent
        if lipstickMarks_child is not None:
            self._XMLID_TAG_LIPSTICKMARKS_CHILD = lipstickMarks_child
        if lipstickMarks_parent is not None:
            self._XMLID_TAG_LIPSTICKMARKS_PARENT = lipstickMarks_parent
        if lipstickMarks_valueelem is not None:
            self._XMLID_TAG_LIPSTICKMARKS_VALUEELEM = lipstickMarks_valueelem
        if locationInformation_element is not None:
            self._XMLID_TAG_LOCATIONINFORMATION_ELEMENT = locationInformation_element
        if potionAttributes_child is not None:
            self._XMLID_TAG_POTIONATTRIBUTES_CHILD = potionAttributes_child
        if potionAttributes_parent is not None:
            self._XMLID_TAG_POTIONATTRIBUTES_PARENT = potionAttributes_parent
        if pregnancy_element is not None:
            self._XMLID_TAG_PREGNANCY_ELEMENT = pregnancy_element
        if relationships_child is not None:
            self._XMLID_TAG_RELATIONSHIPS_CHILD = relationships_child
        if relationships_parent is not None:
            self._XMLID_TAG_RELATIONSHIPS_PARENT = relationships_parent
        if savedOutfits_parent is not None:
            self._XMLID_TAG_SAVEDOUTFITS_PARENT = savedOutfits_parent
        if savedOutfits_valueelem is not None:
            self._XMLID_TAG_SAVEDOUTFITS_VALUEELEM = savedOutfits_valueelem
        if scars_child is not None:
            self._XMLID_TAG_SCARS_CHILD = scars_child
        if scars_parent is not None:
            self._XMLID_TAG_SCARS_PARENT = scars_parent
        if scars_valueelem is not None:
            self._XMLID_TAG_SCARS_VALUEELEM = scars_valueelem
        if sexStats_element is not None:
            self._XMLID_TAG_SEXSTATS_ELEMENT = sexStats_element
        if slavery_element is not None:
            self._XMLID_TAG_SLAVERY_ELEMENT = slavery_element
        if specialPerks_child is not None:
            self._XMLID_TAG_SPECIALPERKS_CHILD = specialPerks_child
        if specialPerks_parent is not None:
            self._XMLID_TAG_SPECIALPERKS_PARENT = specialPerks_parent
        if spellUpgradePoints_child is not None:
            self._XMLID_TAG_SPELLUPGRADEPOINTS_CHILD = spellUpgradePoints_child
        if spellUpgradePoints_parent is not None:
            self._XMLID_TAG_SPELLUPGRADEPOINTS_PARENT = spellUpgradePoints_parent
        if spellUpgrades_child is not None:
            self._XMLID_TAG_SPELLUPGRADES_CHILD = spellUpgrades_child
        if spellUpgrades_parent is not None:
            self._XMLID_TAG_SPELLUPGRADES_PARENT = spellUpgrades_parent
        if statusEffects_child is not None:
            self._XMLID_TAG_STATUSEFFECTS_CHILD = statusEffects_child
        if statusEffects_parent is not None:
            self._XMLID_TAG_STATUSEFFECTS_PARENT = statusEffects_parent
        if statusEffects_valueelem is not None:
            self._XMLID_TAG_STATUSEFFECTS_VALUEELEM = statusEffects_valueelem
        if tattoos_child is not None:
            self._XMLID_TAG_TATTOOS_CHILD = tattoos_child
        if tattoos_parent is not None:
            self._XMLID_TAG_TATTOOS_PARENT = tattoos_parent
        if tattoos_valueelem is not None:
            self._XMLID_TAG_TATTOOS_VALUEELEM = tattoos_valueelem
        if traits_child is not None:
            self._XMLID_TAG_TRAITS_CHILD = traits_child
        if traits_parent is not None:
            self._XMLID_TAG_TRAITS_PARENT = traits_parent

    def _fromXML_perks(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromXML_perks()')

    def _toXML_perks(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toXML_perks()')

    def _fromDict_perks(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromDict_perks()')

    def _toDict_perks(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toDict_perks()')

    def _fromXML_fetishes(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromXML_fetishes()')

    def _toXML_fetishes(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toXML_fetishes()')

    def _fromDict_fetishes(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromDict_fetishes()')

    def _toDict_fetishes(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toDict_fetishes()')

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        dc: etree._Element
        df: etree._Element
        dv: Savable
        lk: str
        lp: etree._Element
        lv: Savable
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_CORE_ELEMENT)) is not None:
            self.core = Core()
            self.core.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'core', self._XMLID_TAG_CORE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_LOCATIONINFORMATION_ELEMENT)) is not None:
            self.locationInformation = LocationInformation()
            self.locationInformation.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'locationInformation', self._XMLID_TAG_LOCATIONINFORMATION_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_BODY_ELEMENT)) is not None:
            self.body = Body()
            self.body.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'body', self._XMLID_TAG_BODY_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_CHARACTERINVENTORY_ELEMENT)) is not None:
            self.characterInventory = CharacterInventory()
            self.characterInventory.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'characterInventory', self._XMLID_TAG_CHARACTERINVENTORY_ELEMENT)
        if (lf := e.find(self._XMLID_TAG_HOLDINGCLOTHING_PARENT)) is not None:
            if lf is not None:
                for lc in lf:
                    lv = AbstractClothing()
                    lv.fromXML(lc)
                    self.holdingClothing.append(lv)
        if (lf := e.find(self._XMLID_TAG_SAVEDOUTFITS_PARENT)) is not None:
            if lf is not None:
                for lc in lf:
                    lv = Outfit()
                    lv.fromXML(lc)
                    self.savedOutfits.append(lv)
        if (df := e.find(self._XMLID_TAG_TATTOOS_PARENT)) is not None:
            for dc in df.iterfind(self._XMLID_TAG_TATTOOS_CHILD):
                dv = Tattoo()
                dv.fromXML(dc.find(self._XMLID_TAG_TATTOOS_VALUEELEM))
                self.tattoos[dc.attrib['slot']] = dv
        if (df := e.find(self._XMLID_TAG_SCARS_PARENT)) is not None:
            for dc in df.iterfind(self._XMLID_TAG_SCARS_CHILD):
                dv = Scar()
                dv.fromXML(dc.find(self._XMLID_TAG_SCARS_VALUEELEM))
                self.scars[dc.attrib['slot']] = dv
        if (df := e.find(self._XMLID_TAG_LIPSTICKMARKS_PARENT)) is not None:
            for dc in df.iterfind(self._XMLID_TAG_LIPSTICKMARKS_CHILD):
                dv = Covering()
                dv.fromXML(dc.find(self._XMLID_TAG_LIPSTICKMARKS_VALUEELEM))
                self.lipstickMarks[dc.attrib['slot']] = dv
        if (df := e.find(self._XMLID_TAG_ATTRIBUTES_PARENT)) is not None:
            for dc in df.iterfind(self._XMLID_TAG_ATTRIBUTES_CHILD):
                self.attributes[EAttribute[dc.attrib['type']]] = float(dc.attrib['value'])
        else:
            self.attributes = {}
        if (df := e.find(self._XMLID_TAG_POTIONATTRIBUTES_PARENT)) is not None:
            for dc in df.iterfind(self._XMLID_TAG_POTIONATTRIBUTES_CHILD):
                self.potionAttributes[dc.attrib['type']] = float(dc.attrib['value'])
        else:
            self.potionAttributes = {}
        if (lf := e.find(self._XMLID_TAG_TRAITS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_TRAITS_CHILD):
                    self.traits.add(lc.attrib[self._XMLID_ATTR_TRAITS_VALUEATTR])
        else:
            self.traits = set()
        if (lf := e.find(self._XMLID_TAG_SPECIALPERKS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_SPECIALPERKS_CHILD):
                    self.specialPerks.add(lc.attrib[self._XMLID_ATTR_SPECIALPERKS_VALUEATTR])
        else:
            self.specialPerks = set()
        self._fromXML_perks(e)
        if (lf := e.find(self._XMLID_TAG_KNOWNSPELLS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_KNOWNSPELLS_CHILD):
                    self.knownSpells.add(lc.attrib[self._XMLID_ATTR_KNOWNSPELLS_VALUEATTR])
        else:
            self.knownSpells = set()
        if (lf := e.find(self._XMLID_TAG_SPELLUPGRADES_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_SPELLUPGRADES_CHILD):
                    self.spellUpgrades.add(lc.attrib[self._XMLID_ATTR_SPELLUPGRADES_VALUEATTR])
        else:
            self.spellUpgrades = set()
        if (df := e.find(self._XMLID_TAG_SPELLUPGRADEPOINTS_PARENT)) is not None:
            for dc in df.iterfind(self._XMLID_TAG_SPELLUPGRADEPOINTS_CHILD):
                self.spellUpgradePoints[dc.attrib['school']] = int(dc.attrib['points'])
        else:
            self.spellUpgradePoints = {}
        self._fromXML_fetishes(e)
        if (lf := e.find(self._XMLID_TAG_STATUSEFFECTS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_STATUSEFFECTS_CHILD):
                    lv = ActiveStatusEffect()
                    lv.fromXML(lc)
                    self.statusEffects.add(lv)
        else:
            self.statusEffects = set()
        if (lf := e.find(self._XMLID_TAG_KNOWNMOVES_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_KNOWNMOVES_CHILD):
                    self.knownMoves.add(lc.attrib[self._XMLID_ATTR_KNOWNMOVES_VALUEATTR])
        else:
            self.knownMoves = set()
        if (lf := e.find(self._XMLID_TAG_EQUIPPEDMOVES_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_EQUIPPEDMOVES_CHILD):
                    self.equippedMoves.append(lc.attrib[self._XMLID_ATTR_EQUIPPEDMOVES_VALUEATTR])
        else:
            self.equippedMoves = list()
        if (df := e.find(self._XMLID_TAG_RELATIONSHIPS_PARENT)) is not None:
            for dc in df.iterfind(self._XMLID_TAG_RELATIONSHIPS_CHILD):
                self.relationships[dc.attrib['character']] = float(dc.attrib['value'])
        else:
            self.relationships = {}
        if (sf := e.find(self._XMLID_TAG_PREGNANCY_ELEMENT)) is not None:
            self.pregnancy = Pregnancy()
            self.pregnancy.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'pregnancy', self._XMLID_TAG_PREGNANCY_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_FAMILY_ELEMENT)) is not None:
            self.family = Family()
            self.family.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'family', self._XMLID_TAG_FAMILY_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_SLAVERY_ELEMENT)) is not None:
            self.slavery = SlaveryInfo()
            self.slavery.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'slavery', self._XMLID_TAG_SLAVERY_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_COMPANIONS_ELEMENT)) is not None:
            self.companions = Companions()
            self.companions.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'companions', self._XMLID_TAG_COMPANIONS_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_SEXSTATS_ELEMENT)) is not None:
            self.sexStats = SexStats()
            self.sexStats.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'sexStats', self._XMLID_TAG_SEXSTATS_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_ADDICTIONSCORE_ELEMENT)) is not None:
            self.addictionsCore = AddictionsCore()
            self.addictionsCore.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'addictionsCore', self._XMLID_TAG_ADDICTIONSCORE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_ARTWORK_ELEMENT)) is not None:
            self.artwork = ArtworkOverride()
            self.artwork.fromXML(sf)
        if (sf := e.find(self._XMLID_TAG_HELPFULINFORMATION_ELEMENT)) is not None:
            self.helpfulInformation = HelpfulCharacterInformation()
            self.helpfulInformation.fromXML(sf)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        dK: Any
        dV: Any
        dc: etree._Element
        dp: etree._Element
        dsc: etree._Element
        dv: Savable
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        e.append(self.core.toXML(self._XMLID_TAG_CORE_ELEMENT))
        e.append(self.locationInformation.toXML(self._XMLID_TAG_LOCATIONINFORMATION_ELEMENT))
        e.append(self.body.toXML(self._XMLID_TAG_BODY_ELEMENT))
        e.append(self.characterInventory.toXML(self._XMLID_TAG_CHARACTERINVENTORY_ELEMENT))
        if self.holdingClothing is not None and len(self.holdingClothing) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_HOLDINGCLOTHING_PARENT)
            for lv in self.holdingClothing:
                lp.append(lv.toXML())
        if self.savedOutfits is not None and len(self.savedOutfits) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_SAVEDOUTFITS_PARENT)
            for lv in self.savedOutfits:
                lp.append(lv.toXML())
        if self.tattoos is not None and len(self.tattoos) > 0:
            dp = etree.SubElement(e, self._XMLID_TAG_TATTOOS_PARENT)
            for dK, dV in sorted(self.tattoos.items(), key=lambda t: t[0]):
                dc = etree.SubElement(dp, self._XMLID_TAG_TATTOOS_CHILD)
                dc.attrib['slot'] = dK
                dc.append(dV.toXML(self._XMLID_TAG_TATTOOS_VALUEELEM))
        if self.scars is not None and len(self.scars) > 0:
            dp = etree.SubElement(e, self._XMLID_TAG_SCARS_PARENT)
            for dK, dV in sorted(self.scars.items(), key=lambda t: t[0]):
                dc = etree.SubElement(dp, self._XMLID_TAG_SCARS_CHILD)
                dc.attrib['slot'] = dK
                dc.append(dV.toXML(self._XMLID_TAG_SCARS_VALUEELEM))
        if self.lipstickMarks is not None and len(self.lipstickMarks) > 0:
            dp = etree.SubElement(e, self._XMLID_TAG_LIPSTICKMARKS_PARENT)
            for dK, dV in sorted(self.lipstickMarks.items(), key=lambda t: t[0]):
                dc = etree.SubElement(dp, self._XMLID_TAG_LIPSTICKMARKS_CHILD)
                dc.attrib['slot'] = dK
                dc.append(dV.toXML(self._XMLID_TAG_LIPSTICKMARKS_VALUEELEM))
        dp = etree.SubElement(e, self._XMLID_TAG_ATTRIBUTES_PARENT)
        for dK, dV in sorted(self.attributes.items(), key=lambda t: t[0].name):
            if dV == dK.baseValue:
                continue
            dc = etree.SubElement(dp, self._XMLID_TAG_ATTRIBUTES_CHILD)
            dc.attrib['type'] = dK.name
            dc.attrib['value'] = str(dV)
        dp = etree.SubElement(e, self._XMLID_TAG_POTIONATTRIBUTES_PARENT)
        for dK, dV in sorted(self.potionAttributes.items(), key=lambda t: t[0]):
            dc = etree.SubElement(dp, self._XMLID_TAG_POTIONATTRIBUTES_CHILD)
            dc.attrib['type'] = dK
            dc.attrib['value'] = str(dV)
        lp = etree.SubElement(e, self._XMLID_TAG_TRAITS_PARENT)
        for lv in sorted(self.traits, key=str):
            etree.SubElement(lp, self._XMLID_TAG_TRAITS_CHILD, {self._XMLID_ATTR_TRAITS_VALUEATTR: lv})
        lp = etree.SubElement(e, self._XMLID_TAG_SPECIALPERKS_PARENT)
        for lv in sorted(self.specialPerks, key=str):
            etree.SubElement(lp, self._XMLID_TAG_SPECIALPERKS_CHILD, {self._XMLID_ATTR_SPECIALPERKS_VALUEATTR: lv})
        self._toXML_perks(e)
        lp = etree.SubElement(e, self._XMLID_TAG_KNOWNSPELLS_PARENT)
        for lv in sorted(self.knownSpells, key=str):
            etree.SubElement(lp, self._XMLID_TAG_KNOWNSPELLS_CHILD, {self._XMLID_ATTR_KNOWNSPELLS_VALUEATTR: lv})
        lp = etree.SubElement(e, self._XMLID_TAG_SPELLUPGRADES_PARENT)
        for lv in sorted(self.spellUpgrades, key=str):
            etree.SubElement(lp, self._XMLID_TAG_SPELLUPGRADES_CHILD, {self._XMLID_ATTR_SPELLUPGRADES_VALUEATTR: lv})
        dp = etree.SubElement(e, self._XMLID_TAG_SPELLUPGRADEPOINTS_PARENT)
        for dK, dV in sorted(self.spellUpgradePoints.items(), key=lambda t: t[0]):
            dc = etree.SubElement(dp, self._XMLID_TAG_SPELLUPGRADEPOINTS_CHILD)
            dc.attrib['school'] = dK
            dc.attrib['points'] = str(dV)
        self._toXML_fetishes(e)
        lp = etree.SubElement(e, self._XMLID_TAG_STATUSEFFECTS_PARENT)
        for lv in sorted(self.statusEffects, key=lambda x: x.type):
            lp.append(lv.toXML())
        lp = etree.SubElement(e, self._XMLID_TAG_KNOWNMOVES_PARENT)
        for lv in sorted(self.knownMoves, key=str):
            etree.SubElement(lp, self._XMLID_TAG_KNOWNMOVES_CHILD, {self._XMLID_ATTR_KNOWNMOVES_VALUEATTR: lv})
        lp = etree.SubElement(e, self._XMLID_TAG_EQUIPPEDMOVES_PARENT)
        for lv in self.equippedMoves:
            etree.SubElement(lp, self._XMLID_TAG_EQUIPPEDMOVES_CHILD, {self._XMLID_ATTR_EQUIPPEDMOVES_VALUEATTR: lv})
        dp = etree.SubElement(e, self._XMLID_TAG_RELATIONSHIPS_PARENT)
        for dK, dV in sorted(self.relationships.items(), key=lambda t: t[0]):
            dc = etree.SubElement(dp, self._XMLID_TAG_RELATIONSHIPS_CHILD)
            dc.attrib['character'] = dK
            dc.attrib['value'] = str(dV)
        e.append(self.pregnancy.toXML(self._XMLID_TAG_PREGNANCY_ELEMENT))
        e.append(self.family.toXML(self._XMLID_TAG_FAMILY_ELEMENT))
        e.append(self.slavery.toXML(self._XMLID_TAG_SLAVERY_ELEMENT))
        e.append(self.companions.toXML(self._XMLID_TAG_COMPANIONS_ELEMENT))
        e.append(self.sexStats.toXML(self._XMLID_TAG_SEXSTATS_ELEMENT))
        e.append(self.addictionsCore.toXML(self._XMLID_TAG_ADDICTIONSCORE_ELEMENT))
        if self.artwork is not None:
            e.append(self.artwork.toXML(self._XMLID_TAG_ARTWORK_ELEMENT))
        if self.helpfulInformation is not None:
            e.append(self.helpfulInformation.toXML(self._XMLID_TAG_HELPFULINFORMATION_ELEMENT))
        return e

    def toDict(self) -> Dict[str, Any]:
        dK: Any
        dV: Any
        data: Dict[str, Any] = super().toDict()
        dp: Dict[str, Any]
        dsc: Dict[str, Any]
        dv: Savable
        data['core'] = self.core.toDict()
        data['locationInformation'] = self.locationInformation.toDict()
        data['body'] = self.body.toDict()
        data['characterInventory'] = self.characterInventory.toDict()
        if self.holdingClothing is None or len(self.holdingClothing) > 0:
            data['holdingClothing'] = list()
        else:
            lc = list()
            if len(self.holdingClothing) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['holdingClothing'] = lc
        if self.savedOutfits is None or len(self.savedOutfits) > 0:
            data['savedOutfits'] = list()
        else:
            lc = list()
            if len(self.savedOutfits) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['savedOutfits'] = lc
        if self.tattoos is not None and len(self.tattoos) > 0:
            dp = {}
            for dK, dV in sorted(self.tattoos.items(), key=lambda t: t[0]):
                sk = dK
                dp[sk] = dV.toDict()
            data['tattoos'] = dp
        if self.scars is not None and len(self.scars) > 0:
            dp = {}
            for dK, dV in sorted(self.scars.items(), key=lambda t: t[0]):
                sk = dK
                dp[sk] = dV.toDict()
            data['scars'] = dp
        if self.lipstickMarks is not None and len(self.lipstickMarks) > 0:
            dp = {}
            for dK, dV in sorted(self.lipstickMarks.items(), key=lambda t: t[0]):
                sk = dK
                dp[sk] = dV.toDict()
            data['lipstickMarks'] = dp
        dp = {}
        for dK, dV in sorted(self.attributes.items(), key=lambda t: t[0].name):
            if dV == dK.baseValue:
                continue
            sk = dK.name
            sv = dV
            dp[sk] = sv
        data['attributes'] = dp
        dp = {}
        for dK, dV in sorted(self.potionAttributes.items(), key=lambda t: t[0]):
            sk = dK
            sv = dV
            dp[sk] = sv
        data['potionAttributes'] = dp
        if self.traits is None or len(self.traits) > 0:
            data['traits'] = list()
        else:
            lc = list()
            if len(self.traits) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['traits'] = lc
        if self.specialPerks is None or len(self.specialPerks) > 0:
            data['specialPerks'] = list()
        else:
            lc = list()
            if len(self.specialPerks) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['specialPerks'] = lc
        self._toDict_perks(data)
        if self.knownSpells is None or len(self.knownSpells) > 0:
            data['knownSpells'] = list()
        else:
            lc = list()
            if len(self.knownSpells) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['knownSpells'] = lc
        if self.spellUpgrades is None or len(self.spellUpgrades) > 0:
            data['spellUpgrades'] = list()
        else:
            lc = list()
            if len(self.spellUpgrades) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['spellUpgrades'] = lc
        dp = {}
        for dK, dV in sorted(self.spellUpgradePoints.items(), key=lambda t: t[0]):
            sk = dK
            sv = dV
            dp[sk] = sv
        data['spellUpgradePoints'] = dp
        self._toDict_fetishes(data)
        if self.statusEffects is None or len(self.statusEffects) > 0:
            data['statusEffects'] = list()
        else:
            lc = list()
            if len(self.statusEffects) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['statusEffects'] = lc
        if self.knownMoves is None or len(self.knownMoves) > 0:
            data['knownMoves'] = list()
        else:
            lc = list()
            if len(self.knownMoves) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['knownMoves'] = lc
        if self.equippedMoves is None or len(self.equippedMoves) > 0:
            data['equippedMoves'] = list()
        else:
            lc = list()
            if len(self.equippedMoves) > 0:
                for lv in lc:
                    lc.append(str(lv))
            data['equippedMoves'] = lc
        dp = {}
        for dK, dV in sorted(self.relationships.items(), key=lambda t: t[0]):
            sk = dK
            sv = dV
            dp[sk] = sv
        data['relationships'] = dp
        data['pregnancy'] = self.pregnancy.toDict()
        data['family'] = self.family.toDict()
        data['slavery'] = self.slavery.toDict()
        data['companions'] = self.companions.toDict()
        data['sexStats'] = self.sexStats.toDict()
        data['addictionsCore'] = self.addictionsCore.toDict()
        if self.artwork is not None:
            data['artwork'] = self.artwork.toDict()
        if self.helpfulInformation is not None:
            data['helpfulInformation'] = self.helpfulInformation.toDict()
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('core')) is not None:
            self.core = Core()
            self.core.fromDict(data['core'])
        else:
            raise KeyRequiredException(self, data, 'core', 'core')
        if (sf := data.get('locationInformation')) is not None:
            self.locationInformation = LocationInformation()
            self.locationInformation.fromDict(data['locationInformation'])
        else:
            raise KeyRequiredException(self, data, 'locationInformation', 'locationInformation')
        if (sf := data.get('body')) is not None:
            self.body = Body()
            self.body.fromDict(data['body'])
        else:
            raise KeyRequiredException(self, data, 'body', 'body')
        if (sf := data.get('characterInventory')) is not None:
            self.characterInventory = CharacterInventory()
            self.characterInventory.fromDict(data['characterInventory'])
        else:
            raise KeyRequiredException(self, data, 'characterInventory', 'characterInventory')
        if (lv := self.holdingClothing) is not None:
            for le in lv:
                self.holdingClothing.append(le.toDict())
        else:
            self.holdingClothing = list()
        if (lv := self.savedOutfits) is not None:
            for le in lv:
                self.savedOutfits.append(le.toDict())
        else:
            self.savedOutfits = list()
        if (df := data.get('tattoos')) is not None:
            for sk, sv in df.items():
                dV = Tattoo()
                dV.fromDict(sv)
                self.tattoos[sk] = dv
        if (df := data.get('scars')) is not None:
            for sk, sv in df.items():
                dV = Scar()
                dV.fromDict(sv)
                self.scars[sk] = dv
        if (df := data.get('lipstickMarks')) is not None:
            for sk, sv in df.items():
                dV = Covering()
                dV.fromDict(sv)
                self.lipstickMarks[sk] = dv
        if (df := data.get('attributes')) is not None:
            for sk, sv in df.items():
                self.attributes[EAttribute[sk]] = sv
        else:
            self.attributes = {}
        if (df := data.get('potionAttributes')) is not None:
            for sk, sv in df.items():
                self.potionAttributes[sk] = sv
        else:
            self.potionAttributes = {}
        if (lv := self.traits) is not None:
            for le in lv:
                self.traits.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'traits', 'traits')
        if (lv := self.specialPerks) is not None:
            for le in lv:
                self.specialPerks.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'specialPerks', 'specialPerks')
        self._fromDict_perks(data)
        if (lv := self.knownSpells) is not None:
            for le in lv:
                self.knownSpells.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'knownSpells', 'knownSpells')
        if (lv := self.spellUpgrades) is not None:
            for le in lv:
                self.spellUpgrades.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'spellUpgrades', 'spellUpgrades')
        if (df := data.get('spellUpgradePoints')) is not None:
            for sk, sv in df.items():
                self.spellUpgradePoints[sk] = sv
        else:
            self.spellUpgradePoints = {}
        self._fromDict_fetishes(data)
        if (lv := self.statusEffects) is not None:
            for le in lv:
                self.statusEffects.add(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'statusEffects', 'statusEffects')
        if (lv := self.knownMoves) is not None:
            for le in lv:
                self.knownMoves.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'knownMoves', 'knownMoves')
        if (lv := self.equippedMoves) is not None:
            for le in lv:
                self.equippedMoves.append(str(le))
        else:
            raise KeyRequiredException(self, data, 'equippedMoves', 'equippedMoves')
        if (df := data.get('relationships')) is not None:
            for sk, sv in df.items():
                self.relationships[sk] = sv
        else:
            self.relationships = {}
        if (sf := data.get('pregnancy')) is not None:
            self.pregnancy = Pregnancy()
            self.pregnancy.fromDict(data['pregnancy'])
        else:
            raise KeyRequiredException(self, data, 'pregnancy', 'pregnancy')
        if (sf := data.get('family')) is not None:
            self.family = Family()
            self.family.fromDict(data['family'])
        else:
            raise KeyRequiredException(self, data, 'family', 'family')
        if (sf := data.get('slavery')) is not None:
            self.slavery = SlaveryInfo()
            self.slavery.fromDict(data['slavery'])
        else:
            raise KeyRequiredException(self, data, 'slavery', 'slavery')
        if (sf := data.get('companions')) is not None:
            self.companions = Companions()
            self.companions.fromDict(data['companions'])
        else:
            raise KeyRequiredException(self, data, 'companions', 'companions')
        if (sf := data.get('sexStats')) is not None:
            self.sexStats = SexStats()
            self.sexStats.fromDict(data['sexStats'])
        else:
            raise KeyRequiredException(self, data, 'sexStats', 'sexStats')
        if (sf := data.get('addictionsCore')) is not None:
            self.addictionsCore = AddictionsCore()
            self.addictionsCore.fromDict(data['addictionsCore'])
        else:
            raise KeyRequiredException(self, data, 'addictionsCore', 'addictionsCore')
        if (sf := data.get('artwork')) is not None:
            self.artwork = ArtworkOverride()
            self.artwork.fromDict(data['artwork'])
        else:
            self.artwork = None
        if (sf := data.get('helpfulInformation')) is not None:
            self.helpfulInformation = HelpfulCharacterInformation()
            self.helpfulInformation.fromDict(data['helpfulInformation'])
        else:
            self.helpfulInformation = None

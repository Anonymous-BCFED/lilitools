#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.math.point2i import Point2I
from lilitools.saves.savable import Savable
__all__ = ['RawLocationInformation']
## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public Element saveAsXML(Element parentElement, Document doc) { @ Ra1rfw7eKd5WT2JXoGa/zLFvb/XgZlpPNZbY2BZwgdTA/t0DkASy5ADAtBbX2DvSTxI43cQAGTPbPsMVcAHyRQ==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawLocationInformation(Savable):
    TAG = 'locationInformation'
    _XMLID_ATTR_GLOBALLOCATION_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_HOMELOCATION_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_HOMEWORLDLOCATION_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_LOCATION_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_WORLDLOCATION_ATTRIBUTE: str = 'value'
    _XMLID_TAG_GLOBALLOCATION_ELEMENT: str = 'globalLocation'
    _XMLID_TAG_HOMELOCATION_ELEMENT: str = 'homeLocation'
    _XMLID_TAG_HOMEWORLDLOCATION_ELEMENT: str = 'homeWorldLocation'
    _XMLID_TAG_LOCATION_ELEMENT: str = 'location'
    _XMLID_TAG_WORLDLOCATION_ELEMENT: str = 'worldLocation'

    def __init__(self) -> None:
        super().__init__()
        self.worldLocation: str = ''  # Element
        self.homeWorldLocation: str = ''  # Element
        self.location: Point2I = Point2I()  # Element
        self.homeLocation: Point2I = Point2I()  # Element
        self.globalLocation: Point2I = Point2I()  # Element

    def overrideAttrs(self, globalLocation_attribute: _Optional_str = None, homeLocation_attribute: _Optional_str = None, homeWorldLocation_attribute: _Optional_str = None, location_attribute: _Optional_str = None, worldLocation_attribute: _Optional_str = None) -> None:
        if globalLocation_attribute is not None:
            self._XMLID_ATTR_GLOBALLOCATION_ATTRIBUTE = globalLocation_attribute
        if homeLocation_attribute is not None:
            self._XMLID_ATTR_HOMELOCATION_ATTRIBUTE = homeLocation_attribute
        if homeWorldLocation_attribute is not None:
            self._XMLID_ATTR_HOMEWORLDLOCATION_ATTRIBUTE = homeWorldLocation_attribute
        if location_attribute is not None:
            self._XMLID_ATTR_LOCATION_ATTRIBUTE = location_attribute
        if worldLocation_attribute is not None:
            self._XMLID_ATTR_WORLDLOCATION_ATTRIBUTE = worldLocation_attribute

    def overrideTags(self, globalLocation_element: _Optional_str = None, homeLocation_element: _Optional_str = None, homeWorldLocation_element: _Optional_str = None, location_element: _Optional_str = None, worldLocation_element: _Optional_str = None) -> None:
        if globalLocation_element is not None:
            self._XMLID_TAG_GLOBALLOCATION_ELEMENT = globalLocation_element
        if homeLocation_element is not None:
            self._XMLID_TAG_HOMELOCATION_ELEMENT = homeLocation_element
        if homeWorldLocation_element is not None:
            self._XMLID_TAG_HOMEWORLDLOCATION_ELEMENT = homeWorldLocation_element
        if location_element is not None:
            self._XMLID_TAG_LOCATION_ELEMENT = location_element
        if worldLocation_element is not None:
            self._XMLID_TAG_WORLDLOCATION_ELEMENT = worldLocation_element

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_WORLDLOCATION_ELEMENT)) is not None:
            if self._XMLID_ATTR_WORLDLOCATION_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_WORLDLOCATION_ATTRIBUTE, 'worldLocation')
            self.worldLocation = sf.attrib[self._XMLID_ATTR_WORLDLOCATION_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'worldLocation', self._XMLID_TAG_WORLDLOCATION_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_HOMEWORLDLOCATION_ELEMENT)) is not None:
            if self._XMLID_ATTR_HOMEWORLDLOCATION_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_HOMEWORLDLOCATION_ATTRIBUTE, 'homeWorldLocation')
            self.homeWorldLocation = sf.attrib[self._XMLID_ATTR_HOMEWORLDLOCATION_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'homeWorldLocation', self._XMLID_TAG_HOMEWORLDLOCATION_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_LOCATION_ELEMENT)) is not None:
            self.location = Point2I()
            self.location.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'location', self._XMLID_TAG_LOCATION_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_HOMELOCATION_ELEMENT)) is not None:
            self.homeLocation = Point2I()
            self.homeLocation.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'homeLocation', self._XMLID_TAG_HOMELOCATION_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_GLOBALLOCATION_ELEMENT)) is not None:
            self.globalLocation = Point2I()
            self.globalLocation.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'globalLocation', self._XMLID_TAG_GLOBALLOCATION_ELEMENT)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        etree.SubElement(e, self._XMLID_TAG_WORLDLOCATION_ELEMENT, {self._XMLID_ATTR_WORLDLOCATION_ATTRIBUTE: str(self.worldLocation)})
        etree.SubElement(e, self._XMLID_TAG_HOMEWORLDLOCATION_ELEMENT, {self._XMLID_ATTR_HOMEWORLDLOCATION_ATTRIBUTE: str(self.homeWorldLocation)})
        e.append(self.location.toXML(self._XMLID_TAG_LOCATION_ELEMENT))
        e.append(self.homeLocation.toXML(self._XMLID_TAG_HOMELOCATION_ELEMENT))
        e.append(self.globalLocation.toXML(self._XMLID_TAG_GLOBALLOCATION_ELEMENT))
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['worldLocation'] = str(self.worldLocation)
        data['homeWorldLocation'] = str(self.homeWorldLocation)
        data['location'] = self.location.toDict()
        data['homeLocation'] = self.homeLocation.toDict()
        data['globalLocation'] = self.globalLocation.toDict()
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('worldLocation')) is not None:
            self.worldLocation = data['worldLocation']
        else:
            raise KeyRequiredException(self, data, 'worldLocation', 'worldLocation')
        if (sf := data.get('homeWorldLocation')) is not None:
            self.homeWorldLocation = data['homeWorldLocation']
        else:
            raise KeyRequiredException(self, data, 'homeWorldLocation', 'homeWorldLocation')
        if (sf := data.get('location')) is not None:
            self.location = Point2I()
            self.location.fromDict(data['location'])
        else:
            raise KeyRequiredException(self, data, 'location', 'location')
        if (sf := data.get('homeLocation')) is not None:
            self.homeLocation = Point2I()
            self.homeLocation.fromDict(data['homeLocation'])
        else:
            raise KeyRequiredException(self, data, 'homeLocation', 'homeLocation')
        if (sf := data.get('globalLocation')) is not None:
            self.globalLocation = Point2I()
            self.globalLocation.fromDict(data['globalLocation'])
        else:
            raise KeyRequiredException(self, data, 'globalLocation', 'globalLocation')

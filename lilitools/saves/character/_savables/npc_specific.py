#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional, Set
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL, Savable
__all__ = ['RawNPCSpecific']
## from [LT]/src/com/lilithsthrone/game/character/npc/NPC.java: public Element saveAsXML(Element parentElement, Document doc) { @ 1CJJMel+v4AsY5of2ew+uZ+74ANVCjySs+ji8ky8bm8ueW3yZr51uF0vYH3W9PvwkfLqW/y4GXuUw/xvl/db1w==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawNPCSpecific(Savable):
    TAG = 'npcSpecific'
    _XMLID_ATTR_ADDEDTOCONTACTS_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_BUYMODIFIER_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_FLAGVALUES_VALUEATTR: str = 'value'
    _XMLID_ATTR_GENDERPREFERENCE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_GENERATEDISPOSABLECLOTHING_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_GENERATEEXTRACLOTHING_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_GENERATEEXTRAITEMS_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_LASTTIMEENCOUNTERED_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_PLAYERSURRENDERCOUNT_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_RACESTAGEPREFERENCE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_SELLMODIFIER_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_SUBSPECIESPREFERENCE_ATTRIBUTE: str = 'value'
    _XMLID_TAG_ADDEDTOCONTACTS_ELEMENT: str = 'addedToContacts'
    _XMLID_TAG_BUYMODIFIER_ELEMENT: str = 'buyModifier'
    _XMLID_TAG_FLAGVALUES_CHILD: str = 'NPCValue'
    _XMLID_TAG_FLAGVALUES_PARENT: str = 'NPCValues'
    _XMLID_TAG_GENDERPREFERENCE_ELEMENT: str = 'genderPreference'
    _XMLID_TAG_GENERATEDISPOSABLECLOTHING_ELEMENT: str = 'generateDisposableClothing'
    _XMLID_TAG_GENERATEEXTRACLOTHING_ELEMENT: str = 'generateExtraClothing'
    _XMLID_TAG_GENERATEEXTRAITEMS_ELEMENT: str = 'generateExtraItems'
    _XMLID_TAG_LASTTIMEENCOUNTERED_ELEMENT: str = 'lastTimeEncountered'
    _XMLID_TAG_PLAYERSURRENDERCOUNT_ELEMENT: str = 'playerSurrenderCount'
    _XMLID_TAG_RACESTAGEPREFERENCE_ELEMENT: str = 'raceStagePreference'
    _XMLID_TAG_SELLMODIFIER_ELEMENT: str = 'sellModifier'
    _XMLID_TAG_SUBSPECIESPREFERENCE_ELEMENT: str = 'subspeciesPreference'

    def __init__(self) -> None:
        super().__init__()
        self.lastTimeEncountered: int = 0  # Element
        self.buyModifier: float = 0.  # Element
        self.sellModifier: float = 0.  # Element
        self.playerSurrenderCount: int = 0  # Element
        self.addedToContacts: bool = False  # Element
        self.generateExtraItems: bool = False  # Element
        self.generateDisposableClothing: bool = False  # Element
        self.generateExtraClothing: bool = False  # Element
        self.flagValues: Set[str] = set()
        self.genderPreference: Optional[str] = None  # Element
        self.subspeciesPreference: Optional[str] = None  # Element
        self.raceStagePreference: Optional[str] = None  # Element

    def overrideAttrs(self, addedToContacts_attribute: _Optional_str = None, buyModifier_attribute: _Optional_str = None, flagValues_valueattr: _Optional_str = None, genderPreference_attribute: _Optional_str = None, generateDisposableClothing_attribute: _Optional_str = None, generateExtraClothing_attribute: _Optional_str = None, generateExtraItems_attribute: _Optional_str = None, lastTimeEncountered_attribute: _Optional_str = None, playerSurrenderCount_attribute: _Optional_str = None, raceStagePreference_attribute: _Optional_str = None, sellModifier_attribute: _Optional_str = None, subspeciesPreference_attribute: _Optional_str = None) -> None:
        if addedToContacts_attribute is not None:
            self._XMLID_ATTR_ADDEDTOCONTACTS_ATTRIBUTE = addedToContacts_attribute
        if buyModifier_attribute is not None:
            self._XMLID_ATTR_BUYMODIFIER_ATTRIBUTE = buyModifier_attribute
        if flagValues_valueattr is not None:
            self._XMLID_ATTR_FLAGVALUES_VALUEATTR = flagValues_valueattr
        if genderPreference_attribute is not None:
            self._XMLID_ATTR_GENDERPREFERENCE_ATTRIBUTE = genderPreference_attribute
        if generateDisposableClothing_attribute is not None:
            self._XMLID_ATTR_GENERATEDISPOSABLECLOTHING_ATTRIBUTE = generateDisposableClothing_attribute
        if generateExtraClothing_attribute is not None:
            self._XMLID_ATTR_GENERATEEXTRACLOTHING_ATTRIBUTE = generateExtraClothing_attribute
        if generateExtraItems_attribute is not None:
            self._XMLID_ATTR_GENERATEEXTRAITEMS_ATTRIBUTE = generateExtraItems_attribute
        if lastTimeEncountered_attribute is not None:
            self._XMLID_ATTR_LASTTIMEENCOUNTERED_ATTRIBUTE = lastTimeEncountered_attribute
        if playerSurrenderCount_attribute is not None:
            self._XMLID_ATTR_PLAYERSURRENDERCOUNT_ATTRIBUTE = playerSurrenderCount_attribute
        if raceStagePreference_attribute is not None:
            self._XMLID_ATTR_RACESTAGEPREFERENCE_ATTRIBUTE = raceStagePreference_attribute
        if sellModifier_attribute is not None:
            self._XMLID_ATTR_SELLMODIFIER_ATTRIBUTE = sellModifier_attribute
        if subspeciesPreference_attribute is not None:
            self._XMLID_ATTR_SUBSPECIESPREFERENCE_ATTRIBUTE = subspeciesPreference_attribute

    def overrideTags(self, addedToContacts_element: _Optional_str = None, buyModifier_element: _Optional_str = None, flagValues_child: _Optional_str = None, flagValues_parent: _Optional_str = None, genderPreference_element: _Optional_str = None, generateDisposableClothing_element: _Optional_str = None, generateExtraClothing_element: _Optional_str = None, generateExtraItems_element: _Optional_str = None, lastTimeEncountered_element: _Optional_str = None, playerSurrenderCount_element: _Optional_str = None, raceStagePreference_element: _Optional_str = None, sellModifier_element: _Optional_str = None, subspeciesPreference_element: _Optional_str = None) -> None:
        if addedToContacts_element is not None:
            self._XMLID_TAG_ADDEDTOCONTACTS_ELEMENT = addedToContacts_element
        if buyModifier_element is not None:
            self._XMLID_TAG_BUYMODIFIER_ELEMENT = buyModifier_element
        if flagValues_child is not None:
            self._XMLID_TAG_FLAGVALUES_CHILD = flagValues_child
        if flagValues_parent is not None:
            self._XMLID_TAG_FLAGVALUES_PARENT = flagValues_parent
        if genderPreference_element is not None:
            self._XMLID_TAG_GENDERPREFERENCE_ELEMENT = genderPreference_element
        if generateDisposableClothing_element is not None:
            self._XMLID_TAG_GENERATEDISPOSABLECLOTHING_ELEMENT = generateDisposableClothing_element
        if generateExtraClothing_element is not None:
            self._XMLID_TAG_GENERATEEXTRACLOTHING_ELEMENT = generateExtraClothing_element
        if generateExtraItems_element is not None:
            self._XMLID_TAG_GENERATEEXTRAITEMS_ELEMENT = generateExtraItems_element
        if lastTimeEncountered_element is not None:
            self._XMLID_TAG_LASTTIMEENCOUNTERED_ELEMENT = lastTimeEncountered_element
        if playerSurrenderCount_element is not None:
            self._XMLID_TAG_PLAYERSURRENDERCOUNT_ELEMENT = playerSurrenderCount_element
        if raceStagePreference_element is not None:
            self._XMLID_TAG_RACESTAGEPREFERENCE_ELEMENT = raceStagePreference_element
        if sellModifier_element is not None:
            self._XMLID_TAG_SELLMODIFIER_ELEMENT = sellModifier_element
        if subspeciesPreference_element is not None:
            self._XMLID_TAG_SUBSPECIESPREFERENCE_ELEMENT = subspeciesPreference_element

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_LASTTIMEENCOUNTERED_ELEMENT)) is not None:
            if self._XMLID_ATTR_LASTTIMEENCOUNTERED_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_LASTTIMEENCOUNTERED_ATTRIBUTE, 'lastTimeEncountered')
            self.lastTimeEncountered = int(sf.attrib[self._XMLID_ATTR_LASTTIMEENCOUNTERED_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'lastTimeEncountered', self._XMLID_TAG_LASTTIMEENCOUNTERED_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_BUYMODIFIER_ELEMENT)) is not None:
            if self._XMLID_ATTR_BUYMODIFIER_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_BUYMODIFIER_ATTRIBUTE, 'buyModifier')
            self.buyModifier = float(sf.attrib[self._XMLID_ATTR_BUYMODIFIER_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'buyModifier', self._XMLID_TAG_BUYMODIFIER_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_SELLMODIFIER_ELEMENT)) is not None:
            if self._XMLID_ATTR_SELLMODIFIER_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_SELLMODIFIER_ATTRIBUTE, 'sellModifier')
            self.sellModifier = float(sf.attrib[self._XMLID_ATTR_SELLMODIFIER_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'sellModifier', self._XMLID_TAG_SELLMODIFIER_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_PLAYERSURRENDERCOUNT_ELEMENT)) is not None:
            if self._XMLID_ATTR_PLAYERSURRENDERCOUNT_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_PLAYERSURRENDERCOUNT_ATTRIBUTE, 'playerSurrenderCount')
            self.playerSurrenderCount = int(sf.attrib[self._XMLID_ATTR_PLAYERSURRENDERCOUNT_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'playerSurrenderCount', self._XMLID_TAG_PLAYERSURRENDERCOUNT_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_ADDEDTOCONTACTS_ELEMENT)) is not None:
            if self._XMLID_ATTR_ADDEDTOCONTACTS_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_ADDEDTOCONTACTS_ATTRIBUTE, 'addedToContacts')
            self.addedToContacts = XML2BOOL[sf.attrib[self._XMLID_ATTR_ADDEDTOCONTACTS_ATTRIBUTE]]
        else:
            raise ElementRequiredException(self, e, 'addedToContacts', self._XMLID_TAG_ADDEDTOCONTACTS_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_GENERATEEXTRAITEMS_ELEMENT)) is not None:
            if self._XMLID_ATTR_GENERATEEXTRAITEMS_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_GENERATEEXTRAITEMS_ATTRIBUTE, 'generateExtraItems')
            self.generateExtraItems = XML2BOOL[sf.attrib[self._XMLID_ATTR_GENERATEEXTRAITEMS_ATTRIBUTE]]
        else:
            raise ElementRequiredException(self, e, 'generateExtraItems', self._XMLID_TAG_GENERATEEXTRAITEMS_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_GENERATEDISPOSABLECLOTHING_ELEMENT)) is not None:
            if self._XMLID_ATTR_GENERATEDISPOSABLECLOTHING_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_GENERATEDISPOSABLECLOTHING_ATTRIBUTE, 'generateDisposableClothing')
            self.generateDisposableClothing = XML2BOOL[sf.attrib[self._XMLID_ATTR_GENERATEDISPOSABLECLOTHING_ATTRIBUTE]]
        else:
            raise ElementRequiredException(self, e, 'generateDisposableClothing', self._XMLID_TAG_GENERATEDISPOSABLECLOTHING_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_GENERATEEXTRACLOTHING_ELEMENT)) is not None:
            if self._XMLID_ATTR_GENERATEEXTRACLOTHING_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_GENERATEEXTRACLOTHING_ATTRIBUTE, 'generateExtraClothing')
            self.generateExtraClothing = XML2BOOL[sf.attrib[self._XMLID_ATTR_GENERATEEXTRACLOTHING_ATTRIBUTE]]
        else:
            raise ElementRequiredException(self, e, 'generateExtraClothing', self._XMLID_TAG_GENERATEEXTRACLOTHING_ELEMENT)
        if (lf := e.find(self._XMLID_TAG_FLAGVALUES_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_FLAGVALUES_CHILD):
                    self.flagValues.add(lc.attrib[self._XMLID_ATTR_FLAGVALUES_VALUEATTR])
        else:
            self.flagValues = set()
        if (sf := e.find(self._XMLID_TAG_GENDERPREFERENCE_ELEMENT)) is not None:
            self.genderPreference = sf.attrib.get(self._XMLID_ATTR_GENDERPREFERENCE_ATTRIBUTE, None)
        if (sf := e.find(self._XMLID_TAG_SUBSPECIESPREFERENCE_ELEMENT)) is not None:
            self.subspeciesPreference = sf.attrib.get(self._XMLID_ATTR_SUBSPECIESPREFERENCE_ATTRIBUTE, None)
        if (sf := e.find(self._XMLID_TAG_RACESTAGEPREFERENCE_ELEMENT)) is not None:
            self.raceStagePreference = sf.attrib.get(self._XMLID_ATTR_RACESTAGEPREFERENCE_ATTRIBUTE, None)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        etree.SubElement(e, self._XMLID_TAG_LASTTIMEENCOUNTERED_ELEMENT, {self._XMLID_ATTR_LASTTIMEENCOUNTERED_ATTRIBUTE: str(self.lastTimeEncountered)})
        etree.SubElement(e, self._XMLID_TAG_BUYMODIFIER_ELEMENT, {self._XMLID_ATTR_BUYMODIFIER_ATTRIBUTE: str(self.buyModifier)})
        etree.SubElement(e, self._XMLID_TAG_SELLMODIFIER_ELEMENT, {self._XMLID_ATTR_SELLMODIFIER_ATTRIBUTE: str(self.sellModifier)})
        etree.SubElement(e, self._XMLID_TAG_PLAYERSURRENDERCOUNT_ELEMENT, {self._XMLID_ATTR_PLAYERSURRENDERCOUNT_ATTRIBUTE: str(self.playerSurrenderCount)})
        etree.SubElement(e, self._XMLID_TAG_ADDEDTOCONTACTS_ELEMENT, {self._XMLID_ATTR_ADDEDTOCONTACTS_ATTRIBUTE: str(self.addedToContacts).lower()})
        etree.SubElement(e, self._XMLID_TAG_GENERATEEXTRAITEMS_ELEMENT, {self._XMLID_ATTR_GENERATEEXTRAITEMS_ATTRIBUTE: str(self.generateExtraItems).lower()})
        etree.SubElement(e, self._XMLID_TAG_GENERATEDISPOSABLECLOTHING_ELEMENT, {self._XMLID_ATTR_GENERATEDISPOSABLECLOTHING_ATTRIBUTE: str(self.generateDisposableClothing).lower()})
        etree.SubElement(e, self._XMLID_TAG_GENERATEEXTRACLOTHING_ELEMENT, {self._XMLID_ATTR_GENERATEEXTRACLOTHING_ATTRIBUTE: str(self.generateExtraClothing).lower()})
        lp = etree.SubElement(e, self._XMLID_TAG_FLAGVALUES_PARENT)
        for lv in sorted(self.flagValues, key=str):
            etree.SubElement(lp, self._XMLID_TAG_FLAGVALUES_CHILD, {self._XMLID_ATTR_FLAGVALUES_VALUEATTR: lv})
        if self.genderPreference is not None:
            etree.SubElement(e, self._XMLID_TAG_GENDERPREFERENCE_ELEMENT, {self._XMLID_ATTR_GENDERPREFERENCE_ATTRIBUTE: str(self.genderPreference)})
        if self.subspeciesPreference is not None:
            etree.SubElement(e, self._XMLID_TAG_SUBSPECIESPREFERENCE_ELEMENT, {self._XMLID_ATTR_SUBSPECIESPREFERENCE_ATTRIBUTE: str(self.subspeciesPreference)})
        if self.raceStagePreference is not None:
            etree.SubElement(e, self._XMLID_TAG_RACESTAGEPREFERENCE_ELEMENT, {self._XMLID_ATTR_RACESTAGEPREFERENCE_ATTRIBUTE: str(self.raceStagePreference)})
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['lastTimeEncountered'] = int(self.lastTimeEncountered)
        data['buyModifier'] = float(self.buyModifier)
        data['sellModifier'] = float(self.sellModifier)
        data['playerSurrenderCount'] = int(self.playerSurrenderCount)
        data['addedToContacts'] = bool(self.addedToContacts)
        data['generateExtraItems'] = bool(self.generateExtraItems)
        data['generateDisposableClothing'] = bool(self.generateDisposableClothing)
        data['generateExtraClothing'] = bool(self.generateExtraClothing)
        if self.flagValues is None or len(self.flagValues) > 0:
            data['flagValues'] = list()
        else:
            lc = list()
            if len(self.flagValues) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['flagValues'] = lc
        if self.genderPreference is not None:
            data['genderPreference'] = str(self.genderPreference)
        if self.subspeciesPreference is not None:
            data['subspeciesPreference'] = str(self.subspeciesPreference)
        if self.raceStagePreference is not None:
            data['raceStagePreference'] = str(self.raceStagePreference)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('lastTimeEncountered')) is not None:
            self.lastTimeEncountered = data['lastTimeEncountered']
        else:
            raise KeyRequiredException(self, data, 'lastTimeEncountered', 'lastTimeEncountered')
        if (sf := data.get('buyModifier')) is not None:
            self.buyModifier = data['buyModifier']
        else:
            raise KeyRequiredException(self, data, 'buyModifier', 'buyModifier')
        if (sf := data.get('sellModifier')) is not None:
            self.sellModifier = data['sellModifier']
        else:
            raise KeyRequiredException(self, data, 'sellModifier', 'sellModifier')
        if (sf := data.get('playerSurrenderCount')) is not None:
            self.playerSurrenderCount = data['playerSurrenderCount']
        else:
            raise KeyRequiredException(self, data, 'playerSurrenderCount', 'playerSurrenderCount')
        if (sf := data.get('addedToContacts')) is not None:
            self.addedToContacts = data['addedToContacts']
        else:
            raise KeyRequiredException(self, data, 'addedToContacts', 'addedToContacts')
        if (sf := data.get('generateExtraItems')) is not None:
            self.generateExtraItems = data['generateExtraItems']
        else:
            raise KeyRequiredException(self, data, 'generateExtraItems', 'generateExtraItems')
        if (sf := data.get('generateDisposableClothing')) is not None:
            self.generateDisposableClothing = data['generateDisposableClothing']
        else:
            raise KeyRequiredException(self, data, 'generateDisposableClothing', 'generateDisposableClothing')
        if (sf := data.get('generateExtraClothing')) is not None:
            self.generateExtraClothing = data['generateExtraClothing']
        else:
            raise KeyRequiredException(self, data, 'generateExtraClothing', 'generateExtraClothing')
        if (lv := self.flagValues) is not None:
            for le in lv:
                self.flagValues.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'flagValues', 'flagValues')
        if (sf := data.get('genderPreference')) is not None:
            self.genderPreference = data['genderPreference']
        else:
            self.genderPreference = None
        if (sf := data.get('subspeciesPreference')) is not None:
            self.subspeciesPreference = data['subspeciesPreference']
        else:
            self.subspeciesPreference = None
        if (sf := data.get('raceStagePreference')) is not None:
            self.raceStagePreference = data['raceStagePreference']
        else:
            self.raceStagePreference = None

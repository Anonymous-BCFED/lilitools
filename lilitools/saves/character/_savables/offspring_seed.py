#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.character.body.offspring_body import OffspringBody
from lilitools.saves.character.family import Family
from lilitools.saves.character.offspring_seed_data import OffspringSeedData
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawOffspringSeed']
## from [LT]/src/com/lilithsthrone/game/character/npc/misc/OffspringSeed.java: public Element saveAsXML(Element parentElement, Document doc) { @ 3O8tOeV+g8vH5OXlDu3VpI2g0sVUMpXV6Kd98/J8gmnnxGqL8ujMziKOJFDtSFEi9j7b2PaLaISOtgXYu0Ehsw==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawOffspringSeed(Savable):
    TAG = 'OffspringSeed'
    _XMLID_ATTR_BODY_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_DATA_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_FAMILY_ATTRIBUTE: str = 'value'
    _XMLID_TAG_BODY_ELEMENT: str = 'body'
    _XMLID_TAG_DATA_ELEMENT: str = 'data'
    _XMLID_TAG_FAMILY_ELEMENT: str = 'family'

    def __init__(self) -> None:
        super().__init__()
        self.data: OffspringSeedData = OffspringSeedData()  # Element
        self.body: OffspringBody = OffspringBody()  # Element
        self.family: Family = Family()  # Element

    def overrideAttrs(self, body_attribute: _Optional_str = None, data_attribute: _Optional_str = None, family_attribute: _Optional_str = None) -> None:
        if body_attribute is not None:
            self._XMLID_ATTR_BODY_ATTRIBUTE = body_attribute
        if data_attribute is not None:
            self._XMLID_ATTR_DATA_ATTRIBUTE = data_attribute
        if family_attribute is not None:
            self._XMLID_ATTR_FAMILY_ATTRIBUTE = family_attribute

    def overrideTags(self, body_element: _Optional_str = None, data_element: _Optional_str = None, family_element: _Optional_str = None) -> None:
        if body_element is not None:
            self._XMLID_TAG_BODY_ELEMENT = body_element
        if data_element is not None:
            self._XMLID_TAG_DATA_ELEMENT = data_element
        if family_element is not None:
            self._XMLID_TAG_FAMILY_ELEMENT = family_element

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_DATA_ELEMENT)) is not None:
            self.data = OffspringSeedData()
            self.data.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'data', self._XMLID_TAG_DATA_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_BODY_ELEMENT)) is not None:
            self.body = OffspringBody()
            self.body.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'body', self._XMLID_TAG_BODY_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_FAMILY_ELEMENT)) is not None:
            self.family = Family()
            self.family.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'family', self._XMLID_TAG_FAMILY_ELEMENT)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.append(self.data.toXML(self._XMLID_TAG_DATA_ELEMENT))
        e.append(self.body.toXML(self._XMLID_TAG_BODY_ELEMENT))
        e.append(self.family.toXML(self._XMLID_TAG_FAMILY_ELEMENT))
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['data'] = self.data.toDict()
        data['body'] = self.body.toDict()
        data['family'] = self.family.toDict()
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('data')) is not None:
            self.data = OffspringSeedData()
            self.data.fromDict(data['data'])
        else:
            raise KeyRequiredException(self, data, 'data', 'data')
        if (sf := data.get('body')) is not None:
            self.body = OffspringBody()
            self.body.fromDict(data['body'])
        else:
            raise KeyRequiredException(self, data, 'body', 'body')
        if (sf := data.get('family')) is not None:
            self.family = Family()
            self.family.fromDict(data['family'])
        else:
            raise KeyRequiredException(self, data, 'family', 'family')

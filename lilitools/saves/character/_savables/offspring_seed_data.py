#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.game.enums.month import EMonth
from lilitools.saves.character.character_names import CharacterNames
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL, Savable
__all__ = ['RawOffspringSeedData']
## from [LT]/src/com/lilithsthrone/game/character/npc/misc/OffspringSeed.java: public Element saveAsXML(Element parentElement, Document doc) { @ 3O8tOeV+g8vH5OXlDu3VpI2g0sVUMpXV6Kd98/J8gmnnxGqL8ujMziKOJFDtSFEi9j7b2PaLaISOtgXYu0Ehsw==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawOffspringSeedData(Savable):
    TAG = 'data'
    _XMLID_ATTR_BORN_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_DAYOFBIRTH_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_DESCRIPTION_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_FROMPLAYER_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_ID_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_MONTHOFBIRTH_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_NAME_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_SURNAME_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_YEAROFBIRTH_ATTRIBUTE: str = 'value'
    _XMLID_TAG_BORN_ELEMENT: str = 'born'
    _XMLID_TAG_DAYOFBIRTH_ELEMENT: str = 'dayOfBirth'
    _XMLID_TAG_DESCRIPTION_ELEMENT: str = 'description'
    _XMLID_TAG_FROMPLAYER_ELEMENT: str = 'fromPlayer'
    _XMLID_TAG_ID_ELEMENT: str = 'id'
    _XMLID_TAG_MONTHOFBIRTH_ELEMENT: str = 'monthOfBirth'
    _XMLID_TAG_NAME_ELEMENT: str = 'name'
    _XMLID_TAG_SURNAME_ELEMENT: str = 'surname'
    _XMLID_TAG_YEAROFBIRTH_ELEMENT: str = 'yearOfBirth'

    def __init__(self) -> None:
        super().__init__()
        self.id: str = ''  # Element
        self.fromPlayer: bool = False  # Element
        self.born: bool = False  # Element
        self.name: CharacterNames = CharacterNames()  # Element
        self.surname: str = ''  # Element
        self.description: str = ''  # Element
        self.yearOfBirth: Optional[int] = None  # Element
        self.monthOfBirth: Optional[EMonth] = None  # Element
        self.dayOfBirth: Optional[int] = None  # Element

    def overrideAttrs(self, born_attribute: _Optional_str = None, dayOfBirth_attribute: _Optional_str = None, description_attribute: _Optional_str = None, fromPlayer_attribute: _Optional_str = None, id_attribute: _Optional_str = None, monthOfBirth_attribute: _Optional_str = None, name_attribute: _Optional_str = None, surname_attribute: _Optional_str = None, yearOfBirth_attribute: _Optional_str = None) -> None:
        if born_attribute is not None:
            self._XMLID_ATTR_BORN_ATTRIBUTE = born_attribute
        if dayOfBirth_attribute is not None:
            self._XMLID_ATTR_DAYOFBIRTH_ATTRIBUTE = dayOfBirth_attribute
        if description_attribute is not None:
            self._XMLID_ATTR_DESCRIPTION_ATTRIBUTE = description_attribute
        if fromPlayer_attribute is not None:
            self._XMLID_ATTR_FROMPLAYER_ATTRIBUTE = fromPlayer_attribute
        if id_attribute is not None:
            self._XMLID_ATTR_ID_ATTRIBUTE = id_attribute
        if monthOfBirth_attribute is not None:
            self._XMLID_ATTR_MONTHOFBIRTH_ATTRIBUTE = monthOfBirth_attribute
        if name_attribute is not None:
            self._XMLID_ATTR_NAME_ATTRIBUTE = name_attribute
        if surname_attribute is not None:
            self._XMLID_ATTR_SURNAME_ATTRIBUTE = surname_attribute
        if yearOfBirth_attribute is not None:
            self._XMLID_ATTR_YEAROFBIRTH_ATTRIBUTE = yearOfBirth_attribute

    def overrideTags(self, born_element: _Optional_str = None, dayOfBirth_element: _Optional_str = None, description_element: _Optional_str = None, fromPlayer_element: _Optional_str = None, id_element: _Optional_str = None, monthOfBirth_element: _Optional_str = None, name_element: _Optional_str = None, surname_element: _Optional_str = None, yearOfBirth_element: _Optional_str = None) -> None:
        if born_element is not None:
            self._XMLID_TAG_BORN_ELEMENT = born_element
        if dayOfBirth_element is not None:
            self._XMLID_TAG_DAYOFBIRTH_ELEMENT = dayOfBirth_element
        if description_element is not None:
            self._XMLID_TAG_DESCRIPTION_ELEMENT = description_element
        if fromPlayer_element is not None:
            self._XMLID_TAG_FROMPLAYER_ELEMENT = fromPlayer_element
        if id_element is not None:
            self._XMLID_TAG_ID_ELEMENT = id_element
        if monthOfBirth_element is not None:
            self._XMLID_TAG_MONTHOFBIRTH_ELEMENT = monthOfBirth_element
        if name_element is not None:
            self._XMLID_TAG_NAME_ELEMENT = name_element
        if surname_element is not None:
            self._XMLID_TAG_SURNAME_ELEMENT = surname_element
        if yearOfBirth_element is not None:
            self._XMLID_TAG_YEAROFBIRTH_ELEMENT = yearOfBirth_element

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_ID_ELEMENT)) is not None:
            if self._XMLID_ATTR_ID_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_ID_ATTRIBUTE, 'id')
            self.id = sf.attrib[self._XMLID_ATTR_ID_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'id', self._XMLID_TAG_ID_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_FROMPLAYER_ELEMENT)) is not None:
            if self._XMLID_ATTR_FROMPLAYER_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_FROMPLAYER_ATTRIBUTE, 'fromPlayer')
            self.fromPlayer = XML2BOOL[sf.attrib[self._XMLID_ATTR_FROMPLAYER_ATTRIBUTE]]
        else:
            raise ElementRequiredException(self, e, 'fromPlayer', self._XMLID_TAG_FROMPLAYER_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_BORN_ELEMENT)) is not None:
            if self._XMLID_ATTR_BORN_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_BORN_ATTRIBUTE, 'born')
            self.born = XML2BOOL[sf.attrib[self._XMLID_ATTR_BORN_ATTRIBUTE]]
        else:
            raise ElementRequiredException(self, e, 'born', self._XMLID_TAG_BORN_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_NAME_ELEMENT)) is not None:
            self.name = CharacterNames()
            self.name.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'name', self._XMLID_TAG_NAME_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_SURNAME_ELEMENT)) is not None:
            if self._XMLID_ATTR_SURNAME_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_SURNAME_ATTRIBUTE, 'surname')
            self.surname = sf.attrib[self._XMLID_ATTR_SURNAME_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'surname', self._XMLID_TAG_SURNAME_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_DESCRIPTION_ELEMENT)) is not None:
            if self._XMLID_ATTR_DESCRIPTION_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_DESCRIPTION_ATTRIBUTE, 'description')
            self.description = sf.attrib[self._XMLID_ATTR_DESCRIPTION_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'description', self._XMLID_TAG_DESCRIPTION_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_YEAROFBIRTH_ELEMENT)) is not None:
            self.yearOfBirth = int(sf.attrib.get(self._XMLID_ATTR_YEAROFBIRTH_ATTRIBUTE, "None"))
        if (sf := e.find(self._XMLID_TAG_MONTHOFBIRTH_ELEMENT)) is not None:
            self.monthOfBirth = EMonth[sf.attrib.get(self._XMLID_ATTR_MONTHOFBIRTH_ATTRIBUTE, None)]
        if (sf := e.find(self._XMLID_TAG_DAYOFBIRTH_ELEMENT)) is not None:
            self.dayOfBirth = int(sf.attrib.get(self._XMLID_ATTR_DAYOFBIRTH_ATTRIBUTE, "None"))

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        etree.SubElement(e, self._XMLID_TAG_ID_ELEMENT, {self._XMLID_ATTR_ID_ATTRIBUTE: str(self.id)})
        etree.SubElement(e, self._XMLID_TAG_FROMPLAYER_ELEMENT, {self._XMLID_ATTR_FROMPLAYER_ATTRIBUTE: str(self.fromPlayer).lower()})
        etree.SubElement(e, self._XMLID_TAG_BORN_ELEMENT, {self._XMLID_ATTR_BORN_ATTRIBUTE: str(self.born).lower()})
        e.append(self.name.toXML(self._XMLID_TAG_NAME_ELEMENT))
        etree.SubElement(e, self._XMLID_TAG_SURNAME_ELEMENT, {self._XMLID_ATTR_SURNAME_ATTRIBUTE: str(self.surname)})
        etree.SubElement(e, self._XMLID_TAG_DESCRIPTION_ELEMENT, {self._XMLID_ATTR_DESCRIPTION_ATTRIBUTE: str(self.description)})
        if self.yearOfBirth is not None:
            etree.SubElement(e, self._XMLID_TAG_YEAROFBIRTH_ELEMENT, {self._XMLID_ATTR_YEAROFBIRTH_ATTRIBUTE: str(self.yearOfBirth)})
        if self.monthOfBirth is not None:
            etree.SubElement(e, self._XMLID_TAG_MONTHOFBIRTH_ELEMENT, {self._XMLID_ATTR_MONTHOFBIRTH_ATTRIBUTE: self.monthOfBirth.name})
        if self.dayOfBirth is not None:
            etree.SubElement(e, self._XMLID_TAG_DAYOFBIRTH_ELEMENT, {self._XMLID_ATTR_DAYOFBIRTH_ATTRIBUTE: str(self.dayOfBirth)})
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['id'] = str(self.id)
        data['fromPlayer'] = bool(self.fromPlayer)
        data['born'] = bool(self.born)
        data['name'] = self.name.toDict()
        data['surname'] = str(self.surname)
        data['description'] = str(self.description)
        if self.yearOfBirth is not None:
            data['yearOfBirth'] = int(self.yearOfBirth)
        if self.monthOfBirth is not None:
            data['monthOfBirth'] = self.monthOfBirth.name
        if self.dayOfBirth is not None:
            data['dayOfBirth'] = int(self.dayOfBirth)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('id')) is not None:
            self.id = data['id']
        else:
            raise KeyRequiredException(self, data, 'id', 'id')
        if (sf := data.get('fromPlayer')) is not None:
            self.fromPlayer = data['fromPlayer']
        else:
            raise KeyRequiredException(self, data, 'fromPlayer', 'fromPlayer')
        if (sf := data.get('born')) is not None:
            self.born = data['born']
        else:
            raise KeyRequiredException(self, data, 'born', 'born')
        if (sf := data.get('name')) is not None:
            self.name = CharacterNames()
            self.name.fromDict(data['name'])
        else:
            raise KeyRequiredException(self, data, 'name', 'name')
        if (sf := data.get('surname')) is not None:
            self.surname = data['surname']
        else:
            raise KeyRequiredException(self, data, 'surname', 'surname')
        if (sf := data.get('description')) is not None:
            self.description = data['description']
        else:
            raise KeyRequiredException(self, data, 'description', 'description')
        if (sf := data.get('yearOfBirth')) is not None:
            self.yearOfBirth = data['yearOfBirth']
        else:
            self.yearOfBirth = None
        if (sf := data.get('monthOfBirth')) is not None:
            self.monthOfBirth = data['monthOfBirth'].name
        else:
            self.monthOfBirth = None
        if (sf := data.get('dayOfBirth')) is not None:
            self.dayOfBirth = data['dayOfBirth']
        else:
            self.dayOfBirth = None

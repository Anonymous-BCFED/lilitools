#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawOutfit']
## from [LT]/src/com/lilithsthrone/game/inventory/outfit/Outfit.java: public Element saveAsXML(Element parentElement, Document doc) { @ kV+FIQuQ1AdJ9DDJDytWMUAXr8frpo2TW8n6b5XaWT717bOhLbQhMlLFnPyYpNdzFIAVc5WhN2lwhihzWBIX5A==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawOutfit(Savable):
    TAG = 'outfit'
    _XMLID_ATTR_CLOTHING_KEYATTR: str = 'id'
    _XMLID_ATTR_CLOTHING_VALUEATTR: str = 'typeId'
    _XMLID_ATTR_NAME_ATTRIBUTE: str = 'name'
    _XMLID_ATTR_OUTFITTYPE_ATTRIBUTE: str = 'outfitType'
    _XMLID_ATTR_WEAPONS_KEYATTR: str = 'id'
    _XMLID_ATTR_WEAPONS_VALUEATTR: str = 'typeId'
    _XMLID_TAG_CLOTHING_CHILD: str = 'c'
    _XMLID_TAG_CLOTHING_PARENT: str = 'clothing'
    _XMLID_TAG_WEAPONS_CHILD: str = 'w'
    _XMLID_TAG_WEAPONS_PARENT: str = 'weapons'

    def __init__(self) -> None:
        super().__init__()
        self.name: str = ''
        self.outfitType: str = ''
        self.weapons: Dict[str, str] = {}
        self.clothing: Dict[str, str] = {}

    def overrideAttrs(self, clothing_keyattr: _Optional_str = None, clothing_valueattr: _Optional_str = None, name_attribute: _Optional_str = None, outfitType_attribute: _Optional_str = None, weapons_keyattr: _Optional_str = None, weapons_valueattr: _Optional_str = None) -> None:
        if clothing_keyattr is not None:
            self._XMLID_ATTR_CLOTHING_KEYATTR = clothing_keyattr
        if clothing_valueattr is not None:
            self._XMLID_ATTR_CLOTHING_VALUEATTR = clothing_valueattr
        if name_attribute is not None:
            self._XMLID_ATTR_NAME_ATTRIBUTE = name_attribute
        if outfitType_attribute is not None:
            self._XMLID_ATTR_OUTFITTYPE_ATTRIBUTE = outfitType_attribute
        if weapons_keyattr is not None:
            self._XMLID_ATTR_WEAPONS_KEYATTR = weapons_keyattr
        if weapons_valueattr is not None:
            self._XMLID_ATTR_WEAPONS_VALUEATTR = weapons_valueattr

    def overrideTags(self, clothing_child: _Optional_str = None, clothing_parent: _Optional_str = None, weapons_child: _Optional_str = None, weapons_parent: _Optional_str = None) -> None:
        if clothing_child is not None:
            self._XMLID_TAG_CLOTHING_CHILD = clothing_child
        if clothing_parent is not None:
            self._XMLID_TAG_CLOTHING_PARENT = clothing_parent
        if weapons_child is not None:
            self._XMLID_TAG_WEAPONS_CHILD = weapons_child
        if weapons_parent is not None:
            self._XMLID_TAG_WEAPONS_PARENT = weapons_parent

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        dc: etree._Element
        value = e.get(self._XMLID_ATTR_NAME_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_NAME_ATTRIBUTE, 'name')
        else:
            self.name = str(value)
        value = e.get(self._XMLID_ATTR_OUTFITTYPE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_OUTFITTYPE_ATTRIBUTE, 'outfitType')
        else:
            self.outfitType = str(value)
        if (df := e.find(self._XMLID_TAG_WEAPONS_PARENT)) is not None:
            for dc in df.iterfind(self._XMLID_TAG_WEAPONS_CHILD):
                self.weapons[dc.attrib['id']] = dc.attrib['typeId']
        else:
            self.weapons = {}
        if (df := e.find(self._XMLID_TAG_CLOTHING_PARENT)) is not None:
            for dc in df.iterfind(self._XMLID_TAG_CLOTHING_CHILD):
                self.clothing[dc.attrib['id']] = dc.attrib['typeId']
        else:
            self.clothing = {}

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        dK: Any
        dV: Any
        dc: etree._Element
        dp: etree._Element
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_NAME_ATTRIBUTE] = str(self.name)
        e.attrib[self._XMLID_ATTR_OUTFITTYPE_ATTRIBUTE] = str(self.outfitType)
        dp = etree.SubElement(e, self._XMLID_TAG_WEAPONS_PARENT)
        for dK, dV in sorted(self.weapons.items(), key=lambda t: t[0]):
            dc = etree.SubElement(dp, self._XMLID_TAG_WEAPONS_CHILD)
            dc.attrib['id'] = dK
            dc.attrib['typeId'] = dV
        dp = etree.SubElement(e, self._XMLID_TAG_CLOTHING_PARENT)
        for dK, dV in sorted(self.clothing.items(), key=lambda t: t[0]):
            dc = etree.SubElement(dp, self._XMLID_TAG_CLOTHING_CHILD)
            dc.attrib['id'] = dK
            dc.attrib['typeId'] = dV
        return e

    def toDict(self) -> Dict[str, Any]:
        dK: Any
        dV: Any
        data: Dict[str, Any] = super().toDict()
        dp: Dict[str, Any]
        data['name'] = self.name
        data['outfitType'] = self.outfitType
        dp = {}
        for dK, dV in sorted(self.weapons.items(), key=lambda t: t[0]):
            sk = dK
            sv = dV
            dp[sk] = sv
        data['weapons'] = dp
        dp = {}
        for dK, dV in sorted(self.clothing.items(), key=lambda t: t[0]):
            sk = dK
            sv = dV
            dp[sk] = sv
        data['clothing'] = dp
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'name' not in data:
            raise KeyRequiredException(self, data, 'name', 'name')
        self.name = str(data['name'])
        if 'outfitType' not in data:
            raise KeyRequiredException(self, data, 'outfitType', 'outfitType')
        self.outfitType = str(data['outfitType'])
        if (df := data.get('weapons')) is not None:
            for sk, sv in df.items():
                self.weapons[sk] = sv
        else:
            self.weapons = {}
        if (df := data.get('clothing')) is not None:
            for sk, sv in df.items():
                self.clothing[sk] = sv
        else:
            self.clothing = {}

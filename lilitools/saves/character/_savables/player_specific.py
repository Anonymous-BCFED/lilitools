#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from collections import OrderedDict
from typing import Any, Dict, Optional, Set
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.quests.quest_updates import QuestUpdates
from lilitools.saves.savable import XML2BOOL, Savable
__all__ = ['RawPlayerSpecific']
## from [LT]/src/com/lilithsthrone/game/character/PlayerCharacter.java: public Element saveAsXML(Element parentElement, Document doc) { @ sBS0CtsgLnnbSc82BIV+fwR5ta5F0fIcwARiSmPFyUfGDlGC/PW8odPH7ZSdA0mCb6EhDaL4Qc8faQlc4W6sRA==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawPlayerSpecific(Savable):
    TAG = 'playerSpecific'
    _XMLID_ATTR_CHARACTERSENCOUNTERED_VALUEATTR: str = 'value'
    _XMLID_ATTR_FRIENDLYOCCUPANTS_VALUEATTR: str = 'id'
    _XMLID_ATTR_KARMA_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_MAINQUESTUPDATED_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_QUESTFAILEDMAP_KEYATTR: str = 'questLine'
    _XMLID_ATTR_QUESTFAILEDMAP_VALUEATTR: str = 'q'
    _XMLID_ATTR_QUESTUPDATES_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_RELATIONSHIPQUESTUPDATED_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_SIDEQUESTUPDATED_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_TITLE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_WORLDSVISITED_VALUEATTR: str = 'id'
    _XMLID_TAG_CHARACTERSENCOUNTERED_CHILD: str = 'id'
    _XMLID_TAG_CHARACTERSENCOUNTERED_PARENT: str = 'charactersEncountered'
    _XMLID_TAG_CLOTHINGDISCOVERED_CHILD: str = 'type'
    _XMLID_TAG_CLOTHINGDISCOVERED_PARENT: str = 'clothingDiscovered'
    _XMLID_TAG_FRIENDLYOCCUPANTS_CHILD: str = 'occupant'
    _XMLID_TAG_FRIENDLYOCCUPANTS_PARENT: str = 'friendlyOccupants'
    _XMLID_TAG_ITEMSDISCOVERED_CHILD: str = 'type'
    _XMLID_TAG_ITEMSDISCOVERED_PARENT: str = 'itemsDiscovered'
    _XMLID_TAG_KARMA_ELEMENT: str = 'karma'
    _XMLID_TAG_MAINQUESTUPDATED_ELEMENT: str = 'mainQuestUpdated'
    _XMLID_TAG_QUESTFAILEDMAP_CHILD: str = 'entry'
    _XMLID_TAG_QUESTFAILEDMAP_PARENT: str = 'questFailedMap'
    _XMLID_TAG_QUESTUPDATES_ELEMENT: str = 'questUpdates'
    _XMLID_TAG_RACEBOOKSDISCOVERED_CHILD: str = 'race'
    _XMLID_TAG_RACEBOOKSDISCOVERED_PARENT: str = 'raceBooksDiscovered'
    _XMLID_TAG_RACESDISCOVEREDADVANCED_CHILD: str = 'race'
    _XMLID_TAG_RACESDISCOVEREDADVANCED_PARENT: str = 'racesDiscoveredAdvanced'
    _XMLID_TAG_RACESDISCOVERED_CHILD: str = 'race'
    _XMLID_TAG_RACESDISCOVERED_PARENT: str = 'racesDiscovered'
    _XMLID_TAG_RELATIONSHIPQUESTUPDATED_ELEMENT: str = 'relationshipQuestUpdated'
    _XMLID_TAG_SIDEQUESTUPDATED_ELEMENT: str = 'sideQuestUpdated'
    _XMLID_TAG_TITLE_ELEMENT: str = 'title'
    _XMLID_TAG_WEAPONSDISCOVERED_CHILD: str = 'type'
    _XMLID_TAG_WEAPONSDISCOVERED_PARENT: str = 'weaponsDiscovered'
    _XMLID_TAG_WORLDSVISITED_CHILD: str = 'world'
    _XMLID_TAG_WORLDSVISITED_PARENT: str = 'worldsVisited'

    def __init__(self) -> None:
        super().__init__()
        self.title: str = ''  # Element
        self.karma: int = 0  # Element
        self.questUpdates: QuestUpdates = QuestUpdates()  # Element
        self.mainQuestUpdated: bool = False  # Element
        self.sideQuestUpdated: bool = False  # Element
        self.relationshipQuestUpdated: bool = False  # Element
        self.raceBooksDiscovered: Set[str] = set()
        self.charactersEncountered: Set[str] = set()
        self.questMap: OrderedDict[str, List[str]] = OrderedDict()
        self.questFailedMap: Dict[str, str] = {}
        self.friendlyOccupants: Set[str] = set()
        self.worldsVisited: Set[str] = set()
        self.itemsDiscovered: Set[str] = set()
        self.weaponsDiscovered: Set[str] = set()
        self.clothingDiscovered: Set[str] = set()
        self.racesDiscovered: Set[str] = set()
        self.racesDiscoveredAdvanced: Set[str] = set()

    def overrideAttrs(self, charactersEncountered_valueattr: _Optional_str = None, friendlyOccupants_valueattr: _Optional_str = None, karma_attribute: _Optional_str = None, mainQuestUpdated_attribute: _Optional_str = None, questFailedMap_keyattr: _Optional_str = None, questFailedMap_valueattr: _Optional_str = None, questUpdates_attribute: _Optional_str = None, relationshipQuestUpdated_attribute: _Optional_str = None, sideQuestUpdated_attribute: _Optional_str = None, title_attribute: _Optional_str = None, worldsVisited_valueattr: _Optional_str = None) -> None:
        if charactersEncountered_valueattr is not None:
            self._XMLID_ATTR_CHARACTERSENCOUNTERED_VALUEATTR = charactersEncountered_valueattr
        if friendlyOccupants_valueattr is not None:
            self._XMLID_ATTR_FRIENDLYOCCUPANTS_VALUEATTR = friendlyOccupants_valueattr
        if karma_attribute is not None:
            self._XMLID_ATTR_KARMA_ATTRIBUTE = karma_attribute
        if mainQuestUpdated_attribute is not None:
            self._XMLID_ATTR_MAINQUESTUPDATED_ATTRIBUTE = mainQuestUpdated_attribute
        if questFailedMap_keyattr is not None:
            self._XMLID_ATTR_QUESTFAILEDMAP_KEYATTR = questFailedMap_keyattr
        if questFailedMap_valueattr is not None:
            self._XMLID_ATTR_QUESTFAILEDMAP_VALUEATTR = questFailedMap_valueattr
        if questUpdates_attribute is not None:
            self._XMLID_ATTR_QUESTUPDATES_ATTRIBUTE = questUpdates_attribute
        if relationshipQuestUpdated_attribute is not None:
            self._XMLID_ATTR_RELATIONSHIPQUESTUPDATED_ATTRIBUTE = relationshipQuestUpdated_attribute
        if sideQuestUpdated_attribute is not None:
            self._XMLID_ATTR_SIDEQUESTUPDATED_ATTRIBUTE = sideQuestUpdated_attribute
        if title_attribute is not None:
            self._XMLID_ATTR_TITLE_ATTRIBUTE = title_attribute
        if worldsVisited_valueattr is not None:
            self._XMLID_ATTR_WORLDSVISITED_VALUEATTR = worldsVisited_valueattr

    def overrideTags(self, charactersEncountered_child: _Optional_str = None, charactersEncountered_parent: _Optional_str = None, clothingDiscovered_child: _Optional_str = None, clothingDiscovered_parent: _Optional_str = None, friendlyOccupants_child: _Optional_str = None, friendlyOccupants_parent: _Optional_str = None, itemsDiscovered_child: _Optional_str = None, itemsDiscovered_parent: _Optional_str = None, karma_element: _Optional_str = None, mainQuestUpdated_element: _Optional_str = None, questFailedMap_child: _Optional_str = None, questFailedMap_parent: _Optional_str = None, questUpdates_element: _Optional_str = None, raceBooksDiscovered_child: _Optional_str = None, raceBooksDiscovered_parent: _Optional_str = None, racesDiscovered_child: _Optional_str = None, racesDiscovered_parent: _Optional_str = None, racesDiscoveredAdvanced_child: _Optional_str = None, racesDiscoveredAdvanced_parent: _Optional_str = None, relationshipQuestUpdated_element: _Optional_str = None, sideQuestUpdated_element: _Optional_str = None, title_element: _Optional_str = None, weaponsDiscovered_child: _Optional_str = None, weaponsDiscovered_parent: _Optional_str = None, worldsVisited_child: _Optional_str = None, worldsVisited_parent: _Optional_str = None) -> None:
        if charactersEncountered_child is not None:
            self._XMLID_TAG_CHARACTERSENCOUNTERED_CHILD = charactersEncountered_child
        if charactersEncountered_parent is not None:
            self._XMLID_TAG_CHARACTERSENCOUNTERED_PARENT = charactersEncountered_parent
        if clothingDiscovered_child is not None:
            self._XMLID_TAG_CLOTHINGDISCOVERED_CHILD = clothingDiscovered_child
        if clothingDiscovered_parent is not None:
            self._XMLID_TAG_CLOTHINGDISCOVERED_PARENT = clothingDiscovered_parent
        if friendlyOccupants_child is not None:
            self._XMLID_TAG_FRIENDLYOCCUPANTS_CHILD = friendlyOccupants_child
        if friendlyOccupants_parent is not None:
            self._XMLID_TAG_FRIENDLYOCCUPANTS_PARENT = friendlyOccupants_parent
        if itemsDiscovered_child is not None:
            self._XMLID_TAG_ITEMSDISCOVERED_CHILD = itemsDiscovered_child
        if itemsDiscovered_parent is not None:
            self._XMLID_TAG_ITEMSDISCOVERED_PARENT = itemsDiscovered_parent
        if karma_element is not None:
            self._XMLID_TAG_KARMA_ELEMENT = karma_element
        if mainQuestUpdated_element is not None:
            self._XMLID_TAG_MAINQUESTUPDATED_ELEMENT = mainQuestUpdated_element
        if questFailedMap_child is not None:
            self._XMLID_TAG_QUESTFAILEDMAP_CHILD = questFailedMap_child
        if questFailedMap_parent is not None:
            self._XMLID_TAG_QUESTFAILEDMAP_PARENT = questFailedMap_parent
        if questUpdates_element is not None:
            self._XMLID_TAG_QUESTUPDATES_ELEMENT = questUpdates_element
        if raceBooksDiscovered_child is not None:
            self._XMLID_TAG_RACEBOOKSDISCOVERED_CHILD = raceBooksDiscovered_child
        if raceBooksDiscovered_parent is not None:
            self._XMLID_TAG_RACEBOOKSDISCOVERED_PARENT = raceBooksDiscovered_parent
        if racesDiscovered_child is not None:
            self._XMLID_TAG_RACESDISCOVERED_CHILD = racesDiscovered_child
        if racesDiscovered_parent is not None:
            self._XMLID_TAG_RACESDISCOVERED_PARENT = racesDiscovered_parent
        if racesDiscoveredAdvanced_child is not None:
            self._XMLID_TAG_RACESDISCOVEREDADVANCED_CHILD = racesDiscoveredAdvanced_child
        if racesDiscoveredAdvanced_parent is not None:
            self._XMLID_TAG_RACESDISCOVEREDADVANCED_PARENT = racesDiscoveredAdvanced_parent
        if relationshipQuestUpdated_element is not None:
            self._XMLID_TAG_RELATIONSHIPQUESTUPDATED_ELEMENT = relationshipQuestUpdated_element
        if sideQuestUpdated_element is not None:
            self._XMLID_TAG_SIDEQUESTUPDATED_ELEMENT = sideQuestUpdated_element
        if title_element is not None:
            self._XMLID_TAG_TITLE_ELEMENT = title_element
        if weaponsDiscovered_child is not None:
            self._XMLID_TAG_WEAPONSDISCOVERED_CHILD = weaponsDiscovered_child
        if weaponsDiscovered_parent is not None:
            self._XMLID_TAG_WEAPONSDISCOVERED_PARENT = weaponsDiscovered_parent
        if worldsVisited_child is not None:
            self._XMLID_TAG_WORLDSVISITED_CHILD = worldsVisited_child
        if worldsVisited_parent is not None:
            self._XMLID_TAG_WORLDSVISITED_PARENT = worldsVisited_parent

    def _fromXML_questMap(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromXML_questMap()')

    def _toXML_questMap(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toXML_questMap()')

    def _fromDict_questMap(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromDict_questMap()')

    def _toDict_questMap(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toDict_questMap()')

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        dc: etree._Element
        lk: str
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_TITLE_ELEMENT)) is not None:
            if self._XMLID_ATTR_TITLE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_TITLE_ATTRIBUTE, 'title')
            self.title = sf.attrib[self._XMLID_ATTR_TITLE_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'title', self._XMLID_TAG_TITLE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_KARMA_ELEMENT)) is not None:
            if self._XMLID_ATTR_KARMA_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_KARMA_ATTRIBUTE, 'karma')
            self.karma = int(sf.attrib[self._XMLID_ATTR_KARMA_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'karma', self._XMLID_TAG_KARMA_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_QUESTUPDATES_ELEMENT)) is not None:
            self.questUpdates = QuestUpdates()
            self.questUpdates.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'questUpdates', self._XMLID_TAG_QUESTUPDATES_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_MAINQUESTUPDATED_ELEMENT)) is not None:
            if self._XMLID_ATTR_MAINQUESTUPDATED_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_MAINQUESTUPDATED_ATTRIBUTE, 'mainQuestUpdated')
            self.mainQuestUpdated = XML2BOOL[sf.attrib[self._XMLID_ATTR_MAINQUESTUPDATED_ATTRIBUTE]]
        else:
            raise ElementRequiredException(self, e, 'mainQuestUpdated', self._XMLID_TAG_MAINQUESTUPDATED_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_SIDEQUESTUPDATED_ELEMENT)) is not None:
            if self._XMLID_ATTR_SIDEQUESTUPDATED_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_SIDEQUESTUPDATED_ATTRIBUTE, 'sideQuestUpdated')
            self.sideQuestUpdated = XML2BOOL[sf.attrib[self._XMLID_ATTR_SIDEQUESTUPDATED_ATTRIBUTE]]
        else:
            raise ElementRequiredException(self, e, 'sideQuestUpdated', self._XMLID_TAG_SIDEQUESTUPDATED_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_RELATIONSHIPQUESTUPDATED_ELEMENT)) is not None:
            if self._XMLID_ATTR_RELATIONSHIPQUESTUPDATED_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_RELATIONSHIPQUESTUPDATED_ATTRIBUTE, 'relationshipQuestUpdated')
            self.relationshipQuestUpdated = XML2BOOL[sf.attrib[self._XMLID_ATTR_RELATIONSHIPQUESTUPDATED_ATTRIBUTE]]
        else:
            raise ElementRequiredException(self, e, 'relationshipQuestUpdated', self._XMLID_TAG_RELATIONSHIPQUESTUPDATED_ELEMENT)
        if (lf := e.find(self._XMLID_TAG_RACEBOOKSDISCOVERED_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_RACEBOOKSDISCOVERED_CHILD):
                    self.raceBooksDiscovered.add(lc.text)
        else:
            self.raceBooksDiscovered = set()
        if (lf := e.find(self._XMLID_TAG_CHARACTERSENCOUNTERED_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_CHARACTERSENCOUNTERED_CHILD):
                    self.charactersEncountered.add(lc.attrib[self._XMLID_ATTR_CHARACTERSENCOUNTERED_VALUEATTR])
        else:
            self.charactersEncountered = set()
        self._fromXML_questMap(e)
        if (df := e.find(self._XMLID_TAG_QUESTFAILEDMAP_PARENT)) is not None:
            for dc in df.iterfind(self._XMLID_TAG_QUESTFAILEDMAP_CHILD):
                self.questFailedMap[dc.attrib['questLine']] = dc.attrib['q']
        else:
            self.questFailedMap = {}
        if (lf := e.find(self._XMLID_TAG_FRIENDLYOCCUPANTS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_FRIENDLYOCCUPANTS_CHILD):
                    self.friendlyOccupants.add(lc.attrib[self._XMLID_ATTR_FRIENDLYOCCUPANTS_VALUEATTR])
        else:
            self.friendlyOccupants = set()
        if (lf := e.find(self._XMLID_TAG_WORLDSVISITED_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_WORLDSVISITED_CHILD):
                    self.worldsVisited.add(lc.attrib[self._XMLID_ATTR_WORLDSVISITED_VALUEATTR])
        else:
            self.worldsVisited = set()
        if (lf := e.find(self._XMLID_TAG_ITEMSDISCOVERED_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_ITEMSDISCOVERED_CHILD):
                    self.itemsDiscovered.add(lc.text)
        else:
            self.itemsDiscovered = set()
        if (lf := e.find(self._XMLID_TAG_WEAPONSDISCOVERED_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_WEAPONSDISCOVERED_CHILD):
                    self.weaponsDiscovered.add(lc.text)
        else:
            self.weaponsDiscovered = set()
        if (lf := e.find(self._XMLID_TAG_CLOTHINGDISCOVERED_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_CLOTHINGDISCOVERED_CHILD):
                    self.clothingDiscovered.add(lc.text)
        else:
            self.clothingDiscovered = set()
        if (lf := e.find(self._XMLID_TAG_RACESDISCOVERED_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_RACESDISCOVERED_CHILD):
                    self.racesDiscovered.add(lc.text)
        else:
            self.racesDiscovered = set()
        if (lf := e.find(self._XMLID_TAG_RACESDISCOVEREDADVANCED_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_RACESDISCOVEREDADVANCED_CHILD):
                    self.racesDiscoveredAdvanced.add(lc.text)
        else:
            self.racesDiscoveredAdvanced = set()

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        dK: Any
        dV: Any
        dc: etree._Element
        dp: etree._Element
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        etree.SubElement(e, self._XMLID_TAG_TITLE_ELEMENT, {self._XMLID_ATTR_TITLE_ATTRIBUTE: str(self.title)})
        etree.SubElement(e, self._XMLID_TAG_KARMA_ELEMENT, {self._XMLID_ATTR_KARMA_ATTRIBUTE: str(self.karma)})
        e.append(self.questUpdates.toXML(self._XMLID_TAG_QUESTUPDATES_ELEMENT))
        etree.SubElement(e, self._XMLID_TAG_MAINQUESTUPDATED_ELEMENT, {self._XMLID_ATTR_MAINQUESTUPDATED_ATTRIBUTE: str(self.mainQuestUpdated).lower()})
        etree.SubElement(e, self._XMLID_TAG_SIDEQUESTUPDATED_ELEMENT, {self._XMLID_ATTR_SIDEQUESTUPDATED_ATTRIBUTE: str(self.sideQuestUpdated).lower()})
        etree.SubElement(e, self._XMLID_TAG_RELATIONSHIPQUESTUPDATED_ELEMENT, {self._XMLID_ATTR_RELATIONSHIPQUESTUPDATED_ATTRIBUTE: str(self.relationshipQuestUpdated).lower()})
        lp = etree.SubElement(e, self._XMLID_TAG_RACEBOOKSDISCOVERED_PARENT)
        for lv in sorted(self.raceBooksDiscovered, key=lambda x: x):
            etree.SubElement(lp, self._XMLID_TAG_RACEBOOKSDISCOVERED_CHILD, {}).text = lv
        lp = etree.SubElement(e, self._XMLID_TAG_CHARACTERSENCOUNTERED_PARENT)
        for lv in sorted(self.charactersEncountered, key=lambda x: x):
            etree.SubElement(lp, self._XMLID_TAG_CHARACTERSENCOUNTERED_CHILD, {self._XMLID_ATTR_CHARACTERSENCOUNTERED_VALUEATTR: lv})
        self._toXML_questMap(e)
        dp = etree.SubElement(e, self._XMLID_TAG_QUESTFAILEDMAP_PARENT)
        for dK, dV in sorted(self.questFailedMap.items(), key=lambda t: t[0]):
            dc = etree.SubElement(dp, self._XMLID_TAG_QUESTFAILEDMAP_CHILD)
            dc.attrib['questLine'] = dK
            dc.attrib['q'] = dV
        lp = etree.SubElement(e, self._XMLID_TAG_FRIENDLYOCCUPANTS_PARENT)
        for lv in sorted(self.friendlyOccupants, key=lambda x: x):
            etree.SubElement(lp, self._XMLID_TAG_FRIENDLYOCCUPANTS_CHILD, {self._XMLID_ATTR_FRIENDLYOCCUPANTS_VALUEATTR: lv})
        lp = etree.SubElement(e, self._XMLID_TAG_WORLDSVISITED_PARENT)
        for lv in sorted(self.worldsVisited, key=lambda x: x):
            etree.SubElement(lp, self._XMLID_TAG_WORLDSVISITED_CHILD, {self._XMLID_ATTR_WORLDSVISITED_VALUEATTR: lv})
        lp = etree.SubElement(e, self._XMLID_TAG_ITEMSDISCOVERED_PARENT)
        for lv in sorted(self.itemsDiscovered, key=lambda x: x):
            etree.SubElement(lp, self._XMLID_TAG_ITEMSDISCOVERED_CHILD, {}).text = lv
        lp = etree.SubElement(e, self._XMLID_TAG_WEAPONSDISCOVERED_PARENT)
        for lv in sorted(self.weaponsDiscovered, key=lambda x: x):
            etree.SubElement(lp, self._XMLID_TAG_WEAPONSDISCOVERED_CHILD, {}).text = lv
        lp = etree.SubElement(e, self._XMLID_TAG_CLOTHINGDISCOVERED_PARENT)
        for lv in sorted(self.clothingDiscovered, key=lambda x: x):
            etree.SubElement(lp, self._XMLID_TAG_CLOTHINGDISCOVERED_CHILD, {}).text = lv
        lp = etree.SubElement(e, self._XMLID_TAG_RACESDISCOVERED_PARENT)
        for lv in sorted(self.racesDiscovered, key=lambda x: x):
            etree.SubElement(lp, self._XMLID_TAG_RACESDISCOVERED_CHILD, {}).text = lv
        lp = etree.SubElement(e, self._XMLID_TAG_RACESDISCOVEREDADVANCED_PARENT)
        for lv in sorted(self.racesDiscoveredAdvanced, key=lambda x: x):
            etree.SubElement(lp, self._XMLID_TAG_RACESDISCOVEREDADVANCED_CHILD, {}).text = lv
        return e

    def toDict(self) -> Dict[str, Any]:
        dK: Any
        dV: Any
        data: Dict[str, Any] = super().toDict()
        dp: Dict[str, Any]
        data['title'] = str(self.title)
        data['karma'] = int(self.karma)
        data['questUpdates'] = self.questUpdates.toDict()
        data['mainQuestUpdated'] = bool(self.mainQuestUpdated)
        data['sideQuestUpdated'] = bool(self.sideQuestUpdated)
        data['relationshipQuestUpdated'] = bool(self.relationshipQuestUpdated)
        if self.raceBooksDiscovered is None or len(self.raceBooksDiscovered) > 0:
            data['raceBooksDiscovered'] = list()
        else:
            lc = list()
            if len(self.raceBooksDiscovered) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['raceBooksDiscovered'] = lc
        if self.charactersEncountered is None or len(self.charactersEncountered) > 0:
            data['charactersEncountered'] = list()
        else:
            lc = list()
            if len(self.charactersEncountered) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['charactersEncountered'] = lc
        self._toDict_questMap(data)
        dp = {}
        for dK, dV in sorted(self.questFailedMap.items(), key=lambda t: t[0]):
            sk = dK
            sv = dV
            dp[sk] = sv
        data['questFailedMap'] = dp
        if self.friendlyOccupants is None or len(self.friendlyOccupants) > 0:
            data['friendlyOccupants'] = list()
        else:
            lc = list()
            if len(self.friendlyOccupants) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['friendlyOccupants'] = lc
        if self.worldsVisited is None or len(self.worldsVisited) > 0:
            data['worldsVisited'] = list()
        else:
            lc = list()
            if len(self.worldsVisited) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['worldsVisited'] = lc
        if self.itemsDiscovered is None or len(self.itemsDiscovered) > 0:
            data['itemsDiscovered'] = list()
        else:
            lc = list()
            if len(self.itemsDiscovered) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['itemsDiscovered'] = lc
        if self.weaponsDiscovered is None or len(self.weaponsDiscovered) > 0:
            data['weaponsDiscovered'] = list()
        else:
            lc = list()
            if len(self.weaponsDiscovered) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['weaponsDiscovered'] = lc
        if self.clothingDiscovered is None or len(self.clothingDiscovered) > 0:
            data['clothingDiscovered'] = list()
        else:
            lc = list()
            if len(self.clothingDiscovered) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['clothingDiscovered'] = lc
        if self.racesDiscovered is None or len(self.racesDiscovered) > 0:
            data['racesDiscovered'] = list()
        else:
            lc = list()
            if len(self.racesDiscovered) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['racesDiscovered'] = lc
        if self.racesDiscoveredAdvanced is None or len(self.racesDiscoveredAdvanced) > 0:
            data['racesDiscoveredAdvanced'] = list()
        else:
            lc = list()
            if len(self.racesDiscoveredAdvanced) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['racesDiscoveredAdvanced'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('title')) is not None:
            self.title = data['title']
        else:
            raise KeyRequiredException(self, data, 'title', 'title')
        if (sf := data.get('karma')) is not None:
            self.karma = data['karma']
        else:
            raise KeyRequiredException(self, data, 'karma', 'karma')
        if (sf := data.get('questUpdates')) is not None:
            self.questUpdates = QuestUpdates()
            self.questUpdates.fromDict(data['questUpdates'])
        else:
            raise KeyRequiredException(self, data, 'questUpdates', 'questUpdates')
        if (sf := data.get('mainQuestUpdated')) is not None:
            self.mainQuestUpdated = data['mainQuestUpdated']
        else:
            raise KeyRequiredException(self, data, 'mainQuestUpdated', 'mainQuestUpdated')
        if (sf := data.get('sideQuestUpdated')) is not None:
            self.sideQuestUpdated = data['sideQuestUpdated']
        else:
            raise KeyRequiredException(self, data, 'sideQuestUpdated', 'sideQuestUpdated')
        if (sf := data.get('relationshipQuestUpdated')) is not None:
            self.relationshipQuestUpdated = data['relationshipQuestUpdated']
        else:
            raise KeyRequiredException(self, data, 'relationshipQuestUpdated', 'relationshipQuestUpdated')
        if (lv := self.raceBooksDiscovered) is not None:
            for le in lv:
                self.raceBooksDiscovered.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'raceBooksDiscovered', 'raceBooksDiscovered')
        if (lv := self.charactersEncountered) is not None:
            for le in lv:
                self.charactersEncountered.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'charactersEncountered', 'charactersEncountered')
        self._fromDict_questMap(data)
        if (df := data.get('questFailedMap')) is not None:
            for sk, sv in df.items():
                self.questFailedMap[sk] = sv
        else:
            self.questFailedMap = {}
        if (lv := self.friendlyOccupants) is not None:
            for le in lv:
                self.friendlyOccupants.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'friendlyOccupants', 'friendlyOccupants')
        if (lv := self.worldsVisited) is not None:
            for le in lv:
                self.worldsVisited.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'worldsVisited', 'worldsVisited')
        if (lv := self.itemsDiscovered) is not None:
            for le in lv:
                self.itemsDiscovered.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'itemsDiscovered', 'itemsDiscovered')
        if (lv := self.weaponsDiscovered) is not None:
            for le in lv:
                self.weaponsDiscovered.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'weaponsDiscovered', 'weaponsDiscovered')
        if (lv := self.clothingDiscovered) is not None:
            for le in lv:
                self.clothingDiscovered.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'clothingDiscovered', 'clothingDiscovered')
        if (lv := self.racesDiscovered) is not None:
            for le in lv:
                self.racesDiscovered.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'racesDiscovered', 'racesDiscovered')
        if (lv := self.racesDiscoveredAdvanced) is not None:
            for le in lv:
                self.racesDiscoveredAdvanced.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'racesDiscoveredAdvanced', 'racesDiscoveredAdvanced')

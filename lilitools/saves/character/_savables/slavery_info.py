#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from collections import OrderedDict
from typing import Any, Dict, List, Optional, Set
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL, Savable
from lilitools.saves.slavery.slave_station import SlaveStation
__all__ = ['RawSlaveryInfo']
## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public Element saveAsXML(Element parentElement, Document doc) { @ Ra1rfw7eKd5WT2JXoGa/zLFvb/XgZlpPNZbY2BZwgdTA/t0DkASy5ADAtBbX2DvSTxI43cQAGTPbPsMVcAHyRQ==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawSlaveryInfo(Savable):
    TAG = 'slavery'
    _XMLID_ATTR_ABLETOBEENSLAVED_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_OWNER_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_SLAVESOWNED_VALUEATTR: str = 'id'
    _XMLID_ATTR_SLAVESTATION_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_VALUE_ATTRIBUTE: str = 'value'
    _XMLID_TAG_ABLETOBEENSLAVED_ELEMENT: str = 'ableToBeEnslaved'
    _XMLID_TAG_OWNER_ELEMENT: str = 'owner'
    _XMLID_TAG_SLAVESOWNED_CHILD: str = 'slave'
    _XMLID_TAG_SLAVESOWNED_PARENT: str = 'slavesOwned'
    _XMLID_TAG_SLAVESTATION_ELEMENT: str = 'slaveStation'
    _XMLID_TAG_VALUE_ELEMENT: str = 'value'

    def __init__(self) -> None:
        super().__init__()
        self.ableToBeEnslaved: bool = False  # Element
        self.slaveStation: Optional[SlaveStation] = None  # Element
        self.slavesOwned: List[str] = list()
        self.owner: Optional[str] = None  # Element
        self.slaveJobSettings: OrderedDict[str, Set[str]] = OrderedDict()
        self.slavePermissionSettings: OrderedDict[str, Set[str]] = OrderedDict()
        self.slaveAssignedJobs: List[str] = ['IDLE'] * 24
        self.value: int = 0  # Element

    def overrideAttrs(self, ableToBeEnslaved_attribute: _Optional_str = None, owner_attribute: _Optional_str = None, slaveStation_attribute: _Optional_str = None, slavesOwned_valueattr: _Optional_str = None, value_attribute: _Optional_str = None) -> None:
        if ableToBeEnslaved_attribute is not None:
            self._XMLID_ATTR_ABLETOBEENSLAVED_ATTRIBUTE = ableToBeEnslaved_attribute
        if owner_attribute is not None:
            self._XMLID_ATTR_OWNER_ATTRIBUTE = owner_attribute
        if slaveStation_attribute is not None:
            self._XMLID_ATTR_SLAVESTATION_ATTRIBUTE = slaveStation_attribute
        if slavesOwned_valueattr is not None:
            self._XMLID_ATTR_SLAVESOWNED_VALUEATTR = slavesOwned_valueattr
        if value_attribute is not None:
            self._XMLID_ATTR_VALUE_ATTRIBUTE = value_attribute

    def overrideTags(self, ableToBeEnslaved_element: _Optional_str = None, owner_element: _Optional_str = None, slaveStation_element: _Optional_str = None, slavesOwned_child: _Optional_str = None, slavesOwned_parent: _Optional_str = None, value_element: _Optional_str = None) -> None:
        if ableToBeEnslaved_element is not None:
            self._XMLID_TAG_ABLETOBEENSLAVED_ELEMENT = ableToBeEnslaved_element
        if owner_element is not None:
            self._XMLID_TAG_OWNER_ELEMENT = owner_element
        if slaveStation_element is not None:
            self._XMLID_TAG_SLAVESTATION_ELEMENT = slaveStation_element
        if slavesOwned_child is not None:
            self._XMLID_TAG_SLAVESOWNED_CHILD = slavesOwned_child
        if slavesOwned_parent is not None:
            self._XMLID_TAG_SLAVESOWNED_PARENT = slavesOwned_parent
        if value_element is not None:
            self._XMLID_TAG_VALUE_ELEMENT = value_element

    def _fromXML_slaveJobSettings(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromXML_slaveJobSettings()')

    def _toXML_slaveJobSettings(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toXML_slaveJobSettings()')

    def _fromDict_slaveJobSettings(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromDict_slaveJobSettings()')

    def _toDict_slaveJobSettings(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toDict_slaveJobSettings()')

    def _fromXML_slavePermissionSettings(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromXML_slavePermissionSettings()')

    def _toXML_slavePermissionSettings(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toXML_slavePermissionSettings()')

    def _fromDict_slavePermissionSettings(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromDict_slavePermissionSettings()')

    def _toDict_slavePermissionSettings(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toDict_slavePermissionSettings()')

    def _fromXML_slaveAssignedJobs(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromXML_slaveAssignedJobs()')

    def _toXML_slaveAssignedJobs(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toXML_slaveAssignedJobs()')

    def _fromDict_slaveAssignedJobs(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromDict_slaveAssignedJobs()')

    def _toDict_slaveAssignedJobs(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toDict_slaveAssignedJobs()')

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_ABLETOBEENSLAVED_ELEMENT)) is not None:
            self.ableToBeEnslaved = XML2BOOL[sf.attrib.get(self._XMLID_ATTR_ABLETOBEENSLAVED_ATTRIBUTE, 'false')]
        else:
            self.ableToBeEnslaved = False
        if (sf := e.find(self._XMLID_TAG_SLAVESTATION_ELEMENT)) is not None:
            self.slaveStation = SlaveStation()
            self.slaveStation.fromXML(sf)
        if (lf := e.find(self._XMLID_TAG_SLAVESOWNED_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_SLAVESOWNED_CHILD):
                    self.slavesOwned.append(lc.attrib[self._XMLID_ATTR_SLAVESOWNED_VALUEATTR])
        else:
            self.slavesOwned = list()
        if (sf := e.find(self._XMLID_TAG_OWNER_ELEMENT)) is not None:
            if self._XMLID_ATTR_OWNER_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_OWNER_ATTRIBUTE, 'owner')
            if sf.attrib[self._XMLID_ATTR_OWNER_ATTRIBUTE] == '':
                self.owner = None
            else:
                self.owner = sf.attrib[self._XMLID_ATTR_OWNER_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'owner', self._XMLID_TAG_OWNER_ELEMENT)
        self._fromXML_slaveJobSettings(e)
        self._fromXML_slavePermissionSettings(e)
        self._fromXML_slaveAssignedJobs(e)
        if (sf := e.find(self._XMLID_TAG_VALUE_ELEMENT)) is not None:
            self.value = int(sf.attrib.get(self._XMLID_ATTR_VALUE_ATTRIBUTE, "0"))
        else:
            self.value = 0

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        if self.ableToBeEnslaved is not None and self.ableToBeEnslaved != False:
            etree.SubElement(e, self._XMLID_TAG_ABLETOBEENSLAVED_ELEMENT, {self._XMLID_ATTR_ABLETOBEENSLAVED_ATTRIBUTE: str(self.ableToBeEnslaved).lower()})
        if self.slaveStation is not None:
            e.append(self.slaveStation.toXML(self._XMLID_TAG_SLAVESTATION_ELEMENT))
        lp = etree.SubElement(e, self._XMLID_TAG_SLAVESOWNED_PARENT)
        for lv in self.slavesOwned:
            etree.SubElement(lp, self._XMLID_TAG_SLAVESOWNED_CHILD, {self._XMLID_ATTR_SLAVESOWNED_VALUEATTR: lv})
        if self.owner is not None:
            etree.SubElement(e, self._XMLID_TAG_OWNER_ELEMENT, {self._XMLID_ATTR_OWNER_ATTRIBUTE: str(self.owner)})
        else:
            etree.SubElement(e, self._XMLID_TAG_OWNER_ELEMENT, {self._XMLID_ATTR_OWNER_ATTRIBUTE: ''})
        self._toXML_slaveJobSettings(e)
        self._toXML_slavePermissionSettings(e)
        self._toXML_slaveAssignedJobs(e)
        if self.value is not None and self.value != 0:
            etree.SubElement(e, self._XMLID_TAG_VALUE_ELEMENT, {self._XMLID_ATTR_VALUE_ATTRIBUTE: str(self.value)})
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        if self.ableToBeEnslaved is not None:
            data['ableToBeEnslaved'] = bool(self.ableToBeEnslaved)
        if self.slaveStation is not None:
            data['slaveStation'] = self.slaveStation.toDict()
        if self.slavesOwned is None or len(self.slavesOwned) > 0:
            data['slavesOwned'] = list()
        else:
            lc = list()
            if len(self.slavesOwned) > 0:
                for lv in lc:
                    lc.append(str(lv))
            data['slavesOwned'] = lc
        data['owner'] = str(self.owner)
        self._toDict_slaveJobSettings(data)
        self._toDict_slavePermissionSettings(data)
        self._toDict_slaveAssignedJobs(data)
        if self.value is not None:
            data['value'] = int(self.value)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('ableToBeEnslaved')) is not None:
            self.ableToBeEnslaved = data['ableToBeEnslaved']
        else:
            self.ableToBeEnslaved = False
        if (sf := data.get('slaveStation')) is not None:
            self.slaveStation = SlaveStation()
            self.slaveStation.fromDict(data['slaveStation'])
        else:
            self.slaveStation = None
        if (lv := self.slavesOwned) is not None:
            for le in lv:
                self.slavesOwned.append(str(le))
        else:
            raise KeyRequiredException(self, data, 'slavesOwned', 'slavesOwned')
        if (sf := data.get('owner')) is not None:
            self.owner = data['owner']
        else:
            raise KeyRequiredException(self, data, 'owner', 'owner')
        self._fromDict_slaveJobSettings(data)
        self._fromDict_slavePermissionSettings(data)
        self._fromDict_slaveAssignedJobs(data)
        if (sf := data.get('value')) is not None:
            self.value = data['value']
        else:
            self.value = 0

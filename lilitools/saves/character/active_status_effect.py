from typing import Optional, Union

from lilitools.saves.character._savables.active_status_effect import RawActiveStatusEffect
from lilitools.saves.character.status_effect_types import STATUS_EFFECT_TYPES
from lilitools.saves.modfiles.character.status_effects.status_effect_type import StatusEffectType


class ActiveStatusEffect(RawActiveStatusEffect):
    def getStatusEffectType(self) -> Optional[StatusEffectType]:
        return STATUS_EFFECT_TYPES.get(self.type)

    def setStatusEffectType(self, type: Union[str,StatusEffectType]) -> None:
        typeid: str = ''
        if isinstance(type, StatusEffectType):
            typeid = type.name
        if isinstance(type, str):
            typeid = type
        self.type = typeid

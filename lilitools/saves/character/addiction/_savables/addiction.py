#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional, Set
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawAddiction']
## from [LT]/src/com/lilithsthrone/game/character/effects/Addiction.java: public Element saveAsXML(Element parentElement, Document doc) { @ hIVDfMOASbgnmU9qHquLXtCsJa4puGXKBnlq7hs1Q1981ZPbf2WhBad5MCiCwnmuVKsEs/FF+iA0I2FK5oY7dw==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawAddiction(Savable):
    TAG = 'addiction'
    _XMLID_ATTR_FLUID_ATTRIBUTE: str = 'fluid'
    _XMLID_ATTR_LASTTIMESATISFIED_ATTRIBUTE: str = 'lastTimeSatisfied'
    _XMLID_ATTR_PROVIDERS_VALUEATTR: str = 'value'
    _XMLID_TAG_PROVIDERS_CHILD: str = 'id'
    _XMLID_TAG_PROVIDERS_PARENT: str = 'providerIDs'

    def __init__(self) -> None:
        super().__init__()
        self.fluid: str = ''
        self.lastTimeSatisfied: int = 0
        self.providers: Set[str] = set()

    def overrideAttrs(self, fluid_attribute: _Optional_str = None, lastTimeSatisfied_attribute: _Optional_str = None, providers_valueattr: _Optional_str = None) -> None:
        if fluid_attribute is not None:
            self._XMLID_ATTR_FLUID_ATTRIBUTE = fluid_attribute
        if lastTimeSatisfied_attribute is not None:
            self._XMLID_ATTR_LASTTIMESATISFIED_ATTRIBUTE = lastTimeSatisfied_attribute
        if providers_valueattr is not None:
            self._XMLID_ATTR_PROVIDERS_VALUEATTR = providers_valueattr

    def overrideTags(self, providers_child: _Optional_str = None, providers_parent: _Optional_str = None) -> None:
        if providers_child is not None:
            self._XMLID_TAG_PROVIDERS_CHILD = providers_child
        if providers_parent is not None:
            self._XMLID_TAG_PROVIDERS_PARENT = providers_parent

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        value = e.get(self._XMLID_ATTR_FLUID_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_FLUID_ATTRIBUTE, 'fluid')
        else:
            self.fluid = str(value)
        value = e.get(self._XMLID_ATTR_LASTTIMESATISFIED_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_LASTTIMESATISFIED_ATTRIBUTE, 'lastTimeSatisfied')
        else:
            self.lastTimeSatisfied = int(value)
        if (lf := e.find(self._XMLID_TAG_PROVIDERS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_PROVIDERS_CHILD):
                    self.providers.add(lc.attrib[self._XMLID_ATTR_PROVIDERS_VALUEATTR])
        else:
            self.providers = set()

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        e.attrib[self._XMLID_ATTR_FLUID_ATTRIBUTE] = str(self.fluid)
        e.attrib[self._XMLID_ATTR_LASTTIMESATISFIED_ATTRIBUTE] = str(self.lastTimeSatisfied)
        lp = etree.SubElement(e, self._XMLID_TAG_PROVIDERS_PARENT)
        for lv in sorted(self.providers, key=str):
            etree.SubElement(lp, self._XMLID_TAG_PROVIDERS_CHILD, {self._XMLID_ATTR_PROVIDERS_VALUEATTR: lv})
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['fluid'] = self.fluid
        data['lastTimeSatisfied'] = int(self.lastTimeSatisfied)
        if self.providers is None or len(self.providers) > 0:
            data['providers'] = list()
        else:
            lc = list()
            if len(self.providers) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['providers'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'fluid' not in data:
            raise KeyRequiredException(self, data, 'fluid', 'fluid')
        self.fluid = str(data['fluid'])
        if 'lastTimeSatisfied' not in data:
            raise KeyRequiredException(self, data, 'lastTimeSatisfied', 'lastTimeSatisfied')
        self.lastTimeSatisfied = int(data['lastTimeSatisfied'])
        if (lv := self.providers) is not None:
            for le in lv:
                self.providers.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'providers', 'providers')

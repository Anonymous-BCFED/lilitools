#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional, Set
from lxml import etree
from lilitools.saves.character.addiction.addiction import Addiction
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawAddictionsCore']
## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public Element saveAsXML(Element parentElement, Document doc) { @ Ra1rfw7eKd5WT2JXoGa/zLFvb/XgZlpPNZbY2BZwgdTA/t0DkASy5ADAtBbX2DvSTxI43cQAGTPbPsMVcAHyRQ==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawAddictionsCore(Savable):
    TAG = 'addictionsCore'
    _XMLID_ATTR_ALCOHOLLEVEL_ATTRIBUTE: str = 'alcoholLevel'
    _XMLID_ATTR_PSYCHOACTIVEFLUIDS_VALUEATTR: str = 'value'
    _XMLID_TAG_ADDICTIONS_CHILD: str = 'addiction'
    _XMLID_TAG_ADDICTIONS_PARENT: str = 'addictions'
    _XMLID_TAG_ADDICTIONS_VALUEELEM: Optional[str] = None
    _XMLID_TAG_PSYCHOACTIVEFLUIDS_CHILD: str = 'fluid'
    _XMLID_TAG_PSYCHOACTIVEFLUIDS_PARENT: str = 'psychoactiveFluids'

    def __init__(self) -> None:
        super().__init__()
        self.alcoholLevel: float = 0.
        self.addictions: Set[Addiction] = set()
        self.psychoactiveFluids: Set[str] = set()

    def overrideAttrs(self, alcoholLevel_attribute: _Optional_str = None, psychoactiveFluids_valueattr: _Optional_str = None) -> None:
        if alcoholLevel_attribute is not None:
            self._XMLID_ATTR_ALCOHOLLEVEL_ATTRIBUTE = alcoholLevel_attribute
        if psychoactiveFluids_valueattr is not None:
            self._XMLID_ATTR_PSYCHOACTIVEFLUIDS_VALUEATTR = psychoactiveFluids_valueattr

    def overrideTags(self, addictions_child: _Optional_str = None, addictions_parent: _Optional_str = None, addictions_valueelem: _Optional_str = None, psychoactiveFluids_child: _Optional_str = None, psychoactiveFluids_parent: _Optional_str = None) -> None:
        if addictions_child is not None:
            self._XMLID_TAG_ADDICTIONS_CHILD = addictions_child
        if addictions_parent is not None:
            self._XMLID_TAG_ADDICTIONS_PARENT = addictions_parent
        if addictions_valueelem is not None:
            self._XMLID_TAG_ADDICTIONS_VALUEELEM = addictions_valueelem
        if psychoactiveFluids_child is not None:
            self._XMLID_TAG_PSYCHOACTIVEFLUIDS_CHILD = psychoactiveFluids_child
        if psychoactiveFluids_parent is not None:
            self._XMLID_TAG_PSYCHOACTIVEFLUIDS_PARENT = psychoactiveFluids_parent

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        lv: Savable
        value = e.get(self._XMLID_ATTR_ALCOHOLLEVEL_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_ALCOHOLLEVEL_ATTRIBUTE, 'alcoholLevel')
        else:
            self.alcoholLevel = float(value)
        if (lf := e.find(self._XMLID_TAG_ADDICTIONS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_ADDICTIONS_CHILD):
                    lv = Addiction()
                    lv.fromXML(lc)
                    self.addictions.add(lv)
        else:
            self.addictions = set()
        if (lf := e.find(self._XMLID_TAG_PSYCHOACTIVEFLUIDS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_PSYCHOACTIVEFLUIDS_CHILD):
                    self.psychoactiveFluids.add(lc.attrib[self._XMLID_ATTR_PSYCHOACTIVEFLUIDS_VALUEATTR])
        else:
            self.psychoactiveFluids = set()

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        e.attrib[self._XMLID_ATTR_ALCOHOLLEVEL_ATTRIBUTE] = str(float(self.alcoholLevel))
        lp = etree.SubElement(e, self._XMLID_TAG_ADDICTIONS_PARENT)
        for lv in sorted(self.addictions, key=lambda x: x.fluid):
            lp.append(lv.toXML())
        lp = etree.SubElement(e, self._XMLID_TAG_PSYCHOACTIVEFLUIDS_PARENT)
        for lv in sorted(self.psychoactiveFluids, key=str):
            etree.SubElement(lp, self._XMLID_TAG_PSYCHOACTIVEFLUIDS_CHILD, {self._XMLID_ATTR_PSYCHOACTIVEFLUIDS_VALUEATTR: lv})
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['alcoholLevel'] = float(self.alcoholLevel)
        if self.addictions is None or len(self.addictions) > 0:
            data['addictions'] = list()
        else:
            lc = list()
            if len(self.addictions) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['addictions'] = lc
        if self.psychoactiveFluids is None or len(self.psychoactiveFluids) > 0:
            data['psychoactiveFluids'] = list()
        else:
            lc = list()
            if len(self.psychoactiveFluids) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['psychoactiveFluids'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'alcoholLevel' not in data:
            raise KeyRequiredException(self, data, 'alcoholLevel', 'alcoholLevel')
        self.alcoholLevel = float(data['alcoholLevel'])
        if (lv := self.addictions) is not None:
            for le in lv:
                self.addictions.add(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'addictions', 'addictions')
        if (lv := self.psychoactiveFluids) is not None:
            for le in lv:
                self.psychoactiveFluids.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'psychoactiveFluids', 'psychoactiveFluids')

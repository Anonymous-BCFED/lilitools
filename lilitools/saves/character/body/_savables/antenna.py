#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.character.body.bodypart import BodyPart
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
__all__ = ['RawAntenna']
## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public Element saveAsXML(Element parentElement, Document doc) { @ tXdWc3t8yE00YDC19flWbA+UmZMQJYMAJV3g5fn/WVE+BPjLWT/vzAiHRERWQ460QYrgA7tZQXtoP5fcY7cq5w==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawAntenna(BodyPart):
    TAG = 'antennae'
    _XMLID_ATTR_ANTENNAEPERROW_ATTRIBUTE: str = 'antennaePerRow'
    _XMLID_ATTR_LENGTH_ATTRIBUTE: str = 'length'
    _XMLID_ATTR_ROWS_ATTRIBUTE: str = 'rows'
    _XMLID_ATTR_TYPE_ATTRIBUTE: str = 'type'

    def __init__(self) -> None:
        super().__init__()
        self.type: str = ''
        self.rows: int = 0
        self.length: int = 0
        self.antennaePerRow: int = 0

    def overrideAttrs(self, antennaePerRow_attribute: _Optional_str = None, length_attribute: _Optional_str = None, rows_attribute: _Optional_str = None, type_attribute: _Optional_str = None) -> None:
        if antennaePerRow_attribute is not None:
            self._XMLID_ATTR_ANTENNAEPERROW_ATTRIBUTE = antennaePerRow_attribute
        if length_attribute is not None:
            self._XMLID_ATTR_LENGTH_ATTRIBUTE = length_attribute
        if rows_attribute is not None:
            self._XMLID_ATTR_ROWS_ATTRIBUTE = rows_attribute
        if type_attribute is not None:
            self._XMLID_ATTR_TYPE_ATTRIBUTE = type_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_TYPE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_TYPE_ATTRIBUTE, 'type')
        else:
            self.type = str(value)
        value = e.get(self._XMLID_ATTR_ROWS_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_ROWS_ATTRIBUTE, 'rows')
        else:
            self.rows = int(value)
        value = e.get(self._XMLID_ATTR_LENGTH_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_LENGTH_ATTRIBUTE, 'length')
        else:
            self.length = int(value)
        value = e.get(self._XMLID_ATTR_ANTENNAEPERROW_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_ANTENNAEPERROW_ATTRIBUTE, 'antennaePerRow')
        else:
            self.antennaePerRow = int(value)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_TYPE_ATTRIBUTE] = str(self.type)
        e.attrib[self._XMLID_ATTR_ROWS_ATTRIBUTE] = str(self.rows)
        e.attrib[self._XMLID_ATTR_LENGTH_ATTRIBUTE] = str(self.length)
        e.attrib[self._XMLID_ATTR_ANTENNAEPERROW_ATTRIBUTE] = str(self.antennaePerRow)
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['type'] = self.type
        data['rows'] = int(self.rows)
        data['length'] = int(self.length)
        data['antennaePerRow'] = int(self.antennaePerRow)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'type' not in data:
            raise KeyRequiredException(self, data, 'type', 'type')
        self.type = str(data['type'])
        if 'rows' not in data:
            raise KeyRequiredException(self, data, 'rows', 'rows')
        self.rows = int(data['rows'])
        if 'length' not in data:
            raise KeyRequiredException(self, data, 'length', 'length')
        self.length = int(data['length'])
        if 'antennaePerRow' not in data:
            raise KeyRequiredException(self, data, 'antennaePerRow', 'antennaePerRow')
        self.antennaePerRow = int(data['antennaePerRow'])

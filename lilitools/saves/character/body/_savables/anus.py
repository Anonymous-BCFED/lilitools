#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.character.body.bodypart import BodyPart
from lilitools.saves.character.enums.body_hair import EBodyHair
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL
__all__ = ['RawAnus']
## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public Element saveAsXML(Element parentElement, Document doc) { @ tXdWc3t8yE00YDC19flWbA+UmZMQJYMAJV3g5fn/WVE+BPjLWT/vzAiHRERWQ460QYrgA7tZQXtoP5fcY7cq5w==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawAnus(BodyPart):
    TAG = 'anus'
    _XMLID_ATTR_ASSHAIR_ATTRIBUTE: str = 'assHair'
    _XMLID_ATTR_BLEACHED_ATTRIBUTE: str = 'bleached'

    def __init__(self) -> None:
        super().__init__()
        self.assHair: EBodyHair = EBodyHair(0)
        self.bleached: bool = False

    def overrideAttrs(self, assHair_attribute: _Optional_str = None, bleached_attribute: _Optional_str = None) -> None:
        if assHair_attribute is not None:
            self._XMLID_ATTR_ASSHAIR_ATTRIBUTE = assHair_attribute
        if bleached_attribute is not None:
            self._XMLID_ATTR_BLEACHED_ATTRIBUTE = bleached_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_ASSHAIR_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_ASSHAIR_ATTRIBUTE, 'assHair')
        else:
            self.assHair = EBodyHair[value]
        value = e.get(self._XMLID_ATTR_BLEACHED_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_BLEACHED_ATTRIBUTE, 'bleached')
        else:
            self.bleached = XML2BOOL[value]

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_ASSHAIR_ATTRIBUTE] = self.assHair.name
        e.attrib[self._XMLID_ATTR_BLEACHED_ATTRIBUTE] = str(self.bleached).lower()
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['assHair'] = self.assHair.name
        data['bleached'] = bool(self.bleached)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'assHair' not in data:
            raise KeyRequiredException(self, data, 'assHair', 'assHair')
        self.assHair = EBodyHair[data['assHair']]
        if 'bleached' not in data:
            raise KeyRequiredException(self, data, 'bleached', 'bleached')
        self.bleached = bool(data['bleached'])

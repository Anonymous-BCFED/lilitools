#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.character.body.bodypart import BodyPart
from lilitools.saves.character.enums.body_hair import EBodyHair
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
__all__ = ['RawArms']
## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public Element saveAsXML(Element parentElement, Document doc) { @ tXdWc3t8yE00YDC19flWbA+UmZMQJYMAJV3g5fn/WVE+BPjLWT/vzAiHRERWQ460QYrgA7tZQXtoP5fcY7cq5w==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawArms(BodyPart):
    TAG = 'arm'
    _XMLID_ATTR_ROWS_ATTRIBUTE: str = 'rows'
    _XMLID_ATTR_TYPE_ATTRIBUTE: str = 'type'
    _XMLID_ATTR_UNDERARMHAIR_ATTRIBUTE: str = 'underarmHair'

    def __init__(self) -> None:
        super().__init__()
        self.type: str = ''
        self.rows: int = 0
        self.underarmHair: EBodyHair = EBodyHair(0)

    def overrideAttrs(self, rows_attribute: _Optional_str = None, type_attribute: _Optional_str = None, underarmHair_attribute: _Optional_str = None) -> None:
        if rows_attribute is not None:
            self._XMLID_ATTR_ROWS_ATTRIBUTE = rows_attribute
        if type_attribute is not None:
            self._XMLID_ATTR_TYPE_ATTRIBUTE = type_attribute
        if underarmHair_attribute is not None:
            self._XMLID_ATTR_UNDERARMHAIR_ATTRIBUTE = underarmHair_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_TYPE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_TYPE_ATTRIBUTE, 'type')
        else:
            self.type = str(value)
        value = e.get(self._XMLID_ATTR_ROWS_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_ROWS_ATTRIBUTE, 'rows')
        else:
            self.rows = int(value)
        value = e.get(self._XMLID_ATTR_UNDERARMHAIR_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_UNDERARMHAIR_ATTRIBUTE, 'underarmHair')
        else:
            self.underarmHair = EBodyHair[value]

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_TYPE_ATTRIBUTE] = str(self.type)
        e.attrib[self._XMLID_ATTR_ROWS_ATTRIBUTE] = str(self.rows)
        e.attrib[self._XMLID_ATTR_UNDERARMHAIR_ATTRIBUTE] = self.underarmHair.name
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['type'] = self.type
        data['rows'] = int(self.rows)
        data['underarmHair'] = self.underarmHair.name
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'type' not in data:
            raise KeyRequiredException(self, data, 'type', 'type')
        self.type = str(data['type'])
        if 'rows' not in data:
            raise KeyRequiredException(self, data, 'rows', 'rows')
        self.rows = int(data['rows'])
        if 'underarmHair' not in data:
            raise KeyRequiredException(self, data, 'underarmHair', 'underarmHair')
        self.underarmHair = EBodyHair[data['underarmHair']]

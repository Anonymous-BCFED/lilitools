#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.character.body.bodypart import BodyPart
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
__all__ = ['RawAss']
## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public Element saveAsXML(Element parentElement, Document doc) { @ tXdWc3t8yE00YDC19flWbA+UmZMQJYMAJV3g5fn/WVE+BPjLWT/vzAiHRERWQ460QYrgA7tZQXtoP5fcY7cq5w==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawAss(BodyPart):
    TAG = 'ass'
    _XMLID_ATTR_ASSSIZE_ATTRIBUTE: str = 'assSize'
    _XMLID_ATTR_HIPSIZE_ATTRIBUTE: str = 'hipSize'
    _XMLID_ATTR_TYPE_ATTRIBUTE: str = 'type'

    def __init__(self) -> None:
        super().__init__()
        self.type: str = ''
        self.assSize: int = 0
        self.hipSize: int = 0

    def overrideAttrs(self, assSize_attribute: _Optional_str = None, hipSize_attribute: _Optional_str = None, type_attribute: _Optional_str = None) -> None:
        if assSize_attribute is not None:
            self._XMLID_ATTR_ASSSIZE_ATTRIBUTE = assSize_attribute
        if hipSize_attribute is not None:
            self._XMLID_ATTR_HIPSIZE_ATTRIBUTE = hipSize_attribute
        if type_attribute is not None:
            self._XMLID_ATTR_TYPE_ATTRIBUTE = type_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_TYPE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_TYPE_ATTRIBUTE, 'type')
        else:
            self.type = str(value)
        value = e.get(self._XMLID_ATTR_ASSSIZE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_ASSSIZE_ATTRIBUTE, 'assSize')
        else:
            self.assSize = int(value)
        value = e.get(self._XMLID_ATTR_HIPSIZE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_HIPSIZE_ATTRIBUTE, 'hipSize')
        else:
            self.hipSize = int(value)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_TYPE_ATTRIBUTE] = str(self.type)
        e.attrib[self._XMLID_ATTR_ASSSIZE_ATTRIBUTE] = str(self.assSize)
        e.attrib[self._XMLID_ATTR_HIPSIZE_ATTRIBUTE] = str(self.hipSize)
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['type'] = self.type
        data['assSize'] = int(self.assSize)
        data['hipSize'] = int(self.hipSize)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'type' not in data:
            raise KeyRequiredException(self, data, 'type', 'type')
        self.type = str(data['type'])
        if 'assSize' not in data:
            raise KeyRequiredException(self, data, 'assSize', 'assSize')
        self.assSize = int(data['assSize'])
        if 'hipSize' not in data:
            raise KeyRequiredException(self, data, 'hipSize', 'hipSize')
        self.hipSize = int(data['hipSize'])

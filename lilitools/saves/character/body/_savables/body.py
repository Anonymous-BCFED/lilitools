#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.character.body.antenna import Antenna
from lilitools.saves.character.body.anus import Anus
from lilitools.saves.character.body.arms import Arms
from lilitools.saves.character.body.ass import Ass
from lilitools.saves.character.body.bodycore import BodyCore
from lilitools.saves.character.body.breasts import Breasts
from lilitools.saves.character.body.ears import Ears
from lilitools.saves.character.body.eyes import Eyes
from lilitools.saves.character.body.face import Face
from lilitools.saves.character.body.hair import Hair
from lilitools.saves.character.body.horns import Horns
from lilitools.saves.character.body.legs import Legs
from lilitools.saves.character.body.mouth import Mouth
from lilitools.saves.character.body.nipples import Nipples
from lilitools.saves.character.body.penis import Penis
from lilitools.saves.character.body.spinneret import Spinneret
from lilitools.saves.character.body.tail import Tail
from lilitools.saves.character.body.tentacles import Tentacles
from lilitools.saves.character.body.testicles import Testicles
from lilitools.saves.character.body.tongue import Tongue
from lilitools.saves.character.body.torso import Torso
from lilitools.saves.character.body.vagina import Vagina
from lilitools.saves.character.body.wings import Wings
from lilitools.saves.character.fluids.cum import Cum
from lilitools.saves.character.fluids.girlcum import GirlCum
from lilitools.saves.character.fluids.milk import Milk
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawBody']
## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public Element saveAsXML(Element parentElement, Document doc) { @ tXdWc3t8yE00YDC19flWbA+UmZMQJYMAJV3g5fn/WVE+BPjLWT/vzAiHRERWQ460QYrgA7tZQXtoP5fcY7cq5w==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawBody(Savable):
    TAG = 'body'
    _XMLID_ATTR_ANTENNAE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_ANUS_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_ARMS_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_ASS_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_BREASTS_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_CORE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_CROTCHBREASTS_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_CROTCHMILK_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_CROTCHNIPPLES_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_CUM_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_EARS_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_EYES_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_FACE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_GIRLCUM_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_HAIR_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_HORNS_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_LEGS_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_MILK_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_MOUTH_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_NIPPLES_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_PENIS_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_SPINNERET_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_TAIL_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_TENTACLES_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_TESTICLES_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_TONGUE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_TORSO_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_VAGINA_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_WINGS_ATTRIBUTE: str = 'value'
    _XMLID_TAG_ANTENNAE_ELEMENT: str = 'antennae'
    _XMLID_TAG_ANUS_ELEMENT: str = 'anus'
    _XMLID_TAG_ARMS_ELEMENT: str = 'arm'
    _XMLID_TAG_ASS_ELEMENT: str = 'ass'
    _XMLID_TAG_BREASTS_ELEMENT: str = 'breasts'
    _XMLID_TAG_CORE_ELEMENT: str = 'bodyCore'
    _XMLID_TAG_CROTCHBREASTS_ELEMENT: str = 'breastsCrotch'
    _XMLID_TAG_CROTCHMILK_ELEMENT: str = 'milkCrotch'
    _XMLID_TAG_CROTCHNIPPLES_ELEMENT: str = 'nipplesCrotch'
    _XMLID_TAG_CUM_ELEMENT: str = 'cum'
    _XMLID_TAG_EARS_ELEMENT: str = 'ear'
    _XMLID_TAG_EYES_ELEMENT: str = 'eye'
    _XMLID_TAG_FACE_ELEMENT: str = 'face'
    _XMLID_TAG_GIRLCUM_ELEMENT: str = 'girlcum'
    _XMLID_TAG_HAIR_ELEMENT: str = 'hair'
    _XMLID_TAG_HORNS_ELEMENT: str = 'horn'
    _XMLID_TAG_LEGS_ELEMENT: str = 'leg'
    _XMLID_TAG_MILK_ELEMENT: str = 'milk'
    _XMLID_TAG_MOUTH_ELEMENT: str = 'mouth'
    _XMLID_TAG_NIPPLES_ELEMENT: str = 'nipples'
    _XMLID_TAG_PENIS_ELEMENT: str = 'penis'
    _XMLID_TAG_SPINNERET_ELEMENT: str = 'spinneret'
    _XMLID_TAG_TAIL_ELEMENT: str = 'tail'
    _XMLID_TAG_TENTACLES_ELEMENT: str = 'tentacle'
    _XMLID_TAG_TESTICLES_ELEMENT: str = 'testicles'
    _XMLID_TAG_TONGUE_ELEMENT: str = 'tongue'
    _XMLID_TAG_TORSO_ELEMENT: str = 'torso'
    _XMLID_TAG_VAGINA_ELEMENT: str = 'vagina'
    _XMLID_TAG_WINGS_ELEMENT: str = 'wing'

    def __init__(self) -> None:
        super().__init__()
        self.core: BodyCore = BodyCore()  # Element
        self.antennae: Antenna = Antenna()  # Element
        self.arms: Arms = Arms()  # Element
        self.ass: Ass = Ass()  # Element
        self.anus: Anus = Anus()  # Element
        self.breasts: Breasts = Breasts()  # Element
        self.nipples: Nipples = Nipples()  # Element
        self.milk: Milk = Milk()  # Element
        self.crotchBreasts: Breasts = Breasts()  # Element
        self.crotchNipples: Nipples = Nipples()  # Element
        self.crotchMilk: Milk = Milk()  # Element
        self.ears: Ears = Ears()  # Element
        self.eyes: Eyes = Eyes()  # Element
        self.face: Face = Face()  # Element
        self.mouth: Mouth = Mouth()  # Element
        self.tongue: Tongue = Tongue()  # Element
        self.hair: Hair = Hair()  # Element
        self.horns: Horns = Horns()  # Element
        self.legs: Legs = Legs()  # Element
        self.penis: Penis = Penis()  # Element
        self.testicles: Testicles = Testicles()  # Element
        self.cum: Cum = Cum()  # Element
        self.spinneret: Spinneret = Spinneret()  # Element
        self.torso: Torso = Torso()  # Element
        self.tail: Tail = Tail()  # Element
        self.tentacles: Tentacles = Tentacles()  # Element
        self.vagina: Vagina = Vagina()  # Element
        self.girlcum: GirlCum = GirlCum()  # Element
        self.wings: Wings = Wings()  # Element

    def overrideAttrs(self, antennae_attribute: _Optional_str = None, anus_attribute: _Optional_str = None, arms_attribute: _Optional_str = None, ass_attribute: _Optional_str = None, breasts_attribute: _Optional_str = None, core_attribute: _Optional_str = None, crotchBreasts_attribute: _Optional_str = None, crotchMilk_attribute: _Optional_str = None, crotchNipples_attribute: _Optional_str = None, cum_attribute: _Optional_str = None, ears_attribute: _Optional_str = None, eyes_attribute: _Optional_str = None, face_attribute: _Optional_str = None, girlcum_attribute: _Optional_str = None, hair_attribute: _Optional_str = None, horns_attribute: _Optional_str = None, legs_attribute: _Optional_str = None, milk_attribute: _Optional_str = None, mouth_attribute: _Optional_str = None, nipples_attribute: _Optional_str = None, penis_attribute: _Optional_str = None, spinneret_attribute: _Optional_str = None, tail_attribute: _Optional_str = None, tentacles_attribute: _Optional_str = None, testicles_attribute: _Optional_str = None, tongue_attribute: _Optional_str = None, torso_attribute: _Optional_str = None, vagina_attribute: _Optional_str = None, wings_attribute: _Optional_str = None) -> None:
        if antennae_attribute is not None:
            self._XMLID_ATTR_ANTENNAE_ATTRIBUTE = antennae_attribute
        if anus_attribute is not None:
            self._XMLID_ATTR_ANUS_ATTRIBUTE = anus_attribute
        if arms_attribute is not None:
            self._XMLID_ATTR_ARMS_ATTRIBUTE = arms_attribute
        if ass_attribute is not None:
            self._XMLID_ATTR_ASS_ATTRIBUTE = ass_attribute
        if breasts_attribute is not None:
            self._XMLID_ATTR_BREASTS_ATTRIBUTE = breasts_attribute
        if core_attribute is not None:
            self._XMLID_ATTR_CORE_ATTRIBUTE = core_attribute
        if crotchBreasts_attribute is not None:
            self._XMLID_ATTR_CROTCHBREASTS_ATTRIBUTE = crotchBreasts_attribute
        if crotchMilk_attribute is not None:
            self._XMLID_ATTR_CROTCHMILK_ATTRIBUTE = crotchMilk_attribute
        if crotchNipples_attribute is not None:
            self._XMLID_ATTR_CROTCHNIPPLES_ATTRIBUTE = crotchNipples_attribute
        if cum_attribute is not None:
            self._XMLID_ATTR_CUM_ATTRIBUTE = cum_attribute
        if ears_attribute is not None:
            self._XMLID_ATTR_EARS_ATTRIBUTE = ears_attribute
        if eyes_attribute is not None:
            self._XMLID_ATTR_EYES_ATTRIBUTE = eyes_attribute
        if face_attribute is not None:
            self._XMLID_ATTR_FACE_ATTRIBUTE = face_attribute
        if girlcum_attribute is not None:
            self._XMLID_ATTR_GIRLCUM_ATTRIBUTE = girlcum_attribute
        if hair_attribute is not None:
            self._XMLID_ATTR_HAIR_ATTRIBUTE = hair_attribute
        if horns_attribute is not None:
            self._XMLID_ATTR_HORNS_ATTRIBUTE = horns_attribute
        if legs_attribute is not None:
            self._XMLID_ATTR_LEGS_ATTRIBUTE = legs_attribute
        if milk_attribute is not None:
            self._XMLID_ATTR_MILK_ATTRIBUTE = milk_attribute
        if mouth_attribute is not None:
            self._XMLID_ATTR_MOUTH_ATTRIBUTE = mouth_attribute
        if nipples_attribute is not None:
            self._XMLID_ATTR_NIPPLES_ATTRIBUTE = nipples_attribute
        if penis_attribute is not None:
            self._XMLID_ATTR_PENIS_ATTRIBUTE = penis_attribute
        if spinneret_attribute is not None:
            self._XMLID_ATTR_SPINNERET_ATTRIBUTE = spinneret_attribute
        if tail_attribute is not None:
            self._XMLID_ATTR_TAIL_ATTRIBUTE = tail_attribute
        if tentacles_attribute is not None:
            self._XMLID_ATTR_TENTACLES_ATTRIBUTE = tentacles_attribute
        if testicles_attribute is not None:
            self._XMLID_ATTR_TESTICLES_ATTRIBUTE = testicles_attribute
        if tongue_attribute is not None:
            self._XMLID_ATTR_TONGUE_ATTRIBUTE = tongue_attribute
        if torso_attribute is not None:
            self._XMLID_ATTR_TORSO_ATTRIBUTE = torso_attribute
        if vagina_attribute is not None:
            self._XMLID_ATTR_VAGINA_ATTRIBUTE = vagina_attribute
        if wings_attribute is not None:
            self._XMLID_ATTR_WINGS_ATTRIBUTE = wings_attribute

    def overrideTags(self, antennae_element: _Optional_str = None, anus_element: _Optional_str = None, arms_element: _Optional_str = None, ass_element: _Optional_str = None, breasts_element: _Optional_str = None, core_element: _Optional_str = None, crotchBreasts_element: _Optional_str = None, crotchMilk_element: _Optional_str = None, crotchNipples_element: _Optional_str = None, cum_element: _Optional_str = None, ears_element: _Optional_str = None, eyes_element: _Optional_str = None, face_element: _Optional_str = None, girlcum_element: _Optional_str = None, hair_element: _Optional_str = None, horns_element: _Optional_str = None, legs_element: _Optional_str = None, milk_element: _Optional_str = None, mouth_element: _Optional_str = None, nipples_element: _Optional_str = None, penis_element: _Optional_str = None, spinneret_element: _Optional_str = None, tail_element: _Optional_str = None, tentacles_element: _Optional_str = None, testicles_element: _Optional_str = None, tongue_element: _Optional_str = None, torso_element: _Optional_str = None, vagina_element: _Optional_str = None, wings_element: _Optional_str = None) -> None:
        if antennae_element is not None:
            self._XMLID_TAG_ANTENNAE_ELEMENT = antennae_element
        if anus_element is not None:
            self._XMLID_TAG_ANUS_ELEMENT = anus_element
        if arms_element is not None:
            self._XMLID_TAG_ARMS_ELEMENT = arms_element
        if ass_element is not None:
            self._XMLID_TAG_ASS_ELEMENT = ass_element
        if breasts_element is not None:
            self._XMLID_TAG_BREASTS_ELEMENT = breasts_element
        if core_element is not None:
            self._XMLID_TAG_CORE_ELEMENT = core_element
        if crotchBreasts_element is not None:
            self._XMLID_TAG_CROTCHBREASTS_ELEMENT = crotchBreasts_element
        if crotchMilk_element is not None:
            self._XMLID_TAG_CROTCHMILK_ELEMENT = crotchMilk_element
        if crotchNipples_element is not None:
            self._XMLID_TAG_CROTCHNIPPLES_ELEMENT = crotchNipples_element
        if cum_element is not None:
            self._XMLID_TAG_CUM_ELEMENT = cum_element
        if ears_element is not None:
            self._XMLID_TAG_EARS_ELEMENT = ears_element
        if eyes_element is not None:
            self._XMLID_TAG_EYES_ELEMENT = eyes_element
        if face_element is not None:
            self._XMLID_TAG_FACE_ELEMENT = face_element
        if girlcum_element is not None:
            self._XMLID_TAG_GIRLCUM_ELEMENT = girlcum_element
        if hair_element is not None:
            self._XMLID_TAG_HAIR_ELEMENT = hair_element
        if horns_element is not None:
            self._XMLID_TAG_HORNS_ELEMENT = horns_element
        if legs_element is not None:
            self._XMLID_TAG_LEGS_ELEMENT = legs_element
        if milk_element is not None:
            self._XMLID_TAG_MILK_ELEMENT = milk_element
        if mouth_element is not None:
            self._XMLID_TAG_MOUTH_ELEMENT = mouth_element
        if nipples_element is not None:
            self._XMLID_TAG_NIPPLES_ELEMENT = nipples_element
        if penis_element is not None:
            self._XMLID_TAG_PENIS_ELEMENT = penis_element
        if spinneret_element is not None:
            self._XMLID_TAG_SPINNERET_ELEMENT = spinneret_element
        if tail_element is not None:
            self._XMLID_TAG_TAIL_ELEMENT = tail_element
        if tentacles_element is not None:
            self._XMLID_TAG_TENTACLES_ELEMENT = tentacles_element
        if testicles_element is not None:
            self._XMLID_TAG_TESTICLES_ELEMENT = testicles_element
        if tongue_element is not None:
            self._XMLID_TAG_TONGUE_ELEMENT = tongue_element
        if torso_element is not None:
            self._XMLID_TAG_TORSO_ELEMENT = torso_element
        if vagina_element is not None:
            self._XMLID_TAG_VAGINA_ELEMENT = vagina_element
        if wings_element is not None:
            self._XMLID_TAG_WINGS_ELEMENT = wings_element

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_CORE_ELEMENT)) is not None:
            self.core = BodyCore()
            self.core.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'core', self._XMLID_TAG_CORE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_ANTENNAE_ELEMENT)) is not None:
            self.antennae = Antenna()
            self.antennae.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'antennae', self._XMLID_TAG_ANTENNAE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_ARMS_ELEMENT)) is not None:
            self.arms = Arms()
            self.arms.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'arms', self._XMLID_TAG_ARMS_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_ASS_ELEMENT)) is not None:
            self.ass = Ass()
            self.ass.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'ass', self._XMLID_TAG_ASS_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_ANUS_ELEMENT)) is not None:
            self.anus = Anus()
            self.anus.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'anus', self._XMLID_TAG_ANUS_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_BREASTS_ELEMENT)) is not None:
            self.breasts = Breasts()
            self.breasts.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'breasts', self._XMLID_TAG_BREASTS_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_NIPPLES_ELEMENT)) is not None:
            self.nipples = Nipples()
            self.nipples.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'nipples', self._XMLID_TAG_NIPPLES_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_MILK_ELEMENT)) is not None:
            self.milk = Milk()
            self.milk.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'milk', self._XMLID_TAG_MILK_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_CROTCHBREASTS_ELEMENT)) is not None:
            self.crotchBreasts = Breasts()
            self.crotchBreasts.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'crotchBreasts', self._XMLID_TAG_CROTCHBREASTS_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_CROTCHNIPPLES_ELEMENT)) is not None:
            self.crotchNipples = Nipples()
            self.crotchNipples.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'crotchNipples', self._XMLID_TAG_CROTCHNIPPLES_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_CROTCHMILK_ELEMENT)) is not None:
            self.crotchMilk = Milk()
            self.crotchMilk.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'crotchMilk', self._XMLID_TAG_CROTCHMILK_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_EARS_ELEMENT)) is not None:
            self.ears = Ears()
            self.ears.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'ears', self._XMLID_TAG_EARS_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_EYES_ELEMENT)) is not None:
            self.eyes = Eyes()
            self.eyes.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'eyes', self._XMLID_TAG_EYES_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_FACE_ELEMENT)) is not None:
            self.face = Face()
            self.face.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'face', self._XMLID_TAG_FACE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_MOUTH_ELEMENT)) is not None:
            self.mouth = Mouth()
            self.mouth.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'mouth', self._XMLID_TAG_MOUTH_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_TONGUE_ELEMENT)) is not None:
            self.tongue = Tongue()
            self.tongue.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'tongue', self._XMLID_TAG_TONGUE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_HAIR_ELEMENT)) is not None:
            self.hair = Hair()
            self.hair.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'hair', self._XMLID_TAG_HAIR_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_HORNS_ELEMENT)) is not None:
            self.horns = Horns()
            self.horns.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'horns', self._XMLID_TAG_HORNS_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_LEGS_ELEMENT)) is not None:
            self.legs = Legs()
            self.legs.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'legs', self._XMLID_TAG_LEGS_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_PENIS_ELEMENT)) is not None:
            self.penis = Penis()
            self.penis.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'penis', self._XMLID_TAG_PENIS_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_TESTICLES_ELEMENT)) is not None:
            self.testicles = Testicles()
            self.testicles.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'testicles', self._XMLID_TAG_TESTICLES_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_CUM_ELEMENT)) is not None:
            self.cum = Cum()
            self.cum.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'cum', self._XMLID_TAG_CUM_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_SPINNERET_ELEMENT)) is not None:
            self.spinneret = Spinneret()
            self.spinneret.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'spinneret', self._XMLID_TAG_SPINNERET_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_TORSO_ELEMENT)) is not None:
            self.torso = Torso()
            self.torso.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'torso', self._XMLID_TAG_TORSO_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_TAIL_ELEMENT)) is not None:
            self.tail = Tail()
            self.tail.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'tail', self._XMLID_TAG_TAIL_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_TENTACLES_ELEMENT)) is not None:
            self.tentacles = Tentacles()
            self.tentacles.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'tentacles', self._XMLID_TAG_TENTACLES_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_VAGINA_ELEMENT)) is not None:
            self.vagina = Vagina()
            self.vagina.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'vagina', self._XMLID_TAG_VAGINA_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_GIRLCUM_ELEMENT)) is not None:
            self.girlcum = GirlCum()
            self.girlcum.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'girlcum', self._XMLID_TAG_GIRLCUM_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_WINGS_ELEMENT)) is not None:
            self.wings = Wings()
            self.wings.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'wings', self._XMLID_TAG_WINGS_ELEMENT)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.append(self.core.toXML(self._XMLID_TAG_CORE_ELEMENT))
        e.append(self.antennae.toXML(self._XMLID_TAG_ANTENNAE_ELEMENT))
        e.append(self.arms.toXML(self._XMLID_TAG_ARMS_ELEMENT))
        e.append(self.ass.toXML(self._XMLID_TAG_ASS_ELEMENT))
        e.append(self.anus.toXML(self._XMLID_TAG_ANUS_ELEMENT))
        e.append(self.breasts.toXML(self._XMLID_TAG_BREASTS_ELEMENT))
        e.append(self.nipples.toXML(self._XMLID_TAG_NIPPLES_ELEMENT))
        e.append(self.milk.toXML(self._XMLID_TAG_MILK_ELEMENT))
        e.append(self.crotchBreasts.toXML(self._XMLID_TAG_CROTCHBREASTS_ELEMENT))
        e.append(self.crotchNipples.toXML(self._XMLID_TAG_CROTCHNIPPLES_ELEMENT))
        e.append(self.crotchMilk.toXML(self._XMLID_TAG_CROTCHMILK_ELEMENT))
        e.append(self.ears.toXML(self._XMLID_TAG_EARS_ELEMENT))
        e.append(self.eyes.toXML(self._XMLID_TAG_EYES_ELEMENT))
        e.append(self.face.toXML(self._XMLID_TAG_FACE_ELEMENT))
        e.append(self.mouth.toXML(self._XMLID_TAG_MOUTH_ELEMENT))
        e.append(self.tongue.toXML(self._XMLID_TAG_TONGUE_ELEMENT))
        e.append(self.hair.toXML(self._XMLID_TAG_HAIR_ELEMENT))
        e.append(self.horns.toXML(self._XMLID_TAG_HORNS_ELEMENT))
        e.append(self.legs.toXML(self._XMLID_TAG_LEGS_ELEMENT))
        e.append(self.penis.toXML(self._XMLID_TAG_PENIS_ELEMENT))
        e.append(self.testicles.toXML(self._XMLID_TAG_TESTICLES_ELEMENT))
        e.append(self.cum.toXML(self._XMLID_TAG_CUM_ELEMENT))
        e.append(self.spinneret.toXML(self._XMLID_TAG_SPINNERET_ELEMENT))
        e.append(self.torso.toXML(self._XMLID_TAG_TORSO_ELEMENT))
        e.append(self.tail.toXML(self._XMLID_TAG_TAIL_ELEMENT))
        e.append(self.tentacles.toXML(self._XMLID_TAG_TENTACLES_ELEMENT))
        e.append(self.vagina.toXML(self._XMLID_TAG_VAGINA_ELEMENT))
        e.append(self.girlcum.toXML(self._XMLID_TAG_GIRLCUM_ELEMENT))
        e.append(self.wings.toXML(self._XMLID_TAG_WINGS_ELEMENT))
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['core'] = self.core.toDict()
        data['antennae'] = self.antennae.toDict()
        data['arms'] = self.arms.toDict()
        data['ass'] = self.ass.toDict()
        data['anus'] = self.anus.toDict()
        data['breasts'] = self.breasts.toDict()
        data['nipples'] = self.nipples.toDict()
        data['milk'] = self.milk.toDict()
        data['crotchBreasts'] = self.crotchBreasts.toDict()
        data['crotchNipples'] = self.crotchNipples.toDict()
        data['crotchMilk'] = self.crotchMilk.toDict()
        data['ears'] = self.ears.toDict()
        data['eyes'] = self.eyes.toDict()
        data['face'] = self.face.toDict()
        data['mouth'] = self.mouth.toDict()
        data['tongue'] = self.tongue.toDict()
        data['hair'] = self.hair.toDict()
        data['horns'] = self.horns.toDict()
        data['legs'] = self.legs.toDict()
        data['penis'] = self.penis.toDict()
        data['testicles'] = self.testicles.toDict()
        data['cum'] = self.cum.toDict()
        data['spinneret'] = self.spinneret.toDict()
        data['torso'] = self.torso.toDict()
        data['tail'] = self.tail.toDict()
        data['tentacles'] = self.tentacles.toDict()
        data['vagina'] = self.vagina.toDict()
        data['girlcum'] = self.girlcum.toDict()
        data['wings'] = self.wings.toDict()
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('core')) is not None:
            self.core = BodyCore()
            self.core.fromDict(data['core'])
        else:
            raise KeyRequiredException(self, data, 'core', 'core')
        if (sf := data.get('antennae')) is not None:
            self.antennae = Antenna()
            self.antennae.fromDict(data['antennae'])
        else:
            raise KeyRequiredException(self, data, 'antennae', 'antennae')
        if (sf := data.get('arms')) is not None:
            self.arms = Arms()
            self.arms.fromDict(data['arms'])
        else:
            raise KeyRequiredException(self, data, 'arms', 'arms')
        if (sf := data.get('ass')) is not None:
            self.ass = Ass()
            self.ass.fromDict(data['ass'])
        else:
            raise KeyRequiredException(self, data, 'ass', 'ass')
        if (sf := data.get('anus')) is not None:
            self.anus = Anus()
            self.anus.fromDict(data['anus'])
        else:
            raise KeyRequiredException(self, data, 'anus', 'anus')
        if (sf := data.get('breasts')) is not None:
            self.breasts = Breasts()
            self.breasts.fromDict(data['breasts'])
        else:
            raise KeyRequiredException(self, data, 'breasts', 'breasts')
        if (sf := data.get('nipples')) is not None:
            self.nipples = Nipples()
            self.nipples.fromDict(data['nipples'])
        else:
            raise KeyRequiredException(self, data, 'nipples', 'nipples')
        if (sf := data.get('milk')) is not None:
            self.milk = Milk()
            self.milk.fromDict(data['milk'])
        else:
            raise KeyRequiredException(self, data, 'milk', 'milk')
        if (sf := data.get('crotchBreasts')) is not None:
            self.crotchBreasts = Breasts()
            self.crotchBreasts.fromDict(data['crotchBreasts'])
        else:
            raise KeyRequiredException(self, data, 'crotchBreasts', 'crotchBreasts')
        if (sf := data.get('crotchNipples')) is not None:
            self.crotchNipples = Nipples()
            self.crotchNipples.fromDict(data['crotchNipples'])
        else:
            raise KeyRequiredException(self, data, 'crotchNipples', 'crotchNipples')
        if (sf := data.get('crotchMilk')) is not None:
            self.crotchMilk = Milk()
            self.crotchMilk.fromDict(data['crotchMilk'])
        else:
            raise KeyRequiredException(self, data, 'crotchMilk', 'crotchMilk')
        if (sf := data.get('ears')) is not None:
            self.ears = Ears()
            self.ears.fromDict(data['ears'])
        else:
            raise KeyRequiredException(self, data, 'ears', 'ears')
        if (sf := data.get('eyes')) is not None:
            self.eyes = Eyes()
            self.eyes.fromDict(data['eyes'])
        else:
            raise KeyRequiredException(self, data, 'eyes', 'eyes')
        if (sf := data.get('face')) is not None:
            self.face = Face()
            self.face.fromDict(data['face'])
        else:
            raise KeyRequiredException(self, data, 'face', 'face')
        if (sf := data.get('mouth')) is not None:
            self.mouth = Mouth()
            self.mouth.fromDict(data['mouth'])
        else:
            raise KeyRequiredException(self, data, 'mouth', 'mouth')
        if (sf := data.get('tongue')) is not None:
            self.tongue = Tongue()
            self.tongue.fromDict(data['tongue'])
        else:
            raise KeyRequiredException(self, data, 'tongue', 'tongue')
        if (sf := data.get('hair')) is not None:
            self.hair = Hair()
            self.hair.fromDict(data['hair'])
        else:
            raise KeyRequiredException(self, data, 'hair', 'hair')
        if (sf := data.get('horns')) is not None:
            self.horns = Horns()
            self.horns.fromDict(data['horns'])
        else:
            raise KeyRequiredException(self, data, 'horns', 'horns')
        if (sf := data.get('legs')) is not None:
            self.legs = Legs()
            self.legs.fromDict(data['legs'])
        else:
            raise KeyRequiredException(self, data, 'legs', 'legs')
        if (sf := data.get('penis')) is not None:
            self.penis = Penis()
            self.penis.fromDict(data['penis'])
        else:
            raise KeyRequiredException(self, data, 'penis', 'penis')
        if (sf := data.get('testicles')) is not None:
            self.testicles = Testicles()
            self.testicles.fromDict(data['testicles'])
        else:
            raise KeyRequiredException(self, data, 'testicles', 'testicles')
        if (sf := data.get('cum')) is not None:
            self.cum = Cum()
            self.cum.fromDict(data['cum'])
        else:
            raise KeyRequiredException(self, data, 'cum', 'cum')
        if (sf := data.get('spinneret')) is not None:
            self.spinneret = Spinneret()
            self.spinneret.fromDict(data['spinneret'])
        else:
            raise KeyRequiredException(self, data, 'spinneret', 'spinneret')
        if (sf := data.get('torso')) is not None:
            self.torso = Torso()
            self.torso.fromDict(data['torso'])
        else:
            raise KeyRequiredException(self, data, 'torso', 'torso')
        if (sf := data.get('tail')) is not None:
            self.tail = Tail()
            self.tail.fromDict(data['tail'])
        else:
            raise KeyRequiredException(self, data, 'tail', 'tail')
        if (sf := data.get('tentacles')) is not None:
            self.tentacles = Tentacles()
            self.tentacles.fromDict(data['tentacles'])
        else:
            raise KeyRequiredException(self, data, 'tentacles', 'tentacles')
        if (sf := data.get('vagina')) is not None:
            self.vagina = Vagina()
            self.vagina.fromDict(data['vagina'])
        else:
            raise KeyRequiredException(self, data, 'vagina', 'vagina')
        if (sf := data.get('girlcum')) is not None:
            self.girlcum = GirlCum()
            self.girlcum.fromDict(data['girlcum'])
        else:
            raise KeyRequiredException(self, data, 'girlcum', 'girlcum')
        if (sf := data.get('wings')) is not None:
            self.wings = Wings()
            self.wings.fromDict(data['wings'])
        else:
            raise KeyRequiredException(self, data, 'wings', 'wings')

#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, List, Optional, Set
from lxml import etree
from lilitools.saves.character.body.body_covering import BodyCovering
from lilitools.saves.character.enums.body_hair import EBodyHair
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL, Savable
__all__ = ['RawBodyCore']
## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public Element saveAsXML(Element parentElement, Document doc) { @ tXdWc3t8yE00YDC19flWbA+UmZMQJYMAJV3g5fn/WVE+BPjLWT/vzAiHRERWQ460QYrgA7tZQXtoP5fcY7cq5w==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawBodyCore(Savable):
    TAG = 'bodyCore'
    _XMLID_ATTR_BODYMATERIAL_ATTRIBUTE: str = 'bodyMaterial'
    _XMLID_ATTR_BODYSIZE_ATTRIBUTE: str = 'bodySize'
    _XMLID_ATTR_FEMININITY_ATTRIBUTE: str = 'femininity'
    _XMLID_ATTR_FERAL_ATTRIBUTE: str = 'feral'
    _XMLID_ATTR_GENITALARRANGEMENT_ATTRIBUTE: str = 'genitalArrangement'
    _XMLID_ATTR_HEIGHT_ATTRIBUTE: str = 'height'
    _XMLID_ATTR_MUSCLE_ATTRIBUTE: str = 'muscle'
    _XMLID_ATTR_PIERCEDSTOMACH_ATTRIBUTE: str = 'piercedStomach'
    _XMLID_ATTR_PUBICHAIR_ATTRIBUTE: str = 'pubicHair'
    _XMLID_ATTR_SUBSPECIESOVERRIDE_ATTRIBUTE: str = 'subspeciesOverride'
    _XMLID_ATTR_SUBSPECIES_ATTRIBUTE: str = 'subspecies'
    _XMLID_ATTR_TAKESAFTERMOTHER_ATTRIBUTE: str = 'takesAfterMother'
    _XMLID_ATTR_VERSION_ATTRIBUTE: str = 'version'
    _XMLID_TAG_BODYCOVERINGS_CHILD: str = 'bodyCovering'
    _XMLID_TAG_BODYCOVERINGS_VALUEELEM: Optional[str] = None
    _XMLID_TAG_HEAVYMAKEUP_CHILD: str = 'type'
    _XMLID_TAG_HEAVYMAKEUP_PARENT: str = 'heavyMakeup'

    def __init__(self) -> None:
        super().__init__()
        self.version: str = ''
        self.piercedStomach: bool = False
        self.height: int = 0
        self.femininity: int = 0
        self.bodySize: int = 0
        self.muscle: int = 0
        self.pubicHair: EBodyHair = EBodyHair(0)
        self.bodyMaterial: str = ''
        self.genitalArrangement: str = ''
        self.feral: bool = False
        self.subspeciesOverride: Optional[str] = None
        self.subspecies: Optional[str] = None
        self.takesAfterMother: bool = False
        self.bodyCoverings: Set[BodyCovering] = set()
        self.heavyMakeup: Optional[List[str]] = list()

    def overrideAttrs(self, bodyMaterial_attribute: _Optional_str = None, bodySize_attribute: _Optional_str = None, femininity_attribute: _Optional_str = None, feral_attribute: _Optional_str = None, genitalArrangement_attribute: _Optional_str = None, height_attribute: _Optional_str = None, muscle_attribute: _Optional_str = None, piercedStomach_attribute: _Optional_str = None, pubicHair_attribute: _Optional_str = None, subspecies_attribute: _Optional_str = None, subspeciesOverride_attribute: _Optional_str = None, takesAfterMother_attribute: _Optional_str = None, version_attribute: _Optional_str = None) -> None:
        if bodyMaterial_attribute is not None:
            self._XMLID_ATTR_BODYMATERIAL_ATTRIBUTE = bodyMaterial_attribute
        if bodySize_attribute is not None:
            self._XMLID_ATTR_BODYSIZE_ATTRIBUTE = bodySize_attribute
        if femininity_attribute is not None:
            self._XMLID_ATTR_FEMININITY_ATTRIBUTE = femininity_attribute
        if feral_attribute is not None:
            self._XMLID_ATTR_FERAL_ATTRIBUTE = feral_attribute
        if genitalArrangement_attribute is not None:
            self._XMLID_ATTR_GENITALARRANGEMENT_ATTRIBUTE = genitalArrangement_attribute
        if height_attribute is not None:
            self._XMLID_ATTR_HEIGHT_ATTRIBUTE = height_attribute
        if muscle_attribute is not None:
            self._XMLID_ATTR_MUSCLE_ATTRIBUTE = muscle_attribute
        if piercedStomach_attribute is not None:
            self._XMLID_ATTR_PIERCEDSTOMACH_ATTRIBUTE = piercedStomach_attribute
        if pubicHair_attribute is not None:
            self._XMLID_ATTR_PUBICHAIR_ATTRIBUTE = pubicHair_attribute
        if subspecies_attribute is not None:
            self._XMLID_ATTR_SUBSPECIES_ATTRIBUTE = subspecies_attribute
        if subspeciesOverride_attribute is not None:
            self._XMLID_ATTR_SUBSPECIESOVERRIDE_ATTRIBUTE = subspeciesOverride_attribute
        if takesAfterMother_attribute is not None:
            self._XMLID_ATTR_TAKESAFTERMOTHER_ATTRIBUTE = takesAfterMother_attribute
        if version_attribute is not None:
            self._XMLID_ATTR_VERSION_ATTRIBUTE = version_attribute

    def overrideTags(self, bodyCoverings_child: _Optional_str = None, bodyCoverings_valueelem: _Optional_str = None, heavyMakeup_child: _Optional_str = None, heavyMakeup_parent: _Optional_str = None) -> None:
        if bodyCoverings_child is not None:
            self._XMLID_TAG_BODYCOVERINGS_CHILD = bodyCoverings_child
        if bodyCoverings_valueelem is not None:
            self._XMLID_TAG_BODYCOVERINGS_VALUEELEM = bodyCoverings_valueelem
        if heavyMakeup_child is not None:
            self._XMLID_TAG_HEAVYMAKEUP_CHILD = heavyMakeup_child
        if heavyMakeup_parent is not None:
            self._XMLID_TAG_HEAVYMAKEUP_PARENT = heavyMakeup_parent

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        lp: etree._Element
        lv: Savable
        value = e.get(self._XMLID_ATTR_VERSION_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_VERSION_ATTRIBUTE, 'version')
        else:
            self.version = str(value)
        value = e.get(self._XMLID_ATTR_PIERCEDSTOMACH_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_PIERCEDSTOMACH_ATTRIBUTE, 'piercedStomach')
        else:
            self.piercedStomach = XML2BOOL[value]
        value = e.get(self._XMLID_ATTR_HEIGHT_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_HEIGHT_ATTRIBUTE, 'height')
        else:
            self.height = int(value)
        value = e.get(self._XMLID_ATTR_FEMININITY_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_FEMININITY_ATTRIBUTE, 'femininity')
        else:
            self.femininity = int(value)
        value = e.get(self._XMLID_ATTR_BODYSIZE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_BODYSIZE_ATTRIBUTE, 'bodySize')
        else:
            self.bodySize = int(value)
        value = e.get(self._XMLID_ATTR_MUSCLE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_MUSCLE_ATTRIBUTE, 'muscle')
        else:
            self.muscle = int(value)
        value = e.get(self._XMLID_ATTR_PUBICHAIR_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_PUBICHAIR_ATTRIBUTE, 'pubicHair')
        else:
            self.pubicHair = EBodyHair[value]
        value = e.get(self._XMLID_ATTR_BODYMATERIAL_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_BODYMATERIAL_ATTRIBUTE, 'bodyMaterial')
        else:
            self.bodyMaterial = str(value)
        value = e.get(self._XMLID_ATTR_GENITALARRANGEMENT_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_GENITALARRANGEMENT_ATTRIBUTE, 'genitalArrangement')
        else:
            self.genitalArrangement = str(value)
        value = e.get(self._XMLID_ATTR_FERAL_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_FERAL_ATTRIBUTE, 'feral')
        else:
            self.feral = XML2BOOL[value]
        value = e.get(self._XMLID_ATTR_SUBSPECIESOVERRIDE_ATTRIBUTE, None)
        if value is None:
            self.subspeciesOverride = None
        else:
            self.subspeciesOverride = str(value)
        value = e.get(self._XMLID_ATTR_SUBSPECIES_ATTRIBUTE, None)
        if value is None:
            self.subspecies = None
        else:
            self.subspecies = str(value)
        value = e.get(self._XMLID_ATTR_TAKESAFTERMOTHER_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_TAKESAFTERMOTHER_ATTRIBUTE, 'takesAfterMother')
        else:
            self.takesAfterMother = XML2BOOL[value]
        for lc in e.iterfind(self._XMLID_TAG_BODYCOVERINGS_CHILD):
            lv = BodyCovering()
            lv.fromXML(lc)
            self.bodyCoverings.add(lv)
        if (lf := e.find(self._XMLID_TAG_HEAVYMAKEUP_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_HEAVYMAKEUP_CHILD):
                    self.heavyMakeup.append(lc.text)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        e.attrib[self._XMLID_ATTR_VERSION_ATTRIBUTE] = str(self.version)
        e.attrib[self._XMLID_ATTR_PIERCEDSTOMACH_ATTRIBUTE] = str(self.piercedStomach).lower()
        e.attrib[self._XMLID_ATTR_HEIGHT_ATTRIBUTE] = str(self.height)
        e.attrib[self._XMLID_ATTR_FEMININITY_ATTRIBUTE] = str(self.femininity)
        e.attrib[self._XMLID_ATTR_BODYSIZE_ATTRIBUTE] = str(self.bodySize)
        e.attrib[self._XMLID_ATTR_MUSCLE_ATTRIBUTE] = str(self.muscle)
        e.attrib[self._XMLID_ATTR_PUBICHAIR_ATTRIBUTE] = self.pubicHair.name
        e.attrib[self._XMLID_ATTR_BODYMATERIAL_ATTRIBUTE] = str(self.bodyMaterial)
        e.attrib[self._XMLID_ATTR_GENITALARRANGEMENT_ATTRIBUTE] = str(self.genitalArrangement)
        e.attrib[self._XMLID_ATTR_FERAL_ATTRIBUTE] = str(self.feral).lower()
        if self.subspeciesOverride is not None:
            e.attrib[self._XMLID_ATTR_SUBSPECIESOVERRIDE_ATTRIBUTE] = str(self.subspeciesOverride)
        if self.subspecies is not None:
            e.attrib[self._XMLID_ATTR_SUBSPECIES_ATTRIBUTE] = str(self.subspecies)
        e.attrib[self._XMLID_ATTR_TAKESAFTERMOTHER_ATTRIBUTE] = str(self.takesAfterMother).lower()
        for lv in sorted(self.bodyCoverings, key=lambda x: x.type):
            e.append(lv.toXML())
        if self.heavyMakeup is not None and len(self.heavyMakeup) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_HEAVYMAKEUP_PARENT)
            for lv in self.heavyMakeup:
                etree.SubElement(lp, self._XMLID_TAG_HEAVYMAKEUP_CHILD, {}).text = lv
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['version'] = self.version
        data['piercedStomach'] = bool(self.piercedStomach)
        data['height'] = int(self.height)
        data['femininity'] = int(self.femininity)
        data['bodySize'] = int(self.bodySize)
        data['muscle'] = int(self.muscle)
        data['pubicHair'] = self.pubicHair.name
        data['bodyMaterial'] = self.bodyMaterial
        data['genitalArrangement'] = self.genitalArrangement
        data['feral'] = bool(self.feral)
        if self.subspeciesOverride is not None:
            data['subspeciesOverride'] = self.subspeciesOverride
        if self.subspecies is not None:
            data['subspecies'] = self.subspecies
        data['takesAfterMother'] = bool(self.takesAfterMother)
        if self.bodyCoverings is None or len(self.bodyCoverings) > 0:
            data['bodyCoverings'] = list()
        else:
            lc = list()
            if len(self.bodyCoverings) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['bodyCoverings'] = lc
        if self.heavyMakeup is None or len(self.heavyMakeup) > 0:
            data['heavyMakeup'] = list()
        else:
            lc = list()
            if len(self.heavyMakeup) > 0:
                for lv in lc:
                    lc.append(str(lv))
            data['heavyMakeup'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'version' not in data:
            raise KeyRequiredException(self, data, 'version', 'version')
        self.version = str(data['version'])
        if 'piercedStomach' not in data:
            raise KeyRequiredException(self, data, 'piercedStomach', 'piercedStomach')
        self.piercedStomach = bool(data['piercedStomach'])
        if 'height' not in data:
            raise KeyRequiredException(self, data, 'height', 'height')
        self.height = int(data['height'])
        if 'femininity' not in data:
            raise KeyRequiredException(self, data, 'femininity', 'femininity')
        self.femininity = int(data['femininity'])
        if 'bodySize' not in data:
            raise KeyRequiredException(self, data, 'bodySize', 'bodySize')
        self.bodySize = int(data['bodySize'])
        if 'muscle' not in data:
            raise KeyRequiredException(self, data, 'muscle', 'muscle')
        self.muscle = int(data['muscle'])
        if 'pubicHair' not in data:
            raise KeyRequiredException(self, data, 'pubicHair', 'pubicHair')
        self.pubicHair = EBodyHair[data['pubicHair']]
        if 'bodyMaterial' not in data:
            raise KeyRequiredException(self, data, 'bodyMaterial', 'bodyMaterial')
        self.bodyMaterial = str(data['bodyMaterial'])
        if 'genitalArrangement' not in data:
            raise KeyRequiredException(self, data, 'genitalArrangement', 'genitalArrangement')
        self.genitalArrangement = str(data['genitalArrangement'])
        if 'feral' not in data:
            raise KeyRequiredException(self, data, 'feral', 'feral')
        self.feral = bool(data['feral'])
        self.subspeciesOverride = str(data['subspeciesOverride']) if 'subspeciesOverride' in data else None
        self.subspecies = str(data['subspecies']) if 'subspecies' in data else None
        if 'takesAfterMother' not in data:
            raise KeyRequiredException(self, data, 'takesAfterMother', 'takesAfterMother')
        self.takesAfterMother = bool(data['takesAfterMother'])
        if (lv := self.bodyCoverings) is not None:
            for le in lv:
                self.bodyCoverings.add(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'bodyCoverings', 'bodyCoverings')
        if (lv := self.heavyMakeup) is not None:
            for le in lv:
                self.heavyMakeup.append(str(le))
        else:
            self.heavyMakeup = list()

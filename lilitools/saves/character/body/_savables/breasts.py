#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.character.body.bodypart import BodyPart
from lilitools.saves.character.enums.breast_shape import EBreastShape
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
__all__ = ['RawBreasts']
## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public Element saveAsXML(Element parentElement, Document doc) { @ tXdWc3t8yE00YDC19flWbA+UmZMQJYMAJV3g5fn/WVE+BPjLWT/vzAiHRERWQ460QYrgA7tZQXtoP5fcY7cq5w==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawBreasts(BodyPart):
    TAG = 'breasts'
    _XMLID_ATTR_MILKREGENERATION_ATTRIBUTE: str = 'milkRegeneration'
    _XMLID_ATTR_MILKSTORAGE_ATTRIBUTE: str = 'milkStorage'
    _XMLID_ATTR_NIPPLECOUNTPERBREAST_ATTRIBUTE: str = 'nippleCountPerBreast'
    _XMLID_ATTR_ROWS_ATTRIBUTE: str = 'rows'
    _XMLID_ATTR_SHAPE_ATTRIBUTE: str = 'shape'
    _XMLID_ATTR_SIZE_ATTRIBUTE: str = 'size'
    _XMLID_ATTR_STOREDMILK_ATTRIBUTE: str = 'storedMilk'
    _XMLID_ATTR_TYPE_ATTRIBUTE: str = 'type'

    def __init__(self) -> None:
        super().__init__()
        self.type: str = ''
        self.shape: EBreastShape = EBreastShape(0)
        self.size: int = 0
        self.rows: int = 0
        self.milkStorage: int = 0
        self.storedMilk: float = 0.
        self.milkRegeneration: int = 0
        self.nippleCountPerBreast: int = 0

    def overrideAttrs(self, milkRegeneration_attribute: _Optional_str = None, milkStorage_attribute: _Optional_str = None, nippleCountPerBreast_attribute: _Optional_str = None, rows_attribute: _Optional_str = None, shape_attribute: _Optional_str = None, size_attribute: _Optional_str = None, storedMilk_attribute: _Optional_str = None, type_attribute: _Optional_str = None) -> None:
        if milkRegeneration_attribute is not None:
            self._XMLID_ATTR_MILKREGENERATION_ATTRIBUTE = milkRegeneration_attribute
        if milkStorage_attribute is not None:
            self._XMLID_ATTR_MILKSTORAGE_ATTRIBUTE = milkStorage_attribute
        if nippleCountPerBreast_attribute is not None:
            self._XMLID_ATTR_NIPPLECOUNTPERBREAST_ATTRIBUTE = nippleCountPerBreast_attribute
        if rows_attribute is not None:
            self._XMLID_ATTR_ROWS_ATTRIBUTE = rows_attribute
        if shape_attribute is not None:
            self._XMLID_ATTR_SHAPE_ATTRIBUTE = shape_attribute
        if size_attribute is not None:
            self._XMLID_ATTR_SIZE_ATTRIBUTE = size_attribute
        if storedMilk_attribute is not None:
            self._XMLID_ATTR_STOREDMILK_ATTRIBUTE = storedMilk_attribute
        if type_attribute is not None:
            self._XMLID_ATTR_TYPE_ATTRIBUTE = type_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_TYPE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_TYPE_ATTRIBUTE, 'type')
        else:
            self.type = str(value)
        value = e.get(self._XMLID_ATTR_SHAPE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_SHAPE_ATTRIBUTE, 'shape')
        else:
            self.shape = EBreastShape[value]
        value = e.get(self._XMLID_ATTR_SIZE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_SIZE_ATTRIBUTE, 'size')
        else:
            self.size = int(value)
        value = e.get(self._XMLID_ATTR_ROWS_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_ROWS_ATTRIBUTE, 'rows')
        else:
            self.rows = int(value)
        value = e.get(self._XMLID_ATTR_MILKSTORAGE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_MILKSTORAGE_ATTRIBUTE, 'milkStorage')
        else:
            self.milkStorage = int(value)
        value = e.get(self._XMLID_ATTR_STOREDMILK_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_STOREDMILK_ATTRIBUTE, 'storedMilk')
        else:
            self.storedMilk = float(value)
        value = e.get(self._XMLID_ATTR_MILKREGENERATION_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_MILKREGENERATION_ATTRIBUTE, 'milkRegeneration')
        else:
            self.milkRegeneration = int(value)
        value = e.get(self._XMLID_ATTR_NIPPLECOUNTPERBREAST_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_NIPPLECOUNTPERBREAST_ATTRIBUTE, 'nippleCountPerBreast')
        else:
            self.nippleCountPerBreast = int(value)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_TYPE_ATTRIBUTE] = str(self.type)
        e.attrib[self._XMLID_ATTR_SHAPE_ATTRIBUTE] = self.shape.name
        e.attrib[self._XMLID_ATTR_SIZE_ATTRIBUTE] = str(self.size)
        e.attrib[self._XMLID_ATTR_ROWS_ATTRIBUTE] = str(self.rows)
        e.attrib[self._XMLID_ATTR_MILKSTORAGE_ATTRIBUTE] = str(self.milkStorage)
        e.attrib[self._XMLID_ATTR_STOREDMILK_ATTRIBUTE] = str(float(self.storedMilk))
        e.attrib[self._XMLID_ATTR_MILKREGENERATION_ATTRIBUTE] = str(self.milkRegeneration)
        e.attrib[self._XMLID_ATTR_NIPPLECOUNTPERBREAST_ATTRIBUTE] = str(self.nippleCountPerBreast)
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['type'] = self.type
        data['shape'] = self.shape.name
        data['size'] = int(self.size)
        data['rows'] = int(self.rows)
        data['milkStorage'] = int(self.milkStorage)
        data['storedMilk'] = float(self.storedMilk)
        data['milkRegeneration'] = int(self.milkRegeneration)
        data['nippleCountPerBreast'] = int(self.nippleCountPerBreast)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'type' not in data:
            raise KeyRequiredException(self, data, 'type', 'type')
        self.type = str(data['type'])
        if 'shape' not in data:
            raise KeyRequiredException(self, data, 'shape', 'shape')
        self.shape = EBreastShape[data['shape']]
        if 'size' not in data:
            raise KeyRequiredException(self, data, 'size', 'size')
        self.size = int(data['size'])
        if 'rows' not in data:
            raise KeyRequiredException(self, data, 'rows', 'rows')
        self.rows = int(data['rows'])
        if 'milkStorage' not in data:
            raise KeyRequiredException(self, data, 'milkStorage', 'milkStorage')
        self.milkStorage = int(data['milkStorage'])
        if 'storedMilk' not in data:
            raise KeyRequiredException(self, data, 'storedMilk', 'storedMilk')
        self.storedMilk = float(data['storedMilk'])
        if 'milkRegeneration' not in data:
            raise KeyRequiredException(self, data, 'milkRegeneration', 'milkRegeneration')
        self.milkRegeneration = int(data['milkRegeneration'])
        if 'nippleCountPerBreast' not in data:
            raise KeyRequiredException(self, data, 'nippleCountPerBreast', 'nippleCountPerBreast')
        self.nippleCountPerBreast = int(data['nippleCountPerBreast'])

#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL, Savable
__all__ = ['RawCovering']
## from [LT]/src/com/lilithsthrone/game/character/body/coverings/Covering.java: public Element saveAsXML(Element parentElement, Document doc) { @ ZgkO+ZY8shmx4Tt88ur5qmsiwwjofmf/G0gUlE3mQgyvzX26jCHh63tElburOtzgkksjODQ/AUCu26zu5dC69g==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawCovering(Savable):
    TAG = 'Covering'
    _XMLID_ATTR_COLOR1_ATTRIBUTE: str = 'c1'
    _XMLID_ATTR_COLOR2_ATTRIBUTE: str = 'c2'
    _XMLID_ATTR_GLOW1_ATTRIBUTE: str = 'g1'
    _XMLID_ATTR_GLOW2_ATTRIBUTE: str = 'g2'
    _XMLID_ATTR_MODIFIER_ATTRIBUTE: str = 'mod'
    _XMLID_ATTR_PATTERN_ATTRIBUTE: str = 'pat'
    _XMLID_ATTR_TYPE_ATTRIBUTE: str = 'type'

    def __init__(self) -> None:
        super().__init__()
        self.type: str = ''
        self.pattern: str = ''
        self.modifier: str = ''
        self.color1: str = ''
        self.glow1: Optional[bool] = None
        self.color2: str = ''
        self.glow2: Optional[bool] = None

    def overrideAttrs(self, color1_attribute: _Optional_str = None, color2_attribute: _Optional_str = None, glow1_attribute: _Optional_str = None, glow2_attribute: _Optional_str = None, modifier_attribute: _Optional_str = None, pattern_attribute: _Optional_str = None, type_attribute: _Optional_str = None) -> None:
        if color1_attribute is not None:
            self._XMLID_ATTR_COLOR1_ATTRIBUTE = color1_attribute
        if color2_attribute is not None:
            self._XMLID_ATTR_COLOR2_ATTRIBUTE = color2_attribute
        if glow1_attribute is not None:
            self._XMLID_ATTR_GLOW1_ATTRIBUTE = glow1_attribute
        if glow2_attribute is not None:
            self._XMLID_ATTR_GLOW2_ATTRIBUTE = glow2_attribute
        if modifier_attribute is not None:
            self._XMLID_ATTR_MODIFIER_ATTRIBUTE = modifier_attribute
        if pattern_attribute is not None:
            self._XMLID_ATTR_PATTERN_ATTRIBUTE = pattern_attribute
        if type_attribute is not None:
            self._XMLID_ATTR_TYPE_ATTRIBUTE = type_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_TYPE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_TYPE_ATTRIBUTE, 'type')
        else:
            self.type = str(value)
        value = e.get(self._XMLID_ATTR_PATTERN_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_PATTERN_ATTRIBUTE, 'pattern')
        else:
            self.pattern = str(value)
        value = e.get(self._XMLID_ATTR_MODIFIER_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_MODIFIER_ATTRIBUTE, 'modifier')
        else:
            self.modifier = str(value)
        value = e.get(self._XMLID_ATTR_COLOR1_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_COLOR1_ATTRIBUTE, 'color1')
        else:
            self.color1 = str(value)
        value = e.get(self._XMLID_ATTR_GLOW1_ATTRIBUTE, None)
        if value is None:
            self.glow1 = None
        else:
            self.glow1 = XML2BOOL[value]
        value = e.get(self._XMLID_ATTR_COLOR2_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_COLOR2_ATTRIBUTE, 'color2')
        else:
            self.color2 = str(value)
        value = e.get(self._XMLID_ATTR_GLOW2_ATTRIBUTE, None)
        if value is None:
            self.glow2 = None
        else:
            self.glow2 = XML2BOOL[value]

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_TYPE_ATTRIBUTE] = str(self.type)
        e.attrib[self._XMLID_ATTR_PATTERN_ATTRIBUTE] = str(self.pattern)
        e.attrib[self._XMLID_ATTR_MODIFIER_ATTRIBUTE] = str(self.modifier)
        e.attrib[self._XMLID_ATTR_COLOR1_ATTRIBUTE] = str(self.color1)
        if self.glow1 is not None:
            e.attrib[self._XMLID_ATTR_GLOW1_ATTRIBUTE] = str(self.glow1).lower()
        e.attrib[self._XMLID_ATTR_COLOR2_ATTRIBUTE] = str(self.color2)
        if self.glow2 is not None:
            e.attrib[self._XMLID_ATTR_GLOW2_ATTRIBUTE] = str(self.glow2).lower()
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['type'] = self.type
        data['pattern'] = self.pattern
        data['modifier'] = self.modifier
        data['color1'] = self.color1
        if self.glow1 is not None:
            data['glow1'] = bool(self.glow1)
        data['color2'] = self.color2
        if self.glow2 is not None:
            data['glow2'] = bool(self.glow2)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'type' not in data:
            raise KeyRequiredException(self, data, 'type', 'type')
        self.type = str(data['type'])
        if 'pattern' not in data:
            raise KeyRequiredException(self, data, 'pattern', 'pattern')
        self.pattern = str(data['pattern'])
        if 'modifier' not in data:
            raise KeyRequiredException(self, data, 'modifier', 'modifier')
        self.modifier = str(data['modifier'])
        if 'color1' not in data:
            raise KeyRequiredException(self, data, 'color1', 'color1')
        self.color1 = str(data['color1'])
        self.glow1 = bool(data['glow1']) if 'glow1' in data else None
        if 'color2' not in data:
            raise KeyRequiredException(self, data, 'color2', 'color2')
        self.color2 = str(data['color2'])
        self.glow2 = bool(data['glow2']) if 'glow2' in data else None

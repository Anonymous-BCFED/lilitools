#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.character.body.bodypart import BodyPart
from lilitools.saves.character.body.enums.eye_shape import EEyeShape
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
__all__ = ['RawEyes']
## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public Element saveAsXML(Element parentElement, Document doc) { @ tXdWc3t8yE00YDC19flWbA+UmZMQJYMAJV3g5fn/WVE+BPjLWT/vzAiHRERWQ460QYrgA7tZQXtoP5fcY7cq5w==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawEyes(BodyPart):
    TAG = 'eye'
    _XMLID_ATTR_EYEPAIRS_ATTRIBUTE: str = 'eyePairs'
    _XMLID_ATTR_IRISSHAPE_ATTRIBUTE: str = 'irisShape'
    _XMLID_ATTR_PUPILSHAPE_ATTRIBUTE: str = 'pupilShape'
    _XMLID_ATTR_TYPE_ATTRIBUTE: str = 'type'

    def __init__(self) -> None:
        super().__init__()
        self.type: str = ''
        self.eyePairs: int = 0
        self.irisShape: EEyeShape = EEyeShape(0)
        self.pupilShape: EEyeShape = EEyeShape(0)

    def overrideAttrs(self, eyePairs_attribute: _Optional_str = None, irisShape_attribute: _Optional_str = None, pupilShape_attribute: _Optional_str = None, type_attribute: _Optional_str = None) -> None:
        if eyePairs_attribute is not None:
            self._XMLID_ATTR_EYEPAIRS_ATTRIBUTE = eyePairs_attribute
        if irisShape_attribute is not None:
            self._XMLID_ATTR_IRISSHAPE_ATTRIBUTE = irisShape_attribute
        if pupilShape_attribute is not None:
            self._XMLID_ATTR_PUPILSHAPE_ATTRIBUTE = pupilShape_attribute
        if type_attribute is not None:
            self._XMLID_ATTR_TYPE_ATTRIBUTE = type_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_TYPE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_TYPE_ATTRIBUTE, 'type')
        else:
            self.type = str(value)
        value = e.get(self._XMLID_ATTR_EYEPAIRS_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_EYEPAIRS_ATTRIBUTE, 'eyePairs')
        else:
            self.eyePairs = int(value)
        value = e.get(self._XMLID_ATTR_IRISSHAPE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_IRISSHAPE_ATTRIBUTE, 'irisShape')
        else:
            self.irisShape = EEyeShape[value]
        value = e.get(self._XMLID_ATTR_PUPILSHAPE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_PUPILSHAPE_ATTRIBUTE, 'pupilShape')
        else:
            self.pupilShape = EEyeShape[value]

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_TYPE_ATTRIBUTE] = str(self.type)
        e.attrib[self._XMLID_ATTR_EYEPAIRS_ATTRIBUTE] = str(self.eyePairs)
        e.attrib[self._XMLID_ATTR_IRISSHAPE_ATTRIBUTE] = self.irisShape.name
        e.attrib[self._XMLID_ATTR_PUPILSHAPE_ATTRIBUTE] = self.pupilShape.name
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['type'] = self.type
        data['eyePairs'] = int(self.eyePairs)
        data['irisShape'] = self.irisShape.name
        data['pupilShape'] = self.pupilShape.name
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'type' not in data:
            raise KeyRequiredException(self, data, 'type', 'type')
        self.type = str(data['type'])
        if 'eyePairs' not in data:
            raise KeyRequiredException(self, data, 'eyePairs', 'eyePairs')
        self.eyePairs = int(data['eyePairs'])
        if 'irisShape' not in data:
            raise KeyRequiredException(self, data, 'irisShape', 'irisShape')
        self.irisShape = EEyeShape[data['irisShape']]
        if 'pupilShape' not in data:
            raise KeyRequiredException(self, data, 'pupilShape', 'pupilShape')
        self.pupilShape = EEyeShape[data['pupilShape']]

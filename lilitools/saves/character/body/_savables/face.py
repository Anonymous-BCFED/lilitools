#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.character.body.bodypart import BodyPart
from lilitools.saves.character.enums.body_hair import EBodyHair
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL
__all__ = ['RawFace']
## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public Element saveAsXML(Element parentElement, Document doc) { @ tXdWc3t8yE00YDC19flWbA+UmZMQJYMAJV3g5fn/WVE+BPjLWT/vzAiHRERWQ460QYrgA7tZQXtoP5fcY7cq5w==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawFace(BodyPart):
    TAG = 'face'
    _XMLID_ATTR_FACIALHAIR_ATTRIBUTE: str = 'facialHair'
    _XMLID_ATTR_PIERCEDNOSE_ATTRIBUTE: str = 'piercedNose'
    _XMLID_ATTR_TYPE_ATTRIBUTE: str = 'type'

    def __init__(self) -> None:
        super().__init__()
        self.type: str = ''
        self.piercedNose: bool = False
        self.facialHair: EBodyHair = EBodyHair(0)

    def overrideAttrs(self, facialHair_attribute: _Optional_str = None, piercedNose_attribute: _Optional_str = None, type_attribute: _Optional_str = None) -> None:
        if facialHair_attribute is not None:
            self._XMLID_ATTR_FACIALHAIR_ATTRIBUTE = facialHair_attribute
        if piercedNose_attribute is not None:
            self._XMLID_ATTR_PIERCEDNOSE_ATTRIBUTE = piercedNose_attribute
        if type_attribute is not None:
            self._XMLID_ATTR_TYPE_ATTRIBUTE = type_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_TYPE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_TYPE_ATTRIBUTE, 'type')
        else:
            self.type = str(value)
        value = e.get(self._XMLID_ATTR_PIERCEDNOSE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_PIERCEDNOSE_ATTRIBUTE, 'piercedNose')
        else:
            self.piercedNose = XML2BOOL[value]
        value = e.get(self._XMLID_ATTR_FACIALHAIR_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_FACIALHAIR_ATTRIBUTE, 'facialHair')
        else:
            self.facialHair = EBodyHair[value]

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_TYPE_ATTRIBUTE] = str(self.type)
        e.attrib[self._XMLID_ATTR_PIERCEDNOSE_ATTRIBUTE] = str(self.piercedNose).lower()
        e.attrib[self._XMLID_ATTR_FACIALHAIR_ATTRIBUTE] = self.facialHair.name
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['type'] = self.type
        data['piercedNose'] = bool(self.piercedNose)
        data['facialHair'] = self.facialHair.name
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'type' not in data:
            raise KeyRequiredException(self, data, 'type', 'type')
        self.type = str(data['type'])
        if 'piercedNose' not in data:
            raise KeyRequiredException(self, data, 'piercedNose', 'piercedNose')
        self.piercedNose = bool(data['piercedNose'])
        if 'facialHair' not in data:
            raise KeyRequiredException(self, data, 'facialHair', 'facialHair')
        self.facialHair = EBodyHair[data['facialHair']]

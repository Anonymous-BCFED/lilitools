#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.character.body.bodypart import BodyPart
from lilitools.saves.character.body.enums.hair_style import EHairStyle
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL
__all__ = ['RawHair']
## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public Element saveAsXML(Element parentElement, Document doc) { @ tXdWc3t8yE00YDC19flWbA+UmZMQJYMAJV3g5fn/WVE+BPjLWT/vzAiHRERWQ460QYrgA7tZQXtoP5fcY7cq5w==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawHair(BodyPart):
    TAG = 'hair'
    _XMLID_ATTR_HAIRSTYLE_ATTRIBUTE: str = 'hairStyle'
    _XMLID_ATTR_LENGTH_ATTRIBUTE: str = 'length'
    _XMLID_ATTR_NECKFLUFF_ATTRIBUTE: str = 'neckFluff'
    _XMLID_ATTR_TYPE_ATTRIBUTE: str = 'type'

    def __init__(self) -> None:
        super().__init__()
        self.type: str = ''
        self.length: int = 0
        self.hairStyle: EHairStyle = EHairStyle(0)
        self.neckFluff: bool = False

    def overrideAttrs(self, hairStyle_attribute: _Optional_str = None, length_attribute: _Optional_str = None, neckFluff_attribute: _Optional_str = None, type_attribute: _Optional_str = None) -> None:
        if hairStyle_attribute is not None:
            self._XMLID_ATTR_HAIRSTYLE_ATTRIBUTE = hairStyle_attribute
        if length_attribute is not None:
            self._XMLID_ATTR_LENGTH_ATTRIBUTE = length_attribute
        if neckFluff_attribute is not None:
            self._XMLID_ATTR_NECKFLUFF_ATTRIBUTE = neckFluff_attribute
        if type_attribute is not None:
            self._XMLID_ATTR_TYPE_ATTRIBUTE = type_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_TYPE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_TYPE_ATTRIBUTE, 'type')
        else:
            self.type = str(value)
        value = e.get(self._XMLID_ATTR_LENGTH_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_LENGTH_ATTRIBUTE, 'length')
        else:
            self.length = int(value)
        value = e.get(self._XMLID_ATTR_HAIRSTYLE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_HAIRSTYLE_ATTRIBUTE, 'hairStyle')
        else:
            self.hairStyle = EHairStyle[value]
        value = e.get(self._XMLID_ATTR_NECKFLUFF_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_NECKFLUFF_ATTRIBUTE, 'neckFluff')
        else:
            self.neckFluff = XML2BOOL[value]

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_TYPE_ATTRIBUTE] = str(self.type)
        e.attrib[self._XMLID_ATTR_LENGTH_ATTRIBUTE] = str(self.length)
        e.attrib[self._XMLID_ATTR_HAIRSTYLE_ATTRIBUTE] = self.hairStyle.name
        e.attrib[self._XMLID_ATTR_NECKFLUFF_ATTRIBUTE] = str(self.neckFluff).lower()
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['type'] = self.type
        data['length'] = int(self.length)
        data['hairStyle'] = self.hairStyle.name
        data['neckFluff'] = bool(self.neckFluff)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'type' not in data:
            raise KeyRequiredException(self, data, 'type', 'type')
        self.type = str(data['type'])
        if 'length' not in data:
            raise KeyRequiredException(self, data, 'length', 'length')
        self.length = int(data['length'])
        if 'hairStyle' not in data:
            raise KeyRequiredException(self, data, 'hairStyle', 'hairStyle')
        self.hairStyle = EHairStyle[data['hairStyle']]
        if 'neckFluff' not in data:
            raise KeyRequiredException(self, data, 'neckFluff', 'neckFluff')
        self.neckFluff = bool(data['neckFluff'])

#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.character.body.bodypart import BodyPart
from lilitools.saves.character.body.enums.foot_structure import EFootStructure
from lilitools.saves.character.body.enums.leg_configuration import ELegConfiguration
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
__all__ = ['RawLegs']
## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public Element saveAsXML(Element parentElement, Document doc) { @ tXdWc3t8yE00YDC19flWbA+UmZMQJYMAJV3g5fn/WVE+BPjLWT/vzAiHRERWQ460QYrgA7tZQXtoP5fcY7cq5w==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawLegs(BodyPart):
    TAG = 'leg'
    _XMLID_ATTR_CONFIGURATION_ATTRIBUTE: str = 'configuration'
    _XMLID_ATTR_FOOTSTRUCTURE_ATTRIBUTE: str = 'footStructure'
    _XMLID_ATTR_TAILLENGTH_ATTRIBUTE: str = 'tailLength'
    _XMLID_ATTR_TYPE_ATTRIBUTE: str = 'type'

    def __init__(self) -> None:
        super().__init__()
        self.type: str = ''
        self.footStructure: EFootStructure = EFootStructure.PLANTIGRADE
        self.configuration: ELegConfiguration = ELegConfiguration.BIPEDAL
        self.tailLength: float = 5.0

    def overrideAttrs(self, configuration_attribute: _Optional_str = None, footStructure_attribute: _Optional_str = None, tailLength_attribute: _Optional_str = None, type_attribute: _Optional_str = None) -> None:
        if configuration_attribute is not None:
            self._XMLID_ATTR_CONFIGURATION_ATTRIBUTE = configuration_attribute
        if footStructure_attribute is not None:
            self._XMLID_ATTR_FOOTSTRUCTURE_ATTRIBUTE = footStructure_attribute
        if tailLength_attribute is not None:
            self._XMLID_ATTR_TAILLENGTH_ATTRIBUTE = tailLength_attribute
        if type_attribute is not None:
            self._XMLID_ATTR_TYPE_ATTRIBUTE = type_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_TYPE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_TYPE_ATTRIBUTE, 'type')
        else:
            self.type = str(value)
        value = e.get(self._XMLID_ATTR_FOOTSTRUCTURE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_FOOTSTRUCTURE_ATTRIBUTE, 'footStructure')
        else:
            self.footStructure = EFootStructure[value]
        value = e.get(self._XMLID_ATTR_CONFIGURATION_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_CONFIGURATION_ATTRIBUTE, 'configuration')
        else:
            self.configuration = ELegConfiguration[value]
        value = e.get(self._XMLID_ATTR_TAILLENGTH_ATTRIBUTE, None)
        if value is None:
            self.tailLength = 5.0
        else:
            self.tailLength = float(value)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_TYPE_ATTRIBUTE] = str(self.type)
        e.attrib[self._XMLID_ATTR_FOOTSTRUCTURE_ATTRIBUTE] = self.footStructure.name
        e.attrib[self._XMLID_ATTR_CONFIGURATION_ATTRIBUTE] = self.configuration.name
        if self.tailLength != 5.0:
            e.attrib[self._XMLID_ATTR_TAILLENGTH_ATTRIBUTE] = str(float(self.tailLength))
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['type'] = self.type
        data['footStructure'] = self.footStructure.name
        data['configuration'] = self.configuration.name
        if self.tailLength != 5.0:
            data['tailLength'] = float(self.tailLength)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'type' not in data:
            raise KeyRequiredException(self, data, 'type', 'type')
        self.type = str(data['type'])
        if 'footStructure' not in data:
            raise KeyRequiredException(self, data, 'footStructure', 'footStructure')
        self.footStructure = EFootStructure[data['footStructure']]
        if 'configuration' not in data:
            raise KeyRequiredException(self, data, 'configuration', 'configuration')
        self.configuration = ELegConfiguration[data['configuration']]
        self.tailLength = float(data['tailLength']) if 'tailLength' in data else None

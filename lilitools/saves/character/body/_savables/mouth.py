#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.character.body.bodypart import BodyPart
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL
__all__ = ['RawMouth']
## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public Element saveAsXML(Element parentElement, Document doc) { @ tXdWc3t8yE00YDC19flWbA+UmZMQJYMAJV3g5fn/WVE+BPjLWT/vzAiHRERWQ460QYrgA7tZQXtoP5fcY7cq5w==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawMouth(BodyPart):
    TAG = 'mouth'
    _XMLID_ATTR_LIPSIZE_ATTRIBUTE: str = 'lipSize'
    _XMLID_ATTR_PIERCEDLIP_ATTRIBUTE: str = 'piercedLip'

    def __init__(self) -> None:
        super().__init__()
        self.piercedLip: bool = False
        self.lipSize: int = 0

    def overrideAttrs(self, lipSize_attribute: _Optional_str = None, piercedLip_attribute: _Optional_str = None) -> None:
        if lipSize_attribute is not None:
            self._XMLID_ATTR_LIPSIZE_ATTRIBUTE = lipSize_attribute
        if piercedLip_attribute is not None:
            self._XMLID_ATTR_PIERCEDLIP_ATTRIBUTE = piercedLip_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_PIERCEDLIP_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_PIERCEDLIP_ATTRIBUTE, 'piercedLip')
        else:
            self.piercedLip = XML2BOOL[value]
        value = e.get(self._XMLID_ATTR_LIPSIZE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_LIPSIZE_ATTRIBUTE, 'lipSize')
        else:
            self.lipSize = int(value)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_PIERCEDLIP_ATTRIBUTE] = str(self.piercedLip).lower()
        e.attrib[self._XMLID_ATTR_LIPSIZE_ATTRIBUTE] = str(self.lipSize)
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['piercedLip'] = bool(self.piercedLip)
        data['lipSize'] = int(self.lipSize)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'piercedLip' not in data:
            raise KeyRequiredException(self, data, 'piercedLip', 'piercedLip')
        self.piercedLip = bool(data['piercedLip'])
        if 'lipSize' not in data:
            raise KeyRequiredException(self, data, 'lipSize', 'lipSize')
        self.lipSize = int(data['lipSize'])

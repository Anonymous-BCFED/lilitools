#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.character.body.bodypart import BodyPart
from lilitools.saves.character.body.enums.areolae_shape import EAreolaeShape
from lilitools.saves.character.body.enums.nipple_shape import ENippleShape
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL
__all__ = ['RawNipples']
## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public Element saveAsXML(Element parentElement, Document doc) { @ tXdWc3t8yE00YDC19flWbA+UmZMQJYMAJV3g5fn/WVE+BPjLWT/vzAiHRERWQ460QYrgA7tZQXtoP5fcY7cq5w==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawNipples(BodyPart):
    TAG = 'nipples'
    _XMLID_ATTR_AREOLAESHAPE_ATTRIBUTE: str = 'areolaeShape'
    _XMLID_ATTR_AREOLAESIZE_ATTRIBUTE: str = 'areolaeSize'
    _XMLID_ATTR_NIPPLESHAPE_ATTRIBUTE: str = 'nippleShape'
    _XMLID_ATTR_NIPPLESIZE_ATTRIBUTE: str = 'nippleSize'
    _XMLID_ATTR_PIERCED_ATTRIBUTE: str = 'pierced'

    def __init__(self) -> None:
        super().__init__()
        self.pierced: bool = False
        self.nippleSize: int = 0
        self.nippleShape: ENippleShape = ENippleShape(0)
        self.areolaeSize: int = 0
        self.areolaeShape: EAreolaeShape = EAreolaeShape(0)

    def overrideAttrs(self, areolaeShape_attribute: _Optional_str = None, areolaeSize_attribute: _Optional_str = None, nippleShape_attribute: _Optional_str = None, nippleSize_attribute: _Optional_str = None, pierced_attribute: _Optional_str = None) -> None:
        if areolaeShape_attribute is not None:
            self._XMLID_ATTR_AREOLAESHAPE_ATTRIBUTE = areolaeShape_attribute
        if areolaeSize_attribute is not None:
            self._XMLID_ATTR_AREOLAESIZE_ATTRIBUTE = areolaeSize_attribute
        if nippleShape_attribute is not None:
            self._XMLID_ATTR_NIPPLESHAPE_ATTRIBUTE = nippleShape_attribute
        if nippleSize_attribute is not None:
            self._XMLID_ATTR_NIPPLESIZE_ATTRIBUTE = nippleSize_attribute
        if pierced_attribute is not None:
            self._XMLID_ATTR_PIERCED_ATTRIBUTE = pierced_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_PIERCED_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_PIERCED_ATTRIBUTE, 'pierced')
        else:
            self.pierced = XML2BOOL[value]
        value = e.get(self._XMLID_ATTR_NIPPLESIZE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_NIPPLESIZE_ATTRIBUTE, 'nippleSize')
        else:
            self.nippleSize = int(value)
        value = e.get(self._XMLID_ATTR_NIPPLESHAPE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_NIPPLESHAPE_ATTRIBUTE, 'nippleShape')
        else:
            self.nippleShape = ENippleShape[value]
        value = e.get(self._XMLID_ATTR_AREOLAESIZE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_AREOLAESIZE_ATTRIBUTE, 'areolaeSize')
        else:
            self.areolaeSize = int(value)
        value = e.get(self._XMLID_ATTR_AREOLAESHAPE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_AREOLAESHAPE_ATTRIBUTE, 'areolaeShape')
        else:
            self.areolaeShape = EAreolaeShape[value]

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_PIERCED_ATTRIBUTE] = str(self.pierced).lower()
        e.attrib[self._XMLID_ATTR_NIPPLESIZE_ATTRIBUTE] = str(self.nippleSize)
        e.attrib[self._XMLID_ATTR_NIPPLESHAPE_ATTRIBUTE] = self.nippleShape.name
        e.attrib[self._XMLID_ATTR_AREOLAESIZE_ATTRIBUTE] = str(self.areolaeSize)
        e.attrib[self._XMLID_ATTR_AREOLAESHAPE_ATTRIBUTE] = self.areolaeShape.name
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['pierced'] = bool(self.pierced)
        data['nippleSize'] = int(self.nippleSize)
        data['nippleShape'] = self.nippleShape.name
        data['areolaeSize'] = int(self.areolaeSize)
        data['areolaeShape'] = self.areolaeShape.name
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'pierced' not in data:
            raise KeyRequiredException(self, data, 'pierced', 'pierced')
        self.pierced = bool(data['pierced'])
        if 'nippleSize' not in data:
            raise KeyRequiredException(self, data, 'nippleSize', 'nippleSize')
        self.nippleSize = int(data['nippleSize'])
        if 'nippleShape' not in data:
            raise KeyRequiredException(self, data, 'nippleShape', 'nippleShape')
        self.nippleShape = ENippleShape[data['nippleShape']]
        if 'areolaeSize' not in data:
            raise KeyRequiredException(self, data, 'areolaeSize', 'areolaeSize')
        self.areolaeSize = int(data['areolaeSize'])
        if 'areolaeShape' not in data:
            raise KeyRequiredException(self, data, 'areolaeShape', 'areolaeShape')
        self.areolaeShape = EAreolaeShape[data['areolaeShape']]

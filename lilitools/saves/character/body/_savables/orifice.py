#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional, Set
from lxml import etree
from lilitools.saves.character.enums.orifice_modifier import EOrificeModifier
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL, Savable
__all__ = ['RawOrifice']
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawOrifice(Savable):
    TAG = 'orifice'
    _XMLID_ATTR_CAPACITY_ATTRIBUTE: str = 'capacity'
    _XMLID_ATTR_DEPTH_ATTRIBUTE: str = 'depth'
    _XMLID_ATTR_ELASTICITY_ATTRIBUTE: str = 'elasticity'
    _XMLID_ATTR_ORIFICEVIRGIN_ATTRIBUTE: str = 'ORIFICE_VIRGINITY_ATTRIBUTE'
    _XMLID_ATTR_PLASTICITY_ATTRIBUTE: str = 'plasticity'
    _XMLID_ATTR_STRETCHEDCAPACITY_ATTRIBUTE: str = 'stretchedCapacity'
    _XMLID_ATTR_WETNESS_ATTRIBUTE: str = 'wetness'
    _XMLID_TAG_ORIFICEMODIFIERS_CHILD: str = 'mod'

    def __init__(self) -> None:
        super().__init__()
        self.wetness: int = 0
        self.depth: int = 0
        self.elasticity: int = 0
        self.plasticity: int = 0
        self.capacity: float = 0.
        self.stretchedCapacity: float = 0.
        self.orificeVirgin: bool = False
        self.orificeModifiers: Set[EOrificeModifier] = set()

    def overrideAttrs(self, capacity_attribute: _Optional_str = None, depth_attribute: _Optional_str = None, elasticity_attribute: _Optional_str = None, orificeVirgin_attribute: _Optional_str = None, plasticity_attribute: _Optional_str = None, stretchedCapacity_attribute: _Optional_str = None, wetness_attribute: _Optional_str = None) -> None:
        if capacity_attribute is not None:
            self._XMLID_ATTR_CAPACITY_ATTRIBUTE = capacity_attribute
        if depth_attribute is not None:
            self._XMLID_ATTR_DEPTH_ATTRIBUTE = depth_attribute
        if elasticity_attribute is not None:
            self._XMLID_ATTR_ELASTICITY_ATTRIBUTE = elasticity_attribute
        if orificeVirgin_attribute is not None:
            self._XMLID_ATTR_ORIFICEVIRGIN_ATTRIBUTE = orificeVirgin_attribute
        if plasticity_attribute is not None:
            self._XMLID_ATTR_PLASTICITY_ATTRIBUTE = plasticity_attribute
        if stretchedCapacity_attribute is not None:
            self._XMLID_ATTR_STRETCHEDCAPACITY_ATTRIBUTE = stretchedCapacity_attribute
        if wetness_attribute is not None:
            self._XMLID_ATTR_WETNESS_ATTRIBUTE = wetness_attribute

    def overrideTags(self, orificeModifiers_child: _Optional_str = None) -> None:
        if orificeModifiers_child is not None:
            self._XMLID_TAG_ORIFICEMODIFIERS_CHILD = orificeModifiers_child

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        value = e.get(self._XMLID_ATTR_WETNESS_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_WETNESS_ATTRIBUTE, 'wetness')
        else:
            self.wetness = int(value)
        value = e.get(self._XMLID_ATTR_DEPTH_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_DEPTH_ATTRIBUTE, 'depth')
        else:
            self.depth = int(value)
        value = e.get(self._XMLID_ATTR_ELASTICITY_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_ELASTICITY_ATTRIBUTE, 'elasticity')
        else:
            self.elasticity = int(value)
        value = e.get(self._XMLID_ATTR_PLASTICITY_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_PLASTICITY_ATTRIBUTE, 'plasticity')
        else:
            self.plasticity = int(value)
        value = e.get(self._XMLID_ATTR_CAPACITY_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_CAPACITY_ATTRIBUTE, 'capacity')
        else:
            self.capacity = float(value)
        value = e.get(self._XMLID_ATTR_STRETCHEDCAPACITY_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_STRETCHEDCAPACITY_ATTRIBUTE, 'stretchedCapacity')
        else:
            self.stretchedCapacity = float(value)
        value = e.get(self._XMLID_ATTR_ORIFICEVIRGIN_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_ORIFICEVIRGIN_ATTRIBUTE, 'orificeVirgin')
        else:
            self.orificeVirgin = XML2BOOL[value]
        for lc in e.iterfind(self._XMLID_TAG_ORIFICEMODIFIERS_CHILD):
            self.orificeModifiers.add(EOrificeModifier[lc.text])

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        e.attrib[self._XMLID_ATTR_WETNESS_ATTRIBUTE] = str(self.wetness)
        e.attrib[self._XMLID_ATTR_DEPTH_ATTRIBUTE] = str(self.depth)
        e.attrib[self._XMLID_ATTR_ELASTICITY_ATTRIBUTE] = str(self.elasticity)
        e.attrib[self._XMLID_ATTR_PLASTICITY_ATTRIBUTE] = str(self.plasticity)
        e.attrib[self._XMLID_ATTR_CAPACITY_ATTRIBUTE] = str(float(self.capacity))
        e.attrib[self._XMLID_ATTR_STRETCHEDCAPACITY_ATTRIBUTE] = str(float(self.stretchedCapacity))
        e.attrib[self._XMLID_ATTR_ORIFICEVIRGIN_ATTRIBUTE] = str(self.orificeVirgin).lower()
        for lv in sorted(self.orificeModifiers, key=lambda x: x.name):
            etree.SubElement(e, self._XMLID_TAG_ORIFICEMODIFIERS_CHILD, {}).text = lv.name
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['wetness'] = int(self.wetness)
        data['depth'] = int(self.depth)
        data['elasticity'] = int(self.elasticity)
        data['plasticity'] = int(self.plasticity)
        data['capacity'] = float(self.capacity)
        data['stretchedCapacity'] = float(self.stretchedCapacity)
        data['orificeVirgin'] = bool(self.orificeVirgin)
        if self.orificeModifiers is None or len(self.orificeModifiers) > 0:
            data['orificeModifiers'] = list()
        else:
            lc = list()
            if len(self.orificeModifiers) > 0:
                for lv in sorted(lc, lambda x: x.name):
                    lc.append(lv.name)
            data['orificeModifiers'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'wetness' not in data:
            raise KeyRequiredException(self, data, 'wetness', 'wetness')
        self.wetness = int(data['wetness'])
        if 'depth' not in data:
            raise KeyRequiredException(self, data, 'depth', 'depth')
        self.depth = int(data['depth'])
        if 'elasticity' not in data:
            raise KeyRequiredException(self, data, 'elasticity', 'elasticity')
        self.elasticity = int(data['elasticity'])
        if 'plasticity' not in data:
            raise KeyRequiredException(self, data, 'plasticity', 'plasticity')
        self.plasticity = int(data['plasticity'])
        if 'capacity' not in data:
            raise KeyRequiredException(self, data, 'capacity', 'capacity')
        self.capacity = float(data['capacity'])
        if 'stretchedCapacity' not in data:
            raise KeyRequiredException(self, data, 'stretchedCapacity', 'stretchedCapacity')
        self.stretchedCapacity = float(data['stretchedCapacity'])
        if 'orificeVirgin' not in data:
            raise KeyRequiredException(self, data, 'orificeVirgin', 'orificeVirgin')
        self.orificeVirgin = bool(data['orificeVirgin'])
        if (lv := self.orificeModifiers) is not None:
            for le in lv:
                self.orificeModifiers.add(le.name)
        else:
            raise KeyRequiredException(self, data, 'orificeModifiers', 'orificeModifiers')

#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional, Set
from lxml import etree
from lilitools.saves.character.enums.penetrator_modifier import EPenetratorModifier
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL, Savable
__all__ = ['RawPenetrator']
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawPenetrator(Savable):
    TAG = 'penetrator'
    _XMLID_ATTR_GIRTH_ATTRIBUTE: str = 'girth'
    _XMLID_ATTR_LENGTH_ATTRIBUTE: str = 'length'
    _XMLID_ATTR_PENETRATORVIRGIN_ATTRIBUTE: str = 'virgin'
    _XMLID_TAG_PENETRATORMODIFIERS_CHILD: str = 'mod'

    def __init__(self) -> None:
        super().__init__()
        self.length: int = 0
        self.girth: int = 0
        self.penetratorVirgin: bool = False
        self.penetratorModifiers: Set[EPenetratorModifier] = set()

    def overrideAttrs(self, girth_attribute: _Optional_str = None, length_attribute: _Optional_str = None, penetratorVirgin_attribute: _Optional_str = None) -> None:
        if girth_attribute is not None:
            self._XMLID_ATTR_GIRTH_ATTRIBUTE = girth_attribute
        if length_attribute is not None:
            self._XMLID_ATTR_LENGTH_ATTRIBUTE = length_attribute
        if penetratorVirgin_attribute is not None:
            self._XMLID_ATTR_PENETRATORVIRGIN_ATTRIBUTE = penetratorVirgin_attribute

    def overrideTags(self, penetratorModifiers_child: _Optional_str = None) -> None:
        if penetratorModifiers_child is not None:
            self._XMLID_TAG_PENETRATORMODIFIERS_CHILD = penetratorModifiers_child

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        value = e.get(self._XMLID_ATTR_LENGTH_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_LENGTH_ATTRIBUTE, 'length')
        else:
            self.length = int(value)
        value = e.get(self._XMLID_ATTR_GIRTH_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_GIRTH_ATTRIBUTE, 'girth')
        else:
            self.girth = int(value)
        value = e.get(self._XMLID_ATTR_PENETRATORVIRGIN_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_PENETRATORVIRGIN_ATTRIBUTE, 'penetratorVirgin')
        else:
            self.penetratorVirgin = XML2BOOL[value]
        for lc in e.iterfind(self._XMLID_TAG_PENETRATORMODIFIERS_CHILD):
            self.penetratorModifiers.add(EPenetratorModifier[lc.text])

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        e.attrib[self._XMLID_ATTR_LENGTH_ATTRIBUTE] = str(self.length)
        e.attrib[self._XMLID_ATTR_GIRTH_ATTRIBUTE] = str(self.girth)
        e.attrib[self._XMLID_ATTR_PENETRATORVIRGIN_ATTRIBUTE] = str(self.penetratorVirgin).lower()
        for lv in sorted(self.penetratorModifiers, key=lambda x: x.name):
            etree.SubElement(e, self._XMLID_TAG_PENETRATORMODIFIERS_CHILD, {}).text = lv.name
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['length'] = int(self.length)
        data['girth'] = int(self.girth)
        data['penetratorVirgin'] = bool(self.penetratorVirgin)
        if self.penetratorModifiers is None or len(self.penetratorModifiers) > 0:
            data['penetratorModifiers'] = list()
        else:
            lc = list()
            if len(self.penetratorModifiers) > 0:
                for lv in sorted(lc, lambda x: x.name):
                    lc.append(lv.name)
            data['penetratorModifiers'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'length' not in data:
            raise KeyRequiredException(self, data, 'length', 'length')
        self.length = int(data['length'])
        if 'girth' not in data:
            raise KeyRequiredException(self, data, 'girth', 'girth')
        self.girth = int(data['girth'])
        if 'penetratorVirgin' not in data:
            raise KeyRequiredException(self, data, 'penetratorVirgin', 'penetratorVirgin')
        self.penetratorVirgin = bool(data['penetratorVirgin'])
        if (lv := self.penetratorModifiers) is not None:
            for le in lv:
                self.penetratorModifiers.add(le.name)
        else:
            raise KeyRequiredException(self, data, 'penetratorModifiers', 'penetratorModifiers')

#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL, Savable
__all__ = ['RawScar']
## from [LT]/src/com/lilithsthrone/game/character/markings/Scar.java: public Element saveAsXML(Element parentElement, Document doc) { @ p8wThoTbOkeyu0vMVGpF/2QYYUbNkHHnGEAEXpC12yuJ5Wmc6sci1SzYz8qlZTpzEW9EKHucAjM3CHd0XYxsxg==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawScar(Savable):
    TAG = 'scar'
    _XMLID_ATTR_PLURAL_ATTRIBUTE: str = 'plural'
    _XMLID_ATTR_TYPE_ATTRIBUTE: str = 'type'

    def __init__(self) -> None:
        super().__init__()
        self.type: str = ''
        self.plural: bool = False

    def overrideAttrs(self, plural_attribute: _Optional_str = None, type_attribute: _Optional_str = None) -> None:
        if plural_attribute is not None:
            self._XMLID_ATTR_PLURAL_ATTRIBUTE = plural_attribute
        if type_attribute is not None:
            self._XMLID_ATTR_TYPE_ATTRIBUTE = type_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_TYPE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_TYPE_ATTRIBUTE, 'type')
        else:
            self.type = str(value)
        value = e.get(self._XMLID_ATTR_PLURAL_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_PLURAL_ATTRIBUTE, 'plural')
        else:
            self.plural = XML2BOOL[value]

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_TYPE_ATTRIBUTE] = str(self.type)
        e.attrib[self._XMLID_ATTR_PLURAL_ATTRIBUTE] = str(self.plural).lower()
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['type'] = self.type
        data['plural'] = bool(self.plural)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'type' not in data:
            raise KeyRequiredException(self, data, 'type', 'type')
        self.type = str(data['type'])
        if 'plural' not in data:
            raise KeyRequiredException(self, data, 'plural', 'plural')
        self.plural = bool(data['plural'])

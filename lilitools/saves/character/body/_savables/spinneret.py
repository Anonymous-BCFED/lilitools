#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.character.body.bodypart import BodyPart
__all__ = ['RawSpinneret']
## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public Element saveAsXML(Element parentElement, Document doc) { @ tXdWc3t8yE00YDC19flWbA+UmZMQJYMAJV3g5fn/WVE+BPjLWT/vzAiHRERWQ460QYrgA7tZQXtoP5fcY7cq5w==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawSpinneret(BodyPart):
    TAG = 'spinneret'

    def __init__(self) -> None:
        super().__init__()

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        return super().toXML(tagOverride)

    def toDict(self) -> Dict[str, Any]:
        return super().toDict()

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)

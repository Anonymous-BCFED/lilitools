#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.character.body.bodypart import BodyPart
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
__all__ = ['RawTail']
## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public Element saveAsXML(Element parentElement, Document doc) { @ tXdWc3t8yE00YDC19flWbA+UmZMQJYMAJV3g5fn/WVE+BPjLWT/vzAiHRERWQ460QYrgA7tZQXtoP5fcY7cq5w==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawTail(BodyPart):
    TAG = 'tail'
    _XMLID_ATTR_COUNT_ATTRIBUTE: str = 'count'
    _XMLID_ATTR_GIRTH_ATTRIBUTE: str = 'girth'
    _XMLID_ATTR_LENGTH_ATTRIBUTE: str = 'length'
    _XMLID_ATTR_TYPE_ATTRIBUTE: str = 'type'

    def __init__(self) -> None:
        super().__init__()
        self.type: str = ''
        self.count: int = 0
        self.girth: int = 0
        self.length: float = 0.

    def overrideAttrs(self, count_attribute: _Optional_str = None, girth_attribute: _Optional_str = None, length_attribute: _Optional_str = None, type_attribute: _Optional_str = None) -> None:
        if count_attribute is not None:
            self._XMLID_ATTR_COUNT_ATTRIBUTE = count_attribute
        if girth_attribute is not None:
            self._XMLID_ATTR_GIRTH_ATTRIBUTE = girth_attribute
        if length_attribute is not None:
            self._XMLID_ATTR_LENGTH_ATTRIBUTE = length_attribute
        if type_attribute is not None:
            self._XMLID_ATTR_TYPE_ATTRIBUTE = type_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_TYPE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_TYPE_ATTRIBUTE, 'type')
        else:
            self.type = str(value)
        value = e.get(self._XMLID_ATTR_COUNT_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_COUNT_ATTRIBUTE, 'count')
        else:
            self.count = int(value)
        value = e.get(self._XMLID_ATTR_GIRTH_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_GIRTH_ATTRIBUTE, 'girth')
        else:
            self.girth = int(value)
        value = e.get(self._XMLID_ATTR_LENGTH_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_LENGTH_ATTRIBUTE, 'length')
        else:
            self.length = float(value)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_TYPE_ATTRIBUTE] = str(self.type)
        e.attrib[self._XMLID_ATTR_COUNT_ATTRIBUTE] = str(self.count)
        e.attrib[self._XMLID_ATTR_GIRTH_ATTRIBUTE] = str(self.girth)
        e.attrib[self._XMLID_ATTR_LENGTH_ATTRIBUTE] = str(float(self.length))
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['type'] = self.type
        data['count'] = int(self.count)
        data['girth'] = int(self.girth)
        data['length'] = float(self.length)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'type' not in data:
            raise KeyRequiredException(self, data, 'type', 'type')
        self.type = str(data['type'])
        if 'count' not in data:
            raise KeyRequiredException(self, data, 'count', 'count')
        self.count = int(data['count'])
        if 'girth' not in data:
            raise KeyRequiredException(self, data, 'girth', 'girth')
        self.girth = int(data['girth'])
        if 'length' not in data:
            raise KeyRequiredException(self, data, 'length', 'length')
        self.length = float(data['length'])

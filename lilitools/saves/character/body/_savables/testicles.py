#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.character.body.bodypart import BodyPart
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL
__all__ = ['RawTesticles']
## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public Element saveAsXML(Element parentElement, Document doc) { @ tXdWc3t8yE00YDC19flWbA+UmZMQJYMAJV3g5fn/WVE+BPjLWT/vzAiHRERWQ460QYrgA7tZQXtoP5fcY7cq5w==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawTesticles(BodyPart):
    TAG = 'testicles'
    _XMLID_ATTR_CUMEXPULSION_ATTRIBUTE: str = 'cumExpulsion'
    _XMLID_ATTR_CUMREGENERATION_ATTRIBUTE: str = 'cumRegeneration'
    _XMLID_ATTR_CUMSTORAGE_ATTRIBUTE: str = 'cumStorage'
    _XMLID_ATTR_INTERNAL_ATTRIBUTE: str = 'internal'
    _XMLID_ATTR_NUMBEROFTESTICLES_ATTRIBUTE: str = 'numberOfTesticles'
    _XMLID_ATTR_STOREDCUM_ATTRIBUTE: str = 'storedCum'
    _XMLID_ATTR_TESTICLESIZE_ATTRIBUTE: str = 'testicleSize'

    def __init__(self) -> None:
        super().__init__()
        self.testicleSize: int = 0
        self.cumStorage: int = 0
        self.storedCum: float = 0.
        self.cumRegeneration: int = 0
        self.cumExpulsion: int = 0
        self.numberOfTesticles: int = 0
        self.internal: bool = False

    def overrideAttrs(self, cumExpulsion_attribute: _Optional_str = None, cumRegeneration_attribute: _Optional_str = None, cumStorage_attribute: _Optional_str = None, internal_attribute: _Optional_str = None, numberOfTesticles_attribute: _Optional_str = None, storedCum_attribute: _Optional_str = None, testicleSize_attribute: _Optional_str = None) -> None:
        if cumExpulsion_attribute is not None:
            self._XMLID_ATTR_CUMEXPULSION_ATTRIBUTE = cumExpulsion_attribute
        if cumRegeneration_attribute is not None:
            self._XMLID_ATTR_CUMREGENERATION_ATTRIBUTE = cumRegeneration_attribute
        if cumStorage_attribute is not None:
            self._XMLID_ATTR_CUMSTORAGE_ATTRIBUTE = cumStorage_attribute
        if internal_attribute is not None:
            self._XMLID_ATTR_INTERNAL_ATTRIBUTE = internal_attribute
        if numberOfTesticles_attribute is not None:
            self._XMLID_ATTR_NUMBEROFTESTICLES_ATTRIBUTE = numberOfTesticles_attribute
        if storedCum_attribute is not None:
            self._XMLID_ATTR_STOREDCUM_ATTRIBUTE = storedCum_attribute
        if testicleSize_attribute is not None:
            self._XMLID_ATTR_TESTICLESIZE_ATTRIBUTE = testicleSize_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_TESTICLESIZE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_TESTICLESIZE_ATTRIBUTE, 'testicleSize')
        else:
            self.testicleSize = int(value)
        value = e.get(self._XMLID_ATTR_CUMSTORAGE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_CUMSTORAGE_ATTRIBUTE, 'cumStorage')
        else:
            self.cumStorage = int(value)
        value = e.get(self._XMLID_ATTR_STOREDCUM_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_STOREDCUM_ATTRIBUTE, 'storedCum')
        else:
            self.storedCum = float(value)
        value = e.get(self._XMLID_ATTR_CUMREGENERATION_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_CUMREGENERATION_ATTRIBUTE, 'cumRegeneration')
        else:
            self.cumRegeneration = int(value)
        value = e.get(self._XMLID_ATTR_CUMEXPULSION_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_CUMEXPULSION_ATTRIBUTE, 'cumExpulsion')
        else:
            self.cumExpulsion = int(value)
        value = e.get(self._XMLID_ATTR_NUMBEROFTESTICLES_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_NUMBEROFTESTICLES_ATTRIBUTE, 'numberOfTesticles')
        else:
            self.numberOfTesticles = int(value)
        value = e.get(self._XMLID_ATTR_INTERNAL_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_INTERNAL_ATTRIBUTE, 'internal')
        else:
            self.internal = XML2BOOL[value]

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_TESTICLESIZE_ATTRIBUTE] = str(self.testicleSize)
        e.attrib[self._XMLID_ATTR_CUMSTORAGE_ATTRIBUTE] = str(self.cumStorage)
        e.attrib[self._XMLID_ATTR_STOREDCUM_ATTRIBUTE] = str(float(self.storedCum))
        e.attrib[self._XMLID_ATTR_CUMREGENERATION_ATTRIBUTE] = str(self.cumRegeneration)
        e.attrib[self._XMLID_ATTR_CUMEXPULSION_ATTRIBUTE] = str(self.cumExpulsion)
        e.attrib[self._XMLID_ATTR_NUMBEROFTESTICLES_ATTRIBUTE] = str(self.numberOfTesticles)
        e.attrib[self._XMLID_ATTR_INTERNAL_ATTRIBUTE] = str(self.internal).lower()
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['testicleSize'] = int(self.testicleSize)
        data['cumStorage'] = int(self.cumStorage)
        data['storedCum'] = float(self.storedCum)
        data['cumRegeneration'] = int(self.cumRegeneration)
        data['cumExpulsion'] = int(self.cumExpulsion)
        data['numberOfTesticles'] = int(self.numberOfTesticles)
        data['internal'] = bool(self.internal)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'testicleSize' not in data:
            raise KeyRequiredException(self, data, 'testicleSize', 'testicleSize')
        self.testicleSize = int(data['testicleSize'])
        if 'cumStorage' not in data:
            raise KeyRequiredException(self, data, 'cumStorage', 'cumStorage')
        self.cumStorage = int(data['cumStorage'])
        if 'storedCum' not in data:
            raise KeyRequiredException(self, data, 'storedCum', 'storedCum')
        self.storedCum = float(data['storedCum'])
        if 'cumRegeneration' not in data:
            raise KeyRequiredException(self, data, 'cumRegeneration', 'cumRegeneration')
        self.cumRegeneration = int(data['cumRegeneration'])
        if 'cumExpulsion' not in data:
            raise KeyRequiredException(self, data, 'cumExpulsion', 'cumExpulsion')
        self.cumExpulsion = int(data['cumExpulsion'])
        if 'numberOfTesticles' not in data:
            raise KeyRequiredException(self, data, 'numberOfTesticles', 'numberOfTesticles')
        self.numberOfTesticles = int(data['numberOfTesticles'])
        if 'internal' not in data:
            raise KeyRequiredException(self, data, 'internal', 'internal')
        self.internal = bool(data['internal'])

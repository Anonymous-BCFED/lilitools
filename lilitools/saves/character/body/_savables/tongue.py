#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional, Set
from lxml import etree
from lilitools.saves.character.body.bodypart import BodyPart
from lilitools.saves.character.body.enums.tongue_modifier import ETongueModifier
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL
__all__ = ['RawTongue']
## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public Element saveAsXML(Element parentElement, Document doc) { @ tXdWc3t8yE00YDC19flWbA+UmZMQJYMAJV3g5fn/WVE+BPjLWT/vzAiHRERWQ460QYrgA7tZQXtoP5fcY7cq5w==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawTongue(BodyPart):
    TAG = 'tongue'
    _XMLID_ATTR_LENGTH_ATTRIBUTE: str = 'tongueLength'
    _XMLID_ATTR_PIERCED_ATTRIBUTE: str = 'piercedTongue'
    _XMLID_TAG_MODIFIERS_CHILD: str = 'mod'

    def __init__(self) -> None:
        super().__init__()
        self.pierced: bool = False
        self.length: int = 0
        self.modifiers: Set[ETongueModifier] = set()

    def overrideAttrs(self, length_attribute: _Optional_str = None, pierced_attribute: _Optional_str = None) -> None:
        if length_attribute is not None:
            self._XMLID_ATTR_LENGTH_ATTRIBUTE = length_attribute
        if pierced_attribute is not None:
            self._XMLID_ATTR_PIERCED_ATTRIBUTE = pierced_attribute

    def overrideTags(self, modifiers_child: _Optional_str = None) -> None:
        if modifiers_child is not None:
            self._XMLID_TAG_MODIFIERS_CHILD = modifiers_child

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        value = e.get(self._XMLID_ATTR_PIERCED_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_PIERCED_ATTRIBUTE, 'pierced')
        else:
            self.pierced = XML2BOOL[value]
        value = e.get(self._XMLID_ATTR_LENGTH_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_LENGTH_ATTRIBUTE, 'length')
        else:
            self.length = int(value)
        for lc in e.iterfind(self._XMLID_TAG_MODIFIERS_CHILD):
            self.modifiers.add(ETongueModifier[lc.text])

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        e.attrib[self._XMLID_ATTR_PIERCED_ATTRIBUTE] = str(self.pierced).lower()
        e.attrib[self._XMLID_ATTR_LENGTH_ATTRIBUTE] = str(self.length)
        for lv in sorted(self.modifiers, key=lambda x: x.name):
            etree.SubElement(e, self._XMLID_TAG_MODIFIERS_CHILD, {}).text = lv.name
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['pierced'] = bool(self.pierced)
        data['length'] = int(self.length)
        if self.modifiers is None or len(self.modifiers) > 0:
            data['modifiers'] = list()
        else:
            lc = list()
            if len(self.modifiers) > 0:
                for lv in sorted(lc, lambda x: x.name):
                    lc.append(lv.name)
            data['modifiers'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'pierced' not in data:
            raise KeyRequiredException(self, data, 'pierced', 'pierced')
        self.pierced = bool(data['pierced'])
        if 'length' not in data:
            raise KeyRequiredException(self, data, 'length', 'length')
        self.length = int(data['length'])
        if (lv := self.modifiers) is not None:
            for le in lv:
                self.modifiers.add(le.name)
        else:
            raise KeyRequiredException(self, data, 'modifiers', 'modifiers')

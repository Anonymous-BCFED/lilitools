#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.character.body.bodypart import BodyPart
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL
__all__ = ['RawVagina']
## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public Element saveAsXML(Element parentElement, Document doc) { @ tXdWc3t8yE00YDC19flWbA+UmZMQJYMAJV3g5fn/WVE+BPjLWT/vzAiHRERWQ460QYrgA7tZQXtoP5fcY7cq5w==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawVagina(BodyPart):
    TAG = 'vagina'
    _XMLID_ATTR_EGGLAYER_ATTRIBUTE: str = 'eggLayer'
    _XMLID_ATTR_HYMEN_ATTRIBUTE: str = 'hymen'
    _XMLID_ATTR_LABIASIZE_ATTRIBUTE: str = 'labiaSize'
    _XMLID_ATTR_PIERCED_ATTRIBUTE: str = 'pierced'
    _XMLID_ATTR_SQUIRTER_ATTRIBUTE: str = 'squirter'
    _XMLID_ATTR_TYPE_ATTRIBUTE: str = 'type'

    def __init__(self) -> None:
        super().__init__()
        self.type: str = ''
        self.labiaSize: int = 0
        self.pierced: bool = False
        self.eggLayer: bool = False
        self.hymen: bool = False
        self.squirter: bool = False

    def overrideAttrs(self, eggLayer_attribute: _Optional_str = None, hymen_attribute: _Optional_str = None, labiaSize_attribute: _Optional_str = None, pierced_attribute: _Optional_str = None, squirter_attribute: _Optional_str = None, type_attribute: _Optional_str = None) -> None:
        if eggLayer_attribute is not None:
            self._XMLID_ATTR_EGGLAYER_ATTRIBUTE = eggLayer_attribute
        if hymen_attribute is not None:
            self._XMLID_ATTR_HYMEN_ATTRIBUTE = hymen_attribute
        if labiaSize_attribute is not None:
            self._XMLID_ATTR_LABIASIZE_ATTRIBUTE = labiaSize_attribute
        if pierced_attribute is not None:
            self._XMLID_ATTR_PIERCED_ATTRIBUTE = pierced_attribute
        if squirter_attribute is not None:
            self._XMLID_ATTR_SQUIRTER_ATTRIBUTE = squirter_attribute
        if type_attribute is not None:
            self._XMLID_ATTR_TYPE_ATTRIBUTE = type_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_TYPE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_TYPE_ATTRIBUTE, 'type')
        else:
            self.type = str(value)
        value = e.get(self._XMLID_ATTR_LABIASIZE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_LABIASIZE_ATTRIBUTE, 'labiaSize')
        else:
            self.labiaSize = int(value)
        value = e.get(self._XMLID_ATTR_PIERCED_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_PIERCED_ATTRIBUTE, 'pierced')
        else:
            self.pierced = XML2BOOL[value]
        value = e.get(self._XMLID_ATTR_EGGLAYER_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_EGGLAYER_ATTRIBUTE, 'eggLayer')
        else:
            self.eggLayer = XML2BOOL[value]
        value = e.get(self._XMLID_ATTR_HYMEN_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_HYMEN_ATTRIBUTE, 'hymen')
        else:
            self.hymen = XML2BOOL[value]
        value = e.get(self._XMLID_ATTR_SQUIRTER_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_SQUIRTER_ATTRIBUTE, 'squirter')
        else:
            self.squirter = XML2BOOL[value]

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_TYPE_ATTRIBUTE] = str(self.type)
        e.attrib[self._XMLID_ATTR_LABIASIZE_ATTRIBUTE] = str(self.labiaSize)
        e.attrib[self._XMLID_ATTR_PIERCED_ATTRIBUTE] = str(self.pierced).lower()
        e.attrib[self._XMLID_ATTR_EGGLAYER_ATTRIBUTE] = str(self.eggLayer).lower()
        e.attrib[self._XMLID_ATTR_HYMEN_ATTRIBUTE] = str(self.hymen).lower()
        e.attrib[self._XMLID_ATTR_SQUIRTER_ATTRIBUTE] = str(self.squirter).lower()
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['type'] = self.type
        data['labiaSize'] = int(self.labiaSize)
        data['pierced'] = bool(self.pierced)
        data['eggLayer'] = bool(self.eggLayer)
        data['hymen'] = bool(self.hymen)
        data['squirter'] = bool(self.squirter)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'type' not in data:
            raise KeyRequiredException(self, data, 'type', 'type')
        self.type = str(data['type'])
        if 'labiaSize' not in data:
            raise KeyRequiredException(self, data, 'labiaSize', 'labiaSize')
        self.labiaSize = int(data['labiaSize'])
        if 'pierced' not in data:
            raise KeyRequiredException(self, data, 'pierced', 'pierced')
        self.pierced = bool(data['pierced'])
        if 'eggLayer' not in data:
            raise KeyRequiredException(self, data, 'eggLayer', 'eggLayer')
        self.eggLayer = bool(data['eggLayer'])
        if 'hymen' not in data:
            raise KeyRequiredException(self, data, 'hymen', 'hymen')
        self.hymen = bool(data['hymen'])
        if 'squirter' not in data:
            raise KeyRequiredException(self, data, 'squirter', 'squirter')
        self.squirter = bool(data['squirter'])

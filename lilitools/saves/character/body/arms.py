from typing import TYPE_CHECKING, Optional, Set

from lilitools.saves.character.body._savables.arms import RawArms
from lilitools.saves.inventoryslot import EInventorySlot
from lilitools.saves.items.enums.item_tags import EItemTags
from lilitools.utils import anyMatchIn

if TYPE_CHECKING:
    from lilitools.saves.modfiles.bodyparttypes.arm_type import ArmType


class Arms(RawArms):
    ## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public AbstractRace getRaceFromPartWeighting(boolean ignoreOverride) { @ PE7oMjFVRwVpX6Pyn6xbqxI9RY5PJFGqku4Jhds+6hnzfRmxtpZSbEflmEKO7mpskcmbfbBubTEuMD5uOcRI+A==
    RACIAL_WEIGHT = 3

    def getRace(self) -> str:
        return self.getArmType().race

    def getArmType(self) -> Optional['ArmType']:
        from lilitools.saves.character.body.bodyparttypes.arms import ARM_TYPES

        # from lilitools.saves.modfiles.bodyparttypes.arm_type import ArmType
        return ARM_TYPES.get(self.type)

    def getBlockedInventorySlots(self, checkingAgainstItemTags: Optional[Set[str]] = None) -> Set[EInventorySlot]:
        itemtags: Set[str] = checkingAgainstItemTags or set()
        tags: Set[str] = set()
        armtype: Optional['ArmType']
        if (armtype := self.getArmType()) is not None:
            tags = armtype.tags
        ## from [LT]/src/com/lilithsthrone/game/character/body/abstractTypes/AbstractArmType.java: public BodyPartClothingBlock getBodyPartClothingBlock() { @ XPBqvn/YOhLZl9ErXTReg00lsvGWTKdCg4AGUNA97vS/7I9XET0COL0maJP+rrTVevbqC+8pbcTaAVWyL3Jc4g==

        # if(this.getTags().contains(BodyPartTag.ARM_WINGS_FEATHERED)) {
        #     return new BodyPartClothingBlock(
        #             Util.newArrayListOfValues(
        #                     InventorySlot.HAND,
        #                     InventorySlot.WRIST,
        #                     InventorySlot.TORSO_OVER,
        #                     InventorySlot.TORSO_UNDER),
        #             Race.HARPY,
        #             "Due to the fact that [npc.nameHasFull] bird-like wings instead of arms, only specialist clothing can be worn in this slot.",
        #             Util.newArrayListOfValues(
        #                 ItemTag.FITS_FEATHERED_ARM_WINGS,
        #                 ItemTag.FITS_FEATHERED_ARM_WINGS_EXCLUSIVE,
        #                 ItemTag.FITS_ARM_WINGS,
        #                 ItemTag.FITS_ARM_WINGS_EXCLUSIVE
        #             ));
        # }
        if 'ARM_WINGS_FEATHERED' in tags and not anyMatchIn(itemtags, {
                EItemTags.FITS_FEATHERED_ARM_WINGS.name,
                EItemTags.FITS_FEATHERED_ARM_WINGS_EXCLUSIVE.name,
                EItemTags.FITS_ARM_WINGS.name,
                EItemTags.FITS_ARM_WINGS_EXCLUSIVE.name,
        }):
            return {
                EInventorySlot.HAND,
                EInventorySlot.WRIST,
                EInventorySlot.TORSO_OVER,
                EInventorySlot.TORSO_UNDER,
            }
        # if(this.getTags().contains(BodyPartTag.ARM_WINGS_LEATHERY)) {
        #     return new BodyPartClothingBlock(
        #             Util.newArrayListOfValues(
        #                     InventorySlot.HAND,
        #                     InventorySlot.WRIST,
        #                     InventorySlot.TORSO_OVER,
        #                     InventorySlot.TORSO_UNDER),
        #             Race.BAT_MORPH,
        #             "Due to the fact that [npc.nameHasFull] leathery wings instead of arms, only specialist clothing can be worn in this slot.",
        #             Util.newArrayListOfValues(
        #                 ItemTag.FITS_LEATHERY_ARM_WINGS,
        #                 ItemTag.FITS_LEATHERY_ARM_WINGS_EXCLUSIVE,
        #                 ItemTag.FITS_ARM_WINGS,
        #                 ItemTag.FITS_ARM_WINGS_EXCLUSIVE
        #             ));
        # }
        if 'ARM_WINGS_LEATHERY' in tags and not anyMatchIn(itemtags, {
                EItemTags.FITS_LEATHERY_ARM_WINGS.name,
                EItemTags.FITS_LEATHERY_ARM_WINGS_EXCLUSIVE.name,
                EItemTags.FITS_ARM_WINGS.name,
                EItemTags.FITS_ARM_WINGS_EXCLUSIVE.name,
        }):
            return {
                EInventorySlot.HAND,
                EInventorySlot.WRIST,
                EInventorySlot.TORSO_OVER,
                EInventorySlot.TORSO_UNDER,
            }
        return set()

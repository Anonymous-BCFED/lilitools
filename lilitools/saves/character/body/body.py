from enum import Enum, auto
from typing import TYPE_CHECKING, List, Optional, Tuple

from lilitools.saves.character.body._savables.body import RawBody
from lilitools.saves.character.body.bodypart import BodyPart
from lilitools.saves.character.body.enums.cupsize import ECupSize
from lilitools.saves.character.body.racial_weight_map import RacialWeightMap
from lilitools.saves.character.enums.genital_arrangement import EGenitalArrangement
from lilitools.saves.character.enums.penetrator_modifier import EPenetratorModifier
from lilitools.saves.modfiles.character.race.abstract_race import AbstractRace
from lilitools.saves.modfiles.character.race.abstract_subspecies import AbstractSubspecies
from lilitools.saves.modfiles.character.race.races import Race

if TYPE_CHECKING:
    from lilitools.saves.character.game_character import GameCharacter


class ERaceStage(Enum):
    FERAL = auto()
    GREATER = auto()
    HUMAN = auto()
    LESSER = auto()


class Body(RawBody):
    def __init__(self) -> None:
        super().__init__()
        self._race: Optional[AbstractRace] = None
        self._halfDemonSubspecies: Optional[AbstractSubspecies] = None

    def hasAntennae(self) -> bool:
        ## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public boolean hasAntennae() { @ odOTR4I7IybUZWDaEQyt2wG66LUj5VCenHKKcClCzXV2Saa0TrHQWIhzVSePKwZXlibb8/OyqVWu6+zcXzRrlg==
        return self.antennae is not None and self.antennae.type != "NONE"

    def hasArms(self) -> bool:
        ## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public boolean hasArms() { @ Er9cMHc/q8SLprZO/FQIUO/baf2q7s3Dj/0FZa7AEanJQ5YU8WE02DDrodgK5qjsdlY5b7kOEMuElLnrrazCqA==
        return not self.core.feral

    def hasHorns(self) -> bool:
        ## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public boolean hasHorns() { @ dsMsPiw8uta0yLi7hp0VLY7RTr/POmlrRPjlHiEqhRHs9rwg9mqosyECTUl0HBD/hzpgCH1pdELL0HKho9uhlA==
        return self.horns is not None and self.horns.type != "NONE"

    def hasTail(self) -> bool:
        ## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public boolean hasTail() { @ LKZXwfdZKnXzXDP+1R9Gc053eoP8BN0+CI1YnQiVU4ejraz/widhgMnrrzIGv+ellX+5P4JtYGGtwEfTdkuLlw==
        return self.tail is not None and self.tail.type != "NONE"

    def hasTentacle(self) -> bool:
        ## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public boolean hasTentacle() { @ pOJQ7iYdVgAwth9Ehi77oKrHEMkTathDiJxBluqupqxvC+O8ifx3DzNk+5yITk+yFrMOyz9wbNzJf6a7H77V8Q==
        return self.tentacles is not None and self.tentacles.type != "NONE"

    def hasWings(self) -> bool:
        ## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public boolean hasWings() { @ 7P/gcp5EVInHbjqkO3kSwH0wv3/PepNnH08VEahHH06HVXJv+N20QSunaVMoIXD1zaQJcIrJPV4ruG6rCeJ2zQ==
        return self.wings is not None and self.wings.type != "NONE"

    def hasCrotchBreasts(self) -> bool:
        ## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public boolean hasBreastsCrotch() { @ lMV7mcqgFVMdxh4z/VxG8ZIB6xurJojZaFj0Whe7jS0hoc1yhY/0xL9/NkaBoE+yVFL+LIRJV8fsvRqnQFRyVw==
        return self.crotchBreasts is not None and self.crotchBreasts.type != "NONE"

    def hasVagina(self) -> bool:
        ## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public boolean hasVagina() { @ b4486CI9F42m+9qSfzyFY+DVwJixiRpqbxUQw9/NnJNbBuSDSVN5AvJw1M0rK9lYlgazkyX4lJ7Z4Ad3As0PXw==
        return self.vagina is not None and self.vagina.type != "NONE"

    def hasPenis(self) -> bool:
        ## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public boolean hasPenis() { @ yMdsvkaEsa7WqWcNApF4b/PiXRoVajHKa1a2cVzNNKq8xPYa5GQB8Om/XOMuq8V9proB2gc6U9/n+DcWgx5tYg==
        return self.penis is not None and self.penis.type != "NONE"

    def hasVisiblePenisBulge(self) -> bool:
        ## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public boolean isPenisBulgeVisible() { @ C8DaIF1PWFX3fqfh6ey9NzN58uaUotCwrsE74VTRuTf8/aYHd9TYc5/sDo5iJ210MQV5syAY8/12YDL22mELQg==
        # return hasPenis()
        # 		&& getGenitalArrangement()==GenitalArrangement.NORMAL
        # 		&& (hasPenisModifier(PenetrationModifier.SHEATHED)
        # 			? getPenisRawSizeValue()>=PenisLength.FOUR_HUGE.getMaximumValue()
        # 			: getPenisRawSizeValue()>=PenisLength.TWO_AVERAGE.getMaximumValue());
        return (
            self.hasPenis()
            and self.core.genitalArrangement == EGenitalArrangement.NORMAL.name
            and (
                (self.penis.penetrator.length >= 40)
                if (
                    EPenetratorModifier.SHEATHED
                    in self.penis.penetrator.penetratorModifiers
                )
                else (self.penis.penetrator.length >= 20)
            )
        )

    def hasVisibleTesticleBulge(self) -> bool:
        ## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public boolean isTesticleBulgeVisible() { @ knZy7KpiUscC6t6IfV4ijM1lULVHGRFssWqbV9ZNmcnqtEWqKkhAuDu56vwYfoA+9de1MWv++Y1vSMgMnXodBg==
        # return hasPenis()
        # 		&& getGenitalArrangement()==GenitalArrangement.NORMAL
        # 		&& !isInternalTesticles()
        # 		&& getTesticleSize().getValue()>=TesticleSize.FOUR_HUGE.getValue();
        return (
            self.hasPenis()
            and self.core.genitalArrangement == EGenitalArrangement.NORMAL.name
            and not self.testicles.internal
            and self.testicles.testicleSize >= 4
        )

    def isFeral(self) -> bool:
        return self.core.feral

    def hasBreasts(self) -> bool:
        ## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public boolean hasBreasts() { @ KhE20d+luqWmbPHdJSgBLHWHMeydElPDSjCHpKBxJrvXFefsFFrr7O828wgk45NIUXXPbp/pzH69FwxUsHL4Xg==
        return (
            not self.isFeral()
            and self.breasts is not None
            and self.breasts.size >= ECupSize.AA.measurement
        )

    def getAllBodyParts(self) -> List[BodyPart]:
        ## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public void handleAllBodyPartsList() { @ mZ4r0CrFWaQ4BBoC63nRYVUZMmsKr7XieU4ay2xjbbluh6+yJ0XLMyUaHie6wWYT2bXVI7tQ2xfBi6eJgnGXOQ==
        o: List[BodyPart] = []
        # allBodyParts.add(antenna);
        if self.antennae:
            o.append(self.antennae)
        # allBodyParts.add(arm);
        if self.arms:
            o.append(self.arms)
        # allBodyParts.add(ass);
        if self.ass:
            o.append(self.ass)
        # allBodyParts.add(breast);
        if self.breasts:
            o.append(self.breasts)
        # allBodyParts.add(breastCrotch);
        if self.crotchBreasts:
            o.append(self.crotchBreasts)
        # allBodyParts.add(face);
        if self.face:
            o.append(self.face)
        # allBodyParts.add(eye);
        if self.eyes:
            o.append(self.eyes)
        # allBodyParts.add(ear);
        if self.ears:
            o.append(self.ears)
        # allBodyParts.add(hair);
        if self.hair:
            o.append(self.hair)
        # allBodyParts.add(leg);
        if self.legs:
            o.append(self.legs)
        # allBodyParts.add(torso);
        if self.torso:
            o.append(self.torso)
        # allBodyParts.add(horn);
        if self.horns:
            o.append(self.horns)
        # allBodyParts.add(penis);
        if self.penis:
            o.append(self.penis)
        # allBodyParts.add(secondPenis);
        # if self.secondPenis:
        #     o.append(self.secondPenis)
        # allBodyParts.add(tail);
        if self.tail:
            o.append(self.tail)
        # allBodyParts.add(tentacle);
        if self.tentacles:
            o.append(self.tentacles)
        # allBodyParts.add(vagina);
        if self.vagina:
            o.append(self.vagina)
        # allBodyParts.add(wing);
        if self.wings:
            o.append(self.wings)
        return o

    def getRaceWeights(self) -> RacialWeightMap:
        m = RacialWeightMap()
        for bp in self.getAllBodyParts():
            if bp.RACIAL_WEIGHT > 0 and bp.getRace() is not None:
                m.add(bp.getRace(), bp.RACIAL_WEIGHT)
        return m

    def getRaceFromPartWeighting(self, demon_override: bool = False) -> str:
        ## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public AbstractRace getRaceFromPartWeighting(boolean ignoreOverride) { @ PE7oMjFVRwVpX6Pyn6xbqxI9RY5PJFGqku4Jhds+6hnzfRmxtpZSbEflmEKO7mpskcmbfbBubTEuMD5uOcRI+A==
        m = self.getRaceWeights()

        return m.demonOrMax() if demon_override else m.max()

    def getRaceStageFromPartWeighting(self) -> ERaceStage:
        if self.isFeral():
            return ERaceStage.FERAL
        m = self.getRaceWeights()
        race = m.demonOrMax()
        # if race is not None:
        #     weight = m.get(race)
        if len(m) == 1:
            if race == "HUMAN":
                return ERaceStage.HUMAN
            else:
                return ERaceStage.GREATER
        return ERaceStage.LESSER

    def getSubspeciesFromBody(self, race: AbstractRace) -> AbstractSubspecies:
        ## from [LT]/src/com/lilithsthrone/game/character/race/AbstractSubspecies.java: public static AbstractSubspecies getSubspeciesFromBody(Body body, AbstractRace race) { @ 6r7mpewWCSMyn9Eysmd/SL/7F26uVnqW7AfMeryuY3+B1n9hcPPtX/skLgiTN/NJieSfCVLCqZzzgd+n5Ga9ig==
        subspecies: AbstractSubspecies = None
        maxWeight: int = 0
        for sub in race.subspecies:
            w = sub.getWeighting(self, race)
            if w > maxWeight and (not self.isFeral() or sub.hasFeralPartsAvailable):
                subspecies = sub
                maxWeight = w

    def getRaceInfo(self) -> Tuple[str, ERaceStage]:
        race = self.getRaceFromPartWeighting()
        stage = self.getRaceStageFromPartWeighting()
        return race, stage

    def getRaceName(self) -> str:
        race, stage = self.getRaceInfo()
        if (r := Race.GetByID(race)) is not None:
            race = r.getName(self, self.isFeral())
        return f"{stage.name} {race}"
    
    def getSubspecies(self)->AbstractSubspecies:
        return self._subspecies

    def getHalfDemonSubspecies(self) -> AbstractSubspecies:
        if self._halfDemonSubspecies is None:
            self._halfDemonSubspecies = self.getSubspeciesFromBody(
                self.getRaceFromPartWeighting(demon_override=False)
            )
        return self._halfDemonSubspecies

    def calculateRace(self, char: 'GameCharacter') -> None:
        ## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public void calculateRace(GameCharacter target) { @ mPQIz2O8rocl3dScj6STkmMIgkaGQjFL00hMM2qaVJ5nq17ehX0yEnnIO7leKI1f4zt9JIl3G8qcWyJWVhgrnQ==
        self._race = Race.HUMAN
        self._raceStage = ERaceStage.HUMAN
        raceDetected: bool = False
        match self.core.bodyMaterial:
            case "SLIME":
                self._raceStage, self._race = ERaceStage.GREATER, Race.SLIME
                raceDetected = True
            case "SILICONE":
                self._raceStage, self._race = ERaceStage.GREATER, Race.DOLL
                raceDetected = True
        if char.isElemental():
            self._raceStage, self._race = ERaceStage.GREATER, Race.ELEMENTAL
            raceDetected = True
        if not raceDetected:
            self._race = self.getRaceFromPartWeighting()
            self._raceStage = self.getRaceStageFromPartWeighting()

        self._subspecies = self.getSubspeciesFromBody(self._race)

        # Reset
        self._halfDemonSubspecies = None

        if self._subspecies.getSubspeciesOverridePriority()>0 and (self.subspeciesOverride is None or self._subspecies.getSubspeciesOverridePriority()>=self.subspeciesOverride.getSubspeciesOverridePriority()):
            self._subspeciesOverride=self._subspecies

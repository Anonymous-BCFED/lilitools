from typing import Set

from lilitools.saves.inventoryslot import EInventorySlot
from lilitools.saves.savable import Savable


class BodyPart(Savable):
    RACIAL_WEIGHT: int = 0  # getRaceFromPartWeighting()

    def getRace(self) -> str:
        return None

    def getBlockedInventorySlots(self) -> Set[EInventorySlot]:
        return set()

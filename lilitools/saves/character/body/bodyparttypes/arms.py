from enum import Enum
from pathlib import Path
from typing import Any, Optional, cast
from lilitools.saves.character.body.bodyparttypes.mfebodyparttable import MFEBodyPartLookupTable
from lilitools.saves.character.body.enums.known_arm_types import EKnownArmTypes
from lilitools.saves.modfiles.bodyparttypes.arm_type import ArmType
__all__=['ARM_TYPES']

class _ArmTypesTable(MFEBodyPartLookupTable[ArmType]):
    ROOT_ELEMENT = 'arms'
    RES_SUB_DIR: str = ''
    # FILTER_FOLDER_NAME: Optional[str] = 'bodyParts'

    def __init__(self, mfe_builtins: type[Enum], resSubDir: Path | None = None, savable_type: type[ArmType] | None = None) -> None:
        super().__init__(mfe_builtins, resSubDir, savable_type)

    def handleBuiltinsEnumEntry(self, e_: Any) -> None:
        # print(repr(e_))
        e = cast(ArmType, e_.value)
        self.entByID[e.id.casefold()] = e


ARM_TYPES = _ArmTypesTable(EKnownArmTypes, Path('res') / 'race', ArmType)
# ARM_TYPES.load(EKnownArmTypes, )

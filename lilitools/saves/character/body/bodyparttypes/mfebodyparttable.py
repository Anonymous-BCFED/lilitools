from enum import Enum
from pathlib import Path
from typing import Optional, Type, TypeVar, cast
from lilitools.saves.modfiles.utils import (
    MFELookupTable,
    ModFileEntry,
    getExternalFileRecordsById,
    getExternalFilesById,
)

from lilitools.saves.modfiles.savable_object_definition import SavableObjectDefinition

from lilitools.logging import ClickWrap
from lxml import etree

log = ClickWrap()
T = TypeVar("T", bound=SavableObjectDefinition)


class MFEBodyPartLookupTable(MFELookupTable[T]):
    RES_SUB_DIR: str = "race"
    FILTER_FOLDER_NAME: Optional[str] = "bodyParts"
    ROOT_ELEMENT: str = ""

    def __init__(
        self,
        mfe_builtins: type[Enum],
        resSubDir: Path | None = None,
        savable_type: type[T] | None = None,
    ) -> None:
        super().__init__(mfe_builtins, resSubDir, savable_type)

    def add_mfe(self, mfe: ModFileEntry) -> None:
        self.mfesById[mfe.id] = mfe
        self.allMFEs.add(mfe)

    def load(
        self,
        mfe_builtins: Type[Enum],
        resSubDir: Path,
        modsDir: Path,
    ) -> None:
        # print(f'{self.__class__.__name__}.load({mfe_builtins}, {resSubDir}) called')
        mfe: ModFileEntry
        # for mfe in getModFileRecordsByID(modSubDir):
        #     self.mfesById[mfe.id]=mfe
        # print(resSubDir / self.RES_SUB_DIR)
        for mfe in getExternalFileRecordsById(
            resSubDir / self.RES_SUB_DIR, self.FILTER_FOLDER_NAME
        ):
            # print(mfe)
            if self.ROOT_ELEMENT == "":
                return self.add_mfe(mfe)
            else:
                try:
                    doc = etree.parse(
                        str(mfe.file), parser=etree.XMLParser(remove_comments=True)
                    )
                except Exception as e:
                    log.warn(f"XML parse error on {mfe.file}: {e}")
                    continue
                if doc.getroot().tag == self.ROOT_ELEMENT:
                    self.add_mfe(mfe)
        for m in cast(Type[Enum], mfe_builtins):
            self.handleBuiltinsEnumEntry(m)

from lilitools.saves.character.body._savables.breasts import RawBreasts


class Breasts(RawBreasts):
    ## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public AbstractRace getRaceFromPartWeighting(boolean ignoreOverride) { @ PE7oMjFVRwVpX6Pyn6xbqxI9RY5PJFGqku4Jhds+6hnzfRmxtpZSbEflmEKO7mpskcmbfbBubTEuMD5uOcRI+A==
    RACIAL_WEIGHT = 0

    MAX_MILK_PER_HOUR = 2500

    def getRace(self) -> str:
        return None

    def getLactationRegenPerBreastPerSecond(self) -> float:
        return self.milkRegeneration / (60 * 60 * 24)

    def getMilkVolumePerHour(self) -> int:
        return (self.getLactationRegenPerBreastPerSecond() * 60 * 60) * (self.rows * 2)

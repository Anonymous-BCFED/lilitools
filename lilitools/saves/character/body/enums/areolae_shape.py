from enum import IntEnum
__all__ = ['EAreolaeShape']


class EAreolaeShape(IntEnum):
    NORMAL = 0
    HEART = 1
    STAR = 2

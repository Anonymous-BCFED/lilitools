from enum import IntEnum
__all__ = ['EAssSize']


class EAssSize(IntEnum):
    ZERO_FLAT = 0
    ONE_TINY = 1
    TWO_SMALL = 2
    THREE_NORMAL = 3
    FOUR_LARGE = 4
    FIVE_HUGE = 5
    SIX_MASSIVE = 6
    SEVEN_GIGANTIC = 7

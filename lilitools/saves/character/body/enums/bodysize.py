from enum import Enum
from functools import lru_cache
from typing_extensions import deprecated
__all__ = ['EBodySize']


class EBodySize(Enum):
    ZERO_SKINNY = 'skinny', 0, 20
    ONE_SLENDER = 'slender', 20, 40
    TWO_AVERAGE = 'average', 40, 60
    THREE_LARGE = 'large', 60, 80
    FOUR_HUGE = 'huge', 80, 100

    def getName(self) ->str:
        return self.name_

    def getMinimumBodySize(self) ->int:
        return self.minimumBodySize

    def getMaximumBodySize(self) ->int:
        return self.maximumBodySize

    def __init__(self, name: str, minimumBodySize: int, maximumBodySize: int) ->None:
        self.name_: str = name
        self.minimumBodySize: int = minimumBodySize
        self.maximumBodySize: int = maximumBodySize

    @property
    @lru_cache
    def medianMuscleSize(self) ->float:
        return self.minimumBodySize + (self.maximumBodySize - self.minimumBodySize) * 2

    @deprecated('Use medianMuscleSize attribute instead')
    def getMedianValue(self) ->int:
        return self.minimumBodySize + (self.maximumBodySize - self.minimumBodySize) / 2

from enum import IntEnum
__all__ = ['ECapacity']


class ECapacity(IntEnum):
    ZERO_IMPENETRABLE = 0
    ONE_EXTREMELY_TIGHT = 1
    TWO_TIGHT = 2
    THREE_SLIGHTLY_LOOSE = 3
    FOUR_LOOSE = 4
    FIVE_ROOMY = 5
    SIX_STRETCHED_OPEN = 6
    SEVEN_GAPING = 7

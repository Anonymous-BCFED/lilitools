from enum import IntEnum
__all__ = ['EDepth']


class EDepth(IntEnum):
    ZERO_EXTREMELY_SHALLOW = 0
    ONE_SHALLOW = 1
    TWO_AVERAGE = 2
    THREE_SPACIOUS = 3
    FOUR_DEEP = 4
    FIVE_VERY_DEEP = 5
    SIX_CAVERNOUS = 6
    SEVEN_FATHOMLESS = 7

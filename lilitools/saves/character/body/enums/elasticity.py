from enum import IntEnum
__all__ = ['EElasticity']


class EElasticity(IntEnum):
    ZERO_UNYIELDING = 0
    ONE_RIGID = 1
    TWO_FIRM = 2
    THREE_FLEXIBLE = 3
    FOUR_LIMBER = 4
    FIVE_STRETCHY = 5
    SIX_SUPPLE = 6
    SEVEN_ELASTIC = 7

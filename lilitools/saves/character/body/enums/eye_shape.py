from enum import IntEnum
__all__ = ['EEyeShape']


class EEyeShape(IntEnum):
    ROUND = 0
    HORIZONTAL = 1
    VERTICAL = 2
    HEART = 3
    STAR = 4

from enum import Enum
from functools import lru_cache
from typing_extensions import deprecated
__all__ = ['EFemininity']


class EFemininity(Enum):
    MASCULINE_STRONG = 0, 19
    MASCULINE = 20, 39
    ANDROGYNOUS = 40, 59
    FEMININE = 60, 79
    FEMININE_STRONG = 80, 100

    def getMinimumFemininity(self) ->int:
        return self.minimumFemininity

    def getMaximumFemininity(self) ->int:
        return self.maximumFemininity

    def __init__(self, minimumFemininity: int, maximumFemininity: int) ->None:
        self.minimumFemininity: int = minimumFemininity
        self.maximumFemininity: int = maximumFemininity

    @property
    @lru_cache
    def medianFemininity(self) ->float:
        return self.minimumFemininity + (self.maximumFemininity - self.minimumFemininity) * 2

    @deprecated('Use medianFemininity attribute instead')
    def getMedianFemininity(self) ->int:
        return self.minimumFemininity + (self.maximumFemininity - self.minimumFemininity) / 2

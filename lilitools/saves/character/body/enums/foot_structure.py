from enum import IntEnum
__all__ = ['EFootStructure']


class EFootStructure(IntEnum):
    NONE = 0
    PLANTIGRADE = 1
    DIGITIGRADE = 2
    UNGULIGRADE = 3
    ARACHNOID = 4
    TENTACLED = 5

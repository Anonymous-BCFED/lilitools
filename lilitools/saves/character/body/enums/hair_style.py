from enum import IntEnum
__all__ = ['EHairStyle']


class EHairStyle(IntEnum):
    NONE = 0
    MESSY = 1
    LOOSE = 2
    CURLY = 3
    STRAIGHT = 4
    SLICKED_BACK = 5
    SIDECUT = 6
    MOHAWK = 7
    DREADLOCKS = 8
    AFRO = 9
    TOPKNOT = 10
    PIXIE = 11
    BUN = 12
    BOB_CUT = 13
    CHONMAGE = 14
    WAVY = 15
    PONYTAIL = 16
    LOW_PONYTAIL = 17
    TWIN_TAILS = 18
    SIDE_BRAIDS = 19
    CHIGNON = 20
    BRAIDED = 21
    TWIN_BRAIDS = 22
    CROWN_BRAID = 23
    DRILLS = 24
    HIME_CUT = 25
    BIRD_CAGE = 26

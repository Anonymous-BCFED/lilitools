from enum import Enum
from functools import lru_cache
from typing_extensions import deprecated
__all__ = ['EHairLength']


class EHairLength(Enum):
    ZERO_BALD = 'bald', 0, 1, False
    ONE_VERY_SHORT = 'very short', 1, 8, False
    TWO_SHORT = 'short', 8, 15, True
    THREE_SHOULDER_LENGTH = 'shoulder-length', 15, 30, True
    FOUR_MID_BACK = 'long', 30, 60, True
    FIVE_ABOVE_ASS = 'very long', 60, 100, True
    SIX_BELOW_ASS = 'incredibly long', 100, 180, True
    SEVEN_TO_FLOOR = 'floor-length', 180, 350, True

    def getDescriptor(self) ->str:
        return self.descriptor

    def getMinimumValue(self) ->int:
        return self.minimumValue

    def getMaximumValue(self) ->int:
        return self.maximumValue

    def getSuitableForPulling(self) ->bool:
        return self.suitableForPulling

    def __init__(self, descriptor: str, minimumValue: int, maximumValue: int, suitableForPulling: bool) ->None:
        self.descriptor: str = descriptor
        self.minimumValue: int = minimumValue
        self.maximumValue: int = maximumValue
        self.suitableForPulling: bool = suitableForPulling

    @property
    @lru_cache
    def medianValue(self) ->float:
        return self.minimumValue + (self.maximumValue - self.minimumValue) * 2

    @deprecated('Use medianValue attribute instead.')
    def getMedianValue(self) ->int:
        return self.minimumValue + (self.maximumValue - self.minimumValue) / 2

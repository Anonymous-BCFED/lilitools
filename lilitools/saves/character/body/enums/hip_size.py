from enum import IntEnum
__all__ = ['EHipSize']


class EHipSize(IntEnum):
    ZERO_NO_HIPS = 0
    ONE_VERY_NARROW = 1
    TWO_NARROW = 2
    THREE_GIRLY = 3
    FOUR_WOMANLY = 4
    FIVE_VERY_WIDE = 5
    SIX_EXTREMELY_WIDE = 6
    SEVEN_ABSURDLY_WIDE = 7

#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/gen_arm_type_enum.py   #
#               Built-in Arm Types              #
#################################################
from enum import Enum
from lilitools.saves.modfiles.bodyparttypes.arm_type import ArmType
__all__ = ["EKnownArmTypes"]


class EKnownArmTypes(Enum):
    ALLIGATOR_MORPH = ArmType("ALLIGATOR_MORPH", "arm", "arms", "ALLIGATOR_MORPH", "ALLIGATOR_SCALES", None, {"ARM_STANDARD"}, set())
    ANGEL = ArmType("ANGEL", "arm", "arms", "ANGEL", "HUMAN", None, {"ARM_STANDARD"}, {"isUnderarmHairAllowed"})
    BAT_MORPH = ArmType("BAT_MORPH", "wing", "wings", "BAT_MORPH", "BAT_SKIN", None, {"ARM_WINGS", "ARM_WINGS_LEATHERY"}, {"allowsFlight", "isUnderarmHairAllowed"})
    CAT_MORPH = ArmType("CAT_MORPH", "arm", "arms", "CAT_MORPH", "FELINE_FUR", None, {"ARM_STANDARD"}, {"isUnderarmHairAllowed"})
    charisma_spider_arm = ArmType("charisma_spider_arm", "arm", "arms", "charisma_spider", "charisma_spider_chitinSmooth", "spider", {"ARM_STANDARD"}, {"isFromExternalFile"})
    charisma_spider_armFluffy = ArmType(
        "charisma_spider_armFluffy", "arm", "arms", "charisma_spider", "charisma_spider_chitinFluffy", "furred spider", {"ARM_STANDARD"}, {"isFromExternalFile", "isUnderarmHairAllowed"}
    )
    COW_MORPH = ArmType("COW_MORPH", "arm", "arms", "COW_MORPH", "BOVINE_FUR", None, {"ARM_STANDARD"}, {"isUnderarmHairAllowed"})
    DEMON_COMMON = ArmType("DEMON_COMMON", "arm", "arms", "DEMON", "DEMON_COMMON", None, {"ARM_STANDARD"}, {"isUnderarmHairAllowed"})
    DOG_MORPH = ArmType("DOG_MORPH", "arm", "arms", "DOG_MORPH", "CANINE_FUR", None, {"ARM_STANDARD"}, {"isUnderarmHairAllowed"})
    dsg_bear_arm = ArmType("dsg_bear_arm", "arm", "arms", "dsg_bear", "dsg_bear_fur", "bear", {"ARM_STANDARD"}, {"isFromExternalFile", "isUnderarmHairAllowed"})
    dsg_dragon_arm = ArmType("dsg_dragon_arm", "arm", "arms", "dsg_dragon", "dsg_dragon_scales", "dragon", {"ARM_STANDARD"}, {"isFromExternalFile"})
    dsg_dragon_armWings = ArmType(
        "dsg_dragon_armWings", "wing", "wings", "dsg_dragon", "dsg_dragon_scales", "dragon winged", {"ARM_WINGS", "ARM_WINGS_LEATHERY"}, {"allowsFlight", "isFromExternalFile"}
    )
    dsg_ferret_arm = ArmType("dsg_ferret_arm", "arm", "arms", "dsg_ferret", "dsg_ferret_fur", "ferret", {"ARM_STANDARD"}, {"isFromExternalFile", "isUnderarmHairAllowed"})
    dsg_gryphon_arm = ArmType("dsg_gryphon_arm", "arm", "arms", "dsg_gryphon", "dsg_gryphon_feathers", "gryphon", {"ARM_STANDARD"}, {"isFromExternalFile", "isUnderarmHairAllowed"})
    dsg_otter_arm = ArmType("dsg_otter_arm", "arm", "arms", "dsg_otter", "dsg_otter_fur", "otter", {"ARM_STANDARD"}, {"isFromExternalFile", "isUnderarmHairAllowed"})
    dsg_raccoon_arm = ArmType("dsg_raccoon_arm", "arm", "arms", "dsg_raccoon", "dsg_raccoon_fur", "raccoon", {"ARM_STANDARD"}, {"isFromExternalFile", "isUnderarmHairAllowed"})
    dsg_shark_arm = ArmType("dsg_shark_arm", "arm", "arms", "dsg_shark", "dsg_shark_skin", "shark", {"ARM_STANDARD"}, {"isFromExternalFile"})
    dsg_shark_armFin = ArmType("dsg_shark_armFin", "arm", "arms", "dsg_shark", "dsg_shark_skin", "shark finned", {"ARM_STANDARD"}, {"isFromExternalFile"})
    FOX_MORPH = ArmType("FOX_MORPH", "arm", "arms", "FOX_MORPH", "FOX_FUR", None, {"ARM_STANDARD"}, {"isUnderarmHairAllowed"})
    HARPY = ArmType("HARPY", "wing", "wings", "HARPY", "FEATHERS", None, {"ARM_WINGS", "ARM_WINGS_FEATHERED"}, {"allowsFlight", "isUnderarmHairAllowed"})
    HORSE_MORPH = ArmType("HORSE_MORPH", "arm", "arms", "HORSE_MORPH", "HORSE_HAIR", None, {"ARM_STANDARD"}, {"isUnderarmHairAllowed"})
    HUMAN = ArmType("HUMAN", "arm", "arms", "HUMAN", "HUMAN", None, {"ARM_STANDARD"}, {"isUnderarmHairAllowed"})
    innoxia_badger_arm = ArmType("innoxia_badger_arm", "arm", "arms", "innoxia_badger", "innoxia_badger_fur", "badger", {"ARM_STANDARD"}, {"isFromExternalFile", "isUnderarmHairAllowed"})
    innoxia_deer_arm = ArmType("innoxia_deer_arm", "arm", "arms", "innoxia_deer", "innoxia_deer_fur", "deer", {"ARM_STANDARD"}, {"isFromExternalFile", "isUnderarmHairAllowed"})
    innoxia_goat_arm = ArmType("innoxia_goat_arm", "arm", "arms", "innoxia_goat", "innoxia_goat_fur", "goat", {"ARM_STANDARD"}, {"isFromExternalFile", "isUnderarmHairAllowed"})
    innoxia_hyena_arm = ArmType("innoxia_hyena_arm", "arm", "arms", "innoxia_hyena", "innoxia_hyena_fur", "hyena", {"ARM_STANDARD"}, {"isFromExternalFile", "isMod", "isUnderarmHairAllowed"})
    innoxia_mouse_arm = ArmType("innoxia_mouse_arm", "arm", "arms", "innoxia_mouse", "innoxia_mouse_fur", "mouse", {"ARM_STANDARD"}, {"isFromExternalFile", "isUnderarmHairAllowed"})
    innoxia_panther_arm = ArmType("innoxia_panther_arm", "arm", "arms", "innoxia_panther", "innoxia_panther_fur", "panther", {"ARM_STANDARD"}, {"isFromExternalFile", "isUnderarmHairAllowed"})
    innoxia_pig_arm = ArmType("innoxia_pig_arm", "arm", "arms", "innoxia_pig", "innoxia_pig_fur", "pig", {"ARM_STANDARD"}, {"isFromExternalFile", "isUnderarmHairAllowed"})
    innoxia_raptor_arm = ArmType(
        "innoxia_raptor_arm",
        "wing",
        "wings",
        "innoxia_raptor",
        "innoxia_raptor_feathers",
        "raptor",
        {"ARM_WINGS", "ARM_WINGS_FEATHERED"},
        {"allowsFlight", "isFromExternalFile", "isUnderarmHairAllowed"},
    )
    innoxia_raptor_armTalons = ArmType(
        "innoxia_raptor_armTalons", "arm", "arms", "innoxia_raptor", "innoxia_raptor_feathers", "raptor taloned", {"ARM_STANDARD"}, {"isFromExternalFile", "isUnderarmHairAllowed"}
    )
    innoxia_sheep_arm = ArmType("innoxia_sheep_arm", "arm", "arms", "innoxia_sheep", "innoxia_sheep_wool", "sheep", {"ARM_STANDARD"}, {"isFromExternalFile", "isUnderarmHairAllowed"})
    mintychip_salamander_arm = ArmType("mintychip_salamander_arm", "arm", "arms", "mintychip_salamander", "mintychip_salamander_scales", "salamander", {"ARM_STANDARD"}, {"isFromExternalFile"})
    NoStepOnSnek_capybara_arm = ArmType(
        "NoStepOnSnek_capybara_arm", "arm", "arms", "NoStepOnSnek_capybara", "NoStepOnSnek_capybara_fur", "capybara", {"ARM_STANDARD"}, {"isFromExternalFile", "isUnderarmHairAllowed"}
    )
    NoStepOnSnek_octopus_arm = ArmType("NoStepOnSnek_octopus_arm", "arm", "arms", "NoStepOnSnek_octopus", "OCTOPUS_SKIN", "octopus", {"ARM_STANDARD"}, {"isFromExternalFile"})
    NoStepOnSnek_snake_arm = ArmType("NoStepOnSnek_snake_arm", "arm", "arms", "NoStepOnSnek_snake", "SNAKE_SCALES", "snake", {"ARM_STANDARD"}, {"isFromExternalFile"})
    RABBIT_MORPH = ArmType("RABBIT_MORPH", "arm", "arms", "RABBIT_MORPH", "RABBIT_FUR", None, {"ARM_STANDARD"}, {"isUnderarmHairAllowed"})
    RAT_MORPH = ArmType("RAT_MORPH", "arm", "arms", "RAT_MORPH", "RAT_FUR", None, {"ARM_STANDARD"}, {"isUnderarmHairAllowed"})
    REINDEER_MORPH = ArmType("REINDEER_MORPH", "arm", "arms", "REINDEER_MORPH", "REINDEER_FUR", None, {"ARM_STANDARD"}, {"isUnderarmHairAllowed"})
    SQUIRREL_MORPH = ArmType("SQUIRREL_MORPH", "arm", "arms", "SQUIRREL_MORPH", "SQUIRREL_FUR", None, {"ARM_STANDARD"}, {"isUnderarmHairAllowed"})
    WOLF_MORPH = ArmType("WOLF_MORPH", "arm", "arms", "WOLF_MORPH", "LYCAN_FUR", None, {"ARM_STANDARD"}, {"isUnderarmHairAllowed"})

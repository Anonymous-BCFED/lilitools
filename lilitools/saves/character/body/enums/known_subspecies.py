#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#  BY devtools/analysis/gen_subspecies_enum.py  #
#           Built-in Subspecies Types           #
#################################################
from enum import Enum
__all__ = ["EKnownSubspecies"]


class EKnownSubspecies(Enum):
    ALITAUR = "ALITAUR", 100000
    ALLIGATOR_MORPH = "ALLIGATOR_MORPH", 10000
    ANGEL = "ANGEL", 80000
    BAT_MORPH = "BAT_MORPH", 10000
    CAT_MORPH = "CAT_MORPH", 12000
    CAT_MORPH_CARACAL = "CAT_MORPH_CARACAL", 12000
    CAT_MORPH_CHEETAH = "CAT_MORPH_CHEETAH", 16000
    CAT_MORPH_LYNX = "CAT_MORPH_LYNX", 12000
    CENTAUR = "CENTAUR", 25000
    charisma_spider_subspecies_spider = "charisma_spider_subspecies_spider", 10000
    charisma_spider_subspecies_tarantula = "charisma_spider_subspecies_tarantula", 10000
    COW_MORPH = "COW_MORPH", 15000
    DEMON = "DEMON", 120000
    DOG_MORPH = "DOG_MORPH", 12000
    DOG_MORPH_BORDER_COLLIE = "DOG_MORPH_BORDER_COLLIE", 24000
    DOG_MORPH_DOBERMANN = "DOG_MORPH_DOBERMANN", 18000
    DOG_MORPH_GERMAN_SHEPHERD = "DOG_MORPH_GERMAN_SHEPHERD", 18000
    DOLL = "DOLL", 10000
    dsg_bear_subspecies_bear = "dsg_bear_subspecies_bear", 10000
    dsg_bear_subspecies_panda = "dsg_bear_subspecies_panda", 11000
    dsg_dog_subspecies_samoyed = "dsg_dog_subspecies_samoyed", 12000
    dsg_dragon_subspecies_coatl = "dsg_dragon_subspecies_coatl", 250000
    dsg_dragon_subspecies_dragon = "dsg_dragon_subspecies_dragon", 250000
    dsg_dragon_subspecies_drake = "dsg_dragon_subspecies_drake", 250000
    dsg_dragon_subspecies_ryu = "dsg_dragon_subspecies_ryu", 250000
    dsg_dragon_subspecies_wyvern = "dsg_dragon_subspecies_wyvern", 250000
    dsg_ferret_subspecies_ferret = "dsg_ferret_subspecies_ferret", 10000
    dsg_gryphon_subspecies_gryphon = "dsg_gryphon_subspecies_gryphon", 90000
    dsg_otter_subspecies_otter = "dsg_otter_subspecies_otter", 10000
    dsg_raccoon_subspecies_raccoon = "dsg_raccoon_subspecies_raccoon", 10000
    dsg_shark_subspecies_shark = "dsg_shark_subspecies_shark", 10000
    ELDER_LILIN = "ELDER_LILIN", 1000000000
    ELEMENTAL_AIR = "ELEMENTAL_AIR", 100000
    ELEMENTAL_ARCANE = "ELEMENTAL_ARCANE", 100000
    ELEMENTAL_EARTH = "ELEMENTAL_EARTH", 100000
    ELEMENTAL_FIRE = "ELEMENTAL_FIRE", 100000
    ELEMENTAL_WATER = "ELEMENTAL_WATER", 100000
    FOX_ASCENDANT = "FOX_ASCENDANT", 15000
    FOX_ASCENDANT_ARCTIC = "FOX_ASCENDANT_ARCTIC", 15000
    FOX_ASCENDANT_FENNEC = "FOX_ASCENDANT_FENNEC", 15000
    FOX_MORPH = "FOX_MORPH", 16000
    FOX_MORPH_ARCTIC = "FOX_MORPH_ARCTIC", 20000
    FOX_MORPH_FENNEC = "FOX_MORPH_FENNEC", 16000
    HALF_DEMON = "HALF_DEMON", 50000
    HARPY = "HARPY", 12000
    HARPY_PHOENIX = "HARPY_PHOENIX", 50000
    HARPY_RAVEN = "HARPY_RAVEN", 14000
    HARPY_SWAN = "HARPY_SWAN", 22000
    HORSE_MORPH = "HORSE_MORPH", 18000
    HORSE_MORPH_ALICORN = "HORSE_MORPH_ALICORN", 60000
    HORSE_MORPH_DONKEY = "HORSE_MORPH_DONKEY", 12000
    HORSE_MORPH_PEGASUS = "HORSE_MORPH_PEGASUS", 24000
    HORSE_MORPH_UNICORN = "HORSE_MORPH_UNICORN", 30000
    HORSE_MORPH_ZEBRA = "HORSE_MORPH_ZEBRA", 18000
    HUMAN = "HUMAN", 4000
    IMP = "IMP", 1000
    IMP_ALPHA = "IMP_ALPHA", 1000
    innoxia_badger_subspecies_badger = "innoxia_badger_subspecies_badger", 12000
    innoxia_cat_subspecies_cougar = "innoxia_cat_subspecies_cougar", 18000
    innoxia_deer_subspecies_deer = "innoxia_deer_subspecies_deer", 12000
    innoxia_dog_subspecies_dalmatian = "innoxia_dog_subspecies_dalmatian", 20000
    innoxia_goat_subspecies_goat = "innoxia_goat_subspecies_goat", 12000
    innoxia_hyena_subspecies_spotted = "innoxia_hyena_subspecies_spotted", 10000
    innoxia_hyena_subspecies_striped = "innoxia_hyena_subspecies_striped", 10000
    innoxia_mouse_subspecies_mouse = "innoxia_mouse_subspecies_mouse", 12000
    innoxia_panther_subspecies_leopard = "innoxia_panther_subspecies_leopard", 16000
    innoxia_panther_subspecies_lion = "innoxia_panther_subspecies_lion", 24000
    innoxia_panther_subspecies_snow_leopard = "innoxia_panther_subspecies_snow_leopard", 40000
    innoxia_panther_subspecies_tiger = "innoxia_panther_subspecies_tiger", 30000
    innoxia_pig_subspecies_pig = "innoxia_pig_subspecies_pig", 12000
    innoxia_raptor_subspecies_bald_eagle = "innoxia_raptor_subspecies_bald_eagle", 18000
    innoxia_raptor_subspecies_eagle = "innoxia_raptor_subspecies_eagle", 18000
    innoxia_raptor_subspecies_falcon = "innoxia_raptor_subspecies_falcon", 14000
    innoxia_raptor_subspecies_owl = "innoxia_raptor_subspecies_owl", 18000
    innoxia_sheep_subspecies_sheep = "innoxia_sheep_subspecies_sheep", 12000
    innoxia_wolf_subspecies_arctic = "innoxia_wolf_subspecies_arctic", 15000
    LILIN = "LILIN", 500000000
    mintychip_salamander_subspecies_flame = "mintychip_salamander_subspecies_flame", 40000
    mintychip_salamander_subspecies_frost = "mintychip_salamander_subspecies_frost", 40000
    NoStepOnSnek_capybara_subspecies_capybara = "NoStepOnSnek_capybara_subspecies_capybara", 10000
    NoStepOnSnek_octopus_subspecies_octopus = "NoStepOnSnek_octopus_subspecies_octopus", 10000
    NoStepOnSnek_snake_subspecies_lamia = "NoStepOnSnek_snake_subspecies_lamia", 10000
    NoStepOnSnek_snake_subspecies_melusine = "NoStepOnSnek_snake_subspecies_melusine", 10000
    NoStepOnSnek_snake_subspecies_snake = "NoStepOnSnek_snake_subspecies_snake", 10000
    PEGATAUR = "PEGATAUR", 35000
    RABBIT_MORPH = "RABBIT_MORPH", 12000
    RABBIT_MORPH_LOP = "RABBIT_MORPH_LOP", 12000
    RAT_MORPH = "RAT_MORPH", 6000
    REINDEER_MORPH = "REINDEER_MORPH", 18000
    SLIME = "SLIME", 10000
    SQUIRREL_MORPH = "SQUIRREL_MORPH", 6000
    UNITAUR = "UNITAUR", 50000
    WOLF_MORPH = "WOLF_MORPH", 10000

    def getId(self) -> str:
        return self.id

    def getBaseSlaveValue(self) -> str:
        return self.baseSlaveValue

    def __init__(self, id: str, baseSlaveValue: str) -> None:
        self.id: str = id
        self.baseSlaveValue: str = baseSlaveValue

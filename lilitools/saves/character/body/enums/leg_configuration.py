from enum import Enum
__all__ = ['ELegConfiguration']


class ELegConfiguration(Enum):
    BIPEDAL = 'bipedal', 0, 0, True, True, False, False, False, 2
    QUADRUPEDAL = 'quadrupedal', -50, 0, False, False, True, True, True, 4
    TAIL_LONG = 'serpent-tailed', 0, 0, True, True, False, False, False, 0
    TAIL = 'mer-tailed', 25, -95, True, True, False, False, False, 0
    ARACHNID = 'arachnid', -25, 100, False, True, True, True, True, 8
    CEPHALOPOD = 'cephalopod', 50, -75, True, True, False, False, False, 8
    AVIAN = 'avian', 0, 0, False, True, True, True, True, 2
    WINGED_BIPED = 'winged-biped', 0, 0, True, True, False, False, False, 2

    def getIdentifier(self) ->str:
        return self.identifier

    def getLandSpeedModifier(self) ->int:
        return self.landSpeedModifier

    def getWaterSpeedModifier(self) ->int:
        return self.waterSpeedModifier

    def getBipedalPositionedGenitals(self) ->bool:
        return self.bipedalPositionedGenitals

    def getBipedalPositionedCrotchBoobs(self) ->bool:
        return self.bipedalPositionedCrotchBoobs

    def getLargeGenitals(self) ->bool:
        return self.largeGenitals

    def getTall(self) ->bool:
        return self.tall

    def getWingsOnLegConfiguration(self) ->bool:
        return self.wingsOnLegConfiguration

    def getNumberOfLegs(self) ->int:
        return self.numberOfLegs

    def __init__(self, identifier: str, landSpeedModifier: int, waterSpeedModifier: int, bipedalPositionedGenitals: bool, bipedalPositionedCrotchBoobs: bool, largeGenitals: bool, tall: bool, wingsOnLegConfiguration: bool, numberOfLegs: int) ->None:
        self.identifier: str = identifier
        self.landSpeedModifier: int = landSpeedModifier
        self.waterSpeedModifier: int = waterSpeedModifier
        self.bipedalPositionedGenitals: bool = bipedalPositionedGenitals
        self.bipedalPositionedCrotchBoobs: bool = bipedalPositionedCrotchBoobs
        self.largeGenitals: bool = largeGenitals
        self.tall: bool = tall
        self.wingsOnLegConfiguration: bool = wingsOnLegConfiguration
        self.numberOfLegs: int = numberOfLegs

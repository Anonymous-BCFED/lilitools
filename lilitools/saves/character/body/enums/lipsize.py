from enum import Enum
__all__ = ['ELipSize']


class ELipSize(Enum):
    ZERO_THIN = 0, 'thin', False
    ONE_AVERAGE = 1, 'average-sized', False
    TWO_FULL = 2, 'full', False
    THREE_PLUMP = 3, 'plump', False
    FOUR_HUGE = 4, 'huge', False
    FIVE_MASSIVE = 5, 'massive', False
    SIX_GIGANTIC = 6, 'gigantic', True
    SEVEN_ABSURD = 7, 'absurdly colossal', True

    def getValue_(self) ->int:
        return self.value_

    def getDescriptor(self) ->str:
        return self.descriptor

    def getImpedesSpeech(self) ->bool:
        return self.impedesSpeech

    def __init__(self, value_: int, descriptor: str, impedesSpeech: bool) ->None:
        self.value_: int = value_
        self.descriptor: str = descriptor
        self.impedesSpeech: bool = impedesSpeech

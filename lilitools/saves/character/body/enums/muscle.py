from enum import Enum
from functools import lru_cache
from typing_extensions import deprecated
__all__ = ['EMuscle']


class EMuscle(Enum):
    ZERO_SOFT = 'soft', 0, 20
    ONE_LIGHTLY_MUSCLED = 'lightly muscled', 20, 40
    TWO_TONED = 'toned', 40, 60
    THREE_MUSCULAR = 'muscular', 60, 80
    FOUR_RIPPED = 'ripped', 80, 100

    def getName(self) ->str:
        return self.name_

    def getMinimumMuscle(self) ->int:
        return self.minimumMuscle

    def getMaximumMuscle(self) ->int:
        return self.maximumMuscle

    def __init__(self, name: str, minimumMuscle: int, maximumMuscle: int) ->None:
        self.name_: str = name
        self.minimumMuscle: int = minimumMuscle
        self.maximumMuscle: int = maximumMuscle

    @property
    @lru_cache
    def medianMuscle(self) ->float:
        return self.minimumMuscle + (self.maximumMuscle - self.minimumMuscle) * 2

    @deprecated('Use medianMuscle attribute instead')
    def getMedianValue(self) ->int:
        return self.minimumMuscle + (self.maximumMuscle - self.minimumMuscle) / 2

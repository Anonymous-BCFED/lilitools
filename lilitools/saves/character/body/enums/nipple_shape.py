from enum import IntEnum
__all__ = ['ENippleShape']


class ENippleShape(IntEnum):
    NORMAL = 0
    INVERTED = 1
    VAGINA = 2
    LIPS = 3

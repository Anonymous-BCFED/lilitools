from enum import IntEnum
__all__ = ['EPenetratorGirth']


class EPenetratorGirth(IntEnum):
    ZERO_THIN = 0
    ONE_SLENDER = 1
    TWO_NARROW = 2
    THREE_AVERAGE = 3
    FOUR_GIRTHY = 4
    FIVE_THICK = 5
    SIX_CHUBBY = 6
    SEVEN_FAT = 7

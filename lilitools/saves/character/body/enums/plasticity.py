from enum import IntEnum
__all__ = ['EPlasticity']


class EPlasticity(IntEnum):
    ZERO_RUBBERY = 0
    ONE_SPRINGY = 1
    TWO_TENSILE = 2
    THREE_RESILIENT = 3
    FOUR_ACCOMMODATING = 4
    FIVE_YIELDING = 5
    SIX_MALLEABLE = 6
    SEVEN_MOULDABLE = 7

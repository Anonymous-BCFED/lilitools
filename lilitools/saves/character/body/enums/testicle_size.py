from enum import IntEnum
__all__ = ['ETesticleSize']


class ETesticleSize(IntEnum):
    ZERO_VESTIGIAL = 0
    ONE_TINY = 1
    TWO_AVERAGE = 2
    THREE_LARGE = 3
    FOUR_HUGE = 4
    FIVE_MASSIVE = 5
    SIX_GIGANTIC = 6
    SEVEN_ABSURD = 7

from enum import IntEnum
__all__ = ['ETongueModifier']


class ETongueModifier(IntEnum):
    RIBBED = 0
    TENTACLED = 1
    BIFURCATED = 2
    WIDE = 3
    FLAT = 4
    STRONG = 5
    TAPERED = 6

from enum import IntEnum
__all__ = ['EWetness']


class EWetness(IntEnum):
    ZERO_DRY = 0
    ONE_SLIGHTLY_MOIST = 1
    TWO_MOIST = 2
    THREE_WET = 3
    FOUR_SLIMY = 4
    FIVE_SLOPPY = 5
    SIX_SOPPING_WET = 6
    SEVEN_DROOLING = 7

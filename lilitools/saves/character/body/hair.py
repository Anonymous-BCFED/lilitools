from lilitools.saves.character.body._savables.hair import RawHair


class Hair(RawHair):
    ## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public AbstractRace getRaceFromPartWeighting(boolean ignoreOverride) { @ PE7oMjFVRwVpX6Pyn6xbqxI9RY5PJFGqku4Jhds+6hnzfRmxtpZSbEflmEKO7mpskcmbfbBubTEuMD5uOcRI+A==
    RACIAL_WEIGHT = 1

    def getRace(self) -> str:
        return None

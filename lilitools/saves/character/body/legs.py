from lilitools.saves.character.body._savables.legs import RawLegs


class Legs(RawLegs):
    ## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public AbstractRace getRaceFromPartWeighting(boolean ignoreOverride) { @ PE7oMjFVRwVpX6Pyn6xbqxI9RY5PJFGqku4Jhds+6hnzfRmxtpZSbEflmEKO7mpskcmbfbBubTEuMD5uOcRI+A==
    RACIAL_WEIGHT = 3

    def getRace(self) -> str:
        return None

from typing import Optional

from lxml import etree

from lilitools.saves.character.body._savables.mouth import RawMouth
from lilitools.saves.character.body.orifice import Orifice
from lilitools.xmlutils import mergetags


class Mouth(RawMouth):
    ## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public AbstractRace getRaceFromPartWeighting(boolean ignoreOverride) { @ PE7oMjFVRwVpX6Pyn6xbqxI9RY5PJFGqku4Jhds+6hnzfRmxtpZSbEflmEKO7mpskcmbfbBubTEuMD5uOcRI+A==
    RACIAL_WEIGHT = 0

    def getRace(self) -> str:
        return None

    def __init__(self) -> None:
        super().__init__()
        self.orifice = Orifice()

    def fromXML(self, e: etree._Element) -> None:
        super().fromXML(e)
        self.orifice.fromXML(e)

    def toXML(self, tagOverride: Optional[str] = None) -> etree._Element:
        e = super().toXML(tagOverride)
        mergetags(e, self.orifice.toXML())
        return e

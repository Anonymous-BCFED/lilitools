from typing import Optional

from lxml import etree

from lilitools.saves.character.body._savables.nipples import RawNipples
from lilitools.saves.character.body.orifice import Orifice
from lilitools.xmlutils import mergetags


class Nipples(RawNipples):
    ## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public AbstractRace getRaceFromPartWeighting(boolean ignoreOverride) { @ PE7oMjFVRwVpX6Pyn6xbqxI9RY5PJFGqku4Jhds+6hnzfRmxtpZSbEflmEKO7mpskcmbfbBubTEuMD5uOcRI+A==
    RACIAL_WEIGHT = 1

    def getRace(self) -> str:
        return None

    def __init__(self) -> None:
        super().__init__()
        self.orifice = Orifice()

    def fromXML(self, e: etree._Element) -> None:
        # HACK
        e.attrib['wetness'] = '0'
        self.orifice.fromXML(e)
        super().fromXML(e)

    def toXML(self, tagOverride: Optional[str] = None) -> etree._Element:
        e = super().toXML(tagOverride=tagOverride)
        mergetags(e, self.orifice.toXML())
        # HACK
        if 'wetness' in e.attrib:
            del e.attrib['wetness']
        return e

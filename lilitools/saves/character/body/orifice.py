from lxml import etree

from lilitools.saves.character.body._savables.orifice import RawOrifice


class Orifice(RawOrifice):
    def __init__(self, modTagName: str='mod', virginityAttrName: str='virgin') -> None:
        super().__init__()
        self.overrideAttrs(orificeVirgin_attribute=virginityAttrName)
        self.overrideTags(orificeModifiers_child=modTagName)

    def fromXML(self, e: etree._Element) -> None:
        assert (
            self._XMLID_ATTR_ORIFICEVIRGIN_ATTRIBUTE != "ORIFICE_VIRGINITY_ATTRIBUTE"
        ), "Please call self.setupOrificeVirginityAttribute()"
        # assert self._XMLID_TAG_ORIFICEMODIFIERS_PARENT != 'ORIFICE_MODIFIERS_PARENT', 'Please call self.setupOrificeModifiersTag()'
        return super().fromXML(e)

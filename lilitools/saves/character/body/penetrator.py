from lxml import etree

from lilitools.saves.character.body._savables.penetrator import RawPenetrator


class Penetrator(RawPenetrator):
    def __init__(self, modTagName: str='mod', virginityAttrName: str='virgin') -> None:
        super().__init__()
        self.overrideTags(penetratorModifiers_child=modTagName)
        self.overrideAttrs(penetratorVirgin_attribute=virginityAttrName)
        # self.overrideTags(penetratorModifiers_parent=modifiersTagName)

    def fromXML(self, e: etree._Element) -> None:
        # assert self._XMLID_ATTR_PENETRATORVIRGIN_ATTRIBUTE != 'PENETRATOR_VIRGINITY_ATTRIBUTE', 'Please call self.setupPenetratorVirginityAttribute()'
        # assert self._XMLID_TAG_PENETRATORMODIFIERS_PARENT != 'PENETRATOR_MODIFIERS_PARENT', 'Please call self.setupPenetratorModifiersTag()'
        return super().fromXML(e)

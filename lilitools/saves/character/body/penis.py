from typing import Optional

from lxml import etree

from lilitools.saves.character.body._savables.penis import RawPenis
from lilitools.saves.character.body.orifice import Orifice
from lilitools.saves.character.body.penetrator import Penetrator
from lilitools.xmlutils import mergetags


class Penis(RawPenis):
    ## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public AbstractRace getRaceFromPartWeighting(boolean ignoreOverride) { @ PE7oMjFVRwVpX6Pyn6xbqxI9RY5PJFGqku4Jhds+6hnzfRmxtpZSbEflmEKO7mpskcmbfbBubTEuMD5uOcRI+A==
    RACIAL_WEIGHT = 0

    def getRace(self) -> str:
        return None

    def __init__(self) -> None:
        super().__init__()
        self.penetrator: Penetrator = Penetrator()
        self.penetrator.overrideAttrs(length_attribute='size')
        self.urethra: Orifice = Orifice(modTagName='modUrethra', virginityAttrName='urethraVirgin')

    def fromXML(self, e: etree._Element) -> None:
        # HACK
        e.attrib['wetness'] = '0'

        self.penetrator.fromXML(e)
        self.urethra.fromXML(e)

        super().fromXML(e)

    def toXML(self, tagOverride: Optional[str] = None) -> etree._Element:
        e = super().toXML(tagOverride=tagOverride)
        mergetags(e, self.penetrator.toXML())
        mergetags(e, self.urethra.toXML())

        # HACK
        if 'wetness' in e.attrib:
            del e.attrib['wetness']
        return e

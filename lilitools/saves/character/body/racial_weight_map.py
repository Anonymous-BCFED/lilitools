from typing import Dict, Optional


class RacialWeightMap:
    def __init__(self) -> None:
        self._data: Dict[str, int] = {}

    def add(self, race: Optional[str], weight: int) -> None:
        if race is None or race == "NONE":
            return
        self._data[race] = weight + self._data.get(race, 0)

    def remove(self, race: str) -> None:
        if race in self._data:
            del self._data[race]

    def max(self) -> Optional[str]:
        return max(self._data, key=self._data.get)

    def demonOrMax(self) -> Optional[str]:
        if "DEMON" in self._data:
            return "DEMON"
        return self.max()

    def get(self, key: str) -> int:
        return max(0, self._data.get(key, 0))

    def __len__(self) -> int:
        return len(self._data)

#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, List, Optional
from lxml import etree
from lilitools.saves.character.body.tattoos.tattoo_counter import TattooCounter
from lilitools.saves.character.body.tattoos.tattoo_writing import TattooWriting
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.items.item_effect import ItemEffect
from lilitools.saves.savable import XML2BOOL, Savable
__all__ = ['RawTattoo']
## from [LT]/src/com/lilithsthrone/game/character/markings/Tattoo.java: public Element saveAsXML(Element parentElement, Document doc) { @ 4AgiBHV2gjALLdNFi9+pNWbUokie3cToOjJ3xfeRrv4Qf9pqtWEKntQW+ekP7gr05/SSNv2K8GOGjh7jpiTxhw==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawTattoo(Savable):
    TAG = 'tattoo'
    _XMLID_ATTR_GLOWING_ATTRIBUTE: str = 'glowing'
    _XMLID_ATTR_ID_ATTRIBUTE: str = 'id'
    _XMLID_ATTR_NAME_ATTRIBUTE: str = 'name'
    _XMLID_ATTR_PRIMARYCOLOUR_ATTRIBUTE: str = 'primaryColour'
    _XMLID_ATTR_SECONDARYCOLOUR_ATTRIBUTE: str = 'secondaryColour'
    _XMLID_ATTR_TATTOOCOUNTER_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_TATTOOWRITING_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_TERTIARYCOLOUR_ATTRIBUTE: str = 'tertiaryColour'
    _XMLID_TAG_EFFECTS_PARENT: str = 'effects'
    _XMLID_TAG_EFFECTS_VALUEELEM: Optional[str] = None
    _XMLID_TAG_TATTOOCOUNTER_ELEMENT: str = 'tattooCounter'
    _XMLID_TAG_TATTOOWRITING_ELEMENT: str = 'tattooWriting'

    def __init__(self) -> None:
        super().__init__()
        self.id: str = ''
        self.name: str = ''
        self.primaryColour: str = ''
        self.secondaryColour: Optional[str] = None
        self.tertiaryColour: Optional[str] = None
        self.glowing: bool = False
        self.tattooWriting: Optional[TattooWriting] = None  # Element
        self.tattooCounter: Optional[TattooCounter] = None  # Element
        self.effects: List[ItemEffect] = list()

    def overrideAttrs(self, glowing_attribute: _Optional_str = None, id_attribute: _Optional_str = None, name_attribute: _Optional_str = None, primaryColour_attribute: _Optional_str = None, secondaryColour_attribute: _Optional_str = None, tattooCounter_attribute: _Optional_str = None, tattooWriting_attribute: _Optional_str = None, tertiaryColour_attribute: _Optional_str = None) -> None:
        if glowing_attribute is not None:
            self._XMLID_ATTR_GLOWING_ATTRIBUTE = glowing_attribute
        if id_attribute is not None:
            self._XMLID_ATTR_ID_ATTRIBUTE = id_attribute
        if name_attribute is not None:
            self._XMLID_ATTR_NAME_ATTRIBUTE = name_attribute
        if primaryColour_attribute is not None:
            self._XMLID_ATTR_PRIMARYCOLOUR_ATTRIBUTE = primaryColour_attribute
        if secondaryColour_attribute is not None:
            self._XMLID_ATTR_SECONDARYCOLOUR_ATTRIBUTE = secondaryColour_attribute
        if tattooCounter_attribute is not None:
            self._XMLID_ATTR_TATTOOCOUNTER_ATTRIBUTE = tattooCounter_attribute
        if tattooWriting_attribute is not None:
            self._XMLID_ATTR_TATTOOWRITING_ATTRIBUTE = tattooWriting_attribute
        if tertiaryColour_attribute is not None:
            self._XMLID_ATTR_TERTIARYCOLOUR_ATTRIBUTE = tertiaryColour_attribute

    def overrideTags(self, effects_parent: _Optional_str = None, effects_valueelem: _Optional_str = None, tattooCounter_element: _Optional_str = None, tattooWriting_element: _Optional_str = None) -> None:
        if effects_parent is not None:
            self._XMLID_TAG_EFFECTS_PARENT = effects_parent
        if effects_valueelem is not None:
            self._XMLID_TAG_EFFECTS_VALUEELEM = effects_valueelem
        if tattooCounter_element is not None:
            self._XMLID_TAG_TATTOOCOUNTER_ELEMENT = tattooCounter_element
        if tattooWriting_element is not None:
            self._XMLID_TAG_TATTOOWRITING_ELEMENT = tattooWriting_element

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        lv: Savable
        sf: etree._Element
        value = e.get(self._XMLID_ATTR_ID_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_ID_ATTRIBUTE, 'id')
        else:
            self.id = str(value)
        value = e.get(self._XMLID_ATTR_NAME_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_NAME_ATTRIBUTE, 'name')
        else:
            self.name = str(value)
        value = e.get(self._XMLID_ATTR_PRIMARYCOLOUR_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_PRIMARYCOLOUR_ATTRIBUTE, 'primaryColour')
        else:
            self.primaryColour = str(value)
        value = e.get(self._XMLID_ATTR_SECONDARYCOLOUR_ATTRIBUTE, None)
        if value is None:
            self.secondaryColour = None
        else:
            self.secondaryColour = str(value)
        value = e.get(self._XMLID_ATTR_TERTIARYCOLOUR_ATTRIBUTE, None)
        if value is None:
            self.tertiaryColour = None
        else:
            self.tertiaryColour = str(value)
        value = e.get(self._XMLID_ATTR_GLOWING_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_GLOWING_ATTRIBUTE, 'glowing')
        else:
            self.glowing = XML2BOOL[value]
        if (sf := e.find(self._XMLID_TAG_TATTOOWRITING_ELEMENT)) is not None:
            self.tattooWriting = TattooWriting()
            self.tattooWriting.fromXML(sf)
        if (sf := e.find(self._XMLID_TAG_TATTOOCOUNTER_ELEMENT)) is not None:
            self.tattooCounter = TattooCounter()
            self.tattooCounter.fromXML(sf)
        if (lf := e.find(self._XMLID_TAG_EFFECTS_PARENT)) is not None:
            if lf is not None:
                for lc in lf:
                    lv = ItemEffect()
                    lv.fromXML(lc)
                    self.effects.append(lv)
        else:
            self.effects = list()

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        e.attrib[self._XMLID_ATTR_ID_ATTRIBUTE] = str(self.id)
        e.attrib[self._XMLID_ATTR_NAME_ATTRIBUTE] = str(self.name)
        e.attrib[self._XMLID_ATTR_PRIMARYCOLOUR_ATTRIBUTE] = str(self.primaryColour)
        if self.secondaryColour is not None:
            e.attrib[self._XMLID_ATTR_SECONDARYCOLOUR_ATTRIBUTE] = str(self.secondaryColour)
        if self.tertiaryColour is not None:
            e.attrib[self._XMLID_ATTR_TERTIARYCOLOUR_ATTRIBUTE] = str(self.tertiaryColour)
        e.attrib[self._XMLID_ATTR_GLOWING_ATTRIBUTE] = str(self.glowing).lower()
        if self.tattooWriting is not None:
            e.append(self.tattooWriting.toXML(self._XMLID_TAG_TATTOOWRITING_ELEMENT))
        if self.tattooCounter is not None:
            e.append(self.tattooCounter.toXML(self._XMLID_TAG_TATTOOCOUNTER_ELEMENT))
        lp = etree.SubElement(e, self._XMLID_TAG_EFFECTS_PARENT)
        for lv in self.effects:
            lp.append(lv.toXML())
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['id'] = self.id
        data['name'] = self.name
        data['primaryColour'] = self.primaryColour
        if self.secondaryColour is not None:
            data['secondaryColour'] = self.secondaryColour
        if self.tertiaryColour is not None:
            data['tertiaryColour'] = self.tertiaryColour
        data['glowing'] = bool(self.glowing)
        if self.tattooWriting is not None:
            data['tattooWriting'] = self.tattooWriting.toDict()
        if self.tattooCounter is not None:
            data['tattooCounter'] = self.tattooCounter.toDict()
        if self.effects is None or len(self.effects) > 0:
            data['effects'] = list()
        else:
            lc = list()
            if len(self.effects) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['effects'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'id' not in data:
            raise KeyRequiredException(self, data, 'id', 'id')
        self.id = str(data['id'])
        if 'name' not in data:
            raise KeyRequiredException(self, data, 'name', 'name')
        self.name = str(data['name'])
        if 'primaryColour' not in data:
            raise KeyRequiredException(self, data, 'primaryColour', 'primaryColour')
        self.primaryColour = str(data['primaryColour'])
        self.secondaryColour = str(data['secondaryColour']) if 'secondaryColour' in data else None
        self.tertiaryColour = str(data['tertiaryColour']) if 'tertiaryColour' in data else None
        if 'glowing' not in data:
            raise KeyRequiredException(self, data, 'glowing', 'glowing')
        self.glowing = bool(data['glowing'])
        if (sf := data.get('tattooWriting')) is not None:
            self.tattooWriting = TattooWriting()
            self.tattooWriting.fromDict(data['tattooWriting'])
        else:
            self.tattooWriting = None
        if (sf := data.get('tattooCounter')) is not None:
            self.tattooCounter = TattooCounter()
            self.tattooCounter.fromDict(data['tattooCounter'])
        else:
            self.tattooCounter = None
        if (lv := self.effects) is not None:
            for le in lv:
                self.effects.append(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'effects', 'effects')

#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL, Savable
__all__ = ['RawTattooCounter']
## from [LT]/src/com/lilithsthrone/game/character/markings/TattooCounter.java: public Element saveAsXML(Element parentElement, Document doc) { @ T220pWICzWz3xPu2ybBPN2ybSHwO4HWA21znGDdPF3qLm90PeelHtUbgMxWNF1+E2nTGYyu4GLk4yLMbx4v8/g==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawTattooCounter(Savable):
    TAG = 'tattooCounter'
    _XMLID_ATTR_COLOUR_ATTRIBUTE: str = 'colour'
    _XMLID_ATTR_COUNTTYPE_ATTRIBUTE: str = 'countType'
    _XMLID_ATTR_GLOW_ATTRIBUTE: str = 'glow'
    _XMLID_ATTR_TYPE_ATTRIBUTE: str = 'type'

    def __init__(self) -> None:
        super().__init__()
        self.type: str = ''
        self.countType: str = ''
        self.colour: str = ''
        self.glow: bool = False

    def overrideAttrs(self, colour_attribute: _Optional_str = None, countType_attribute: _Optional_str = None, glow_attribute: _Optional_str = None, type_attribute: _Optional_str = None) -> None:
        if colour_attribute is not None:
            self._XMLID_ATTR_COLOUR_ATTRIBUTE = colour_attribute
        if countType_attribute is not None:
            self._XMLID_ATTR_COUNTTYPE_ATTRIBUTE = countType_attribute
        if glow_attribute is not None:
            self._XMLID_ATTR_GLOW_ATTRIBUTE = glow_attribute
        if type_attribute is not None:
            self._XMLID_ATTR_TYPE_ATTRIBUTE = type_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_TYPE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_TYPE_ATTRIBUTE, 'type')
        else:
            self.type = str(value)
        value = e.get(self._XMLID_ATTR_COUNTTYPE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_COUNTTYPE_ATTRIBUTE, 'countType')
        else:
            self.countType = str(value)
        value = e.get(self._XMLID_ATTR_COLOUR_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_COLOUR_ATTRIBUTE, 'colour')
        else:
            self.colour = str(value)
        value = e.get(self._XMLID_ATTR_GLOW_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_GLOW_ATTRIBUTE, 'glow')
        else:
            self.glow = XML2BOOL[value]

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_TYPE_ATTRIBUTE] = str(self.type)
        e.attrib[self._XMLID_ATTR_COUNTTYPE_ATTRIBUTE] = str(self.countType)
        e.attrib[self._XMLID_ATTR_COLOUR_ATTRIBUTE] = str(self.colour)
        e.attrib[self._XMLID_ATTR_GLOW_ATTRIBUTE] = str(self.glow).lower()
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['type'] = self.type
        data['countType'] = self.countType
        data['colour'] = self.colour
        data['glow'] = bool(self.glow)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'type' not in data:
            raise KeyRequiredException(self, data, 'type', 'type')
        self.type = str(data['type'])
        if 'countType' not in data:
            raise KeyRequiredException(self, data, 'countType', 'countType')
        self.countType = str(data['countType'])
        if 'colour' not in data:
            raise KeyRequiredException(self, data, 'colour', 'colour')
        self.colour = str(data['colour'])
        if 'glow' not in data:
            raise KeyRequiredException(self, data, 'glow', 'glow')
        self.glow = bool(data['glow'])

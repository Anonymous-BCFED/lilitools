#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, List, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL, Savable
__all__ = ['RawTattooWriting']
## from [LT]/src/com/lilithsthrone/game/character/markings/TattooWriting.java: public Element saveAsXML(Element parentElement, Document doc) { @ mmLuJ1LUmuFv5JJxU+xYN/M8uNsG1AnAcHx+j96EKuLkWhYE6Wyvr7LAYVxTXoS4h2JBjj67vycZfJfO0Xic/A==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawTattooWriting(Savable):
    TAG = 'tattooWriting'
    _XMLID_ATTR_COLOUR_ATTRIBUTE: str = 'colour'
    _XMLID_ATTR_GLOW_ATTRIBUTE: str = 'glow'
    _XMLID_ATTR_STYLES_VALUEATTR: str = 'value'
    _XMLID_TAG_STYLES_CHILD: str = 'style'
    _XMLID_TAG_STYLES_PARENT: str = 'styles'

    def __init__(self) -> None:
        super().__init__()
        self.colour: str = ''
        self.glow: bool = False
        self.text: str = ''  # CDATA
        self.styles: List[str] = list()

    def overrideAttrs(self, colour_attribute: _Optional_str = None, glow_attribute: _Optional_str = None, styles_valueattr: _Optional_str = None) -> None:
        if colour_attribute is not None:
            self._XMLID_ATTR_COLOUR_ATTRIBUTE = colour_attribute
        if glow_attribute is not None:
            self._XMLID_ATTR_GLOW_ATTRIBUTE = glow_attribute
        if styles_valueattr is not None:
            self._XMLID_ATTR_STYLES_VALUEATTR = styles_valueattr

    def overrideTags(self, styles_child: _Optional_str = None, styles_parent: _Optional_str = None) -> None:
        if styles_child is not None:
            self._XMLID_TAG_STYLES_CHILD = styles_child
        if styles_parent is not None:
            self._XMLID_TAG_STYLES_PARENT = styles_parent

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        value = e.get(self._XMLID_ATTR_COLOUR_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_COLOUR_ATTRIBUTE, 'colour')
        else:
            self.colour = str(value)
        value = e.get(self._XMLID_ATTR_GLOW_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_GLOW_ATTRIBUTE, 'glow')
        else:
            self.glow = XML2BOOL[value]
        self.text = e.text
        if (lf := e.find(self._XMLID_TAG_STYLES_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_STYLES_CHILD):
                    self.styles.append(lc.attrib[self._XMLID_ATTR_STYLES_VALUEATTR])
        else:
            self.styles = list()

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        e.attrib[self._XMLID_ATTR_COLOUR_ATTRIBUTE] = str(self.colour)
        e.attrib[self._XMLID_ATTR_GLOW_ATTRIBUTE] = str(self.glow).lower()
        e.text = etree.CDATA(str(self.text))
        lp = etree.SubElement(e, self._XMLID_TAG_STYLES_PARENT)
        for lv in self.styles:
            etree.SubElement(lp, self._XMLID_TAG_STYLES_CHILD, {self._XMLID_ATTR_STYLES_VALUEATTR: lv})
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['colour'] = self.colour
        data['glow'] = bool(self.glow)
        data['text'] = self.text
        if self.styles is None or len(self.styles) > 0:
            data['styles'] = list()
        else:
            lc = list()
            if len(self.styles) > 0:
                for lv in lc:
                    lc.append(str(lv))
            data['styles'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'colour' not in data:
            raise KeyRequiredException(self, data, 'colour', 'colour')
        self.colour = str(data['colour'])
        if 'glow' not in data:
            raise KeyRequiredException(self, data, 'glow', 'glow')
        self.glow = bool(data['glow'])
        self.text = data['text']
        if (lv := self.styles) is not None:
            for le in lv:
                self.styles.append(str(le))
        else:
            raise KeyRequiredException(self, data, 'styles', 'styles')

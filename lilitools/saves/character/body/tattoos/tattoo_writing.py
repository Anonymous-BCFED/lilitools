from typing import Optional

from lxml import etree

from lilitools.saves.character.body.tattoos._savables.tattoo_writing import RawTattooWriting


class TattooWriting(RawTattooWriting):
    def fromXML(self, e: etree._Element) -> None:
        super().fromXML(e)
        self.text = self.text.strip()

    def toXML(self, tagOverride: Optional[str] = None) -> etree._Element:
        self.text = self.text.strip()
        return super().toXML(tagOverride)

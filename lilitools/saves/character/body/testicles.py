from lilitools.saves.character.body._savables.testicles import RawTesticles


class Testicles(RawTesticles):
    ## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public AbstractRace getRaceFromPartWeighting(boolean ignoreOverride) { @ PE7oMjFVRwVpX6Pyn6xbqxI9RY5PJFGqku4Jhds+6hnzfRmxtpZSbEflmEKO7mpskcmbfbBubTEuMD5uOcRI+A==
    RACIAL_WEIGHT = 0

    def getRace(self) -> str:
        return None

    def getCumRegenPerBreastPerSecond(self) -> float:
        return self.cumRegeneration / (60 * 60 * 24)

    def getCumVolumePerHour(self) -> int:
        return self.getCumRegenPerBreastPerSecond() * 60 * 60

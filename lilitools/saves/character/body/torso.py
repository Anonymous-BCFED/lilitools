from lilitools.saves.character.body._savables.torso import RawTorso


class Torso(RawTorso):
    RACIAL_WEIGHT = 4

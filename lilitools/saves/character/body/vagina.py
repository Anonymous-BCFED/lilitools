from typing import Optional

from lxml import etree

from lilitools.saves.character.body._savables.vagina import RawVagina
from lilitools.saves.character.body.orifice import Orifice
from lilitools.saves.character.body.penetrator import Penetrator
from lilitools.xmlutils import mergetags


class Vagina(RawVagina):
    ## from [LT]/src/com/lilithsthrone/game/character/body/Body.java: public AbstractRace getRaceFromPartWeighting(boolean ignoreOverride) { @ PE7oMjFVRwVpX6Pyn6xbqxI9RY5PJFGqku4Jhds+6hnzfRmxtpZSbEflmEKO7mpskcmbfbBubTEuMD5uOcRI+A==
    RACIAL_WEIGHT = 0

    def getRace(self) -> str:
        return None

    def __init__(self) -> None:
        super().__init__()
        self.orifice = Orifice()

        self.clit = Penetrator(
            modTagName="modClit", virginityAttrName="NOTUSED_clitVirgin"
        )
        self.clit.overrideAttrs(
            girth_attribute="clitGirth", length_attribute="clitSize"
        )

        self.urethra: Orifice = Orifice("modUrethra", "urethraVirgin")
        self.urethra.overrideAttrs(
            capacity_attribute="urethraCapacity",
            depth_attribute="urethraDepth",
            elasticity_attribute="urethraElasticity",
            orificeVirgin_attribute="urethraVirgin",
            plasticity_attribute="urethraPlasticity",
            stretchedCapacity_attribute="urethraStretchedCapacity",
            wetness_attribute="NOTUSED_urethraWetness",
        )

    def fromXML(self, e: etree._Element) -> None:
        e.attrib["NOTUSED_clitVirgin"] = "true"
        e.attrib["NOTUSED_urethraWetness"] = "0"
        super().fromXML(e)
        self.orifice.fromXML(e)
        self.clit.fromXML(e)
        self.urethra.fromXML(e)

    def toXML(self, tagOverride: Optional[str] = None) -> etree._Element:
        e = super().toXML(tagOverride=tagOverride)
        mergetags(e, self.orifice.toXML())
        mergetags(e, self.clit.toXML())
        mergetags(e, self.urethra.toXML())
        if "NOTUSED_clitVirgin" in e.attrib:
            del e.attrib["NOTUSED_clitVirgin"]
        if "NOTUSED_urethraWetness" in e.attrib:
            del e.attrib["NOTUSED_urethraWetness"]
        return e

    ORGASMS_PER_HOUR: int = 10

    def getGirlcumVolumePerHour(self) -> int:
        return (
            self.ORGASMS_PER_HOUR * self.orifice.wetness * (5 if self.squirter else 1)
        )

from typing import Any, Dict, Optional

from lxml import etree

from lilitools.saves.savable import Savable


class CharacterNames(Savable):
    TAG = 'name'
    MIN_FEM_FOR_MASCULINE: int = 39
    MAX_FEM_FOR_FEMININE: int = 60

    def __init__(self) -> None:
        super().__init__()
        self.masculine: str = ''
        self.feminine: str = ''
        self.androgynous: str = ''

    def fromDict(self, data: Dict[str, Any]) -> None:
        self.masculine = data['masculine']
        self.feminine = data['feminine']
        self.androgynous = data['androgynous']

    def toDict(self) -> Dict[str, Any]:
        return {
            'masculine': self.masculine,
            'feminine': self.feminine,
            'androgynous': self.androgynous,
        }

    def fromXML(self, e: etree._Element) -> None:
        super().fromXML(e)
        if 'value' in e.attrib:
            self.masculine = self.feminine = self.androgynous = e.attrib['value']
        else:
            self.masculine = e.attrib.get('nameMasculine')
            self.androgynous = e.attrib.get('nameAndrogynous')
            self.feminine = e.attrib.get('nameFeminine')

    def toXML(self, tagOverride: Optional[str] = None) -> etree._Element:
        e = super().toXML(tagOverride)
        if self.masculine:
            e.attrib['nameMasculine'] = self.masculine
        if self.feminine:
            e.attrib['nameFeminine'] = self.feminine
        if self.androgynous:
            e.attrib['nameAndrogynous'] = self.androgynous
        return e

    def getCurrentName(self, femininity: int) -> str:
        if femininity <= self.MIN_FEM_FOR_MASCULINE:
            return self.masculine
        elif femininity > self.MAX_FEM_FOR_FEMININE:
            return self.feminine
        return self.androgynous

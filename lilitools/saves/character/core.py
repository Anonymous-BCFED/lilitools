import datetime
from typing import Any, Dict, List

from lxml import etree

from lilitools.consts import PRESERVE_SAVE_ORDERING
from lilitools.logging import ClickWrap
from lilitools.saves.character._savables.core import RawCore
from lilitools.utils import bool2yesno

click = ClickWrap()


class Core(RawCore):
    def __init__(self) -> None:
        super().__init__()

    if PRESERVE_SAVE_ORDERING:

        def _fromXML_areasKnownByCharacters(self, e: etree._Element) -> None:
            self.areasKnownByCharacters = {}
            if (
                areasKnownByCharactersElem := e.find("areasKnownByCharacters")
            ) is not None:
                for areaElem in areasKnownByCharactersElem.findall("area"):
                    charsKnowingOfThisArea = list()
                    for characterElem in areaElem.findall("character"):
                        charsKnowingOfThisArea.append(characterElem.attrib["id"])
                    for characterElem in areaElem.findall("c"):
                        charsKnowingOfThisArea.append(characterElem.text)
                    self.areasKnownByCharacters[areaElem.attrib["type"]] = (
                        charsKnowingOfThisArea
                    )

        def _toXML_areasKnownByCharacters(self, e: etree._Element) -> None:
            areasKnownByCharactersElem = etree.Element("areasKnownByCharacters")
            for areaID, chars in self.areasKnownByCharacters.items():
                areaElem = etree.SubElement(
                    areasKnownByCharactersElem, "area", {"type": areaID}
                )
                for characterID in chars:
                    etree.SubElement(areaElem, "c").text = characterID
            e.append(areasKnownByCharactersElem)

    else:

        def _fromXML_areasKnownByCharacters(self, e: etree._Element) -> None:
            self.areasKnownByCharacters = {}
            if (
                areasKnownByCharactersElem := e.find("areasKnownByCharacters")
            ) is not None:
                for areaElem in areasKnownByCharactersElem.findall("area"):
                    charsKnowingOfThisArea = set()
                    for characterElem in areaElem.findall("character"):
                        charsKnowingOfThisArea.add(characterElem.attrib["id"])
                    for characterElem in areaElem.findall("c"):
                        charsKnowingOfThisArea.add(characterElem.text)
                    self.areasKnownByCharacters[areaElem.attrib["type"]] = (
                        charsKnowingOfThisArea
                    )

        def _toXML_areasKnownByCharacters(self, e: etree._Element) -> None:
            areasKnownByCharactersElem = etree.Element("areasKnownByCharacters")
            for areaID, chars in sorted(
                self.areasKnownByCharacters.items(), key=lambda t: t[0]
            ):
                areaElem = etree.SubElement(
                    areasKnownByCharactersElem, "area", {"type": areaID}
                )
                for characterID in sorted(chars):
                    etree.SubElement(areaElem, "c").text = characterID
            e.append(areasKnownByCharactersElem)

    def _toDict_areasKnownByCharacters(self, data: Dict[str, Any]) -> None:
        akbcs: Dict[str, List[str]] = {}
        for areaID, chars in self.areasKnownByCharacters.items():
            akbcs[areaID] = [str(x) for x in sorted(chars)]
        data["areasKnownByCharacters"] = akbcs

    def _fromDict_areasKnownByCharacters(self, data: Dict[str, Any]) -> None:
        akbcs: Dict[str, List[str]]
        areaID: str
        chars: List[str]
        if (akbcs := data.get("areasKnownByCharacters")) is not None:
            self.areasKnownByCharacters = {}
            for areaID, chars in akbcs.items():
                self.areasKnownByCharacters[areaID] = list(chars)

    def getBirthday(self) -> datetime.date:
        return datetime.date(self.yearOfBirth, self.monthOfBirth, self.dayOfBirth)

    def show(self) -> None:
        from lilitools.cli.units import f2pct

        click.echo(f"ID: {self.id}")
        click.echo(f"Path: {self.pathName}")
        with click.echo("Name:"):
            click.echo(f"Androgynous: {self.name.androgynous}")
            click.echo(f"Feminine: {self.name.feminine}")
            click.echo(f"Masculine: {self.name.masculine}")
        click.echo(f"Surname: {self.surname}")
        click.echo(f"Description: {self.description}")
        click.echo(f"Generic Name: {self.genericName}")
        click.echo(f"Speech Colour: {self.speechColour}")

        with click.echo("Combat Stats:"):
            click.echo(f"Total: {self.foughtPlayerCount:,}")
            click.echo(f"Won vs. Player: {self.lostCombatCount:,}")
            click.echo(f"Lost vs. Player: {self.wonCombatCount:,}")
            click.echo(f"Combat Behaviour: {self.combatBehaviour}")

        with click.echo("Knowledge Flags:"):
            click.echo(f"Player Knows Name: {bool2yesno(self.playerKnowsName)}")
            click.echo(f"First Name Terms: {bool2yesno(self.playerOnFirstNameTerms)}")
            click.echo(f"Race Concealed: {bool2yesno(self.raceConcealed)}")
            click.echo(f"Captive: {bool2yesno(self.captive)}")

        with click.echo("Stats:"):
            click.echo(f"Level: {self.level}")
            click.echo(f"XP: {self.experience:,}")
            click.echo(f"Perk Points: {self.perkPoints:,}")
            with click.echo(f"Perk Category Points:"):
                if len(self.perkCategoryPoints) > 0:
                    for k, v in self.perkCategoryPoints.items():
                        click.info(f"{k} => {v:,}")
                else:
                    click.info(f"(Empty)")
            click.echo(f"Health: {self.health:,}")
            # click.echo(f'Aura: {self.aura}')
            click.echo(f"Mana: {self.mana:,}")

        with click.echo("Age-Related:"):
            click.echo(f"Birthday: {self.getBirthday()}")
            click.echo(f"Age Appearance Offset: {self.ageAppearanceDifference}")

        with click.echo("Occupation:"):
            click.echo(f"Desired Jobs: {self.desiredJobs!r}")

        with click.echo("Personality:"):
            if len(self.personality):
                for x in sorted(self.personality):
                    click.echo(f"- {x}")
            else:
                click.echo("(Empty)")

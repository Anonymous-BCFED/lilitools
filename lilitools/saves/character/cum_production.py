from enum import Enum
from functools import lru_cache
__all__ = ['ECumProduction']


class ECumProduction(Enum):
    ZERO_NONE = 'none', 'no', 0, 1, 100
    ONE_TRICKLE = 'drop', 'a few drops of', 1, 3, 75
    TWO_SMALL_AMOUNT = 'trickle', 'a trickle of', 3, 6, 50
    THREE_AVERAGE = 'average', 'an average amount of', 6, 16, 25
    FOUR_LARGE = 'large', 'a large amount of', 16, 30, 5
    FIVE_HUGE = 'huge', 'a huge amount of', 30, 100, 0
    SIX_EXTREME = 'extreme', 'an extreme amount of', 100, 1000, 0
    SEVEN_MONSTROUS = 'monstrous', 'a monstrous amount of', 1000, 10000, 0

    def getName(self) ->str:
        return self.name_

    def getDescriptor(self) ->str:
        return self.descriptor

    def getMin(self) ->int:
        return self.min

    def getMax(self) ->int:
        return self.max

    def getArousalNeededToStartPreCumming(self) ->int:
        return self.arousalNeededToStartPreCumming

    def __init__(self, name: str, descriptor: str, min: int, max: int, arousalNeededToStartPreCumming: int) ->None:
        self.name_: str = name
        self.descriptor: str = descriptor
        self.min: int = min
        self.max: int = max
        self.arousalNeededToStartPreCumming: int = arousalNeededToStartPreCumming

    @property
    @lru_cache
    def median(self) ->float:
        return self.min + (self.max - self.min) * 2

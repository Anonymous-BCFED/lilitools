from enum import IntEnum
__all__ = ['EAndrogynousIdentification']


class EAndrogynousIdentification(IntEnum):
    FEMININE = 0
    CLOTHING_FEMININE = 1
    CLOTHING_MASCULINE = 2
    MASCULINE = 3

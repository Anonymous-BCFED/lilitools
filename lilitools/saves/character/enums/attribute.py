from enum import Enum
__all__ = ['EAttribute']


class EAttribute(Enum):
    HEALTH_MAXIMUM = False, 1, 1, 1000, 'health', 'Health', 'healthIcon', 'health'
    MANA_MAXIMUM = False, 1, 1, 1000, 'aura', 'Aura', 'manaIcon', 'aura-boost'
    EXPERIENCE = False, 0, 0, 1000000, 'experience', 'Experience', 'experienceIcon', 'learning'
    ACTION_POINTS = False, 0, 0, 10, 'action points', 'Action points', 'action_points', 'initiative'
    AROUSAL = False, 0, 0, 100, 'arousal', 'Arousal', 'arousalIcon', 'long-lasting'
    LUST = False, 0, 0, 100, 'lust', 'Lust', 'arousalIcon', 'passion'
    RESTING_LUST = False, 0, 0, 80, 'resting lust', 'Resting lust', 'arousalIcon', 'passion'
    MAJOR_PHYSIQUE = False, 0, 0, 100, 'physique', 'Physique', 'strengthIcon', 'power'
    MAJOR_ARCANE = False, 0, 0, 100, 'arcane', 'Arcane', 'intelligenceIcon', 'arcane-boost'
    MAJOR_CORRUPTION = False, 0, 0, 100, 'corruption', 'Corruption', 'corruptionIcon', 'corruption'
    ENCHANTMENT_LIMIT = False, 0, 0, 1000, 'enchantment capacity', 'Enchantment capacity', 'enchantmentLimitIcon', 'harnessing'
    FERTILITY = True, 10, -100, 100, 'fertility', 'Fertility', 'shieldIcon', 'fertility'
    VIRILITY = True, 10, -100, 100, 'virility', 'Virility', 'shieldIcon', 'virility'
    SPELL_COST_MODIFIER = True, 0, 0, 80, 'spell efficiency', 'Spell efficiency', 'shieldIcon', 'proficiency'
    CRITICAL_DAMAGE = True, 150, 100, 500, 'critical power', 'Critical power', 'shieldIcon', 'impact'
    ENERGY_SHIELDING = False, 0, -100, 500, 'health shielding', 'Health shielding', 'shieldIcon', 'endurance'
    RESISTANCE_PHYSICAL = False, 0, -100, 500, 'physical shielding', 'Physical shielding', 'shieldIcon', 'toughness'
    RESISTANCE_LUST = False, 0, -100, 500, 'lust shielding', 'Lust shielding', 'shieldIcon', 'chastity'
    RESISTANCE_FIRE = False, 0, -100, 500, 'fire shielding', 'Fire shielding', 'shieldIcon', 'extinguishing'
    RESISTANCE_ICE = False, 0, -100, 500, 'cold shielding', 'Cold shielding', 'shieldIcon', 'warmth'
    RESISTANCE_POISON = False, 0, -100, 500, 'poison shielding', 'Poison shielding', 'shieldIcon', 'anti-venom'
    DAMAGE_UNARMED = True, 0, -80, 100, 'unarmed damage', 'Unarmed damage', 'swordIcon', 'martial arts'
    DAMAGE_MELEE_WEAPON = True, 0, -80, 100, 'melee weapon damage', 'Melee Weapon damage', 'swordIcon', 'melee mastery'
    DAMAGE_RANGED_WEAPON = True, 0, -80, 100, 'ranged weapon damage', 'Ranged weapon damage', 'swordIcon', 'ranged mastery'
    DAMAGE_SPELLS = True, 0, -80, 100, 'spell damage', 'Spell damage', 'swordIcon', 'arcane power'
    DAMAGE_PHYSICAL = True, 0, -80, 100, 'physical damage', 'Physical damage', 'swordIcon', 'force'
    DAMAGE_LUST = True, 0, -80, 100, 'lust damage', 'Lust damage', 'swordIcon', 'seduction'
    DAMAGE_FIRE = True, 0, -80, 100, 'fire damage', 'Fire damage', 'swordIcon', 'inferno'
    DAMAGE_ICE = True, 0, -80, 100, 'cold damage', 'Cold damage', 'swordIcon', 'blizzard'
    DAMAGE_POISON = True, 0, -80, 100, 'poison damage', 'Poison damage', 'swordIcon', 'venom'
    DAMAGE_IMP = True, 0, -100, 100, 'imp damage', 'Imp damage', 'swordIcon', 'impish-obliteration'
    DAMAGE_LILIN = True, 0, -100, 100, 'lilin damage', 'Lilin damage', 'swordIcon', 'lilin-obliteration'
    DAMAGE_ELDER_LILIN = True, 0, -100, 100, 'elder lilin damage', 'Elder lilin damage', 'swordIcon', 'elder-lilin-obliteration'

    def getPercentage(self) ->bool:
        return self.percentage

    def getBaseValue(self) ->int:
        return self.baseValue

    def getLowerLimit(self) ->int:
        return self.lowerLimit

    def getUpperLimit(self) ->int:
        return self.upperLimit

    def getName(self) ->str:
        return self.name_

    def getNameAbbreviation(self) ->str:
        return self.nameAbbreviation

    def getPositiveEnchantment(self) ->str:
        return self.positiveEnchantment

    def getNegativeEnchantment(self) ->str:
        return self.negativeEnchantment

    def __init__(self, percentage: bool, baseValue: int, lowerLimit: int, upperLimit: int, name: str, nameAbbreviation: str, positiveEnchantment: str, negativeEnchantment: str) ->None:
        self.percentage: bool = percentage
        self.baseValue: int = baseValue
        self.lowerLimit: int = lowerLimit
        self.upperLimit: int = upperLimit
        self.name_: str = name
        self.nameAbbreviation: str = nameAbbreviation
        self.positiveEnchantment: str = positiveEnchantment
        self.negativeEnchantment: str = negativeEnchantment

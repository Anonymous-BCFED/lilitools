from enum import IntEnum
__all__ = ['EBodyHair']


class EBodyHair(IntEnum):
    ZERO_NONE = 0
    ONE_STUBBLE = 1
    TWO_MANICURED = 2
    THREE_TRIMMED = 3
    FOUR_NATURAL = 4
    FIVE_UNKEMPT = 5
    SIX_BUSHY = 6
    SEVEN_WILD = 7

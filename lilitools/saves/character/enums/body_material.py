from enum import IntEnum
__all__ = ['EBodyMaterial']


class EBodyMaterial(IntEnum):
    FLESH = 0
    SLIME = 1
    SILICONE = 2
    FIRE = 3
    WATER = 4
    ICE = 5
    AIR = 6
    STONE = 7
    RUBBER = 8
    ARCANE = 9

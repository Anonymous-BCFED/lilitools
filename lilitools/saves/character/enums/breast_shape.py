from enum import IntEnum
__all__ = ['EBreastShape']


class EBreastShape(IntEnum):
    UDDERS = 0
    ROUND = 1
    POINTY = 2
    PERKY = 3
    SIDE_SET = 4
    WIDE = 5
    NARROW = 6

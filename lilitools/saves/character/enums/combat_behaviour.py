from enum import IntEnum
__all__ = ['ECombatBehaviour']


class ECombatBehaviour(IntEnum):
    BALANCED = 0
    ATTACK = 1
    DEFEND = 2
    SEDUCE = 3
    SPELLS = 4
    SUPPORT = 5

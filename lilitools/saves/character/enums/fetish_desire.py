from enum import IntEnum
__all__ = ['EFetishDesire']


class EFetishDesire(IntEnum):
    ZERO_HATE = 0
    ONE_DISLIKE = 1
    TWO_NEUTRAL = 2
    THREE_LIKE = 3
    FOUR_LOVE = 4

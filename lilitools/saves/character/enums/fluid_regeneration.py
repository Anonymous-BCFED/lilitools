from enum import Enum
from functools import lru_cache
__all__ = ['EFluidRegeneration']


class EFluidRegeneration(Enum):
    ZERO_SLOW = 'slow', 0, 250, 'slowly'
    ONE_AVERAGE = 'average', 250, 750, ''
    TWO_FAST = 'fast', 750, 5000, 'quickly'
    THREE_RAPID = 'rapid', 5000, 100000, 'rapidly'
    FOUR_VERY_RAPID = 'very rapid', 100000, 500000, 'very rapidly'

    def getName(self) ->str:
        return self.name_

    def getMin(self) ->int:
        return self.min

    def getMax(self) ->int:
        return self.max

    def getVerb(self) ->str:
        return self.verb

    def __init__(self, name: str, min: int, max: int, verb: str) ->None:
        self.name_: str = name
        self.min: int = min
        self.max: int = max
        self.verb: str = verb

    @property
    @lru_cache
    def median(self) ->float:
        return self.min + (self.max - self.min) * 2

from enum import IntEnum
__all__ = ['EGenitalArrangement']


class EGenitalArrangement(IntEnum):
    NORMAL = 0
    CLOACA = 1
    CLOACA_BEHIND = 2

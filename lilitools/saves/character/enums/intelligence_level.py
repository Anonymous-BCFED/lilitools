from enum import Enum
from functools import lru_cache
__all__ = ['EIntelligenceLevel']


class EIntelligenceLevel(Enum):
    ZERO_AIRHEAD = 'arcane impotence', 0, 10
    ONE_AVERAGE = 'arcane potential', 10, 30
    TWO_SMART = 'arcane proficiency', 30, 50
    THREE_BRAINY = 'arcane prowess', 50, 70
    FOUR_GENIUS = 'arcane mastery', 70, 90
    FIVE_POLYMATH = 'arcane brilliance', 90, 100

    def getName(self) ->str:
        return self.name_

    def getMinimumValue(self) ->int:
        return self.minimumValue

    def getMaximumValue(self) ->int:
        return self.maximumValue

    def __init__(self, name: str, minimumValue: int, maximumValue: int) ->None:
        self.name_: str = name
        self.minimumValue: int = minimumValue
        self.maximumValue: int = maximumValue

    @property
    @lru_cache
    def medianValue(self) ->float:
        return self.minimumValue + (self.maximumValue - self.minimumValue) * 2

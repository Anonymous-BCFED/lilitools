# AUTOGENERATED. DO NOT EDIT.
from enum import Enum
__all__ = ['EKnownPerks']


class EKnownPerks(Enum):
    AHEGAO = 'AHEGAO', 'ahegao', 4
    AMAZONIAN_TRAINING = 'AMAZONIAN_TRAINING', 'Amazonian training', 0
    ARCANE_BASE = 'ARCANE_BASE', 'natural arcane power', 1
    ARCANE_BOOST = 'ARCANE_BOOST', 'arcane training', 2
    ARCANE_BOOST_ALT = 'ARCANE_BOOST_ALT', 'arcane training', 0
    ARCANE_BOOST_MAJOR = 'ARCANE_BOOST_MAJOR', 'arcane affinity', 6
    ARCANE_COMBATANT = 'ARCANE_COMBATANT', 'arcane combatant', 5
    ARCANE_CRITICALS = 'ARCANE_CRITICALS', 'arcane precision', 2
    ARCANE_VAMPYRISM = 'ARCANE_VAMPYRISM', 'arcane vampyrism', 10
    AURA_BOOST = 'AURA_BOOST', 'aura reserves', 3
    BARREN = 'BARREN', 'barren', 2
    BESERK = 'BESERK', 'berserk', 10
    CHUUNI = 'CHUUNI', 'chuuni', 5
    CLOTHING_ENCHANTER = 'CLOTHING_ENCHANTER', 'arcane weaver', 4
    COLD_ENHANCEMENT = 'COLD_ENHANCEMENT', 'frosty', 0
    COLD_ENHANCEMENT_2 = 'COLD_ENHANCEMENT_2', 'ice cold', 0
    COMBAT_REGENERATION = 'COMBAT_REGENERATION', 'combat regeneration', 10
    CONVINCING_REQUESTS = 'CONVINCING_REQUESTS', 'irresistible appeals', 8
    CRITICAL_BOOST = 'CRITICAL_BOOST', 'critical power', 5
    CRITICAL_BOOST_ARCANE = 'CRITICAL_BOOST_ARCANE', 'critical power', 5
    CRITICAL_BOOST_LUST = 'CRITICAL_BOOST_LUST', 'critical power', 5
    DOLL_ARCANE_1 = 'DOLL_ARCANE_1', 'no aura', 0
    DOLL_ARCANE_2 = 'DOLL_ARCANE_2', 'object limitations', 0
    DOLL_ARCANE_3 = 'DOLL_ARCANE_3', 'arcane-powered', 0
    DOLL_ARCANE_CORE = 'DOLL_ARCANE_CORE', 'automaton', 0
    DOLL_LUST_1 = 'DOLL_LUST_1', 'live to fuck', 0
    DOLL_LUST_2 = 'DOLL_LUST_2', 'owned', 0
    DOLL_LUST_3 = 'DOLL_LUST_3', 'anything goes', 0
    DOLL_LUST_CORE = 'DOLL_LUST_CORE', 'sex toy', 0
    DOLL_PHYSICAL_1 = 'DOLL_PHYSICAL_1', 'hypermobility', 0
    DOLL_PHYSICAL_2 = 'DOLL_PHYSICAL_2', 'no means of reproduction', 0
    DOLL_PHYSICAL_3 = 'DOLL_PHYSICAL_3', 'organs not included', 0
    DOLL_PHYSICAL_CORE = 'DOLL_PHYSICAL_CORE', 'arcane-infused silicone', 0
    EIGHT_TAILED_YOUKO = 'EIGHT_TAILED_YOUKO', 'Eight tailed Youko', 0
    ELEMENTAL_AIR_BOOST = 'ELEMENTAL_AIR_BOOST', 'gale', 0
    ELEMENTAL_AIR_BOOST_MAJOR = 'ELEMENTAL_AIR_BOOST_MAJOR', 'storm', 0
    ELEMENTAL_AIR_BOOST_MINOR = 'ELEMENTAL_AIR_BOOST_MINOR', 'breeze', 0
    ELEMENTAL_AIR_BOOST_ULTIMATE = 'ELEMENTAL_AIR_BOOST_ULTIMATE', 'supercell', 0
    ELEMENTAL_AIR_SPELL_1 = 'ELEMENTAL_AIR_SPELL_1', 'Spell: Poison Vapours', 0
    ELEMENTAL_AIR_SPELL_1_1 = 'ELEMENTAL_AIR_SPELL_1_1', 'Upgrade: Choking Haze', 0
    ELEMENTAL_AIR_SPELL_1_2 = 'ELEMENTAL_AIR_SPELL_1_2', 'Upgrade: Arcane Sickness', 0
    ELEMENTAL_AIR_SPELL_1_3 = 'ELEMENTAL_AIR_SPELL_1_3', 'Upgrade: Weakening Cloud', 0
    ELEMENTAL_AIR_SPELL_2 = 'ELEMENTAL_AIR_SPELL_2', 'Spell: Vacuum', 0
    ELEMENTAL_AIR_SPELL_2_1 = 'ELEMENTAL_AIR_SPELL_2_1', 'Upgrade: Secondary Voids', 0
    ELEMENTAL_AIR_SPELL_2_2 = 'ELEMENTAL_AIR_SPELL_2_2', 'Upgrade: Suction', 0
    ELEMENTAL_AIR_SPELL_2_3 = 'ELEMENTAL_AIR_SPELL_2_3', 'Upgrade: Total Void', 0
    ELEMENTAL_AIR_SPELL_3 = 'ELEMENTAL_AIR_SPELL_3', 'Spell: Protective Gusts', 0
    ELEMENTAL_AIR_SPELL_3_1 = 'ELEMENTAL_AIR_SPELL_3_1', 'Upgrade: Guiding Wind', 0
    ELEMENTAL_AIR_SPELL_3_2 = 'ELEMENTAL_AIR_SPELL_3_2', 'Upgrade: Focused Blast', 0
    ELEMENTAL_AIR_SPELL_3_3 = 'ELEMENTAL_AIR_SPELL_3_3', 'Upgrade: Lingering Presence', 0
    ELEMENTAL_ARCANE_BOOST = 'ELEMENTAL_ARCANE_BOOST', 'passion', 0
    ELEMENTAL_ARCANE_BOOST_MAJOR = 'ELEMENTAL_ARCANE_BOOST_MAJOR', 'infatuation', 0
    ELEMENTAL_ARCANE_BOOST_MINOR = 'ELEMENTAL_ARCANE_BOOST_MINOR', 'arousal', 0
    ELEMENTAL_ARCANE_BOOST_ULTIMATE = 'ELEMENTAL_ARCANE_BOOST_ULTIMATE', 'nympholepsy', 0
    ELEMENTAL_ARCANE_SPELL_1 = 'ELEMENTAL_ARCANE_SPELL_1', 'Spell: Arcane Arousal', 0
    ELEMENTAL_ARCANE_SPELL_1_1 = 'ELEMENTAL_ARCANE_SPELL_1_1', 'Upgrade: Overwhelming Lust', 0
    ELEMENTAL_ARCANE_SPELL_1_2 = 'ELEMENTAL_ARCANE_SPELL_1_2', 'Upgrade: Lustful Distraction', 0
    ELEMENTAL_ARCANE_SPELL_1_3 = 'ELEMENTAL_ARCANE_SPELL_1_3', 'Upgrade: Dirty Promises', 0
    ELEMENTAL_ARCANE_SPELL_2 = 'ELEMENTAL_ARCANE_SPELL_2', 'Spell: Telepathic Communication', 0
    ELEMENTAL_ARCANE_SPELL_2_1 = 'ELEMENTAL_ARCANE_SPELL_2_1', 'Upgrade: Echoing Moans', 0
    ELEMENTAL_ARCANE_SPELL_2_2 = 'ELEMENTAL_ARCANE_SPELL_2_2', 'Upgrade: Projected Touch', 0
    ELEMENTAL_ARCANE_SPELL_2_3 = 'ELEMENTAL_ARCANE_SPELL_2_3', 'Upgrade: Power of Suggestion', 0
    ELEMENTAL_ARCANE_SPELL_3 = 'ELEMENTAL_ARCANE_SPELL_3', 'Spell: Arcane Cloud', 0
    ELEMENTAL_ARCANE_SPELL_3_1 = 'ELEMENTAL_ARCANE_SPELL_3_1', 'Upgrade: Arcane Lightning', 0
    ELEMENTAL_ARCANE_SPELL_3_2 = 'ELEMENTAL_ARCANE_SPELL_3_2', 'Upgrade: Arcane Thunder', 0
    ELEMENTAL_ARCANE_SPELL_3_3 = 'ELEMENTAL_ARCANE_SPELL_3_3', 'Upgrade: Localised Storm', 0
    ELEMENTAL_BOOST = 'ELEMENTAL_BOOST', 'elemental striker', 5
    ELEMENTAL_BOOST_ALT = 'ELEMENTAL_BOOST_ALT', 'elemental striker', 12
    ELEMENTAL_BOOST_ALT_2 = 'ELEMENTAL_BOOST_ALT_2', 'elemental striker', 12
    ELEMENTAL_CORE_OCCUPATION = 'ELEMENTAL_CORE_OCCUPATION', 'elemental', 0
    ELEMENTAL_DEFENCE_BOOST = 'ELEMENTAL_DEFENCE_BOOST', 'elemental defender', 11
    ELEMENTAL_EARTH_BOOST = 'ELEMENTAL_EARTH_BOOST', 'building pressure', 0
    ELEMENTAL_EARTH_BOOST_MAJOR = 'ELEMENTAL_EARTH_BOOST_MAJOR', 'seismic activity', 0
    ELEMENTAL_EARTH_BOOST_MINOR = 'ELEMENTAL_EARTH_BOOST_MINOR', 'impact', 0
    ELEMENTAL_EARTH_BOOST_ULTIMATE = 'ELEMENTAL_EARTH_BOOST_ULTIMATE', 'epicentre', 0
    ELEMENTAL_EARTH_SPELL_1 = 'ELEMENTAL_EARTH_SPELL_1', 'Spell: Slam', 0
    ELEMENTAL_EARTH_SPELL_1_1 = 'ELEMENTAL_EARTH_SPELL_1_1', 'Upgrade: Ground Shake', 0
    ELEMENTAL_EARTH_SPELL_1_2 = 'ELEMENTAL_EARTH_SPELL_1_2', 'Upgrade: Aftershock', 0
    ELEMENTAL_EARTH_SPELL_1_3 = 'ELEMENTAL_EARTH_SPELL_1_3', 'Upgrade: Earthquake', 0
    ELEMENTAL_EARTH_SPELL_2 = 'ELEMENTAL_EARTH_SPELL_2', 'Spell: Telekinetic Shower', 0
    ELEMENTAL_EARTH_SPELL_2_1 = 'ELEMENTAL_EARTH_SPELL_2_1', 'Upgrade: Mind Over Matter', 0
    ELEMENTAL_EARTH_SPELL_2_2 = 'ELEMENTAL_EARTH_SPELL_2_2', 'Upgrade: Precision Strikes', 0
    ELEMENTAL_EARTH_SPELL_2_3 = 'ELEMENTAL_EARTH_SPELL_2_3', 'Upgrade: Unseen Force', 0
    ELEMENTAL_EARTH_SPELL_3 = 'ELEMENTAL_EARTH_SPELL_3', 'Spell: Stone Shell', 0
    ELEMENTAL_EARTH_SPELL_3_1 = 'ELEMENTAL_EARTH_SPELL_3_1', 'Upgrade: Shifting Sands', 0
    ELEMENTAL_EARTH_SPELL_3_2 = 'ELEMENTAL_EARTH_SPELL_3_2', 'Upgrade: Hardened Carapace', 0
    ELEMENTAL_EARTH_SPELL_3_3 = 'ELEMENTAL_EARTH_SPELL_3_3', 'Upgrade: Explosive Finish', 0
    ELEMENTAL_FIRE_BOOST = 'ELEMENTAL_FIRE_BOOST', 'ablaze', 0
    ELEMENTAL_FIRE_BOOST_MAJOR = 'ELEMENTAL_FIRE_BOOST_MAJOR', 'conflagration', 0
    ELEMENTAL_FIRE_BOOST_MINOR = 'ELEMENTAL_FIRE_BOOST_MINOR', 'ignition', 0
    ELEMENTAL_FIRE_BOOST_ULTIMATE = 'ELEMENTAL_FIRE_BOOST_ULTIMATE', 'incineration', 0
    ELEMENTAL_FIRE_SPELL_1 = 'ELEMENTAL_FIRE_SPELL_1', 'Spell: Fireball', 0
    ELEMENTAL_FIRE_SPELL_1_1 = 'ELEMENTAL_FIRE_SPELL_1_1', 'Upgrade: Lingering Flames', 0
    ELEMENTAL_FIRE_SPELL_1_2 = 'ELEMENTAL_FIRE_SPELL_1_2', 'Upgrade: Twin Comets', 0
    ELEMENTAL_FIRE_SPELL_1_3 = 'ELEMENTAL_FIRE_SPELL_1_3', 'Upgrade: Burning Fury', 0
    ELEMENTAL_FIRE_SPELL_2 = 'ELEMENTAL_FIRE_SPELL_2', 'Spell: Flash', 0
    ELEMENTAL_FIRE_SPELL_2_1 = 'ELEMENTAL_FIRE_SPELL_2_1', 'Upgrade: Secondary Sparks', 0
    ELEMENTAL_FIRE_SPELL_2_2 = 'ELEMENTAL_FIRE_SPELL_2_2', 'Upgrade: Arcing Flash', 0
    ELEMENTAL_FIRE_SPELL_2_3 = 'ELEMENTAL_FIRE_SPELL_2_3', 'Upgrade: Efficient Burn', 0
    ELEMENTAL_FIRE_SPELL_3 = 'ELEMENTAL_FIRE_SPELL_3', 'Spell: Cloak of Flames', 0
    ELEMENTAL_FIRE_SPELL_3_1 = 'ELEMENTAL_FIRE_SPELL_3_1', 'Upgrade: Incendiary', 0
    ELEMENTAL_FIRE_SPELL_3_2 = 'ELEMENTAL_FIRE_SPELL_3_2', 'Upgrade: Inferno', 0
    ELEMENTAL_FIRE_SPELL_3_3 = 'ELEMENTAL_FIRE_SPELL_3_3', 'Upgrade: Ring of Fire', 0
    ELEMENTAL_WATER_BOOST = 'ELEMENTAL_WATER_BOOST', 'frost', 0
    ELEMENTAL_WATER_BOOST_MAJOR = 'ELEMENTAL_WATER_BOOST_MAJOR', 'freeze', 0
    ELEMENTAL_WATER_BOOST_MINOR = 'ELEMENTAL_WATER_BOOST_MINOR', 'chill', 0
    ELEMENTAL_WATER_BOOST_ULTIMATE = 'ELEMENTAL_WATER_BOOST_ULTIMATE', 'ice-age', 0
    ELEMENTAL_WATER_SPELL_1 = 'ELEMENTAL_WATER_SPELL_1', 'Spell: Ice Shard', 0
    ELEMENTAL_WATER_SPELL_1_1 = 'ELEMENTAL_WATER_SPELL_1_1', 'Upgrade: Freezing Fog', 0
    ELEMENTAL_WATER_SPELL_1_2 = 'ELEMENTAL_WATER_SPELL_1_2', 'Upgrade: Cold Snap', 0
    ELEMENTAL_WATER_SPELL_1_3 = 'ELEMENTAL_WATER_SPELL_1_3', 'Upgrade: Deep Freeze', 0
    ELEMENTAL_WATER_SPELL_2 = 'ELEMENTAL_WATER_SPELL_2', 'Spell: Rain Cloud', 0
    ELEMENTAL_WATER_SPELL_2_1 = 'ELEMENTAL_WATER_SPELL_2_1', 'Upgrade: Deep Chill', 0
    ELEMENTAL_WATER_SPELL_2_2 = 'ELEMENTAL_WATER_SPELL_2_2', 'Upgrade: Downpour', 0
    ELEMENTAL_WATER_SPELL_2_3 = 'ELEMENTAL_WATER_SPELL_2_3', 'Upgrade: Cloud Burst', 0
    ELEMENTAL_WATER_SPELL_3 = 'ELEMENTAL_WATER_SPELL_3', 'Spell: Soothing Waters', 0
    ELEMENTAL_WATER_SPELL_3_1 = 'ELEMENTAL_WATER_SPELL_3_1', 'Upgrade: Arcane Springs', 0
    ELEMENTAL_WATER_SPELL_3_2 = 'ELEMENTAL_WATER_SPELL_3_2', 'Upgrade: Rejuvenation', 0
    ELEMENTAL_WATER_SPELL_3_3 = 'ELEMENTAL_WATER_SPELL_3_3', 'Upgrade: Bouncing Orbs', 0
    ENCHANTMENT_STABILITY = 'ENCHANTMENT_STABILITY', 'stable enchantments', 2
    ENCHANTMENT_STABILITY_ALT = 'ENCHANTMENT_STABILITY_ALT', 'stable enchantments', 2
    ENERGY_BOOST = 'ENERGY_BOOST', 'energy reserves', 3
    ENERGY_BOOST_DRAIN_DAMAGE = 'ENERGY_BOOST_DRAIN_DAMAGE', 'aura shielding', 7
    FEMALE_ATTRACTION = 'FEMALE_ATTRACTION', 'ladykiller', 6
    FEROCIOUS_WARRIOR = 'FEROCIOUS_WARRIOR', 'ferocious warrior', 10
    FERTILITY_BOOST = 'FERTILITY_BOOST', 'fertile', 2
    FERTILITY_MAJOR_BOOST = 'FERTILITY_MAJOR_BOOST', 'fertile', 3
    FETISH_BROODMOTHER = 'FETISH_BROODMOTHER', 'broodmother', 4
    FETISH_SEEDER = 'FETISH_SEEDER', 'seeder', 4
    FIRE_ENHANCEMENT = 'FIRE_ENHANCEMENT', 'firebrand', 0
    FIRE_ENHANCEMENT_2 = 'FIRE_ENHANCEMENT_2', 'incendiary', 0
    FIRING_BLANKS = 'FIRING_BLANKS', 'sterile', 2
    FIVE_TAILED_YOUKO = 'FIVE_TAILED_YOUKO', 'Five tailed Youko', 0
    FOUR_TAILED_YOUKO = 'FOUR_TAILED_YOUKO', 'Four tailed Youko', 0
    HEAVY_SLEEPER = 'HEAVY_SLEEPER', 'heavy sleeper', 2
    HERO_OF_THEMISCYRA = 'HERO_OF_THEMISCYRA', 'Hero of Themiscyra', 0
    HYPERMOBILITY = 'HYPERMOBILITY', 'hypermobility', 6
    IMP_SLAYER = 'IMP_SLAYER', 'doomguy', 0
    JOB_AMAZONIAN = 'JOB_AMAZONIAN', 'Girl Power', 0
    JOB_AMAZONIAN_QUEEN = 'JOB_AMAZONIAN_QUEEN', 'Queen of Queens', 0
    JOB_ARISTOCRAT = 'JOB_ARISTOCRAT', 'Blue Blood', 0
    JOB_ATHLETE = 'JOB_ATHLETE', 'Ten-Second Barrier', 0
    JOB_BOUNTY_HUNTER = 'JOB_BOUNTY_HUNTER', 'No Escape', 0
    JOB_BUTLER = 'JOB_BUTLER', 'Legacy of Jeeves', 0
    JOB_CAPTIVE = 'JOB_CAPTIVE', 'Kidnapped', 0
    JOB_CHEF = 'JOB_CHEF', 'Fine Taste', 0
    JOB_CONSTRUCTION_WORKER = 'JOB_CONSTRUCTION_WORKER', 'Builder', 0
    JOB_CONSTRUCTION_WORKER_ARCANE = 'JOB_CONSTRUCTION_WORKER_ARCANE', 'Matter manipulation', 0
    JOB_ELDER_LILIN = 'JOB_ELDER_LILIN', 'untouchable', 0
    JOB_ELDER_LILIN_PAWN = 'JOB_ELDER_LILIN_PAWN', 'A loyal pawn', 0
    JOB_EPONA = 'JOB_EPONA', 'fertility queen', 0
    JOB_GANG_BODY_GUARD = 'JOB_GANG_BODY_GUARD', 'sharpest fangs', 0
    JOB_GANG_LEADER = 'JOB_GANG_LEADER', 'ruthless leadership', 0
    JOB_GANG_MEMBER = 'JOB_GANG_MEMBER', 'one of us', 0
    JOB_LUNETTE_HERD = 'JOB_LUNETTE_HERD', 'Merciless Raider', 0
    JOB_LUNETTE_RECOGNISED_DAUGHTER = 'JOB_LUNETTE_RECOGNISED_DAUGHTER', "Lunette's favourite", 0
    JOB_LYSSIETH_GUARD = 'JOB_LYSSIETH_GUARD', 'dutiful daughter', 0
    JOB_MAID = 'JOB_MAID', 'Housekeeper', 0
    JOB_MISC = 'JOB_MISC', 'Misc', 0
    JOB_MUGGER = 'JOB_MUGGER', 'Outlaw', 0
    JOB_MUSICIAN = 'JOB_MUSICIAN', 'Arcane Composition', 0
    JOB_NPC_ARCANE_RESEARCHER = 'JOB_NPC_ARCANE_RESEARCHER', 'secrets of the arcane', 0
    JOB_NPC_ASSISTANT = 'JOB_NPC_ASSISTANT', 'A Helping Hand', 0
    JOB_NPC_BARMAID = 'JOB_NPC_BARMAID', 'barman', 0
    JOB_NPC_BEAUTICIAN = 'JOB_NPC_BEAUTICIAN', 'beauty ideal', 0
    JOB_NPC_BOUNCER = 'JOB_NPC_BOUNCER', 'bouncer', 0
    JOB_NPC_CULTIST = 'JOB_NPC_CULTIST', 'Worshipper of Lilith', 0
    JOB_NPC_ENFORCER_ORICL_CONSTABLE = 'JOB_NPC_ENFORCER_ORICL_CONSTABLE', 'Enforcer: ORICL Constable', 0
    JOB_NPC_ENFORCER_ORICL_INSPECTOR = 'JOB_NPC_ENFORCER_ORICL_INSPECTOR', 'Enforcer: ORICL Inspector', 0
    JOB_NPC_ENFORCER_ORICL_SERGEANT = 'JOB_NPC_ENFORCER_ORICL_SERGEANT', 'Enforcer: ORICL Sergeant', 0
    JOB_NPC_ENFORCER_PATROL_CONSTABLE = 'JOB_NPC_ENFORCER_PATROL_CONSTABLE', 'Enforcer: Patrol Constable', 0
    JOB_NPC_ENFORCER_PATROL_INSPECTOR = 'JOB_NPC_ENFORCER_PATROL_INSPECTOR', 'Enforcer: Patrol Inspector', 0
    JOB_NPC_ENFORCER_PATROL_SERGEANT = 'JOB_NPC_ENFORCER_PATROL_SERGEANT', 'Enforcer: Patrol Sergeant', 0
    JOB_NPC_ENFORCER_SWORD_CHIEF_INSPECTOR = 'JOB_NPC_ENFORCER_SWORD_CHIEF_INSPECTOR', 'Enforcer: SWORD Chief Inspector', 0
    JOB_NPC_ENFORCER_SWORD_CONSTABLE = 'JOB_NPC_ENFORCER_SWORD_CONSTABLE', 'Enforcer: SWORD Constable', 0
    JOB_NPC_ENFORCER_SWORD_INSPECTOR = 'JOB_NPC_ENFORCER_SWORD_INSPECTOR', 'Enforcer: SWORD Inspector', 0
    JOB_NPC_ENFORCER_SWORD_SERGEANT = 'JOB_NPC_ENFORCER_SWORD_SERGEANT', 'Enforcer: SWORD Sergeant', 0
    JOB_NPC_ENFORCER_SWORD_SUPER = 'JOB_NPC_ENFORCER_SWORD_SUPER', 'Enforcer: SWORD Superintendent', 0
    JOB_NPC_FARMER = 'JOB_NPC_FARMER', 'Feeding the World', 0
    JOB_NPC_HARPY_FLOCK_MEMBER = 'JOB_NPC_HARPY_FLOCK_MEMBER', 'pecking order', 0
    JOB_NPC_HARPY_MATRIARCH = 'JOB_NPC_HARPY_MATRIARCH', 'queen of the skies', 0
    JOB_NPC_JOURNALIST = 'JOB_NPC_JOURNALIST', 'Walk the Line', 0
    JOB_NPC_MAYOR = 'JOB_NPC_MAYOR', 'Weight of Responsibility', 0
    JOB_NPC_MUSHROOM_FORAGER = 'JOB_NPC_MUSHROOM_FORAGER', 'Masterful Mycologist', 0
    JOB_NPC_NIGHTCLUB_OWNER = 'JOB_NPC_NIGHTCLUB_OWNER', 'the boss', 0
    JOB_NPC_OFFICE_WORKER = 'JOB_NPC_OFFICE_WORKER', "It's just good business", 0
    JOB_NPC_REBEL_FIGHTER = 'JOB_NPC_REBEL_FIGHTER', 'rebel fighter', 0
    JOB_NPC_REINDEER_OVERSEER = 'JOB_NPC_REINDEER_OVERSEER', 'winter-proof', 0
    JOB_NPC_SHOP_MANAGER = 'JOB_NPC_SHOP_MANAGER', 'manager', 0
    JOB_NPC_SLAVER_ADMIN = 'JOB_NPC_SLAVER_ADMIN', 'shady overseer', 0
    JOB_NPC_SLIME_QUEEN = 'JOB_NPC_SLIME_QUEEN', 'royal jelly', 0
    JOB_NPC_SLIME_QUEEN_GUARD = 'JOB_NPC_SLIME_QUEEN_GUARD', 'slime servant', 0
    JOB_NPC_STABLE_MISTRESS = 'JOB_NPC_STABLE_MISTRESS', 'horse trainer', 0
    JOB_OFFICE_WORKER = 'JOB_OFFICE_WORKER', 'The Salaryman', 0
    JOB_PLAYER_CONSTRUCTION_WORKER = 'JOB_PLAYER_CONSTRUCTION_WORKER', 'Project Manager', 0
    JOB_PROSTITUTE = 'JOB_PROSTITUTE', 'The oldest profession', 0
    JOB_PUGILIST = 'JOB_PUGILIST', 'pugalist', 0
    JOB_SEX_DOLL = 'JOB_SEX_DOLL', 'dutiful doll', 0
    JOB_SLAVE = 'JOB_SLAVE', 'A life of servitude', 0
    JOB_SOLDIER = 'JOB_SOLDIER', 'Controlled Aggression', 0
    JOB_STUDENT = 'JOB_STUDENT', 'Student Discount', 0
    JOB_TAUR_TRANSPORT = 'JOB_TAUR_TRANSPORT', 'Keep on pulling!', 0
    JOB_TEACHER = 'JOB_TEACHER', 'In Control', 0
    JOB_TOURIST = 'JOB_TOURIST', "I'm an American!", 0
    JOB_UNEMPLOYED = 'JOB_UNEMPLOYED', 'NEET', 0
    JOB_WRITER = 'JOB_WRITER', 'Meditations', 0
    LEWD_KNOWLEDGE = 'LEWD_KNOWLEDGE', 'lewd knowledge', 1
    LUSTPYRE = 'LUSTPYRE', 'lustpyre', 10
    MALE_ATTRACTION = 'MALE_ATTRACTION', 'minx', 6
    MARTIAL_ARTIST = 'MARTIAL_ARTIST', 'martial artist', 0
    MELEE_DAMAGE = 'MELEE_DAMAGE', 'melee weapons expert', 8
    NINE_TAILED_YOUKO = 'NINE_TAILED_YOUKO', 'Nine tailed Youko', 0
    NYMPHOMANIAC = 'NYMPHOMANIAC', 'nymphomaniac', 10
    OBJECT_OF_DESIRE = 'OBJECT_OF_DESIRE', 'object of desire', 8
    OBSERVANT = 'OBSERVANT', 'observant', 2
    ORGASMIC_LEVEL_DRAIN = 'ORGASMIC_LEVEL_DRAIN', 'orgasmic level drain', 3
    PHYSICAL_BASE = 'PHYSICAL_BASE', 'natural fitness', 1
    PHYSICAL_DAMAGE = 'PHYSICAL_DAMAGE', 'striker', 3
    PHYSICAL_DEFENCE = 'PHYSICAL_DEFENCE', 'defender', 3
    PHYSIQUE_BOOST = 'PHYSIQUE_BOOST', 'physically fit', 2
    PHYSIQUE_BOOST_ALT = 'PHYSIQUE_BOOST_ALT', 'physically fit', 0
    PHYSIQUE_BOOST_MAJOR = 'PHYSIQUE_BOOST_MAJOR', 'physically fit', 6
    PIX_TRAINING = 'PIX_TRAINING', "Pix's Training", 0
    POISON_ENHANCEMENT = 'POISON_ENHANCEMENT', 'venomous', 0
    POISON_ENHANCEMENT_2 = 'POISON_ENHANCEMENT_2', 'toxic', 0
    POWER_OF_LASIELLE_3 = 'POWER_OF_LASIELLE_3', "Lasielle's power", 0
    POWER_OF_LASIELLE_3_DEMON = 'POWER_OF_LASIELLE_3_DEMON', "Lasielle's true power", 0
    POWER_OF_LIRECEA_1 = 'POWER_OF_LIRECEA_1', "Lirecea's power", 0
    POWER_OF_LIRECEA_1_DEMON = 'POWER_OF_LIRECEA_1_DEMON', "Lirecea's true power", 0
    POWER_OF_LISOPHIA_7 = 'POWER_OF_LISOPHIA_7', "Lisophia's power", 0
    POWER_OF_LISOPHIA_7_DEMON = 'POWER_OF_LISOPHIA_7_DEMON', "Lisophia's true power", 0
    POWER_OF_LOVIENNE_2 = 'POWER_OF_LOVIENNE_2', "Lovienne's power", 0
    POWER_OF_LOVIENNE_2_DEMON = 'POWER_OF_LOVIENNE_2_DEMON', "Lovienne's true power", 0
    POWER_OF_LUNETTE_5 = 'POWER_OF_LUNETTE_5', "Lunette's power", 0
    POWER_OF_LUNETTE_5_DEMON = 'POWER_OF_LUNETTE_5_DEMON', "Lunette's true power", 0
    POWER_OF_LYSSIETH_4 = 'POWER_OF_LYSSIETH_4', "Lyssieth's power", 0
    POWER_OF_LYSSIETH_4_DEMON = 'POWER_OF_LYSSIETH_4_DEMON', "Lyssieth's true power", 0
    POWER_OF_LYXIAS_6 = 'POWER_OF_LYXIAS_6', "Lyxias's power", 0
    POWER_OF_LYXIAS_6_DEMON = 'POWER_OF_LYXIAS_6_DEMON', "Lyxias's true power", 0
    PURE_MIND = 'PURE_MIND', 'pure thoughts', 10
    RANGED_DAMAGE = 'RANGED_DAMAGE', 'sharp-shooter', 9
    RUNNER = 'RUNNER', 'runner', 0
    RUNNER_2 = 'RUNNER_2', 'Cardio King', 5
    SACRIFICIAL_SHIELDING = 'SACRIFICIAL_SHIELDING', 'sacrificial shielding', 10
    SEDUCTION_BOOST = 'SEDUCTION_BOOST', 'seductive', 2
    SEDUCTION_BOOST_ALT = 'SEDUCTION_BOOST_ALT', 'seductive', 7
    SEDUCTION_BOOST_ALT_2 = 'SEDUCTION_BOOST_ALT_2', 'seductive', 0
    SEDUCTION_BOOST_MAJOR = 'SEDUCTION_BOOST_MAJOR', 'seductive', 6
    SEDUCTION_DEFENCE_BOOST = 'SEDUCTION_DEFENCE_BOOST', 'resistance', 3
    SEVEN_TAILED_YOUKO = 'SEVEN_TAILED_YOUKO', 'Seven tailed Youko', 0
    SINGLE_TAILED_YOUKO = 'SINGLE_TAILED_YOUKO', 'Single tailed Youko', 0
    SIX_TAILED_YOUKO = 'SIX_TAILED_YOUKO', 'Six tailed Youko', 0
    SPECIAL_ARCANE_ALLERGY = 'SPECIAL_ARCANE_ALLERGY', 'arcane allergy', 0
    SPECIAL_ARCANE_LIGHTNING = 'SPECIAL_ARCANE_LIGHTNING', 'exceptional arcanist', 0
    SPECIAL_ARCANE_TATTOOIST = 'SPECIAL_ARCANE_TATTOOIST', 'Arcane Tattooist', 0
    SPECIAL_ARCANE_TRAINING = 'SPECIAL_ARCANE_TRAINING', 'arcane training', 0
    SPECIAL_CHILD_OF_THE_CRAG = 'SPECIAL_CHILD_OF_THE_CRAG', 'Child of the Crag', 0
    SPECIAL_CLOTHING_FEMININITY_INDIFFERENCE = 'SPECIAL_CLOTHING_FEMININITY_INDIFFERENCE', 'feminine clothing indifference', 0
    SPECIAL_CLOTHING_MASCULINITY_INDIFFERENCE = 'SPECIAL_CLOTHING_MASCULINITY_INDIFFERENCE', 'masculine clothing indifference', 0
    SPECIAL_DIRTY_MINDED = 'SPECIAL_DIRTY_MINDED', 'dirty-minded', 0
    SPECIAL_ENFORCER_FIREARMS_TRAINING = 'SPECIAL_ENFORCER_FIREARMS_TRAINING', 'Firearms Mastery', 0
    SPECIAL_HEALTH_FANATIC = 'SPECIAL_HEALTH_FANATIC', 'health fanatic', 0
    SPECIAL_MARTIAL_BACKGROUND = 'SPECIAL_MARTIAL_BACKGROUND', 'martial background', 0
    SPECIAL_MEGA_SLUT = 'SPECIAL_MEGA_SLUT', 'debauched', 0
    SPECIAL_MELEE_EXPERT = 'SPECIAL_MELEE_EXPERT', 'melee expert', 0
    SPECIAL_MERAXIS = 'SPECIAL_MERAXIS', 'The Dark Siren', 0
    SPECIAL_PLAYER = 'SPECIAL_PLAYER', 'abnormal aura', 0
    SPECIAL_RANGED_EXPERT = 'SPECIAL_RANGED_EXPERT', 'ranged expert', 0
    SPECIAL_SLUT = 'SPECIAL_SLUT', 'slut', 0
    SPELL_DAMAGE = 'SPELL_DAMAGE', 'spell power', 3
    SPELL_DAMAGE_MAJOR = 'SPELL_DAMAGE_MAJOR', 'spell mastery', 9
    SPELL_EFFICIENCY = 'SPELL_EFFICIENCY', 'spell efficiency', 3
    THREE_TAILED_YOUKO = 'THREE_TAILED_YOUKO', 'Three tailed Youko', 0
    TWO_TAILED_YOUKO = 'TWO_TAILED_YOUKO', 'Two tailed Youko', 0
    UNARMED_DAMAGE = 'UNARMED_DAMAGE', 'hand-to-hand', 5
    UNARMED_TRAINING = 'UNARMED_TRAINING', 'brawler', 5
    VIRILITY_BOOST = 'VIRILITY_BOOST', 'virile', 2
    VIRILITY_MAJOR_BOOST = 'VIRILITY_MAJOR_BOOST', 'virile', 3
    WEAPON_ENCHANTER = 'WEAPON_ENCHANTER', 'arcane smith', 4

    def getId(self) ->str:
        return self.id

    def getRealname(self) ->str:
        return self.realname

    def getRow(self) ->int:
        return self.row

    def __init__(self, id: str, realname: str, row: int) ->None:
        self.id: str = id
        self.realname: str = realname
        self.row: int = row

from enum import IntEnum
__all__ = ['EOccupation']


class EOccupation(IntEnum):
    ELEMENTAL = 0
    NPC_ENFORCER_PATROL_INSPECTOR = 1
    NPC_ENFORCER_PATROL_SERGEANT = 2
    NPC_ENFORCER_PATROL_CONSTABLE = 3
    NPC_ENFORCER_SWORD_SUPER = 4
    NPC_ENFORCER_SWORD_CHIEF_INSPECTOR = 5
    NPC_ENFORCER_SWORD_INSPECTOR = 6
    NPC_ENFORCER_SWORD_SERGEANT = 7
    NPC_ENFORCER_SWORD_CONSTABLE = 8
    NPC_ENFORCER_ORICL_INSPECTOR = 9
    NPC_ENFORCER_ORICL_SERGEANT = 10
    NPC_ENFORCER_ORICL_CONSTABLE = 11
    NPC_HARPY_MATRIARCH = 12
    NPC_HARPY_FLOCK_MEMBER = 13
    NPC_CULTIST = 14
    NPC_SLAVER_ADMIN = 15
    NPC_NIGHTCLUB_OWNER = 16
    NPC_BAR_TENDER = 17
    NPC_BOUNCER = 18
    NPC_BEAUTICIAN = 19
    NPC_ARCANE_RESEARCHER = 20
    NPC_CLOTHING_STORE_OWNER = 21
    NPC_GYM_OWNER = 22
    NPC_STORE_OWNER = 23
    NPC_CASINO_OWNER = 24
    NPC_BUSINESS_OWNER = 25
    NPC_TAVERN_OWNER = 26
    NPC_FARMER = 27
    NPC_JOURNALIST = 28
    REINDEER_OVERSEER = 29
    NPC_SLIME_QUEEN = 30
    NPC_SLIME_QUEEN_GUARD = 31
    NPC_EPONA = 32
    NPC_GANG_LEADER = 33
    NPC_GANG_BODY_GUARD = 34
    NPC_GANG_MEMBER = 35
    NPC_STABLE_MISTRESS = 36
    NPC_LYSSIETH_GUARD = 37
    NPC_ELDER_LILIN = 38
    NPC_TAUR_TRANSPORT = 39
    NPC_ELIS_MAYOR = 40
    NPC_ASSISTANT = 41
    NPC_LUNETTE_HERD = 42
    NPC_MUSHROOM_FORAGER = 43
    NPC_LUNETTE_RECOGNISED_DAUGHTER = 44
    NPC_AMAZONIAN_QUEEN = 45
    NPC_AMAZONIAN = 46
    NPC_PUGILIST = 47
    NPC_LILIN_PAWN = 48
    NPC_SEX_DOLL = 49
    NPC_UNEMPLOYED = 50
    NPC_SLAVE = 51
    NPC_CAPTIVE = 52
    NPC_REBEL_FIGHTER = 53
    NPC_PROSTITUTE = 54
    NPC_STRIPPER = 55
    NPC_MASSAGE_THERAPIST = 56
    NPC_WAITRESS = 57
    NPC_MUSICIAN = 58
    NPC_FITNESS_INSTRUCTOR = 59
    NPC_MUGGER = 60
    NPC_BOUNTY_HUNTER = 61
    NPC_CONSTRUCTION_WORKER = 62
    NPC_CONSTRUCTION_WORKER_ARCANE = 63
    NPC_MECHANIC = 64
    NPC_TEACHER = 65
    NPC_LIBRARIAN = 66
    NPC_UNIVERSITY_STUDENT = 67
    NPC_WRITER = 68
    NPC_ENGINEER = 69
    NPC_ARCHITECT = 70
    NPC_DOCTOR = 71
    NPC_MAID = 72
    NPC_BUTLER = 73
    NPC_OFFICE_WORKER = 74
    NPC_RECEPTIONIST = 75
    NPC_SHOP_ASSISTANT = 76
    NPC_ARTIST = 77
    NPC_NURSE = 78
    NPC_CHEF = 79
    NPC_ATHLETE = 80
    NPC_MODEL = 81
    NPC_TRADER = 82
    UNEMPLOYED = 83
    OFFICE_WORKER = 84
    STUDENT = 85
    MUSICIAN = 86
    TEACHER = 87
    WRITER = 88
    CHEF = 89
    CONSTRUCTION_WORKER = 90
    SOLDIER = 91
    ATHLETE = 92
    ARISTOCRAT = 93
    MAID = 94
    BUTLER = 95
    TOURIST = 96

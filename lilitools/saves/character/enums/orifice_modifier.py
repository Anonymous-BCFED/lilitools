from enum import IntEnum
__all__ = ['EOrificeModifier']


class EOrificeModifier(IntEnum):
    PUFFY = 0
    RIBBED = 1
    TENTACLED = 2
    MUSCLE_CONTROL = 3

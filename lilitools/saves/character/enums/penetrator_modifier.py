from enum import IntEnum
__all__ = ['EPenetratorModifier']


class EPenetratorModifier(IntEnum):
    SHEATHED = 0
    RIBBED = 1
    TENTACLED = 2
    KNOTTED = 3
    BLUNT = 4
    TAPERED = 5
    FLARED = 6
    BARBED = 7
    VEINY = 8
    PREHENSILE = 9
    OVIPOSITOR = 10

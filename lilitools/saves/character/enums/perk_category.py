from enum import IntEnum
__all__ = ['EPerkCategory']


class EPerkCategory(IntEnum):
    JOB = 0
    PHYSICAL = 1
    LUST = 2
    ARCANE = 3
    PHYSICAL_EARTH = 4
    PHYSICAL_WATER = 5
    ARCANE_FIRE = 6
    ARCANE_AIR = 7

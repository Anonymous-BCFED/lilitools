from enum import IntEnum
__all__ = ['EPersonalityTrait']


class EPersonalityTrait(IntEnum):
    CONFIDENT = 0
    SHY = 1
    KIND = 2
    SELFISH = 3
    NAIVE = 4
    CYNICAL = 5
    BRAVE = 6
    COWARDLY = 7
    LEWD = 8
    INNOCENT = 9
    PRUDE = 10
    LISP = 11
    STUTTER = 12
    MUTE = 13
    SLOVENLY = 14

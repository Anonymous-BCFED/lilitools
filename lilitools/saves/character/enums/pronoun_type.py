from enum import IntEnum
__all__ = ['EPronounType']


class EPronounType(IntEnum):
    FEMININE = 0
    NEUTRAL = 1
    MASCULINE = 2

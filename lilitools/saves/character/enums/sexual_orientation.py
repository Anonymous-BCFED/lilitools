from enum import IntEnum
__all__ = ['ESexualOrientation']


class ESexualOrientation(IntEnum):
    ANDROPHILIC = 0
    AMBIPHILIC = 1
    GYNEPHILIC = 2

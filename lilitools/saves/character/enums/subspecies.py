from enum import IntEnum
__all__ = ['ESubspecies']


class ESubspecies(IntEnum):
    HUMAN = 0
    ANGEL = 1
    ELDER_LILIN = 2
    LILIN = 3
    DEMON = 4
    HALF_DEMON = 5
    IMP = 6
    IMP_ALPHA = 7
    COW_MORPH = 8
    DOG_MORPH = 9
    DOG_MORPH_BORDER_COLLIE = 10
    DOG_MORPH_DOBERMANN = 11
    DOG_MORPH_GERMAN_SHEPHERD = 12
    WOLF_MORPH = 13
    FOX_MORPH = 14
    FOX_MORPH_ARCTIC = 15
    FOX_MORPH_FENNEC = 16
    FOX_ASCENDANT = 17
    FOX_ASCENDANT_ARCTIC = 18
    FOX_ASCENDANT_FENNEC = 19
    CAT_MORPH = 20
    CAT_MORPH_LYNX = 21
    CAT_MORPH_CHEETAH = 22
    CAT_MORPH_CARACAL = 23
    HORSE_MORPH = 24
    HORSE_MORPH_UNICORN = 25
    HORSE_MORPH_PEGASUS = 26
    HORSE_MORPH_ALICORN = 27
    CENTAUR = 28
    PEGATAUR = 29
    UNITAUR = 30
    ALITAUR = 31
    HORSE_MORPH_ZEBRA = 32
    HORSE_MORPH_DONKEY = 33
    REINDEER_MORPH = 34
    ALLIGATOR_MORPH = 35
    SQUIRREL_MORPH = 36
    RAT_MORPH = 37
    RABBIT_MORPH = 38
    RABBIT_MORPH_LOP = 39
    BAT_MORPH = 40
    HARPY = 41
    HARPY_RAVEN = 42
    HARPY_SWAN = 43
    HARPY_PHOENIX = 44
    SLIME = 45
    DOLL = 46
    ELEMENTAL_FIRE = 47
    ELEMENTAL_EARTH = 48
    ELEMENTAL_WATER = 49
    ELEMENTAL_AIR = 50
    ELEMENTAL_ARCANE = 51

from lilitools.saves.character._savables.exported_character import RawExportedCharacter
from lilitools.saves.character.addiction.addictions_core import AddictionsCore
from lilitools.saves.character.companions import Companions
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.character.location_information import LocationInformation
from lilitools.saves.character.slavery_info import SlaveryInfo
from lilitools.saves.inventory.character_inventory import CharacterInventory


class ExportedCharacter(RawExportedCharacter):
    TAG = "exportedCharacter"

    def __init__(self) -> None:
        super().__init__()

    @classmethod
    def FromCharacter(cls, char: GameCharacter) -> "ExportedCharacter":
        export = cls()
        export.character = char.getCharacterForExport()

        # Probably not necessary but it's cleaner.
        export.character.addictionsCore = AddictionsCore()
        export.character.characterInventory = CharacterInventory()
        export.character.companions = Companions()
        export.character.core.areasKnownByCharacters.clear()
        export.character.core.captive=False
        export.character.core.creampieRetentionAreas.clear()
        export.character.core.elemental=''
        export.character.core.elementalSummoned=False
        export.character.core.foughtPlayerCount=0
        export.character.core.lostCombatCount=0
        export.character.core.muskMarker=None
        export.character.core.wonCombatCount=0
        export.character.holdingClothing.clear()
        export.character.locationInformation=LocationInformation()
        export.character.potionAttributes.clear()
        export.character.relationships.clear()
        export.character.slavery = SlaveryInfo()

        return export

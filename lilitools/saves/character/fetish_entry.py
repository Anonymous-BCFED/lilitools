from lilitools.saves.character._savables.fetish_entry import RawFetishEntry
from lilitools.saves.character.enums.fetish_desire import EFetishDesire


class FetishEntry(RawFetishEntry):
    def __init__(self,id:str='',hasFetish:bool=False,desire:EFetishDesire=EFetishDesire(2),xp:int=0) -> None:
        super().__init__()
        self.hasFetish: bool = hasFetish
        self.desire: EFetishDesire = desire
        self.xp: int = xp
        self.id: str = id
    def __str__(self) -> str:
        return f'{self.id} - hasFetish: {self.hasFetish}, desire: {self.desire}, xp: {self.xp}'

from enum import Enum

__all__ = ['EFluidExpulsion']


class EFluidExpulsion(Enum):
    ZERO_NONE = 'tiny', 0, 20
    ONE_SMALL = 'small', 20, 40
    TWO_MODERATE = 'moderate', 40, 60
    THREE_LARGE = 'large', 60, 80
    FOUR_HUGE = 'huge', 80, 100

    def __init__(self, label: str, min: int, max: int) -> None:
        self.label: str = label
        self.min: int = min
        self.max: int = max

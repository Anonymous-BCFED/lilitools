#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional, Set
from lxml import etree
from lilitools.saves.character.fluids.enums.fluid_modifier import EFluidModifier
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawMilk']
## from [LT]/src/com/lilithsthrone/game/character/body/FluidMilk.java: public Element saveAsXML(String rootElementName, Element parentElement, Document doc) { @ aMe/nFz4mZP9cxgE2Yfe/Qy8SeW1sv/bdowuhjPaq0Hq8z9ErnBXv/gsqKMJCGOYRdJUErHYQ5SFrWINkYkbaw==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawMilk(Savable):
    TAG = 'milk'
    _XMLID_ATTR_FLAVOUR_ATTRIBUTE: str = 'flavour'
    _XMLID_ATTR_TYPE_ATTRIBUTE: str = 'type'
    _XMLID_TAG_MODIFIERS_CHILD: str = 'mod'

    def __init__(self) -> None:
        super().__init__()
        self.type: str = ''
        self.flavour: str = ''
        self.modifiers: Set[EFluidModifier] = set()

    def overrideAttrs(self, flavour_attribute: _Optional_str = None, type_attribute: _Optional_str = None) -> None:
        if flavour_attribute is not None:
            self._XMLID_ATTR_FLAVOUR_ATTRIBUTE = flavour_attribute
        if type_attribute is not None:
            self._XMLID_ATTR_TYPE_ATTRIBUTE = type_attribute

    def overrideTags(self, modifiers_child: _Optional_str = None) -> None:
        if modifiers_child is not None:
            self._XMLID_TAG_MODIFIERS_CHILD = modifiers_child

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        value = e.get(self._XMLID_ATTR_TYPE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_TYPE_ATTRIBUTE, 'type')
        else:
            self.type = str(value)
        value = e.get(self._XMLID_ATTR_FLAVOUR_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_FLAVOUR_ATTRIBUTE, 'flavour')
        else:
            self.flavour = str(value)
        for lc in e.iterfind(self._XMLID_TAG_MODIFIERS_CHILD):
            self.modifiers.add(EFluidModifier[lc.text])

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        e.attrib[self._XMLID_ATTR_TYPE_ATTRIBUTE] = str(self.type)
        e.attrib[self._XMLID_ATTR_FLAVOUR_ATTRIBUTE] = str(self.flavour)
        for lv in sorted(self.modifiers, key=lambda x: x.name):
            etree.SubElement(e, self._XMLID_TAG_MODIFIERS_CHILD, {}).text = lv.name
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['type'] = self.type
        data['flavour'] = self.flavour
        if self.modifiers is None or len(self.modifiers) > 0:
            data['modifiers'] = list()
        else:
            lc = list()
            if len(self.modifiers) > 0:
                for lv in sorted(lc, lambda x: x.name):
                    lc.append(lv.name)
            data['modifiers'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'type' not in data:
            raise KeyRequiredException(self, data, 'type', 'type')
        self.type = str(data['type'])
        if 'flavour' not in data:
            raise KeyRequiredException(self, data, 'flavour', 'flavour')
        self.flavour = str(data['flavour'])
        if (lv := self.modifiers) is not None:
            for le in lv:
                self.modifiers.add(le.name)
        else:
            raise KeyRequiredException(self, data, 'modifiers', 'modifiers')

#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.character.body.body import Body
from lilitools.saves.character.fluids.cum import Cum
from lilitools.saves.character.fluids.girlcum import GirlCum
from lilitools.saves.character.fluids.milk import Milk
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL, Savable
__all__ = ['RawStoredFluid']
## from [LT]/src/com/lilithsthrone/game/character/FluidStored.java: public Element saveAsXML(Element parentElement, Document doc) { @ cI4mmyQcpeFW8IGPHkRrb4CAqWwieKYF/U29EUlnWgFsYRFYiQTc4mjmbUbdS4nFV55w8LfioAfKPK6L138n0Q==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawStoredFluid(Savable):
    TAG = 'fluidStored'
    _XMLID_ATTR_BESTIAL_ATTRIBUTE: str = 'bestial'
    _XMLID_ATTR_BODY_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_CHARACTERSFLUIDID_ATTRIBUTE: str = 'charactersFluidID'
    _XMLID_ATTR_CUMVIRILE_ATTRIBUTE: str = 'cumVirile'
    _XMLID_ATTR_CUM_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_GIRLCUM_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_MILK_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_MILLILITRES_ATTRIBUTE: str = 'millilitres'
    _XMLID_ATTR_VIRILITY_ATTRIBUTE: str = 'virility'
    _XMLID_TAG_BODY_ELEMENT: str = 'body'
    _XMLID_TAG_CUM_ELEMENT: str = 'fluidCum'
    _XMLID_TAG_GIRLCUM_ELEMENT: str = 'fluidGirlCum'
    _XMLID_TAG_MILK_ELEMENT: str = 'fluidMilk'

    def __init__(self) -> None:
        super().__init__()
        self.charactersFluidID: str = ''
        self.bestial: bool = False
        self.cumVirile: bool = False
        self.virility: float = 0.
        self.millilitres: float = 0.
        self.body: Optional[Body] = None  # Element
        self.cum: Optional[Cum] = None  # Element
        self.milk: Optional[Milk] = None  # Element
        self.girlCum: Optional[GirlCum] = None  # Element

    def overrideAttrs(self, bestial_attribute: _Optional_str = None, body_attribute: _Optional_str = None, charactersFluidID_attribute: _Optional_str = None, cum_attribute: _Optional_str = None, cumVirile_attribute: _Optional_str = None, girlCum_attribute: _Optional_str = None, milk_attribute: _Optional_str = None, millilitres_attribute: _Optional_str = None, virility_attribute: _Optional_str = None) -> None:
        if bestial_attribute is not None:
            self._XMLID_ATTR_BESTIAL_ATTRIBUTE = bestial_attribute
        if body_attribute is not None:
            self._XMLID_ATTR_BODY_ATTRIBUTE = body_attribute
        if charactersFluidID_attribute is not None:
            self._XMLID_ATTR_CHARACTERSFLUIDID_ATTRIBUTE = charactersFluidID_attribute
        if cum_attribute is not None:
            self._XMLID_ATTR_CUM_ATTRIBUTE = cum_attribute
        if cumVirile_attribute is not None:
            self._XMLID_ATTR_CUMVIRILE_ATTRIBUTE = cumVirile_attribute
        if girlCum_attribute is not None:
            self._XMLID_ATTR_GIRLCUM_ATTRIBUTE = girlCum_attribute
        if milk_attribute is not None:
            self._XMLID_ATTR_MILK_ATTRIBUTE = milk_attribute
        if millilitres_attribute is not None:
            self._XMLID_ATTR_MILLILITRES_ATTRIBUTE = millilitres_attribute
        if virility_attribute is not None:
            self._XMLID_ATTR_VIRILITY_ATTRIBUTE = virility_attribute

    def overrideTags(self, body_element: _Optional_str = None, cum_element: _Optional_str = None, girlCum_element: _Optional_str = None, milk_element: _Optional_str = None) -> None:
        if body_element is not None:
            self._XMLID_TAG_BODY_ELEMENT = body_element
        if cum_element is not None:
            self._XMLID_TAG_CUM_ELEMENT = cum_element
        if girlCum_element is not None:
            self._XMLID_TAG_GIRLCUM_ELEMENT = girlCum_element
        if milk_element is not None:
            self._XMLID_TAG_MILK_ELEMENT = milk_element

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        sf: etree._Element
        value = e.get(self._XMLID_ATTR_CHARACTERSFLUIDID_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_CHARACTERSFLUIDID_ATTRIBUTE, 'charactersFluidID')
        else:
            self.charactersFluidID = str(value)
        value = e.get(self._XMLID_ATTR_BESTIAL_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_BESTIAL_ATTRIBUTE, 'bestial')
        else:
            self.bestial = XML2BOOL[value]
        value = e.get(self._XMLID_ATTR_CUMVIRILE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_CUMVIRILE_ATTRIBUTE, 'cumVirile')
        else:
            self.cumVirile = XML2BOOL[value]
        value = e.get(self._XMLID_ATTR_VIRILITY_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_VIRILITY_ATTRIBUTE, 'virility')
        else:
            self.virility = float(value)
        value = e.get(self._XMLID_ATTR_MILLILITRES_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_MILLILITRES_ATTRIBUTE, 'millilitres')
        else:
            self.millilitres = float(value)
        if (sf := e.find(self._XMLID_TAG_BODY_ELEMENT)) is not None:
            self.body = Body()
            self.body.fromXML(sf)
        if (sf := e.find(self._XMLID_TAG_CUM_ELEMENT)) is not None:
            self.cum = Cum()
            self.cum.fromXML(sf)
        if (sf := e.find(self._XMLID_TAG_MILK_ELEMENT)) is not None:
            self.milk = Milk()
            self.milk.fromXML(sf)
        if (sf := e.find(self._XMLID_TAG_GIRLCUM_ELEMENT)) is not None:
            self.girlCum = GirlCum()
            self.girlCum.fromXML(sf)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_CHARACTERSFLUIDID_ATTRIBUTE] = str(self.charactersFluidID)
        e.attrib[self._XMLID_ATTR_BESTIAL_ATTRIBUTE] = str(self.bestial).lower()
        e.attrib[self._XMLID_ATTR_CUMVIRILE_ATTRIBUTE] = str(self.cumVirile).lower()
        e.attrib[self._XMLID_ATTR_VIRILITY_ATTRIBUTE] = str(float(self.virility))
        e.attrib[self._XMLID_ATTR_MILLILITRES_ATTRIBUTE] = str(float(self.millilitres))
        if self.body is not None:
            e.append(self.body.toXML(self._XMLID_TAG_BODY_ELEMENT))
        if self.cum is not None:
            e.append(self.cum.toXML(self._XMLID_TAG_CUM_ELEMENT))
        if self.milk is not None:
            e.append(self.milk.toXML(self._XMLID_TAG_MILK_ELEMENT))
        if self.girlCum is not None:
            e.append(self.girlCum.toXML(self._XMLID_TAG_GIRLCUM_ELEMENT))
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['charactersFluidID'] = self.charactersFluidID
        data['bestial'] = bool(self.bestial)
        data['cumVirile'] = bool(self.cumVirile)
        data['virility'] = float(self.virility)
        data['millilitres'] = float(self.millilitres)
        if self.body is not None:
            data['body'] = self.body.toDict()
        if self.cum is not None:
            data['cum'] = self.cum.toDict()
        if self.milk is not None:
            data['milk'] = self.milk.toDict()
        if self.girlCum is not None:
            data['girlCum'] = self.girlCum.toDict()
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'charactersFluidID' not in data:
            raise KeyRequiredException(self, data, 'charactersFluidID', 'charactersFluidID')
        self.charactersFluidID = str(data['charactersFluidID'])
        if 'bestial' not in data:
            raise KeyRequiredException(self, data, 'bestial', 'bestial')
        self.bestial = bool(data['bestial'])
        if 'cumVirile' not in data:
            raise KeyRequiredException(self, data, 'cumVirile', 'cumVirile')
        self.cumVirile = bool(data['cumVirile'])
        if 'virility' not in data:
            raise KeyRequiredException(self, data, 'virility', 'virility')
        self.virility = float(data['virility'])
        if 'millilitres' not in data:
            raise KeyRequiredException(self, data, 'millilitres', 'millilitres')
        self.millilitres = float(data['millilitres'])
        if (sf := data.get('body')) is not None:
            self.body = Body()
            self.body.fromDict(data['body'])
        else:
            self.body = None
        if (sf := data.get('cum')) is not None:
            self.cum = Cum()
            self.cum.fromDict(data['cum'])
        else:
            self.cum = None
        if (sf := data.get('milk')) is not None:
            self.milk = Milk()
            self.milk.fromDict(data['milk'])
        else:
            self.milk = None
        if (sf := data.get('girlCum')) is not None:
            self.girlCum = GirlCum()
            self.girlCum.fromDict(data['girlCum'])
        else:
            self.girlCum = None

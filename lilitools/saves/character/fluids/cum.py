from lilitools.saves.character.fluids._savables.cum import RawCum
from lxml import etree

from lilitools.logging import ClickWrap
click = ClickWrap()
class Cum(RawCum):
    def fromXML(self, e: etree._Element) -> None:
        super().fromXML(e)
        path = e.getroottree().getpath(e)
        if self.type is None:
            click.warn(f'{path}[type] = {self.type!r}; changed to \'CUM_HUMAN\'')
            self.type = 'CUM_HUMAN'
        if self.flavour is None:
            click.warn(f'{path}[flavour] = {self.flavour!r}; changed to \'CUM\'')
            self.flavour = 'CUM'
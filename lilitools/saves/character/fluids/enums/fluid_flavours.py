from enum import IntEnum
__all__ = ['EFluidFlavour']


class EFluidFlavour(IntEnum):
    CUM = 0
    MILK = 1
    GIRL_CUM = 2
    FLAVOURLESS = 3
    BUBBLEGUM = 4
    BEER = 5
    VANILLA = 6
    STRAWBERRY = 7
    CHOCOLATE = 8
    PINEAPPLE = 9
    HONEY = 10
    MINT = 11
    CHERRY = 12
    COFFEE = 13
    TEA = 14
    MAPLE = 15
    CINNAMON = 16
    LEMON = 17
    ORANGE = 18
    GRAPE = 19
    MELON = 20
    COCONUT = 21
    BLUEBERRY = 22
    BANANA = 23

from enum import IntEnum
__all__ = ['EFluidModifier']


class EFluidModifier(IntEnum):
    VISCOUS = 0
    STICKY = 1
    SLIMY = 2
    BUBBLING = 3
    MUSKY = 4
    MINERAL_OIL = 5
    ALCOHOLIC = 6
    ALCOHOLIC_WEAK = 7
    ADDICTIVE = 8
    HALLUCINOGENIC = 9

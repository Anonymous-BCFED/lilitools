from typing import Optional
from lilitools.logging import ClickWrap
from lilitools.saves.character.body.body import Body
from lilitools.saves.character.fluids._savables.stored_fluid import RawStoredFluid

from lxml import etree

from lilitools.saves.character.fluids.cum import Cum
from lilitools.saves.character.fluids.girlcum import GirlCum
from lilitools.saves.character.fluids.milk import Milk
from lilitools.saves.savable import XML2BOOL

click = ClickWrap()


class BadFluidStateException(Exception):
    pass


class StoredFluid(RawStoredFluid):
    def toXML(self, tagOverride: str | None = None) -> etree._Element:
        e = etree.Element(
            tagOverride or "fluidStored",
            attrib={
                "charactersFluidID": self.charactersFluidID,
                "bestial": str(self.feral).lower(),
                "cumVirile": str(self.cumVirile).lower(),
                "virility": str(self.virility),
                "millilitres": str(self.millilitres),
            },
        )
        if self.cum is not None:
            e.append(self.body.toXML("body"))
            e.append(self.cum.toXML("fluidCum"))

        if self.milk is not None:
            e.append(self.milk.toXML("fluidMilk"))

        if self.girlCum is not None:
            e.append(self.girlCum.toXML("fluidGirlCum"))

        return e

    def fromXML(self, e: etree._Element) -> None:
        ## from [LT]/src/com/lilithsthrone/game/character/FluidStored.java: public static FluidStored loadFromXML(StringBuilder log, Element parentElement, Document doc) { @ VYVZLU6B06BLr7PnyUvQOaOMo6QxcdDv72UlXL5TpqoiGgnrgl1JoADLaNG7n5viFVl97OMDmyP9Yi+v9S4PlA==
        self.charactersFluidID = e.attrib["charactersFluidID"]
        self.millilitres = float(e.attrib["millilitres"])
        feral = False
        cumVirile = False
        virility = 25.0
        try:
            feral = XML2BOOL[e.attrib["bestial"]]
            virility = float(e.attrib["virility"])
        except Exception as ex:
            raise ex

        if "cumVirile" in e.attrib:
            cumVirile = XML2BOOL[e.attrib["cumVirile"]]

        if (bodyEl := e.find("body")) is not None and (
            fluidCumEl := e.find("fluidCum")
        ) is not None:
            body = Body()
            body.fromXML(bodyEl)
            cum = Cum()
            cum.fromXML(fluidCumEl)
            self.body = body
            self.cum = cum
            self.feral = feral
            self.cumVirile = cumVirile
            self.virility = virility
            return
        else:
            milkEl: Optional[etree._Element] = next(
                filter(lambda x: x is not None, [e.find("milk"), e.find("fluidMilk")]),
                None,
            )
            if milkEl is not None:
                self.milk = Milk()
                self.milk.fromXML(milkEl)
                self.feral = feral
                self.cumVirile = False
                self.virility = 0.0
                return

            girlcumEl: Optional[etree._Element] = next(
                filter(
                    lambda x: x is not None, [e.find("girlcum"), e.find("fluidGirlCum")]
                ),
                None,
            )
            if girlcumEl is not None:
                self.girlCum = GirlCum()
                self.girlCum.fromXML(girlcumEl)
                self.feral = feral
                self.cumVirile = False
                self.virility = 0.0
                return

            # OLD CUM
            if (fluidCumEl := e.find("cum")) is not None:
                # FIXME: I have no idea what to do here. We can't generate bodies without context.
                click.warn(
                    f"{self.__class__.__name__}: Found old <cum> element.  We can't generate bodies. Throwing in a blank one. THIS MAY CAUSE PROBLEMS"
                )

                subspecies: str = e.attrib.get("cumSubspecies", "HUMAN")
                halfDemonSubspecies: str = e.attrib.get(
                    "cumHalfDemonSubspecies", "HUMAN"
                )
                self.body = Body()

                self.cum = Cum()
                self.cum.fromXML(fluidCumEl)

                self.feral = feral
                self.cumVirile = cumVirile
                self.virility = virility
                return
        raise BadFluidStateException(
            "Cannot parse fluidStored: Invalid state!",
            etree.tostring(e),
            e.getroottree().getpath(e),
        )

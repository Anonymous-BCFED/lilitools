import typing
from dataclasses import dataclass
from enum import Enum
from typing import Any, Dict, FrozenSet, List, Optional, Set

import tabulate
from lxml import etree

from lilitools.consts import DEFAULT_COMBAT_AP, PRESERVE_SAVE_ORDERING
from lilitools.game.enums.known_status_effect import EKnownStatusEffect
from lilitools.game.histories import HISTORIES_BY_ID
from lilitools.game.histories.history import History
from lilitools.logging import ClickWrap
from lilitools.saves.character._savables.game_character import RawGameCharacter
from lilitools.saves.character.active_status_effect import ActiveStatusEffect
from lilitools.saves.character.body.enums.femininity import EFemininity
from lilitools.saves.character.enums.attribute import EAttribute
from lilitools.saves.character.enums.fetish_desire import EFetishDesire
from lilitools.saves.character.enums.intelligence_level import EIntelligenceLevel
from lilitools.saves.character.enums.known_perks import EKnownPerks
from lilitools.saves.character.enums.occupation import EOccupation
from lilitools.saves.character.enums.subspecies import ESubspecies
from lilitools.saves.character.fetish_entry import FetishEntry
from lilitools.saves.character.gender import EGender
from lilitools.saves.character.pregnancy.litter import Litter
from lilitools.saves.character.pregnancy.pregnancy_possibility import (
    PregnancyPossibility,
)
from lilitools.saves.character.subspecies_types import SUBSPECIES_TYPES
from lilitools.saves.enums.difficulty_level import EDifficultyLevel
from lilitools.saves.enums.tf_modifier import ETFModifier
from lilitools.saves.enums.tf_potency import ETFPotency
from lilitools.saves.inventoryslot import EInventorySlot
from lilitools.saves.modfiles.character.status_effects.status_effect_type import (
    StatusEffectType,
)
from lilitools.saves.slavery.enums.slave_job_settings import ESlaveJobSettings
from lilitools.saves.slavery.enums.slave_jobs import ESlaveJob
from lilitools.utils import clampFloat, clampInt

if typing.TYPE_CHECKING:
    from lilitools.game.characters.elemental import Elemental
    from lilitools.saves.character.player_character import PlayerCharacter
    from lilitools.saves.game import Game

click = ClickWrap()
PREGNANCY_STATUS_EFFECTS: FrozenSet[str] = frozenset(
    {
        # Util.newArrayListOfValues(StatusEffect.PREGNANT_1, StatusEffect.PREGNANT_2, StatusEffect.PREGNANT_3)
        "PREGNANT_1",
        "PREGNANT_2",
        "PREGNANT_3",
    }
)
STATUS_EFFECTS_THAT_REDUCE_HEALTH_BY_HALF: FrozenSet[str] = frozenset(
    {
        "ELEMENTAL_EARTH_SERVANT_OF_EARTH",
        "ELEMENTAL_WATER_SERVANT_OF_WATER",
        "ELEMENTAL_AIR_SERVANT_OF_AIR",
        "ELEMENTAL_FIRE_SERVANT_OF_FIRE",
        "ELEMENTAL_ARCANE_SERVANT_OF_ARCANE",
    }
)

ALL_RESISTANCES: FrozenSet[EAttribute] = frozenset(
    {
        EAttribute.RESISTANCE_FIRE,
        EAttribute.RESISTANCE_ICE,
        EAttribute.RESISTANCE_LUST,
        EAttribute.RESISTANCE_PHYSICAL,
        EAttribute.RESISTANCE_POISON,
    }
)


class EAppendOperator(Enum):
    ADD = "+"
    MULT = "*"


@dataclass
class SlaveValueLineItem:
    name: str
    description: str
    value: int
    count: Optional[float] = None
    appendOp: EAppendOperator = EAppendOperator.ADD

    def total(self) -> int:
        return (self.count * self.value) if self.count is not None else self.value

    def combine_with(self, v: float) -> float:
        match self.appendOp:
            case EAppendOperator.ADD:
                return v + self.total()
            case EAppendOperator.MULT:
                return v + self.total()


class GameCharacter(RawGameCharacter):
    def __init__(self) -> None:
        super().__init__()
        if PRESERVE_SAVE_ORDERING:
            self.allPerks: List[str] = list()
            self.perks: Dict[int, List[str]] = {}
        else:
            self.allPerks: Set[str] = set()
            self.perks: Dict[int, Set[str]] = {}

        self.bonusAttributes: Dict[EAttribute, float] = {}

        ## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: for(AbstractAttribute a : Attribute.getAllAttributes()) { @ ZvQT7sC+qAGkGec7AAaGNdBuZ7/+Yc/ZGSZEVFvVSHPoQPpVrwN6CDd/LuCcfB8FmBnU73WOzJt6gV4gczn67A==
        for attr in EAttribute:
            self.attributes[attr] = attr.baseValue
            self.bonusAttributes[attr] = 0.0

        self._historyApplied: Optional[History] = None

    ## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public void setHistory(Occupation history) { @ LCvshH/WlVxKI+nuKvSZT71YliV+daRF2F3ARxt6s5zK3+SirP8ELkEpNJ5x7Ad1cQ7afg7aGwfWnVaYAJuBIg==
    def setHistory(self, history: str) -> None:
        if self._historyApplied is not None:
            self._historyApplied.removeFromCharacter(self)
        self.core.history = history
        self._historyApplied = None
        self.applyHistory()

    def applyHistory(self) -> None:
        if self._historyApplied is not None:
            return
        self._historyApplied = HISTORIES_BY_ID[self.core.history]()
        self._historyApplied.applyToCharacter(self)

    ## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public void calculateStatusEffects(int secondsPassed) { @ GiDhigSbiYL+m9o8bfb4zEs8dDRt7pDJ0AHuh358LCdz2JabWyA86D1tljxE/29YUTXB4I0lEnbcNA3GpizSzg==
    def calculateStatusEffects(self, game: "Game", secondsPassed: int) -> None:
        # // Count down status effects:
        # float healthPercentage = this.getHealthPercentage();
        healthPercentage: float = self.getHealthPercent()
        # float manaPercentage = this.getManaPercentage();
        manaPercentage: float = self.getManaPercent()

        # float startMaxHealth = this.getAttributeValue(Attribute.HEALTH_MAXIMUM);
        startMaxHealth: float = self.getAttributeValue(EAttribute.HEALTH_MAXIMUM)
        # float startMaxMana = this.getAttributeValue(Attribute.MANA_MAXIMUM);
        startMaxMana: float = self.getAttributeValue(EAttribute.MANA_MAXIMUM)

        # float startHealth = this.getHealth();
        startHealth: float = self.core.health
        # float startMana = this.getMana();
        startMana: float = self.core.mana

        # List<AbstractStatusEffect> tempListStatusEffects = new ArrayList<>();
        tempListStatusEffects: List[ActiveStatusEffect] = []
        # for(AppliedStatusEffect appliedSe : new ArrayList<>(statusEffects)) {
        for appliedSE in self.statusEffects.copy():
            # AbstractStatusEffect se = appliedSe.getEffect();
            se: StatusEffectType = appliedSE.getStatusEffectType()
            if se is None:
                continue
            # appliedSe.setSecondsPassed(appliedSe.getSecondsPassed() + secondsPassed);
            appliedSE.secondsPassed += secondsPassed
            # StringBuilder sb = new StringBuilder();
            sb = ""
            # if (!se.isCombatEffect()) {
            if not se.isCombatEffect():
                # if(appliedSe.getEffect().getEffectInterval()<=0 || ((Main.game.getSecondsPassed()-appliedSe.getLastTimeAppliedEffect())>appliedSe.getEffect().getEffectInterval())) {
                if se.getEffectInterval() <= 0 or (
                    (game.coreInfo.secondsPassed - appliedSE.lastTimeApplied)
                    > se.getEffectInterval()
                ):
                    # if(appliedSe.getEffect().getEffectInterval()<=0) {
                    #     sb.append(se.applyEffect(this, secondsPassed, appliedSe.getSecondsPassed()));
                    if se.getEffectInterval() <= 0:
                        se.apply(self, secondsPassed, appliedSE.secondsPassed)
                    # } else {
                    else:
                        # for(int i=0; i<((Main.game.getSecondsPassed()-appliedSe.getLastTimeAppliedEffect())/appliedSe.getEffect().getEffectInterval()); i++) {
                        for __init__ in range(
                            (game.coreInfo.secondsPassed - appliedSE.lastTimeApplied)
                            / appliedSE.getEffectInterval()
                        ):
                            # if(sb.length()>0) {
                            #     sb.append("<br/>");
                            # }
                            # sb.append(se.applyEffect(this, secondsPassed, appliedSe.getSecondsPassed()));
                            se.apply(self, secondsPassed, appliedSE.secondsPassed)
                        # }

                    # }

                    # appliedSe.setLastTimeAppliedEffect(Main.game.getSecondsPassed());
                    # if(sb.length()!=0) {
                    #     addStatusEffectDescription(se, sb.toString());
                    # }
                # }
            # }

            # incrementStatusEffectDuration(se, -secondsPassed);

            # if(appliedSe.getSecondsRemaining()<0
            #         && ((!se.isConditionsMet(this)) || se.getApplicationLength(this)>0)) { // If getApplicationLength() is not -1, then this status effect should be removed and re-checked, even if isConditionsMet() is returning true.
            #     tempListStatusEffects.add(se);
            # }
        # }

        # // Remove all status effects that are no longer applicable:
        # for(AbstractStatusEffect se : tempListStatusEffects) {
        #     removeStatusEffect(se);
        # }
        # // Combat status effects are automatically removed at the end of combat (in Combat.java), so there's no need to remove them here.

        # // Add all status effects that are applicable:
        # for (AbstractStatusEffect se : StatusEffect.getAllStatusEffectsRequiringApplicationCheck()) {
        #     if(!this.hasStatusEffect(se) || se.isConstantRefresh()) {
        #         if((se.getCategory()==StatusEffectCategory.DEFAULT && (!se.isFromExternalFile() || se.isMod())) // Modded SEs probably won't have taken into account category, so let them always be checked.
        #                 || (se.getCategory()==StatusEffectCategory.INVENTORY && requiresInventoryStatusEffectCheck)
        #                 || (se.getCategory()==StatusEffectCategory.ATTRIBUTE && requiresAttributeStatusEffectCheck)) {
        #             if(se.isConditionsMet(this)) {
        #                 addStatusEffect(se, se.getApplicationLength()); // Default application length is -1
        #             }
        #         }
        #     }
        # }
        # requiresInventoryStatusEffectCheck = false;
        # requiresAttributeStatusEffectCheck = false;

        # // Clothing effects:
        # for(AbstractClothing c : this.getClothingCurrentlyEquipped()) {
        #     for(ItemEffect ie : c.getEffects()) {
        #         String clothingEffectDescription = ie.applyEffect(this, this, secondsPassed);
        #         if(!clothingEffectDescription.isEmpty()) {
        #             addStatusEffectDescription(StatusEffect.CLOTHING_EFFECT,
        #                     "<p style='margin:0 auto;padding:0 auto;color:"+c.getRarity().getColour().toWebHexString()+";'><b>"+ Util.capitaliseSentence(c.getName())+":</b></p>"
        #                     + clothingEffectDescription);
        #         }
        #     }
        # }

        # // Tattoo effects:
        # for(Tattoo tattoo : tattoos.values()) {
        #     for(ItemEffect ie : tattoo.getEffects()) {
        #         String tattooEffectDescription = ie.applyEffect(this, this, secondsPassed);
        #         if (!tattooEffectDescription.isEmpty()) {
        #             addStatusEffectDescription(StatusEffect.CLOTHING_EFFECT,
        #                     "<p style='margin:0 auto;padding:0 auto;'><b>"+ Util.capitaliseSentence(tattoo.getName())+" tattoo:</b></p>"
        #                     + tattooEffectDescription);
        #         }
        #     }
        # }

        # float healthGain = this.getHealth() - startHealth;
        # float manaGain = this.getMana() - startMana;

        # this.setHealthPercentage(healthPercentage);
        # this.setManaPercentage(manaPercentage);

        # this.incrementHealth(healthGain);
        # this.incrementMana(manaGain);

        # // Increment health/mana by maximum resource attribute loss to offset the artificial loss applied above
        # float maxHealthChange = this.getAttributeValue(Attribute.HEALTH_MAXIMUM) - startMaxHealth;
        # if(maxHealthChange<0) {
        #     this.incrementHealth(Math.abs(maxHealthChange));
        # }
        # float maxManaChange = this.getAttributeValue(Attribute.MANA_MAXIMUM) - startMaxMana;
        # if(maxManaChange<0) {
        #     this.incrementMana(Math.abs(maxManaChange));
        # }

    ## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public float getBaseAttributeValue(AbstractAttribute attribute) { @ QSKyqurONeTUgRE02sJo15JZwlZvcbPOstc246C8SmaWH0reep1hQ6hkZu7fDbiVb2QDkqUMc6j/8B+Uh9ApNg==
    def getAttributeBaseValue(self, attr: EAttribute) -> float:
        # return Math.round((attributes.get(attribute)) * 100) / 100f
        return round(self.attributes[attr] * 100.0) / 100.0

    ## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public float getBonusAttributeValue(AbstractAttribute attribute) { @ uX6PBj+52rX2FY19bldgS7UH2QHuFLoIsUz9GO9GA6xlITEW/hpBXWEiQBf7+qImMpgaxNhJtgP9i07QQmNMxg==
    def getBonusAttributeValue(self, attr: EAttribute) -> float:
        self.applyHistory()

        # float value = 0;
        value: float = 0.0
        # // Special case for health:
        # if (attribute == Attribute.HEALTH_MAXIMUM) {
        #     value = 10 + 5*getLevel() + 2*getAttributeValue(Attribute.MAJOR_PHYSIQUE);
        # }
        if attr == EAttribute.HEALTH_MAXIMUM:
            value = (
                10.0
                + (5.0 * self.core.level)
                + (2.0 * self.getAttributeValue(EAttribute.MAJOR_PHYSIQUE))
            )

        # // Special case for mana:
        # if (attribute == Attribute.MANA_MAXIMUM) {
        #     if(getAttributeValue(Attribute.MAJOR_ARCANE) < IntelligenceLevel.ONE_AVERAGE.getMinimumValue()) {
        #         value = 5;
        #     } else {
        #         value = 5 + 2*getLevel() + 5*getAttributeValue(Attribute.MAJOR_ARCANE);
        #     }
        # }
        if attr == EAttribute.MANA_MAXIMUM:
            if (
                self.getAttributeValue(EAttribute.MAJOR_ARCANE)
                < EIntelligenceLevel.ONE_AVERAGE.minimumValue
            ):
                value = 5.0
            else:
                value = (
                    5.0
                    + (2.0 * self.core.level)
                    + (5.0 * self.getAttributeValue(EAttribute.MAJOR_ARCANE))
                )

        # // Increment over all status effects instead of using bonus attributes, as status effect's attribute values can vary depending on external factors:
        # for(AppliedStatusEffect se : statusEffects) {
        #     if(se.getEffect().getAttributeModifiers(this)!=null) {
        #         for(Entry<AbstractAttribute, Float> att : se.getEffect().getAttributeModifiers(this).entrySet()) {
        #             if(att.getKey()==attribute) {
        #                 value+=att.getValue();
        #             }
        #         }
        #     }
        # }

        for se in self.statusEffects:
            try:
                effects = EKnownStatusEffect.GetByName(se.type).effects
                if len(effects) > 0:
                    for sfxattr, val in effects.items():
                        # print(repr(sfxattr), attr)
                        if attr == sfxattr:
                            value += val
                            click.debug(f"{se.type} contributed {val} to {attr}")
            except KeyError:
                click.warn(
                    f"Status Effect {se.type} is currently unknown to us, and therefore will not be factored into the calculations."
                )

        # if(this.getHistory()==Occupation.ARISTOCRAT
        #     && (attribute == Attribute.RESISTANCE_FIRE
        #         || attribute == Attribute.RESISTANCE_ICE
        #         || attribute == Attribute.RESISTANCE_LUST
        #         || attribute == Attribute.RESISTANCE_PHYSICAL
        #         || attribute == Attribute.RESISTANCE_POISON)) {
        #     value += (this.getAttributeValue(Attribute.MAJOR_CORRUPTION)/4);
        # }
        if self.core.history == EOccupation.ARISTOCRAT.name and attr in ALL_RESISTANCES:
            value += self.getAttributeValue(EAttribute.MAJOR_CORRUPTION) / 4.0

        # return Math.round((value + bonusAttributes.get(attribute))*100)/100f;
        return round((value + self.bonusAttributes[attr]) * 100.0) / 100.0
        # //return Math.round(bonusAttributes.get(att)*100)/100f;

    ## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public float getPhysicalResistanceAttributeFromClothingAndWeapons() { @ oL9kbq3d6J+lqygr4iioHln3ctQMwxUv6R+cqXb9YESMfrvHjwvQuuHGgpsUL1X3VSeOm9yfCfBfuN34sDQr4g==
    def _upstream_getPhysicalResistanceAttributeFromClothingAndWeapons(self) -> float:
        # float value = 0

        # for (AbstractClothing clothing: this.getClothingCurrentlyEquipped()) {
        #         value += clothing.getClothingType().getPhysicalResistance(); }
        # for (AbstractWeapon weapon: this.inventory.getMainWeaponArray()) {
        #         if (weapon != null) {
        #             value += weapon.getWeaponType().getPhysicalResistance(); }
        #     }
        # for (AbstractWeapon weapon: this.inventory.getOffhandWeaponArray()) {
        #         if (weapon != null) {
        #             value += weapon.getWeaponType().getPhysicalResistance(); }
        #     }

        # return value
        value: float = 0.0
        value += sum(
            [
                c.getClothingType().getPhysicalResistance()
                for c in self.characterInventory.clothingEquipped
            ]
        )
        value += sum(
            [
                c.getWeaponType().getPhysicalResistance()
                for c in self.characterInventory.mainWeapons
                if c is not None
            ]
        )
        value += sum(
            [
                c.getWeaponType().getPhysicalResistance()
                for c in self.characterInventory.offhandWeapons
                if c is not None
            ]
        )
        return value

    ## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public int getLevel() { @ 6EKv2y91ZKx5R7hXf7yVELCfZzvyiYxUhRK6wPJEKiPDfizlt+kYHvboAbGm3RHOSTD0KUvHT+P2+Mf+k/qRcA==
    def getLevel(self) -> int:
        # public int getLevel() {
        # 	try { // There was a NullPointerException being thrown somewhere in here during NPC load from XML.
        # 		if(this.isPlayer()
        # 				|| !Main.getProperties().difficultyLevel.isNPCLevelScaling()
        # 				|| (this.isSlave() && this.getOwner().isPlayer())
        # 				|| (this.isElemental() && ((Elemental)this).getSummoner()!=null && ((Elemental)this).getSummoner().isPlayer())
        # 				|| Main.game.getPlayer().getFriendlyOccupants().contains(this.getId())
        # 				|| (this.getPartyLeader()!=null && this.getPartyLeader().isPlayer())) {
        # 			return level;
        g = Game.getAnInstance()
        if (
            isinstance(self, PlayerCharacter)
            or not g.properties.settings.difficultyLevel.isNPCLevelScaling
            or (self.slavery.isSlave() and self.slavery.owner == "PlayerCharacter")
            or (
                isinstance(self, Elemental)
                and self.elemental.summoner == "PlayerCharacter"
            )
            or self.core.id in g.playerCharacter.player_specific.friendlyOccupants
            or self.companions.partyLeader == "PlayerCharacter"
        ):
            return self.core.level
            # } else {
        else:
            # if(Main.getProperties().difficultyLevel == DifficultyLevel.HELL) {
            if g.properties.settings.difficultyLevel == EDifficultyLevel.HELL:
                # if(level < Main.game.getPlayer().getLevel() * 2) {
                if self.core.level < g.playerCharacter.core.level * 2:
                    # return Main.game.getPlayer().getLevel() * 2;
                    return g.playerCharacter.core.level * 2
                else:
                    # } else {
                    # return level;
                    # }
                    return self.core.level
            elif (
                self.core.level < g.playerCharacter.core.level
            ):  # } else if(level < Main.game.getPlayer().getLevel()) {
                # return Main.game.getPlayer().getLevel();
                return g.playerCharacter.core.level
            else:  # } else {
                #   return level;
                # }
                return self.core.level

    ## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public boolean isDoll() { @ 0ZSnnBVluvzLu2FKcKcAvt8WJ1z2TlKkGmsT57Zchr8MbURjk3p+XacrNA6J9dFRqasTRoV6eb7YQXPblcvp5A==
    def isDoll(self) -> bool:
        # public boolean isDoll() {
        #     if (getBody() == null) {
        #         return false
        #     }
        #     return getBody().isDoll();
        #  .... Which is the below
        # }
        return self.body.core.subspecies == ESubspecies.DOLL

    ## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public float getAttributeValue(AbstractAttribute att) { @ V3onYEvrHYEQnmnUZMFa6C/LJd/GPHoYVO+04ycljYqmchhJwLYIDwRPVkWMnM6QAA0RlpiaqBy4HYJCITguYg==
    def getAttributeValue(self, attr: EAttribute) -> float:
        # JFC

        # -Skipped, not germane.
        # if(!Main.game.isInNewWorld() && att == Attribute.MAJOR_ARCANE) {
        #     return 0;
        # }

        # float value = getBaseAttributeValue(att) + getBonusAttributeValue(att);
        value: float = self.getAttributeBaseValue(attr) + self.getBonusAttributeValue(
            attr
        )

        # if(Main.game.isStarted() && att==Attribute.MAJOR_CORRUPTION && this.getHistory()==Occupation.ARISTOCRAT) {
        #     value *= 2;
        # }
        if (
            self.core.history == EOccupation.ARISTOCRAT.name
            and attr == EAttribute.MAJOR_CORRUPTION
        ):
            value *= 2.0

        # if(att == Attribute.ACTION_POINTS) {
        #     value += DEFAULT_COMBAT_AP;
        # }
        if attr == EAttribute.ACTION_POINTS:
            value += DEFAULT_COMBAT_AP

        # if(att == Attribute.RESISTANCE_PHYSICAL) {
        # 	value += getPhysicalResistanceAttributeFromClothingAndWeapons();
        # }

        if attr == EAttribute.RESISTANCE_PHYSICAL:
            value = (
                self._upstream_getPhysicalResistanceAttributeFromClothingAndWeapons()
            )

        # if(att == Attribute.ENCHANTMENT_LIMIT) {
        # 	return 10 + this.getLevel() + value;
        # }
        if attr == EAttribute.ENCHANTMENT_LIMIT:
            return 10 + self.getLevel() + value

        # float reduction = 0;
        reduction: float = 0.0

        # if(att == Attribute.HEALTH_MAXIMUM) {
        #     if(this.hasStatusEffect(StatusEffect.ELEMENTAL_EARTH_SERVANT_OF_EARTH)
        #             || this.hasStatusEffect(StatusEffect.ELEMENTAL_WATER_SERVANT_OF_WATER)
        #             || this.hasStatusEffect(StatusEffect.ELEMENTAL_AIR_SERVANT_OF_AIR)
        #             || this.hasStatusEffect(StatusEffect.ELEMENTAL_FIRE_SERVANT_OF_FIRE)
        #             || this.hasStatusEffect(StatusEffect.ELEMENTAL_ARCANE_SERVANT_OF_ARCANE)) {
        #         reduction +=  value/2f;
        #     }
        #     if(this.hasStatusEffect(StatusEffect.PREGNANT_1)) {
        #         reduction += value*0.05f;
        #     } else if(this.hasStatusEffect(StatusEffect.PREGNANT_2)) {
        #         reduction += value*0.1f;
        #     } else if(this.hasStatusEffect(StatusEffect.PREGNANT_3)) {
        #         reduction += value*0.15f;
        #     }
        # }
        if attr == EAttribute.HEALTH_MAXIMUM:
            if any(
                [
                    x.type in STATUS_EFFECTS_THAT_REDUCE_HEALTH_BY_HALF
                    for x in self.statusEffects
                ]
            ):
                reduction += value / 2.0
            if self.hasStatusEffect("PREGNANT_1"):
                reduction += value * 0.05
            elif self.hasStatusEffect("PREGNANT_2"):
                reduction += value * 0.1
            elif self.hasStatusEffect("PREGNANT_3"):
                reduction += value * 0.15
        # if (att == Attribute.HEALTH_MAXIMUM || att == Attribute.MANA_MAXIMUM) {
        #     if (att == Attribute.MANA_MAXIMUM && (this.hasStatusEffect(StatusEffect.INTELLIGENCE_PERK_0) | | this.hasStatusEffect(StatusEffect.INTELLIGENCE_PERK_0_OLD_WORLD))) {
        #         if (this.isDoll() || !Main.game.isInNewWorld()) {
        #             return 0
        #         }
        #         return 5
        #     } else if (this.hasStatusEffect(StatusEffect.CLOTHING_ENCHANTMENT_OVER_LIMIT)) {
        #         reduction += value * 0.1f
        #     } else if (this.hasStatusEffect(StatusEffect.CLOTHING_ENCHANTMENT_OVER_LIMIT_2)) {
        #         reduction += value * 0.5f
        #     } else if (this.hasStatusEffect(StatusEffect.CLOTHING_ENCHANTMENT_OVER_LIMIT_3)) {
        #         return 1
        #     }
        # }
        if attr == EAttribute.HEALTH_MAXIMUM or attr == EAttribute.MANA_MAXIMUM:
            if attr == EAttribute.MANA_MAXIMUM and (
                self.hasStatusEffect("INTELLIGENCE_PERK_0")
                or self.hasStatusEffect("INTELLIGENCE_PERK_0_OLD_WORLD")
            ):
                if self.isDoll():
                    return 0.0
                return 5.0
            if self.hasStatusEffect("CLOTHING_ENCHANTMENT_OVER_LIMIT"):
                reduction += value * 0.1
            if self.hasStatusEffect("CLOTHING_ENCHANTMENT_OVER_LIMIT_2"):
                reduction += value * 0.5
            if self.hasStatusEffect("CLOTHING_ENCHANTMENT_OVER_LIMIT_3"):
                return 1.0

        # if(this.hasStatusEffect(StatusEffect.CLOTHING_ENCHANTMENT_OVER_LIMIT_3)
        #         && (att == Attribute.RESISTANCE_FIRE
        #             || att == Attribute.RESISTANCE_ICE
        #             || att == Attribute.RESISTANCE_LUST
        #             || att == Attribute.RESISTANCE_PHYSICAL
        #             || att == Attribute.RESISTANCE_POISON)) {
        #     if(value>0) {
        #         return 0;
        #     }
        # }
        if (
            self.hasStatusEffect("CLOTHING_ENCHANTMENT_OVER_LIMIT_3")
            and attr in ALL_RESISTANCES
            and value > 0.0
        ):
            return 0.0

        # value = value - reduction;
        value = value - reduction

        # if(value < att.getLowerLimit()) {
        #     return att.getLowerLimit();
        if value < attr.lowerLimit:
            return attr.lowerLimit

        # } else if(value > att.getUpperLimit()) {
        #     return att.getUpperLimit();
        # }
        if value > attr.upperLimit:
            return attr.upperLimit

        # print('before qnr', attr.name, value)
        return (
            round(clampFloat(value, attr.lowerLimit, attr.upperLimit) * 100.0) / 100.0
        )

        # return Math.round(value * 100)/100f;

    def getHealthPercent(self) -> float:
        return self.core.health / self.getAttributeValue(EAttribute.HEALTH_MAXIMUM)

    def setHealthPercent(self, value: float) -> None:
        self.core.health = value * self.getAttributeValue(EAttribute.HEALTH_MAXIMUM)

    def getManaPercent(self) -> float:
        return self.core.mana / self.getAttributeValue(EAttribute.MANA_MAXIMUM)

    def setManaPercent(self, value: float) -> None:
        self.core.mana = value * self.getAttributeValue(EAttribute.MANA_MAXIMUM)

    def incrementBonusAttribute(self, attr: EAttribute, amount: float) -> None:
        pctHP = self.getHealthPercent()
        pctMana = self.getManaPercent()

        self.bonusAttributes[attr] += amount
        self.setHealthPercent(pctHP)
        self.setManaPercent(pctHP)

    def setAttributeValue(self, attr: EAttribute, value: float) -> None:
        self.attributes[attr] = value

    def hasStatusEffect(self, id: str) -> bool:
        return any([x.type == id for x in self.statusEffects])

    def removeStatusEffect(self, id: str) -> None:
        for x in list(self.statusEffects):
            if x.type == id:
                self.statusEffects.remove(x)

    if PRESERVE_SAVE_ORDERING:

        def _fromXML_perks(self, e: etree._Element) -> None:
            row: int
            perkName: str
            perk: etree._Element
            self.perks = {}
            self.allPerks = list()
            for perk in e.find("perks").findall("perk"):
                row = int(perk.attrib["row"])
                perkName = perk.attrib["type"]
                if row not in self.perks:
                    self.perks[row] = list([perkName])
                else:
                    self.perks[row].append(perkName)
                self.allPerks.append(perkName)

        def _toXML_perks(self, e: etree._Element) -> None:
            perkElem: etree._Element = etree.SubElement(e, "perks")
            for row, perks in self.perks.items():
                for perk in perks:
                    etree.SubElement(perkElem, "perk", {"row": str(row), "type": perk})

        def _fromDict_perks(self, data: Dict[str, typing.Any]) -> None:
            row: int
            perkName: str
            self.perks = {}
            self.allPerks = list()
            for perkName, perkData in data["perks"].items():
                row = perkData["row"]
                if row not in self.perks:
                    self.perks[row] = list([perkName])
                else:
                    self.perks[row].append(perkName)
                self.allPerks.append(perkName)

        def _toDict_perks(self, data: Dict[str, typing.Any]) -> None:
            o: Dict[str, Dict[str, Any]] = {}
            for rowID, perks in self.perks.items():
                for perk in perks:
                    o[perk] = {"row": rowID}
            data["perks"] = o

        def addPerk(self, perkName: str, row: Optional[int] = None) -> None:
            if row is None:
                row = EKnownPerks[perkName.upper()].row
            self.allPerks.append(perkName)
            if row not in self.perks:
                self.perks[row] = [perkName]
            else:
                self.perks[row].append(perkName)

        def rmPerk(self, perkName: str) -> None:
            self.allPerks.remove(perkName)
            for row in self.perks.keys():
                self.perks[row].remove(perkName)

    else:

        def _fromXML_perks(self, e: etree._Element) -> None:
            row: int
            perkName: str
            perk: etree._Element
            self.perks = {}
            self.allPerks = set()
            for perk in e.find("perks").findall("perk"):
                row = int(perk.attrib["row"])
                perkName = perk.attrib["type"]
                if row not in self.perks:
                    self.perks[row] = set([perkName])
                else:
                    self.perks[row].add(perkName)
                self.allPerks.add(perkName)

        def _toXML_perks(self, e: etree._Element) -> None:
            perkElem: etree._Element = etree.SubElement(e, "perks")
            for row, perks in sorted(self.perks.items(), key=lambda t: t[0]):
                for perk in sorted(perks):
                    etree.SubElement(perkElem, "perk", {"row": str(row), "type": perk})

        def _fromDict_perks(self, data: Dict[str, typing.Any]) -> None:
            row: int
            perkName: str
            self.perks = {}
            self.allPerks = set()
            for perkName, perkData in data["perks"].items():
                row = perkData["row"]
                if row not in self.perks:
                    self.perks[row] = set([perkName])
                else:
                    self.perks[row].append(perkName)
                self.allPerks.append(perkName)

        def _toDict_perks(self, data: Dict[str, typing.Any]) -> None:
            o: Dict[str, Dict[str, Any]] = {}
            for rowID, perks in self.perks.items():
                for perk in perks:
                    o[perk] = {"row": rowID}
            data["perks"] = o

        def addPerk(self, perkName: str, row: Optional[int] = None) -> None:
            if row is None:
                row = EKnownPerks[perkName.upper()].row
            self.allPerks.add(perkName)
            if row not in self.perks:
                self.perks[row] = set([perkName])
            else:
                self.perks[row].add(perkName)

        def rmPerk(self, perkName: str) -> None:
            self.allPerks.remove(perkName)
            for row in self.perks.keys():
                self.perks[row].remove(perkName)

    def hasPerk(self, perkName: str) -> bool:
        return perkName in self.allPerks

    def getFetishEntry(self, id: str) -> FetishEntry:
        if id not in self.fetishes.keys():
            f = FetishEntry()
            f.id = id
            f.hasFetish = False
            f.desire = EFetishDesire.TWO_NEUTRAL
            f.xp = 0
            self.fetishes[f.id] = f
            return f
        else:
            return self.fetishes[id]

    def hasFetishEntry(self, id: str) -> bool:
        return id in self.fetishes.keys()

    def hasFetish(self, id: str) -> bool:
        if id not in self.fetishes.keys():
            return False
        return self.fetishes[id].hasFetish

    def removeFetishEntry(self, id: str) -> bool:
        if id in self.fetishes.keys():
            del self.fetishes[id]
            return True
        return False

    def _toXML_fetishes(self, e: etree._Element) -> None:
        fetishes = etree.SubElement(e, "fetishes")
        if PRESERVE_SAVE_ORDERING:
            for _, fetish in self.fetishes.items():
                fetishes.append(fetish.toXML())
        else:
            for _, fetish in sorted(self.fetishes.items(), key=lambda x: x[0]):
                fetishes.append(fetish.toXML())

    def _fromXML_fetishes(self, e: etree._Element) -> None:
        for f in e.find("fetishes").findall("f"):
            fe = FetishEntry()
            fe.fromXML(f)
            self.fetishes[fe.id] = fe

    def _toDict_fetishes(self, data: Dict[str, Any]) -> None:
        fetishes = list()
        for _, fetish in sorted(self.fetishes.items(), key=lambda x: x[0]):
            fetishes.append(fetish.toDict())
        data["fetishes"] = fetishes

    def _fromDict_fetishes(self, data: Dict[str, Any]) -> None:
        for fdata in data["fetishes"]:
            fe = FetishEntry()
            fe.fromDict(fdata)
            self.fetishes[fe.id] = fe

    def getName(self) -> str:
        return self.core.name.getCurrentName(self.body.core.femininity)

    def show(self) -> None:
        """
        Used in lilitool <save> npc show <npc>
        """
        with click.echo("Core:"):
            self.core.show()
        if self.slavery.isSlave():
            with click.echo(f"Slave:"):
                ss = SUBSPECIES_TYPES.get(self.helpfulInformation.subspecies)
                v = ss.baseSlaveValue if ss is not None else 1000  # FIXME
                header = ["Op", "Name", "Description", "Count", "Value", "Total"]
                rows = []
                for svle in self.describeSlaveValue():
                    rows.append(
                        [
                            svle.appendOp.value,
                            svle.name,
                            svle.description,
                            svle.count or "N/A",
                            svle.value,
                            svle.total(),
                        ]
                    )
                    v = svle.combine_with(v)
                with click.echo(f"Value:"):
                    rows.append(["", "TOTAL", "", "", "", f"{int(v):,d}"])
                    print(tabulate.tabulate(headers=header, tabular_data=rows))

    def getRestingLust(self) -> int:
        ## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public int getRestingLust() { @ FosE6RJ1HMj08wPn4leWtpuJkG40R8bSNg/fG6qJG7ENR621CrgUnyivwt11nzfESzu4Jh58WeinXz4lO4uEcw==
        # int restingLust = (int) Math.round(getAttributeValue(Attribute.MAJOR_CORRUPTION) / 2)
        resting_lust: int = round(
            self.getAttributeValue(EAttribute.MAJOR_CORRUPTION) / 2.0
        )
        # restingLust += this.getAttributeValue(Attribute.RESTING_LUST)
        resting_lust += round(self.getAttributeValue(EAttribute.RESTING_LUST))
        # for(AbstractClothing c: this.getClothingCurrentlyEquipped()) {
        #     if(c.isVibrator()) {
        #         switch(c.getVibratorIntensity()) {
        #             case MINOR_BOOST:
        #             restingLust += 5;
        #             break;
        #             case BOOST:
        #             restingLust += 10;
        #             break;
        #             case MAJOR_BOOST:
        #             restingLust += 20;
        #             break;
        #             default:
        #             break; }
        #     }
        # }
        for c in self.characterInventory.clothingEquipped:
            if (intensity := c.getVibratorIntensity()) is not None:
                if intensity == ETFPotency.MINOR_BOOST:
                    resting_lust += 5
                if intensity == ETFPotency.BOOST:
                    resting_lust += 10
                if intensity == ETFPotency.MAJOR_BOOST:
                    resting_lust += 20

        # return Math.max(0, Math.min(restingLust, Attribute.RESTING_LUST.getUpperLimit()))
        return clampInt(resting_lust, 0, EAttribute.RESTING_LUST.getUpperLimit())

    def getMother(self, game: "Game") -> "GameCharacter":
        ## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public GameCharacter getMother() { @ QNyb0+PGVJaq2rn3tyF75cGhdFJMw4s4Kxmpm0rOxalgTg2UbHI5GfAfcO7S4rk/5gLSRuvMnLGSZzRMC1mSCA==

        # 	public GameCharacter getMother() {
        # 		if(motherId==null || motherId.isEmpty() || motherId.equals("NOT_SET")) {
        # 			return null;
        # 		}
        # 		try {
        # 			return Main.game.getNPCById(motherId);
        # 		} catch(Exception e) {
        # //			Util.logGetNpcByIdError("getMother()", motherId);
        # 			return null; // It should be safe to return null, and there's no need for error logging.
        # 		}
        # 	}
        if self.family.motherId is None or self.family.motherId in ("", "NOT_SET"):
            return None
        return game.getNPCById(self.family.motherId)

    def isPregnant(self) -> bool:
        return self.pregnancy is not None and self.pregnancy.pregnantLitter is not None

    def isPotentiallyPregnant(self) -> bool:
        return self.hasStatusEffect(EKnownStatusEffect.PREGNANT_0.name)

    def terminatePregnancies(self, game: "Game") -> None:
        """
        Loosely based off of endPregnancy, but we aren't going to fuck around with children.

        This pregnancy never happened.
        """
        litterSz: int = 0
        if self.pregnancy is not None or self.pregnancy.pregnantLitter is not None:
            litterSz = len(self.pregnancy.pregnantLitter.offspringList)
        pregnancyies = "pregnancy" if litterSz == 1 else "pregnancies"
        with click.info(f"Terminating {litterSz} {pregnancyies} on {self.core.id}..."):
            if (
                self.pregnancy is None
                or self.pregnancy.potentialPartnersAsMother is None
            ):
                return
            pp: PregnancyPossibility
            fpp: PregnancyPossibility
            for pp in self.pregnancy.potentialPartnersAsMother:
                if pp.fatherId is not None:
                    father = game.getNPCById(pp.fatherId)
                    if (
                        father is not None
                        and father.pregnancy is not None
                        and father.pregnancy.potentialPartnersAsFather is not None
                    ):
                        ppaf = father.pregnancy.potentialPartnersAsFather
                    for fpp in list(ppaf):
                        if pp.motherId == fpp.motherId:
                            ppaf.remove(fpp)
            self.pregnancy.potentialPartnersAsMother.clear()
            click.success("Cleared potential partners (mother)")
            if not self.isPregnant():
                return

            pregnantLitter = self.pregnancy.pregnantLitter
            if pregnantLitter.fatherId is not None:
                father = game.getNPCById(pregnantLitter.fatherId)
                fatherCopy: Litter
                for fatherCopy in list(father.pregnancy.littersFathered):
                    if (
                        fatherCopy.id != ""
                        and fatherCopy.id is not None
                        and fatherCopy.id == pregnantLitter.id
                    ):
                        father.pregnancy.littersFathered.remove(fatherCopy)

            click.success("Cleared potential partners (father)")

            for oid in pregnantLitter.offspringList:
                if "NPCOffspring" in oid:
                    game.removeOffspringById(oid)
                    click.success(f"Removed OffspringSeed {oid}")
                else:
                    game.removeNPCById(oid)
                    click.success(f"Removed NPC {oid}")

            for sfx in self.statusEffects.copy():
                if sfx.type in PREGNANCY_STATUS_EFFECTS:
                    self.statusEffects.remove(sfx)
                    click.success(f"Removed status effect {sfx.type}")

            self.pregnancy.pregnantLitter = None
            click.success(f"Cleared litter.")

            self.pregnancy.pregnancyReactions.clear()
            click.success(f"Cleared reactions.")

    def getCalculatedGender(self, ignore_visibility: bool) -> EGender:
        haveVagina = self.body.hasVagina()
        havePenis = self.body.hasPenis()
        haveBreasts = self.body.hasBreasts()
        femininityPrefix = "N"
        if self.body.core.femininity >= EFemininity.FEMININE.minimumFemininity:
            femininityPrefix = "F"
        # this.getFemininityValue()>=Femininity.ANDROGYNOUS.getMinimumFemininity()
        elif self.body.core.femininity >= EFemininity.ANDROGYNOUS.minimumFemininity:
            femininityPrefix = "N"
        else:
            femininityPrefix = "M"
        for g in EGender:
            if (
                g.name.startswith(femininityPrefix)
                and g.hasBreasts == haveBreasts
                and g.hasPenis == havePenis
                and g.hasVagina == haveVagina
            ):
                return g
        return EGender.N_NEUTER

    def isInventorySlotBlockedByBodyParts(self, slot: EInventorySlot) -> bool:
        for bp in self.body.getAllBodyParts():
            if slot in bp.getBlockedInventorySlots():
                return True
        return False

    def getAllFetishes(self, include_clothing: bool = False) -> Dict[str, FetishEntry]:
        o = dict(self.fetishes)
        if include_clothing:
            for c in self.characterInventory.clothingEquipped:
                for e in c.effects:
                    if (
                        e.type == "CLOTHING"
                        and e.mod1
                        in (
                            ETFModifier.TF_MOD_FETISH_BEHAVIOUR,
                            ETFModifier.TF_MOD_FETISH_BODY_PART,
                        )
                        and e.potency >= ETFPotency.MINOR_BOOST
                    ):
                        match e.mod2:
                            # fmt: off
                            case ETFModifier.TF_MOD_FETISH_ANAL_GIVING: o['fetish_anal_giving']=FetishEntry('fetish_anal_giving',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_ANAL_RECEIVING: o['fetish_anal_receiving']=FetishEntry('fetish_anal_receiving',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_VAGINAL_GIVING: o['fetish_vaginal_giving']=FetishEntry('fetish_vaginal_giving',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_VAGINAL_RECEIVING: o['fetish_vaginal_receiving']=FetishEntry('fetish_vaginal_receiving',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_PENIS_GIVING: o['fetish_penis_giving']=FetishEntry('fetish_penis_giving',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_PENIS_RECEIVING:  o['fetish_penis_receiving']=FetishEntry('fetish_penis_receiving',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_BREASTS_OTHERS:  o['fetish_breasts_others']=FetishEntry('fetish_breasts_others',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_BREASTS_SELF: o['fetish_breasts_self']=FetishEntry('fetish_breasts_self',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_ORAL_RECEIVING: o['fetish_oral_receiving']=FetishEntry('fetish_oral_receiving',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_ORAL_GIVING: o['fetish_oral_giving']=FetishEntry('fetish_oral_giving',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_LEG_LOVER: o['fetish_leg_lover']=FetishEntry('fetish_leg_lover',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_STRUTTER: o['fetish_strutter']=FetishEntry('fetish_strutter',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_FOOT_GIVING: o['fetish_foot_giving']=FetishEntry('fetish_foot_giving',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_FOOT_RECEIVING: o['fetish_foot_receiving']=FetishEntry('fetish_foot_receiving',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_ARMPIT_GIVING: o['fetish_armpit_giving']=FetishEntry('fetish_armpit_giving',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_ARMPIT_RECEIVING: o['fetish_armpit_receiving']=FetishEntry('fetish_armpit_receiving',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_LACTATION_OTHERS: o['fetish_lactation_others']=FetishEntry('fetish_lactation_others',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_LACTATION_SELF: o['fetish_lactation_self']=FetishEntry('fetish_lactation_self',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_DOMINANT: o['fetish_dominant']=FetishEntry('fetish_dominant',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_SUBMISSIVE: o['fetish_submissive']=FetishEntry('fetish_submissive',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_BONDAGE_VICTIM: o['fetish_bondage_victim']=FetishEntry('fetish_bondage_victim',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_BONDAGE_APPLIER: o['fetish_bondage_applier']=FetishEntry('fetish_bondage_applier',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_CROSS_DRESSER: o['fetish_cross_dresser']=FetishEntry('fetish_cross_dresser',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_CUM_ADDICT: o['fetish_cum_addict']=FetishEntry('fetish_cum_addict',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_CUM_STUD: o['fetish_cum_stud']=FetishEntry('fetish_cum_stud',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_DEFLOWERING: o['fetish_deflowering']=FetishEntry('fetish_deflowering',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_DENIAL: o['fetish_denial']=FetishEntry('fetish_denial',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_DENIAL_SELF: o['fetish_denial_self']=FetishEntry('fetish_denial_self',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_EXHIBITIONIST: o['fetish_exhibitionist']=FetishEntry('fetish_exhibitionist',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_VOYEURIST: o['fetish_voyeurist']=FetishEntry('fetish_voyeurist',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_IMPREGNATION: o['fetish_impregnation']=FetishEntry('fetish_impregnation',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_INCEST: o['fetish_incest']=FetishEntry('fetish_incest',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_MASOCHIST: o['fetish_masochist']=FetishEntry('fetish_masochist',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_MASTURBATION: o['fetish_masturbation']=FetishEntry('fetish_masturbation',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_NON_CON_DOM: o['fetish_non_con_dom']=FetishEntry('fetish_non_con_dom',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_NON_CON_SUB: o['fetish_non_con_sub']=FetishEntry('fetish_non_con_sub',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_PREGNANCY: o['fetish_pregnancy']=FetishEntry('fetish_pregnancy',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_PURE_VIRGIN: o['fetish_pure_virgin']=FetishEntry('fetish_pure_virgin',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_SADIST: o['fetish_sadist']=FetishEntry('fetish_sadist',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_TRANSFORMATION_GIVING: o['fetish_transformation_giving']=FetishEntry('fetish_transformation_giving',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_TRANSFORMATION_RECEIVING: o['fetish_transformation_receiving']=FetishEntry('fetish_transformation_receiving',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_BIMBO: o['fetish_bimbo']=FetishEntry('fetish_bimbo',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_KINK_GIVING: o['fetish_kink_giving']=FetishEntry('fetish_kink_giving',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_KINK_RECEIVING: o['fetish_kink_receiving']=FetishEntry('fetish_kink_receiving',True,EFetishDesire.FOUR_LOVE)
                            case ETFModifier.TF_MOD_FETISH_SIZE_QUEEN: o['fetish_size_queen']=FetishEntry('fetish_size_queen',True,EFetishDesire.FOUR_LOVE)
                            # fmt: on
        return o

    def describeSlaveValue(self) -> List[SlaveValueLineItem]:
        o: List[SlaveValueLineItem] = []
        ss = SUBSPECIES_TYPES.get(self.helpfulInformation.subspecies)
        baseValue = ss.baseSlaveValue if ss is not None else 1000
        o.append(
            SlaveValueLineItem(
                name="Base Value",
                description=f"Based on subspecies ({self.helpfulInformation.subspecies})",
                value=baseValue,
            )
        )
        if self.body.anus.orifice.orificeVirgin:
            o.append(
                SlaveValueLineItem(
                    name="Anal Virginity Bonus",
                    description="baseValue*0.2",
                    value=baseValue * 0.2,
                )
            )
        if self.body.hasPenis() and self.body.penis.penetrator.penetratorVirgin:
            o.append(
                SlaveValueLineItem(
                    name="Penile Virginity Bonus",
                    description="baseValue*0.2",
                    value=baseValue * 0.2,
                )
            )
        if self.body.hasVagina() and self.body.vagina.orifice.orificeVirgin:
            o.append(
                SlaveValueLineItem(
                    name="Vaginal Virginity Bonus",
                    description="baseValue*0.5",
                    value=baseValue * 0.5,
                )
            )

        fetishCount = len(self.getAllFetishes(include_clothing=True))
        o.append(
            SlaveValueLineItem(
                name="Fetish Bonus",
                description="1+(0.05*count(fetishes)) - includes clothing",
                value=1.0 + (0.05 * fetishCount),
                appendOp=EAppendOperator.MULT,
            )
        )
        o.append(
            SlaveValueLineItem(
                name="Obedience Bonus",
                description="(100+(obedience/2))/100",
                value=(100.0 + (self.core.obedience / 2.0)) / 100.0,
                appendOp=EAppendOperator.MULT,
            )
        )
        return o

    def getSlaveValue(self) -> None:
        o = 1000  # FIXME
        for svle in self.describeSlaveValue():
            o = svle.combine_with(o)
        return round(max(0, o))

    @property
    def affection(self) -> float:
        return self.relationships.get("PlayerCharacter", 0.0)

    @property
    def obedience(self) -> float:
        return self.core.obedience

    def setSlaveJobSetting(
        self, job: ESlaveJob, setting: ESlaveJobSettings, v: bool
    ) -> None:
        js = self.slavery.slaveJobSettings.setdefault(job.name, set())
        cv = setting.name in js
        if v:
            js.add(setting.name)
        else:
            js.remove(setting.name)

    def hasSlaveJobSetting(self, job: ESlaveJob, setting: ESlaveJobSettings) -> bool:
        if (js := self.slavery.slaveJobSettings.get(job.name)) is None:
            return False
        return setting.name in js

    def _getAffectionIncomeModifier(self, job: ESlaveJob) -> float:
        return job.getIncomeAffectionModifier()

    def _getObedienceIncomeModifier(self, job: ESlaveJob) -> float:
        return job.getIncomeObedienceModifier()

    def _getFinalHourlyIncomeAfterModifiers(self, game: 'Game', job: ESlaveJob) -> int:
        ## from [LT]src/com/lilithsthrone/game/occupantManagement/slave/SlaveJob.java: public static int getFinalHourlyIncomeAfterModifiers(GameCharacter character) {
        value = int(
            max(
                0,
                job.income
                + (self._getAffectionIncomeModifier(job) * self.affection)
                + (self._getObedienceIncomeModifier(job) * self.obedience),
            )
        )

        match job:
            case ESlaveJob.MILKING:
                value = 0
                if (
                    self.body.hasBreasts()
                    and self.body.breasts.storedMilk > 0
                    and self.hasSlaveJobSetting(job, ESlaveJobSettings.MILKING_MILK)
                    and self.hasSlaveJobSetting(
                        job, ESlaveJobSettings.MILKING_MILK_AUTO_SELL
                    )
                ):
                    value += self.body.breasts.getMilkVolumePerHour() * 0.01
                if (
                    self.body.hasCrotchBreasts()
                    and self.body.crotchBreasts.storedMilk > 0
                    and self.hasSlaveJobSetting(
                        job, ESlaveJobSettings.MILKING_MILK_CROTCH
                    )
                    and self.hasSlaveJobSetting(
                        job, ESlaveJobSettings.MILKING_MILK_CROTCH_AUTO_SELL
                    )
                ):
                    value += self.body.crotchBreasts.getMilkVolumePerHour() * 0.01
                if (
                    self.body.hasPenis()
                    and self.body.testicles.storedCum > 0
                    and self.hasSlaveJobSetting(job, ESlaveJobSettings.MILKING_CUM)
                    and self.hasSlaveJobSetting(
                        job, ESlaveJobSettings.MILKING_CUM_AUTO_SELL
                    )
                ):
                    value += self.body.testicles.getCumVolumePerHour() * 0.01
                if (
                    self.body.hasVagina()
                    and self.hasSlaveJobSetting(job, ESlaveJobSettings.MILKING_GIRLCUM)
                    and self.hasSlaveJobSetting(
                        job, ESlaveJobSettings.MILKING_GIRLCUM_AUTO_SELL
                    )
                ):
                    value += self.body.vagina.getGirlcumVolumePerHour() * 0.01
        if self.slavery is not None:
            owner = game.getNPCById(self.slavery.owner)
            if owner.hasPerk("JOB_OFFICE_WORKER"):
                return int(value * 1.25)
            elif (
                owner.hasPerk("JOB_MAIN") or owner.hasPerk("JOB_BUTLER")
            ) and job == ESlaveJob.CLEANING:
                return int(value * 2.0)
        return value

    def getSlaveIncomePerDay(self, game: 'Game') -> int:
        ## from [LT]src/com/lilithsthrone/game/occupantManagement/slave/SlaveJob.java: public static int getFinalDailyIncomeAfterModifiers(GameCharacter character) {
        o = 0
        for i in range(24):
            o += self._getFinalHourlyIncomeAfterModifiers(
                game, ESlaveJob[self.slavery.slaveAssignedJobs[i]]
            )
        return o

    def isElemental(self) -> bool:
        return False

    def getCharacterForExport(self) -> 'GameCharacter':
        return self
from enum import Enum, IntFlag
from typing import List

from lilitools.saves.character.enums.pronoun_type import EPronounType


class EGenitalFlag(IntFlag):
    PENIS = 1
    VAGINA = 2
    BREASTS = 4

    def toGenderStringFlags(self) -> List[str]:
        o: List[str] = []
        for m in EGenitalFlag:
            if (self & m) == m:
                o.append(m.name[0])
        return o

    def toListOfStrings(self) -> List[str]:
        o: List[str] = []
        for m in EGenitalFlag:
            if (self & m) == m:
                o.append(m.name.capitalize())
        return o


class EGender(Enum):
    M_P_V_B_HERMAPHRODITE = 'Hermaphrodite', True, True, True, EPronounType.MASCULINE
    M_P_V_HERMAPHRODITE = 'Hermaphrodite', True, True, False, EPronounType.MASCULINE
    M_P_B_BUSTYBOY = 'Bustyboy', True, False, True, EPronounType.MASCULINE
    M_P_MALE = 'Male', True, False, False, EPronounType.MASCULINE
    M_V_B_BUTCH = 'Butch', False, True, True, EPronounType.MASCULINE
    M_V_CUNTBOY = 'Cuntboy', False, True, False, EPronounType.MASCULINE
    M_B_MANNEQUIN = 'Mannequin', False, False, True, EPronounType.MASCULINE
    M_MANNEQUIN = 'Mannequin', False, False, False, EPronounType.MASCULINE

    F_P_V_B_FUTANARI = 'Futanari', True, True, True, EPronounType.FEMININE
    F_P_V_FUTANARI = 'Futanari', True, True, False, EPronounType.FEMININE
    F_P_B_SHEMALE = 'Shemale', True, False, True, EPronounType.FEMININE
    F_P_TRAP = 'Trap', True, False, False, EPronounType.FEMININE
    F_V_B_FEMALE = 'Female', False, True, True, EPronounType.FEMININE
    F_V_FEMALE = 'Female', False, True, False, EPronounType.FEMININE
    F_B_DOLL = 'Doll', False, False, True, EPronounType.FEMININE
    F_DOLL = 'Doll', False, False, False, EPronounType.FEMININE

    N_P_V_B_HERMAPHRODITE = 'Hermaphrodite', True, True, True, EPronounType.NEUTRAL
    N_P_V_HERMAPHRODITE = 'Hermaphrodite', True, True, False, EPronounType.NEUTRAL
    N_P_B_SHEMALE = 'Shemale', True, False, True, EPronounType.NEUTRAL
    N_P_TRAP = 'Trap', True, False, False, EPronounType.NEUTRAL
    N_V_B_TOMBOY = 'Tomboy', False, True, True, EPronounType.NEUTRAL
    N_V_TOMBOY = 'Tomboy', False, True, False, EPronounType.NEUTRAL
    N_B_DOLL = 'Doll', False, False, True, EPronounType.NEUTRAL
    N_NEUTER = 'Neuter', False, False, False, EPronounType.NEUTRAL

    def __init__(self, slang: str, penis: bool, vagina: bool, breasts: bool, pronounType: EPronounType) -> None:
        self.slang: str = slang
        self.genitalFlags: EGenitalFlag = EGenitalFlag(0)
        self.hasPenis: bool = penis
        self.hasVagina: bool = vagina
        self.hasBreasts: bool = breasts
        self.pronounType: EPronounType = pronounType
        if self.hasBreasts:
            self.genitalFlags |= EGenitalFlag.BREASTS
        if self.hasPenis:
            self.genitalFlags |= EGenitalFlag.PENIS
        if self.hasVagina:
            self.genitalFlags |= EGenitalFlag.VAGINA

    def getID(self) -> str:
        o: List[str] = [self.pronounType.name[0]]
        if self.hasPenis:
            o.append('P')
        if self.hasVagina:
            o.append('V')
        if self.hasBreasts:
            o.append('B')
        o.append(self.slang.upper())
        return '_'.join(o)

from enum import Enum


class EHeight(Enum):
    # UPSTREAM: Need to standardise to 1.5 each category

    # 6" to 2'
    NEGATIVE_THREE_MINIMUM = 'fairy-sized', 15, 61

    # 2' to 3'6"
    NEGATIVE_TWO_MINIMUM = 'very tiny', 61, 106

    # 3'6" to 4'
    NEGATIVE_ONE_TINY = 'tiny', 106, 122

    # 4' to 5'
    ZERO_TINY = 'small', 122, 152

    # 5' to 5'6"
    ONE_SHORT = 'short', 152, 166

    # 5'6" to 6'
    TWO_AVERAGE = 'average height', 166, 183

    # 6' to 6'6"
    THREE_TALL = 'tall', 183, 198

    # 6'6" to 7'
    FOUR_VERY_TALL = 'very tall', 198, 214

    # 7' to 7'6"
    FIVE_ENORMOUS = 'towering', 214, 228

    # 7'6" to 9'"
    SIX_GIANT = 'gigantic', 228, 274

    # 9' to 12'
    SEVEN_COLOSSAL = 'colossal', 274, 366

    def __init__(self, desc: str, minval: int, maxval: int) -> None:
        super().__init__()
        self.desc: str = desc
        self.min: int = minval
        self.max: int = maxval

    # 43 -> 5' 5"
    @staticmethod
    def enchantmentLimit2Height(limit: int) -> int:
        return EHeight.ZERO_TINY.min + limit

    @staticmethod
    def height2enchantmentLimit(cm: int) -> int:
        return cm - EHeight.ZERO_TINY.min

from __future__ import annotations

from typing import TYPE_CHECKING

from lilitools.saves.character._savables.location_information import RawLocationInformation

if TYPE_CHECKING:
    from lilitools.saves.game import Game
    from lilitools.saves.world.cell import Cell


class LocationInformation(RawLocationInformation):
    @classmethod
    def FromCell(cls, cell:Cell) -> LocationInformation:
        loc = cls()
        loc.location.x = cell.location.x
        loc.location.y = cell.location.y
        loc.worldLocation = cell.worldID
        return loc

    def asCell(self, game: Game) -> Cell:
        return game.worlds.getCellByWorldCoords(self.worldLocation, self.location.x, self.location.y)

    def moveToCell(self, cell: Cell) -> None:
        self.location.x = cell.location.x
        self.location.y = cell.location.y
        self.worldLocation = cell.worldID

    def moveToPos(self, x: int, y: int) -> None:
        self.location.x = x
        self.location.y = y

    def moveToWorldPos(self, worldID: str, x: int, y: int) -> None:
        self.worldLocation = worldID
        self.location.x = x
        self.location.y = y

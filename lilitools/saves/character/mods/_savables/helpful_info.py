#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawHelpfulCharacterInformation']
## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public Element saveAsXML(Element parentElement, Document doc) { @ Ra1rfw7eKd5WT2JXoGa/zLFvb/XgZlpPNZbY2BZwgdTA/t0DkASy5ADAtBbX2DvSTxI43cQAGTPbPsMVcAHyRQ==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawHelpfulCharacterInformation(Savable):
    TAG = 'helpfulInformation'
    _XMLID_ATTR_FULLSPECIESNAME_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_RACE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_SLAVEVALUE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_SUBSPECIES_ATTRIBUTE: str = 'value'
    _XMLID_TAG_FULLSPECIESNAME_ELEMENT: str = 'fullSpeciesName'
    _XMLID_TAG_RACE_ELEMENT: str = 'race'
    _XMLID_TAG_SLAVEVALUE_ELEMENT: str = 'slaveValue'
    _XMLID_TAG_SUBSPECIES_ELEMENT: str = 'subspecies'

    def __init__(self) -> None:
        super().__init__()
        self.race: str = ''  # Element
        self.subspecies: str = ''  # Element
        self.fullSpeciesName: str = ''  # Element
        self.slaveValue: int = 0  # Element

    def overrideAttrs(self, fullSpeciesName_attribute: _Optional_str = None, race_attribute: _Optional_str = None, slaveValue_attribute: _Optional_str = None, subspecies_attribute: _Optional_str = None) -> None:
        if fullSpeciesName_attribute is not None:
            self._XMLID_ATTR_FULLSPECIESNAME_ATTRIBUTE = fullSpeciesName_attribute
        if race_attribute is not None:
            self._XMLID_ATTR_RACE_ATTRIBUTE = race_attribute
        if slaveValue_attribute is not None:
            self._XMLID_ATTR_SLAVEVALUE_ATTRIBUTE = slaveValue_attribute
        if subspecies_attribute is not None:
            self._XMLID_ATTR_SUBSPECIES_ATTRIBUTE = subspecies_attribute

    def overrideTags(self, fullSpeciesName_element: _Optional_str = None, race_element: _Optional_str = None, slaveValue_element: _Optional_str = None, subspecies_element: _Optional_str = None) -> None:
        if fullSpeciesName_element is not None:
            self._XMLID_TAG_FULLSPECIESNAME_ELEMENT = fullSpeciesName_element
        if race_element is not None:
            self._XMLID_TAG_RACE_ELEMENT = race_element
        if slaveValue_element is not None:
            self._XMLID_TAG_SLAVEVALUE_ELEMENT = slaveValue_element
        if subspecies_element is not None:
            self._XMLID_TAG_SUBSPECIES_ELEMENT = subspecies_element

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_RACE_ELEMENT)) is not None:
            if self._XMLID_ATTR_RACE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_RACE_ATTRIBUTE, 'race')
            self.race = sf.attrib[self._XMLID_ATTR_RACE_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'race', self._XMLID_TAG_RACE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_SUBSPECIES_ELEMENT)) is not None:
            if self._XMLID_ATTR_SUBSPECIES_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_SUBSPECIES_ATTRIBUTE, 'subspecies')
            self.subspecies = sf.attrib[self._XMLID_ATTR_SUBSPECIES_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'subspecies', self._XMLID_TAG_SUBSPECIES_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_FULLSPECIESNAME_ELEMENT)) is not None:
            if self._XMLID_ATTR_FULLSPECIESNAME_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_FULLSPECIESNAME_ATTRIBUTE, 'fullSpeciesName')
            self.fullSpeciesName = sf.attrib[self._XMLID_ATTR_FULLSPECIESNAME_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'fullSpeciesName', self._XMLID_TAG_FULLSPECIESNAME_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_SLAVEVALUE_ELEMENT)) is not None:
            if self._XMLID_ATTR_SLAVEVALUE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_SLAVEVALUE_ATTRIBUTE, 'slaveValue')
            self.slaveValue = int(sf.attrib[self._XMLID_ATTR_SLAVEVALUE_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'slaveValue', self._XMLID_TAG_SLAVEVALUE_ELEMENT)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.append(etree.Comment('Added by HelpfulInfo mod.'))
        etree.SubElement(e, self._XMLID_TAG_RACE_ELEMENT, {self._XMLID_ATTR_RACE_ATTRIBUTE: str(self.race)})
        etree.SubElement(e, self._XMLID_TAG_SUBSPECIES_ELEMENT, {self._XMLID_ATTR_SUBSPECIES_ATTRIBUTE: str(self.subspecies)})
        etree.SubElement(e, self._XMLID_TAG_FULLSPECIESNAME_ELEMENT, {self._XMLID_ATTR_FULLSPECIESNAME_ATTRIBUTE: str(self.fullSpeciesName)})
        etree.SubElement(e, self._XMLID_TAG_SLAVEVALUE_ELEMENT, {self._XMLID_ATTR_SLAVEVALUE_ATTRIBUTE: str(self.slaveValue)})
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data["@comment-0003"] = 'Added by HelpfulInfo mod.'
        data['race'] = str(self.race)
        data['subspecies'] = str(self.subspecies)
        data['fullSpeciesName'] = str(self.fullSpeciesName)
        data['slaveValue'] = int(self.slaveValue)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('race')) is not None:
            self.race = data['race']
        else:
            raise KeyRequiredException(self, data, 'race', 'race')
        if (sf := data.get('subspecies')) is not None:
            self.subspecies = data['subspecies']
        else:
            raise KeyRequiredException(self, data, 'subspecies', 'subspecies')
        if (sf := data.get('fullSpeciesName')) is not None:
            self.fullSpeciesName = data['fullSpeciesName']
        else:
            raise KeyRequiredException(self, data, 'fullSpeciesName', 'fullSpeciesName')
        if (sf := data.get('slaveValue')) is not None:
            self.slaveValue = data['slaveValue']
        else:
            raise KeyRequiredException(self, data, 'slaveValue', 'slaveValue')

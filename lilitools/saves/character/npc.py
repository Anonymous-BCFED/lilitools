from typing import Any, Dict, Optional

from lxml import etree

from lilitools.logging import ClickWrap
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.character.npc_specific import NPCSpecific

click = ClickWrap()


class NPC(GameCharacter):
    TAG = "NPC"

    def __init__(self) -> None:
        super().__init__()
        self.npc_specifics: NPCSpecific = NPCSpecific()

    def toXML(self, tagOverride: Optional[str] = None) -> etree._Element:
        char = super().toXML("character")

        char.append(self.npc_specifics.toXML())
        self.on_toXML_character(char)

        npc = etree.Element(tagOverride or self.TAG)
        npc.append(char)
        return npc

    def toDict(self) -> Dict[str, Any]:
        char = super().toDict()
        char["npcSpecific"] = self.npc_specifics.toDict()
        self.on_toDict_character(char)
        return char

    def fromDict(self, data: Dict[str, Any]) -> None:
        char = data["character"]
        super().fromDict(char)
        self.on_fromDict_character(char)
        if (npcSpecific := char.get("npcSpecific")) is not None:
            self.npc_specifics.fromDict(npcSpecific)

    def on_toXML_character(self, char: etree._Element) -> None:
        if self.__class__.__qualname__ != "NPC":
            raise NotImplementedError(
                f"Not implemented: {self.__class__.__qualname__}.on_toXML_character()"
            )

    def on_toDict_character(self, char: Dict[str, Any]) -> None:
        if self.__class__.__qualname__ != "NPC":
            raise NotImplementedError(
                f"Not implemented: {self.__class__.__qualname__}.on_toDict_character()"
            )

    def fromXML(self, e: etree._Element) -> None:
        char = e.find("character")
        assert char is not None

        super().fromXML(char)
        self.on_fromXML_character(char)
        if (npcspecific := char.find("npcSpecific")) is not None:
            self.npc_specifics.fromXML(npcspecific)

    def on_fromXML_character(self, char: etree._Element) -> None:
        if self.__class__.__qualname__ != "NPC":
            raise NotImplementedError(
                f"Not implemented: {self.__class__.__qualname__}.on_fromXML_character()"
            )

    def on_fromDict_character(self, char: Dict[str, Any]) -> None:
        if self.__class__.__qualname__ != "NPC":
            raise NotImplementedError(
                f"Not implemented: {self.__class__.__qualname__}.on_fromDict_character()"
            )

    def show(self) -> None:
        super().show()
        self.npc_specifics.show()

    def getCharacterForExport(self) -> GameCharacter:
        return super()

from lilitools.logging import ClickWrap
from lilitools.utils import bool2yesno

from ._savables.npc_specific import RawNPCSpecific

click = ClickWrap()


class NPCSpecific(RawNPCSpecific):
    def show(self) -> None:
        from lilitools.cli.units import f2pct
        with click.echo('NPC-Specific:'):
            #self.lastTimeEncountered: int = 0  # Element
            click.echo(f'Last Time Encountered: {self.lastTimeEncountered}')
            #self.buyModifier: float = 0.  # Element
            click.echo(f'Buy Modifier: {f2pct(self.buyModifier)}')
            #self.sellModifier: float = 0.  # Element
            click.echo(f'Sell Modifier: {f2pct(self.buyModifier)}')
            #self.addedToContacts: bool = False  # Element
            click.echo(f'Is Added To Contacts: ' + bool2yesno(self.addedToContacts))
            #self.flagValues: Set[str] = set()
            click.echo(f'Flag Values: {self.flagValues!r}')
            #self.genderPreference: Optional[str] = None  # Element
            click.echo(f'Gender Preference: {self.genderPreference!r}')
            #self.subspeciesPreference: Optional[str] = None  # Element
            click.echo(f'Subspecies Preference: {self.subspeciesPreference!r}')
            #self.raceStagePreference: Optional[str] = None  # Element
            click.echo(f'Race Stage Preference: {self.raceStagePreference!r}')

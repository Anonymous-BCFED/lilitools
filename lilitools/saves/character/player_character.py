from typing import Optional

from lxml import etree

from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.character.player_specific import PlayerSpecific


class PlayerCharacter(GameCharacter):
    TAG = 'playerCharacter'

    def __init__(self) -> None:
        super().__init__()

        self.player_specific: PlayerSpecific = PlayerSpecific()

    def toXML(self, tagOverride: Optional[str] = None) -> etree._Element:
        e = super().toXML('character')

        e.append(self.player_specific.toXML())

        npc = etree.Element(tagOverride or self.TAG)
        npc.append(e)
        return npc

    def fromXML(self, e: etree._Element) -> None:
        char = e.find('character')
        assert char is not None
        core = char.find('core')
        super().fromXML(char)
        self.player_specific.fromXML(char.find("playerSpecific"))

    def getCharacterForExport(self) -> GameCharacter:
        return super()

from typing import Any, Dict
from lxml import etree
from lilitools.consts import PRESERVE_SAVE_ORDERING

from lilitools.saves.exceptions import ElementRequiredException

from ._savables.player_specific import RawPlayerSpecific


class PlayerSpecific(RawPlayerSpecific):

    # <questMap>
    #     <entry q0="MAIN_PROLOGUE" q1="MAIN_1_A_LILAYAS_TESTS" q2="MAIN_1_B_DEMON_HOME" questLine="MAIN"/>
    #     <entry q0="SLIME_QUEEN_ONE" questLine="SIDE_SLIME_QUEEN"/>
    #     <entry q0="SIDE_ENCHANTMENTS_LILAYA_HELP" q1="SIDE_UTIL_COMPLETE" questLine="SIDE_ENCHANTMENT_DISCOVERY"/>
    # </questMap>
    if PRESERVE_SAVE_ORDERING:

        def _fromXML_questMap(self, e: etree._Element) -> None:
            if (qmap := e.find("questMap")) is not None:
                for qe in qmap.findall("entry"):
                    lineID = qe.attrib["questLine"]
                    lineEntries = []
                    for i in range(len(qe.attrib) - 1):
                        lineEntries.append(qe.attrib[f"q{i}"])
                    self.questMap[lineID] = lineEntries
            else:
                raise ElementRequiredException(self, e, "questMap", "questMap")

        def _toXML_questMap(self, e: etree._Element) -> None:
            qmap = etree.SubElement(e, "questMap")
            for lineID, lineEntries in self.questMap.items():
                qe = etree.SubElement(qmap, "entry", {"questLine": lineID})
                for i, entry in enumerate(lineEntries):
                    qe.attrib[f"q{i}"] = entry

    else:

        def _fromXML_questMap(self, e: etree._Element) -> None:
            if (qmap := e.find("questMap")) is not None:
                for qe in qmap.findall("entry"):
                    lineID = qe.attrib["questLine"]
                    lineEntries = []
                    for i in range(len(qe.attrib) - 1):
                        lineEntries.append(qe.attrib[f"q{i}"])
                    self.questMap[lineID] = lineEntries
            else:
                raise ElementRequiredException(self, e, "questMap", "questMap")

        def _toXML_questMap(self, e: etree._Element) -> None:
            qmap = etree.SubElement(e, "questMap")
            for lineID, lineEntries in sorted(
                self.questMap.items(), key=lambda t: t[0]
            ):
                qe = etree.SubElement(qmap, "entry", {"questLine": lineID})
                for i, entry in enumerate(lineEntries):
                    qe.attrib[f"q{i}"] = entry

    def _fromDict_questMap(self, data: Dict[str, Any]) -> None:
        if (qMap := data.get("questMap")) is not None:
            self.questMap = {}
            for quest_line, entries in qMap.items():
                self.questMap[quest_line] = [str(x) for x in entries]

    def _toDict_questMap(self, data: Dict[str, Any]) -> None:
        if len(self.questMap) > 0:
            qmap = {}
            for qline, qentries in self.questMap.items():
                qmap[qline] = [str(x) for x in qentries]
            data["questMap"] = qmap

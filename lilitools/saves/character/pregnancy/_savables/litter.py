#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional, Set
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawLitter']
## from [LT]/src/com/lilithsthrone/game/character/pregnancy/Litter.java: public Element saveAsXML(Element parentElement, Document doc) { @ 8kcteV4M0AzlIm23/Ol9ScCQuyR1GTYquPIm5F+bYlD715xG5HO89OBmmmu0FR6EL7Wnhv3ehZvtA1Toc4ZwXw==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawLitter(Savable):
    TAG = 'litter'
    _XMLID_ATTR_BIRTHEDDESCRIPTION_ATTRIBUTE: str = 'birthedDescription'
    _XMLID_ATTR_DAUGHTERSFATHER_ATTRIBUTE: str = 'daughtersFather'
    _XMLID_ATTR_DAUGHTERSMOTHER_ATTRIBUTE: str = 'daughtersMother'
    _XMLID_ATTR_DAYOFBIRTH_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_DAYOFCONCEPTION_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_DAYOFINCUBATIONSTART_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_FATHERID_ATTRIBUTE: str = 'fatherId'
    _XMLID_ATTR_FATHERRACE_ATTRIBUTE: str = 'fatherRace'
    _XMLID_ATTR_INCUBATORID_ATTRIBUTE: str = 'incubatorId'
    _XMLID_ATTR_MONTHOFBIRTH_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_MONTHOFCONCEPTION_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_MONTHOFINCUBATIONSTART_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_MOTHERID_ATTRIBUTE: str = 'motherId'
    _XMLID_ATTR_MOTHERRACE_ATTRIBUTE: str = 'motherRace'
    _XMLID_ATTR_OFFSPRINGLIST_VALUEATTR: str = 'id'
    _XMLID_ATTR_SONSFATHER_ATTRIBUTE: str = 'sonsFather'
    _XMLID_ATTR_SONSMOTHER_ATTRIBUTE: str = 'sonsMother'
    _XMLID_ATTR_YEAROFBIRTH_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_YEAROFCONCEPTION_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_YEAROFINCUBATIONSTART_ATTRIBUTE: str = 'value'
    _XMLID_TAG_DAYOFBIRTH_ELEMENT: str = 'dayOfBirth'
    _XMLID_TAG_DAYOFCONCEPTION_ELEMENT: str = 'dayOfConception'
    _XMLID_TAG_DAYOFINCUBATIONSTART_ELEMENT: str = 'dayOfIncubationStart'
    _XMLID_TAG_ID_ELEMENT: str = 'id'
    _XMLID_TAG_MONTHOFBIRTH_ELEMENT: str = 'monthOfBirth'
    _XMLID_TAG_MONTHOFCONCEPTION_ELEMENT: str = 'monthOfConception'
    _XMLID_TAG_MONTHOFINCUBATIONSTART_ELEMENT: str = 'monthOfIncubationStart'
    _XMLID_TAG_OFFSPRINGLIST_CHILD: str = 'offspring'
    _XMLID_TAG_OFFSPRINGLIST_PARENT: str = 'offspringList'
    _XMLID_TAG_YEAROFBIRTH_ELEMENT: str = 'yearOfBirth'
    _XMLID_TAG_YEAROFCONCEPTION_ELEMENT: str = 'yearOfConception'
    _XMLID_TAG_YEAROFINCUBATIONSTART_ELEMENT: str = 'yearOfIncubationStart'

    def __init__(self) -> None:
        super().__init__()
        self.motherId: str = ''
        self.fatherId: str = ''
        self.incubatorId: str = ''
        self.sonsMother: int = 0
        self.daughtersMother: int = 0
        self.sonsFather: int = 0
        self.daughtersFather: int = 0
        self.motherRace: str = ''
        self.fatherRace: str = ''
        self.birthedDescription: str = ''
        self.id: str = ''  # Element
        self.yearOfBirth: int = 0  # Element
        self.monthOfBirth: str = ''  # Element
        self.dayOfBirth: int = 0  # Element
        self.yearOfConception: int = 0  # Element
        self.monthOfConception: str = ''  # Element
        self.dayOfConception: int = 0  # Element
        self.yearOfIncubationStart: Optional[int] = None  # Element
        self.monthOfIncubationStart: Optional[str] = None  # Element
        self.dayOfIncubationStart: Optional[int] = None  # Element
        self.offspringList: Set[str] = set()

    def overrideAttrs(self, birthedDescription_attribute: _Optional_str = None, daughtersFather_attribute: _Optional_str = None, daughtersMother_attribute: _Optional_str = None, dayOfBirth_attribute: _Optional_str = None, dayOfConception_attribute: _Optional_str = None, dayOfIncubationStart_attribute: _Optional_str = None, fatherId_attribute: _Optional_str = None, fatherRace_attribute: _Optional_str = None, incubatorId_attribute: _Optional_str = None, monthOfBirth_attribute: _Optional_str = None, monthOfConception_attribute: _Optional_str = None, monthOfIncubationStart_attribute: _Optional_str = None, motherId_attribute: _Optional_str = None, motherRace_attribute: _Optional_str = None, offspringList_valueattr: _Optional_str = None, sonsFather_attribute: _Optional_str = None, sonsMother_attribute: _Optional_str = None, yearOfBirth_attribute: _Optional_str = None, yearOfConception_attribute: _Optional_str = None, yearOfIncubationStart_attribute: _Optional_str = None) -> None:
        if birthedDescription_attribute is not None:
            self._XMLID_ATTR_BIRTHEDDESCRIPTION_ATTRIBUTE = birthedDescription_attribute
        if daughtersFather_attribute is not None:
            self._XMLID_ATTR_DAUGHTERSFATHER_ATTRIBUTE = daughtersFather_attribute
        if daughtersMother_attribute is not None:
            self._XMLID_ATTR_DAUGHTERSMOTHER_ATTRIBUTE = daughtersMother_attribute
        if dayOfBirth_attribute is not None:
            self._XMLID_ATTR_DAYOFBIRTH_ATTRIBUTE = dayOfBirth_attribute
        if dayOfConception_attribute is not None:
            self._XMLID_ATTR_DAYOFCONCEPTION_ATTRIBUTE = dayOfConception_attribute
        if dayOfIncubationStart_attribute is not None:
            self._XMLID_ATTR_DAYOFINCUBATIONSTART_ATTRIBUTE = dayOfIncubationStart_attribute
        if fatherId_attribute is not None:
            self._XMLID_ATTR_FATHERID_ATTRIBUTE = fatherId_attribute
        if fatherRace_attribute is not None:
            self._XMLID_ATTR_FATHERRACE_ATTRIBUTE = fatherRace_attribute
        if incubatorId_attribute is not None:
            self._XMLID_ATTR_INCUBATORID_ATTRIBUTE = incubatorId_attribute
        if monthOfBirth_attribute is not None:
            self._XMLID_ATTR_MONTHOFBIRTH_ATTRIBUTE = monthOfBirth_attribute
        if monthOfConception_attribute is not None:
            self._XMLID_ATTR_MONTHOFCONCEPTION_ATTRIBUTE = monthOfConception_attribute
        if monthOfIncubationStart_attribute is not None:
            self._XMLID_ATTR_MONTHOFINCUBATIONSTART_ATTRIBUTE = monthOfIncubationStart_attribute
        if motherId_attribute is not None:
            self._XMLID_ATTR_MOTHERID_ATTRIBUTE = motherId_attribute
        if motherRace_attribute is not None:
            self._XMLID_ATTR_MOTHERRACE_ATTRIBUTE = motherRace_attribute
        if offspringList_valueattr is not None:
            self._XMLID_ATTR_OFFSPRINGLIST_VALUEATTR = offspringList_valueattr
        if sonsFather_attribute is not None:
            self._XMLID_ATTR_SONSFATHER_ATTRIBUTE = sonsFather_attribute
        if sonsMother_attribute is not None:
            self._XMLID_ATTR_SONSMOTHER_ATTRIBUTE = sonsMother_attribute
        if yearOfBirth_attribute is not None:
            self._XMLID_ATTR_YEAROFBIRTH_ATTRIBUTE = yearOfBirth_attribute
        if yearOfConception_attribute is not None:
            self._XMLID_ATTR_YEAROFCONCEPTION_ATTRIBUTE = yearOfConception_attribute
        if yearOfIncubationStart_attribute is not None:
            self._XMLID_ATTR_YEAROFINCUBATIONSTART_ATTRIBUTE = yearOfIncubationStart_attribute

    def overrideTags(self, dayOfBirth_element: _Optional_str = None, dayOfConception_element: _Optional_str = None, dayOfIncubationStart_element: _Optional_str = None, id_element: _Optional_str = None, monthOfBirth_element: _Optional_str = None, monthOfConception_element: _Optional_str = None, monthOfIncubationStart_element: _Optional_str = None, offspringList_child: _Optional_str = None, offspringList_parent: _Optional_str = None, yearOfBirth_element: _Optional_str = None, yearOfConception_element: _Optional_str = None, yearOfIncubationStart_element: _Optional_str = None) -> None:
        if dayOfBirth_element is not None:
            self._XMLID_TAG_DAYOFBIRTH_ELEMENT = dayOfBirth_element
        if dayOfConception_element is not None:
            self._XMLID_TAG_DAYOFCONCEPTION_ELEMENT = dayOfConception_element
        if dayOfIncubationStart_element is not None:
            self._XMLID_TAG_DAYOFINCUBATIONSTART_ELEMENT = dayOfIncubationStart_element
        if id_element is not None:
            self._XMLID_TAG_ID_ELEMENT = id_element
        if monthOfBirth_element is not None:
            self._XMLID_TAG_MONTHOFBIRTH_ELEMENT = monthOfBirth_element
        if monthOfConception_element is not None:
            self._XMLID_TAG_MONTHOFCONCEPTION_ELEMENT = monthOfConception_element
        if monthOfIncubationStart_element is not None:
            self._XMLID_TAG_MONTHOFINCUBATIONSTART_ELEMENT = monthOfIncubationStart_element
        if offspringList_child is not None:
            self._XMLID_TAG_OFFSPRINGLIST_CHILD = offspringList_child
        if offspringList_parent is not None:
            self._XMLID_TAG_OFFSPRINGLIST_PARENT = offspringList_parent
        if yearOfBirth_element is not None:
            self._XMLID_TAG_YEAROFBIRTH_ELEMENT = yearOfBirth_element
        if yearOfConception_element is not None:
            self._XMLID_TAG_YEAROFCONCEPTION_ELEMENT = yearOfConception_element
        if yearOfIncubationStart_element is not None:
            self._XMLID_TAG_YEAROFINCUBATIONSTART_ELEMENT = yearOfIncubationStart_element

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        sf: etree._Element
        value = e.get(self._XMLID_ATTR_MOTHERID_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_MOTHERID_ATTRIBUTE, 'motherId')
        else:
            self.motherId = str(value)
        value = e.get(self._XMLID_ATTR_FATHERID_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_FATHERID_ATTRIBUTE, 'fatherId')
        else:
            self.fatherId = str(value)
        value = e.get(self._XMLID_ATTR_INCUBATORID_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_INCUBATORID_ATTRIBUTE, 'incubatorId')
        else:
            self.incubatorId = str(value)
        value = e.get(self._XMLID_ATTR_SONSMOTHER_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_SONSMOTHER_ATTRIBUTE, 'sonsMother')
        else:
            self.sonsMother = int(value)
        value = e.get(self._XMLID_ATTR_DAUGHTERSMOTHER_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_DAUGHTERSMOTHER_ATTRIBUTE, 'daughtersMother')
        else:
            self.daughtersMother = int(value)
        value = e.get(self._XMLID_ATTR_SONSFATHER_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_SONSFATHER_ATTRIBUTE, 'sonsFather')
        else:
            self.sonsFather = int(value)
        value = e.get(self._XMLID_ATTR_DAUGHTERSFATHER_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_DAUGHTERSFATHER_ATTRIBUTE, 'daughtersFather')
        else:
            self.daughtersFather = int(value)
        value = e.get(self._XMLID_ATTR_MOTHERRACE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_MOTHERRACE_ATTRIBUTE, 'motherRace')
        else:
            self.motherRace = str(value)
        value = e.get(self._XMLID_ATTR_FATHERRACE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_FATHERRACE_ATTRIBUTE, 'fatherRace')
        else:
            self.fatherRace = str(value)
        value = e.get(self._XMLID_ATTR_BIRTHEDDESCRIPTION_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_BIRTHEDDESCRIPTION_ATTRIBUTE, 'birthedDescription')
        else:
            self.birthedDescription = str(value)
        if (sf := e.find(self._XMLID_TAG_ID_ELEMENT)) is not None:
            self.id = sf.text
        else:
            raise ElementRequiredException(self, e, 'id', self._XMLID_TAG_ID_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_YEAROFBIRTH_ELEMENT)) is not None:
            if self._XMLID_ATTR_YEAROFBIRTH_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_YEAROFBIRTH_ATTRIBUTE, 'yearOfBirth')
            self.yearOfBirth = int(sf.attrib[self._XMLID_ATTR_YEAROFBIRTH_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'yearOfBirth', self._XMLID_TAG_YEAROFBIRTH_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_MONTHOFBIRTH_ELEMENT)) is not None:
            if self._XMLID_ATTR_MONTHOFBIRTH_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_MONTHOFBIRTH_ATTRIBUTE, 'monthOfBirth')
            self.monthOfBirth = sf.attrib[self._XMLID_ATTR_MONTHOFBIRTH_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'monthOfBirth', self._XMLID_TAG_MONTHOFBIRTH_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_DAYOFBIRTH_ELEMENT)) is not None:
            if self._XMLID_ATTR_DAYOFBIRTH_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_DAYOFBIRTH_ATTRIBUTE, 'dayOfBirth')
            self.dayOfBirth = int(sf.attrib[self._XMLID_ATTR_DAYOFBIRTH_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'dayOfBirth', self._XMLID_TAG_DAYOFBIRTH_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_YEAROFCONCEPTION_ELEMENT)) is not None:
            if self._XMLID_ATTR_YEAROFCONCEPTION_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_YEAROFCONCEPTION_ATTRIBUTE, 'yearOfConception')
            self.yearOfConception = int(sf.attrib[self._XMLID_ATTR_YEAROFCONCEPTION_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'yearOfConception', self._XMLID_TAG_YEAROFCONCEPTION_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_MONTHOFCONCEPTION_ELEMENT)) is not None:
            if self._XMLID_ATTR_MONTHOFCONCEPTION_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_MONTHOFCONCEPTION_ATTRIBUTE, 'monthOfConception')
            self.monthOfConception = sf.attrib[self._XMLID_ATTR_MONTHOFCONCEPTION_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'monthOfConception', self._XMLID_TAG_MONTHOFCONCEPTION_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_DAYOFCONCEPTION_ELEMENT)) is not None:
            if self._XMLID_ATTR_DAYOFCONCEPTION_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_DAYOFCONCEPTION_ATTRIBUTE, 'dayOfConception')
            self.dayOfConception = int(sf.attrib[self._XMLID_ATTR_DAYOFCONCEPTION_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'dayOfConception', self._XMLID_TAG_DAYOFCONCEPTION_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_YEAROFINCUBATIONSTART_ELEMENT)) is not None:
            self.yearOfIncubationStart = int(sf.attrib.get(self._XMLID_ATTR_YEAROFINCUBATIONSTART_ATTRIBUTE, "None"))
        if (sf := e.find(self._XMLID_TAG_MONTHOFINCUBATIONSTART_ELEMENT)) is not None:
            self.monthOfIncubationStart = sf.attrib.get(self._XMLID_ATTR_MONTHOFINCUBATIONSTART_ATTRIBUTE, None)
        if (sf := e.find(self._XMLID_TAG_DAYOFINCUBATIONSTART_ELEMENT)) is not None:
            self.dayOfIncubationStart = int(sf.attrib.get(self._XMLID_ATTR_DAYOFINCUBATIONSTART_ATTRIBUTE, "None"))
        if (lf := e.find(self._XMLID_TAG_OFFSPRINGLIST_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_OFFSPRINGLIST_CHILD):
                    self.offspringList.add(lc.attrib[self._XMLID_ATTR_OFFSPRINGLIST_VALUEATTR])
        else:
            self.offspringList = set()

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        e.attrib[self._XMLID_ATTR_MOTHERID_ATTRIBUTE] = str(self.motherId)
        e.attrib[self._XMLID_ATTR_FATHERID_ATTRIBUTE] = str(self.fatherId)
        e.attrib[self._XMLID_ATTR_INCUBATORID_ATTRIBUTE] = str(self.incubatorId)
        e.attrib[self._XMLID_ATTR_SONSMOTHER_ATTRIBUTE] = str(self.sonsMother)
        e.attrib[self._XMLID_ATTR_DAUGHTERSMOTHER_ATTRIBUTE] = str(self.daughtersMother)
        e.attrib[self._XMLID_ATTR_SONSFATHER_ATTRIBUTE] = str(self.sonsFather)
        e.attrib[self._XMLID_ATTR_DAUGHTERSFATHER_ATTRIBUTE] = str(self.daughtersFather)
        e.attrib[self._XMLID_ATTR_MOTHERRACE_ATTRIBUTE] = str(self.motherRace)
        e.attrib[self._XMLID_ATTR_FATHERRACE_ATTRIBUTE] = str(self.fatherRace)
        e.attrib[self._XMLID_ATTR_BIRTHEDDESCRIPTION_ATTRIBUTE] = str(self.birthedDescription)
        etree.SubElement(e, self._XMLID_TAG_ID_ELEMENT).text = str(self.id)
        etree.SubElement(e, self._XMLID_TAG_YEAROFBIRTH_ELEMENT, {self._XMLID_ATTR_YEAROFBIRTH_ATTRIBUTE: str(self.yearOfBirth)})
        etree.SubElement(e, self._XMLID_TAG_MONTHOFBIRTH_ELEMENT, {self._XMLID_ATTR_MONTHOFBIRTH_ATTRIBUTE: str(self.monthOfBirth)})
        etree.SubElement(e, self._XMLID_TAG_DAYOFBIRTH_ELEMENT, {self._XMLID_ATTR_DAYOFBIRTH_ATTRIBUTE: str(self.dayOfBirth)})
        etree.SubElement(e, self._XMLID_TAG_YEAROFCONCEPTION_ELEMENT, {self._XMLID_ATTR_YEAROFCONCEPTION_ATTRIBUTE: str(self.yearOfConception)})
        etree.SubElement(e, self._XMLID_TAG_MONTHOFCONCEPTION_ELEMENT, {self._XMLID_ATTR_MONTHOFCONCEPTION_ATTRIBUTE: str(self.monthOfConception)})
        etree.SubElement(e, self._XMLID_TAG_DAYOFCONCEPTION_ELEMENT, {self._XMLID_ATTR_DAYOFCONCEPTION_ATTRIBUTE: str(self.dayOfConception)})
        if self.yearOfIncubationStart is not None:
            etree.SubElement(e, self._XMLID_TAG_YEAROFINCUBATIONSTART_ELEMENT, {self._XMLID_ATTR_YEAROFINCUBATIONSTART_ATTRIBUTE: str(self.yearOfIncubationStart)})
        if self.monthOfIncubationStart is not None:
            etree.SubElement(e, self._XMLID_TAG_MONTHOFINCUBATIONSTART_ELEMENT, {self._XMLID_ATTR_MONTHOFINCUBATIONSTART_ATTRIBUTE: str(self.monthOfIncubationStart)})
        if self.dayOfIncubationStart is not None:
            etree.SubElement(e, self._XMLID_TAG_DAYOFINCUBATIONSTART_ELEMENT, {self._XMLID_ATTR_DAYOFINCUBATIONSTART_ATTRIBUTE: str(self.dayOfIncubationStart)})
        lp = etree.SubElement(e, self._XMLID_TAG_OFFSPRINGLIST_PARENT)
        for lv in sorted(self.offspringList, key=str):
            etree.SubElement(lp, self._XMLID_TAG_OFFSPRINGLIST_CHILD, {self._XMLID_ATTR_OFFSPRINGLIST_VALUEATTR: lv})
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['motherId'] = self.motherId
        data['fatherId'] = self.fatherId
        data['incubatorId'] = self.incubatorId
        data['sonsMother'] = int(self.sonsMother)
        data['daughtersMother'] = int(self.daughtersMother)
        data['sonsFather'] = int(self.sonsFather)
        data['daughtersFather'] = int(self.daughtersFather)
        data['motherRace'] = self.motherRace
        data['fatherRace'] = self.fatherRace
        data['birthedDescription'] = self.birthedDescription
        data['id'] = str(self.id)
        data['yearOfBirth'] = int(self.yearOfBirth)
        data['monthOfBirth'] = str(self.monthOfBirth)
        data['dayOfBirth'] = int(self.dayOfBirth)
        data['yearOfConception'] = int(self.yearOfConception)
        data['monthOfConception'] = str(self.monthOfConception)
        data['dayOfConception'] = int(self.dayOfConception)
        if self.yearOfIncubationStart is not None:
            data['yearOfIncubationStart'] = int(self.yearOfIncubationStart)
        if self.monthOfIncubationStart is not None:
            data['monthOfIncubationStart'] = str(self.monthOfIncubationStart)
        if self.dayOfIncubationStart is not None:
            data['dayOfIncubationStart'] = int(self.dayOfIncubationStart)
        if self.offspringList is None or len(self.offspringList) > 0:
            data['offspringList'] = list()
        else:
            lc = list()
            if len(self.offspringList) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['offspringList'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'motherId' not in data:
            raise KeyRequiredException(self, data, 'motherId', 'motherId')
        self.motherId = str(data['motherId'])
        if 'fatherId' not in data:
            raise KeyRequiredException(self, data, 'fatherId', 'fatherId')
        self.fatherId = str(data['fatherId'])
        if 'incubatorId' not in data:
            raise KeyRequiredException(self, data, 'incubatorId', 'incubatorId')
        self.incubatorId = str(data['incubatorId'])
        if 'sonsMother' not in data:
            raise KeyRequiredException(self, data, 'sonsMother', 'sonsMother')
        self.sonsMother = int(data['sonsMother'])
        if 'daughtersMother' not in data:
            raise KeyRequiredException(self, data, 'daughtersMother', 'daughtersMother')
        self.daughtersMother = int(data['daughtersMother'])
        if 'sonsFather' not in data:
            raise KeyRequiredException(self, data, 'sonsFather', 'sonsFather')
        self.sonsFather = int(data['sonsFather'])
        if 'daughtersFather' not in data:
            raise KeyRequiredException(self, data, 'daughtersFather', 'daughtersFather')
        self.daughtersFather = int(data['daughtersFather'])
        if 'motherRace' not in data:
            raise KeyRequiredException(self, data, 'motherRace', 'motherRace')
        self.motherRace = str(data['motherRace'])
        if 'fatherRace' not in data:
            raise KeyRequiredException(self, data, 'fatherRace', 'fatherRace')
        self.fatherRace = str(data['fatherRace'])
        if 'birthedDescription' not in data:
            raise KeyRequiredException(self, data, 'birthedDescription', 'birthedDescription')
        self.birthedDescription = str(data['birthedDescription'])
        if (sf := data.get('id')) is not None:
            self.id = data['id']
        else:
            raise KeyRequiredException(self, data, 'id', 'id')
        if (sf := data.get('yearOfBirth')) is not None:
            self.yearOfBirth = data['yearOfBirth']
        else:
            raise KeyRequiredException(self, data, 'yearOfBirth', 'yearOfBirth')
        if (sf := data.get('monthOfBirth')) is not None:
            self.monthOfBirth = data['monthOfBirth']
        else:
            raise KeyRequiredException(self, data, 'monthOfBirth', 'monthOfBirth')
        if (sf := data.get('dayOfBirth')) is not None:
            self.dayOfBirth = data['dayOfBirth']
        else:
            raise KeyRequiredException(self, data, 'dayOfBirth', 'dayOfBirth')
        if (sf := data.get('yearOfConception')) is not None:
            self.yearOfConception = data['yearOfConception']
        else:
            raise KeyRequiredException(self, data, 'yearOfConception', 'yearOfConception')
        if (sf := data.get('monthOfConception')) is not None:
            self.monthOfConception = data['monthOfConception']
        else:
            raise KeyRequiredException(self, data, 'monthOfConception', 'monthOfConception')
        if (sf := data.get('dayOfConception')) is not None:
            self.dayOfConception = data['dayOfConception']
        else:
            raise KeyRequiredException(self, data, 'dayOfConception', 'dayOfConception')
        if (sf := data.get('yearOfIncubationStart')) is not None:
            self.yearOfIncubationStart = data['yearOfIncubationStart']
        else:
            self.yearOfIncubationStart = None
        if (sf := data.get('monthOfIncubationStart')) is not None:
            self.monthOfIncubationStart = data['monthOfIncubationStart']
        else:
            self.monthOfIncubationStart = None
        if (sf := data.get('dayOfIncubationStart')) is not None:
            self.dayOfIncubationStart = data['dayOfIncubationStart']
        else:
            self.dayOfIncubationStart = None
        if (lv := self.offspringList) is not None:
            for le in lv:
                self.offspringList.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'offspringList', 'offspringList')

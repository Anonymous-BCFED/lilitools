#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, List, Optional, Set
from lxml import etree
from lilitools.saves.character.pregnancy.litter import Litter
from lilitools.saves.character.pregnancy.pregnancy_possibility import PregnancyPossibility
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawPregnancy']
## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public Element saveAsXML(Element parentElement, Document doc) { @ Ra1rfw7eKd5WT2JXoGa/zLFvb/XgZlpPNZbY2BZwgdTA/t0DkASy5ADAtBbX2DvSTxI43cQAGTPbPsMVcAHyRQ==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawPregnancy(Savable):
    TAG = 'pregnancy'
    _XMLID_ATTR_INCUBATINGLITTERS_KEYATTR: str = 'orifice'
    _XMLID_ATTR_LITTERSGENERATED_ATTRIBUTE: str = 'littersGenerated'
    _XMLID_ATTR_PREGNANCYREACTIONS_VALUEATTR: str = 'value'
    _XMLID_ATTR_PREGNANTLITTER_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_TIMEPROGRESSEDTOFINALINCUBATIONSTAGE_KEYATTR: str = 'orifice'
    _XMLID_ATTR_TIMEPROGRESSEDTOFINALPREGNANCYSTAGE_ATTRIBUTE: str = 'timeProgressedToFinalPregnancyStage'
    _XMLID_TAG_BIRTHEDLITTERS_PARENT: str = 'birthedLitters'
    _XMLID_TAG_BIRTHEDLITTERS_VALUEELEM: Optional[str] = None
    _XMLID_TAG_IMPLANTEDLITTERS_PARENT: str = 'implantedLitters'
    _XMLID_TAG_IMPLANTEDLITTERS_VALUEELEM: Optional[str] = None
    _XMLID_TAG_INCUBATEDLITTERS_PARENT: str = 'incubatedLitters'
    _XMLID_TAG_INCUBATEDLITTERS_VALUEELEM: Optional[str] = None
    _XMLID_TAG_INCUBATINGLITTERS_CHILD: str = 'entry'
    _XMLID_TAG_INCUBATINGLITTERS_PARENT: str = 'incubatingLitters'
    _XMLID_TAG_LITTERSFATHERED_PARENT: str = 'littersFathered'
    _XMLID_TAG_LITTERSFATHERED_VALUEELEM: Optional[str] = None
    _XMLID_TAG_POTENTIALPARTNERSASFATHER_PARENT: str = 'potentialPartnersAsFather'
    _XMLID_TAG_POTENTIALPARTNERSASFATHER_VALUEELEM: Optional[str] = None
    _XMLID_TAG_POTENTIALPARTNERSASMOTHER_PARENT: str = 'potentialPartnersAsMother'
    _XMLID_TAG_POTENTIALPARTNERSASMOTHER_VALUEELEM: Optional[str] = None
    _XMLID_TAG_PREGNANCYREACTIONS_CHILD: str = 'id'
    _XMLID_TAG_PREGNANCYREACTIONS_PARENT: str = 'pregnancyReactions'
    _XMLID_TAG_PREGNANTLITTER_ELEMENT: str = 'pregnantLitter'
    _XMLID_TAG_PREGNANTLITTER_SUBELEMENT: str = 'litter'
    _XMLID_TAG_TIMEPROGRESSEDTOFINALINCUBATIONSTAGE_CHILD: str = 'entry'
    _XMLID_TAG_TIMEPROGRESSEDTOFINALINCUBATIONSTAGE_PARENT: str = 'timeProgressedToFinalIncubationStage'

    def __init__(self) -> None:
        super().__init__()
        self.timeProgressedToFinalPregnancyStage: int = 0
        self.littersGenerated: int = 0
        self.timeProgressedToFinalIncubationStage: Dict[str, int] = {}
        self.potentialPartnersAsMother: Optional[Set[PregnancyPossibility]] = set()
        self.potentialPartnersAsFather: Optional[Set[PregnancyPossibility]] = set()
        self.pregnantLitter: Optional[Litter] = None  # Element
        self.incubatingLitters: Dict[str, Litter] = {}
        self.birthedLitters: Optional[List[Litter]] = list()
        self.littersFathered: Optional[List[Litter]] = list()
        self.incubatedLitters: Optional[List[Litter]] = list()
        self.implantedLitters: Optional[List[Litter]] = list()
        self.pregnancyReactions: Optional[Set[str]] = set()

    def overrideAttrs(self, incubatingLitters_keyattr: _Optional_str = None, littersGenerated_attribute: _Optional_str = None, pregnancyReactions_valueattr: _Optional_str = None, pregnantLitter_attribute: _Optional_str = None, timeProgressedToFinalIncubationStage_keyattr: _Optional_str = None, timeProgressedToFinalPregnancyStage_attribute: _Optional_str = None) -> None:
        if incubatingLitters_keyattr is not None:
            self._XMLID_ATTR_INCUBATINGLITTERS_KEYATTR = incubatingLitters_keyattr
        if littersGenerated_attribute is not None:
            self._XMLID_ATTR_LITTERSGENERATED_ATTRIBUTE = littersGenerated_attribute
        if pregnancyReactions_valueattr is not None:
            self._XMLID_ATTR_PREGNANCYREACTIONS_VALUEATTR = pregnancyReactions_valueattr
        if pregnantLitter_attribute is not None:
            self._XMLID_ATTR_PREGNANTLITTER_ATTRIBUTE = pregnantLitter_attribute
        if timeProgressedToFinalIncubationStage_keyattr is not None:
            self._XMLID_ATTR_TIMEPROGRESSEDTOFINALINCUBATIONSTAGE_KEYATTR = timeProgressedToFinalIncubationStage_keyattr
        if timeProgressedToFinalPregnancyStage_attribute is not None:
            self._XMLID_ATTR_TIMEPROGRESSEDTOFINALPREGNANCYSTAGE_ATTRIBUTE = timeProgressedToFinalPregnancyStage_attribute

    def overrideTags(self, birthedLitters_parent: _Optional_str = None, birthedLitters_valueelem: _Optional_str = None, implantedLitters_parent: _Optional_str = None, implantedLitters_valueelem: _Optional_str = None, incubatedLitters_parent: _Optional_str = None, incubatedLitters_valueelem: _Optional_str = None, incubatingLitters_child: _Optional_str = None, incubatingLitters_parent: _Optional_str = None, littersFathered_parent: _Optional_str = None, littersFathered_valueelem: _Optional_str = None, potentialPartnersAsFather_parent: _Optional_str = None, potentialPartnersAsFather_valueelem: _Optional_str = None, potentialPartnersAsMother_parent: _Optional_str = None, potentialPartnersAsMother_valueelem: _Optional_str = None, pregnancyReactions_child: _Optional_str = None, pregnancyReactions_parent: _Optional_str = None, pregnantLitter_element: _Optional_str = None, pregnantLitter_subelement: _Optional_str = None, timeProgressedToFinalIncubationStage_child: _Optional_str = None, timeProgressedToFinalIncubationStage_parent: _Optional_str = None) -> None:
        if birthedLitters_parent is not None:
            self._XMLID_TAG_BIRTHEDLITTERS_PARENT = birthedLitters_parent
        if birthedLitters_valueelem is not None:
            self._XMLID_TAG_BIRTHEDLITTERS_VALUEELEM = birthedLitters_valueelem
        if implantedLitters_parent is not None:
            self._XMLID_TAG_IMPLANTEDLITTERS_PARENT = implantedLitters_parent
        if implantedLitters_valueelem is not None:
            self._XMLID_TAG_IMPLANTEDLITTERS_VALUEELEM = implantedLitters_valueelem
        if incubatedLitters_parent is not None:
            self._XMLID_TAG_INCUBATEDLITTERS_PARENT = incubatedLitters_parent
        if incubatedLitters_valueelem is not None:
            self._XMLID_TAG_INCUBATEDLITTERS_VALUEELEM = incubatedLitters_valueelem
        if incubatingLitters_child is not None:
            self._XMLID_TAG_INCUBATINGLITTERS_CHILD = incubatingLitters_child
        if incubatingLitters_parent is not None:
            self._XMLID_TAG_INCUBATINGLITTERS_PARENT = incubatingLitters_parent
        if littersFathered_parent is not None:
            self._XMLID_TAG_LITTERSFATHERED_PARENT = littersFathered_parent
        if littersFathered_valueelem is not None:
            self._XMLID_TAG_LITTERSFATHERED_VALUEELEM = littersFathered_valueelem
        if potentialPartnersAsFather_parent is not None:
            self._XMLID_TAG_POTENTIALPARTNERSASFATHER_PARENT = potentialPartnersAsFather_parent
        if potentialPartnersAsFather_valueelem is not None:
            self._XMLID_TAG_POTENTIALPARTNERSASFATHER_VALUEELEM = potentialPartnersAsFather_valueelem
        if potentialPartnersAsMother_parent is not None:
            self._XMLID_TAG_POTENTIALPARTNERSASMOTHER_PARENT = potentialPartnersAsMother_parent
        if potentialPartnersAsMother_valueelem is not None:
            self._XMLID_TAG_POTENTIALPARTNERSASMOTHER_VALUEELEM = potentialPartnersAsMother_valueelem
        if pregnancyReactions_child is not None:
            self._XMLID_TAG_PREGNANCYREACTIONS_CHILD = pregnancyReactions_child
        if pregnancyReactions_parent is not None:
            self._XMLID_TAG_PREGNANCYREACTIONS_PARENT = pregnancyReactions_parent
        if pregnantLitter_element is not None:
            self._XMLID_TAG_PREGNANTLITTER_ELEMENT = pregnantLitter_element
        if pregnantLitter_subelement is not None:
            self._XMLID_TAG_PREGNANTLITTER_SUBELEMENT = pregnantLitter_subelement
        if timeProgressedToFinalIncubationStage_child is not None:
            self._XMLID_TAG_TIMEPROGRESSEDTOFINALINCUBATIONSTAGE_CHILD = timeProgressedToFinalIncubationStage_child
        if timeProgressedToFinalIncubationStage_parent is not None:
            self._XMLID_TAG_TIMEPROGRESSEDTOFINALINCUBATIONSTAGE_PARENT = timeProgressedToFinalIncubationStage_parent

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        dc: etree._Element
        df: etree._Element
        dv: Savable
        lk: str
        lp: etree._Element
        lv: Savable
        se: etree._Element
        sf: etree._Element
        value = e.get(self._XMLID_ATTR_TIMEPROGRESSEDTOFINALPREGNANCYSTAGE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_TIMEPROGRESSEDTOFINALPREGNANCYSTAGE_ATTRIBUTE, 'timeProgressedToFinalPregnancyStage')
        else:
            self.timeProgressedToFinalPregnancyStage = int(value)
        value = e.get(self._XMLID_ATTR_LITTERSGENERATED_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_LITTERSGENERATED_ATTRIBUTE, 'littersGenerated')
        else:
            self.littersGenerated = int(value)
        if (df := e.find(self._XMLID_TAG_TIMEPROGRESSEDTOFINALINCUBATIONSTAGE_PARENT)) is not None:
            for dc in df.iterfind(self._XMLID_TAG_TIMEPROGRESSEDTOFINALINCUBATIONSTAGE_CHILD):
                self.timeProgressedToFinalIncubationStage[dc.attrib['orifice']] = int(dc.text)
        if (lf := e.find(self._XMLID_TAG_POTENTIALPARTNERSASMOTHER_PARENT)) is not None:
            if lf is not None:
                for lc in lf:
                    lv = PregnancyPossibility()
                    lv.fromXML(lc)
                    self.potentialPartnersAsMother.add(lv)
        if (lf := e.find(self._XMLID_TAG_POTENTIALPARTNERSASFATHER_PARENT)) is not None:
            if lf is not None:
                for lc in lf:
                    lv = PregnancyPossibility()
                    lv.fromXML(lc)
                    self.potentialPartnersAsFather.add(lv)
        if (se := e.find(self._XMLID_TAG_PREGNANTLITTER_ELEMENT)) is not None:
            if (sf := se.find(self._XMLID_TAG_PREGNANTLITTER_SUBELEMENT)) is not None:
                self.pregnantLitter = Litter()
                self.pregnantLitter.fromXML(sf)
        if (df := e.find(self._XMLID_TAG_INCUBATINGLITTERS_PARENT)) is not None:
            for dc in df.iterfind(self._XMLID_TAG_INCUBATINGLITTERS_CHILD):
                dv = Litter()
                dv.fromXML(dc)
                self.incubatingLitters[dc.attrib['orifice']] = dv
        if (lf := e.find(self._XMLID_TAG_BIRTHEDLITTERS_PARENT)) is not None:
            if lf is not None:
                for lc in lf:
                    lv = Litter()
                    lv.fromXML(lc)
                    self.birthedLitters.append(lv)
        if (lf := e.find(self._XMLID_TAG_LITTERSFATHERED_PARENT)) is not None:
            if lf is not None:
                for lc in lf:
                    lv = Litter()
                    lv.fromXML(lc)
                    self.littersFathered.append(lv)
        if (lf := e.find(self._XMLID_TAG_INCUBATEDLITTERS_PARENT)) is not None:
            if lf is not None:
                for lc in lf:
                    lv = Litter()
                    lv.fromXML(lc)
                    self.incubatedLitters.append(lv)
        if (lf := e.find(self._XMLID_TAG_IMPLANTEDLITTERS_PARENT)) is not None:
            if lf is not None:
                for lc in lf:
                    lv = Litter()
                    lv.fromXML(lc)
                    self.implantedLitters.append(lv)
        if (lf := e.find(self._XMLID_TAG_PREGNANCYREACTIONS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_PREGNANCYREACTIONS_CHILD):
                    self.pregnancyReactions.add(lc.attrib[self._XMLID_ATTR_PREGNANCYREACTIONS_VALUEATTR])

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        dK: Any
        dV: Any
        dc: etree._Element
        dp: etree._Element
        dsc: etree._Element
        dv: Savable
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        e.attrib[self._XMLID_ATTR_TIMEPROGRESSEDTOFINALPREGNANCYSTAGE_ATTRIBUTE] = str(self.timeProgressedToFinalPregnancyStage)
        e.attrib[self._XMLID_ATTR_LITTERSGENERATED_ATTRIBUTE] = str(self.littersGenerated)
        if self.timeProgressedToFinalIncubationStage is not None and len(self.timeProgressedToFinalIncubationStage) > 0:
            dp = etree.SubElement(e, self._XMLID_TAG_TIMEPROGRESSEDTOFINALINCUBATIONSTAGE_PARENT)
            for dK, dV in sorted(self.timeProgressedToFinalIncubationStage.items(), key=lambda t: t[0]):
                dc = etree.SubElement(dp, self._XMLID_TAG_TIMEPROGRESSEDTOFINALINCUBATIONSTAGE_CHILD)
                dc.attrib['orifice'] = dK
                dc.text = str(dV)
        if self.potentialPartnersAsMother is not None and len(self.potentialPartnersAsMother) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_POTENTIALPARTNERSASMOTHER_PARENT)
            for lv in sorted(self.potentialPartnersAsMother, key=lambda x: (x.motherId, x.fatherId)):
                lp.append(lv.toXML())
        if self.potentialPartnersAsFather is not None and len(self.potentialPartnersAsFather) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_POTENTIALPARTNERSASFATHER_PARENT)
            for lv in sorted(self.potentialPartnersAsFather, key=lambda x: (x.motherId, x.fatherId)):
                lp.append(lv.toXML())
        if self.pregnantLitter is not None:
            se = etree.SubElement(e, self._XMLID_TAG_PREGNANTLITTER_ELEMENT)
            se.append(self.pregnantLitter.toXML(self._XMLID_TAG_PREGNANTLITTER_SUBELEMENT))
        if self.incubatingLitters is not None and len(self.incubatingLitters) > 0:
            dp = etree.SubElement(e, self._XMLID_TAG_INCUBATINGLITTERS_PARENT)
            for dK, dV in sorted(self.incubatingLitters.items(), key=lambda t: t[0]):
                dc = etree.SubElement(dp, self._XMLID_TAG_INCUBATINGLITTERS_CHILD)
                dc.attrib['orifice'] = dK
                dc.append(dV.toXML(self._XMLID_TAG_INCUBATINGLITTERS_VALUEELEM))
        if self.birthedLitters is not None and len(self.birthedLitters) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_BIRTHEDLITTERS_PARENT)
            for lv in self.birthedLitters:
                lp.append(lv.toXML())
        if self.littersFathered is not None and len(self.littersFathered) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_LITTERSFATHERED_PARENT)
            for lv in self.littersFathered:
                lp.append(lv.toXML())
        if self.incubatedLitters is not None and len(self.incubatedLitters) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_INCUBATEDLITTERS_PARENT)
            for lv in self.incubatedLitters:
                lp.append(lv.toXML())
        if self.implantedLitters is not None and len(self.implantedLitters) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_IMPLANTEDLITTERS_PARENT)
            for lv in self.implantedLitters:
                lp.append(lv.toXML())
        if self.pregnancyReactions is not None and len(self.pregnancyReactions) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_PREGNANCYREACTIONS_PARENT)
            for lv in sorted(self.pregnancyReactions, key=lambda x: x):
                etree.SubElement(lp, self._XMLID_TAG_PREGNANCYREACTIONS_CHILD, {self._XMLID_ATTR_PREGNANCYREACTIONS_VALUEATTR: lv})
        return e

    def toDict(self) -> Dict[str, Any]:
        dK: Any
        dV: Any
        data: Dict[str, Any] = super().toDict()
        dp: Dict[str, Any]
        dsc: Dict[str, Any]
        dv: Savable
        data['timeProgressedToFinalPregnancyStage'] = int(self.timeProgressedToFinalPregnancyStage)
        data['littersGenerated'] = int(self.littersGenerated)
        if self.timeProgressedToFinalIncubationStage is not None and len(self.timeProgressedToFinalIncubationStage) > 0:
            dp = {}
            for dK, dV in sorted(self.timeProgressedToFinalIncubationStage.items(), key=lambda t: t[0]):
                sk = dK
                sv = dV
                dp[sk] = sv
            data['timeProgressedToFinalIncubationStage'] = dp
        if self.potentialPartnersAsMother is None or len(self.potentialPartnersAsMother) > 0:
            data['potentialPartnersAsMother'] = list()
        else:
            lc = list()
            if len(self.potentialPartnersAsMother) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['potentialPartnersAsMother'] = lc
        if self.potentialPartnersAsFather is None or len(self.potentialPartnersAsFather) > 0:
            data['potentialPartnersAsFather'] = list()
        else:
            lc = list()
            if len(self.potentialPartnersAsFather) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['potentialPartnersAsFather'] = lc
        if self.pregnantLitter is not None:
            data['pregnantLitter'] = self.pregnantLitter.toDict()
        if self.incubatingLitters is not None and len(self.incubatingLitters) > 0:
            dp = {}
            for dK, dV in sorted(self.incubatingLitters.items(), key=lambda t: t[0]):
                sk = dK
                dp[sk] = dV.toDict()
            data['incubatingLitters'] = dp
        if self.birthedLitters is None or len(self.birthedLitters) > 0:
            data['birthedLitters'] = list()
        else:
            lc = list()
            if len(self.birthedLitters) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['birthedLitters'] = lc
        if self.littersFathered is None or len(self.littersFathered) > 0:
            data['littersFathered'] = list()
        else:
            lc = list()
            if len(self.littersFathered) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['littersFathered'] = lc
        if self.incubatedLitters is None or len(self.incubatedLitters) > 0:
            data['incubatedLitters'] = list()
        else:
            lc = list()
            if len(self.incubatedLitters) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['incubatedLitters'] = lc
        if self.implantedLitters is None or len(self.implantedLitters) > 0:
            data['implantedLitters'] = list()
        else:
            lc = list()
            if len(self.implantedLitters) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['implantedLitters'] = lc
        if self.pregnancyReactions is None or len(self.pregnancyReactions) > 0:
            data['pregnancyReactions'] = list()
        else:
            lc = list()
            if len(self.pregnancyReactions) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['pregnancyReactions'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'timeProgressedToFinalPregnancyStage' not in data:
            raise KeyRequiredException(self, data, 'timeProgressedToFinalPregnancyStage', 'timeProgressedToFinalPregnancyStage')
        self.timeProgressedToFinalPregnancyStage = int(data['timeProgressedToFinalPregnancyStage'])
        if 'littersGenerated' not in data:
            raise KeyRequiredException(self, data, 'littersGenerated', 'littersGenerated')
        self.littersGenerated = int(data['littersGenerated'])
        if (df := data.get('timeProgressedToFinalIncubationStage')) is not None:
            for sk, sv in df.items():
                self.timeProgressedToFinalIncubationStage[sk] = sv
        if (lv := self.potentialPartnersAsMother) is not None:
            for le in lv:
                self.potentialPartnersAsMother.add(le.toDict())
        else:
            self.potentialPartnersAsMother = set()
        if (lv := self.potentialPartnersAsFather) is not None:
            for le in lv:
                self.potentialPartnersAsFather.add(le.toDict())
        else:
            self.potentialPartnersAsFather = set()
        if (sf := data.get('pregnantLitter')) is not None:
            self.pregnantLitter = Litter()
            self.pregnantLitter.fromDict(data['pregnantLitter'])
        else:
            self.pregnantLitter = None
        if (df := data.get('incubatingLitters')) is not None:
            for sk, sv in df.items():
                dV = Litter()
                dV.fromDict(sv)
                self.incubatingLitters[sk] = dv
        if (lv := self.birthedLitters) is not None:
            for le in lv:
                self.birthedLitters.append(le.toDict())
        else:
            self.birthedLitters = list()
        if (lv := self.littersFathered) is not None:
            for le in lv:
                self.littersFathered.append(le.toDict())
        else:
            self.littersFathered = list()
        if (lv := self.incubatedLitters) is not None:
            for le in lv:
                self.incubatedLitters.append(le.toDict())
        else:
            self.incubatedLitters = list()
        if (lv := self.implantedLitters) is not None:
            for le in lv:
                self.implantedLitters.append(le.toDict())
        else:
            self.implantedLitters = list()
        if (lv := self.pregnancyReactions) is not None:
            for le in lv:
                self.pregnancyReactions.add(str(le))
        else:
            self.pregnancyReactions = set()

#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawPregnancyPossibility']
## from [LT]/src/com/lilithsthrone/game/character/pregnancy/PregnancyPossibility.java: public Element saveAsXML(Element parentElement, Document doc) { @ gkv97K0XKyMXW7tb1770j24Nmsjcj0KzbYeekxRZSXa5qhsl0o1Ib1hX3pzboN0g1TXJfd4a2dwWJvr5/MpTWQ==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawPregnancyPossibility(Savable):
    TAG = 'pregnancyPossibility'
    _XMLID_ATTR_FATHERID_ATTRIBUTE: str = 'fatherId'
    _XMLID_ATTR_MOTHERID_ATTRIBUTE: str = 'motherId'
    _XMLID_ATTR_PROBABILITY_ATTRIBUTE: str = 'probability'

    def __init__(self) -> None:
        super().__init__()
        self.motherId: str = ''
        self.fatherId: str = ''
        self.probability: float = 0.

    def overrideAttrs(self, fatherId_attribute: _Optional_str = None, motherId_attribute: _Optional_str = None, probability_attribute: _Optional_str = None) -> None:
        if fatherId_attribute is not None:
            self._XMLID_ATTR_FATHERID_ATTRIBUTE = fatherId_attribute
        if motherId_attribute is not None:
            self._XMLID_ATTR_MOTHERID_ATTRIBUTE = motherId_attribute
        if probability_attribute is not None:
            self._XMLID_ATTR_PROBABILITY_ATTRIBUTE = probability_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_MOTHERID_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_MOTHERID_ATTRIBUTE, 'motherId')
        else:
            self.motherId = str(value)
        value = e.get(self._XMLID_ATTR_FATHERID_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_FATHERID_ATTRIBUTE, 'fatherId')
        else:
            self.fatherId = str(value)
        value = e.get(self._XMLID_ATTR_PROBABILITY_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_PROBABILITY_ATTRIBUTE, 'probability')
        else:
            self.probability = float(value)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_MOTHERID_ATTRIBUTE] = str(self.motherId)
        e.attrib[self._XMLID_ATTR_FATHERID_ATTRIBUTE] = str(self.fatherId)
        e.attrib[self._XMLID_ATTR_PROBABILITY_ATTRIBUTE] = str(float(self.probability))
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['motherId'] = self.motherId
        data['fatherId'] = self.fatherId
        data['probability'] = float(self.probability)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'motherId' not in data:
            raise KeyRequiredException(self, data, 'motherId', 'motherId')
        self.motherId = str(data['motherId'])
        if 'fatherId' not in data:
            raise KeyRequiredException(self, data, 'fatherId', 'fatherId')
        self.fatherId = str(data['fatherId'])
        if 'probability' not in data:
            raise KeyRequiredException(self, data, 'probability', 'probability')
        self.probability = float(data['probability'])

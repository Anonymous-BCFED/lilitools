from lilitools.saves.character.pregnancy._savables.pregnancy_possibility import RawPregnancyPossibility


class PregnancyPossibility(RawPregnancyPossibility):
    pass
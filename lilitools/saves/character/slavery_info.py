from collections import OrderedDict
from typing import Any, Dict, List, Optional, Set

from lxml import etree

from lilitools.consts import PRESERVE_SAVE_ORDERING

from ._savables.slavery_info import RawSlaveryInfo


class SlaveryInfo(RawSlaveryInfo):
    def __init__(self) -> None:
        super().__init__()
        self.slaveAssignedJobs: List[str] = ["IDLE"] * 24
        if PRESERVE_SAVE_ORDERING:
            self.slavePermissionSettings: OrderedDict[str, List[str]] = OrderedDict()
        else:
            self.slavePermissionSettings: OrderedDict[str, Set[str]] = OrderedDict()

    # public boolean isSlave() {
    # 	return !getOwnerId().isEmpty()
    # }
    def isSlave(self) -> bool:
        return self.owner is not None

    def _serializeDictSetToDict(
        self, data: Dict[str, Set[Any]]
    ) -> Dict[str, List[Any]]:
        return {k: list(sorted(v)) for k, v in data.items()}

    def _deserializeDictSetFromDict(
        self, data: Dict[str, List[Any]]
    ) -> Dict[str, Set[Any]]:
        return {k: set(v) for k, v in data.items()}

    if PRESERVE_SAVE_ORDERING:

        def _deserializeSetDict(
            self,
            e: etree._Element,
            parent_elem_name: str,
            child_elem_name: str,
            key_attr_name: str,
            value_elem_name: str,
            value_attr_name: Optional[str] = None,
            debug: bool = False,
        ) -> Dict[str, List[str]]:
            o: OrderedDict[str, List[str]] = {}
            entry: etree._Element
            value: etree._Element
            if (parentElem := e.find(parent_elem_name)) is not None:
                # print(parentElem.getchildren()[0].tag, child_elem_name)
                for entry in parentElem.findall(child_elem_name):
                    # print(entry.attrib)
                    k = entry.attrib[key_attr_name]
                    # print(k)
                    v: List[str] = list()
                    for value in entry.findall(value_elem_name):
                        v.append(
                            value.text
                            if value_attr_name is None
                            else value.attrib[value_attr_name]
                        )
                    o[k] = v
            return o

        def _serializeSetDict(
            self,
            e: etree._Element,
            data: OrderedDict[str, List[str]],
            parent_elem_name: str,
            child_elem: str,
            key_attr: str,
            value_elem: str,
            value_attr_name: Optional[str] = None,
        ) -> None:
            if data is None or len(data) == 0:
                return
            parentElem: etree._Element = etree.SubElement(e, parent_elem_name)
            childElem: etree._Element
            value: etree._Element
            for k, s in data.items():
                childElem = etree.SubElement(parentElem, child_elem, {key_attr: k})
                for item in s:
                    value = etree.SubElement(childElem, value_elem)
                    if value_attr_name is None:
                        value.text = item
                    else:
                        value.attrib[value_attr_name] = item

    else:

        def _deserializeSetDict(
            self,
            e: etree._Element,
            parent_elem_name: str,
            child_elem_name: str,
            key_attr_name: str,
            value_elem_name: str,
            value_attr_name: Optional[str] = None,
            debug: bool = False,
        ) -> Dict[str, Set[str]]:
            o: OrderedDict[str, Set[str]] = {}
            entry: etree._Element
            value: etree._Element
            if (parentElem := e.find(parent_elem_name)) is not None:
                # print(parentElem.getchildren()[0].tag, child_elem_name)
                for entry in parentElem.findall(child_elem_name):
                    # print(entry.attrib)
                    k = entry.attrib[key_attr_name]
                    # print(k)
                    v: Set[str] = set()
                    for value in entry.findall(value_elem_name):
                        v.add(
                            value.text
                            if value_attr_name is None
                            else value.attrib[value_attr_name]
                        )
                    o[k] = v
            return o

        def _serializeSetDict(
            self,
            e: etree._Element,
            data: Dict[str, Set[str]],
            parent_elem_name: str,
            child_elem: str,
            key_attr: str,
            value_elem: str,
            value_attr_name: Optional[str] = None,
        ) -> None:
            if data is None or len(data) == 0:
                return
            parentElem: etree._Element = etree.SubElement(e, parent_elem_name)
            childElem: etree._Element
            value: etree._Element
            for k, s in sorted(data.items(), key=lambda t: t[0]):
                childElem = etree.SubElement(parentElem, child_elem, {key_attr: k})
                for item in sorted(s):
                    value = etree.SubElement(childElem, value_elem)
                    if value_attr_name is None:
                        value.text = item
                    else:
                        value.attrib[value_attr_name] = item

    def _fromXML_slaveAssignedJobs(self, e: etree._Element) -> None:
        self.slaveAssignedJobs = ["IDLE"] * 24
        # if (elem := e.find('slaveAssignedJobs')) is not None:
        #     for k, v in elem.attrib.items():
        #         hour = int(k[1:])
        #         print(hour, v)
        #         self.slaveAssignedJobs[hour] = str(v)
        if (elem := e.find("slaveAssignedJobs")) is not None:
            for hr in range(24):
                self.slaveAssignedJobs[hr] = elem.attrib.get(f"h{hr}", "IDLE")

    def _toXML_slaveAssignedJobs(self, e: etree._Element) -> None:
        # All slaves are supposed to have this set.
        # Non-slaves do NOT have it set.
        if not self.isSlave():
            return
        # if len(list(filter(lambda j: j!='IDLE', self.slaveAssignedJobs))) == 0:
        #     return
        elem: etree._Element = etree.SubElement(e, "slaveAssignedJobs")
        # if len(self.slaveAssignedJobs) > 0:
        #     for k, v in self.slaveAssignedJobs.items():
        #         elem.attrib[f'h{k}'] = v
        for hr in range(24):
            job = self.slaveAssignedJobs[hr]
            if job != "IDLE":
                elem.attrib[f"h{hr}"] = job

    def _fromDict_slaveAssignedJobs(self, data: Dict[str, Any]) -> None:
        if (saj := data.get("slaveAssignedJobs")) is not None:
            self.slaveAssignedJobs = ["IDLE"] * 24
            for h, j in saj.items():
                if h != "IDLE":
                    self.slaveAssignedJobs[h] = j

    def _toDict_slaveAssignedJobs(self, data: Dict[str, Any]) -> None:
        # All slaves are supposed to have this set.
        # Non-slaves do NOT have it set.
        if not self.isSlave():
            return
        saj = {}
        for hr in range(24):
            job = self.slaveAssignedJobs[hr]
            if job != "IDLE":
                saj[hr] = job
        data["slaveAssignedJobs"] = saj

    def _fromXML_slaveJobSettings(self, e: etree._Element) -> None:
        self.slaveJobSettings = self._deserializeSetDict(
            e, "slaveJobSettings", "jobSetting", "job", "setting"
        )

    def _toXML_slaveJobSettings(self, e: etree._Element) -> None:
        # All slaves are supposed to have this set.
        # Non-slaves do NOT have it set.
        if not self.isSlave():
            return
        self._serializeSetDict(
            e, self.slaveJobSettings, "slaveJobSettings", "jobSetting", "job", "setting"
        )

    def _toDict_slaveJobSettings(self, data: Dict[str, Any]) -> None:
        if self.isSlave():
            data["slaveJobSettings"] = self._serializeDictSetToDict(self.slaveJobSettings)

    def _fromDict_slaveJobSettings(self, data: Dict[str, Any]) -> None:
        if 'slaveJobSettings' in data:
            self.slaveJobSettings = self._deserializeDictSetFromDict(data["slaveJobSettings"])

    def _fromXML_slavePermissionSettings(self, e: etree._Element) -> None:
        self.slavePermissionSettings = self._deserializeSetDict(
            e, "slavePermissionSettings", "permission", "type", "setting", "value"
        )

    def _toXML_slavePermissionSettings(self, e: etree._Element) -> None:
        # All slaves are supposed to have this set.
        # Non-slaves do NOT have it set.
        if not self.isSlave():
            return
        self._serializeSetDict(
            e,
            self.slavePermissionSettings,
            "slavePermissionSettings",
            "permission",
            "type",
            "setting",
            "value",
        )

    def _toDict_slavePermissionSettings(self, data: Dict[str, Any]) -> None:
        if self.isSlave():
            data["slavePermissionSettings"] = self._serializeDictSetToDict(
                self.slavePermissionSettings
            )

    def _fromDict_slavePermissionSettings(self, data: Dict[str, Any]) -> None:
        if "slavePermissionSettings" in data:
            self.slavePermissionSettings = self._deserializeDictSetFromDict(
                data["slavePermissionSettings"]
            )

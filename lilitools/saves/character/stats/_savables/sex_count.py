#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional, Set
from lxml import etree
from lilitools.saves.character.stats.sex_type import SexType
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawSexCount']
## from [LT]/src/com/lilithsthrone/game/character/SexCount.java: public Element saveAsXML(Element parentElement, Document doc) { @ 6c4f8vkQUVkOjhquJNjGaBPigxVrP13LM83/fCMrky/VuOMZSk04vtJVQgjkVfP+/jidxo4PRhvpJl+M4/iCwQ==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawSexCount(Savable):
    TAG = 'sexCount'
    _XMLID_ATTR_ASDOM_ATTRIBUTE: str = 'asDom'
    _XMLID_ATTR_ASSUB_ATTRIBUTE: str = 'asSub'
    _XMLID_ATTR_CONSENSUAL_ATTRIBUTE: str = 'consensual'
    _XMLID_ATTR_ID_ATTRIBUTE: str = 'id'
    _XMLID_TAG_SEXTYPES_CHILD: str = 'sexType'
    _XMLID_TAG_SEXTYPES_VALUEELEM: Optional[str] = None

    def __init__(self) -> None:
        super().__init__()
        self.consensual: int = 0
        self.asSub: int = 0
        self.asDom: int = 0
        self.id: str = ''
        self.sexTypes: Set[SexType] = set()

    def overrideAttrs(self, asDom_attribute: _Optional_str = None, asSub_attribute: _Optional_str = None, consensual_attribute: _Optional_str = None, id_attribute: _Optional_str = None) -> None:
        if asDom_attribute is not None:
            self._XMLID_ATTR_ASDOM_ATTRIBUTE = asDom_attribute
        if asSub_attribute is not None:
            self._XMLID_ATTR_ASSUB_ATTRIBUTE = asSub_attribute
        if consensual_attribute is not None:
            self._XMLID_ATTR_CONSENSUAL_ATTRIBUTE = consensual_attribute
        if id_attribute is not None:
            self._XMLID_ATTR_ID_ATTRIBUTE = id_attribute

    def overrideTags(self, sexTypes_child: _Optional_str = None, sexTypes_valueelem: _Optional_str = None) -> None:
        if sexTypes_child is not None:
            self._XMLID_TAG_SEXTYPES_CHILD = sexTypes_child
        if sexTypes_valueelem is not None:
            self._XMLID_TAG_SEXTYPES_VALUEELEM = sexTypes_valueelem

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        lv: Savable
        value = e.get(self._XMLID_ATTR_CONSENSUAL_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_CONSENSUAL_ATTRIBUTE, 'consensual')
        else:
            self.consensual = int(value)
        value = e.get(self._XMLID_ATTR_ASSUB_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_ASSUB_ATTRIBUTE, 'asSub')
        else:
            self.asSub = int(value)
        value = e.get(self._XMLID_ATTR_ASDOM_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_ASDOM_ATTRIBUTE, 'asDom')
        else:
            self.asDom = int(value)
        value = e.get(self._XMLID_ATTR_ID_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_ID_ATTRIBUTE, 'id')
        else:
            self.id = str(value)
        for lc in e.iterfind(self._XMLID_TAG_SEXTYPES_CHILD):
            lv = SexType()
            lv.fromXML(lc)
            self.sexTypes.add(lv)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        e.attrib[self._XMLID_ATTR_CONSENSUAL_ATTRIBUTE] = str(self.consensual)
        e.attrib[self._XMLID_ATTR_ASSUB_ATTRIBUTE] = str(self.asSub)
        e.attrib[self._XMLID_ATTR_ASDOM_ATTRIBUTE] = str(self.asDom)
        e.attrib[self._XMLID_ATTR_ID_ATTRIBUTE] = str(self.id)
        for lv in sorted(self.sexTypes, key=lambda x: (x.participant, x.self, x.other)):
            e.append(lv.toXML())
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['consensual'] = int(self.consensual)
        data['asSub'] = int(self.asSub)
        data['asDom'] = int(self.asDom)
        data['id'] = self.id
        if self.sexTypes is None or len(self.sexTypes) > 0:
            data['sexTypes'] = list()
        else:
            lc = list()
            if len(self.sexTypes) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['sexTypes'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'consensual' not in data:
            raise KeyRequiredException(self, data, 'consensual', 'consensual')
        self.consensual = int(data['consensual'])
        if 'asSub' not in data:
            raise KeyRequiredException(self, data, 'asSub', 'asSub')
        self.asSub = int(data['asSub'])
        if 'asDom' not in data:
            raise KeyRequiredException(self, data, 'asDom', 'asDom')
        self.asDom = int(data['asDom'])
        if 'id' not in data:
            raise KeyRequiredException(self, data, 'id', 'id')
        self.id = str(data['id'])
        if (lv := self.sexTypes) is not None:
            for le in lv:
                self.sexTypes.add(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'sexTypes', 'sexTypes')

#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from collections import OrderedDict
from typing import Any, Dict, List, Optional, Set
from lxml import etree
from lilitools.saves.character.fluids.stored_fluid import StoredFluid
from lilitools.saves.character.stats.sex_count import SexCount
from lilitools.saves.character.stats.sex_type import SexType
from lilitools.saves.character.stats.virginity_loss_entry import VirginityLossEntry
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawSexStats']
## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public Element saveAsXML(Element parentElement, Document doc) { @ Ra1rfw7eKd5WT2JXoGa/zLFvb/XgZlpPNZbY2BZwgdTA/t0DkASy5ADAtBbX2DvSTxI43cQAGTPbPsMVcAHyRQ==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawSexStats(Savable):
    TAG = 'sexStats'
    _XMLID_ATTR_DAYSORGASMCOUNTRECORD_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_DAYSORGASMCOUNT_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_TOTALORGASMCOUNT_ATTRIBUTE: str = 'value'
    _XMLID_TAG_DAYSORGASMCOUNTRECORD_ELEMENT: str = 'daysOrgasmCountRecord'
    _XMLID_TAG_DAYSORGASMCOUNT_ELEMENT: str = 'daysOrgasmCount'
    _XMLID_TAG_TOTALORGASMCOUNT_ELEMENT: str = 'totalOrgasmCount'
    _XMLID_TAG_VIRGINITYLOSSES_CHILD: str = 'sexType'
    _XMLID_TAG_VIRGINITYLOSSES_PARENT: str = 'virginityLosses'
    _XMLID_TAG_VIRGINITYLOSSES_VALUEELEM: Optional[str] = None

    def __init__(self) -> None:
        super().__init__()
        self.fluidsStoredMap: OrderedDict[str, List[StoredFluid]] = OrderedDict()
        self.daysOrgasmCount: int = 0  # Element
        self.daysOrgasmCountRecord: int = 0  # Element
        self.totalOrgasmCount: int = 0  # Element
        self.sexPartners: OrderedDict[str, SexCount] = OrderedDict()
        self.virginityLosses: Set[VirginityLossEntry] = set()
        self.virginityLossesBackup: Optional[OrderedDict[SexType, str]] = OrderedDict()

    def overrideAttrs(self, daysOrgasmCount_attribute: _Optional_str = None, daysOrgasmCountRecord_attribute: _Optional_str = None, totalOrgasmCount_attribute: _Optional_str = None) -> None:
        if daysOrgasmCount_attribute is not None:
            self._XMLID_ATTR_DAYSORGASMCOUNT_ATTRIBUTE = daysOrgasmCount_attribute
        if daysOrgasmCountRecord_attribute is not None:
            self._XMLID_ATTR_DAYSORGASMCOUNTRECORD_ATTRIBUTE = daysOrgasmCountRecord_attribute
        if totalOrgasmCount_attribute is not None:
            self._XMLID_ATTR_TOTALORGASMCOUNT_ATTRIBUTE = totalOrgasmCount_attribute

    def overrideTags(self, daysOrgasmCount_element: _Optional_str = None, daysOrgasmCountRecord_element: _Optional_str = None, totalOrgasmCount_element: _Optional_str = None, virginityLosses_child: _Optional_str = None, virginityLosses_parent: _Optional_str = None, virginityLosses_valueelem: _Optional_str = None) -> None:
        if daysOrgasmCount_element is not None:
            self._XMLID_TAG_DAYSORGASMCOUNT_ELEMENT = daysOrgasmCount_element
        if daysOrgasmCountRecord_element is not None:
            self._XMLID_TAG_DAYSORGASMCOUNTRECORD_ELEMENT = daysOrgasmCountRecord_element
        if totalOrgasmCount_element is not None:
            self._XMLID_TAG_TOTALORGASMCOUNT_ELEMENT = totalOrgasmCount_element
        if virginityLosses_child is not None:
            self._XMLID_TAG_VIRGINITYLOSSES_CHILD = virginityLosses_child
        if virginityLosses_parent is not None:
            self._XMLID_TAG_VIRGINITYLOSSES_PARENT = virginityLosses_parent
        if virginityLosses_valueelem is not None:
            self._XMLID_TAG_VIRGINITYLOSSES_VALUEELEM = virginityLosses_valueelem

    def _fromXML_fluidsStoredMap(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromXML_fluidsStoredMap()')

    def _toXML_fluidsStoredMap(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toXML_fluidsStoredMap()')

    def _fromDict_fluidsStoredMap(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromDict_fluidsStoredMap()')

    def _toDict_fluidsStoredMap(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toDict_fluidsStoredMap()')

    def _fromXML_sexPartners(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromXML_sexPartners()')

    def _toXML_sexPartners(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toXML_sexPartners()')

    def _fromDict_sexPartners(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromDict_sexPartners()')

    def _toDict_sexPartners(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toDict_sexPartners()')

    def _fromXML_virginityLossesBackup(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromXML_virginityLossesBackup()')

    def _toXML_virginityLossesBackup(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toXML_virginityLossesBackup()')

    def _fromDict_virginityLossesBackup(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromDict_virginityLossesBackup()')

    def _toDict_virginityLossesBackup(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toDict_virginityLossesBackup()')

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        lv: Savable
        sf: etree._Element
        self._fromXML_fluidsStoredMap(e)
        if (sf := e.find(self._XMLID_TAG_DAYSORGASMCOUNT_ELEMENT)) is not None:
            if self._XMLID_ATTR_DAYSORGASMCOUNT_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_DAYSORGASMCOUNT_ATTRIBUTE, 'daysOrgasmCount')
            self.daysOrgasmCount = int(sf.attrib[self._XMLID_ATTR_DAYSORGASMCOUNT_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'daysOrgasmCount', self._XMLID_TAG_DAYSORGASMCOUNT_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_DAYSORGASMCOUNTRECORD_ELEMENT)) is not None:
            if self._XMLID_ATTR_DAYSORGASMCOUNTRECORD_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_DAYSORGASMCOUNTRECORD_ATTRIBUTE, 'daysOrgasmCountRecord')
            self.daysOrgasmCountRecord = int(sf.attrib[self._XMLID_ATTR_DAYSORGASMCOUNTRECORD_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'daysOrgasmCountRecord', self._XMLID_TAG_DAYSORGASMCOUNTRECORD_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_TOTALORGASMCOUNT_ELEMENT)) is not None:
            if self._XMLID_ATTR_TOTALORGASMCOUNT_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_TOTALORGASMCOUNT_ATTRIBUTE, 'totalOrgasmCount')
            self.totalOrgasmCount = int(sf.attrib[self._XMLID_ATTR_TOTALORGASMCOUNT_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'totalOrgasmCount', self._XMLID_TAG_TOTALORGASMCOUNT_ELEMENT)
        self._fromXML_sexPartners(e)
        if (lf := e.find(self._XMLID_TAG_VIRGINITYLOSSES_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_VIRGINITYLOSSES_CHILD):
                    lv = VirginityLossEntry()
                    lv.fromXML(lc)
                    self.virginityLosses.add(lv)
        else:
            self.virginityLosses = set()
        self._fromXML_virginityLossesBackup(e)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        self._toXML_fluidsStoredMap(e)
        etree.SubElement(e, self._XMLID_TAG_DAYSORGASMCOUNT_ELEMENT, {self._XMLID_ATTR_DAYSORGASMCOUNT_ATTRIBUTE: str(self.daysOrgasmCount)})
        etree.SubElement(e, self._XMLID_TAG_DAYSORGASMCOUNTRECORD_ELEMENT, {self._XMLID_ATTR_DAYSORGASMCOUNTRECORD_ATTRIBUTE: str(self.daysOrgasmCountRecord)})
        etree.SubElement(e, self._XMLID_TAG_TOTALORGASMCOUNT_ELEMENT, {self._XMLID_ATTR_TOTALORGASMCOUNT_ATTRIBUTE: str(self.totalOrgasmCount)})
        self._toXML_sexPartners(e)
        lp = etree.SubElement(e, self._XMLID_TAG_VIRGINITYLOSSES_PARENT)
        for lv in sorted(self.virginityLosses, key=lambda x: (x.other, x.participant, x.self, x.takenBy)):
            lp.append(lv.toXML())
        self._toXML_virginityLossesBackup(e)
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        self._toDict_fluidsStoredMap(data)
        data['daysOrgasmCount'] = int(self.daysOrgasmCount)
        data['daysOrgasmCountRecord'] = int(self.daysOrgasmCountRecord)
        data['totalOrgasmCount'] = int(self.totalOrgasmCount)
        self._toDict_sexPartners(data)
        if self.virginityLosses is None or len(self.virginityLosses) > 0:
            data['virginityLosses'] = list()
        else:
            lc = list()
            if len(self.virginityLosses) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['virginityLosses'] = lc
        self._toDict_virginityLossesBackup(data)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        self._fromDict_fluidsStoredMap(data)
        if (sf := data.get('daysOrgasmCount')) is not None:
            self.daysOrgasmCount = data['daysOrgasmCount']
        else:
            raise KeyRequiredException(self, data, 'daysOrgasmCount', 'daysOrgasmCount')
        if (sf := data.get('daysOrgasmCountRecord')) is not None:
            self.daysOrgasmCountRecord = data['daysOrgasmCountRecord']
        else:
            raise KeyRequiredException(self, data, 'daysOrgasmCountRecord', 'daysOrgasmCountRecord')
        if (sf := data.get('totalOrgasmCount')) is not None:
            self.totalOrgasmCount = data['totalOrgasmCount']
        else:
            raise KeyRequiredException(self, data, 'totalOrgasmCount', 'totalOrgasmCount')
        self._fromDict_sexPartners(data)
        if (lv := self.virginityLosses) is not None:
            for le in lv:
                self.virginityLosses.add(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'virginityLosses', 'virginityLosses')
        self._fromDict_virginityLossesBackup(data)

#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawSexType']
## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public Element saveAsXML(Element parentElement, Document doc) { @ Ra1rfw7eKd5WT2JXoGa/zLFvb/XgZlpPNZbY2BZwgdTA/t0DkASy5ADAtBbX2DvSTxI43cQAGTPbPsMVcAHyRQ==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawSexType(Savable):
    TAG = 'sexType'
    _XMLID_ATTR_CUM_ATTRIBUTE: str = 'cum'
    _XMLID_ATTR_OTHER_ATTRIBUTE: str = 'other'
    _XMLID_ATTR_PARTICIPANT_ATTRIBUTE: str = 'participant'
    _XMLID_ATTR_SELF_ATTRIBUTE: str = 'self'
    _XMLID_ATTR_SEX_ATTRIBUTE: str = 'sex'

    def __init__(self) -> None:
        super().__init__()
        self.participant: str = ''
        self.self: str = ''
        self.other: str = ''
        self.sex: Optional[str] = None
        self.cum: Optional[str] = None

    def overrideAttrs(self, cum_attribute: _Optional_str = None, other_attribute: _Optional_str = None, participant_attribute: _Optional_str = None, self_attribute: _Optional_str = None, sex_attribute: _Optional_str = None) -> None:
        if cum_attribute is not None:
            self._XMLID_ATTR_CUM_ATTRIBUTE = cum_attribute
        if other_attribute is not None:
            self._XMLID_ATTR_OTHER_ATTRIBUTE = other_attribute
        if participant_attribute is not None:
            self._XMLID_ATTR_PARTICIPANT_ATTRIBUTE = participant_attribute
        if self_attribute is not None:
            self._XMLID_ATTR_SELF_ATTRIBUTE = self_attribute
        if sex_attribute is not None:
            self._XMLID_ATTR_SEX_ATTRIBUTE = sex_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_PARTICIPANT_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_PARTICIPANT_ATTRIBUTE, 'participant')
        else:
            self.participant = str(value)
        value = e.get(self._XMLID_ATTR_SELF_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_SELF_ATTRIBUTE, 'self')
        else:
            self.self = str(value)
        value = e.get(self._XMLID_ATTR_OTHER_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_OTHER_ATTRIBUTE, 'other')
        else:
            self.other = str(value)
        value = e.get(self._XMLID_ATTR_SEX_ATTRIBUTE, None)
        if value is None:
            self.sex = None
        else:
            self.sex = str(value)
        value = e.get(self._XMLID_ATTR_CUM_ATTRIBUTE, None)
        if value is None:
            self.cum = None
        else:
            self.cum = str(value)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_PARTICIPANT_ATTRIBUTE] = str(self.participant)
        e.attrib[self._XMLID_ATTR_SELF_ATTRIBUTE] = str(self.self)
        e.attrib[self._XMLID_ATTR_OTHER_ATTRIBUTE] = str(self.other)
        if self.sex is not None:
            e.attrib[self._XMLID_ATTR_SEX_ATTRIBUTE] = str(self.sex)
        if self.cum is not None:
            e.attrib[self._XMLID_ATTR_CUM_ATTRIBUTE] = str(self.cum)
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['participant'] = self.participant
        data['self'] = self.self
        data['other'] = self.other
        if self.sex is not None:
            data['sex'] = self.sex
        if self.cum is not None:
            data['cum'] = self.cum
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'participant' not in data:
            raise KeyRequiredException(self, data, 'participant', 'participant')
        self.participant = str(data['participant'])
        if 'self' not in data:
            raise KeyRequiredException(self, data, 'self', 'self')
        self.self = str(data['self'])
        if 'other' not in data:
            raise KeyRequiredException(self, data, 'other', 'other')
        self.other = str(data['other'])
        self.sex = str(data['sex']) if 'sex' in data else None
        self.cum = str(data['cum']) if 'cum' in data else None

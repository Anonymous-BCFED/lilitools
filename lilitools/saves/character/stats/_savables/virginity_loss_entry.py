#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawVirginityLossEntry']
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawVirginityLossEntry(Savable):
    TAG = 'sexType'
    _XMLID_ATTR_OTHER_ATTRIBUTE: str = 'other'
    _XMLID_ATTR_PARTICIPANT_ATTRIBUTE: str = 'participant'
    _XMLID_ATTR_SELF_ATTRIBUTE: str = 'self'
    _XMLID_ATTR_TAKENBY_ATTRIBUTE: str = 'takenBy'
    _XMLID_ATTR_TAKENDESCRIPTION_ATTRIBUTE: str = 'takenDescription'

    def __init__(self) -> None:
        super().__init__()
        self.other: str = ''
        self.participant: str = ''
        self.self: str = ''
        self.takenBy: str = ''
        self.takenDescription: str = ''

    def overrideAttrs(self, other_attribute: _Optional_str = None, participant_attribute: _Optional_str = None, self_attribute: _Optional_str = None, takenBy_attribute: _Optional_str = None, takenDescription_attribute: _Optional_str = None) -> None:
        if other_attribute is not None:
            self._XMLID_ATTR_OTHER_ATTRIBUTE = other_attribute
        if participant_attribute is not None:
            self._XMLID_ATTR_PARTICIPANT_ATTRIBUTE = participant_attribute
        if self_attribute is not None:
            self._XMLID_ATTR_SELF_ATTRIBUTE = self_attribute
        if takenBy_attribute is not None:
            self._XMLID_ATTR_TAKENBY_ATTRIBUTE = takenBy_attribute
        if takenDescription_attribute is not None:
            self._XMLID_ATTR_TAKENDESCRIPTION_ATTRIBUTE = takenDescription_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_OTHER_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_OTHER_ATTRIBUTE, 'other')
        else:
            self.other = str(value)
        value = e.get(self._XMLID_ATTR_PARTICIPANT_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_PARTICIPANT_ATTRIBUTE, 'participant')
        else:
            self.participant = str(value)
        value = e.get(self._XMLID_ATTR_SELF_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_SELF_ATTRIBUTE, 'self')
        else:
            self.self = str(value)
        value = e.get(self._XMLID_ATTR_TAKENBY_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_TAKENBY_ATTRIBUTE, 'takenBy')
        else:
            self.takenBy = str(value)
        value = e.get(self._XMLID_ATTR_TAKENDESCRIPTION_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_TAKENDESCRIPTION_ATTRIBUTE, 'takenDescription')
        else:
            self.takenDescription = str(value)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_OTHER_ATTRIBUTE] = str(self.other)
        e.attrib[self._XMLID_ATTR_PARTICIPANT_ATTRIBUTE] = str(self.participant)
        e.attrib[self._XMLID_ATTR_SELF_ATTRIBUTE] = str(self.self)
        e.attrib[self._XMLID_ATTR_TAKENBY_ATTRIBUTE] = str(self.takenBy)
        e.attrib[self._XMLID_ATTR_TAKENDESCRIPTION_ATTRIBUTE] = str(self.takenDescription)
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['other'] = self.other
        data['participant'] = self.participant
        data['self'] = self.self
        data['takenBy'] = self.takenBy
        data['takenDescription'] = self.takenDescription
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'other' not in data:
            raise KeyRequiredException(self, data, 'other', 'other')
        self.other = str(data['other'])
        if 'participant' not in data:
            raise KeyRequiredException(self, data, 'participant', 'participant')
        self.participant = str(data['participant'])
        if 'self' not in data:
            raise KeyRequiredException(self, data, 'self', 'self')
        self.self = str(data['self'])
        if 'takenBy' not in data:
            raise KeyRequiredException(self, data, 'takenBy', 'takenBy')
        self.takenBy = str(data['takenBy'])
        if 'takenDescription' not in data:
            raise KeyRequiredException(self, data, 'takenDescription', 'takenDescription')
        self.takenDescription = str(data['takenDescription'])

from typing import Any, Dict
from lxml import etree

from lilitools.saves.character.stats._savables.sex_stats import RawSexStats
from lilitools.saves.character.stats.sex_count import SexCount
from lilitools.saves.character.stats.sex_type import SexType
from lilitools.saves.character.fluids.stored_fluid import StoredFluid


class SexStats(RawSexStats):
    def _fromXML_fluidsStoredMap(self, e: etree._Element) -> None:
        self.fluidsStoredMap = {}
        for entry in e.find("fluidsStoredMap").findall("entry"):
            orifice = entry.attrib["orifice"]
            contents = []
            for fluidElem in entry.findall("fluidStored"):
                f = StoredFluid()
                f.fromXML(fluidElem)
                contents.append(f)
            self.fluidsStoredMap[orifice] = contents

    def _toXML_fluidsStoredMap(self, e: etree._Element) -> None:
        fsmElem = etree.SubElement(e, "fluidsStoredMap")
        for orifice, contents in self.fluidsStoredMap.items():
            orificeElem = etree.SubElement(fsmElem, "entry", {"orifice": orifice})
            for fluid in contents:
                orificeElem.append(fluid.toXML())

    def _fromDict_fluidsStoredMap(self, data: Dict[str, Any]) -> None:
        self.fluidsStoredMap = {}
        for orifice, entry in data["fluidsStoredMap"].items():
            fluidsInOrifice = []
            for fluid in entry:
                f = StoredFluid()
                f.fromDict(fluid)
                fluidsInOrifice.append(f)
            self.fluidsStoredMap[orifice] = fluidsInOrifice

    def _toDict_fluidsStoredMap(self, data: Dict[str, Any]) -> None:
        o = {}
        for orifice, contents in self.fluidsStoredMap.items():
            c = []
            for fluid in contents:
                c.append(fluid.toDict())
            o[orifice] = c
        data["fluidsStoredMap"] = o

    def _fromXML_sexPartners(self, e: etree._Element) -> None:
        self.sexPartners = {}
        if (sexPartnersElem := e.find("sexPartners")) is not None:
            for partnerXML in sexPartnersElem.findall("sexCount"):
                partner = SexCount()
                partner.fromXML(partnerXML)
                self.sexPartners[partnerXML.attrib["id"]] = partner

    def _toXML_sexPartners(self, e: etree._Element) -> None:
        partners = etree.SubElement(e, "sexPartners")
        for partnerID, stats in sorted(self.sexPartners.items(), key=lambda x: x[0]):
            partner = stats.toXML("sexCount")
            partner.attrib["id"] = partnerID
            partners.append(partner)

    def _fromDict_sexPartners(self, data: Dict[str, Any]) -> None:
        self.sexPartners = {}
        if (sexPartners := data.get("sexPartners")) is not None:
            for id, partner in sexPartners.items():
                p = SexCount()
                p.fromDict(partner)
                self.sexPartners[id] = p

    def _toDict_sexPartners(self, data: Dict[str, Any]) -> None:
        data["sexPartners"] = {k: v.toDict() for k, v in self.sexPartners.items()}

    # def _fromXML_virginityLosses(self, e: etree._Element) -> None:
    #     # <virginityLosses>
    #     #   <sexType other="PENIS" participant="NORMAL" self="ANUS" takenBy="41,Cultist" takenDescription="in her chapel"/>
    #     #   <sexType other="PENIS" participant="NORMAL" self="MOUTH" takenBy="42,Cultist" takenDescription="in her chapel"/>
    #     #   <sexType other="PENIS" participant="NORMAL" self="VAGINA" takenBy="53,ImpAttacker" takenDescription="in Submission"/>
    #     #   <sexType other="VAGINA" participant="NORMAL" self="PENIS" takenBy="55,ImpAttacker" takenDescription="in Submission"/>
    #     # </virginityLosses>
    #     self.virginityLosses = {}
    #     if (vlE := e.find('virginityLosses')) is not None:
    #         for entryElem in vlE.findall('sexType'):
    #             takenBy: str = entryElem.attrib['takenBy']
    #             takenDescription: str = entryElem.attrib['takenDescription']
    #             sexType = VirginityLossEntry()
    #             sexType.fromXML(entryElem)
    #             self.virginityLosses[sexType] = (takenBy, takenDescription)

    # def _toXML_virginityLosses(self, e: etree._Element) -> None:
    #     vlElem = etree.SubElement(e, 'virginityLosses')
    #     if len(self.virginityLosses) > 0:
    #         for sexType, (takenBy, takenDesc) in self.virginityLosses.items():
    #             sexTypeElem = sexType.toXML()
    #             sexTypeElem.attrib['takenBy'] = takenBy
    #             sexTypeElem.attrib['takenDescription'] = takenDesc
    #             vlElem.append(sexTypeElem)

    def _fromXML_virginityLossesBackup(self, e: etree._Element) -> None:
        self.virginityLossesBackup = {}
        if (vlbE := e.find("virginityLossesBackup")) is not None:
            for entryElem in vlbE.findall("sexType"):
                takenDescription: str = entryElem.attrib["takenDescription"]
                sexType = SexType()
                sexType.fromXML(entryElem)
                self.virginityLossesBackup[sexType] = takenDescription

    def _toXML_virginityLossesBackup(self, e: etree._Element) -> None:
        vlElem = etree.SubElement(e, "virginityLossesBackup")
        for sexType, takenDesc in self.virginityLossesBackup.items():
            sexTypeElem = sexType.toXML()
            sexTypeElem.attrib["takenDescription"] = takenDesc
            vlElem.append(sexTypeElem)

    def _fromDict_virginityLossesBackup(self, data: Dict[str, Any]) -> None:
        self.virginityLossesBackup = {}
        if (vlb := data.get("virginityLossesBackup")) is not None:
            for desc, data in vlb.items():
                sexType = SexType()
                sexType.fromDict(data)
                self.virginityLossesBackup[sexType] = desc

    def _toDict_virginityLossesBackup(self, data: Dict[str, Any]) -> None:
        data["virginityLossesBackup"] = {
            desc: sexType.toDict()
            for sexType, desc in self.virginityLossesBackup.items()
        }

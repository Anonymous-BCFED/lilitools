from pathlib import Path
from typing import Any

from lilitools.game.enums.known_status_effect import EKnownStatusEffect
from lilitools.saves.modfiles.character.status_effects.status_effect_type import StatusEffectType
from lilitools.saves.modfiles.utils import MFELookupTable


class _StatusEffectsTable(MFELookupTable[StatusEffectType]):
    def handleBuiltinsEnumEntry(self, e: Any) -> None:
        return super().handleBuiltinsEnumEntry(e)


STATUS_EFFECT_TYPES = _StatusEffectsTable(EKnownStatusEffect, Path('res/statusEffect'), StatusEffectType)

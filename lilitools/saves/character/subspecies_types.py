from pathlib import Path

from lilitools.saves.character.body.enums.known_subspecies import EKnownSubspecies
from lilitools.saves.modfiles.character.subspecies.subspecies_type import SubSpeciesType
from lilitools.saves.modfiles.utils import MFELookupTable


class _SubspeciesTable(MFELookupTable[SubSpeciesType]):
    MOD_SUB_DIR = 'subspecies'
    FILTER_MOD_FOLDER_NAME='subspecies'
    FILTER_ROOT_ELEMENT='subspecies'
    def handleBuiltinsEnumEntry(self, e: EKnownSubspecies) -> None:
        ss = SubSpeciesType()
        ss.id = e.id
        ss.baseSlaveValue=e.baseSlaveValue
        self.entByID[ss.id.casefold()] = ss
        # print(ss.id,ss.baseSlaveValue)

# MFELookupTable.DEBUG = True
SUBSPECIES_TYPES = _SubspeciesTable(
    EKnownSubspecies, Path("res/race"), SubSpeciesType
)
MFELookupTable.DEBUG = False

from lilitools.saves.character._savables.exported_character import RawExportedCharacter
from lilitools.saves.character.body.body import Body
from lilitools.saves.character.game_character import GameCharacter


class TransformationPreset(Body):
    TAG = "body"

    def __init__(self) -> None:
        super().__init__()

    @classmethod
    def FromCharacter(cls, char: GameCharacter) -> "TransformationPreset":
        tfp = cls()
        tfp.fromXML(char.body.toXML())
        return tfp

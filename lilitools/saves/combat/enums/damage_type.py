from enum import IntEnum
__all__ = ['EDamageType']


class EDamageType(IntEnum):
    HEALTH = 0
    PHYSICAL = 1
    ICE = 2
    FIRE = 3
    POISON = 4
    UNARMED = 5
    LUST = 6
    MISC = 7

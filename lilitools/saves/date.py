import datetime
from typing import Optional

from lxml import etree

from lilitools.saves.savable import Savable


class Date(Savable):
    def __init__(self, year: int = 1970, day: int = 1, month: int = 1, hour: int = 0, minute: int = 0) -> None:
        super().__init__()
        self.datetime: datetime.datetime = datetime.datetime(year,month,day,hour,minute)

    @property
    def dayOfMonth(self) -> int:
        return self.datetime.day
    @dayOfMonth.setter
    def dayOfMonth(self, value: int) -> None:
        self.datetime.day = value

    @property
    def hour(self) -> int:
        return self.datetime.hour
    @hour.setter
    def hour(self, value: int) -> None:
        self.datetime.hour = value

    @property
    def minute(self) -> int:
        return self.datetime.minute
    @minute.setter
    def minute(self, value: int) -> None:
        self.datetime.minute = value

    @property
    def month(self) -> int:
        return self.datetime.month
    @month.setter
    def month(self, value: int) -> None:
        self.datetime.month = value

    @property
    def year(self) -> int:
        return self.datetime.year
    @year.setter
    def year(self, value: int) -> None:
        self.datetime.year = value

    def toXML(self, tagOverride: Optional[str] = None) -> etree._Element:
        return etree.Element(tagOverride or self.TAG, {
            'dayOfMonth': str(self.dayOfMonth),
            'hour': str(self.hour),
            'minute': str(self.minute),
            'month': str(self.month),
            'year': str(self.year),
        })

    def fromXML(self, e: etree._Element) -> None:
        self.datetime = datetime.datetime(
            int(e.attrib['year']), 
            int(e.attrib['month']), 
            int(e.attrib['dayOfMonth']), 
            int(e.attrib['hour']), 
            int(e.attrib['minute']))

Date.EPOCH = Date(1970, 1, 1, 0, 0)

from typing import Dict, Tuple

from lilitools.saves.enums.tf_modifier import ETFModifier
from lilitools.saves.enums.tf_potency import ETFPotency

from ._savables.enchantment_recipe import RawEnchantmentRecipe


class EnchantmentRecipe(RawEnchantmentRecipe):
    def sort(self, reverse: bool = False) -> None:
        if self.itemEffects is not None:
            self.itemEffects.sort(key=lambda e: (e.mod1.name, e.mod2.name, e.potency.name), reverse=reverse)

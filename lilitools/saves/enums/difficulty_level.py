from enum import Enum
__all__ = ['EDifficultyLevel']


class EDifficultyLevel(Enum):
    NORMAL = 'Human', 'The way the game is meant to be played. No level scaling and no damage modifications.', False, 1, 1
    LEVEL_SCALING = 'Morph', 'Enemies level up alongside your character, but do normal damage.', True, 1, 1
    HARD = 'Demon', 'Enemies level up alongside your character and do 200% damage.', True, 2, 1
    NIGHTMARE = 'Lilin', 'Enemies level up alongside your character, do 200% damage, and take only 50% damage from all sources.', True, 2, 0
    HELL = 'Lilith', "Enemies are always 2x your character's level, do 400% damage, and take only 25% damage from all sources. Be prepared to lose. A lot.", True, 4, 0

    def getName(self) ->str:
        return self.name_

    def getDescription(self) ->str:
        return self.description

    def getIsNPCLevelScaling(self) ->bool:
        return self.isNPCLevelScaling

    def getDamageModifierNPC(self) ->int:
        return self.damageModifierNPC

    def getDamageModifierPlayer(self) ->int:
        return self.damageModifierPlayer

    def __init__(self, name: str, description: str, isNPCLevelScaling: bool, damageModifierNPC: int, damageModifierPlayer: int) ->None:
        self.name_: str = name
        self.description: str = description
        self.isNPCLevelScaling: bool = isNPCLevelScaling
        self.damageModifierNPC: int = damageModifierNPC
        self.damageModifierPlayer: int = damageModifierPlayer

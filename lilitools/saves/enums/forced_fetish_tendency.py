from enum import IntEnum
__all__ = ['EForcedFetishTendency']


class EForcedFetishTendency(IntEnum):
    BOTTOM_HEAVY = 0
    BOTTOM = 1
    NEUTRAL = 2
    TOP = 3
    TOP_HEAVY = 4

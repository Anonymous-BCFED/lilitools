from enum import IntEnum
__all__ = ['EForcedTFTendency']


class EForcedTFTendency(IntEnum):
    FEMININE_HEAVY = 0
    FEMININE = 1
    NEUTRAL = 2
    MASCULINE = 3
    MASCULINE_HEAVY = 4

from enum import IntEnum
__all__ = ['EFurryPreference']


class EFurryPreference(IntEnum):
    HUMAN = 0
    MINIMUM = 1
    REDUCED = 2
    NORMAL = 3
    MAXIMUM = 4

from enum import IntEnum
__all__ = ['ESubspeciesPreference']


class ESubspeciesPreference(IntEnum):
    ZERO_NONE = 0
    ONE_LOW = 1
    TWO_AVERAGE = 2
    THREE_HIGH = 3
    FOUR_ABUNDANT = 4

from enum import IntEnum
__all__ = ['ETFPotency']


class ETFPotency(IntEnum):
    MAJOR_DRAIN = 0
    DRAIN = 1
    MINOR_DRAIN = 2
    MINOR_BOOST = 3
    BOOST = 4
    MAJOR_BOOST = 5

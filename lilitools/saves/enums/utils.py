from typing import Final, List, Optional, Tuple
from frozenlist import FrozenList
from lilitools.saves.enums.tf_modifier import ETFModifier

##from [LT]src/com/lilithsthrone/game/inventory/enchanting/AbstractItemEffectType.java: private static int modifierTypeToInt(TFModifier modifier) {
TF_MOD_TYPES: FrozenList[ETFModifier] = FrozenList([
    ETFModifier.TF_TYPE_1,
    ETFModifier.TF_TYPE_2,
    ETFModifier.TF_TYPE_3,
    ETFModifier.TF_TYPE_4,
    ETFModifier.TF_TYPE_5,
    ETFModifier.TF_TYPE_6,
    ETFModifier.TF_TYPE_7,
    ETFModifier.TF_TYPE_8,
    ETFModifier.TF_TYPE_9,
    ETFModifier.TF_TYPE_10,
])
def is_tfmod_type(tfmod: ETFModifier) -> bool:
    return tfmod in TF_MOD_TYPES

def modtype2int(tfmod: ETFModifier) -> int:
    return TF_MOD_TYPES.index(tfmod)

def int2modtype(i: int) -> ETFModifier:
    return TF_MOD_TYPES[i]

from enum import IntEnum
__all__ = ['EWeather']


class EWeather(IntEnum):
    CLOUD = 0
    CLEAR = 1
    RAIN = 2
    SNOW = 3
    MAGIC_STORM_GATHERING = 4
    MAGIC_STORM = 5

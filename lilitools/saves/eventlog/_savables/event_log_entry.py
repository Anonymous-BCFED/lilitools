#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawEventLogEntry']
## from [LT]/src/com/lilithsthrone/game/dialogue/eventLog/EventLogEntry.java: public Element saveAsXML(Element parentElement, Document doc) { @ 4wtwCpB/6iqjQOp/4ElWiEoA18sulM3VfvBeWFixSStr2DLGt03hXkrt/DgDUDg+vQmIuD+1Q3cJU4zDJZJj6g==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawEventLogEntry(Savable):
    TAG = 'eventLogEntry'
    _XMLID_ATTR_DESCRIPTION_ATTRIBUTE: str = 'description'
    _XMLID_ATTR_NAME_ATTRIBUTE: str = 'name'

    def __init__(self) -> None:
        super().__init__()
        self.name: str = ''
        self.description: str = ''

    def overrideAttrs(self, description_attribute: _Optional_str = None, name_attribute: _Optional_str = None) -> None:
        if description_attribute is not None:
            self._XMLID_ATTR_DESCRIPTION_ATTRIBUTE = description_attribute
        if name_attribute is not None:
            self._XMLID_ATTR_NAME_ATTRIBUTE = name_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_NAME_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_NAME_ATTRIBUTE, 'name')
        else:
            self.name = str(value)
        value = e.get(self._XMLID_ATTR_DESCRIPTION_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_DESCRIPTION_ATTRIBUTE, 'description')
        else:
            self.description = str(value)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_NAME_ATTRIBUTE] = str(self.name)
        e.attrib[self._XMLID_ATTR_DESCRIPTION_ATTRIBUTE] = str(self.description)
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['name'] = self.name
        data['description'] = self.description
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'name' not in data:
            raise KeyRequiredException(self, data, 'name', 'name')
        self.name = str(data['name'])
        if 'description' not in data:
            raise KeyRequiredException(self, data, 'description', 'description')
        self.description = str(data['description'])

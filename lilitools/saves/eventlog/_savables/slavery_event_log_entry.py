#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, List, Optional, Set
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawSlaveryEventLogEntry']
## from [LT]/src/com/lilithsthrone/game/dialogue/eventLog/SlaveryEventLogEntry.java: public Element saveAsXML(Element parentElement, Document doc) { @ 9IZVMorsXrvdx2u9CusBvnrL+O2qh2azvxCqmYhd+00uqCmMwEvxsl1QYODN2NrKgfOSSp3Aom4rOcESU578ug==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawSlaveryEventLogEntry(Savable):
    TAG = 'eventLogEntry'
    _XMLID_ATTR_EVENT_ATTRIBUTE: str = 'event'
    _XMLID_ATTR_EXTRAEFFECTS_VALUEATTR: str = 'value'
    _XMLID_ATTR_SLAVEID_ATTRIBUTE: str = 'slaveID'
    _XMLID_ATTR_TAGS_VALUEATTR: str = 'value'
    _XMLID_ATTR_TIME_ATTRIBUTE: str = 'time'
    _XMLID_TAG_EXTRAEFFECTS_CHILD: str = 'entry'
    _XMLID_TAG_EXTRAEFFECTS_PARENT: str = 'extraEffects'
    _XMLID_TAG_INVOLVEDSLAVES_CHILD: str = 'id'
    _XMLID_TAG_INVOLVEDSLAVES_PARENT: str = 'involvedSlaves'
    _XMLID_TAG_TAGS_CHILD: str = 'tag'
    _XMLID_TAG_TAGS_PARENT: str = 'tags'

    def __init__(self) -> None:
        super().__init__()
        self.time: int = 0
        self.slaveID: str = ''
        self.event: str = ''
        self.involvedSlaves: Set[str] = set()
        self.tags: Optional[Set[str]] = set()
        self.extraEffects: Optional[List[str]] = list()

    def overrideAttrs(self, event_attribute: _Optional_str = None, extraEffects_valueattr: _Optional_str = None, slaveID_attribute: _Optional_str = None, tags_valueattr: _Optional_str = None, time_attribute: _Optional_str = None) -> None:
        if event_attribute is not None:
            self._XMLID_ATTR_EVENT_ATTRIBUTE = event_attribute
        if extraEffects_valueattr is not None:
            self._XMLID_ATTR_EXTRAEFFECTS_VALUEATTR = extraEffects_valueattr
        if slaveID_attribute is not None:
            self._XMLID_ATTR_SLAVEID_ATTRIBUTE = slaveID_attribute
        if tags_valueattr is not None:
            self._XMLID_ATTR_TAGS_VALUEATTR = tags_valueattr
        if time_attribute is not None:
            self._XMLID_ATTR_TIME_ATTRIBUTE = time_attribute

    def overrideTags(self, extraEffects_child: _Optional_str = None, extraEffects_parent: _Optional_str = None, involvedSlaves_child: _Optional_str = None, involvedSlaves_parent: _Optional_str = None, tags_child: _Optional_str = None, tags_parent: _Optional_str = None) -> None:
        if extraEffects_child is not None:
            self._XMLID_TAG_EXTRAEFFECTS_CHILD = extraEffects_child
        if extraEffects_parent is not None:
            self._XMLID_TAG_EXTRAEFFECTS_PARENT = extraEffects_parent
        if involvedSlaves_child is not None:
            self._XMLID_TAG_INVOLVEDSLAVES_CHILD = involvedSlaves_child
        if involvedSlaves_parent is not None:
            self._XMLID_TAG_INVOLVEDSLAVES_PARENT = involvedSlaves_parent
        if tags_child is not None:
            self._XMLID_TAG_TAGS_CHILD = tags_child
        if tags_parent is not None:
            self._XMLID_TAG_TAGS_PARENT = tags_parent

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        lp: etree._Element
        value = e.get(self._XMLID_ATTR_TIME_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_TIME_ATTRIBUTE, 'time')
        else:
            self.time = int(value)
        value = e.get(self._XMLID_ATTR_SLAVEID_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_SLAVEID_ATTRIBUTE, 'slaveID')
        else:
            self.slaveID = str(value)
        value = e.get(self._XMLID_ATTR_EVENT_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_EVENT_ATTRIBUTE, 'event')
        else:
            self.event = str(value)
        if (lf := e.find(self._XMLID_TAG_INVOLVEDSLAVES_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_INVOLVEDSLAVES_CHILD):
                    self.involvedSlaves.add(lc.text)
        else:
            self.involvedSlaves = set()
        if (lf := e.find(self._XMLID_TAG_TAGS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_TAGS_CHILD):
                    self.tags.add(lc.attrib[self._XMLID_ATTR_TAGS_VALUEATTR])
        if (lf := e.find(self._XMLID_TAG_EXTRAEFFECTS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_EXTRAEFFECTS_CHILD):
                    self.extraEffects.append(lc.attrib[self._XMLID_ATTR_EXTRAEFFECTS_VALUEATTR])

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        e.attrib[self._XMLID_ATTR_TIME_ATTRIBUTE] = str(self.time)
        e.attrib[self._XMLID_ATTR_SLAVEID_ATTRIBUTE] = str(self.slaveID)
        e.attrib[self._XMLID_ATTR_EVENT_ATTRIBUTE] = str(self.event)
        lp = etree.SubElement(e, self._XMLID_TAG_INVOLVEDSLAVES_PARENT)
        for lv in sorted(self.involvedSlaves, key=str):
            etree.SubElement(lp, self._XMLID_TAG_INVOLVEDSLAVES_CHILD, {}).text = lv
        if self.tags is not None and len(self.tags) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_TAGS_PARENT)
            for lv in sorted(self.tags, key=str):
                etree.SubElement(lp, self._XMLID_TAG_TAGS_CHILD, {self._XMLID_ATTR_TAGS_VALUEATTR: lv})
        if self.extraEffects is not None and len(self.extraEffects) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_EXTRAEFFECTS_PARENT)
            for lv in self.extraEffects:
                etree.SubElement(lp, self._XMLID_TAG_EXTRAEFFECTS_CHILD, {self._XMLID_ATTR_EXTRAEFFECTS_VALUEATTR: lv})
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['time'] = int(self.time)
        data['slaveID'] = self.slaveID
        data['event'] = self.event
        if self.involvedSlaves is None or len(self.involvedSlaves) > 0:
            data['involvedSlaves'] = list()
        else:
            lc = list()
            if len(self.involvedSlaves) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['involvedSlaves'] = lc
        if self.tags is None or len(self.tags) > 0:
            data['tags'] = list()
        else:
            lc = list()
            if len(self.tags) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['tags'] = lc
        if self.extraEffects is None or len(self.extraEffects) > 0:
            data['extraEffects'] = list()
        else:
            lc = list()
            if len(self.extraEffects) > 0:
                for lv in lc:
                    lc.append(str(lv))
            data['extraEffects'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'time' not in data:
            raise KeyRequiredException(self, data, 'time', 'time')
        self.time = int(data['time'])
        if 'slaveID' not in data:
            raise KeyRequiredException(self, data, 'slaveID', 'slaveID')
        self.slaveID = str(data['slaveID'])
        if 'event' not in data:
            raise KeyRequiredException(self, data, 'event', 'event')
        self.event = str(data['event'])
        if (lv := self.involvedSlaves) is not None:
            for le in lv:
                self.involvedSlaves.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'involvedSlaves', 'involvedSlaves')
        if (lv := self.tags) is not None:
            for le in lv:
                self.tags.add(str(le))
        else:
            self.tags = set()
        if (lv := self.extraEffects) is not None:
            for le in lv:
                self.extraEffects.append(str(le))
        else:
            self.extraEffects = list()

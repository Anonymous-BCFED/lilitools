from typing import Any, Dict, Type, Union

from lxml import etree


def _getQualName(cls: Union[str, "lilitools.saves.savable.Savable"]) -> str:
    if isinstance(cls, str):
        return cls
    else:
        mod = cls.__class__.__module__
        clsname = cls.__class__.__qualname__
        return f"{mod}.{clsname}"


############################################################
# Remember to update bindings if any of these are changed. #
############################################################


class ElementRequiredException(Exception):
    def __init__(
        self,
        cls: Union[str, "lilitools.saves.savable.Savable"],
        parentElement: etree._Element,
        cls_attr: str,
        expectedTag: str,
    ) -> None:
        super().__init__(cls, parentElement, cls_attr, expectedTag)
        tree = parentElement.getroottree()
        self.message = f"{_getQualName(cls)}.{cls_attr}: Element {expectedTag} is required in {tree.getpath(parentElement)}."

    def __str__(self) -> str:
        return self.message


class AttributeRequiredException(Exception):
    def __init__(
        self,
        cls: Union[str, "lilitools.saves.savable.Savable"],
        element: etree._Element,
        xml_attr: str,
        cls_attr: str,
    ) -> None:
        super().__init__(cls, element, xml_attr, cls_attr)
        tree = element.getroottree()
        self.message = f"{_getQualName(cls)}.{cls_attr}: Attribute {xml_attr} is required in {tree.getpath(element)}."

    def __str__(self) -> str:
        return self.message


class CannotSerializeToXML(Exception):
    pass


class KeyRequiredException(Exception):
    def __init__(
        self,
        cls: Union[str, "lilitools.saves.savable.Savable"],
        data: Dict[str, Any],
        cls_attr: str,
        expectedKey: str,
    ) -> None:
        super().__init__(cls, data, expectedKey)
        self.message = (
            f"{_getQualName(cls)}.{cls_attr}: Element {expectedKey} is required"
        )

    def __str__(self) -> str:
        return self.message


from typing import Dict, Generator, List

from lilitools.saves.enums.tf_modifier import ETFModifier


FETISH_TYPES: Dict[ETFModifier, List[ETFModifier]] = {
    ETFModifier.TF_MOD_FETISH_BEHAVIOUR: [
        ETFModifier.TF_MOD_FETISH_BIMBO,
        ETFModifier.TF_MOD_FETISH_BONDAGE_APPLIER,
        ETFModifier.TF_MOD_FETISH_BONDAGE_VICTIM,
        ETFModifier.TF_MOD_FETISH_CROSS_DRESSER,
        ETFModifier.TF_MOD_FETISH_CUM_ADDICT,
        ETFModifier.TF_MOD_FETISH_CUM_STUD,
        ETFModifier.TF_MOD_FETISH_DEFLOWERING,
        ETFModifier.TF_MOD_FETISH_DENIAL_SELF,
        ETFModifier.TF_MOD_FETISH_DENIAL,
        ETFModifier.TF_MOD_FETISH_DOMINANT,
        ETFModifier.TF_MOD_FETISH_EXHIBITIONIST,
        ETFModifier.TF_MOD_FETISH_IMPREGNATION,
        ETFModifier.TF_MOD_FETISH_INCEST,
        ETFModifier.TF_MOD_FETISH_KINK_GIVING,
        ETFModifier.TF_MOD_FETISH_KINK_RECEIVING,
        ETFModifier.TF_MOD_FETISH_MASOCHIST,
        ETFModifier.TF_MOD_FETISH_MASTURBATION,
        ETFModifier.TF_MOD_FETISH_NON_CON_DOM,
        ETFModifier.TF_MOD_FETISH_NON_CON_SUB,
        ETFModifier.TF_MOD_FETISH_PREGNANCY,
        ETFModifier.TF_MOD_FETISH_PURE_VIRGIN,
        ETFModifier.TF_MOD_FETISH_SADIST,
        ETFModifier.TF_MOD_FETISH_SIZE_QUEEN,
        ETFModifier.TF_MOD_FETISH_SUBMISSIVE,
        ETFModifier.TF_MOD_FETISH_TRANSFORMATION_GIVING,
        ETFModifier.TF_MOD_FETISH_TRANSFORMATION_RECEIVING,
        ETFModifier.TF_MOD_FETISH_VOYEURIST,
    ],
    ETFModifier.TF_MOD_FETISH_BODY_PART: [
        ETFModifier.TF_MOD_FETISH_ANAL_GIVING,
        ETFModifier.TF_MOD_FETISH_ANAL_RECEIVING,
        ETFModifier.TF_MOD_FETISH_ARMPIT_GIVING,
        ETFModifier.TF_MOD_FETISH_ARMPIT_RECEIVING,
        ETFModifier.TF_MOD_FETISH_BREASTS_OTHERS,
        ETFModifier.TF_MOD_FETISH_BREASTS_SELF,
        ETFModifier.TF_MOD_FETISH_FOOT_GIVING,
        ETFModifier.TF_MOD_FETISH_FOOT_RECEIVING,
        ETFModifier.TF_MOD_FETISH_LACTATION_OTHERS,
        ETFModifier.TF_MOD_FETISH_LACTATION_SELF,
        ETFModifier.TF_MOD_FETISH_LEG_LOVER,
        ETFModifier.TF_MOD_FETISH_ORAL_GIVING,
        ETFModifier.TF_MOD_FETISH_ORAL_RECEIVING,
        ETFModifier.TF_MOD_FETISH_PENIS_GIVING,
        ETFModifier.TF_MOD_FETISH_PENIS_RECEIVING,
        ETFModifier.TF_MOD_FETISH_STRUTTER,
        ETFModifier.TF_MOD_FETISH_VAGINAL_GIVING,
        ETFModifier.TF_MOD_FETISH_VAGINAL_RECEIVING,
    ],
}


def getAllKnownFetishes() -> Generator[str, None, None]:
    for mod in ETFModifier:
        name = mod.name
        if name.startswith('TF_MOD_FETISH'):
            yield name[7:]


def getAllKnownFetishModifiers() -> Generator[ETFModifier, None, None]:
    for mod in ETFModifier:
        name = mod.name
        if name.startswith('TF_MOD_FETISH'):
            yield mod

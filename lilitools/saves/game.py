from typing import Any, Dict, List, Optional, Set

from lxml import etree
from lilitools.consts import PRESERVE_SAVE_ORDERING
from lilitools.game.characters import NPCS_BY_PATH
from lilitools.logging import ClickWrap

from lilitools.saves._savables.game import RawGame
from lilitools.saves.character.game_character import GameCharacter
from lilitools.saves.character.npc import NPC
from lilitools.saves.character.offspring_seed import OffspringSeed
from lilitools.saves.eventlog.slavery_event_log_entry import SlaveryEventLogEntry
from lilitools.saves.properties.gameproperties import GameProperties

click = ClickWrap()


class Game(RawGame):
    INSTANCE_LOADING: Optional["Game"] = None
    ALL_INSTANCES: List["Game"] = []

    def __init__(self) -> None:
        super().__init__()
        self.npcsByID: Dict[str, NPC] = {}
        self.currentNPCID: int = 0

        self.show_warnings_during_load: bool = False
        self.load_quietly: bool = False
        self.properties: Optional[GameProperties] = None

        self.id: int = len(self.ALL_INSTANCES)
        self.ALL_INSTANCES.append(self)

    @classmethod
    def getAnInstance(self, id: Optional[None] = None) -> "Game":
        g: "Game"
        if len(self.ALL_INSTANCES) == 0:
            self.ALL_INSTANCES.append(Game())
        return self.ALL_INSTANCES[0]

    def getNPCById(self, npcid: str) -> Optional[NPC]:
        if npcid == "PlayerCharacter":
            return self.playerCharacter
        return self.npcsByID.get(npcid, None)

    def addNPC(self, npc: NPC) -> NPC:
        self.npcs.append(npc)
        self.npcsByID[npc.core.id] = npc
        return npc

    def removeNPCById(self, npcid: str) -> Optional[NPC]:
        if (npc := self.npcsByID.get(npcid)) is not None:
            del self.npcsByID[npc.core.id]
            self.npcs.remove(npc)
            return npc
        return None

    def removeNPC(self, npc: NPC) -> Optional[NPC]:
        if npc.core.id in self.npcsByID:
            del self.npcsByID[npc.core.id]
            self.npcs.remove(npc)
            return npc
        return None

    def removeOffspringById(self, oid: str) -> Optional[OffspringSeed]:
        sd: OffspringSeed
        for sd in self.offspringSeed.copy():
            if sd.data.id == oid:
                self.offspringSeed.remove(sd)
                return sd
        return None

    def _fromXML_npcs(self, e: etree._Element) -> None:
        for i, lc in enumerate(e.findall("NPC")):
            path = lc.xpath("./character/core/pathName")[0].attrib["value"]
            lv = NPCS_BY_PATH.get(path, NPC)()
            lv.fromXML(lc)
            self.addNPC(lv)
            # if not self.load_quietly:
            #     if path in NPCS_BY_PATH:
            #         print(f'I: [{i}] Loaded NPC {lv.core.id} ({lv.core.pathName}) as {lv.__class__.__module__}.{lv.__class__.__qualname__}.')

    def _toXML_npcs(self, e: etree._Element) -> None:
        # for npc in sorted(self.npcs, key=lambda c: c.core.id):
        for npc in self.npcs:
            e.append(npc.toXML())

    def _toDict_npcs(self, data: Dict[str, Any]) -> None:
        npcs = []
        for npc in self.npcs:
            npcs.append(npc.toDict())
        data["npcs"] = npcs

    def _fromDict_npcs(self, data: Dict[str, Any]) -> None:
        for i, lc in enumerate(data["npcs"]):
            path = lc["character"]["core"]["pathName"]
            lv = NPCS_BY_PATH.get(path, NPC)()
            lv.fromDict(lc)
            self.addNPC(lv)

    if PRESERVE_SAVE_ORDERING:

        def _toXML_savedEnforcers(self, e: etree._Element) -> None:
            se: etree._Element = etree.SubElement(e, "savedEnforcers")
            wrldID: str
            enforcerSquads: List[List[str]]
            enforcerSquad: List[str]
            for wrldID, enforcerSquads in self.savedEnforcers.items():
                w = etree.SubElement(se, "world", {"type": wrldID})
                for enforcerSquad in enforcerSquads:
                    squad = etree.SubElement(w, "enforcers", {})
                    for enforcer in enforcerSquad:
                        etree.SubElement(squad, "id", {}).text = enforcer

        def _fromXML_savedEnforcers(self, e: etree._Element) -> None:
            squadWorlds: etree._Element
            squadWorld: etree._Element
            squadElement: etree._Element
            enforcer: etree._Element
            if (squadWorlds := e.find("savedEnforcers")) is not None:
                for squadWorld in squadWorlds.findall("world"):
                    w = squadWorld.attrib["type"]
                    self.savedEnforcers[w] = []
                    for squadElement in squadWorld.findall("enforcers"):
                        squad: List[str] = list()
                        for enforcer in squadElement.findall("id"):
                            squad.append(enforcer.text or "")
                        self.savedEnforcers[w].append(squad)

    else:

        def _toXML_savedEnforcers(self, e: etree._Element) -> None:
            se: etree._Element = etree.SubElement(e, "savedEnforcers")
            wrldID: str
            enforcerSquads: Set[Set[str]]
            enforcerSquad: Set[str]
            for wrldID, enforcerSquads in sorted(
                self.savedEnforcers.items(), key=lambda t: t[0]
            ):
                w = etree.SubElement(se, "world", {"type": wrldID})
                for enforcerSquad in sorted(
                    enforcerSquads, key=lambda s: tuple(sorted(s))
                ):
                    squad = etree.SubElement(w, "enforcers", {})
                    for enforcer in sorted(enforcerSquad):
                        etree.SubElement(squad, "id", {}).text = enforcer

        def _fromXML_savedEnforcers(self, e: etree._Element) -> None:
            squadWorlds: etree._Element
            squadWorld: etree._Element
            squadElement: etree._Element
            enforcer: etree._Element
            if (squadWorlds := e.find("savedEnforcers")) is not None:
                for squadWorld in squadWorlds.findall("world"):
                    w = squadWorld.attrib["type"]
                    self.savedEnforcers[w] = []
                    for squadElement in squadWorld.findall("enforcers"):
                        squad: Set[str] = set()
                        for enforcer in squadElement.findall("id"):
                            squad.add(enforcer.text or "")
                        self.savedEnforcers[w].append(squad)

    def _toDict_savedEnforcers(self, data: Dict[str, Any]) -> None:
        squadWorlds: Dict[str, List[List[str]]] = {}
        for wrldID, enforcerSquads in self.savedEnforcers.items():
            squads: List[List[str]] = []
            for enforcerSquad in enforcerSquads:
                squad: List[str] = []
                for enforcer in sorted(enforcerSquad):
                    squad.append(enforcer)
                squads.append(squad)
            squadWorlds[wrldID] = squads
        data["savedEnforcers"] = squadWorlds

    def _fromDict_savedEnforcers(self, data: Dict[str, Any]) -> None:
        wrldID: str
        squads: List[List[str]]
        squad: List[str]
        enforcer: str
        self.savedEnforcers = {}
        for wrldID, squads in data["savedEnforcers"].items():
            self.savedEnforcers[wrldID] = set()
            currentSquads: Set[Set[str]] = self.savedEnforcers[wrldID]
            for squad in squads:
                currentSquad: Set[str] = set()
                currentSquads.add(currentSquad)
                for enforcer in squad:
                    currentSquad.add(enforcer)

    def _fromXML_slaveryEventLog(self, e: etree._Element) -> None:
        if (slaveryEventLogElem := e.find("slaveryEventLog")) is not None:
            for dayElem in slaveryEventLogElem.findall("day"):
                day: int = int(dayElem.attrib["value"])
                entries = []
                for entryElem in dayElem.findall("eventLogEntry"):
                    entry = SlaveryEventLogEntry()
                    entry.fromXML(entryElem)
                    entries.append(entry)
                self.slaveryEventLog[day] = entries

    def _toXML_slaveryEventLog(self, e: etree._Element) -> None:
        slaveryEventLogElem = etree.SubElement(e, "slaveryEventLog")
        iterdata = list(self.slaveryEventLog.items())
        if not PRESERVE_SAVE_ORDERING:
            iterdata.sort(key=lambda t: t[0])
        for seDay, eventLogEntries in iterdata:
            eventLogDayEntryElem = etree.SubElement(
                slaveryEventLogElem, "day", {"value": str(seDay)}
            )
            for eventLogEntry in eventLogEntries:
                eventLogDayEntryElem.append(eventLogEntry.toXML())

    def _fromDict_slaveryEventLog(self, data: Dict[str, Any]) -> None:
        day: int
        entries: List[Dict[str, Any]]
        entry: Dict[str, Any]
        if (slaveryEventLogEntries := data.get("slaveryEventLog")) is not None:
            self.slaveryEventLog = {}
            for day, entries in slaveryEventLogEntries.items():
                read_entries: List[SlaveryEventLogEntry] = []
                for entry in entries:
                    sele = SlaveryEventLogEntry()
                    sele.fromDict(entry)
                    read_entries.append(sele)
                self.slaveryEventLog[day] = read_entries

    def _toDict_slaveryEventLog(self, data: Dict[str, Any]) -> None:
        sSlaveryEventLogs: Dict[int, List[Dict[str, Any]]] = {}
        for day, entries in self.slaveryEventLog.items():
            sSlaveryEventLogs[day] = [e.toDict() for e in entries]
        data["slaveryEventLogs"] = sSlaveryEventLogs

    def fromXML(self, e: etree._Element) -> None:
        self.INSTANCE_LOADING = self
        super().fromXML(e)

        npcs: Set[NPC] = self.npcs.copy()
        self.npcs.clear()
        for npc in npcs:
            self.addNPC(npc)
        self.INSTANCE_LOADING = None

    def fromXMLBase(self, e: etree.ElementBase) -> None:
        if isinstance(e, etree._ElementTree):
            e = e.getroot()
        self.fromXML(e)

    def propogateNameChangeToChildren(self, parent: GameCharacter) -> None:
        ## from [LT]/src/com/lilithsthrone/game/dialogue/places/dominion/cityHall/CityHallDemographics.java: private static void applyNameChange(boolean applyOffspringSurnames) { @ e98Sxy03lVeJn11eENZgvse88Efq3IkiH0Q/QvzB7+rvKnvJPniBUv0JsIsHtzB8zrIGKh+RxBxdGMMDiXb8Hg==
        mother: Optional[NPC] = None
        npc: NPC
        for npc in self.npcs:
            if (mother := npc.getMother(self)) is not None:
                while (mother := mother.getMother(self)) is not None:
                    pass
                if mother == parent:
                    old = npc.core.surname
                    npc.core.surname = parent.core.surname
                    click.success(
                        f"{npc.core.id}: {npc.getName()} {old!r} -> {npc.core.surname}"
                    )

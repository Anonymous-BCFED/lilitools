#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional, Set
from lxml import etree
from lilitools.saves.character.enums.coverable_area import ECoverableArea
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.inventory.enums.clothing_access import EClothingAccess
from lilitools.saves.inventory.enums.displacement_type import EDisplacementType
from lilitools.saves.inventoryslot import EInventorySlot
from lilitools.saves.savable import Savable
__all__ = ['RawBlockedParts']
## from [LT]/src/com/lilithsthrone/game/inventory/clothing/BlockedParts.java: public Element saveAsXML(Element parentElement, Document doc) { @ +GHcfBtQDSs3s9Njokgfc3c2MpNY7a3d5OW9HO7t7UaweWVqg8R0M/dVha7fitfnEVZny7j1J+hhbf+JM479gg==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawBlockedParts(Savable):
    TAG = 'blockedParts'
    _XMLID_TAG_BLOCKEDBODYPARTS_CHILD: str = 'bodyPart'
    _XMLID_TAG_BLOCKEDBODYPARTS_PARENT: str = 'blockedBodyParts'
    _XMLID_TAG_CLOTHINGACCESSBLOCKED_CHILD: str = 'clothingAccess'
    _XMLID_TAG_CLOTHINGACCESSBLOCKED_PARENT: str = 'clothingAccessBlocked'
    _XMLID_TAG_CLOTHINGACCESSREQUIRED_CHILD: str = 'clothingAccess'
    _XMLID_TAG_CLOTHINGACCESSREQUIRED_PARENT: str = 'clothingAccessRequired'
    _XMLID_TAG_CONCEALEDSLOTS_CHILD: str = 'slot'
    _XMLID_TAG_CONCEALEDSLOTS_PARENT: str = 'concealedSlots'
    _XMLID_TAG_DISPLACEMENTTYPE_ELEMENT: str = 'displacementType'

    def __init__(self) -> None:
        super().__init__()
        self.displacementType: EDisplacementType = EDisplacementType(0)  # Element
        self.clothingAccessRequired: Set[EClothingAccess] = set()
        self.blockedBodyParts: Set[ECoverableArea] = set()
        self.clothingAccessBlocked: Set[EClothingAccess] = set()
        self.concealedSlots: Set[EInventorySlot] = set()

    def overrideTags(self, blockedBodyParts_child: _Optional_str = None, blockedBodyParts_parent: _Optional_str = None, clothingAccessBlocked_child: _Optional_str = None, clothingAccessBlocked_parent: _Optional_str = None, clothingAccessRequired_child: _Optional_str = None, clothingAccessRequired_parent: _Optional_str = None, concealedSlots_child: _Optional_str = None, concealedSlots_parent: _Optional_str = None, displacementType_element: _Optional_str = None) -> None:
        if blockedBodyParts_child is not None:
            self._XMLID_TAG_BLOCKEDBODYPARTS_CHILD = blockedBodyParts_child
        if blockedBodyParts_parent is not None:
            self._XMLID_TAG_BLOCKEDBODYPARTS_PARENT = blockedBodyParts_parent
        if clothingAccessBlocked_child is not None:
            self._XMLID_TAG_CLOTHINGACCESSBLOCKED_CHILD = clothingAccessBlocked_child
        if clothingAccessBlocked_parent is not None:
            self._XMLID_TAG_CLOTHINGACCESSBLOCKED_PARENT = clothingAccessBlocked_parent
        if clothingAccessRequired_child is not None:
            self._XMLID_TAG_CLOTHINGACCESSREQUIRED_CHILD = clothingAccessRequired_child
        if clothingAccessRequired_parent is not None:
            self._XMLID_TAG_CLOTHINGACCESSREQUIRED_PARENT = clothingAccessRequired_parent
        if concealedSlots_child is not None:
            self._XMLID_TAG_CONCEALEDSLOTS_CHILD = concealedSlots_child
        if concealedSlots_parent is not None:
            self._XMLID_TAG_CONCEALEDSLOTS_PARENT = concealedSlots_parent
        if displacementType_element is not None:
            self._XMLID_TAG_DISPLACEMENTTYPE_ELEMENT = displacementType_element

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_DISPLACEMENTTYPE_ELEMENT)) is not None:
            self.displacementType = EDisplacementType[sf.text]
        else:
            raise ElementRequiredException(self, e, 'displacementType', self._XMLID_TAG_DISPLACEMENTTYPE_ELEMENT)
        if (lf := e.find(self._XMLID_TAG_CLOTHINGACCESSREQUIRED_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_CLOTHINGACCESSREQUIRED_CHILD):
                    self.clothingAccessRequired.add(EClothingAccess[lc.text])
        else:
            self.clothingAccessRequired = set()
        if (lf := e.find(self._XMLID_TAG_BLOCKEDBODYPARTS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_BLOCKEDBODYPARTS_CHILD):
                    self.blockedBodyParts.add(ECoverableArea[lc.text])
        else:
            self.blockedBodyParts = set()
        if (lf := e.find(self._XMLID_TAG_CLOTHINGACCESSBLOCKED_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_CLOTHINGACCESSBLOCKED_CHILD):
                    self.clothingAccessBlocked.add(EClothingAccess[lc.text])
        else:
            self.clothingAccessBlocked = set()
        if (lf := e.find(self._XMLID_TAG_CONCEALEDSLOTS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_CONCEALEDSLOTS_CHILD):
                    self.concealedSlots.add(EInventorySlot[lc.text])
        else:
            self.concealedSlots = set()

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        etree.SubElement(e, self._XMLID_TAG_DISPLACEMENTTYPE_ELEMENT).text = self.displacementType.name
        lp = etree.SubElement(e, self._XMLID_TAG_CLOTHINGACCESSREQUIRED_PARENT)
        for lv in sorted(self.clothingAccessRequired, key=lambda x: x.name):
            etree.SubElement(lp, self._XMLID_TAG_CLOTHINGACCESSREQUIRED_CHILD, {}).text = lv.name
        lp = etree.SubElement(e, self._XMLID_TAG_BLOCKEDBODYPARTS_PARENT)
        for lv in sorted(self.blockedBodyParts, key=lambda x: x.name):
            etree.SubElement(lp, self._XMLID_TAG_BLOCKEDBODYPARTS_CHILD, {}).text = lv.name
        lp = etree.SubElement(e, self._XMLID_TAG_CLOTHINGACCESSBLOCKED_PARENT)
        for lv in sorted(self.clothingAccessBlocked, key=lambda x: x.name):
            etree.SubElement(lp, self._XMLID_TAG_CLOTHINGACCESSBLOCKED_CHILD, {}).text = lv.name
        lp = etree.SubElement(e, self._XMLID_TAG_CONCEALEDSLOTS_PARENT)
        for lv in sorted(self.concealedSlots, key=lambda x: x.name):
            etree.SubElement(lp, self._XMLID_TAG_CONCEALEDSLOTS_CHILD, {}).text = lv.name
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['displacementType'] = self.displacementType.name
        if self.clothingAccessRequired is None or len(self.clothingAccessRequired) > 0:
            data['clothingAccessRequired'] = list()
        else:
            lc = list()
            if len(self.clothingAccessRequired) > 0:
                for lv in sorted(lc, lambda x: x.name):
                    lc.append(lv.name)
            data['clothingAccessRequired'] = lc
        if self.blockedBodyParts is None or len(self.blockedBodyParts) > 0:
            data['blockedBodyParts'] = list()
        else:
            lc = list()
            if len(self.blockedBodyParts) > 0:
                for lv in sorted(lc, lambda x: x.name):
                    lc.append(lv.name)
            data['blockedBodyParts'] = lc
        if self.clothingAccessBlocked is None or len(self.clothingAccessBlocked) > 0:
            data['clothingAccessBlocked'] = list()
        else:
            lc = list()
            if len(self.clothingAccessBlocked) > 0:
                for lv in sorted(lc, lambda x: x.name):
                    lc.append(lv.name)
            data['clothingAccessBlocked'] = lc
        if self.concealedSlots is None or len(self.concealedSlots) > 0:
            data['concealedSlots'] = list()
        else:
            lc = list()
            if len(self.concealedSlots) > 0:
                for lv in sorted(lc, lambda x: x.name):
                    lc.append(lv.name)
            data['concealedSlots'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('displacementType')) is not None:
            self.displacementType = data['displacementType'].name
        else:
            raise KeyRequiredException(self, data, 'displacementType', 'displacementType')
        if (lv := self.clothingAccessRequired) is not None:
            for le in lv:
                self.clothingAccessRequired.add(le.name)
        else:
            raise KeyRequiredException(self, data, 'clothingAccessRequired', 'clothingAccessRequired')
        if (lv := self.blockedBodyParts) is not None:
            for le in lv:
                self.blockedBodyParts.add(le.name)
        else:
            raise KeyRequiredException(self, data, 'blockedBodyParts', 'blockedBodyParts')
        if (lv := self.clothingAccessBlocked) is not None:
            for le in lv:
                self.clothingAccessBlocked.add(le.name)
        else:
            raise KeyRequiredException(self, data, 'clothingAccessBlocked', 'clothingAccessBlocked')
        if (lv := self.concealedSlots) is not None:
            for le in lv:
                self.concealedSlots.add(le.name)
        else:
            raise KeyRequiredException(self, data, 'concealedSlots', 'concealedSlots')

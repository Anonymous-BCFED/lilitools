#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from collections import OrderedDict
from typing import Any, Dict, List, Optional, Set
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.inventory.blocked_parts import BlockedParts
from lilitools.saves.inventoryslot import EInventorySlot
from lilitools.saves.items.abstract_clothing import AbstractClothing
from lilitools.saves.items.abstract_weapon import AbstractWeapon
from lilitools.saves.savable import Savable
__all__ = ['RawCharacterInventory']
## from [LT]/src/com/lilithsthrone/game/inventory/CharacterInventory.java: public Element saveAsXML(Element parentElement, Document doc) { @ epbQPMaCuIhXNQXfSVRO3qRQf/SRfQnhGUzXjMqYYqB8MUCGDL/+ME/zyl2Xdx4eGCZKduAMaQIiQclHGYINcA==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawCharacterInventory(Savable):
    TAG = 'characterInventory'
    _XMLID_ATTR_CLOTHINGININVENTORY_VALUEATTR: str = 'count'
    _XMLID_ATTR_DIRTYSLOTS_VALUEATTR: str = 'slot'
    _XMLID_ATTR_ESSENCECOUNT_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_EXTRABLOCKEDPARTS_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_MAXINVENTORYSPACE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_MONEY_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_WEAPONSININVENTORY_VALUEATTR: str = 'count'
    _XMLID_TAG_CLOTHINGEQUIPPED_PARENT: str = 'clothingEquipped'
    _XMLID_TAG_CLOTHINGEQUIPPED_VALUEELEM: Optional[str] = None
    _XMLID_TAG_CLOTHINGININVENTORY_CHILD: Optional[str] = None
    _XMLID_TAG_CLOTHINGININVENTORY_KEYELEM: str = 'clothing'
    _XMLID_TAG_CLOTHINGININVENTORY_PARENT: str = 'clothingInInventory'
    _XMLID_TAG_DIRTYSLOTS_CHILD: str = 'dirtySlot'
    _XMLID_TAG_DIRTYSLOTS_PARENT: str = 'dirtySlots'
    _XMLID_TAG_ESSENCECOUNT_ELEMENT: str = 'essenceCount'
    _XMLID_TAG_EXTRABLOCKEDPARTS_ELEMENT: str = 'extraBlockedParts'
    _XMLID_TAG_MAXINVENTORYSPACE_ELEMENT: str = 'maxInventorySpace'
    _XMLID_TAG_MONEY_ELEMENT: str = 'money'
    _XMLID_TAG_WEAPONSININVENTORY_CHILD: Optional[str] = None
    _XMLID_TAG_WEAPONSININVENTORY_KEYELEM: str = 'weapon'
    _XMLID_TAG_WEAPONSININVENTORY_PARENT: str = 'weaponsInInventory'

    def __init__(self) -> None:
        super().__init__()
        self.maxInventorySpace: int = 0  # Element
        self.money: int = 0  # Element
        self.essenceCount: int = 0  # Element
        self.extraBlockedParts: Optional[BlockedParts] = None  # Element
        self.dirtySlots: Set[EInventorySlot] = set()
        self.unlockKeyMap: OrderedDict[str, Set[EInventorySlot]] = OrderedDict()
        self.mainWeapons: List[AbstractWeapon] = []
        self.offhandWeapons: List[AbstractWeapon] = []
        self.clothingEquipped: List[AbstractClothing] = list()
        self.itemsInInventory: OrderedDict[AbstractItem, int] = OrderedDict()
        self.clothingInInventory: Dict[AbstractClothing, int] = {}
        self.weaponsInInventory: Dict[AbstractWeapon, int] = {}

    def overrideAttrs(self, clothingInInventory_valueattr: _Optional_str = None, dirtySlots_valueattr: _Optional_str = None, essenceCount_attribute: _Optional_str = None, extraBlockedParts_attribute: _Optional_str = None, maxInventorySpace_attribute: _Optional_str = None, money_attribute: _Optional_str = None, weaponsInInventory_valueattr: _Optional_str = None) -> None:
        if clothingInInventory_valueattr is not None:
            self._XMLID_ATTR_CLOTHINGININVENTORY_VALUEATTR = clothingInInventory_valueattr
        if dirtySlots_valueattr is not None:
            self._XMLID_ATTR_DIRTYSLOTS_VALUEATTR = dirtySlots_valueattr
        if essenceCount_attribute is not None:
            self._XMLID_ATTR_ESSENCECOUNT_ATTRIBUTE = essenceCount_attribute
        if extraBlockedParts_attribute is not None:
            self._XMLID_ATTR_EXTRABLOCKEDPARTS_ATTRIBUTE = extraBlockedParts_attribute
        if maxInventorySpace_attribute is not None:
            self._XMLID_ATTR_MAXINVENTORYSPACE_ATTRIBUTE = maxInventorySpace_attribute
        if money_attribute is not None:
            self._XMLID_ATTR_MONEY_ATTRIBUTE = money_attribute
        if weaponsInInventory_valueattr is not None:
            self._XMLID_ATTR_WEAPONSININVENTORY_VALUEATTR = weaponsInInventory_valueattr

    def overrideTags(self, clothingEquipped_parent: _Optional_str = None, clothingEquipped_valueelem: _Optional_str = None, clothingInInventory_child: _Optional_str = None, clothingInInventory_keyelem: _Optional_str = None, clothingInInventory_parent: _Optional_str = None, dirtySlots_child: _Optional_str = None, dirtySlots_parent: _Optional_str = None, essenceCount_element: _Optional_str = None, extraBlockedParts_element: _Optional_str = None, maxInventorySpace_element: _Optional_str = None, money_element: _Optional_str = None, weaponsInInventory_child: _Optional_str = None, weaponsInInventory_keyelem: _Optional_str = None, weaponsInInventory_parent: _Optional_str = None) -> None:
        if clothingEquipped_parent is not None:
            self._XMLID_TAG_CLOTHINGEQUIPPED_PARENT = clothingEquipped_parent
        if clothingEquipped_valueelem is not None:
            self._XMLID_TAG_CLOTHINGEQUIPPED_VALUEELEM = clothingEquipped_valueelem
        if clothingInInventory_child is not None:
            self._XMLID_TAG_CLOTHINGININVENTORY_CHILD = clothingInInventory_child
        if clothingInInventory_keyelem is not None:
            self._XMLID_TAG_CLOTHINGININVENTORY_KEYELEM = clothingInInventory_keyelem
        if clothingInInventory_parent is not None:
            self._XMLID_TAG_CLOTHINGININVENTORY_PARENT = clothingInInventory_parent
        if dirtySlots_child is not None:
            self._XMLID_TAG_DIRTYSLOTS_CHILD = dirtySlots_child
        if dirtySlots_parent is not None:
            self._XMLID_TAG_DIRTYSLOTS_PARENT = dirtySlots_parent
        if essenceCount_element is not None:
            self._XMLID_TAG_ESSENCECOUNT_ELEMENT = essenceCount_element
        if extraBlockedParts_element is not None:
            self._XMLID_TAG_EXTRABLOCKEDPARTS_ELEMENT = extraBlockedParts_element
        if maxInventorySpace_element is not None:
            self._XMLID_TAG_MAXINVENTORYSPACE_ELEMENT = maxInventorySpace_element
        if money_element is not None:
            self._XMLID_TAG_MONEY_ELEMENT = money_element
        if weaponsInInventory_child is not None:
            self._XMLID_TAG_WEAPONSININVENTORY_CHILD = weaponsInInventory_child
        if weaponsInInventory_keyelem is not None:
            self._XMLID_TAG_WEAPONSININVENTORY_KEYELEM = weaponsInInventory_keyelem
        if weaponsInInventory_parent is not None:
            self._XMLID_TAG_WEAPONSININVENTORY_PARENT = weaponsInInventory_parent

    def _fromXML_unlockKeyMap(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromXML_unlockKeyMap()')

    def _toXML_unlockKeyMap(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toXML_unlockKeyMap()')

    def _fromDict_unlockKeyMap(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromDict_unlockKeyMap()')

    def _toDict_unlockKeyMap(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toDict_unlockKeyMap()')

    def _fromXML_mainWeapons(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromXML_mainWeapons()')

    def _toXML_mainWeapons(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toXML_mainWeapons()')

    def _fromDict_mainWeapons(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromDict_mainWeapons()')

    def _toDict_mainWeapons(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toDict_mainWeapons()')

    def _fromXML_offhandWeapons(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromXML_offhandWeapons()')

    def _toXML_offhandWeapons(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toXML_offhandWeapons()')

    def _fromDict_offhandWeapons(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromDict_offhandWeapons()')

    def _toDict_offhandWeapons(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toDict_offhandWeapons()')

    def _fromXML_itemsInInventory(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromXML_itemsInInventory()')

    def _toXML_itemsInInventory(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toXML_itemsInInventory()')

    def _fromDict_itemsInInventory(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromDict_itemsInInventory()')

    def _toDict_itemsInInventory(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toDict_itemsInInventory()')

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        dc: etree._Element
        df: etree._Element
        dk: Savable
        lk: str
        lv: Savable
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_MAXINVENTORYSPACE_ELEMENT)) is not None:
            if self._XMLID_ATTR_MAXINVENTORYSPACE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_MAXINVENTORYSPACE_ATTRIBUTE, 'maxInventorySpace')
            self.maxInventorySpace = int(sf.attrib[self._XMLID_ATTR_MAXINVENTORYSPACE_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'maxInventorySpace', self._XMLID_TAG_MAXINVENTORYSPACE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_MONEY_ELEMENT)) is not None:
            if self._XMLID_ATTR_MONEY_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_MONEY_ATTRIBUTE, 'money')
            self.money = int(sf.attrib[self._XMLID_ATTR_MONEY_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'money', self._XMLID_TAG_MONEY_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_ESSENCECOUNT_ELEMENT)) is not None:
            if self._XMLID_ATTR_ESSENCECOUNT_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_ESSENCECOUNT_ATTRIBUTE, 'essenceCount')
            self.essenceCount = int(sf.attrib[self._XMLID_ATTR_ESSENCECOUNT_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'essenceCount', self._XMLID_TAG_ESSENCECOUNT_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_EXTRABLOCKEDPARTS_ELEMENT)) is not None:
            self.extraBlockedParts = BlockedParts()
            self.extraBlockedParts.fromXML(sf)
        if (lf := e.find(self._XMLID_TAG_DIRTYSLOTS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_DIRTYSLOTS_CHILD):
                    self.dirtySlots.add(EInventorySlot[lc.attrib[self._XMLID_ATTR_DIRTYSLOTS_VALUEATTR]])
        else:
            self.dirtySlots = set()
        self._fromXML_unlockKeyMap(e)
        self._fromXML_mainWeapons(e)
        self._fromXML_offhandWeapons(e)
        if (lf := e.find(self._XMLID_TAG_CLOTHINGEQUIPPED_PARENT)) is not None:
            if lf is not None:
                for lc in lf:
                    lv = AbstractClothing()
                    lv.fromXML(lc)
                    self.clothingEquipped.append(lv)
        else:
            self.clothingEquipped = list()
        self._fromXML_itemsInInventory(e)
        if (df := e.find(self._XMLID_TAG_CLOTHINGININVENTORY_PARENT)) is not None:
            for dc in df.iterfind(self._XMLID_TAG_CLOTHINGININVENTORY_KEYELEM):
                dk = AbstractClothing()
                dk.fromXML(dc)
                self.clothingInInventory[dk] = int(dc.attrib['count'])
        if (df := e.find(self._XMLID_TAG_WEAPONSININVENTORY_PARENT)) is not None:
            for dc in df.iterfind(self._XMLID_TAG_WEAPONSININVENTORY_KEYELEM):
                dk = AbstractWeapon()
                dk.fromXML(dc)
                self.weaponsInInventory[dk] = int(dc.attrib['count'])

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        dK: Any
        dV: Any
        dc: etree._Element
        dk: Savable
        dp: etree._Element
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        etree.SubElement(e, self._XMLID_TAG_MAXINVENTORYSPACE_ELEMENT, {self._XMLID_ATTR_MAXINVENTORYSPACE_ATTRIBUTE: str(self.maxInventorySpace)})
        etree.SubElement(e, self._XMLID_TAG_MONEY_ELEMENT, {self._XMLID_ATTR_MONEY_ATTRIBUTE: str(self.money)})
        etree.SubElement(e, self._XMLID_TAG_ESSENCECOUNT_ELEMENT, {self._XMLID_ATTR_ESSENCECOUNT_ATTRIBUTE: str(self.essenceCount)})
        if self.extraBlockedParts is not None:
            e.append(self.extraBlockedParts.toXML(self._XMLID_TAG_EXTRABLOCKEDPARTS_ELEMENT))
        lp = etree.SubElement(e, self._XMLID_TAG_DIRTYSLOTS_PARENT)
        for lv in sorted(self.dirtySlots, key=lambda x: x.name):
            etree.SubElement(lp, self._XMLID_TAG_DIRTYSLOTS_CHILD, {self._XMLID_ATTR_DIRTYSLOTS_VALUEATTR: lv.name})
        self._toXML_unlockKeyMap(e)
        self._toXML_mainWeapons(e)
        self._toXML_offhandWeapons(e)
        lp = etree.SubElement(e, self._XMLID_TAG_CLOTHINGEQUIPPED_PARENT)
        for lv in self.clothingEquipped:
            lp.append(lv.toXML())
        self._toXML_itemsInInventory(e)
        if self.clothingInInventory is not None and len(self.clothingInInventory) > 0:
            dp = etree.SubElement(e, self._XMLID_TAG_CLOTHINGININVENTORY_PARENT)
            for dK, dV in sorted(self.clothingInInventory.items(), key=lambda t: (t[0].id, t[0].name)):
                dc = dK.toXML(self._XMLID_TAG_CLOTHINGININVENTORY_KEYELEM)
                dc.attrib['count'] = str(dV)
                dp.append(dc)
        if self.weaponsInInventory is not None and len(self.weaponsInInventory) > 0:
            dp = etree.SubElement(e, self._XMLID_TAG_WEAPONSININVENTORY_PARENT)
            for dK, dV in sorted(self.weaponsInInventory.items(), key=lambda t: (t[0].id, t[0].name)):
                dc = dK.toXML(self._XMLID_TAG_WEAPONSININVENTORY_KEYELEM)
                dc.attrib['count'] = str(dV)
                dp.append(dc)
        return e

    def toDict(self) -> Dict[str, Any]:
        dK: Any
        dV: Any
        data: Dict[str, Any] = super().toDict()
        dk: Savable
        dp: Dict[str, Any]
        data['maxInventorySpace'] = int(self.maxInventorySpace)
        data['money'] = int(self.money)
        data['essenceCount'] = int(self.essenceCount)
        if self.extraBlockedParts is not None:
            data['extraBlockedParts'] = self.extraBlockedParts.toDict()
        if self.dirtySlots is None or len(self.dirtySlots) > 0:
            data['dirtySlots'] = list()
        else:
            lc = list()
            if len(self.dirtySlots) > 0:
                for lv in sorted(lc, lambda x: x.name):
                    lc.append(lv.name)
            data['dirtySlots'] = lc
        self._toDict_unlockKeyMap(data)
        self._toDict_mainWeapons(data)
        self._toDict_offhandWeapons(data)
        if self.clothingEquipped is None or len(self.clothingEquipped) > 0:
            data['clothingEquipped'] = list()
        else:
            lc = list()
            if len(self.clothingEquipped) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['clothingEquipped'] = lc
        self._toDict_itemsInInventory(data)
        if self.clothingInInventory is not None and len(self.clothingInInventory) > 0:
            dp = []
            for dK, dV in sorted(self.clothingInInventory.items(), key=lambda t: (t[0].id, t[0].name)):
                sv = dV
                dp.append([dK.toDict(), sv])
            data['clothingInInventory'] = dp
        if self.weaponsInInventory is not None and len(self.weaponsInInventory) > 0:
            dp = []
            for dK, dV in sorted(self.weaponsInInventory.items(), key=lambda t: (t[0].id, t[0].name)):
                sv = dV
                dp.append([dK.toDict(), sv])
            data['weaponsInInventory'] = dp
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('maxInventorySpace')) is not None:
            self.maxInventorySpace = data['maxInventorySpace']
        else:
            raise KeyRequiredException(self, data, 'maxInventorySpace', 'maxInventorySpace')
        if (sf := data.get('money')) is not None:
            self.money = data['money']
        else:
            raise KeyRequiredException(self, data, 'money', 'money')
        if (sf := data.get('essenceCount')) is not None:
            self.essenceCount = data['essenceCount']
        else:
            raise KeyRequiredException(self, data, 'essenceCount', 'essenceCount')
        if (sf := data.get('extraBlockedParts')) is not None:
            self.extraBlockedParts = BlockedParts()
            self.extraBlockedParts.fromDict(data['extraBlockedParts'])
        else:
            self.extraBlockedParts = None
        if (lv := self.dirtySlots) is not None:
            for le in lv:
                self.dirtySlots.add(le.name)
        else:
            raise KeyRequiredException(self, data, 'dirtySlots', 'dirtySlots')
        self._fromDict_unlockKeyMap(data)
        self._fromDict_mainWeapons(data)
        self._fromDict_offhandWeapons(data)
        if (lv := self.clothingEquipped) is not None:
            for le in lv:
                self.clothingEquipped.append(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'clothingEquipped', 'clothingEquipped')
        self._fromDict_itemsInInventory(data)
        if (df := data.get('clothingInInventory')) is not None:
            for sk, sv in df:
                dK = AbstractClothing()
                dK.fromDict(sk)
                self.clothingInInventory[dK] = sv
        if (df := data.get('weaponsInInventory')) is not None:
            for sk, sv in df:
                dK = AbstractWeapon()
                dK.fromDict(sk)
                self.weaponsInInventory[dK] = sv

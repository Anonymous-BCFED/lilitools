
from lilitools.saves.inventory._savables.blocked_parts import RawBlockedParts


class BlockedParts(RawBlockedParts):
    def clone(self) -> 'BlockedParts':
        o = BlockedParts()
        o.displacementType = self.displacementType
        o.clothingAccessRequired = {x for x in self.clothingAccessRequired}
        o.blockedBodyParts = {x for x in self.blockedBodyParts}
        o.clothingAccessBlocked = {x for x in self.clothingAccessBlocked}
        o.concealedSlots = {x for x in self.concealedSlots}
        return o

from typing import Any, Dict, List

from lxml import etree

from lilitools.saves.inventory._savables.character_inventory import (
    RawCharacterInventory,
)
from lilitools.saves.inventoryslot import EInventorySlot
from lilitools.saves.items.abstract_clothing import AbstractClothing
from lilitools.saves.items.abstract_item import AbstractItem
from lilitools.saves.items.abstract_weapon import AbstractWeapon
from lilitools.saves.items.itemtypes import ITEMTYPES_BY_ID


class CharacterInventory(RawCharacterInventory):

    def __init__(self) -> None:
        super().__init__()
        self.unlockKeyMap: Dict[str, List[EInventorySlot]] = {}

        self.mainWeapons: List[AbstractWeapon] = []
        self.offhandWeapons: List[AbstractWeapon] = []

        # self.clothingEquipped: List[AbstractClothing] = []

        self.itemsInInventory: Dict[AbstractItem, int] = {}
        self.clothingInInventory: Dict[AbstractClothing, int] = {}
        self.weaponsInInventory: Dict[AbstractWeapon, int] = {}

    def clone(self) -> "CharacterInventory":
        o = CharacterInventory()
        o.maxInventorySpace = self.maxInventorySpace
        o.money = self.money
        o.essenceCount = self.essenceCount
        o.extraBlockedParts = (
            self.extraBlockedParts.clone() if self.extraBlockedParts else None
        )
        o.dirtySlots = {x for x in self.dirtySlots}
        o.unlockKeyMap = {}
        for k, v in self.unlockKeyMap.items():
            o.unlockKeyMap[k] = [x for x in v]
        o.mainWeapons = [x.clone() for x in self.mainWeapons]
        o.offhandWeapons = [x.clone() for x in self.offhandWeapons]
        o.clothingEquipped = [x.clone() for x in self.clothingEquipped]
        o.itemsInInventory = {i.clone(): c for i, c in self.itemsInInventory.items()}
        o.clothingInInventory = {
            C.clone(): c for C, c in self.clothingInInventory.items()
        }
        o.weaponsInInventory = {
            w.clone(): c for w, c in self.weaponsInInventory.items()
        }
        o.clothingEquipped = [c.clone() for c in self.clothingEquipped]
        return o

    def isEmpty(self) -> bool:
        if len(self.mainWeapons):
            if any([(slot is not None) for slot in self.mainWeapons]):
                return False
        if len(self.offhandWeapons):
            if any([(slot is not None) for slot in self.offhandWeapons]):
                return False
        if (
            self.money > 0
            or self.essenceCount > 0
            or len(self.clothingEquipped) > 0
            or len(self.itemsInInventory) > 0
            or len(self.clothingInInventory) > 0
            or len(self.weaponsInInventory) > 0
            or len(self.dirtySlots) > 0
        ):
            return False
        return True

    def _fromXML_mainWeapons(self, e: etree._Element) -> None:
        pass

    def _toXML_mainWeapons(self, e: etree._Element) -> None:
        i: int
        v: AbstractWeapon
        for i, v in enumerate(
            sorted(self.mainWeapons, key=lambda x: (x.id, getattr(x, "name", "")))
        ):
            etree.SubElement(e, f"mainWeapon{i}").append(v.toXML())

    def _fromDict_mainWeapons(self, data: Dict[str, Any]) -> None:
        for mainWeapon in data["mainWeapons"]:
            w = AbstractWeapon()
            w.fromDict(mainWeapon)
            self.mainWeapons.append(w)

    def _toDict_mainWeapons(self, data: Dict[str, Any]) -> None:
        data["mainWeapons"] = [w.toDict() for w in self.mainWeapons]

    def _fromXML_offhandWeapons(self, e: etree._Element) -> None:
        pass

    def _toXML_offhandWeapons(self, e: etree._Element) -> None:
        i: int
        v: AbstractWeapon
        for i, v in enumerate(
            sorted(self.offhandWeapons, key=lambda x: (x.id, getattr(x, "name", "")))
        ):
            etree.SubElement(e, f"offhandWeapon{i}").append(v.toXML())

    def _fromDict_offhandWeapons(self, data: Dict[str, Any]) -> None:
        for offhandWeapon in data["offhandWeapons"]:
            w = AbstractWeapon()
            w.fromDict(offhandWeapon)
            self.offhandWeapons.append(w)

    def _toDict_offhandWeapons(self, data: Dict[str, Any]) -> None:
        data["offhandWeapons"] = [w.toDict() for w in self.offhandWeapons]

    def _fromXML_unlockKeyMap(self, e: etree._Element) -> None:
        self.unlockKeyMap = {}
        if (ulkm := e.find("unlockKeyMap")) is not None:
            for ce in ulkm.findall("character"):
                charid = ce.attrib["id"]
                charSlots: List[EInventorySlot] = []
                for slot in ce.findall("slot"):
                    charSlots.append(EInventorySlot[slot.attrib["id"]])
                self.unlockKeyMap[charid] = charSlots

    def _toXML_unlockKeyMap(self, e: etree._Element) -> None:
        if len(self.unlockKeyMap):
            ukme = etree.SubElement(e, "unlockKeyMap")
            for charID, slots in sorted(self.unlockKeyMap.items(), key=lambda t: t[0]):
                char = etree.SubElement(ukme, "character", {"id": charID})
                for slot in sorted(slots, key=lambda s: s.name):
                    etree.SubElement(char, "slot", {"id": slot.name})

    def _toDict_unlockKeyMap(self, data: Dict[str, Any]) -> None:
        if len(self.unlockKeyMap):
            o = {}
            for charID, slots in sorted(self.unlockKeyMap.items(), key=lambda t: t[0]):
                charslots = []
                for slot in sorted(slots, key=lambda s: s.name):
                    charslots.append(slot.name)
                o[charID] = charslots
            data["unlockKeyMap"] = o

    def _fromDict_unlockKeyMap(self, data: Dict[str, Any]) -> None:
        self.unlockKeyMap = {}
        if "unlockKeyMap" in data:
            for charID, slots in data["unlockKeyMap"].items():
                self.unlockKeyMap[charID] = [EInventorySlot[x] for x in slots]

    def fromXML(self, e: etree._Element) -> None:
        super().fromXML(e)
        mainWeaps = []
        offhandWeaps = []
        subEl: etree._Element
        for subEl in e.getchildren():
            if subEl.tag.startswith("mainWeapon"):
                n = int(subEl.tag[10:])
                w = AbstractWeapon()
                w.fromXML(subEl[0])
                mainWeaps += [(n, w)]
            if subEl.tag.startswith("offhandWeapon"):
                n = int(subEl.tag[13:])
                w = AbstractWeapon()
                w.fromXML(subEl[0])
                offhandWeaps += [(n, w)]
        self.mainWeapons = [None] * len(mainWeaps)
        if len(mainWeaps):
            for idx, w in mainWeaps:
                self.mainWeapons[idx] = w
        self.offhandWeapons = [None] * len(offhandWeaps)
        if len(offhandWeaps):
            for idx, w in offhandWeaps:
                self.offhandWeapons[idx] = w

    def _fromXML_itemsInInventory(self, e: etree._Element) -> None:
        if (df := e.find("itemsInInventory")) is not None:
            for dc in df.findall("item"):
                dk: AbstractItem = ITEMTYPES_BY_ID.get(dc.attrib["id"], AbstractItem)()
                dk.fromXML(dc)
                self.itemsInInventory[dk] = int(dc.attrib["count"])

    def _toXML_itemsInInventory(self, e: etree._Element) -> None:
        if self.itemsInInventory is not None and len(self.itemsInInventory) > 0:
            dp = etree.SubElement(e, "itemsInInventory")
            for dK, dV in sorted(
                self.itemsInInventory.items(),
                key=lambda t: (t[0].id, getattr(t[0], "name", "")),
            ):
                dc = dK.toXML("item")
                if (id := getattr(dK, "ID", None)) is not None:
                    dc.attrib["id"] = id
                dc.attrib["count"] = str(dV)
                dp.append(dc)

    def _fromDict_itemsInInventory(self, data: Dict[str, Any]) -> None:
        if (inv := data.get("itemsInInventory")) is not None:
            for item in inv:
                dk: AbstractItem = ITEMTYPES_BY_ID.get(item["id"], AbstractItem)()
                dk.fromDict(item)
                self.itemsInInventory[dk] = int(item["count"])

    def _toDict_itemsInInventory(self, data: Dict[str, Any]) -> None:
        if len(self.itemsInInventory):
            o = []
            for item, count in self.itemsInInventory.items():
                itemdata = item.toDict()
                itemdata["count"] = count
                o.append(itemdata)
            data["itemsInInventory"] = o

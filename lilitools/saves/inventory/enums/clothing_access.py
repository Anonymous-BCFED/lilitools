from enum import IntEnum
__all__ = ['EClothingAccess']


class EClothingAccess(IntEnum):
    EYES = 0
    MOUTH = 1
    HEAD = 2
    ARMS_UP_TO_SHOULDER = 3
    WRISTS = 4
    FINGERS = 5
    CHEST = 6
    WAIST = 7
    LEGS_UP_TO_GROIN_LOW_LEVEL = 8
    LEGS_UP_TO_GROIN = 9
    GROIN = 10
    PENIS = 11
    VAGINA = 12
    ANUS = 13
    CALVES = 14
    FEET = 15

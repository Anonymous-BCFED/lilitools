from enum import IntEnum
__all__ = ['EDisplacementType']


class EDisplacementType(IntEnum):
    REMOVE_OR_EQUIP = 0
    OPEN = 1
    PULLS_UP = 2
    PULLS_DOWN = 3
    PULLS_OUT = 4
    PULLS_OFF = 5
    SHIFTS_ASIDE = 6
    UNZIPS = 7
    UNTIE = 8
    UNBUCKLE = 9
    UNBUTTONS = 10

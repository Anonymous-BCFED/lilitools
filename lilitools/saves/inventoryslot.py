from enum import Enum


class _InventorySlot:
    def __init__(self, layer: int, name: str, plural: bool, core: bool, jewelry: bool, tattooName: str) -> None:
        self.layer: int = layer
        self.name: str = name
        self.plural: bool = plural
        self.core: bool = core
        self.jewelry: bool = jewelry
        self.tattooName: str = tattooName

class EInventorySlot(Enum):
    HEAD = _InventorySlot(40, "head", False, False, False, "head")
    EYES = _InventorySlot(50, "eyes", True, False, False, "upper face")
    HAIR = _InventorySlot(20, "hair", False, False, False, "ears")
    MOUTH = _InventorySlot(10, "mouth", False, False, False, "lower face")
    NECK = _InventorySlot(30, "neck", False, False, False, "neck")
    TORSO_OVER = _InventorySlot(50, "over-torso", False, False, False, "upper back")
    TORSO_UNDER = _InventorySlot(40, "torso", False, True, False, "lower back")
    CHEST = _InventorySlot(10, "chest", False, True, False, "chest")
    NIPPLE = _InventorySlot(5, "nipples", False, False, False, "nipples")
    STOMACH = _InventorySlot(10, "stomach", False, False, False, "stomach")
    HAND = _InventorySlot(20, "hands", True, False, False, "forearms")
    WRIST = _InventorySlot(30, "wrists", True, False, False, "upper arms")
    FINGER = _InventorySlot(30, "fingers", True, False, False, "hands")
    HIPS = _InventorySlot(40, "hips", True, False, False, "hips")
    ANUS = _InventorySlot(0, "anus", False, False, False, "ass")
    LEG = _InventorySlot(30, "legs", True, True, False, "upper leg")
    GROIN = _InventorySlot(10, "groin", False, True, False, "lower abdomen")
    FOOT = _InventorySlot(40, "feet", True, True, False, "feet")
    SOCK = _InventorySlot(10, "calves", True, True, False, "lower leg")
    ANKLE = _InventorySlot(50, "ankles", True, False, False, "ankles")
    HORNS = _InventorySlot(50, "horns", True, False, False, "horns")
    WINGS = _InventorySlot(50, "wings", True, False, False, "wings")
    TAIL = _InventorySlot(50, "tail", False, False, False, "tail")
    PENIS = _InventorySlot(0, "penis", False, False, False, "penis")
    VAGINA = _InventorySlot(0, "vagina", False, False, False, "vagina")
    PIERCING_EAR = _InventorySlot(0, "ear piercing", False, False, True, None)
    PIERCING_NOSE = _InventorySlot(0, "nose piercing", False, False, True, None)
    PIERCING_TONGUE = _InventorySlot(0, "tongue piercing", False, False, True, None)
    PIERCING_LIP = _InventorySlot(0, "lip piercing", False, False, True, None)
    PIERCING_STOMACH = _InventorySlot(0, "navel piercing", False, False, True, None)
    PIERCING_NIPPLE = _InventorySlot(0, "nipple piercing", False, False, True, None)
    PIERCING_VAGINA = _InventorySlot(0, "vaginal piercing", False, False, True, None)
    PIERCING_PENIS = _InventorySlot(0, "cock piercing", False, False, True, None)
    WEAPON_MAIN_1 = _InventorySlot(0, "primary weapon", False, False, False, None)
    WEAPON_MAIN_2 = _InventorySlot(0, "primary weapon (2nd)", False, False, False, None)
    WEAPON_MAIN_3 = _InventorySlot(0, "primary weapon (3rd)", False, False, False, None)
    WEAPON_OFFHAND_1 = _InventorySlot(0, "secondary weapon", False, False, False, None)
    WEAPON_OFFHAND_2 = _InventorySlot(0, "secondary weapon (2nd)", False, False, False, None)
    WEAPON_OFFHAND_3 = _InventorySlot(0, "secondary weapon (3rd)", False, False, False, None)
#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, List, Optional, Tuple
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.items.abstract_pattern import AbstractPattern
from lilitools.saves.items.item_effect import ItemEffect
from lilitools.saves.savable import XML2BOOL, Savable
__all__ = ['RawAbstractClothing']
## from [LT]/src/com/lilithsthrone/game/inventory/clothing/AbstractClothing.java: public Element saveAsXML(Element parentElement, Document doc) { @ mTnXs98CbmdaqTlQEYCnsi/DC5dpGjd792fz5hFwyHqonQqACVhEgpTSihyB17MHNPk4UaXFgp6fl5N74XFwfA==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawAbstractClothing(Savable):
    TAG = 'clothing'
    _XMLID_ATTR_COLOURS_INDEX: str = 'i'
    _XMLID_ATTR_DISPLACEDLIST_VALUEATTR: str = 'value'
    _XMLID_ATTR_ENCHANTMENTKNOWN_ATTRIBUTE: str = 'enchantmentKnown'
    _XMLID_ATTR_ID_ATTRIBUTE: str = 'id'
    _XMLID_ATTR_ISDIRTY_ATTRIBUTE: str = 'isDirty'
    _XMLID_ATTR_NAME_ATTRIBUTE: str = 'name'
    _XMLID_ATTR_PATTERN_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_SEALED_ATTRIBUTE: str = 'sealed'
    _XMLID_ATTR_SLOTEQUIPPEDTO_ATTRIBUTE: str = 'slotEquippedTo'
    _XMLID_ATTR_STICKERS_KEYATTR: str = 'category'
    _XMLID_TAG_COLOURS_CHILD: str = 'colour'
    _XMLID_TAG_COLOURS_PARENT: str = 'colours'
    _XMLID_TAG_DISPLACEDLIST_CHILD: str = 'displacementType'
    _XMLID_TAG_DISPLACEDLIST_PARENT: str = 'displacedList'
    _XMLID_TAG_EFFECTS_PARENT: str = 'effects'
    _XMLID_TAG_EFFECTS_VALUEELEM: Optional[str] = None
    _XMLID_TAG_PATTERN_ELEMENT: str = 'pattern'
    _XMLID_TAG_STICKERS_CHILD: str = 'sticker'
    _XMLID_TAG_STICKERS_PARENT: str = 'stickers'

    def __init__(self) -> None:
        super().__init__()
        self.id: str = ''
        self.name: str = ''
        self.slotEquippedTo: Optional[str] = None
        self.sealed: bool = False
        self.isDirty: bool = False
        self.enchantmentKnown: bool = False
        self.colours: Optional[List[str]] = list()
        self.pattern: Optional[AbstractPattern] = None  # Element
        self.effects: Optional[List[ItemEffect]] = list()
        self.stickers: Dict[str, str] = {}
        self.displacedList: Optional[List[str]] = list()

    def overrideAttrs(self, colours_index: _Optional_str = None, displacedList_valueattr: _Optional_str = None, enchantmentKnown_attribute: _Optional_str = None, id_attribute: _Optional_str = None, isDirty_attribute: _Optional_str = None, name_attribute: _Optional_str = None, pattern_attribute: _Optional_str = None, sealed_attribute: _Optional_str = None, slotEquippedTo_attribute: _Optional_str = None, stickers_keyattr: _Optional_str = None) -> None:
        if colours_index is not None:
            self._XMLID_ATTR_COLOURS_INDEX = colours_index
        if displacedList_valueattr is not None:
            self._XMLID_ATTR_DISPLACEDLIST_VALUEATTR = displacedList_valueattr
        if enchantmentKnown_attribute is not None:
            self._XMLID_ATTR_ENCHANTMENTKNOWN_ATTRIBUTE = enchantmentKnown_attribute
        if id_attribute is not None:
            self._XMLID_ATTR_ID_ATTRIBUTE = id_attribute
        if isDirty_attribute is not None:
            self._XMLID_ATTR_ISDIRTY_ATTRIBUTE = isDirty_attribute
        if name_attribute is not None:
            self._XMLID_ATTR_NAME_ATTRIBUTE = name_attribute
        if pattern_attribute is not None:
            self._XMLID_ATTR_PATTERN_ATTRIBUTE = pattern_attribute
        if sealed_attribute is not None:
            self._XMLID_ATTR_SEALED_ATTRIBUTE = sealed_attribute
        if slotEquippedTo_attribute is not None:
            self._XMLID_ATTR_SLOTEQUIPPEDTO_ATTRIBUTE = slotEquippedTo_attribute
        if stickers_keyattr is not None:
            self._XMLID_ATTR_STICKERS_KEYATTR = stickers_keyattr

    def overrideTags(self, colours_child: _Optional_str = None, colours_parent: _Optional_str = None, displacedList_child: _Optional_str = None, displacedList_parent: _Optional_str = None, effects_parent: _Optional_str = None, effects_valueelem: _Optional_str = None, pattern_element: _Optional_str = None, stickers_child: _Optional_str = None, stickers_parent: _Optional_str = None) -> None:
        if colours_child is not None:
            self._XMLID_TAG_COLOURS_CHILD = colours_child
        if colours_parent is not None:
            self._XMLID_TAG_COLOURS_PARENT = colours_parent
        if displacedList_child is not None:
            self._XMLID_TAG_DISPLACEDLIST_CHILD = displacedList_child
        if displacedList_parent is not None:
            self._XMLID_TAG_DISPLACEDLIST_PARENT = displacedList_parent
        if effects_parent is not None:
            self._XMLID_TAG_EFFECTS_PARENT = effects_parent
        if effects_valueelem is not None:
            self._XMLID_TAG_EFFECTS_VALUEELEM = effects_valueelem
        if pattern_element is not None:
            self._XMLID_TAG_PATTERN_ELEMENT = pattern_element
        if stickers_child is not None:
            self._XMLID_TAG_STICKERS_CHILD = stickers_child
        if stickers_parent is not None:
            self._XMLID_TAG_STICKERS_PARENT = stickers_parent

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        dc: etree._Element
        df: etree._Element
        li: int
        lk: str
        lp: etree._Element
        lv: Savable
        sf: etree._Element
        value = e.get(self._XMLID_ATTR_ID_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_ID_ATTRIBUTE, 'id')
        else:
            self.id = str(value)
        value = e.get(self._XMLID_ATTR_NAME_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_NAME_ATTRIBUTE, 'name')
        else:
            self.name = str(value)
        value = e.get(self._XMLID_ATTR_SLOTEQUIPPEDTO_ATTRIBUTE, None)
        if value is None:
            self.slotEquippedTo = None
        else:
            self.slotEquippedTo = str(value)
        value = e.get(self._XMLID_ATTR_SEALED_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_SEALED_ATTRIBUTE, 'sealed')
        else:
            self.sealed = XML2BOOL[value]
        value = e.get(self._XMLID_ATTR_ISDIRTY_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_ISDIRTY_ATTRIBUTE, 'isDirty')
        else:
            self.isDirty = XML2BOOL[value]
        value = e.get(self._XMLID_ATTR_ENCHANTMENTKNOWN_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_ENCHANTMENTKNOWN_ATTRIBUTE, 'enchantmentKnown')
        else:
            self.enchantmentKnown = XML2BOOL[value]
        if (lf := e.find(self._XMLID_TAG_COLOURS_PARENT)) is not None:
            tmp_colours: List[Tuple[int, str]] = []
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_COLOURS_CHILD):
                    li = int(lc.attrib[self._XMLID_ATTR_COLOURS_INDEX])
                    tmp_colours.append((li, lc.text))
            self.colours = [None] * len(tmp_colours)
            for li, lv in tmp_colours:
                self.colours[li] = lv
        if (sf := e.find(self._XMLID_TAG_PATTERN_ELEMENT)) is not None:
            self.pattern = AbstractPattern()
            self.pattern.fromXML(sf)
        if (lf := e.find(self._XMLID_TAG_EFFECTS_PARENT)) is not None:
            if lf is not None:
                for lc in lf:
                    lv = ItemEffect()
                    lv.fromXML(lc)
                    self.effects.append(lv)
        if (df := e.find(self._XMLID_TAG_STICKERS_PARENT)) is not None:
            for dc in df.iterfind(self._XMLID_TAG_STICKERS_CHILD):
                self.stickers[dc.attrib['category']] = dc.text
        if (lf := e.find(self._XMLID_TAG_DISPLACEDLIST_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_DISPLACEDLIST_CHILD):
                    self.displacedList.append(lc.attrib[self._XMLID_ATTR_DISPLACEDLIST_VALUEATTR])

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        dK: Any
        dV: Any
        dc: etree._Element
        dp: etree._Element
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        li: int
        lp: etree._Element
        e.attrib[self._XMLID_ATTR_ID_ATTRIBUTE] = str(self.id)
        e.attrib[self._XMLID_ATTR_NAME_ATTRIBUTE] = str(self.name)
        if self.slotEquippedTo is not None:
            e.attrib[self._XMLID_ATTR_SLOTEQUIPPEDTO_ATTRIBUTE] = str(self.slotEquippedTo)
        e.attrib[self._XMLID_ATTR_SEALED_ATTRIBUTE] = str(self.sealed).lower()
        e.attrib[self._XMLID_ATTR_ISDIRTY_ATTRIBUTE] = str(self.isDirty).lower()
        e.attrib[self._XMLID_ATTR_ENCHANTMENTKNOWN_ATTRIBUTE] = str(self.enchantmentKnown).lower()
        if self.colours is not None and len(self.colours) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_COLOURS_PARENT)
            for li, lv in enumerate(self.colours):
                etree.SubElement(lp, self._XMLID_TAG_COLOURS_CHILD, {self._XMLID_ATTR_COLOURS_INDEX: str(li)}).text = lv
        if self.pattern is not None:
            e.append(self.pattern.toXML(self._XMLID_TAG_PATTERN_ELEMENT))
        if self.effects is not None and len(self.effects) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_EFFECTS_PARENT)
            for lv in self.effects:
                lp.append(lv.toXML())
        if self.stickers is not None and len(self.stickers) > 0:
            dp = etree.SubElement(e, self._XMLID_TAG_STICKERS_PARENT)
            for dK, dV in sorted(self.stickers.items(), key=lambda t: t[0]):
                dc = etree.SubElement(dp, self._XMLID_TAG_STICKERS_CHILD)
                dc.attrib['category'] = dK
                dc.text = dV
        if self.displacedList is not None and len(self.displacedList) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_DISPLACEDLIST_PARENT)
            for lv in self.displacedList:
                etree.SubElement(lp, self._XMLID_TAG_DISPLACEDLIST_CHILD, {self._XMLID_ATTR_DISPLACEDLIST_VALUEATTR: lv})
        return e

    def toDict(self) -> Dict[str, Any]:
        dK: Any
        dV: Any
        data: Dict[str, Any] = super().toDict()
        dp: Dict[str, Any]
        data['id'] = self.id
        data['name'] = self.name
        if self.slotEquippedTo is not None:
            data['slotEquippedTo'] = self.slotEquippedTo
        data['sealed'] = bool(self.sealed)
        data['isDirty'] = bool(self.isDirty)
        data['enchantmentKnown'] = bool(self.enchantmentKnown)
        if self.colours is None or len(self.colours) > 0:
            data['colours'] = list()
        else:
            lc = list()
            if len(self.colours) > 0:
                for lv in lc:
                    lc.append(str(lv))
            data['colours'] = lc
        if self.pattern is not None:
            data['pattern'] = self.pattern.toDict()
        if self.effects is None or len(self.effects) > 0:
            data['effects'] = list()
        else:
            lc = list()
            if len(self.effects) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['effects'] = lc
        if self.stickers is not None and len(self.stickers) > 0:
            dp = {}
            for dK, dV in sorted(self.stickers.items(), key=lambda t: t[0]):
                sk = dK
                sv = dV
                dp[sk] = sv
            data['stickers'] = dp
        if self.displacedList is None or len(self.displacedList) > 0:
            data['displacedList'] = list()
        else:
            lc = list()
            if len(self.displacedList) > 0:
                for lv in lc:
                    lc.append(str(lv))
            data['displacedList'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'id' not in data:
            raise KeyRequiredException(self, data, 'id', 'id')
        self.id = str(data['id'])
        if 'name' not in data:
            raise KeyRequiredException(self, data, 'name', 'name')
        self.name = str(data['name'])
        self.slotEquippedTo = str(data['slotEquippedTo']) if 'slotEquippedTo' in data else None
        if 'sealed' not in data:
            raise KeyRequiredException(self, data, 'sealed', 'sealed')
        self.sealed = bool(data['sealed'])
        if 'isDirty' not in data:
            raise KeyRequiredException(self, data, 'isDirty', 'isDirty')
        self.isDirty = bool(data['isDirty'])
        if 'enchantmentKnown' not in data:
            raise KeyRequiredException(self, data, 'enchantmentKnown', 'enchantmentKnown')
        self.enchantmentKnown = bool(data['enchantmentKnown'])
        if (lv := self.colours) is not None:
            for le in lv:
                self.colours.append(str(le))
        else:
            self.colours = list()
        if (sf := data.get('pattern')) is not None:
            self.pattern = AbstractPattern()
            self.pattern.fromDict(data['pattern'])
        else:
            self.pattern = None
        if (lv := self.effects) is not None:
            for le in lv:
                self.effects.append(le.toDict())
        else:
            self.effects = list()
        if (df := data.get('stickers')) is not None:
            for sk, sv in df.items():
                self.stickers[sk] = sv
        if (lv := self.displacedList) is not None:
            for le in lv:
                self.displacedList.append(str(le))
        else:
            self.displacedList = list()

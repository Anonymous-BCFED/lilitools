#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, List, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.items.item_effect import ItemEffect
from lilitools.saves.savable import Savable
__all__ = ['RawAbstractItem']
## from [LT]/src/com/lilithsthrone/game/inventory/item/AbstractItem.java: public Element saveAsXML(Element parentElement, Document doc) { @ e1YFY8BztYTgw28WCcTtAl/CnUuknYf39wtZAuHi9JuAO9zWMFKMeU3u/3XZYLyO35WNMjTo26XEO45oW7c/Rg==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawAbstractItem(Savable):
    TAG = 'item'
    _XMLID_ATTR_COLOUR_ATTRIBUTE: str = 'colour'
    _XMLID_ATTR_ID_ATTRIBUTE: str = 'id'
    _XMLID_ATTR_NAME_ATTRIBUTE: str = 'name'
    _XMLID_TAG_ITEMEFFECTS_CHILD: str = 'effect'
    _XMLID_TAG_ITEMEFFECTS_PARENT: str = 'itemEffects'
    _XMLID_TAG_ITEMEFFECTS_VALUEELEM: Optional[str] = None

    def __init__(self) -> None:
        super().__init__()
        self.id: str = ''
        self.name: str = ''
        self.colour: Optional[str] = None
        self.itemEffects: List[ItemEffect] = list()

    def overrideAttrs(self, colour_attribute: _Optional_str = None, id_attribute: _Optional_str = None, name_attribute: _Optional_str = None) -> None:
        if colour_attribute is not None:
            self._XMLID_ATTR_COLOUR_ATTRIBUTE = colour_attribute
        if id_attribute is not None:
            self._XMLID_ATTR_ID_ATTRIBUTE = id_attribute
        if name_attribute is not None:
            self._XMLID_ATTR_NAME_ATTRIBUTE = name_attribute

    def overrideTags(self, itemEffects_child: _Optional_str = None, itemEffects_parent: _Optional_str = None, itemEffects_valueelem: _Optional_str = None) -> None:
        if itemEffects_child is not None:
            self._XMLID_TAG_ITEMEFFECTS_CHILD = itemEffects_child
        if itemEffects_parent is not None:
            self._XMLID_TAG_ITEMEFFECTS_PARENT = itemEffects_parent
        if itemEffects_valueelem is not None:
            self._XMLID_TAG_ITEMEFFECTS_VALUEELEM = itemEffects_valueelem

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        lv: Savable
        value = e.get(self._XMLID_ATTR_ID_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_ID_ATTRIBUTE, 'id')
        else:
            self.id = str(value)
        value = e.get(self._XMLID_ATTR_NAME_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_NAME_ATTRIBUTE, 'name')
        else:
            self.name = str(value)
        value = e.get(self._XMLID_ATTR_COLOUR_ATTRIBUTE, None)
        if value is None:
            self.colour = None
        else:
            self.colour = str(value)
        if (lf := e.find(self._XMLID_TAG_ITEMEFFECTS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_ITEMEFFECTS_CHILD):
                    lv = ItemEffect()
                    lv.fromXML(lc)
                    self.itemEffects.append(lv)
        else:
            self.itemEffects = list()

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        e.attrib[self._XMLID_ATTR_ID_ATTRIBUTE] = str(self.id)
        e.attrib[self._XMLID_ATTR_NAME_ATTRIBUTE] = str(self.name)
        if self.colour is not None:
            e.attrib[self._XMLID_ATTR_COLOUR_ATTRIBUTE] = str(self.colour)
        lp = etree.SubElement(e, self._XMLID_TAG_ITEMEFFECTS_PARENT)
        for lv in self.itemEffects:
            lp.append(lv.toXML())
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['id'] = self.id
        data['name'] = self.name
        if self.colour is not None:
            data['colour'] = self.colour
        if self.itemEffects is None or len(self.itemEffects) > 0:
            data['itemEffects'] = list()
        else:
            lc = list()
            if len(self.itemEffects) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['itemEffects'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'id' not in data:
            raise KeyRequiredException(self, data, 'id', 'id')
        self.id = str(data['id'])
        if 'name' not in data:
            raise KeyRequiredException(self, data, 'name', 'name')
        self.name = str(data['name'])
        self.colour = str(data['colour']) if 'colour' in data else None
        if (lv := self.itemEffects) is not None:
            for le in lv:
                self.itemEffects.append(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'itemEffects', 'itemEffects')

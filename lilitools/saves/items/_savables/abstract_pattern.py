#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, List, Optional, Tuple
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawAbstractPattern']
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawAbstractPattern(Savable):
    TAG = 'pattern'
    _XMLID_ATTR_COLOURS_INDEX: str = 'i'
    _XMLID_ATTR_ID_ATTRIBUTE: str = 'id'
    _XMLID_TAG_COLOURS_CHILD: str = 'colour'

    def __init__(self) -> None:
        super().__init__()
        self.id: str = ''
        self.colours: Optional[List[str]] = list()

    def overrideAttrs(self, colours_index: _Optional_str = None, id_attribute: _Optional_str = None) -> None:
        if colours_index is not None:
            self._XMLID_ATTR_COLOURS_INDEX = colours_index
        if id_attribute is not None:
            self._XMLID_ATTR_ID_ATTRIBUTE = id_attribute

    def overrideTags(self, colours_child: _Optional_str = None) -> None:
        if colours_child is not None:
            self._XMLID_TAG_COLOURS_CHILD = colours_child

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        li: int
        lk: str
        value = e.get(self._XMLID_ATTR_ID_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_ID_ATTRIBUTE, 'id')
        else:
            self.id = str(value)
        tmp_colours: List[Tuple[int, str]] = []
        for lc in e.iterfind(self._XMLID_TAG_COLOURS_CHILD):
            li = int(lc.attrib[self._XMLID_ATTR_COLOURS_INDEX])
            tmp_colours.append((li, lc.text))
        self.colours = [None] * len(tmp_colours)
        for li, lv in tmp_colours:
            self.colours[li] = lv

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        li: int
        lp: etree._Element
        e.attrib[self._XMLID_ATTR_ID_ATTRIBUTE] = str(self.id)
        if self.colours is not None and len(self.colours) > 0:
            for li, lv in enumerate(self.colours):
                etree.SubElement(e, self._XMLID_TAG_COLOURS_CHILD, {self._XMLID_ATTR_COLOURS_INDEX: str(li)}).text = lv
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['id'] = self.id
        if self.colours is None or len(self.colours) > 0:
            data['colours'] = list()
        else:
            lc = list()
            if len(self.colours) > 0:
                for lv in lc:
                    lc.append(str(lv))
            data['colours'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'id' not in data:
            raise KeyRequiredException(self, data, 'id', 'id')
        self.id = str(data['id'])
        if (lv := self.colours) is not None:
            for le in lv:
                self.colours.append(str(le))
        else:
            self.colours = list()

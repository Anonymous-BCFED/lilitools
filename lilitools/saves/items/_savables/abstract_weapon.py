#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, List, Optional, Tuple
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.items.item_effect import ItemEffect
from lilitools.saves.savable import Savable
__all__ = ['RawAbstractWeapon']
## from [LT]/src/com/lilithsthrone/game/inventory/weapon/AbstractWeapon.java: public Element saveAsXML(Element parentElement, Document doc) { @ mMDoVKEKrCYS+8mqYdG5kjPyi53dcKSSGvc688J2PBgRC+j/njshXF7+Agawt14hdCNSc8Pw5XWffB/cUzlMhA==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawAbstractWeapon(Savable):
    TAG = 'weapon'
    _XMLID_ATTR_COLOURS_INDEX: str = 'i'
    _XMLID_ATTR_COMBATMOVES_VALUEATTR: str = 'value'
    _XMLID_ATTR_COREENCHANTMENT_ATTRIBUTE: str = 'coreEnchantment'
    _XMLID_ATTR_DAMAGETYPE_ATTRIBUTE: str = 'damageType'
    _XMLID_ATTR_ID_ATTRIBUTE: str = 'id'
    _XMLID_ATTR_NAME_ATTRIBUTE: str = 'name'
    _XMLID_ATTR_SPELLS_VALUEATTR: str = 'value'
    _XMLID_TAG_COLOURS_CHILD: str = 'colour'
    _XMLID_TAG_COLOURS_PARENT: str = 'colours'
    _XMLID_TAG_COMBATMOVES_CHILD: str = 'move'
    _XMLID_TAG_COMBATMOVES_PARENT: str = 'combatMoves'
    _XMLID_TAG_EFFECTS_CHILD: str = 'effect'
    _XMLID_TAG_EFFECTS_PARENT: str = 'effects'
    _XMLID_TAG_EFFECTS_VALUEELEM: Optional[str] = None
    _XMLID_TAG_SPELLS_CHILD: str = 'spell'
    _XMLID_TAG_SPELLS_PARENT: str = 'spells'

    def __init__(self) -> None:
        super().__init__()
        self.id: str = ''
        self.name: str = ''
        self.damageType: str = ''
        self.coreEnchantment: Optional[str] = None
        self.colours: Optional[List[str]] = list()
        self.effects: List[ItemEffect] = list()
        self.spells: List[str] = list()
        self.combatMoves: List[str] = list()

    def overrideAttrs(self, colours_index: _Optional_str = None, combatMoves_valueattr: _Optional_str = None, coreEnchantment_attribute: _Optional_str = None, damageType_attribute: _Optional_str = None, id_attribute: _Optional_str = None, name_attribute: _Optional_str = None, spells_valueattr: _Optional_str = None) -> None:
        if colours_index is not None:
            self._XMLID_ATTR_COLOURS_INDEX = colours_index
        if combatMoves_valueattr is not None:
            self._XMLID_ATTR_COMBATMOVES_VALUEATTR = combatMoves_valueattr
        if coreEnchantment_attribute is not None:
            self._XMLID_ATTR_COREENCHANTMENT_ATTRIBUTE = coreEnchantment_attribute
        if damageType_attribute is not None:
            self._XMLID_ATTR_DAMAGETYPE_ATTRIBUTE = damageType_attribute
        if id_attribute is not None:
            self._XMLID_ATTR_ID_ATTRIBUTE = id_attribute
        if name_attribute is not None:
            self._XMLID_ATTR_NAME_ATTRIBUTE = name_attribute
        if spells_valueattr is not None:
            self._XMLID_ATTR_SPELLS_VALUEATTR = spells_valueattr

    def overrideTags(self, colours_child: _Optional_str = None, colours_parent: _Optional_str = None, combatMoves_child: _Optional_str = None, combatMoves_parent: _Optional_str = None, effects_child: _Optional_str = None, effects_parent: _Optional_str = None, effects_valueelem: _Optional_str = None, spells_child: _Optional_str = None, spells_parent: _Optional_str = None) -> None:
        if colours_child is not None:
            self._XMLID_TAG_COLOURS_CHILD = colours_child
        if colours_parent is not None:
            self._XMLID_TAG_COLOURS_PARENT = colours_parent
        if combatMoves_child is not None:
            self._XMLID_TAG_COMBATMOVES_CHILD = combatMoves_child
        if combatMoves_parent is not None:
            self._XMLID_TAG_COMBATMOVES_PARENT = combatMoves_parent
        if effects_child is not None:
            self._XMLID_TAG_EFFECTS_CHILD = effects_child
        if effects_parent is not None:
            self._XMLID_TAG_EFFECTS_PARENT = effects_parent
        if effects_valueelem is not None:
            self._XMLID_TAG_EFFECTS_VALUEELEM = effects_valueelem
        if spells_child is not None:
            self._XMLID_TAG_SPELLS_CHILD = spells_child
        if spells_parent is not None:
            self._XMLID_TAG_SPELLS_PARENT = spells_parent

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        li: int
        lk: str
        lp: etree._Element
        lv: Savable
        value = e.get(self._XMLID_ATTR_ID_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_ID_ATTRIBUTE, 'id')
        else:
            self.id = str(value)
        value = e.get(self._XMLID_ATTR_NAME_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_NAME_ATTRIBUTE, 'name')
        else:
            self.name = str(value)
        value = e.get(self._XMLID_ATTR_DAMAGETYPE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_DAMAGETYPE_ATTRIBUTE, 'damageType')
        else:
            self.damageType = str(value)
        value = e.get(self._XMLID_ATTR_COREENCHANTMENT_ATTRIBUTE, None)
        if value is None:
            self.coreEnchantment = None
        else:
            if value == 'null':
                self.coreEnchantment = None
            else:
                self.coreEnchantment = str(value)
        if (lf := e.find(self._XMLID_TAG_COLOURS_PARENT)) is not None:
            tmp_colours: List[Tuple[int, str]] = []
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_COLOURS_CHILD):
                    li = int(lc.attrib[self._XMLID_ATTR_COLOURS_INDEX])
                    tmp_colours.append((li, lc.text))
            self.colours = [None] * len(tmp_colours)
            for li, lv in tmp_colours:
                self.colours[li] = lv
        if (lf := e.find(self._XMLID_TAG_EFFECTS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_EFFECTS_CHILD):
                    lv = ItemEffect()
                    lv.fromXML(lc)
                    self.effects.append(lv)
        else:
            self.effects = list()
        if (lf := e.find(self._XMLID_TAG_SPELLS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_SPELLS_CHILD):
                    self.spells.append(lc.attrib[self._XMLID_ATTR_SPELLS_VALUEATTR])
        else:
            self.spells = list()
        if (lf := e.find(self._XMLID_TAG_COMBATMOVES_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_COMBATMOVES_CHILD):
                    self.combatMoves.append(lc.attrib[self._XMLID_ATTR_COMBATMOVES_VALUEATTR])
        else:
            self.combatMoves = list()

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        li: int
        lp: etree._Element
        e.attrib[self._XMLID_ATTR_ID_ATTRIBUTE] = str(self.id)
        e.attrib[self._XMLID_ATTR_NAME_ATTRIBUTE] = str(self.name)
        e.attrib[self._XMLID_ATTR_DAMAGETYPE_ATTRIBUTE] = str(self.damageType)
        if self.coreEnchantment is None:
            e.attrib[self._XMLID_ATTR_COREENCHANTMENT_ATTRIBUTE] = 'null'
        else:
            e.attrib[self._XMLID_ATTR_COREENCHANTMENT_ATTRIBUTE] = str(self.coreEnchantment)
        if self.colours is not None and len(self.colours) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_COLOURS_PARENT)
            for li, lv in enumerate(self.colours):
                etree.SubElement(lp, self._XMLID_TAG_COLOURS_CHILD, {self._XMLID_ATTR_COLOURS_INDEX: str(li)}).text = lv
        lp = etree.SubElement(e, self._XMLID_TAG_EFFECTS_PARENT)
        for lv in self.effects:
            lp.append(lv.toXML())
        lp = etree.SubElement(e, self._XMLID_TAG_SPELLS_PARENT)
        for lv in self.spells:
            etree.SubElement(lp, self._XMLID_TAG_SPELLS_CHILD, {self._XMLID_ATTR_SPELLS_VALUEATTR: lv})
        lp = etree.SubElement(e, self._XMLID_TAG_COMBATMOVES_PARENT)
        for lv in self.combatMoves:
            etree.SubElement(lp, self._XMLID_TAG_COMBATMOVES_CHILD, {self._XMLID_ATTR_COMBATMOVES_VALUEATTR: lv})
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['id'] = self.id
        data['name'] = self.name
        data['damageType'] = self.damageType
        if self.coreEnchantment is None:
            data['coreEnchantment'] = 'null'
        else:
            data['coreEnchantment'] = self.coreEnchantment
        if self.colours is None or len(self.colours) > 0:
            data['colours'] = list()
        else:
            lc = list()
            if len(self.colours) > 0:
                for lv in lc:
                    lc.append(str(lv))
            data['colours'] = lc
        if self.effects is None or len(self.effects) > 0:
            data['effects'] = list()
        else:
            lc = list()
            if len(self.effects) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['effects'] = lc
        if self.spells is None or len(self.spells) > 0:
            data['spells'] = list()
        else:
            lc = list()
            if len(self.spells) > 0:
                for lv in lc:
                    lc.append(str(lv))
            data['spells'] = lc
        if self.combatMoves is None or len(self.combatMoves) > 0:
            data['combatMoves'] = list()
        else:
            lc = list()
            if len(self.combatMoves) > 0:
                for lv in lc:
                    lc.append(str(lv))
            data['combatMoves'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'id' not in data:
            raise KeyRequiredException(self, data, 'id', 'id')
        self.id = str(data['id'])
        if 'name' not in data:
            raise KeyRequiredException(self, data, 'name', 'name')
        self.name = str(data['name'])
        if 'damageType' not in data:
            raise KeyRequiredException(self, data, 'damageType', 'damageType')
        self.damageType = str(data['damageType'])
        self.coreEnchantment = str(data['coreEnchantment']) if 'coreEnchantment' in data else None
        if (lv := self.colours) is not None:
            for le in lv:
                self.colours.append(str(le))
        else:
            self.colours = list()
        if (lv := self.effects) is not None:
            for le in lv:
                self.effects.append(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'effects', 'effects')
        if (lv := self.spells) is not None:
            for le in lv:
                self.spells.append(str(le))
        else:
            raise KeyRequiredException(self, data, 'spells', 'spells')
        if (lv := self.combatMoves) is not None:
            for le in lv:
                self.combatMoves.append(str(le))
        else:
            raise KeyRequiredException(self, data, 'combatMoves', 'combatMoves')

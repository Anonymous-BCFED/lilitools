from typing import Optional
from lilitools.logging import ClickWrap
from lilitools.saves.enums.tf_modifier import ETFModifier
from lilitools.saves.enums.tf_potency import ETFPotency
from lilitools.saves.items.clothing.clothing_types import CLOTHING_TYPES
from lilitools.saves.items.thing_with_effects_mixin import ThingWithEffectsMixin
from lilitools.saves.modfiles.character.clothing.clothing_type import ClothingType
from ._savables.abstract_clothing import RawAbstractClothing

click = ClickWrap()


class AbstractClothing(ThingWithEffectsMixin, RawAbstractClothing):

    def clone(self) -> 'AbstractClothing':
        o = AbstractClothing()
        o.colours = [c for c in self.colours] if self.colours else None
        o.displacedList = [d for d in self.displacedList] if self.displacedList else None
        o.effects = [e.clone() for e in self.effects]
        o.enchantmentKnown = self.enchantmentKnown
        o.id = self.id
        o.isDirty = self.isDirty
        o.name = self.name
        o.pattern = self.pattern.clone() if self.pattern else None
        o.sealed = self.sealed
        o.slotEquippedTo = self.slotEquippedTo
        o.stickers = {k: v for k, v in self.stickers.items()}
        return o

    def isVibrator(self) -> bool:
        return self.getVibratorIntensity() is not None

    def getVibratorIntensity(self) -> Optional[ETFPotency]:
        ## from [LT]/src/com/lilithsthrone/game/inventory/clothing/AbstractClothing.java: public TFPotency getVibratorIntensity() { @ r3cRp76M5EuTkx+uMtocYAX9W+5QOXLStw8zmyzYTpDMEwIPA9DOCzCcH8FxOjbjM+R9zyKkZwfBcVyhQHdoNQ==
        for effect in self.effects:
            if effect is None:
                click.warn(f'AbstractClothing.getVibratorIntensity() - Encountered None effect.')
            elif effect.mod2 == ETFModifier.CLOTHING_VIBRATION:
                return effect.potency
        return None

    def getClothingType(self) -> ClothingType:
        return CLOTHING_TYPES.get(self.id)

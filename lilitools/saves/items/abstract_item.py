from lilitools.saves.items.thing_with_effects_mixin import ThingWithEffectsMixin
from ._savables.abstract_item import RawAbstractItem


class AbstractItem(ThingWithEffectsMixin, RawAbstractItem):
    def clone(self) -> 'AbstractItem':
        o = AbstractItem()
        o.id = self.id
        o.name = self.name
        o.colour = self.colour
        o.itemEffects = [x.clone() for x in self.itemEffects]
        return o

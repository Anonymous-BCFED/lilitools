from ._savables.abstract_pattern import RawAbstractPattern


class AbstractPattern(RawAbstractPattern):
    def clone(self) -> 'AbstractPattern':
        o = AbstractPattern()
        o.id = self.id
        o.colours = [c for c in self.colours] if self.colours else None
        return o

from typing import Optional

from lilitools.saves.items._savables.abstract_weapon import RawAbstractWeapon
from lilitools.saves.items.thing_with_effects_mixin import ThingWithEffectsMixin
from lilitools.saves.items.weapons.weapon_types import WEAPON_TYPES
from lilitools.saves.modfiles.weapons.weapon_type import WeaponType


class AbstractWeapon(ThingWithEffectsMixin, RawAbstractWeapon):
    def clone(self) -> 'AbstractWeapon':
        o = AbstractWeapon()
        o.id = self.id
        o.name = self.name
        o.damageType = self.damageType
        o.coreEnchantment = self.coreEnchantment
        o.colours = [x for x in self.colours] if self.colours else None
        o.effects = [x.clone() for x in self.effects]
        o.spells = [x for x in self.spells]
        o.combatMoves = [x for x in self.combatMoves]
        return o

    def getWeaponType(self) -> Optional[WeaponType]:
        return WEAPON_TYPES.get(self.id)

from pathlib import Path

from lilitools.saves.items.enums.known_clothing import EKnownClothing
from lilitools.saves.modfiles.character.clothing.clothing_type import ClothingType
from lilitools.saves.modfiles.utils import MFELookupTable


class _ClothingTypes(MFELookupTable[ClothingType]):
    MOD_SUB_DIR = Path("items") / "clothing"
    def handleBuiltinsEnumEntry(self, e: EKnownClothing) -> None:
        self.addBuiltin("", e.name, e.value)


# _ClothingTypes.DEBUG=True
CLOTHING_TYPES = _ClothingTypes(EKnownClothing, Path("res") / "clothing", ClothingType)

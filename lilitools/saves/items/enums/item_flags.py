
from enum import IntFlag, auto
from typing import List, Set


class EItemFlags(IntFlag):
    NONE = 0
    isAbleToBeDropped = auto()
    isAbleToBeSold = auto()
    isAbleToBeUsedFromInventory = auto()
    isAbleToBeUsedInCombatAllies = auto()
    isAbleToBeUsedInCombatEnemies = auto()
    isAbleToBeUsedInSex = auto()
    isConsumedOnUse = auto()
    isFetishGiving = auto()
    isFromExternalFile = auto()
    isGift = auto()
    isMod = auto()
    isPlural = auto()
    isTransformative = auto()

    @classmethod
    def FromStrList(self, l: List[str]) -> 'EItemFlags':
        o = EItemFlags.NONE
        for m in EItemFlags:
            if m.name in l:
                o |= m
        return o

    @classmethod
    def FromStrSet(self, l: Set[str]) -> 'EItemFlags':
        o = EItemFlags.NONE
        for m in EItemFlags:
            if m.name in l:
                o |= m
        return o

    def toStrList(self) -> List[str]:
        o:List[str] = [x.name for x in EItemFlags if (self & x) == x]
        if EItemFlags.NONE.name in o:
            o.remove(EItemFlags.NONE.name)
        return o

    def toStrSet(self) -> Set[str]:
        return set(self.toStrList())

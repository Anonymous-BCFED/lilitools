#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#     BY devtools/analysis/gen_items_enum.py    #
#              Built-in Item Types              #
#################################################
from enum import Enum
from typing import List
from lilitools.saves.items.enums.item_flags import EItemFlags
from lilitools.saves.items.item_effect import ItemEffect
__all__ = ["EKnownItems"]


class EKnownItems(Enum):
    ADDICTION_REMOVAL = (
        "ADDICTION_REMOVAL",
        "",
        750,
        "A delicate crystal bottle, filled with a cool, blue liquid. Engraved into one side are the words 'Angel's Nectar', although you're unsure if this fluid really does have anything to do with them...",
        "a bottle of",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Angel's Nectar",
        "Angel's Nectars",
        ["RACE_HUMAN", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("ADDICTION_REMOVAL", None, None, None, 0, 0)],
    )
    ADDICTION_REMOVAL_REFINED = (
        "ADDICTION_REMOVAL_REFINED",
        "",
        1500,
        "A vial of cool, light-blue liquid, which gives off a faint, steady glow. Being a refined form of 'Angel's Nectar', this liquid has lost its ability to remove addictions, but is instead able to permanently lower the corruption of whoever drinks it...",
        "a delicate vial of",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Angel's Purity",
        "Angel's Purities",
        ["RACE_HUMAN", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [],
    )
    ARTHURS_PACKAGE = (
        "ARTHURS_PACKAGE",
        "",
        0,
        "A package that you collected from Arcane Arts. You need to deliver this to Arthur.",
        "",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Arthur's Package",
        "Arthur's Packages",
        ["ANDROGYNOUS", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [],
    )
    BOOK_ALLIGATOR_MORPH = (
        "BOOK_ALLIGATOR_MORPH",
        "",
        250,
        "A book which contains advanced lore concerning alligator-morphs.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Rasselin' Gators",
        "Rasselin' Gators",
        ["RACE_ALLIGATOR_MORPH", "CLOTHING_GOLD", "RACE_ALLIGATOR_MORPH"],
        [ItemEffect("BOOK_READ_ALLIGATOR_MORPH", None, None, None, 0, 0)],
    )
    BOOK_ANGEL = (
        "BOOK_ANGEL",
        "",
        250,
        "A book which contains advanced lore concerning angels.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "The Protectors",
        "The Protectors",
        ["RACE_ANGEL", "CLOTHING_GOLD", "RACE_ANGEL"],
        [ItemEffect("BOOK_READ_ANGEL", None, None, None, 0, 0)],
    )
    BOOK_BAT_MORPH = (
        "BOOK_BAT_MORPH",
        "",
        250,
        "A book which contains advanced lore concerning bat-morphs.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Flying Foxes",
        "Flying Foxes'",
        ["RACE_BAT_MORPH", "CLOTHING_GOLD", "RACE_BAT_MORPH"],
        [ItemEffect("BOOK_READ_BAT_MORPH", None, None, None, 0, 0)],
    )
    BOOK_CAT_MORPH = (
        "BOOK_CAT_MORPH",
        "",
        250,
        "A book which contains advanced lore concerning cat-morphs.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Curious Kitties",
        "Curious Kitties",
        ["RACE_CAT_MORPH", "CLOTHING_GOLD", "RACE_CAT_MORPH"],
        [ItemEffect("BOOK_READ_CAT_MORPH", None, None, None, 0, 0)],
    )
    BOOK_CENTAUR = (
        "BOOK_CENTAUR",
        "",
        250,
        "A book which contains advanced lore concerning centaurs.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Centaurs & More",
        "Centaurs & More",
        ["RACE_CENTAUR", "CLOTHING_GOLD", "RACE_CENTAUR"],
        [ItemEffect("BOOK_READ_CENTAUR", None, None, None, 0, 0)],
    )
    BOOK_charisma_spider_subspecies_spider = (
        "BOOK_charisma_spider_subspecies_spider",
        "",
        250,
        "A book which contains advanced lore concerning spider-morphs.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Deadly Spiders",
        "Deadly Spiders",
        ["charisma_spider_silk_white", "CLOTHING_GOLD", "charisma_spider_silk_white"],
        [ItemEffect("BOOK_READ_charisma_spider_subspecies_spider", None, None, None, 0, 0)],
    )
    BOOK_COW_MORPH = (
        "BOOK_COW_MORPH",
        "",
        250,
        "A book which contains advanced lore concerning cattle-morphs.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Milking Cows",
        "Milking Cows'",
        ["RACE_COW_MORPH", "CLOTHING_GOLD", "RACE_COW_MORPH"],
        [ItemEffect("BOOK_READ_COW_MORPH", None, None, None, 0, 0)],
    )
    BOOK_DEMON = (
        "BOOK_DEMON",
        "",
        250,
        "A book which contains advanced lore concerning demons.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Demonic Origins",
        "Demonic Origins'",
        ["RACE_DEMON", "CLOTHING_GOLD", "RACE_DEMON"],
        [ItemEffect("BOOK_READ_DEMON", None, None, None, 0, 0)],
    )
    BOOK_DOG_MORPH = (
        "BOOK_DOG_MORPH",
        "",
        250,
        "A book which contains advanced lore concerning dog-morphs.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Canine Culture",
        "Canine Cultures",
        ["RACE_DOG_MORPH", "CLOTHING_GOLD", "RACE_DOG_MORPH"],
        [ItemEffect("BOOK_READ_DOG_MORPH", None, None, None, 0, 0)],
    )
    BOOK_DOLL = (
        "BOOK_DOLL",
        "",
        250,
        "A book which contains advanced lore concerning dolls.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "The Ultimate Toy",
        "The Ultimate Toys",
        ["RACE_DOLL", "CLOTHING_GOLD", "RACE_DOLL"],
        [ItemEffect("BOOK_READ_DOLL", None, None, None, 0, 0)],
    )
    BOOK_dsg_bear_subspecies_bear = (
        "BOOK_dsg_bear_subspecies_bear",
        "",
        250,
        "A book which contains advanced lore concerning bear-morphs. The book's author is identified as 'Xuanlong'.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Strolling Bears",
        "Strolling Bears",
        ["dsg_bearBrown", "CLOTHING_GOLD", "dsg_bearBrown"],
        [ItemEffect("BOOK_READ_dsg_bear_subspecies_bear", None, None, None, 0, 0)],
    )
    BOOK_dsg_dragon_subspecies_dragon = (
        "BOOK_dsg_dragon_subspecies_dragon",
        "",
        250,
        "A book which contains advanced lore concerning dragon-morphs. The book's author is identified as 'Xuanlong'.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Mysterious Dragons",
        "Mysterious Dragons",
        ["dsg_dragonRed", "CLOTHING_GOLD", "dsg_dragonRed"],
        [ItemEffect("BOOK_READ_dsg_dragon_subspecies_dragon", None, None, None, 0, 0)],
    )
    BOOK_dsg_ferret_subspecies_ferret = (
        "BOOK_dsg_ferret_subspecies_ferret",
        "",
        250,
        "A book which contains advanced lore concerning ferret-morphs. The book's author is identified as 'Xuanlong'.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Playful Ferrets",
        "Playful Ferrets",
        ["dsg_ferretCinnamon", "CLOTHING_GOLD", "dsg_ferretCinnamon"],
        [ItemEffect("BOOK_READ_dsg_ferret_subspecies_ferret", None, None, None, 0, 0)],
    )
    BOOK_dsg_gryphon_subspecies_gryphon = (
        "BOOK_dsg_gryphon_subspecies_gryphon",
        "",
        250,
        "A book which contains advanced lore concerning gryphon-morphs. The book's author is identified as 'Xuanlong'.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Soaring Gryphons",
        "Soaring Gryphons",
        ["dsg_gryphonBlue", "CLOTHING_GOLD", "dsg_gryphonBlue"],
        [ItemEffect("BOOK_READ_dsg_gryphon_subspecies_gryphon", None, None, None, 0, 0)],
    )
    BOOK_dsg_otter_subspecies_otter = (
        "BOOK_dsg_otter_subspecies_otter",
        "",
        250,
        "A book which contains advanced lore concerning otter-morphs. The book's author is identified as 'Xuanlong'.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Frolicking Otters",
        "Frolicking Otters",
        ["dsg_otterBrown", "CLOTHING_GOLD", "dsg_otterBrown"],
        [ItemEffect("BOOK_READ_dsg_otter_subspecies_otter", None, None, None, 0, 0)],
    )
    BOOK_dsg_raccoon_subspecies_raccoon = (
        "BOOK_dsg_raccoon_subspecies_raccoon",
        "",
        250,
        "A book which contains advanced lore concerning raccoon-morphs. The book's author is identified as 'Xuanlong'.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Foraging Raccoons",
        "Foraging Raccoons",
        ["dsg_raccoonGrey", "CLOTHING_GOLD", "dsg_raccoonGrey"],
        [ItemEffect("BOOK_READ_dsg_raccoon_subspecies_raccoon", None, None, None, 0, 0)],
    )
    BOOK_dsg_shark_subspecies_shark = (
        "BOOK_dsg_shark_subspecies_shark",
        "",
        250,
        "A book which contains advanced lore concerning shark-morphs. The book's author is identified as 'Xuanlong'.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Circling Sharks",
        "Circling Sharks",
        ["dsg_sharkGrey", "CLOTHING_GOLD", "dsg_sharkGrey"],
        [ItemEffect("BOOK_READ_dsg_shark_subspecies_shark", None, None, None, 0, 0)],
    )
    BOOK_ELDER_LILIN = (
        "BOOK_ELDER_LILIN",
        "",
        250,
        "A book which contains advanced lore concerning elder lilin.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Lilith's Spawn",
        "Lilith's Spawns",
        ["RACE_LILIN", "CLOTHING_GOLD", "RACE_LILIN"],
        [ItemEffect("BOOK_READ_ELDER_LILIN", None, None, None, 0, 0)],
    )
    BOOK_ELEMENTAL_AIR = (
        "BOOK_ELEMENTAL_AIR",
        "",
        250,
        "A book which contains advanced lore concerning air elementals.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Air Elementals",
        "Air Elementals'",
        ["SPELL_SCHOOL_AIR", "CLOTHING_GOLD", "SPELL_SCHOOL_AIR"],
        [ItemEffect("BOOK_READ_ELEMENTAL_AIR", None, None, None, 0, 0)],
    )
    BOOK_ELEMENTAL_ARCANE = (
        "BOOK_ELEMENTAL_ARCANE",
        "",
        250,
        "A book which contains advanced lore concerning arcane elementals.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Arcane Elementals",
        "Arcane Elementals'",
        ["SPELL_SCHOOL_ARCANE", "CLOTHING_GOLD", "SPELL_SCHOOL_ARCANE"],
        [ItemEffect("BOOK_READ_ELEMENTAL_ARCANE", None, None, None, 0, 0)],
    )
    BOOK_ELEMENTAL_EARTH = (
        "BOOK_ELEMENTAL_EARTH",
        "",
        250,
        "A book which contains advanced lore concerning earth elementals.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Earth Elementals",
        "Earth Elementals'",
        ["SPELL_SCHOOL_EARTH", "CLOTHING_GOLD", "SPELL_SCHOOL_EARTH"],
        [ItemEffect("BOOK_READ_ELEMENTAL_EARTH", None, None, None, 0, 0)],
    )
    BOOK_ELEMENTAL_FIRE = (
        "BOOK_ELEMENTAL_FIRE",
        "",
        250,
        "A book which contains advanced lore concerning fire elementals.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Fire Elementals",
        "Fire Elementals'",
        ["SPELL_SCHOOL_FIRE", "CLOTHING_GOLD", "SPELL_SCHOOL_FIRE"],
        [ItemEffect("BOOK_READ_ELEMENTAL_FIRE", None, None, None, 0, 0)],
    )
    BOOK_ELEMENTAL_WATER = (
        "BOOK_ELEMENTAL_WATER",
        "",
        250,
        "A book which contains advanced lore concerning water elementals.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Water Elementals",
        "Water Elementals'",
        ["SPELL_SCHOOL_WATER", "CLOTHING_GOLD", "SPELL_SCHOOL_WATER"],
        [ItemEffect("BOOK_READ_ELEMENTAL_WATER", None, None, None, 0, 0)],
    )
    BOOK_FOX_MORPH = (
        "BOOK_FOX_MORPH",
        "",
        250,
        "A book which contains advanced lore concerning fox-morphs.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Skulking Vulpines",
        "Skulking Vulpines",
        ["RACE_FOX_MORPH", "CLOTHING_GOLD", "RACE_FOX_MORPH"],
        [ItemEffect("BOOK_READ_FOX_MORPH", None, None, None, 0, 0)],
    )
    BOOK_HALF_DEMON = (
        "BOOK_HALF_DEMON",
        "",
        250,
        "A book which contains advanced lore concerning half-demons.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Demonic Half-breeds",
        "Demonic Half-breeds'",
        ["RACE_HALF_DEMON", "CLOTHING_GOLD", "RACE_HALF_DEMON"],
        [ItemEffect("BOOK_READ_HALF_DEMON", None, None, None, 0, 0)],
    )
    BOOK_HARPY = (
        "BOOK_HARPY",
        "",
        250,
        "A book which contains advanced lore concerning harpies.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "All About Harpies",
        "All About Harpies'",
        ["RACE_HARPY", "CLOTHING_GOLD", "RACE_HARPY"],
        [ItemEffect("BOOK_READ_HARPY", None, None, None, 0, 0)],
    )
    BOOK_HORSE_MORPH = (
        "BOOK_HORSE_MORPH",
        "",
        250,
        "A book which contains advanced lore concerning horse-morphs.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Equine Encyclopedia",
        "Equine Encyclopedias",
        ["RACE_HORSE_MORPH", "CLOTHING_GOLD", "RACE_HORSE_MORPH"],
        [ItemEffect("BOOK_READ_HORSE_MORPH", None, None, None, 0, 0)],
    )
    BOOK_HUMAN = (
        "BOOK_HUMAN",
        "",
        250,
        "A book which contains advanced lore concerning humans.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Concerning Humans",
        "Concerning Humans",
        ["RACE_HUMAN", "CLOTHING_GOLD", "RACE_HUMAN"],
        [ItemEffect("BOOK_READ_HUMAN", None, None, None, 0, 0)],
    )
    BOOK_IMP = (
        "BOOK_IMP",
        "",
        250,
        "A book which contains advanced lore concerning imps.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Impish Fiends",
        "Impish Fiends'",
        ["RACE_IMP", "CLOTHING_GOLD", "RACE_IMP"],
        [ItemEffect("BOOK_READ_IMP", None, None, None, 0, 0)],
    )
    BOOK_innoxia_badger_subspecies_badger = (
        "BOOK_innoxia_badger_subspecies_badger",
        "",
        250,
        "A book which contains advanced lore concerning badger-morphs.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Moonlit Stripes",
        "Moonlit Stripes",
        ["innoxia_badger", "CLOTHING_GOLD", "innoxia_badger"],
        [ItemEffect("BOOK_READ_innoxia_badger_subspecies_badger", None, None, None, 0, 0)],
    )
    BOOK_innoxia_deer_subspecies_deer = (
        "BOOK_innoxia_deer_subspecies_deer",
        "",
        250,
        "A book which contains advanced lore concerning deer-morphs.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Harts and Hinds",
        "Harts and Hinds",
        ["innoxia_deer", "CLOTHING_GOLD", "innoxia_deer"],
        [ItemEffect("BOOK_READ_innoxia_deer_subspecies_deer", None, None, None, 0, 0)],
    )
    BOOK_innoxia_goat_subspecies_goat = (
        "BOOK_innoxia_goat_subspecies_goat",
        "",
        250,
        "A book which contains advanced lore concerning goat-morphs.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Bleating Headbutters",
        "Bleating Headbutters",
        ["innoxia_goat", "CLOTHING_GOLD", "innoxia_goat"],
        [ItemEffect("BOOK_READ_innoxia_goat_subspecies_goat", None, None, None, 0, 0)],
    )
    BOOK_innoxia_hyena_subspecies_spotted = (
        "BOOK_innoxia_hyena_subspecies_spotted",
        "",
        250,
        "A book which contains advanced lore concerning spotted hyena-morphs. The book's author is identified as 'Professor Webster'.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Laughing Hyenas",
        "Laughing Hyenas",
        ["innoxia_hyena", "CLOTHING_GOLD", "innoxia_hyena"],
        [ItemEffect("BOOK_READ_innoxia_hyena_subspecies_spotted", None, None, None, 0, 0)],
    )
    BOOK_innoxia_mouse_subspecies_mouse = (
        "BOOK_innoxia_mouse_subspecies_mouse",
        "",
        250,
        "A book which contains advanced lore concerning mouse-morphs.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Harvest Friends",
        "Harvest Friends",
        ["innoxia_mouse", "CLOTHING_GOLD", "innoxia_mouse"],
        [ItemEffect("BOOK_READ_innoxia_mouse_subspecies_mouse", None, None, None, 0, 0)],
    )
    BOOK_innoxia_panther_subspecies_lion = (
        "BOOK_innoxia_panther_subspecies_lion",
        "",
        250,
        "A book which contains advanced lore concerning lion-morphs.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Big Cats",
        "Big Cats",
        ["RACE_CAT_MORPH_LION", "CLOTHING_GOLD", "RACE_CAT_MORPH_LION"],
        [ItemEffect("BOOK_READ_innoxia_panther_subspecies_lion", None, None, None, 0, 0)],
    )
    BOOK_innoxia_pig_subspecies_pig = (
        "BOOK_innoxia_pig_subspecies_pig",
        "",
        250,
        "A book which contains advanced lore concerning pig-morphs.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Porky Porcines",
        "Porky Porcines",
        ["innoxia_pig", "CLOTHING_GOLD", "innoxia_pig"],
        [ItemEffect("BOOK_READ_innoxia_pig_subspecies_pig", None, None, None, 0, 0)],
    )
    BOOK_innoxia_raptor_subspecies_falcon = (
        "BOOK_innoxia_raptor_subspecies_falcon",
        "",
        250,
        "A book which contains advanced lore concerning falcon-morphs.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "The Sharpest Talons",
        "The Sharpest Talons",
        ["innoxia_raptor", "CLOTHING_GOLD", "innoxia_raptor"],
        [ItemEffect("BOOK_READ_innoxia_raptor_subspecies_falcon", None, None, None, 0, 0)],
    )
    BOOK_innoxia_sheep_subspecies_sheep = (
        "BOOK_innoxia_sheep_subspecies_sheep",
        "",
        250,
        "A book which contains advanced lore concerning sheep-morphs.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Woolly Friends",
        "Woolly Friends",
        ["innoxia_sheep", "CLOTHING_GOLD", "innoxia_sheep"],
        [ItemEffect("BOOK_READ_innoxia_sheep_subspecies_sheep", None, None, None, 0, 0)],
    )
    BOOK_LILIN = (
        "BOOK_LILIN",
        "",
        250,
        "A book which contains advanced lore concerning lilin.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Lilith's Brood",
        "Lilith's Broods",
        ["RACE_LILIN", "CLOTHING_GOLD", "RACE_LILIN"],
        [ItemEffect("BOOK_READ_LILIN", None, None, None, 0, 0)],
    )
    BOOK_mintychip_salamander_subspecies_flame = (
        "BOOK_mintychip_salamander_subspecies_flame",
        "",
        250,
        "A book which contains advanced lore concerning flame salamander-morphs.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Primal Salamanders",
        "Primal Salamanders",
        ["mintychip_salamander", "CLOTHING_GOLD", "mintychip_salamander"],
        [ItemEffect("BOOK_READ_mintychip_salamander_subspecies_flame", None, None, None, 0, 0)],
    )
    BOOK_NoStepOnSnek_capybara_subspecies_capybara = (
        "BOOK_NoStepOnSnek_capybara_subspecies_capybara",
        "",
        250,
        "A book which contains advanced lore concerning capybara-morphs.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Friendshaped Serenity",
        "Friendshaped Serenity",
        ["NoStepOnSnek_capybara", "CLOTHING_GOLD", "NoStepOnSnek_capybara"],
        [ItemEffect("BOOK_READ_NoStepOnSnek_capybara_subspecies_capybara", None, None, None, 0, 0)],
    )
    BOOK_NoStepOnSnek_octopus_subspecies_octopus = (
        "BOOK_NoStepOnSnek_octopus_subspecies_octopus",
        "",
        250,
        "A book which contains advanced lore concerning octopus-morphs.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Heavily Armed Suckers",
        "Heavily Armed Suckers",
        ["NoStepOnSnek_octopus", "CLOTHING_GOLD", "NoStepOnSnek_octopus"],
        [ItemEffect("BOOK_READ_NoStepOnSnek_octopus_subspecies_octopus", None, None, None, 0, 0)],
    )
    BOOK_NoStepOnSnek_snake_subspecies_lamia = (
        "BOOK_NoStepOnSnek_snake_subspecies_lamia",
        "",
        250,
        "A book which contains advanced lore concerning lamias.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Clutching Coils",
        "Clutching Coils",
        ["NoStepOnSnek_snake", "CLOTHING_GOLD", "NoStepOnSnek_snake"],
        [ItemEffect("BOOK_READ_NoStepOnSnek_snake_subspecies_lamia", None, None, None, 0, 0)],
    )
    BOOK_RABBIT_MORPH = (
        "BOOK_RABBIT_MORPH",
        "",
        250,
        "A book which contains advanced lore concerning rabbit-morphs.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Bunny Litters",
        "Bunny Litters'",
        ["RACE_RABBIT_MORPH", "CLOTHING_GOLD", "RACE_RABBIT_MORPH"],
        [ItemEffect("BOOK_READ_RABBIT_MORPH", None, None, None, 0, 0)],
    )
    BOOK_RAT_MORPH = (
        "BOOK_RAT_MORPH",
        "",
        250,
        "A book which contains advanced lore concerning rat-morphs. The book's author is identified as 'Professor Webster'.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Causing Mischief",
        "Causing Mischiefs",
        ["RACE_RAT_MORPH", "CLOTHING_GOLD", "RACE_RAT_MORPH"],
        [ItemEffect("BOOK_READ_RAT_MORPH", None, None, None, 0, 0)],
    )
    BOOK_REINDEER_MORPH = (
        "BOOK_REINDEER_MORPH",
        "",
        250,
        "A book which contains advanced lore concerning reindeer-morphs.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Reindeer Migrations",
        "Reindeer Migrations",
        ["RACE_REINDEER_MORPH", "CLOTHING_GOLD", "RACE_REINDEER_MORPH"],
        [ItemEffect("BOOK_READ_REINDEER_MORPH", None, None, None, 0, 0)],
    )
    BOOK_SLIME = (
        "BOOK_SLIME",
        "",
        250,
        "A book which contains advanced lore concerning slimes.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Slimy Fun",
        "Slimy Funs",
        ["RACE_SLIME", "CLOTHING_GOLD", "RACE_SLIME"],
        [ItemEffect("BOOK_READ_SLIME", None, None, None, 0, 0)],
    )
    BOOK_SQUIRREL_MORPH = (
        "BOOK_SQUIRREL_MORPH",
        "",
        250,
        "A book which contains advanced lore concerning squirrel-morphs.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Chasing Squirrels",
        "Chasing Squirrels'",
        ["RACE_SQUIRREL_MORPH", "CLOTHING_GOLD", "RACE_SQUIRREL_MORPH"],
        [ItemEffect("BOOK_READ_SQUIRREL_MORPH", None, None, None, 0, 0)],
    )
    BOOK_WOLF_MORPH = (
        "BOOK_WOLF_MORPH",
        "",
        250,
        "A book which contains advanced lore concerning wolf-morphs.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Prowling Lupines",
        "Prowling Lupines'",
        ["RACE_WOLF_MORPH", "CLOTHING_GOLD", "RACE_WOLF_MORPH"],
        [ItemEffect("BOOK_READ_WOLF_MORPH", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_ALLIGATOR_MORPH = (
        "BOTTLED_ESSENCE_ALLIGATOR_MORPH",
        "",
        40,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling dark green glow of an arcane essence, imbued with the energy of an alligator, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Alligator Essence",
        "Bottled Alligator Essences",
        ["RACE_ALLIGATOR_MORPH", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_ALLIGATOR_MORPH", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_ANGEL = (
        "BOTTLED_ESSENCE_ANGEL",
        "",
        320,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling light blue glow of an arcane essence, imbued with the energy of an angel, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Angel Essence",
        "Bottled Angel Essences",
        ["RACE_ANGEL", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_ANGEL", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_ARCANE = (
        "BOTTLED_ESSENCE_ARCANE",
        "",
        40,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling pink glow of an arcane essence flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Arcane Essence",
        "Bottled Arcane Essences",
        ["GENERIC_ARCANE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_ARCANE", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_BAT_MORPH = (
        "BOTTLED_ESSENCE_BAT_MORPH",
        "",
        40,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling black glow of an arcane essence, imbued with the energy of a bat, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Bat Essence",
        "Bottled Bat Essences",
        ["RACE_BAT_MORPH", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_BAT_MORPH", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_CAT_MORPH = (
        "BOTTLED_ESSENCE_CAT_MORPH",
        "",
        48,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling salmon-pink glow of an arcane essence, imbued with the energy of a cat, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Cat Essence",
        "Bottled Cat Essences",
        ["RACE_CAT_MORPH", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_CAT_MORPH", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_CHARISMA_SPIDER_SUBSPECIES_SPIDER = (
        "BOTTLED_ESSENCE_CHARISMA_SPIDER_SUBSPECIES_SPIDER",
        "",
        40,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling silk white glow of an arcane essence, imbued with the energy of a spider, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Spider Essence",
        "Bottled Spider Essences",
        ["charisma_spider_silk_white", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_CHARISMA_SPIDER_SUBSPECIES_SPIDER", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_COW_MORPH = (
        "BOTTLED_ESSENCE_COW_MORPH",
        "",
        60,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling tan glow of an arcane essence, imbued with the energy of a cattle, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Cattle Essence",
        "Bottled Cattle Essences",
        ["RACE_COW_MORPH", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_COW_MORPH", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_DEMON = (
        "BOTTLED_ESSENCE_DEMON",
        "",
        480,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling light purple glow of an arcane essence, imbued with the energy of a demon, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Demon Essence",
        "Bottled Demon Essences",
        ["RACE_DEMON", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_DEMON", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_DOG_MORPH = (
        "BOTTLED_ESSENCE_DOG_MORPH",
        "",
        48,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling brown glow of an arcane essence, imbued with the energy of a dog, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Dog Essence",
        "Bottled Dog Essences",
        ["RACE_DOG_MORPH", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_DOG_MORPH", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_DOLL = (
        "BOTTLED_ESSENCE_DOLL",
        "",
        40,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling black glow of an arcane essence, imbued with the energy of a doll, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Doll Essence",
        "Bottled Doll Essences",
        ["RACE_DOLL", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_DOLL", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_DSG_BEAR_SUBSPECIES_BEAR = (
        "BOTTLED_ESSENCE_DSG_BEAR_SUBSPECIES_BEAR",
        "",
        40,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling bear brown glow of an arcane essence, imbued with the energy of a bear, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Bear Essence",
        "Bottled Bear Essences",
        ["dsg_bearBrown", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_DSG_BEAR_SUBSPECIES_BEAR", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_DSG_DRAGON_SUBSPECIES_DRAGON = (
        "BOTTLED_ESSENCE_DSG_DRAGON_SUBSPECIES_DRAGON",
        "",
        1000,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling dragon red glow of an arcane essence, imbued with the energy of a dragon, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Dragon Essence",
        "Bottled Dragon Essences",
        ["dsg_dragonRed", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_DSG_DRAGON_SUBSPECIES_DRAGON", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_DSG_FERRET_SUBSPECIES_FERRET = (
        "BOTTLED_ESSENCE_DSG_FERRET_SUBSPECIES_FERRET",
        "",
        40,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling ferret cinnamon glow of an arcane essence, imbued with the energy of a ferret, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Ferret Essence",
        "Bottled Ferret Essences",
        ["dsg_ferretCinnamon", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_DSG_FERRET_SUBSPECIES_FERRET", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_DSG_GRYPHON_SUBSPECIES_GRYPHON = (
        "BOTTLED_ESSENCE_DSG_GRYPHON_SUBSPECIES_GRYPHON",
        "",
        360,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling gryphon blue glow of an arcane essence, imbued with the energy of a gryphon, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Gryphon Essence",
        "Bottled Gryphon Essences",
        ["dsg_gryphonBlue", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_DSG_GRYPHON_SUBSPECIES_GRYPHON", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_DSG_OTTER_SUBSPECIES_OTTER = (
        "BOTTLED_ESSENCE_DSG_OTTER_SUBSPECIES_OTTER",
        "",
        40,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling otter brown glow of an arcane essence, imbued with the energy of an otter, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Otter Essence",
        "Bottled Otter Essences",
        ["dsg_otterBrown", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_DSG_OTTER_SUBSPECIES_OTTER", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_DSG_RACCOON_SUBSPECIES_RACCOON = (
        "BOTTLED_ESSENCE_DSG_RACCOON_SUBSPECIES_RACCOON",
        "",
        40,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling raccoon grey glow of an arcane essence, imbued with the energy of a raccoon, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Raccoon Essence",
        "Bottled Raccoon Essences",
        ["dsg_raccoonGrey", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_DSG_RACCOON_SUBSPECIES_RACCOON", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_DSG_SHARK_SUBSPECIES_SHARK = (
        "BOTTLED_ESSENCE_DSG_SHARK_SUBSPECIES_SHARK",
        "",
        40,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling shark grey glow of an arcane essence, imbued with the energy of a shark, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Shark Essence",
        "Bottled Shark Essences",
        ["dsg_sharkGrey", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_DSG_SHARK_SUBSPECIES_SHARK", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_ELDER_LILIN = (
        "BOTTLED_ESSENCE_ELDER_LILIN",
        "",
        10000,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling purple glow of an arcane essence, imbued with the energy of an elder lilin, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Elder lilin Essence",
        "Bottled Elder lilin Essences",
        ["RACE_LILIN", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_ELDER_LILIN", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_ELEMENTAL_AIR = (
        "BOTTLED_ESSENCE_ELEMENTAL_AIR",
        "",
        400,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling light blue glow of an arcane essence, imbued with the energy of an air elemental, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Air elemental Essence",
        "Bottled Air elemental Essences",
        ["SPELL_SCHOOL_AIR", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_ELEMENTAL_AIR", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_ELEMENTAL_ARCANE = (
        "BOTTLED_ESSENCE_ELEMENTAL_ARCANE",
        "",
        400,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling pink glow of an arcane essence, imbued with the energy of an arcane elemental, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Arcane elemental Essence",
        "Bottled Arcane elemental Essences",
        ["SPELL_SCHOOL_ARCANE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_ELEMENTAL_ARCANE", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_ELEMENTAL_EARTH = (
        "BOTTLED_ESSENCE_ELEMENTAL_EARTH",
        "",
        400,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling brown glow of an arcane essence, imbued with the energy of an earth elemental, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Earth elemental Essence",
        "Bottled Earth elemental Essences",
        ["SPELL_SCHOOL_EARTH", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_ELEMENTAL_EARTH", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_ELEMENTAL_FIRE = (
        "BOTTLED_ESSENCE_ELEMENTAL_FIRE",
        "",
        400,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling orange glow of an arcane essence, imbued with the energy of a fire elemental, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Fire elemental Essence",
        "Bottled Fire elemental Essences",
        ["SPELL_SCHOOL_FIRE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_ELEMENTAL_FIRE", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_ELEMENTAL_WATER = (
        "BOTTLED_ESSENCE_ELEMENTAL_WATER",
        "",
        400,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling aqua glow of an arcane essence, imbued with the energy of a water elemental, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Water elemental Essence",
        "Bottled Water elemental Essences",
        ["SPELL_SCHOOL_WATER", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_ELEMENTAL_WATER", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_FOX_MORPH = (
        "BOTTLED_ESSENCE_FOX_MORPH",
        "",
        64,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling ginger glow of an arcane essence, imbued with the energy of a fox, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Fox Essence",
        "Bottled Fox Essences",
        ["RACE_FOX_MORPH", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_FOX_MORPH", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_HALF_DEMON = (
        "BOTTLED_ESSENCE_HALF_DEMON",
        "",
        200,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling indigo glow of an arcane essence, imbued with the energy of a demon, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Demon Essence",
        "Bottled Demon Essences",
        ["RACE_HALF_DEMON", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_HALF_DEMON", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_HARPY = (
        "BOTTLED_ESSENCE_HARPY",
        "",
        48,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling light pink glow of an arcane essence, imbued with the energy of a bird, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Bird Essence",
        "Bottled Bird Essences",
        ["RACE_HARPY", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_HARPY", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_HORSE_MORPH = (
        "BOTTLED_ESSENCE_HORSE_MORPH",
        "",
        72,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling orange glow of an arcane essence, imbued with the energy of a horse, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Horse Essence",
        "Bottled Horse Essences",
        ["RACE_HORSE_MORPH", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_HORSE_MORPH", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_HUMAN = (
        "BOTTLED_ESSENCE_HUMAN",
        "",
        40,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling pale blue glow of an arcane essence, imbued with the energy of a human, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Human Essence",
        "Bottled Human Essences",
        ["RACE_HUMAN", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_HUMAN", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_IMP = (
        "BOTTLED_ESSENCE_IMP",
        "",
        40,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling purple glow of an arcane essence, imbued with the energy of an imp, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Imp Essence",
        "Bottled Imp Essences",
        ["RACE_IMP", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_IMP", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_INNOXIA_BADGER_SUBSPECIES_BADGER = (
        "BOTTLED_ESSENCE_INNOXIA_BADGER_SUBSPECIES_BADGER",
        "",
        48,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling dark grey glow of an arcane essence, imbued with the energy of a badger, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Badger Essence",
        "Bottled Badger Essences",
        ["innoxia_badger", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_INNOXIA_BADGER_SUBSPECIES_BADGER", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_INNOXIA_DEER_SUBSPECIES_DEER = (
        "BOTTLED_ESSENCE_INNOXIA_DEER_SUBSPECIES_DEER",
        "",
        48,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling brown glow of an arcane essence, imbued with the energy of a deer, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Deer Essence",
        "Bottled Deer Essences",
        ["innoxia_deer", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_INNOXIA_DEER_SUBSPECIES_DEER", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_INNOXIA_GOAT_SUBSPECIES_GOAT = (
        "BOTTLED_ESSENCE_INNOXIA_GOAT_SUBSPECIES_GOAT",
        "",
        48,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling brown glow of an arcane essence, imbued with the energy of a goat, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Goat Essence",
        "Bottled Goat Essences",
        ["innoxia_goat", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_INNOXIA_GOAT_SUBSPECIES_GOAT", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_INNOXIA_HYENA_SUBSPECIES_SPOTTED = (
        "BOTTLED_ESSENCE_INNOXIA_HYENA_SUBSPECIES_SPOTTED",
        "",
        40,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling faded brown glow of an arcane essence, imbued with the energy of a hyena, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Hyena Essence",
        "Bottled Hyena Essences",
        ["innoxia_hyena", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_INNOXIA_HYENA_SUBSPECIES_SPOTTED", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_INNOXIA_MOUSE_SUBSPECIES_MOUSE = (
        "BOTTLED_ESSENCE_INNOXIA_MOUSE_SUBSPECIES_MOUSE",
        "",
        48,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling light brown glow of an arcane essence, imbued with the energy of a mouse, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Mouse Essence",
        "Bottled Mouse Essences",
        ["innoxia_mouse", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_INNOXIA_MOUSE_SUBSPECIES_MOUSE", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_INNOXIA_PANTHER_SUBSPECIES_LION = (
        "BOTTLED_ESSENCE_INNOXIA_PANTHER_SUBSPECIES_LION",
        "",
        96,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling yellow glow of an arcane essence, imbued with the energy of a panther, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Panther Essence",
        "Bottled Panther Essences",
        ["RACE_CAT_MORPH_LION", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_INNOXIA_PANTHER_SUBSPECIES_LION", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_INNOXIA_PIG_SUBSPECIES_PIG = (
        "BOTTLED_ESSENCE_INNOXIA_PIG_SUBSPECIES_PIG",
        "",
        48,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling pink glow of an arcane essence, imbued with the energy of a pig, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Pig Essence",
        "Bottled Pig Essences",
        ["innoxia_pig", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_INNOXIA_PIG_SUBSPECIES_PIG", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_INNOXIA_RAPTOR_SUBSPECIES_FALCON = (
        "BOTTLED_ESSENCE_INNOXIA_RAPTOR_SUBSPECIES_FALCON",
        "",
        56,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling orange glow of an arcane essence, imbued with the energy of a raptor, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Raptor Essence",
        "Bottled Raptor Essences",
        ["innoxia_raptor", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_INNOXIA_RAPTOR_SUBSPECIES_FALCON", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_INNOXIA_SHEEP_SUBSPECIES_SHEEP = (
        "BOTTLED_ESSENCE_INNOXIA_SHEEP_SUBSPECIES_SHEEP",
        "",
        48,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling light grey glow of an arcane essence, imbued with the energy of a sheep, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Sheep Essence",
        "Bottled Sheep Essences",
        ["innoxia_sheep", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_INNOXIA_SHEEP_SUBSPECIES_SHEEP", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_LILIN = (
        "BOTTLED_ESSENCE_LILIN",
        "",
        10000,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling purple glow of an arcane essence, imbued with the energy of a lilin, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Lilin Essence",
        "Bottled Lilin Essences",
        ["RACE_LILIN", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_LILIN", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_MINTYCHIP_SALAMANDER_SUBSPECIES_FLAME = (
        "BOTTLED_ESSENCE_MINTYCHIP_SALAMANDER_SUBSPECIES_FLAME",
        "",
        160,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling flame red glow of an arcane essence, imbued with the energy of a salamander, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Salamander Essence",
        "Bottled Salamander Essences",
        ["mintychip_salamander", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_MINTYCHIP_SALAMANDER_SUBSPECIES_FLAME", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_NOSTEPONSNEK_CAPYBARA_SUBSPECIES_CAPYBARA = (
        "BOTTLED_ESSENCE_NOSTEPONSNEK_CAPYBARA_SUBSPECIES_CAPYBARA",
        "",
        40,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling dark brown glow of an arcane essence, imbued with the energy of a capybara, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Capybara Essence",
        "Bottled Capybara Essences",
        ["NoStepOnSnek_capybara", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_NOSTEPONSNEK_CAPYBARA_SUBSPECIES_CAPYBARA", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_NOSTEPONSNEK_OCTOPUS_SUBSPECIES_OCTOPUS = (
        "BOTTLED_ESSENCE_NOSTEPONSNEK_OCTOPUS_SUBSPECIES_OCTOPUS",
        "",
        40,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling royal purple glow of an arcane essence, imbued with the energy of an octopus, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Octopus Essence",
        "Bottled Octopus Essences",
        ["NoStepOnSnek_octopus", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_NOSTEPONSNEK_OCTOPUS_SUBSPECIES_OCTOPUS", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_NOSTEPONSNEK_SNAKE_SUBSPECIES_LAMIA = (
        "BOTTLED_ESSENCE_NOSTEPONSNEK_SNAKE_SUBSPECIES_LAMIA",
        "",
        40,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling lime glow of an arcane essence, imbued with the energy of a snake, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Snake Essence",
        "Bottled Snake Essences",
        ["NoStepOnSnek_snake", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_NOSTEPONSNEK_SNAKE_SUBSPECIES_LAMIA", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_RABBIT_MORPH = (
        "BOTTLED_ESSENCE_RABBIT_MORPH",
        "",
        48,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling dark brown glow of an arcane essence, imbued with the energy of a rabbit, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Rabbit Essence",
        "Bottled Rabbit Essences",
        ["RACE_RABBIT_MORPH", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_RABBIT_MORPH", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_RAT_MORPH = (
        "BOTTLED_ESSENCE_RAT_MORPH",
        "",
        40,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling dark brown glow of an arcane essence, imbued with the energy of a rat, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Rat Essence",
        "Bottled Rat Essences",
        ["RACE_RAT_MORPH", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_RAT_MORPH", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_REINDEER_MORPH = (
        "BOTTLED_ESSENCE_REINDEER_MORPH",
        "",
        72,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling dark brown glow of an arcane essence, imbued with the energy of a reindeer, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Reindeer Essence",
        "Bottled Reindeer Essences",
        ["RACE_REINDEER_MORPH", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_REINDEER_MORPH", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_SLIME = (
        "BOTTLED_ESSENCE_SLIME",
        "",
        40,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling light green glow of an arcane essence, imbued with the energy of a slime, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Slime Essence",
        "Bottled Slime Essences",
        ["RACE_SLIME", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_SLIME", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_SQUIRREL_MORPH = (
        "BOTTLED_ESSENCE_SQUIRREL_MORPH",
        "",
        40,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling ginger glow of an arcane essence, imbued with the energy of a squirrel, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Squirrel Essence",
        "Bottled Squirrel Essences",
        ["RACE_SQUIRREL_MORPH", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_SQUIRREL_MORPH", None, None, None, 0, 0)],
    )
    BOTTLED_ESSENCE_WOLF_MORPH = (
        "BOTTLED_ESSENCE_WOLF_MORPH",
        "",
        40,
        "A small glass bottle, with a little cork stopper wedged firmly in the top. Inside, the swirling black glow of an arcane essence, imbued with the energy of a wolf, flickers and swirls about in a mesmerising, cyclical pattern.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Bottled Wolf Essence",
        "Bottled Wolf Essences",
        ["RACE_WOLF_MORPH", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BOTTLED_ESSENCE_WOLF_MORPH", None, None, None, 0, 0)],
    )
    CANDI_CONTRABAND = (
        "CANDI_CONTRABAND",
        "",
        1000,
        "A box full of contraband lollipops, seized by the Enforcers up in the Harpy Nests, and very much desired by Candi.",
        "",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Box of Contraband Lollipops",
        "Boxes of Contraband Lollipops",
        ["BASE_PINK_DEEP", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [],
    )
    CANDI_HUNDRED_KISSES = (
        "CANDI_HUNDRED_KISSES",
        "",
        50000,
        "A limited-edition box containing a hundred differently-coloured lipsticks, produced by one of the most exclusive and upmarket cosmetics companies in all of Dominion.",
        "",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "'A Hundred Kisses'",
        "'A Hundred Kisses'",
        ["BASE_PINK_DEEP", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [],
    )
    CANDI_PERFUMES = (
        "CANDI_PERFUMES",
        "",
        500,
        "A couple of bottles of perfume which you collected from Kate at Succubi's Secrets. You need to deliver these to Candi back at the Enforcer Headquarters.",
        "",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex | EItemFlags.isPlural,
        "Candi's Perfumes",
        "Candi's Perfumes",
        ["BASE_ROSE", "BASE_PURPLE_LIGHT", "CLOTHING_BLACK"],
        [],
    )
    charisma_race_spider_chocolate_coated_cocoa_beans = (
        "charisma_race_spider_chocolate_coated_cocoa_beans",
        "A tag discreetly attached to the Chocolate Coated Coffee Beans informs you that they were made by a certain 'Charisma'.",
        250,
        "A metal tin containing several dozen coffee beans covered in dark chocolate. On one side of the tin there's a picture of a flushed spider-girl winking at the viewer, along with the cautionary words: <i>'Warning! Extremely alcoholic to spiders!'</i>",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isPlural
        | EItemFlags.isTransformative,
        "Chocolate Coated Coffee Beans",
        "Chocolate Coated Coffee Beans",
        ["charisma_spider_silk_white"],
        [],
    )
    charisma_race_spider_jet_black_coffee = (
        "charisma_race_spider_jet_black_coffee",
        "A tag discreetly attached to the Jet Black Coffee informs you that it was made by a certain 'Charisma'.",
        25,
        "A plastic bottle of cold-brew coffee, advertised as having an extremely high caffeine content. At the bottom of the bottle's label there's a picture of a spider-girl who's passed out drunk, and beneath her, the cautionary words: <i>'Warning! Extremely alcoholic to spiders!'</i>",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Jet Black Coffee",
        "Jet Black Coffees",
        ["ATTRIBUTE_PHYSIQUE"],
        [],
    )
    CIGARETTE = (
        "CIGARETTE",
        "",
        20,
        "A rolled up paper cylinder, fitted with a sponge-like filter, and packed with a combination of dried plant matter and aura-boosting supplements. It's been enchanted with a very weak fire spell, so that when placed in someone's mouth, it will self-ignite.",
        "a",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Starr Cigarette",
        "Starr Cigarettes",
        ["CLOTHING_ORANGE", "CLOTHING_BRASS", "CLOTHING_WHITE"],
        [ItemEffect("CIGARETTE", None, None, None, 0, 0)],
    )
    CIGARETTE_PACK = (
        "CIGARETTE_PACK",
        "",
        350,
        "An unopened, purple-and-white cardboard pack which contains twenty 'Starr Cigarettes'. According to the information printed on the back of the box, these cigarettes are both 'enhanced with aura-fortifying supplements' and 'guaranteed to make you look cool'.",
        "a pack of",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isPlural,
        "Starr Cigarettes",
        "Starr Cigarettes",
        ["CLOTHING_PURPLE_DARK", "CLOTHING_GOLD", "CLOTHING_BLACK"],
        [ItemEffect("CIGARETTE_PACK", None, None, None, 0, 0)],
    )
    CONDOM_USED = (
        "CONDOM_USED",
        "",
        1,
        "A used condom, tied at the top and filled with someone's cum. While most people would simply throw this away, those with a particularly dirty mind might find a use for it...",
        "a",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInSex | EItemFlags.isConsumedOnUse,
        "used condom",
        "used condoms",
        ["CLOTHING_WHITE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("USED_CONDOM_DRINK", None, None, None, 0, 0)],
    )
    CONDOM_USED_WEBBING = (
        "CONDOM_USED_WEBBING",
        "",
        1,
        "A used, condom-like sheath of spider's webbing, tied at the top and filled with someone's cum. While most people would simply throw this away, those with a particularly dirty mind might find a use for it...",
        "a",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInSex | EItemFlags.isConsumedOnUse,
        "used condom-webbing",
        "used condom-webbings",
        ["CLOTHING_WHITE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("USED_CONDOM_DRINK", None, None, None, 0, 0)],
    )
    DOLL_CONSOLE = (
        "DOLL_CONSOLE",
        "",
        120000,
        "The <i>'Doll's Expedient Customisation Kit'</i> is the only way to transform an autonomous sex doll. By plugging the cable into a doll's rear neck port, the user can modify a wide range of the connected doll's physical attributes.",
        "a",
        EItemFlags.isAbleToBeUsedFromInventory,
        "D.E.C.K.",
        "D.E.C.K.s",
        ["CLOTHING_BLUE_LIGHT", "CLOTHING_PINK_LIGHT", "CLOTHING_PURPLE"],
        [ItemEffect("DOLL_CONSOLE", None, None, None, 0, 0)],
    )
    dsg_eisek_apples = (
        "dsg_eisek_apples",
        'A faint wax stamp on the peel reads "Grown by the dragons of the DSG Farmer\'s Guild."',
        8,
        "Fruit from a carefully cultivated highland apple tree. The peel has a glossy shine to it, as if it has been polished.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "apples",
        "apples",
        ["BASE_RED"],
        [],
    )
    dsg_eisek_cartonobberries = (
        "dsg_eisek_cartonobberries",
        '"Grown by the dragons of the DSG Farmer\'s Guild" is lightly embossed under the carton.',
        5,
        "A carton of berries picked from a highbush blueberry plant. They have a faint, translucent glow about them when under sunlight.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "carton of blueberries",
        "cartons of blueberries",
        ["BASE_BLUE_DARK"],
        [],
    )
    dsg_eisek_cartonosberries = (
        "dsg_eisek_cartonosberries",
        '"Grown by the dragons of the DSG Farmer\'s Guild" is lightly embossed under the carton.',
        5,
        "A carton of berries picked from a domestic strawberry plant. They have a faint, translucent glow about them when under sunlight.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "carton of strawberries",
        "cartons of strawberries",
        ["BASE_CRIMSON"],
        [],
    )
    dsg_eisek_tangerines = (
        "dsg_eisek_tangerines",
        'A faint wax stamp on the peel reads "Grown by the dragons of the DSG Farmer\'s Guild."',
        8,
        "A winter citrus fruit that is less sour than many of its larger cousins. The peel has a glossy shine to it, as if it has been polished.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "tangerines",
        "tangerines",
        ["BASE_ORANGE"],
        [],
    )
    dsg_quest_awningpoles = (
        "dsg_quest_awningpoles",
        '"MFD by DSG Tarp and Awning Co." is burned into one section.',
        550,
        "Originally destined to become the haft of several battleaxes, this set of wooden poles has been re-purposed to hold up an awning instead.",
        "",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isFromExternalFile,
        "foldable awning poles",
        "foldable awning poles",
        ["CLOTHING_DESATURATED_BROWN", "CLOTHING_STEEL"],
        [],
    )
    dsg_quest_embsign = (
        "dsg_quest_embsign",
        "'Woven by the DSG Textile Guild' in very small print is woven in one corner.",
        2480,
        "A folded up piece of rectangular canvas fabric embroidered with a design that Monica had made for you based on your interpretation of the qualities that Eisek values most in his goods.",
        "",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isFromExternalFile,
        "embroidered sign",
        "embroidered signs",
        ["CLOTHING_TAN", "CLOTHING_BLUE_LIGHT"],
        [],
    )
    dsg_quest_fabricbolt = (
        "dsg_quest_fabricbolt",
        "'Woven by the DSG Textile Guild' is printed on one end of the board.",
        20,
        "A cut of dark green fabric wrapped around cardboard for easy transport. You selected the colour based on Eisek's stated preferences.",
        "",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isFromExternalFile,
        "bolt of green cloth",
        "bolts of green cloth",
        ["CLOTHING_GREEN_DARK", "CLOTHING_DESATURATED_BROWN"],
        [],
    )
    dsg_quest_hazmat_rat_card = (
        "dsg_quest_hazmat_rat_card",
        "'Printed by DSG Stationery Co.' is lightly embossed on the back.",
        100,
        "A plastic card, bearing a golden, holographic icon of a smiling rat's face. The writing on the front identifies it as a 'HappyRat Gold+ discount card', which can be used at 'any Hazmat Rat vending machine'.",
        "",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isFromExternalFile,
        "HappyRat Gold+ discount card",
        "HappyRat Gold+ discount cards",
        ["CLOTHING_GOLD"],
        [],
    )
    dsg_quest_sm_magicorb = (
        "dsg_quest_sm_magicorb",
        "'MFD by DSG Novelty Co.' is printed around one of the sphere's poles.",
        1000,
        "Supposedly a powerful arcane artefact capable of seeing into the future, it is actually just a novelty toy. Inside its spherical plastic shell, an icosahedral die floats in alcohol. The small window allows the wielder to view a random answer inscribed on the die as it floats up.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isFromExternalFile,
        "Mystical Orb of Fate",
        "Mystical Orbs of Fate",
        ["CLOTHING_BLACK", "CLOTHING_BLUE_DARK"],
        [],
    )
    dsg_race_bear_honey_bread = (
        "dsg_race_bear_honey_bread",
        "A faint inscription underneath the plate reads 'Packed by DSG Foodstuffs LLC'",
        250,
        "A plate of gingerbread made with honey split by a layer of plum jam and frosted with sugar frosting all underneath a glass cover. On the label is a pastoral painting of a bear-girl asleep at her kitchen table while raw dough sits out in the open and snow falls outside.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Honey Bread",
        "Honey Bread",
        ["dsg_bearBrown"],
        [],
    )
    dsg_race_bear_vodka = (
        "dsg_race_bear_vodka",
        "A faint inscription underneath the bottle reads 'Distilled by DSG Foodstuffs LLC'",
        200,
        """A glass bottle containing a highly alcoholic beverage made from potatoes.
		#IF(!game.isSillyModeEnabled())
		 The label depicts a bear-boy hard at work loading a still.
		#ELSE
		 The label depicts a bear-boy squatting in a tracksuit. Above him a speech bubble reads "Иди нахуй", which you assume is some kind of swear.
		#ENDIF
		""",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Vodka",
        "Vodkas",
        ["ATTRIBUTE_PHYSIQUE"],
        [],
    )
    dsg_race_dragon_draft_green = (
        "dsg_race_dragon_draft_green",
        "A faint inscription underneath the bottle reads 'Bottled by DSG Foodstuffs LLC'",
        500,
        "The recipe for this mysterious liquid and how it has maintained its carbonation for so long are both unknown. Even the dragons for whom this beverage was named after have mostly forgotten. Those who have tasted it have described it as tasting vaguely of prickly pears.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Green Dragon Draft",
        "Green Dragon Drafts",
        ["ATTRIBUTE_ARCANE"],
        [],
    )
    dsg_race_dragon_draft_orange = (
        "dsg_race_dragon_draft_orange",
        "A faint inscription underneath the bottle reads 'Bottled by DSG Foodstuffs LLC'",
        500,
        "The recipe for this mysterious liquid and how it has maintained its carbonation for so long are both unknown. Even the dragons for whom this beverage was named after have mostly forgotten. Those who have tasted it have described it as tasting vaguely of prickly pears.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Orange Dragon Draft",
        "Orange Dragon Drafts",
        ["ATTRIBUTE_PHYSIQUE"],
        [],
    )
    dsg_race_dragon_draft_pink = (
        "dsg_race_dragon_draft_pink",
        "A faint inscription underneath the bottle reads 'Bottled by DSG Foodstuffs LLC'",
        500,
        "The recipe for this mysterious liquid and how it has maintained its carbonation for so long are both unknown. Even the dragons for whom this beverage was named after have mostly forgotten. Those who have tasted it have described it as tasting vaguely of prickly pears.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Pink Dragon Draft",
        "Pink Dragon Drafts",
        ["ATTRIBUTE_ARCANE"],
        [],
    )
    dsg_race_dragon_dragonfruit_pink = (
        "dsg_race_dragon_dragonfruit_pink",
        'A faint wax stamp on the peel reads "Grown by the dragons of the DSG Farmer\'s Guild."',
        100,
        "Named for the scaly, spiky leaves on its exterior, this rare fruit was first cultivated by dragon-morphs centuries ago. Legend has it that every hidden grove where it now grows was once a dragon's home. This variety grows best in loam soils that remain moist year-round.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Pink Dragonfruit",
        "Pink Dragonfruits",
        ["dsg_ryuYellow"],
        [],
    )
    dsg_race_dragon_dragonfruit_red = (
        "dsg_race_dragon_dragonfruit_red",
        'A faint wax stamp on the peel reads "Grown by the dragons of the DSG Farmer\'s Guild."',
        100,
        "Named for the scaly, spiky leaves on its exterior, this rare fruit was first cultivated by dragon-morphs centuries ago. Legend has it that every hidden grove where it now grows was once a dragon's home. This variety grows best in poor, clay heavy soils.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Red Dragonfruit",
        "Red Dragonfruits",
        ["dsg_coatlJade"],
        [],
    )
    dsg_race_dragon_dragonfruit_yellow = (
        "dsg_race_dragon_dragonfruit_yellow",
        'A faint wax stamp on the peel reads "Grown by the dragons of the DSG Farmer\'s Guild."',
        100,
        "Named for the scaly, spiky leaves on its exterior, this rare fruit was first cultivated by dragon-morphs centuries ago. Legend has it that every hidden grove where it now grows was once a dragon's home. This variety grows best in rocky and sandy soils.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Yellow Dragonfruit",
        "Yellow Dragonfruits",
        ["dsg_dragonRed"],
        [],
    )
    dsg_race_ferret_orange_juice = (
        "dsg_race_ferret_orange_juice",
        "A faint inscription underneath the can reads 'Canned by DSG Foodstuffs LLC'",
        15,
        "A can of carbonated orange juice. The label depicts a ferret-boy wrapped around the branch of an orange tree, stretching his long body to reach for the fruit.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Fizzy Orange Juice",
        "Fizzy Orange Juices",
        ["ATTRIBUTE_ARCANE"],
        [],
    )
    dsg_race_ferret_sausages = (
        "dsg_race_ferret_sausages",
        "A faint inscription underneath the plate reads 'Packed by DSG Foodstuffs LLC'",
        250,
        "A plate of high quality sausages accompanied by seasoned tomatoes and cucumbers all underneath a glass cover. The label on the cover depicts a nude ferret-girl straddling a similar, but comically over-sized sausage, coquettishly hiding her nipples from the viewer.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Sausages",
        "Sausages",
        ["dsg_ferretCinnamon"],
        [],
    )
    dsg_race_gryphon_pate_and_crackers = (
        "dsg_race_gryphon_pate_and_crackers",
        "A faint inscription printed on the packaging reads 'Packed by DSG Foodstuffs LLC'",
        350,
        "A small tin of meat pâté and crackers wrapped in a wax paper package. The label on package is a cartoony depiction a smiling gryphon-boy with crumbs all around his beak.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Pâté and Crackers",
        "Tuna Sashimi",
        ["dsg_gryphonBlue"],
        [],
    )
    dsg_race_gryphon_two_tone_slushie = (
        "dsg_race_gryphon_two_tone_slushie",
        "A faint inscription underneath the cup reads 'Mixed by DSG Foodstuffs LLC'",
        20,
        "A kiwi-and-strawberry flavored two-tone slushie. The label depicts a gryphon-girl wearing only an apron handing a fountain cup resembling this one out towards an imaginary customer.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Two-Tone Slushie",
        "Grogs",
        ["ATTRIBUTE_ARCANE"],
        [],
    )
    dsg_race_otter_berry_soda = (
        "dsg_race_otter_berry_soda",
        "A faint inscription underneath the bottle reads 'Distilled by DSG Foodstuffs LLC'",
        15,
        """An aluminium can of berry-flavoured soda. The label on the can depicts an otter-boy in some swim trunks sitting on the bank of a river while drinking from a similar can.
		""",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Berry Soda",
        "Berry Sodas",
        ["ATTRIBUTE_ARCANE"],
        [],
    )
    dsg_race_otter_fish_and_chips = (
        "dsg_race_otter_fish_and_chips",
        "A faint inscription underneath the box reads 'Packed by DSG Foodstuffs LLC'",
        250,
        "A takeaway box of deep fried potatoes and battered cod with a dip made from mashed peas. On the box is a portrait of an otter-girl in a barmaid costume winking at the viewer.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Fish and Chips",
        "Fish and Chips",
        ["dsg_otterBrown"],
        [],
    )
    dsg_race_raccoon_cotton_candy_soda = (
        "dsg_race_raccoon_cotton_candy_soda",
        "A faint inscription underneath the can reads 'Canned by DSG Foodstuffs LLC'",
        10,
        "A large translucent plastic cup of blue raspberry cotton candy flavoured soda. The label depicts a raccoon-boy in a lab coat dangling a wad of cotton candy over a bowl of carbonated water.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Cotton Candy Soda",
        "Cotton Candy Sodas",
        ["ATTRIBUTE_ARCANE"],
        [],
    )
    dsg_race_raccoon_popcorn = (
        "dsg_race_raccoon_popcorn",
        "A faint inscription underneath the plate reads 'Packed by DSG Foodstuffs LLC'",
        15,
        "A plastic bag of lightly buttered popcorn. The label on the cover depicts a raccoon-girl in denim overalls triumphantly raising an ear of corn above her while popcorn kernels explode off of it.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Popcorn",
        "Popcorn",
        ["dsg_raccoonGrey"],
        [],
    )
    dsg_race_shark_grog = (
        "dsg_race_shark_grog",
        "A faint inscription underneath the bottle reads 'Mixed by DSG Foodstuffs LLC'",
        200,
        "A glass bottle of traditional navel grog made with rum, lemon juice, and cinnamon. The label depicts a shark-boy in a pirate costume picking his teeth with a cutlass.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Grog",
        "Grogs",
        ["ATTRIBUTE_PHYSIQUE"],
        [],
    )
    dsg_race_shark_tuna_sashimi = (
        "dsg_race_shark_tuna_sashimi",
        "A faint inscription underneath the plate reads 'Packed by DSG Foodstuffs LLC'",
        300,
        "A plate of tuna-belly sashimi with wasabi and decorative greens, all underneath a glass cover. The label on the cover depicts a bikini-clad shark-girl with a fishing rod over her shoulder and a toothy grin on her face.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Tuna Sashimi",
        "Tuna Sashimi",
        ["dsg_sharkGrey"],
        [],
    )
    dsg_wetwipes_wetwipes = (
        "dsg_wetwipes_wetwipes",
        '"MFD by DSG Sanitary Supply GmbH" is lightly embossed on one edge of the packaging.',
        10,
        "Some foil wrapped, alcohol soaked cotton wipes.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isPlural,
        "wet wipes",
        "wet wipes",
        ["CLOTHING_WHITE", "CLOTHING_BLUE", "CLOTHING_BLUE"],
        [],
    )
    dsg_wetwipes_wetwipes_plus = (
        "dsg_wetwipes_wetwipes_plus",
        '"MFD by DSG Sanitary Supply GmbH" is lightly embossed on one edge of the packaging.',
        50,
        "Some foil wrapped, alcohol soaked cotton wipes. The writing on the rear of the packet declares that these wet wipes have been 'arcane-enhanced to remove all odours'.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isPlural,
        "arcane wet wipes",
        "arcane wet wipes",
        ["CLOTHING_WHITE", "CLOTHING_GOLD", "CLOTHING_GOLD"],
        [],
    )
    DYE_BRUSH = (
        "DYE_BRUSH",
        "",
        150,
        "A small, very ordinary-looking brush, of the sort used for fine detailing on canvas or models. On closer inspection, you notice a very faint purple glow emanating from the brush's tip, revealing its true nature as an arcane-enchanted dye-brush.",
        "a",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isConsumedOnUse,
        "dye-brush",
        "dye-brushes",
        ["CLOTHING_WHITE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("DYE_BRUSH", None, None, None, 0, 0)],
    )
    EGGPLANT = (
        "EGGPLANT",
        "",
        25,
        "A delicate, tropical perennial often cultivated as a tender or half-hardy annual in temperate climates. Also it kind of looks like a penis if you squint.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Eggplant",
        "Eggplants",
        ["GENERIC_ARCANE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EGGPLANT", None, None, None, 0, 0)],
    )
    EGGPLANT_POTION = (
        "EGGPLANT_POTION",
        "",
        250,
        "A potion made from the bitter flesh of an eggplant. Just like the berry from which it was made, the bottle containing this potion sort of looks like a penis if you squint at it.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isTransformative,
        "Eggplant Potion",
        "Eggplant Potions",
        ["GENERIC_ARCANE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [],
    )
    ELIXIR = (
        "ELIXIR",
        "",
        750,
        "Created by infusing arcane essences with a consumable item, elixirs such as these can hold a huge number of transformative effects. As elixir creation is limited to those with a high level of arcane proficiency, such as demons, they are quite rare, and fetch a high price.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isTransformative,
        "elixir",
        "elixirs",
        ["CLOTHING_PINK", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [],
    )
    FETISH_REFINED = (
        "FETISH_REFINED",
        "",
        750,
        "A vial of bubbling pink liquid, which was refined from a bottle of 'Succubus's Kiss'. Its potent enchantment is very different from the effects of the liquid that it was distilled from, and is able to add or remove specific fetishes.",
        "a vial of",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFetishGiving,
        "Fetish Endowment",
        "Fetish Endowments",
        ["FETISH", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [],
    )
    FETISH_UNREFINED = (
        "FETISH_UNREFINED",
        "",
        500,
        "A heart-shaped glass bottle, filled with a swirling, glowing-pink liquid. The words 'Succubus's Kiss' have been etched into the bottle in an ornate script, and beneath that the words 'Fool-proof love potion' make it clear what this liquid will do.",
        "a bottle of",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Succubus's Kiss",
        "Succubus's Kisses",
        ["CLOTHING_PINK", "CLOTHING_PINK_LIGHT", "CLOTHING_WHITE", "CLOTHING_PINK_DARK"],
        [ItemEffect("MYSTERY_KINK", None, None, None, 0, 0)],
    )
    GIFT_CHOCOLATES = (
        "GIFT_CHOCOLATES",
        "",
        300,
        "A box filled with various chocolates.",
        "a box of",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isPlural,
        "Chocolates",
        "Chocolates",
        ["BASE_TAN", "BASE_BROWN_DARK", "BASE_YELLOW"],
        [ItemEffect("GIFT_CHOCOLATES", None, None, None, 0, 0)],
    )
    GIFT_PERFUME = (
        "GIFT_PERFUME",
        "",
        300,
        "A small bottle of perfume.",
        "a bottle of",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Rose Perfume",
        "Rose Perfumes",
        ["BASE_ROSE", "BASE_PURPLE_LIGHT", "CLOTHING_BLACK"],
        [ItemEffect("GIFT_PERFUME", None, None, None, 0, 0)],
    )
    GIFT_ROSE_BOUQUET = (
        "GIFT_ROSE_BOUQUET",
        "",
        500,
        "A bouquet filled with roses of many colours, it smells pleasant even from a distance.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Rose Bouquet",
        "Rose Bouquets",
        ["BASE_RED", "BASE_ORANGE", "BASE_YELLOW"],
        [],
    )
    GIFT_TEDDY_BEAR = (
        "GIFT_TEDDY_BEAR",
        "",
        600,
        "A cute brown teddy bear, with the words 'Hug me!' sewed onto a little heart that it's holding.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Teddy Bear",
        "Teddy Bears",
        ["BASE_TAN", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [],
    )
    HARPY_MATRIARCH_BIMBO_LOLLIPOP = (
        "HARPY_MATRIARCH_BIMBO_LOLLIPOP",
        "",
        1250,
        "A swirly lollipop that you got from the harpy matriarch [bimboHarpy.name]. Although it doesn't look out of the ordinary, you're pretty sure that eating it would result in a potent transformation...",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isTransformative,
        "[bimboHarpy.namePos] lollipop",
        "[bimboHarpy.namePos] lollipops",
        ["RARITY_LEGENDARY", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("BIMBO_LOLLIPOP", None, None, None, 0, 0)],
    )
    HARPY_MATRIARCH_DOMINANT_PERFUME = (
        "HARPY_MATRIARCH_DOMINANT_PERFUME",
        "",
        1250,
        "A bottle of perfume that you got from the harpy matriarch [dominantHarpy.name]. Although it looks to contain normal perfume, you're pretty sure that using it would result in a potent transformation...",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isTransformative,
        "[dominantHarpy.namePos] perfume",
        "[dominantHarpy.namePos] perfumes",
        ["RARITY_LEGENDARY", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("DOMINANT_PERFUME", None, None, None, 0, 0)],
    )
    HARPY_MATRIARCH_NYMPHO_LOLLIPOP = (
        "HARPY_MATRIARCH_NYMPHO_LOLLIPOP",
        "",
        1250,
        "A cock-shaped lollipop that you got from the harpy matriarch [nymphoHarpy.name]. Although it looks to be made from regular candy, you're pretty sure that eating it would result in a potent transformation...",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isTransformative,
        "[nymphoHarpy.namePos] lollipop",
        "[nymphoHarpy.namePos] lollipops",
        ["RARITY_LEGENDARY", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("NYMPHO_LOLLIPOP", None, None, None, 0, 0)],
    )
    IMP_FORTRESS_ARCANE_KEY = (
        "IMP_FORTRESS_ARCANE_KEY",
        "",
        0,
        "An arcane key that you obtained from Fyrsia, the leader of an imp fortress in Submission. When used in combination with keys obtained from the other two fortresses, it will grant entry to the central citadel.",
        "",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Fyrsia's Key",
        "Fyrsia's Keys",
        ["CLOTHING_SILVER", "GENERIC_ARCANE", "CLOTHING_BLACK"],
        [],
    )
    IMP_FORTRESS_ARCANE_KEY_2 = (
        "IMP_FORTRESS_ARCANE_KEY_2",
        "",
        0,
        "An arcane key that you obtained from Jhortrax, the leader of an imp fortress in Submission. When used in combination with keys obtained from the other two fortresses, it will grant entry to the central citadel.",
        "",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Jhortrax's Key",
        "Jhortrax's Keys",
        ["CLOTHING_STEEL", "GENERIC_ARCANE", "CLOTHING_BLACK"],
        [],
    )
    IMP_FORTRESS_ARCANE_KEY_3 = (
        "IMP_FORTRESS_ARCANE_KEY_3",
        "",
        0,
        "An arcane key that you obtained from Hyorlyss, the leader of an imp fortress in Submission. When used in combination with keys obtained from the other two fortresses, it will grant entry to the central citadel.",
        "",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Hyorlyss's Key",
        "Hyorlyss's Keys",
        ["CLOTHING_GOLD", "GENERIC_ARCANE", "CLOTHING_BLACK"],
        [],
    )
    innoxia_cheat_inno_chans_gift = (
        "innoxia_cheat_inno_chans_gift",
        "",
        100000000,
        "This is no ordinary bottle of wine, as it turns anyone who drinks it into a youko! Additional consumption will increase the number of Youko tails the drinker has.<br/>[style.italicsMinorGood(While this is a debug-only item, it should be safe to use anywhere.)]",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Inno-chan's gift",
        "Inno-chan's gifts",
        ["RACE_FOX_MORPH"],
        [],
    )
    innoxia_cheat_unlikely_whammer = (
        "innoxia_cheat_unlikely_whammer",
        "",
        80,
        "Although this may look and taste like an ordinary burger, the packaging declares this 'Improbable-Whammer' to be 100% meat free. Printed in very small, hard-to-read text, there's a disclaimer which states that the meat substitute is 'dangerously high in female hormones'...",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Improbable-Whammer",
        "Improbable-Whammer",
        ["GENERIC_SEX"],
        [],
    )
    innoxia_food_doughnut = (
        "innoxia_food_doughnut",
        "",
        15,
        "A small ring of sweetened dough that's been fried and finished with a light glaze.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "doughnut",
        "doughnuts",
        ["BASE_BROWN_LIGHT"],
        [],
    )
    innoxia_food_doughnut_iced = (
        "innoxia_food_doughnut_iced",
        "",
        20,
        "A small ring of sweetened dough that's been fried and topped with plain icing.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "iced doughnut",
        "iced doughnuts",
        ["BASE_WHITE"],
        [],
    )
    innoxia_food_doughnut_iced_sprinkles = (
        "innoxia_food_doughnut_iced_sprinkles",
        "",
        25,
        "A small ring of sweetened dough that's been fried and topped with icing and sprinkles.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "iced doughnut with sprinkles",
        "iced doughnuts with sprinkles",
        ["BASE_BROWN_DARK", "BASE_WHITE"],
        [],
    )
    innoxia_pills_broodmother = (
        "innoxia_pills_broodmother",
        "",
        50,
        "A small, pink pill, individually packaged in a foil and plastic wrapper. While the text printed on the foil identifies this pill as an 'Orally-Administered Fecundity Enhancer', it's colloquially known as a 'broodmother pill'. Its main effect is to make the user conceive far more offspring than usual.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "broodmother pill",
        "broodmother pills",
        ["CLOTHING_PINK"],
        [],
    )
    innoxia_pills_fertility = (
        "innoxia_pills_fertility",
        "",
        20,
        "A small, light-purple pill, individually packaged in a foil and plastic wrapper. While the text printed on the foil identifies this pill as an 'Orally-Administered Reproduction Enhancer', it's colloquially known as a 'breeder pill', and temporarily boosts both fertility and virility when ingested.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "breeder pill",
        "breeder pills",
        ["CLOTHING_PURPLE_LIGHT"],
        [],
    )
    innoxia_pills_sterility = (
        "innoxia_pills_sterility",
        "",
        20,
        "A small, blue pill, individually packaged in a foil and plastic wrapper. While the text printed on the foil identifies this pill as an 'Orally-Administered Reproduction Inhibitor', it's colloquially known as either a 'sterility pill' or 'slut pill', and temporarily reduces both fertility and virility when ingested.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "sterility pill",
        "sterility pills",
        ["CLOTHING_BLUE"],
        [],
    )
    innoxia_potions_amazonian_ambrosia = (
        "innoxia_potions_amazonian_ambrosia",
        "",
        5000,
        "A miniature terracotta amphora, filled with a heady red wine. The description on the label is terse, but promises to 'make an Amazon' out of anyone who drinks it.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Amazonian Ambrosia",
        "Amazonian Ambrosias",
        ["CLOTHING_DESATURATED_BROWN"],
        [],
    )
    innoxia_potions_amazons_secret = (
        "innoxia_potions_amazons_secret",
        "",
        500,
        "A delicate, slender glass bottle, filled with a viscous pink liquid. A label on the rear of the bottle declares that it 'enables impregnation via orgasmic vaginal contact for up to twenty four hours after ingestion'.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Amazon's Secret",
        "Amazon's Secrets",
        ["BASE_PINK"],
        [],
    )
    innoxia_quest_angelixx_diary = (
        "innoxia_quest_angelixx_diary",
        "",
        0,
        "A small, pink diary, with the name 'Angelixx' written on the front in flowing white script.",
        "",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isFromExternalFile,
        "Angelixx's diary",
        "Angelixx's diaries",
        ["CLOTHING_PINK"],
        [],
    )
    innoxia_quest_bank_card = (
        "innoxia_quest_bank_card",
        "",
        10000,
        "A plastic card which declares the holder as possessing a platinum account with the Bank of Dominion, thereby identifying them as a direct descendant of an elder lilin.",
        "",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isFromExternalFile,
        "platinum Bank of Dominion card",
        "platinum Bank of Dominion cards",
        ["CLOTHING_PLATINUM"],
        [],
    )
    innoxia_quest_clothing_keys = (
        "innoxia_quest_clothing_keys",
        "",
        0,
        "A collection of small keys, each of which unlocks an item of clothing...",
        "",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isFromExternalFile | EItemFlags.isPlural,
        "clothing keys",
        "clothing keys",
        ["CLOTHING_STEEL"],
        [],
    )
    innoxia_quest_faire_ticket = (
        "innoxia_quest_faire_ticket",
        "",
        0,
        "A type of fairground ticket given out at the Farmer's Market in Elis. It can be traded in at some of the stalls for special rewards.",
        "",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isFromExternalFile,
        "Faire ticket",
        "Faire tickets",
        ["CLOTHING_GREEN_LIME"],
        [],
    )
    innoxia_quest_gym_membership_card = (
        "innoxia_quest_gym_membership_card",
        "",
        8000,
        "A cardboard card with laminate coating, declaring the holder as being a member of the gym, 'Pix's Playground'.",
        "",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isFromExternalFile,
        "'Pix's Playground' membership card",
        "'Pix's Playground' membership card",
        ["CLOTHING_BLACK"],
        [],
    )
    innoxia_quest_minotallys_pass = (
        "innoxia_quest_minotallys_pass",
        "",
        1000,
        "A plastic card which grants the holder access to the tower of Elis's town hall.",
        "",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isFromExternalFile,
        "Elis Town Hall Pass",
        "Elis Town Hall Passes",
        ["CLOTHING_GOLD"],
        [],
    )
    innoxia_quest_monica_milker = (
        "innoxia_quest_monica_milker",
        "",
        50,
        "A manual cow-themed breast pump, complete with plastic beaker, which used to belong to Monica.",
        "",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isFromExternalFile,
        "Monica's Moo Milker",
        "Monica's Moo Milkers",
        ["CLOTHING_YELLOW"],
        [],
    )
    innoxia_quest_oglix_beer_token = (
        "innoxia_quest_oglix_beer_token",
        "",
        0,
        "A token which was given to you by Oglix upon purchase of a pint of beer. Collecting five of these tokens allows you to trade them in for a fuck of Oglix's beer-bitch.",
        "",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isFromExternalFile,
        "beer-bitch token",
        "beer-bitch tokens",
        ["CLOTHING_GREEN", "CLOTHING_IRON"],
        [],
    )
    innoxia_quest_recorder = (
        "innoxia_quest_recorder",
        "",
        50000,
        "An arcane recorder given to you by Wes. Resembling an old-fashioned camcorder, this device functions in much the same way as any old video recorder from your old reality.",
        "",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isFromExternalFile,
        "arcane recorder",
        "arcane recorders",
        ["CLOTHING_TAN"],
        [],
    )
    innoxia_quest_special_pass = (
        "innoxia_quest_special_pass",
        "",
        5000,
        "Complete with a sturdy fabric lanyard, and signed by Superintendent Wesley Wormwood himself, this pass grants the bearer access to the requisitions area of Dominion's Enforcer HQ. It can also be shown to Enforcer patrols to prevent illegal items from being confiscated.",
        "",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isFromExternalFile,
        "Wes's Contractor Pass",
        "Wes's Contractor Pass",
        ["CLOTHING_BLUE_LIGHT"],
        [],
    )
    innoxia_quest_special_pass_elle = (
        "innoxia_quest_special_pass_elle",
        "",
        5000,
        "Complete with a sturdy fabric lanyard, and signed by Superintendent Aellasys Lunettemartu herself, this pass grants the bearer access to the requisitions area of Dominion's Enforcer HQ. It can also be shown to Enforcer patrols to prevent illegal items from being confiscated.",
        "",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isFromExternalFile,
        "Elle's Contractor Pass",
        "Elle's Contractor Pass",
        ["CLOTHING_PURPLE_LIGHT"],
        [],
    )
    innoxia_race_alligator_gators_gumbo = (
        "innoxia_race_alligator_gators_gumbo",
        "",
        100,
        "A novelty, individual-sized cast iron bowl, complete with a resealable lid. The contents take the form of a delicious-smelling variety of gumbo, which contains meat, okra, and a variety of other mysterious vegetables.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Gator's Gumbo",
        "Gator's Gumbo",
        ["RACE_ALLIGATOR_MORPH"],
        [],
    )
    innoxia_race_alligator_swamp_water = (
        "innoxia_race_alligator_swamp_water",
        "",
        40,
        "A sturdy glass bottle filled with what appears to be some kind of moonshine. A label on the front shows a greater alligator-boy receiving oral from an alligator-girl while drinking from a bottle just like this one.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Swamp Water",
        "Swamp Waters",
        ["ATTRIBUTE_PHYSIQUE"],
        [],
    )
    innoxia_race_angel_angels_tears = (
        "innoxia_race_angel_angels_tears",
        "",
        100000,
        "A delicate glass vial full of a light turquoise liquid.\u200b There's an image of a weeping angel engraved into the glass, and you see that her tears are falling into a vial just like this one.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Angel's Tears",
        "Angel's Tears",
        ["ATTRIBUTE_CORRUPTION"],
        [],
    )
    innoxia_race_badger_berry_trifle = (
        "innoxia_race_badger_berry_trifle",
        "",
        40,
        "A small plastic pot, containing a delicious-looking berry trifle. A stylised pair of white stripes streak across the lid's black background, and from the description printed on the pot's side, you can safely deduce that this trifle is advertised towards badger-morphs.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Berry Trifle",
        "Berry Trifles",
        ["innoxia_badger"],
        [],
    )
    innoxia_race_badger_black_stripes_perry = (
        "innoxia_race_badger_black_stripes_perry",
        "",
        40,
        "A glass bottle containing pear cider, known more commonly as 'perry'. A stylised image of a badger's face is displayed upon the bottle's neck, while the label declares it to be of the brand 'Black Stripes'.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Black Stripes Perry",
        "Black Stripes Perries",
        ["ATTRIBUTE_PHYSIQUE"],
        [],
    )
    innoxia_race_bat_fruit_bats_juice_box = (
        "innoxia_race_bat_fruit_bats_juice_box",
        "",
        20,
        "A small cardboard carton, labelled as 'Fruit Bat's Juice Box'. On one side of the carton, there's an image of a scantily-clad bat-girl squeezing the juice from all sorts of fruit over her breasts.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Fruit Bat's Juice Box",
        "Fruit Bat's Juice Boxes",
        ["ATTRIBUTE_ARCANE"],
        [],
    )
    innoxia_race_bat_fruit_bats_salad = (
        "innoxia_race_bat_fruit_bats_salad",
        "",
        40,
        "A little plastic pot, containing a delicious-looking fruit salad. Printed into the film lid, there's a little picture of a bat-girl sitting cross-legged as she eats her way through a mountain of fruit.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Fruit Bat's Salad",
        "Fruit Bat's Salad",
        ["RACE_BAT_MORPH"],
        [],
    )
    innoxia_race_cat_felines_fancy = (
        "innoxia_race_cat_felines_fancy",
        "",
        150,
        "A delicate glass bottle filled with a thick, cream-like liquid. A label on the front shows a pair of cat-girls lovingly kissing one another, with the dominant partner slipping a hand down between her partner's legs.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Feline's Fancy",
        "Feline's Fancies",
        ["ATTRIBUTE_ARCANE"],
        [],
    )
    innoxia_race_cat_kittys_reward = (
        "innoxia_race_cat_kittys_reward",
        "",
        30,
        "A small, square food tin with a ring-pull lid. A label on the side shows a greater cat-girl devouring a plate of what looks to be this can's contents; some sort of tinned salmon.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Kitty's Reward",
        "Kitty's Rewards",
        ["RACE_CAT_MORPH"],
        [],
    )
    innoxia_race_cow_bubble_cream = (
        "innoxia_race_cow_bubble_cream",
        "",
        20,
        "A small pot of yoghurt, with a black-and-white cow-pattern styled onto the lid.  A label on the side declares it to be 'Bubble Cream', which seems to be a little misleading, as there isn't any sort of bubbling going on in the creamy mixture contained within.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Bubble Cream",
        "Bubble Creams",
        ["RACE_COW_MORPH"],
        [],
    )
    innoxia_race_cow_bubble_milk = (
        "innoxia_race_cow_bubble_milk",
        "",
        20,
        "The thick glass bottle of 'Bubble Milk' appears to contain, much as its name would suggest, a generous helping of milk. Looking through the glass, you see that there are little bubbles fizzing up in the liquid within, making this milk appear to be carbonated.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Bubble Milk",
        "Bubble Milks",
        ["ATTRIBUTE_PHYSIQUE"],
        [],
    )
    innoxia_race_deer_glade_springs = (
        "innoxia_race_deer_glade_springs",
        "",
        20,
        "A glass bottle of sparkling water bearing the brand name 'Glade Springs'. On the reverse label, a muscular stag-boy is pictured post-coitus with two deer-girls, and is drinking a bottle of this sparkling water.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Glade Springs",
        "Glade Springs",
        ["ATTRIBUTE_PHYSIQUE"],
        [],
    )
    innoxia_race_deer_tree_shoots_salad = (
        "innoxia_race_deer_tree_shoots_salad",
        "",
        30,
        "A plastic bag containing a somewhat bland-looking assortment of green leaves, stems, and shoots. On the reverse of the bag it proudly declares that it 'contains all the nutrients a deer needs', beside a picture of a naked deer-girl fondling her breasts.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Tree Shoots Salad",
        "Tree Shoots Salads",
        ["innoxia_deer"],
        [],
    )
    innoxia_race_demon_innoxias_gift = (
        "innoxia_race_demon_innoxias_gift",
        "",
        100000000,
        "Once thought to be lost forever, this bottle of bubbling pink liquid has made a surprise return, and can turn anyone who drinks it into a demon!<br/>[style.italicsMinorGood(While this is a debug-only item, it should be safe to use anywhere.)]",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Innoxia's Gift",
        "Innoxia's Gifts",
        ["ATTRIBUTE_CORRUPTION"],
        [],
    )
    innoxia_race_demon_liliths_gift = (
        "innoxia_race_demon_liliths_gift",
        "",
        1500,
        "A glass bottle, filled with bubbling pink liquid. On the bottle's label, there's an image of a succubus's perfectly-formed, heart-shaped ass. Her delicate hands are reaching down to pull apart her soft ass cheeks, fully exposing her tight asshole and dripping-wet pussy.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Lilith's Gift",
        "Lilith's Gifts",
        ["ATTRIBUTE_CORRUPTION"],
        [],
    )
    innoxia_race_dog_canine_crunch = (
        "innoxia_race_dog_canine_crunch",
        "",
        25,
        "An individually-wrapped cookie baked in the shape of a bone. It's covered in icing and sprinkles, and while the packaging declares that the recipe has been specially formulated for dog-morphs, it's completely edible for all races.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Canine Crunch",
        "Canine Crunches",
        ["RACE_DOG_MORPH"],
        [],
    )
    innoxia_race_dog_canine_crush = (
        "innoxia_race_dog_canine_crush",
        "",
        35,
        "A glass bottle of what looks to be some kind of beer. A label on the front shows a beautiful dog-girl down on all fours presenting her naked, dripping pussy to a greater dog-boy's erect, knotted dog-cock.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Canine Crush",
        "Canine Crushes",
        ["ATTRIBUTE_PHYSIQUE"],
        [],
    )
    innoxia_race_doll_silic_oil = (
        "innoxia_race_doll_silic_oil",
        "",
        225,
        "A plastic bottle full of 'Silic Oil'. It claims to be a lubricant for 'sex toys, dolls, and heavy machinery', and bears large letters reading 'Do Not Drink'. The instructions on the back go on to say that it's safe for consumption only for dolls, which should result in the doll's orifices getting wetter.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInSex | EItemFlags.isConsumedOnUse | EItemFlags.isFromExternalFile,
        "Silic Oil",
        "Silic Oils",
        ["GENERIC_SEX"],
        [],
    )
    innoxia_race_fox_chicken_pot_pie = (
        "innoxia_race_fox_chicken_pot_pie",
        "",
        75,
        "A small tin containing a pie with a mix of vegetables and meat. While plenty of omnivorous races enjoy the taste of these pies, they are a particular favourite of fox-morphs.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Chicken Pot Pie",
        "Chicken Pot Pies",
        ["RACE_FOX_MORPH"],
        [],
    )
    innoxia_race_fox_vulpines_vineyard = (
        "innoxia_race_fox_vulpines_vineyard",
        "",
        250,
        "A sturdy glass bottle filled with red wine. A bunch of grapes is painted onto the front of the label, while on the bottom of the bottle itself, the image of a snickering fox-morph is etched into the glass.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Vulpine's Vineyard",
        "Vulpine's Vineyards",
        ["ATTRIBUTE_ARCANE"],
        [],
    )
    innoxia_race_goat_billys_best = (
        "innoxia_race_goat_billys_best",
        "",
        45,
        "A small, circular package containing a very strong goat cheese. The branding on the top of the package declares it to be 'Billy's Best', while the label underneath shows a naked goat-boy, who you assume to be Billy, showing off his muscled body.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Billy's Best",
        "Billy's Bests",
        ["innoxia_goat"],
        [],
    )
    innoxia_race_goat_pans_flute = (
        "innoxia_race_goat_pans_flute",
        "",
        180,
        "A thick glass bottle containing a type of high-quality champagne. The bottle's label declares it to be of the brand 'Pan's Flute', which seems to refer both to the name of a champagne glass and to the brand's symbol of a pan flute.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Pan's Flute",
        "Pan's Flutes",
        ["ATTRIBUTE_PHYSIQUE"],
        [],
    )
    innoxia_race_harpy_bubblegum_lollipop = (
        "innoxia_race_harpy_bubblegum_lollipop",
        "",
        10,
        "A bright pink lollipop, with a little ball of gum at its core. Although it doesn't look out of the ordinary, it's somewhat unusual in the fact that it has an incredibly strong smell of bubblegum.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Bubblegum Lollipop",
        "Bubblegum Lollipop",
        ["RACE_HARPY"],
        [],
    )
    innoxia_race_harpy_harpy_perfume = (
        "innoxia_race_harpy_harpy_perfume",
        "",
        250,
        "A bright pink glass bottle of what looks to be a kind of feminine perfume. There's a stylised image of a harpy's feather etched into the base of the bottle, but other than that, it bears no markings.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Harpy Perfume",
        "Harpy Perfumes",
        ["GENERIC_SEX"],
        [],
    )
    innoxia_race_horse_equine_cider = (
        "innoxia_race_horse_equine_cider",
        "",
        60,
        "The thick glass bottle of 'Equine Cider' appears to contain, much as its name would suggest, a generous helping of some sort of alcoholic cider. On the label, there's an incredibly lewd illustration of a horse-boy slamming his massive cock deep into a girl's eager pussy.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Equine Cider",
        "Equine Ciders",
        ["ATTRIBUTE_PHYSIQUE"],
        [],
    )
    innoxia_race_horse_sugar_carrot_cube = (
        "innoxia_race_horse_sugar_carrot_cube",
        "",
        15,
        "A bright orange sugar cube, which smells of carrots. From the horse-shoe icon on the wrapper, this is obviously meant as a snack for horse-morphs, but is nevertheless edible for all races.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Sugar Carrot Cube",
        "Sugar Carrot Cube",
        ["RACE_HORSE_MORPH"],
        [],
    )
    innoxia_race_human_bread_roll = (
        "innoxia_race_human_bread_roll",
        "",
        15,
        "A perfectly average bread roll, wrapped in a small, brown paper bag. A logo printed on one side of this wrapper shows a portly human chef giving you a thumbs-up while surrounded by all manner of delicious-looking baked goods.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Bread Roll",
        "Bread Rolls",
        ["RACE_HUMAN"],
        [],
    )
    innoxia_race_human_vanilla_water = (
        "innoxia_race_human_vanilla_water",
        "",
        10,
        "A plastic bottle filled with what appears to be nothing but water. While there's no label on the bottle, there is a slight indentation in its surface, and, holding it up to the light to get a better look, you see that the impression spells the words 'Vanilla Water'.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Vanilla Water",
        "Vanilla Waters",
        ["ATTRIBUTE_ARCANE"],
        [],
    )
    innoxia_race_imp_impish_brew = (
        "innoxia_race_imp_impish_brew",
        "",
        10,
        "A cracked and dirty glass bottle, filled with a creamy-yellow liquid. There's no label, but someone's helpfully, albeit crudely, written 'Impish Brew' in black marker pen on one side. You think you can guess what the thick, musky liquid is...",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Impish Brew",
        "Impish Brews",
        ["ATTRIBUTE_CORRUPTION"],
        [],
    )
    innoxia_race_mouse_barley_water = (
        "innoxia_race_mouse_barley_water",
        "",
        50,
        "A bottle of barley water, bearing the brand of 'Harvest Mouse'. On the reverse label, there's a lewd image of a mouse-girl hiking up her skirt to show off her tight, wet pussy.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Harvest Mouse's barley water",
        "Harvest Mouse's barley waters",
        ["ATTRIBUTE_ARCANE"],
        [],
    )
    innoxia_race_mouse_cheese = (
        "innoxia_race_mouse_cheese",
        "",
        75,
        "A wedge of holey cheese, wrapped in plastic and bearing a 'Harvest Mouse'-branded label. Another label on the underside of the cheese displays a graphic scene of a mouse-girl receiving a creampie while in the missionary position.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Harvest Mouse's cheese",
        "Harvest Mouse's cheeses",
        ["innoxia_mouse"],
        [],
    )
    innoxia_race_none_mince_pie = (
        "innoxia_race_none_mince_pie",
        "",
        15,
        "A sweet pie, filled with a mixture of dried fruits and spices. Curiously, the pie seems to remain permanently warm to the touch, revealing that an enchantment of some sort must have been placed on it.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "mince pie",
        "mince pies",
        ["GENERIC_SEX"],
        [],
    )
    innoxia_race_panther_deep_roar = (
        "innoxia_race_panther_deep_roar",
        "",
        250,
        "A glass bottle filled with arrack; a distilled alcoholic drink made from fermented coconut sugar. The branding on the front proudly carries the name 'Deep Roar', while a label on the back describes how a family of tiger-morphs have been distilling this beverage for twelve generations.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Deep Roar",
        "Deep Roars",
        ["ATTRIBUTE_PHYSIQUE"],
        [],
    )
    innoxia_race_panther_panthers_delight = (
        "innoxia_race_panther_panthers_delight",
        "",
        500,
        "A large, raw tomahawk steak, with a small brown tag tied to the bone which displays the words 'Panther's Delight'. Flipping the label over, there's some information which declares the piece of meat to contain  all the minerals and nutrients a growing big cat needs.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Panther's Delight",
        "Panther's Delights",
        ["innoxia_panther"],
        [],
    )
    innoxia_race_pig_oinkers_fries = (
        "innoxia_race_pig_oinkers_fries",
        "",
        15,
        "A cardboard box stuffed full of greasy, curly fries. The branding on the front of the packet declares it to be from the fast food outlet, 'Oinkers!', while a list of ingredients on the back worryingly reassures you that the fat content is less than 50%.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Oinkers Fries",
        "Oinkers Fries'",
        ["innoxia_pig"],
        [],
    )
    innoxia_race_pig_porcine_pop = (
        "innoxia_race_pig_porcine_pop",
        "",
        18,
        "A small glass bottle filled with a dark brown liquid. The list of ingredients on the back of the bottle's label reveals this to be a sort of cola, albeit one with an extraordinarily high sugar content.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Porcine Pop",
        "Porcine Pops",
        ["ATTRIBUTE_PHYSIQUE"],
        [],
    )
    innoxia_race_rabbit_bunny_carrot_cake = (
        "innoxia_race_rabbit_bunny_carrot_cake",
        "",
        40,
        "An individually-wrapped slice of frosted carrot cake, complete with a decorative icing carrot on the top. On the wrapper, there's a very lewd image of a rabbit-girl being bred by a muscular rabbit-boy.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Bunny Carrot-Cake",
        "Bunny Carrot-Cakes",
        ["RACE_RABBIT_MORPH"],
        [],
    )
    innoxia_race_rabbit_bunny_juice = (
        "innoxia_race_rabbit_bunny_juice",
        "",
        30,
        "A small plastic bottle of what appears to be some sort of carrot juice, labelled as 'Bunny Juice'.  On the label, there's a rather obscene image of a rabbit-girl stuffing a carrot-shaped dildo into her pussy.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Bunny Juice",
        "Bunny Juices",
        ["GENERIC_SEX"],
        [],
    )
    innoxia_race_raptor_energy_bar = (
        "innoxia_race_raptor_energy_bar",
        "",
        50,
        "A small, Raptor-branded energy bar, which proudly states on the back to be of 'Over 80% meat content'. On the reverse of the wrapper, there's an image of a fit falcon-boy dominantly face-fucking a naked mouse-girl.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Raptor energy bar",
        "Raptor energy bars",
        ["innoxia_raptor"],
        [],
    )
    innoxia_race_raptor_energy_drink = (
        "innoxia_race_raptor_energy_drink",
        "",
        45,
        "A brightly-coloured can of caffeinated, Raptor-branded energy drink. On the reverse label there's a graphic image of a wild-eyed eagle-girl pinning a timid-looking rabbit-boy down and riding his cock.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Raptor energy drink",
        "Raptor energy drinks",
        ["ATTRIBUTE_PHYSIQUE"],
        [],
    )
    innoxia_race_rat_black_rats_rum = (
        "innoxia_race_rat_black_rats_rum",
        "",
        200,
        "A glass bottle of 'Black Rat's Rum', filled with orange-coloured alcohol. The label on the front shows an image of a black-furred rat-boy wearing a thief's mask, who's pinning a rat-girl against a wall as he fucks her from behind.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Black Rat's Rum",
        "Black Rat's Rums",
        ["ATTRIBUTE_PHYSIQUE"],
        [],
    )
    innoxia_race_rat_brown_rats_burger = (
        "innoxia_race_rat_brown_rats_burger",
        "",
        80,
        "A double-cheeseburger, wrapped up in greaseproof paper. On the wrapper, there's a picture of a brown-furred rat-boy greedily shoving one of these burgers into his mouth.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Brown Rat's Burger",
        "Brown Rat's Burgers",
        ["RACE_RAT_MORPH"],
        [],
    )
    innoxia_race_reindeer_rudolphs_egg_nog = (
        "innoxia_race_reindeer_rudolphs_egg_nog",
        "",
        30,
        "The label on the front of this carton declares it to be 'Rudolph's Egg Nog'. The drink's namesake, a buff, stark-naked reindeer-boy, is shown on the label to be drinking a glass of egg nog while receiving oral sex from three enraptured reindeer-girls.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Rudolph's Egg nog",
        "Rudolph's Egg nogs",
        ["ATTRIBUTE_PHYSIQUE"],
        [],
    )
    innoxia_race_reindeer_sugar_cookie = (
        "innoxia_race_reindeer_sugar_cookie",
        "",
        25,
        "An individually-wrapped sugar cookie, which is covered in white icing and multi-coloured sprinkles. Although the icon on the package is of a reindeer-morph, it is declared to be safe to eat for all races.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Sugar Cookie",
        "Sugar Cookies",
        ["RACE_REINDEER_MORPH"],
        [],
    )
    innoxia_race_sheep_ewes_brew = (
        "innoxia_race_sheep_ewes_brew",
        "",
        35,
        "A small glass bottle filled with a light brownish-yellow liquid. The label on the front simply declares it to be 'Ewe's Brew', and you have to look over the list of ingredients to discover that it's a type of dandelion tea, made from both the leaves and flower of the plant.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Ewe's Brew",
        "Ewe's Brew",
        ["ATTRIBUTE_PHYSIQUE"],
        [],
    )
    innoxia_race_sheep_woolly_goodness = (
        "innoxia_race_sheep_woolly_goodness",
        "",
        10,
        "A large serving of fluffy candy floss, covered in a type of protective cellophane wrapper and skewered by a wooden stick. A label on the plastic packaging bears the brand name 'Woolly Goodness', and displays a naked sheep-girl happily presenting herself to the viewer.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Woolly Goodness",
        "Woolly Goodnesses",
        ["innoxia_sheep"],
        [],
    )
    innoxia_race_slime_biojuice_canister = (
        "innoxia_race_slime_biojuice_canister",
        "",
        2500,
        "A canister of heavily processed glowing pink liquid, which has a thick, slimy consistency. The warning sign on the front makes it quite clear that drinking this would be a bad idea...",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Biojuice Canister",
        "Biojuice Canister",
        ["RACE_SLIME"],
        [],
    )
    innoxia_race_slime_slime_quencher = (
        "innoxia_race_slime_slime_quencher",
        "",
        250,
        "A small glass bottle of luminescent, fizzy pop. The label on the front reads 'Slime Quencher', and beneath this, there's a picture of a completely naked slime-girl pressing her breasts together and smiling at you.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Slime Quencher",
        "Slime Quenchers",
        ["GENERIC_SEX"],
        [],
    )
    innoxia_race_squirrel_round_nuts = (
        "innoxia_race_squirrel_round_nuts",
        "",
        50,
        "A small bag of round nuts. A label on one side shows a greater squirrel-girl stuffing a handful of nuts into her mouth.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Round Nuts",
        "Round Nuts",
        ["RACE_SQUIRREL_MORPH"],
        [],
    )
    innoxia_race_squirrel_squirrel_java = (
        "innoxia_race_squirrel_squirrel_java",
        "",
        25,
        "A plastic bottle of what looks to be some kind of coffee. A label on the front shows a squirrel-girl fingering herself over the top of a bottle just like this one; her juices dripping down into the coffee to provide some extra cream.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Squirrel Java",
        "Squirrel Javas",
        ["ATTRIBUTE_PHYSIQUE"],
        [],
    )
    innoxia_race_wolf_meat_and_marrow = (
        "innoxia_race_wolf_meat_and_marrow",
        "",
        80,
        "A package of 'Meat and Marrow', which consists of a slab of some sort of raw meat, wrapped in grease-proof paper and tied up with brown string.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Meat and Marrow",
        "Meat and Marrows",
        ["RACE_WOLF_MORPH"],
        [],
    )
    innoxia_race_wolf_wolf_whiskey = (
        "innoxia_race_wolf_wolf_whiskey",
        "",
        120,
        "Filled with a highly alcoholic whiskey, this glass bottle has a label on the front which depicts a greater wolf-boy having sex with a trio of lesser wolf-girls. A slogan written above this reads: 'Wolf Whiskey; For a real alpha!'",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Wolf Whiskey",
        "Wolf Whiskies",
        ["ATTRIBUTE_PHYSIQUE"],
        [],
    )
    innoxia_slavery_freedom_certification = (
        "innoxia_slavery_freedom_certification",
        "",
        50000,
        "An official document which has the power to declare a slave as being legally freed. While it carries the Slavery Administration's official seal, both the slave's name and the owner's signature still need to be filled out before it can be considered a legitimate legal document.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isFromExternalFile,
        "Freedom Certification",
        "Freedom Certifications",
        ["CLOTHING_BLACK"],
        [],
    )
    LYSSIETHS_RING = (
        "LYSSIETHS_RING",
        "",
        0,
        "Beautifully crafted from rose gold, and encrusted with precious gemstones, this ring has been enchanted to enslave whoever puts it on. If you were able to trick 'The Dark Siren' into wearing it, you could earn an audience with Lyssieth without needing to fight.",
        "",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Lyssieth's Trapped Signet Ring",
        "Lyssieth's Trapped Signet Rings",
        ["CLOTHING_ROSE_GOLD", "CLOTHING_RED_DARK", "CLOTHING_ROSE_GOLD"],
        [],
    )
    MAKEUP_SET = (
        "MAKEUP_SET",
        "",
        5000,
        "A highly sought-after compact makeup set. The cosmetics contained within have been enchanted in such a way as to enable the user to change their colour at will. Even more impressively, no matter how much they're used, the makeup never expires.",
        "an",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInSex,
        "arcane makeup set",
        "arcane makeup set",
        ["CLOTHING_BLACK", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("MAKEUP_SET", None, None, None, 0, 0)],
    )
    mintychip_race_salamander_apple_pie = (
        "mintychip_race_salamander_apple_pie",
        "A tag discreetly attached to the Apple Pie à la Mode informs you that it was made by a certain 'Mintychip'.",
        400,
        "A plate container with a slice of pie and a scoop of vanilla ice cream all underneath a glass cover. Even though the container is freezing cold, the label states that the pie will be arcanely heated instantly when the glass is lifted. The label also features a salamander-girl allowing melted ice cream to drip on her naked body as she eats the pie.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Apple Pie à la Mode",
        "Apple Pie à la Mode",
        ["mintychip_salamander"],
        [],
    )
    mintychip_race_salamander_hot_cocoa = (
        "mintychip_race_salamander_hot_cocoa",
        "A tag discreetly attached to the Hot Chocolate informs you that it was made by a certain 'Mintychip'.",
        150,
        "An insulated metal bottle containing hot cocoa. A sticker on the side claims the sweet drink is the perfect comfort during cold winter nights. The sticker also depicts a flame salamander-boy sipping from a similar bottle while louging near an open fire.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Hot Chocolate",
        "Hot Chocolates",
        ["ATTRIBUTE_PHYSIQUE", "mintychip_salamander", "CLOTHING_STEEL"],
        [],
    )
    mintychip_race_salamander_iced_tea = (
        "mintychip_race_salamander_iced_tea",
        "A tag discreetly attached to the Iced Tea informs you that it was made by a certain 'Mintychip'.",
        150,
        "An insulated metal bottle containing ice and mint tea. A sticker on the side claims the drink is the perfect refreshment for a hot summer day. The sticker also depicts a sweaty frost salamander-girl drinking from a similar bottle.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Iced Tea",
        "Iced Tea",
        ["ATTRIBUTE_PHYSIQUE", "mintychip_salamanderFrost", "CLOTHING_STEEL"],
        [],
    )
    MOO_MILKER_EMPTY = (
        "MOO_MILKER_EMPTY",
        "",
        50,
        "A manual cow-themed breast pump, capable of holding up to 35oz of liquid in the attached plastic beaker.",
        "a",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Moo Milker",
        "Moo Milkers",
        ["BASE_PURPLE_LIGHT", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("MOO_MILKER", None, None, None, 0, 0)],
    )
    MOO_MILKER_FULL = (
        "MOO_MILKER_FULL",
        "",
        150,
        "A manual cow-themed breast pump. The attached plastic beaker has been filled with milk, and, by unscrewing the pumping mechanism on top, you can gain access to the liquid at any time.",
        "a",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Filled Moo Milker",
        "Filled Moo Milkers",
        ["BASE_PURPLE_LIGHT", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("FILLED_MOO_MILKER_DRINK", None, None, None, 0, 0)],
    )
    MOTHERS_MILK = (
        "MOTHERS_MILK",
        "",
        100,
        "A baby bottle filled with a rich, creamy milk. On the side, a little sticker declares that this drink is able to rapidly advance pregnancies and egg incubations.",
        "a bottle of",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Mother's Milk",
        "Mother's Milks",
        ["CLOTHING_WHITE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("MOTHERS_MILK", None, None, None, 0, 0)],
    )
    MUSHROOM = (
        "MUSHROOM",
        "",
        500,
        "Bioluminescent mushrooms such as these are commonly found growing in the Bat Caverns. The slimes which call those caverns their home are particularly fond of consuming these mushrooms, which is what causes their bodies to glow.",
        "a cluster of",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isPlural
        | EItemFlags.isTransformative,
        "Glowing Mushrooms",
        "Glowing Mushrooms",
        ["BASE_BLUE_LIGHT", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("MUSHROOMS", None, None, None, 0, 0)],
    )
    NATALYA_BUSINESS_CARD = (
        "NATALYA_BUSINESS_CARD",
        "",
        0,
        "A business card given to you by Natalya, the dominant succutaur who holds the position of 'Stable Mistress' at the delivery company 'Dominion Express'. The address printed on the card directs you to Dominion's Warehouse District.",
        "",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Natalya's Business Card",
        "Natalya's Business Cards",
        ["CLOTHING_WHITE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [],
    )
    NATALYA_BUSINESS_CARD_STAMPED = (
        "NATALYA_BUSINESS_CARD_STAMPED",
        "",
        0,
        "A business card given to you by Natalya, the dominant succutaur who holds the position of 'Stable Mistress' at the delivery company 'Dominion Express'. The address printed on the card directs you to Dominion's Warehouse District.",
        "",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Natalya's Business Card (Stamped)",
        "Natalya's Business Cards (Stamped)",
        ["CLOTHING_WHITE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [],
    )
    NoStepOnSnek_race_capybara_brownie = (
        "NoStepOnSnek_race_capybara_brownie",
        "A tag discreetly attached to the Chocolate Brownie informs you that it was made by a certain 'NoStepOnSnek'.",
        25,
        "A small cardboard box containing both a delicious chocolate brownie as well as a small ceramic plate upon which to present it. While a popular snack in general, capybaras have somehow become closely associated with it, although nobody seems to know quite how.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Chocolate Brownie",
        "Chocolate Brownies",
        ["NoStepOnSnek_capybara"],
        [],
    )
    NoStepOnSnek_race_capybara_tea2go = (
        "NoStepOnSnek_race_capybara_tea2go",
        "A tag discreetly attached to the Chamomile Tea informs you that it was made by a certain 'NoStepOnSnek'.",
        100,
        "A large, insulated cup filled with a calming chamomile tea. Nobody seems completely sure whether it causes the capybaras' legendary serenity or merely complements it, but they're certainly consuming a lot of the stuff.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Chamomile Tea",
        "Chamomile Teas",
        ["ATTRIBUTE_ARCANE"],
        [],
    )
    NoStepOnSnek_race_octopus_ink_vodka = (
        "NoStepOnSnek_race_octopus_ink_vodka",
        "A tag discreetly attached to the Ink Vodka informs you that it was made by a certain 'NoStepOnSnek'.",
        200,
        "A glass bottle in a minimalistic and entirely black design. The label reads 'Ink Vodka' and depicts a large splotch of the liquid this drink was named after.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Ink Vodka",
        "Ink Vodkas",
        ["ATTRIBUTE_PHYSIQUE"],
        [],
    )
    NoStepOnSnek_race_octopus_shrimp_cocktail = (
        "NoStepOnSnek_race_octopus_shrimp_cocktail",
        "A tag discreetly attached to the Shrimp Cocktail informs you that it was made by a certain 'NoStepOnSnek'.",
        70,
        "A disposable cocktail glass rimmed with freshly poached shrimp and with a small cup of spicy sauce in the centre. The illustration on the outer packaging shows an octopus-morph in chef's clothing handling a comically large number of ingredients and utensils at once.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isTransformative,
        "Shrimp Cocktail",
        "Shrimp Cocktails",
        ["NoStepOnSnek_octopus"],
        [],
    )
    NoStepOnSnek_race_snake_eggs = (
        "NoStepOnSnek_race_snake_eggs",
        "A tag discreetly attached to the Boiled Eggs informs you that they were made by a certain 'NoStepOnSnek'.",
        60,
        "A small egg box holding four, ready-to-eat, hard-boiled eggs. The top of the box portraits a small and perhaps slightly too ambitious snake trying to swallow an egg whole.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile
        | EItemFlags.isPlural
        | EItemFlags.isTransformative,
        "Boiled Eggs",
        "Boiled Eggs",
        ["NoStepOnSnek_snake"],
        [],
    )
    NoStepOnSnek_race_snake_oil = (
        "NoStepOnSnek_race_snake_oil",
        "A tag discreetly attached to the Snake Oil informs you that it was made by a certain 'NoStepOnSnek'.",
        200,
        "A clear glass bottle containing a bright blue liquid. The label on this 'Genuine Snake Oil' makes outrageous promises as to its efficacy as a panacea, and declares it as being suitable 'For both internal and external application'.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "Snake Oil",
        "Snake Oils",
        ["ATTRIBUTE_PHYSIQUE"],
        [],
    )
    OFFSPRING_MAP = (
        "OFFSPRING_MAP",
        "",
        50000,
        "An arcane-enchanted map, obtained from Dominion's city hall, which is able to track the rough location of any of your nearby offspring.",
        "an",
        EItemFlags.isAbleToBeUsedFromInventory,
        "arcane offspring map",
        "arcane offspring maps",
        ["BASE_BROWN", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("OFFSPRING_MAP", None, None, None, 0, 0)],
    )
    ORIENTATION_HYPNO_WATCH = (
        "ORIENTATION_HYPNO_WATCH",
        "",
        50000,
        "A unique, incredibly-powerful arcane instrument. When enchanted, this Hypno-Watch has the ability to change a person's sexual orientation, at the cost of increasing their corruption...",
        "a",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex | EItemFlags.isFetishGiving,
        "Hypno-Watch",
        "Hypno-Watches",
        ["ANDROGYNOUS", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("ORIENTATION_CHANGE", None, None, None, 0, 0)],
    )
    PAINT_CAN = (
        "PAINT_CAN",
        "",
        250,
        "A can of golden paint, branded with the standard-grade 'Bronze-star' logo, which you purchased from 'Argus's DIY Depot'. Hopefully Helena won't be disappointed with this...",
        "a can of",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "'Bronze-star' golden paint",
        "'Bronze-star' golden paint",
        ["CLOTHING_GOLD", "CLOTHING_BRONZE", "CLOTHING_BLACK"],
        [],
    )
    PAINT_CAN_PREMIUM = (
        "PAINT_CAN_PREMIUM",
        "",
        1500,
        "A can of golden paint, branded with the premium-grade 'Purple-star' logo, which you purchased from 'Argus's DIY Depot'. Hopefully Helena will appreciate how much this cost...",
        "a can of",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "'Purple-star' golden paint",
        "'Purple-star' golden paint",
        ["CLOTHING_GOLD", "CLOTHING_PURPLE_DARK", "CLOTHING_BLACK"],
        [],
    )
    POTION = (
        "POTION",
        "",
        500,
        "Created by infusing arcane essences with a consumable item, potions such as these can hold a huge number of restorative effects or performance enhancements. As potion creation is limited to those with a high level of arcane proficiency, such as demons, they are quite rare, and fetch a high price.",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "potion",
        "potions",
        ["CLOTHING_PINK", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [],
    )
    PREGNANCY_TEST = (
        "PREGNANCY_TEST",
        "",
        100,
        "A small plastic wand, no longer than 6&quot;, which has a digital readout embedded in the middle. The small instruction leaflet that came with it says to 'swipe the tester over the target's stomach to find out who the father is!'",
        "an",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Arcane Pregnancy Tester",
        "Arcane Pregnancy Testers",
        ["CLOTHING_WHITE", "GENERIC_ARCANE", "CLOTHING_BLACK"],
        [ItemEffect("PREGNANCY_TEST", None, None, None, 0, 0)],
    )
    PRESENT = (
        "PRESENT",
        "",
        250,
        "A wrapped present, sold by one of the reindeer-morph overseers in Dominion. It contains a random item from their store, and can also be given as a gift to your offspring, slaves, or Lilaya.",
        "a",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "Yuletide Gift",
        "Yuletide Gift",
        ["GENERIC_ARCANE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("PRESENT", None, None, None, 0, 0)],
    )
    REFORGE_HAMMER = (
        "REFORGE_HAMMER",
        "",
        150,
        "A small hammer, with a solid metal head and wooden shaft. It has been imbued with a unique arcane enchantment, which has not only made it as light as a feather, but has also granted it the ability to instantly reforge any weapon.",
        "a",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isConsumedOnUse,
        "reforging hammer",
        "reforging hammers",
        ["CLOTHING_WHITE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("REFORGE_HAMMER", None, None, None, 0, 0)],
    )
    REJUVENATION_POTION = (
        "REJUVENATION_POTION",
        "",
        1000,
        "A decorative glass bottle, filled with a light blue liquid and sealed with an ornate gold-and-glass stopper. An informative sticker on the underside of the bottle reads: '<i>Rejuvenation potion; guaranteed to restore over-used orifices and refill all of your fluids!</i>'",
        "a bottle of",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse,
        "rejuvenation potion",
        "rejuvenation potions",
        ["CLOTHING_BLUE_LIGHT", "CLOTHING_GOLD", "CLOTHING_BLUE_GREY"],
        [ItemEffect("REJUVENATION_POTION", None, None, None, 0, 0)],
    )
    RESONANCE_STONE = (
        "RESONANCE_STONE",
        "",
        0,
        "A small, polished sphere, which has a single groove running around its entire circumference. Normally prohibited to citizens, Claire has given you this one so that you can send a signal to the SWORD Enforcers stationed near the Rat Warrens.",
        "",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Claire's Resonance Stone",
        "Claire's Resonance Stones",
        ["CLOTHING_PURPLE_VERY_DARK", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [],
    )
    rfpnj_peach_peach = (
        "rfpnj_peach_peach",
        "A tag discreetly attached to the peach informs you that it was made by a certain 'Rfpnj'.",
        25,
        "A delicate, juicy fruit of the peach tree, which is often cultivated in temperate climates. It kind of looks like a butt if you squint...",
        "",
        EItemFlags.isAbleToBeDropped
        | EItemFlags.isAbleToBeSold
        | EItemFlags.isAbleToBeUsedFromInventory
        | EItemFlags.isAbleToBeUsedInCombatAllies
        | EItemFlags.isAbleToBeUsedInSex
        | EItemFlags.isConsumedOnUse
        | EItemFlags.isFromExternalFile,
        "peach",
        "peaches",
        ["BASE_PINK_SALMON"],
        [],
    )
    ROLLED_UP_POSTERS = (
        "ROLLED_UP_POSTERS",
        "",
        0,
        "Half a dozen rolled-up posters, given to you by Helena with the order to paste them onto the walls near the entrance to Slaver Alley. Each one displays an enchanted, moving image of the beautiful harpy striking a seductive pose while wearing a skimpy bikini.",
        "half a dozen",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex | EItemFlags.isPlural,
        "rolled-up enchanted posters",
        "rolled-up enchanted posters",
        ["CLOTHING_DESATURATED_BROWN", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [],
    )
    SLAVER_LICENSE = (
        "SLAVER_LICENSE",
        "",
        5000,
        "An official document declaring that you're legally entitled to own, purchase, sell, and even capture slaves. Although slaver licenses are extremely difficult to obtain, they only really have any value to their rightful owner...",
        "",
        EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isAbleToBeUsedInCombatAllies | EItemFlags.isAbleToBeUsedInSex,
        "Slaver license",
        "Slaver licenses",
        ["CLOTHING_WHITE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [],
    )
    SPELL_BOOK_ARCANE_AROUSAL = (
        "SPELL_BOOK_ARCANE_AROUSAL",
        "",
        2500,
        "An arcane tome which contains detailed instructions on how to cast the spell 'Arcane Arousal'. Reading this tome will permanently unlock the ability to cast this spell.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Spellbook: Arcane Arousal",
        "Spellbooks: Arcane Arousal",
        ["GENERIC_ARCANE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SPELL_ARCANE_AROUSAL", None, None, None, 0, 0)],
    )
    SPELL_BOOK_ARCANE_CLOUD = (
        "SPELL_BOOK_ARCANE_CLOUD",
        "",
        5000,
        "An arcane tome which contains detailed instructions on how to cast the spell 'Arcane Cloud'. Reading this tome will permanently unlock the ability to cast this spell.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Spellbook: Arcane Cloud",
        "Spellbooks: Arcane Cloud",
        ["GENERIC_ARCANE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SPELL_ARCANE_CLOUD", None, None, None, 0, 0)],
    )
    SPELL_BOOK_CLEANSE = (
        "SPELL_BOOK_CLEANSE",
        "",
        10000,
        "An arcane tome which contains detailed instructions on how to cast the spell 'Cleanse'. Reading this tome will permanently unlock the ability to cast this spell.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Spellbook: Cleanse",
        "Spellbooks: Cleanse",
        ["GENERIC_ARCANE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SPELL_CLEANSE", None, None, None, 0, 0)],
    )
    SPELL_BOOK_CLOAK_OF_FLAMES = (
        "SPELL_BOOK_CLOAK_OF_FLAMES",
        "",
        10000,
        "An arcane tome which contains detailed instructions on how to cast the spell 'Cloak of Flames'. Reading this tome will permanently unlock the ability to cast this spell.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Spellbook: Cloak of Flames",
        "Spellbooks: Cloak of Flames",
        ["BASE_ORANGE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SPELL_CLOAK_OF_FLAMES", None, None, None, 0, 0)],
    )
    SPELL_BOOK_ELEMENTAL_AIR = (
        "SPELL_BOOK_ELEMENTAL_AIR",
        "",
        25000,
        "An arcane tome which contains detailed instructions on how to cast the spell 'Elemental Air'. Reading this tome will permanently unlock the ability to cast this spell.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Spellbook: Elemental Air",
        "Spellbooks: Elemental Air",
        ["BASE_BLUE_LIGHT", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SPELL_ELEMENTAL_AIR", None, None, None, 0, 0)],
    )
    SPELL_BOOK_ELEMENTAL_ARCANE = (
        "SPELL_BOOK_ELEMENTAL_ARCANE",
        "",
        25000,
        "An arcane tome which contains detailed instructions on how to cast the spell 'Elemental Arcane'. Reading this tome will permanently unlock the ability to cast this spell.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Spellbook: Elemental Arcane",
        "Spellbooks: Elemental Arcane",
        ["GENERIC_ARCANE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SPELL_ELEMENTAL_ARCANE", None, None, None, 0, 0)],
    )
    SPELL_BOOK_ELEMENTAL_EARTH = (
        "SPELL_BOOK_ELEMENTAL_EARTH",
        "",
        25000,
        "An arcane tome which contains detailed instructions on how to cast the spell 'Elemental Earth'. Reading this tome will permanently unlock the ability to cast this spell.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Spellbook: Elemental Earth",
        "Spellbooks: Elemental Earth",
        ["BASE_BROWN", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SPELL_ELEMENTAL_EARTH", None, None, None, 0, 0)],
    )
    SPELL_BOOK_ELEMENTAL_FIRE = (
        "SPELL_BOOK_ELEMENTAL_FIRE",
        "",
        25000,
        "An arcane tome which contains detailed instructions on how to cast the spell 'Elemental Fire'. Reading this tome will permanently unlock the ability to cast this spell.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Spellbook: Elemental Fire",
        "Spellbooks: Elemental Fire",
        ["BASE_ORANGE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SPELL_ELEMENTAL_FIRE", None, None, None, 0, 0)],
    )
    SPELL_BOOK_ELEMENTAL_WATER = (
        "SPELL_BOOK_ELEMENTAL_WATER",
        "",
        25000,
        "An arcane tome which contains detailed instructions on how to cast the spell 'Elemental Water'. Reading this tome will permanently unlock the ability to cast this spell.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Spellbook: Elemental Water",
        "Spellbooks: Elemental Water",
        ["BASE_AQUA", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SPELL_ELEMENTAL_WATER", None, None, None, 0, 0)],
    )
    SPELL_BOOK_FIREBALL = (
        "SPELL_BOOK_FIREBALL",
        "",
        2500,
        "An arcane tome which contains detailed instructions on how to cast the spell 'Fireball'. Reading this tome will permanently unlock the ability to cast this spell.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Spellbook: Fireball",
        "Spellbooks: Fireball",
        ["BASE_ORANGE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SPELL_FIREBALL", None, None, None, 0, 0)],
    )
    SPELL_BOOK_FLASH = (
        "SPELL_BOOK_FLASH",
        "",
        5000,
        "An arcane tome which contains detailed instructions on how to cast the spell 'Flash'. Reading this tome will permanently unlock the ability to cast this spell.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Spellbook: Flash",
        "Spellbooks: Flash",
        ["BASE_ORANGE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SPELL_FLASH", None, None, None, 0, 0)],
    )
    SPELL_BOOK_ICE_SHARD = (
        "SPELL_BOOK_ICE_SHARD",
        "",
        2500,
        "An arcane tome which contains detailed instructions on how to cast the spell 'Ice Shard'. Reading this tome will permanently unlock the ability to cast this spell.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Spellbook: Ice Shard",
        "Spellbooks: Ice Shard",
        ["BASE_AQUA", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SPELL_ICE_SHARD", None, None, None, 0, 0)],
    )
    SPELL_BOOK_LILITHS_COMMAND = (
        "SPELL_BOOK_LILITHS_COMMAND",
        "",
        1000000,
        "An arcane tome which contains detailed instructions on how to cast the spell 'Lilith's Command'. Reading this tome will permanently unlock the ability to cast this spell.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Spellbook: Lilith's Command",
        "Spellbooks: Lilith's Command",
        ["GENERIC_ARCANE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SPELL_LILITHS_COMMAND", None, None, None, 0, 0)],
    )
    SPELL_BOOK_POISON_VAPOURS = (
        "SPELL_BOOK_POISON_VAPOURS",
        "",
        2500,
        "An arcane tome which contains detailed instructions on how to cast the spell 'Poison Vapours'. Reading this tome will permanently unlock the ability to cast this spell.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Spellbook: Poison Vapours",
        "Spellbooks: Poison Vapours",
        ["BASE_BLUE_LIGHT", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SPELL_POISON_VAPOURS", None, None, None, 0, 0)],
    )
    SPELL_BOOK_PROTECTIVE_GUSTS = (
        "SPELL_BOOK_PROTECTIVE_GUSTS",
        "",
        10000,
        "An arcane tome which contains detailed instructions on how to cast the spell 'Protective Gusts'. Reading this tome will permanently unlock the ability to cast this spell.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Spellbook: Protective Gusts",
        "Spellbooks: Protective Gusts",
        ["BASE_BLUE_LIGHT", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SPELL_PROTECTIVE_GUSTS", None, None, None, 0, 0)],
    )
    SPELL_BOOK_RAIN_CLOUD = (
        "SPELL_BOOK_RAIN_CLOUD",
        "",
        5000,
        "An arcane tome which contains detailed instructions on how to cast the spell 'Rain Cloud'. Reading this tome will permanently unlock the ability to cast this spell.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Spellbook: Rain Cloud",
        "Spellbooks: Rain Cloud",
        ["BASE_AQUA", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SPELL_RAIN_CLOUD", None, None, None, 0, 0)],
    )
    SPELL_BOOK_SLAM = (
        "SPELL_BOOK_SLAM",
        "",
        2500,
        "An arcane tome which contains detailed instructions on how to cast the spell 'Slam'. Reading this tome will permanently unlock the ability to cast this spell.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Spellbook: Slam",
        "Spellbooks: Slam",
        ["BASE_BROWN", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SPELL_SLAM", None, None, None, 0, 0)],
    )
    SPELL_BOOK_SOOTHING_WATERS = (
        "SPELL_BOOK_SOOTHING_WATERS",
        "",
        10000,
        "An arcane tome which contains detailed instructions on how to cast the spell 'Soothing Waters'. Reading this tome will permanently unlock the ability to cast this spell.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Spellbook: Soothing Waters",
        "Spellbooks: Soothing Waters",
        ["BASE_AQUA", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SPELL_SOOTHING_WATERS", None, None, None, 0, 0)],
    )
    SPELL_BOOK_STEAL = (
        "SPELL_BOOK_STEAL",
        "",
        10000,
        "An arcane tome which contains detailed instructions on how to cast the spell 'Steal'. Reading this tome will permanently unlock the ability to cast this spell.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Spellbook: Steal",
        "Spellbooks: Steal",
        ["GENERIC_ARCANE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SPELL_STEAL", None, None, None, 0, 0)],
    )
    SPELL_BOOK_STONE_SHELL = (
        "SPELL_BOOK_STONE_SHELL",
        "",
        10000,
        "An arcane tome which contains detailed instructions on how to cast the spell 'Stone Shell'. Reading this tome will permanently unlock the ability to cast this spell.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Spellbook: Stone Shell",
        "Spellbooks: Stone Shell",
        ["BASE_BROWN", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SPELL_STONE_SHELL", None, None, None, 0, 0)],
    )
    SPELL_BOOK_TELEKENETIC_SHOWER = (
        "SPELL_BOOK_TELEKENETIC_SHOWER",
        "",
        5000,
        "An arcane tome which contains detailed instructions on how to cast the spell 'Telekinetic Shower'. Reading this tome will permanently unlock the ability to cast this spell.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Spellbook: Telekinetic Shower",
        "Spellbooks: Telekinetic Shower",
        ["BASE_BROWN", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SPELL_TELEKENETIC_SHOWER", None, None, None, 0, 0)],
    )
    SPELL_BOOK_TELEPATHIC_COMMUNICATION = (
        "SPELL_BOOK_TELEPATHIC_COMMUNICATION",
        "",
        5000,
        "An arcane tome which contains detailed instructions on how to cast the spell 'Telepathic Communication'. Reading this tome will permanently unlock the ability to cast this spell.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Spellbook: Telepathic Communication",
        "Spellbooks: Telepathic Communication",
        ["GENERIC_ARCANE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SPELL_TELEPATHIC_COMMUNICATION", None, None, None, 0, 0)],
    )
    SPELL_BOOK_TELEPORT = (
        "SPELL_BOOK_TELEPORT",
        "",
        1000000,
        "An arcane tome which contains detailed instructions on how to cast the spell 'Teleport'. Reading this tome will permanently unlock the ability to cast this spell.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Spellbook: Teleport",
        "Spellbooks: Teleport",
        ["GENERIC_ARCANE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SPELL_TELEPORT", None, None, None, 0, 0)],
    )
    SPELL_BOOK_VACUUM = (
        "SPELL_BOOK_VACUUM",
        "",
        5000,
        "An arcane tome which contains detailed instructions on how to cast the spell 'Vacuum'. Reading this tome will permanently unlock the ability to cast this spell.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Spellbook: Vacuum",
        "Spellbooks: Vacuum",
        ["BASE_BLUE_LIGHT", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SPELL_VACUUM", None, None, None, 0, 0)],
    )
    SPELL_SCROLL_AIR = (
        "SPELL_SCROLL_AIR",
        "",
        1000,
        "An arcane scroll which, when read, imbues the reader with the power of the school of 'Air'.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Scroll of Air",
        "Scrolls of Air",
        ["BASE_BLUE_LIGHT", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SCROLL_SCHOOL_AIR", None, None, None, 0, 0)],
    )
    SPELL_SCROLL_ARCANE = (
        "SPELL_SCROLL_ARCANE",
        "",
        1000,
        "An arcane scroll which, when read, imbues the reader with the power of the school of 'Arcane'.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Scroll of Arcane",
        "Scrolls of Arcane",
        ["GENERIC_ARCANE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SCROLL_SCHOOL_ARCANE", None, None, None, 0, 0)],
    )
    SPELL_SCROLL_EARTH = (
        "SPELL_SCROLL_EARTH",
        "",
        1000,
        "An arcane scroll which, when read, imbues the reader with the power of the school of 'Earth'.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Scroll of Earth",
        "Scrolls of Earth",
        ["BASE_BROWN", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SCROLL_SCHOOL_EARTH", None, None, None, 0, 0)],
    )
    SPELL_SCROLL_FIRE = (
        "SPELL_SCROLL_FIRE",
        "",
        1000,
        "An arcane scroll which, when read, imbues the reader with the power of the school of 'Fire'.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Scroll of Fire",
        "Scrolls of Fire",
        ["BASE_ORANGE", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SCROLL_SCHOOL_FIRE", None, None, None, 0, 0)],
    )
    SPELL_SCROLL_WATER = (
        "SPELL_SCROLL_WATER",
        "",
        1000,
        "An arcane scroll which, when read, imbues the reader with the power of the school of 'Water'.",
        "",
        EItemFlags.isAbleToBeDropped | EItemFlags.isAbleToBeSold | EItemFlags.isAbleToBeUsedFromInventory | EItemFlags.isConsumedOnUse,
        "Scroll of Water",
        "Scrolls of Water",
        ["BASE_AQUA", "CLOTHING_BLACK", "CLOTHING_BLACK"],
        [ItemEffect("EFFECT_SCROLL_SCHOOL_WATER", None, None, None, 0, 0)],
    )

    def getId(self) -> str:
        return self.id

    def getAuthorDescription(self) -> str:
        return self.authorDescription

    def getBaseValue(self) -> int:
        return self.baseValue

    def getDescription(self) -> str:
        return self.description

    def getDeterminer(self) -> str:
        return self.determiner

    def getFlags(self) -> EItemFlags:
        return self.flags

    def getNameSingular(self) -> str:
        return self.nameSingular

    def getNamePlural(self) -> str:
        return self.namePlural

    def getColours(self) -> List[str]:
        return self.colours

    def getEffects(self) -> List[ItemEffect]:
        return self.effects

    def __init__(
        self, id: str, authorDescription: str, baseValue: int, description: str, determiner: str, flags: EItemFlags, nameSingular: str, namePlural: str, colours: List[str], effects: List[ItemEffect]
    ) -> None:
        self.id: str = id
        self.authorDescription: str = authorDescription
        self.baseValue: int = baseValue
        self.description: str = description
        self.determiner: str = determiner
        self.flags: EItemFlags = flags
        self.nameSingular: str = nameSingular
        self.namePlural: str = namePlural
        self.colours: List[str] = colours
        self.effects: List[ItemEffect] = effects

    @property
    def isAbleToBeDropped(self) -> bool:
        return self.flags & EItemFlags.isAbleToBeDropped == EItemFlags.isAbleToBeDropped

    @property
    def isAbleToBeSold(self) -> bool:
        return self.flags & EItemFlags.isAbleToBeSold == EItemFlags.isAbleToBeSold

    @property
    def isAbleToBeUsedFromInventory(self) -> bool:
        return self.flags & EItemFlags.isAbleToBeUsedFromInventory == EItemFlags.isAbleToBeUsedFromInventory

    @property
    def isAbleToBeUsedInCombatAllies(self) -> bool:
        return self.flags & EItemFlags.isAbleToBeUsedInCombatAllies == EItemFlags.isAbleToBeUsedInCombatAllies

    @property
    def isAbleToBeUsedInCombatEnemies(self) -> bool:
        return self.flags & EItemFlags.isAbleToBeUsedInCombatEnemies == EItemFlags.isAbleToBeUsedInCombatEnemies

    @property
    def isAbleToBeUsedInSex(self) -> bool:
        return self.flags & EItemFlags.isAbleToBeUsedInSex == EItemFlags.isAbleToBeUsedInSex

    @property
    def isConsumedOnUse(self) -> bool:
        return self.flags & EItemFlags.isConsumedOnUse == EItemFlags.isConsumedOnUse

    @property
    def isFetishGiving(self) -> bool:
        return self.flags & EItemFlags.isFetishGiving == EItemFlags.isFetishGiving

    @property
    def isFromExternalFile(self) -> bool:
        return self.flags & EItemFlags.isFromExternalFile == EItemFlags.isFromExternalFile

    @property
    def isGift(self) -> bool:
        return self.flags & EItemFlags.isGift == EItemFlags.isGift

    @property
    def isMod(self) -> bool:
        return self.flags & EItemFlags.isMod == EItemFlags.isMod

    @property
    def isPlural(self) -> bool:
        return self.flags & EItemFlags.isPlural == EItemFlags.isPlural

    @property
    def isTransformative(self) -> bool:
        return self.flags & EItemFlags.isTransformative == EItemFlags.isTransformative

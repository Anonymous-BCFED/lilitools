from enum import IntEnum
__all__ = ['ERarity']


class ERarity(IntEnum):
    COMMON = 0
    UNCOMMON = 1
    RARE = 2
    EPIC = 3
    LEGENDARY = 4
    QUEST = 5
    JINXED = 6

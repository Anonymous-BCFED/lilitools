from typing import Optional

from lxml import etree

from lilitools.consts import SECONDS_PER_HOUR
from lilitools.saves.enums.tf_modifier import ETFModifier
from lilitools.saves.enums.tf_potency import ETFPotency

from ._savables.item_effect import RawItemEffect


class ItemEffect(RawItemEffect):

    def __init__(self, type: Optional[str] = None, mod1: Optional[ETFModifier] = None, mod2: Optional[ETFModifier] = None, potency: Optional[ETFPotency] = None, limit: Optional[int] = None, timer: Optional[int] = None, repeatable: bool = True) -> None:
        super().__init__()
        if type:
            self.type = type
        self.mod1 = mod1
        self.mod2 = mod2
        self.potency = potency
        self.limit = limit
        self.timer = timer
        self.repeatable = repeatable

    def clone(self) -> 'ItemEffect':
        return ItemEffect(self.type, self.mod1, self.mod2, self.potency, self.limit, self.timer, self.repeatable)

    def resetTimer(self) -> None:
        from lilitools.saves.game import Game
        self.timer = Game.getAnInstance().coreInfo.secondsPassed % SECONDS_PER_HOUR

    def toXML(self, tagOverride: Optional[str] = None) -> etree._Element:
        # if self.timer == 0:
        #     self.resetTimer()
        if self.timer is not None and self.timer > 0:
            self.timer = round(self.timer % SECONDS_PER_HOUR)
        return super().toXML(tagOverride)

    def __str__(self) -> str:
        return etree.tostring(self.toXML(), encoding='unicode')

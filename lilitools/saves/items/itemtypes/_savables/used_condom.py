#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, List, Optional
from lxml import etree
from lilitools.saves.character.fluids.stored_fluid import StoredFluid
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.items.item_effect import ItemEffect
from lilitools.saves.savable import Savable
__all__ = ['RawUsedCondom']
## from [LT]/src/com/lilithsthrone/game/inventory/item/AbstractFilledCondom.java: public Element saveAsXML(Element parentElement, Document doc) { @ OZ0IlM61BzbJOwOxRwDpXLbUNmsBLWdMT/vwllAIit3WtWcI+TZPqWDznKwjK+wK9sVzTUwaoxn52LgYa98Xqg==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawUsedCondom(Savable):
    TAG = 'item'
    _XMLID_ATTR_COLOUR_ATTRIBUTE: str = 'colour'
    _XMLID_ATTR_CUMPROVIDER_ATTRIBUTE: str = 'cumProvider'
    _XMLID_ATTR_FLUIDSTORED_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_ID_ATTRIBUTE: str = 'id'
    _XMLID_ATTR_MILLILITRESSTORED_ATTRIBUTE: str = 'millilitresStored'
    _XMLID_TAG_FLUIDSTORED_ELEMENT: str = 'fluidStored'
    _XMLID_TAG_ITEMEFFECTS_CHILD: str = 'effect'
    _XMLID_TAG_ITEMEFFECTS_PARENT: str = 'itemEffects'
    _XMLID_TAG_ITEMEFFECTS_VALUEELEM: Optional[str] = None

    def __init__(self) -> None:
        super().__init__()
        self.id: str = ''
        self.colour: str = ''
        self.cumProvider: str = ''
        self.millilitresStored: float = 0.
        self.itemEffects: List[ItemEffect] = list()
        self.fluidStored: StoredFluid = StoredFluid()  # Element

    def overrideAttrs(self, colour_attribute: _Optional_str = None, cumProvider_attribute: _Optional_str = None, fluidStored_attribute: _Optional_str = None, id_attribute: _Optional_str = None, millilitresStored_attribute: _Optional_str = None) -> None:
        if colour_attribute is not None:
            self._XMLID_ATTR_COLOUR_ATTRIBUTE = colour_attribute
        if cumProvider_attribute is not None:
            self._XMLID_ATTR_CUMPROVIDER_ATTRIBUTE = cumProvider_attribute
        if fluidStored_attribute is not None:
            self._XMLID_ATTR_FLUIDSTORED_ATTRIBUTE = fluidStored_attribute
        if id_attribute is not None:
            self._XMLID_ATTR_ID_ATTRIBUTE = id_attribute
        if millilitresStored_attribute is not None:
            self._XMLID_ATTR_MILLILITRESSTORED_ATTRIBUTE = millilitresStored_attribute

    def overrideTags(self, fluidStored_element: _Optional_str = None, itemEffects_child: _Optional_str = None, itemEffects_parent: _Optional_str = None, itemEffects_valueelem: _Optional_str = None) -> None:
        if fluidStored_element is not None:
            self._XMLID_TAG_FLUIDSTORED_ELEMENT = fluidStored_element
        if itemEffects_child is not None:
            self._XMLID_TAG_ITEMEFFECTS_CHILD = itemEffects_child
        if itemEffects_parent is not None:
            self._XMLID_TAG_ITEMEFFECTS_PARENT = itemEffects_parent
        if itemEffects_valueelem is not None:
            self._XMLID_TAG_ITEMEFFECTS_VALUEELEM = itemEffects_valueelem

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        lv: Savable
        sf: etree._Element
        value = e.get(self._XMLID_ATTR_ID_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_ID_ATTRIBUTE, 'id')
        else:
            self.id = str(value)
        value = e.get(self._XMLID_ATTR_COLOUR_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_COLOUR_ATTRIBUTE, 'colour')
        else:
            self.colour = str(value)
        value = e.get(self._XMLID_ATTR_CUMPROVIDER_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_CUMPROVIDER_ATTRIBUTE, 'cumProvider')
        else:
            self.cumProvider = str(value)
        value = e.get(self._XMLID_ATTR_MILLILITRESSTORED_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_MILLILITRESSTORED_ATTRIBUTE, 'millilitresStored')
        else:
            self.millilitresStored = float(value)
        if (lf := e.find(self._XMLID_TAG_ITEMEFFECTS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_ITEMEFFECTS_CHILD):
                    lv = ItemEffect()
                    lv.fromXML(lc)
                    self.itemEffects.append(lv)
        else:
            self.itemEffects = list()
        if (sf := e.find(self._XMLID_TAG_FLUIDSTORED_ELEMENT)) is not None:
            self.fluidStored = StoredFluid()
            self.fluidStored.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'fluidStored', self._XMLID_TAG_FLUIDSTORED_ELEMENT)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        e.attrib[self._XMLID_ATTR_ID_ATTRIBUTE] = str(self.id)
        e.attrib[self._XMLID_ATTR_COLOUR_ATTRIBUTE] = str(self.colour)
        e.attrib[self._XMLID_ATTR_CUMPROVIDER_ATTRIBUTE] = str(self.cumProvider)
        e.attrib[self._XMLID_ATTR_MILLILITRESSTORED_ATTRIBUTE] = str(float(self.millilitresStored))
        lp = etree.SubElement(e, self._XMLID_TAG_ITEMEFFECTS_PARENT)
        for lv in self.itemEffects:
            lp.append(lv.toXML())
        e.append(self.fluidStored.toXML(self._XMLID_TAG_FLUIDSTORED_ELEMENT))
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['id'] = self.id
        data['colour'] = self.colour
        data['cumProvider'] = self.cumProvider
        data['millilitresStored'] = float(self.millilitresStored)
        if self.itemEffects is None or len(self.itemEffects) > 0:
            data['itemEffects'] = list()
        else:
            lc = list()
            if len(self.itemEffects) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['itemEffects'] = lc
        data['fluidStored'] = self.fluidStored.toDict()
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'id' not in data:
            raise KeyRequiredException(self, data, 'id', 'id')
        self.id = str(data['id'])
        if 'colour' not in data:
            raise KeyRequiredException(self, data, 'colour', 'colour')
        self.colour = str(data['colour'])
        if 'cumProvider' not in data:
            raise KeyRequiredException(self, data, 'cumProvider', 'cumProvider')
        self.cumProvider = str(data['cumProvider'])
        if 'millilitresStored' not in data:
            raise KeyRequiredException(self, data, 'millilitresStored', 'millilitresStored')
        self.millilitresStored = float(data['millilitresStored'])
        if (lv := self.itemEffects) is not None:
            for le in lv:
                self.itemEffects.append(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'itemEffects', 'itemEffects')
        if (sf := data.get('fluidStored')) is not None:
            self.fluidStored = StoredFluid()
            self.fluidStored.fromDict(data['fluidStored'])
        else:
            raise KeyRequiredException(self, data, 'fluidStored', 'fluidStored')

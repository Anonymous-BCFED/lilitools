#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, List, Optional
from lxml import etree
from lilitools.saves.character.fluids.milk import Milk
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.items.item_effect import ItemEffect
from lilitools.saves.savable import Savable
__all__ = ['RawUsedMilker']
## from [LT]/src/com/lilithsthrone/game/inventory/item/AbstractFilledBreastPump.java: public Element saveAsXML(Element parentElement, Document doc) { @ 9E0IWQLL7hA7K1eY+ER14FwtktWvkLl6/FF8A1XFH2KB7ZwQTaKi0LNkqc2icHJ0p6e+cnnX+PZlzIDiV+J9pA==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawUsedMilker(Savable):
    TAG = 'item'
    _XMLID_ATTR_COLOUR_ATTRIBUTE: str = 'colour'
    _XMLID_ATTR_ID_ATTRIBUTE: str = 'id'
    _XMLID_ATTR_MILKPROVIDER_ATTRIBUTE: str = 'milkProvider'
    _XMLID_ATTR_MILK_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_MILLILITRESSTORED_ATTRIBUTE: str = 'millilitresStored'
    _XMLID_TAG_ITEMEFFECTS_CHILD: str = 'effect'
    _XMLID_TAG_ITEMEFFECTS_PARENT: str = 'itemEffects'
    _XMLID_TAG_ITEMEFFECTS_VALUEELEM: Optional[str] = None
    _XMLID_TAG_MILK_ELEMENT: str = 'milk'

    def __init__(self) -> None:
        super().__init__()
        self.id: str = ''
        self.colour: str = ''
        self.milkProvider: str = ''
        self.millilitresStored: float = 0.
        self.itemEffects: List[ItemEffect] = list()
        self.milk: Milk = Milk()  # Element

    def overrideAttrs(self, colour_attribute: _Optional_str = None, id_attribute: _Optional_str = None, milk_attribute: _Optional_str = None, milkProvider_attribute: _Optional_str = None, millilitresStored_attribute: _Optional_str = None) -> None:
        if colour_attribute is not None:
            self._XMLID_ATTR_COLOUR_ATTRIBUTE = colour_attribute
        if id_attribute is not None:
            self._XMLID_ATTR_ID_ATTRIBUTE = id_attribute
        if milk_attribute is not None:
            self._XMLID_ATTR_MILK_ATTRIBUTE = milk_attribute
        if milkProvider_attribute is not None:
            self._XMLID_ATTR_MILKPROVIDER_ATTRIBUTE = milkProvider_attribute
        if millilitresStored_attribute is not None:
            self._XMLID_ATTR_MILLILITRESSTORED_ATTRIBUTE = millilitresStored_attribute

    def overrideTags(self, itemEffects_child: _Optional_str = None, itemEffects_parent: _Optional_str = None, itemEffects_valueelem: _Optional_str = None, milk_element: _Optional_str = None) -> None:
        if itemEffects_child is not None:
            self._XMLID_TAG_ITEMEFFECTS_CHILD = itemEffects_child
        if itemEffects_parent is not None:
            self._XMLID_TAG_ITEMEFFECTS_PARENT = itemEffects_parent
        if itemEffects_valueelem is not None:
            self._XMLID_TAG_ITEMEFFECTS_VALUEELEM = itemEffects_valueelem
        if milk_element is not None:
            self._XMLID_TAG_MILK_ELEMENT = milk_element

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        lv: Savable
        sf: etree._Element
        value = e.get(self._XMLID_ATTR_ID_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_ID_ATTRIBUTE, 'id')
        else:
            self.id = str(value)
        value = e.get(self._XMLID_ATTR_COLOUR_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_COLOUR_ATTRIBUTE, 'colour')
        else:
            self.colour = str(value)
        value = e.get(self._XMLID_ATTR_MILKPROVIDER_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_MILKPROVIDER_ATTRIBUTE, 'milkProvider')
        else:
            self.milkProvider = str(value)
        value = e.get(self._XMLID_ATTR_MILLILITRESSTORED_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_MILLILITRESSTORED_ATTRIBUTE, 'millilitresStored')
        else:
            self.millilitresStored = float(value)
        if (lf := e.find(self._XMLID_TAG_ITEMEFFECTS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_ITEMEFFECTS_CHILD):
                    lv = ItemEffect()
                    lv.fromXML(lc)
                    self.itemEffects.append(lv)
        else:
            self.itemEffects = list()
        if (sf := e.find(self._XMLID_TAG_MILK_ELEMENT)) is not None:
            self.milk = Milk()
            self.milk.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'milk', self._XMLID_TAG_MILK_ELEMENT)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        e.attrib[self._XMLID_ATTR_ID_ATTRIBUTE] = str(self.id)
        e.attrib[self._XMLID_ATTR_COLOUR_ATTRIBUTE] = str(self.colour)
        e.attrib[self._XMLID_ATTR_MILKPROVIDER_ATTRIBUTE] = str(self.milkProvider)
        e.attrib[self._XMLID_ATTR_MILLILITRESSTORED_ATTRIBUTE] = str(float(self.millilitresStored))
        lp = etree.SubElement(e, self._XMLID_TAG_ITEMEFFECTS_PARENT)
        for lv in self.itemEffects:
            lp.append(lv.toXML())
        e.append(self.milk.toXML(self._XMLID_TAG_MILK_ELEMENT))
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['id'] = self.id
        data['colour'] = self.colour
        data['milkProvider'] = self.milkProvider
        data['millilitresStored'] = float(self.millilitresStored)
        if self.itemEffects is None or len(self.itemEffects) > 0:
            data['itemEffects'] = list()
        else:
            lc = list()
            if len(self.itemEffects) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['itemEffects'] = lc
        data['milk'] = self.milk.toDict()
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'id' not in data:
            raise KeyRequiredException(self, data, 'id', 'id')
        self.id = str(data['id'])
        if 'colour' not in data:
            raise KeyRequiredException(self, data, 'colour', 'colour')
        self.colour = str(data['colour'])
        if 'milkProvider' not in data:
            raise KeyRequiredException(self, data, 'milkProvider', 'milkProvider')
        self.milkProvider = str(data['milkProvider'])
        if 'millilitresStored' not in data:
            raise KeyRequiredException(self, data, 'millilitresStored', 'millilitresStored')
        self.millilitresStored = float(data['millilitresStored'])
        if (lv := self.itemEffects) is not None:
            for le in lv:
                self.itemEffects.append(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'itemEffects', 'itemEffects')
        if (sf := data.get('milk')) is not None:
            self.milk = Milk()
            self.milk.fromDict(data['milk'])
        else:
            raise KeyRequiredException(self, data, 'milk', 'milk')

from abc import ABC
from typing import FrozenSet


class IItemType(ABC):
    ID_SET: FrozenSet[str]
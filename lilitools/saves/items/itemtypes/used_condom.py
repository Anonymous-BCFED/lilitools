from typing import FrozenSet

from lilitools.saves.items.itemtypes._savables.used_condom import RawUsedCondom
from lilitools.saves.items.itemtypes.item_type import IItemType
from lxml import etree

class UsedCondomItemType(IItemType, RawUsedCondom):
    ID_SET: FrozenSet[str] = frozenset({
        'CONDOM_USED',
        'CONDOM_USED_WEBBING'})

    '''Missing in 0.4.9.9'''
    @property
    def name(self)->str:
        return ''
    
    def fromXML(self, e: etree._Element) -> None:
        super().fromXML(e)
from typing import FrozenSet

from lilitools.saves.items.itemtypes._savables.used_milker import RawUsedMilker
from lilitools.saves.items.itemtypes.item_type import IItemType


class UsedMilkerItemType(IItemType, RawUsedMilker):
    ID_SET: FrozenSet[str] = frozenset({'MOO_MILKER_FULL'})

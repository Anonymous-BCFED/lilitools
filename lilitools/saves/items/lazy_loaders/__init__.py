
from typing import Set

from lilitools.saves.items.lazy_loaders.base_lazy_loader import BaseLazyLoader
from .clothing_loader import _ClothingLazyLoader

ALL_LAZY_LOADERS:Set[BaseLazyLoader] = {
    _ClothingLazyLoader()
}
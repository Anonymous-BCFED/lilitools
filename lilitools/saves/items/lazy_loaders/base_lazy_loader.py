import hashlib
import os
import pickle
from abc import abstractmethod
from functools import lru_cache
from pathlib import Path
from typing import Any, Dict, Generic, Optional, Set, TypeVar

import human_readable
from construct import Array, Bytes, Const, Struct, VarInt, this
from rich import get_console
from ruamel.yaml import YAML as Yaml

from lilitools.cli.config import LTConfiguration
from lilitools.lazy_enum import LazyValueProvider
from lilitools.logging import ClickWrap
from lilitools.saves.modfiles.character.clothing.clothing_type import ClothingType

click = ClickWrap()

T = TypeVar("T")


class DumpDataDirIsMissing(Exception):
    def __init__(self, data_type: str, data_dump_dir: Path) -> None:
        super().__init__(
            f"Expected {data_type} directory at {data_dump_dir} is missing!"
        )


class BaseLazyLoader(Generic[T]):
    MAGIC = b"BINARY CLOTHING CACHE\nDO NOT EDIT\n"

    CACHE_NAME = "unknown cache"
    # Rebuilding {this}...
    CACHE_DATA_TYPE_DESC: str = "unknown data"

    SOURCE_FILESET: Set[bytes] = set()
    CACHE_FILENAME: str = None
    DATA_DIR: Path = None
    CACHE_DATA: Optional[Dict[str, Dict[str, Any]]] = {}
    CACHE_OBJECTS: Optional[Dict[str, ClothingType]] = {}
    LOADED: bool = False
    CACHE_VERSION: int = 1

    CACHE_STRUCTURE = Struct(
        "magic" / Const(MAGIC),
        "version" / VarInt,
        "n_file_hashes" / VarInt,
        "file_hashes" / Array(this.n_file_hashes, Bytes(16)),
        "sz_data" / VarInt,
        "data" / Bytes(this.sz_data),
    )

    def __init__(self) -> None:
        self.objects: Dict[str, T] = {}

    @property
    @lru_cache
    def cache_file(self) -> Path:
        cfg = LTConfiguration.GetInstance()
        return cfg.cache_dir / self.CACHE_FILENAME

    def clearCache(self, verbose: bool = False) -> None:
        self.LOADED = False
        self.objects.clear()

        if self.cache_file.is_file():
            os.remove(self.cache_file)
            if verbose:
                click.info(f"Removed {self.cache_file}")

    def rebuildCache(self) -> None:
        self.clearCache()
        self.objects = self._loadFromFiles()

    def _init(self) -> None:
        if self.LOADED:
            return
        self.LOADED = True
        self.objects = self._loadFromFiles()

    def _loadFromFiles(self) -> Dict[str, T]:
        o = {}
        fs = set()
        for filename in self.DATA_DIR.rglob("*.yml"):
            with filename.open("rb") as f:
                filehash = hashlib.file_digest(f, hashlib.md5).digest()
            fs.add(filehash)

        YAML = Yaml(typ="rt", pure=True)
        if self.cache_file.is_file():
            with self.cache_file.open("rb") as f:
                actual_magic = f.read(len(self.MAGIC))
                actual_version = VarInt.parse_stream(f)
                # print(repr(actual_magic), actual_version)
                if actual_magic == self.MAGIC and actual_version == self.CACHE_VERSION:
                    f.seek(0)
                    cf = self.CACHE_STRUCTURE.parse_stream(f)
                    # print(repr(fs), repr(cf.file_hashes))
                    if set(cf.file_hashes) == fs:
                        return pickle.loads(cf.data)
        relative_cache_filename = self.cache_file.name

        if not self.DATA_DIR.is_dir():
            raise DumpDataDirIsMissing(self.CACHE_DATA_TYPE_DESC, self.DATA_DIR)

        console=get_console()
        with console.status(f"Building {relative_cache_filename}...") as status:
            flist = list(self.DATA_DIR.rglob("*.yml"))
            l_flist=len(flist)
            for i,filename in enumerate(flist):
                status.update(f"Building {relative_cache_filename}... ({i+1}/{l_flist})")
                with filename.open("r") as fy:
                    o.update(self.initFromData(YAML.load(fy)))

        self.cache_file.parent.mkdir(parents=True, exist_ok=True)
        with self.cache_file.open("wb") as f:
            d = pickle.dumps(o)
            self.CACHE_STRUCTURE.build_stream(
                dict(
                    version=self.CACHE_VERSION,
                    n_file_hashes=len(fs),
                    file_hashes=fs,
                    sz_data=len(d),
                    data=d,
                ),
                f,
            )
        cachefile_size = human_readable.file_size(os.path.getsize(self.cache_file))
        click.success(
            f"Built {relative_cache_filename} ({len(o):,} entries, {cachefile_size})"
        )
        return o

    @abstractmethod
    def initFromData(self, data: dict) -> Dict[str, T]:
        pass

    def getItemByID(self, item_id: str) -> T:
        self._init()
        # print(repr(self.objects))
        return self.objects[item_id]

    def getLazyLoader(self) -> LazyValueProvider[T]:
        _this = self

        class LazyLoadingTypeLoader(LazyValueProvider[T]):
            def __init__(lltl, itemID: str) -> None:
                lltl.item_id: str = itemID

            def get_value(lltl) -> T:
                return _this.getItemByID(lltl.item_id)

        return LazyLoadingTypeLoader

from pathlib import Path
from typing import TYPE_CHECKING, Dict

from lilitools.lazy_enum import LazyValueProvider
from lilitools.saves.items.lazy_loaders.base_lazy_loader import BaseLazyLoader
from lilitools.saves.modfiles.character.clothing.clothing_type import ClothingType
from lilitools.saves.modfiles.character.clothing.colour_info import ColourInfo

__all__ = ["LazyClothing"]
if TYPE_CHECKING:# and False:

    def LazyClothing(clothing_id: str) -> ClothingType:
        return ClothingType()

else:

    class _ClothingLazyLoader(BaseLazyLoader[ClothingType]):
        MAGIC = b"BINARY CLOTHING CACHE\nDO NOT EDIT\n"
        CACHE_DATA_TYPE_DESC='clothing data'
        CACHE_FILENAME = "clothing.dat"
        DATA_DIR = Path.cwd() / "data" / "dumps" / "clothing"

        def initFromData(self, data: dict) -> Dict[str, ClothingType]:
            c = ClothingType()
            c.id = data["id"]
            c.coreAttributes.baseValue = data["baseValue"]
            c.coreAttributes.determiner = data["determiner"]
            c.coreAttributes.nameSingular = data["name"]
            c.coreAttributes.namePlural = data["namePlural"]
            c.coreAttributes.isPlural = data["isPlural"]
            c.coreAttributes.description = data["description"]
            c.allColourPossibilities=None
            for i in range(len(data["colourReplacements"])):
                ci = ColourInfo()
                ci._allColours = data["colourReplacements"][i]["all"]
                ci.defaultColours = data["colourReplacements"][i]["defaults"]
                ci.extraColours = data["colourReplacements"][i]["extras"]
                ci.replacementStrings = data["colourReplacements"][i]["replacements"]
                ci._precalculate()
                c.coreAttributes.colourReplacements.append(ci)
            c.loadColours()
            return {c.id: c}

    LazyClothing: LazyValueProvider[ClothingType] = (
        _ClothingLazyLoader().getLazyLoader()
    )

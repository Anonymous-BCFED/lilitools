from typing import List, Optional, Protocol

from lilitools.saves.items.item_effect import ItemEffect


class HasEffectsProtocol(Protocol):
    '''Basically just to tell mypy to eat a big fat one.'''
    @property
    def effects(self) -> Optional[List[ItemEffect]]: ...


class ThingWithEffectsMixin:

    def resetEffectTimers(self: HasEffectsProtocol, value: Optional[int] = None) -> None:
        '''
        Resets all effects to the same timer value.

        If value is None, timer is set to game time % 3600.
        '''
        if self.effects is None:
            return
        for e in self.effects:
            if value is None:
                e.resetTimer()
            else:
                e.timer = value

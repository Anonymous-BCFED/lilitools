from pathlib import Path
from typing import Any

from lilitools.saves.items.enums.known_weapons import EKnownWeapons
from lilitools.saves.modfiles.utils import MFELookupTable
from lilitools.saves.modfiles.weapons.weapon_type import WeaponType


class _WeaponTypes(MFELookupTable[WeaponType]):
    MOD_SUB_DIR = Path("items") / "weapons"

    def handleBuiltinsEnumEntry(self, e: Any) -> None:
        return super().handleBuiltinsEnumEntry(e)


WEAPON_TYPES = _WeaponTypes(EKnownWeapons, Path("res") / "weapons", WeaponType)

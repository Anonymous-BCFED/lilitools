from __future__ import annotations
from typing import Optional

from lilitools.saves.game import Game
from lilitools.saves.math.point2i import Point2I
from lilitools.saves.world.cell import Cell

'''
<world height="2" width="3" worldType="EMPTY">
  <grid>
    <cell discovered="false" travelledTo="false">
      <location x="0" y="0"/>
      <place type="GENERIC_HOLDING_CELL"/>
    </cell>
    <cell discovered="false" travelledTo="false">
      <location x="0" y="1"/>
      <place type="GENERIC_MUSEUM"/>
    </cell>
    <cell discovered="false" travelledTo="false">
      <location x="1" y="0"/>
      <place type="GENERIC_EMPTY_TILE"/>
    </cell>
    <cell discovered="false" travelledTo="false">
      <location x="2" y="0"/>
      <place type="GENERIC_EMPTY_TILE"/>
    </cell>
    <cell discovered="false" travelledTo="false">
      <location x="2" y="1"/>
      <place type="GENERIC_CLUB_HOLDING_CELL"/>
    </cell>
  </grid>
</world>
'''

class Location:
    def __init__(self) -> None:
        self.world: str = ''
        self.position: Point2I = Point2I()

    @classmethod
    def FromCell(cls, cell: Cell) -> Location:
        loc = cls()
        loc.position.x = cell.location.x
        loc.position.y = cell.location.y
        loc.world = cell.worldID
        return loc

    def asCell(self, game: Game) -> Optional[Cell]:
        try:
            return game.worlds.getCellByWorldCoords(self.world, self.position.x, self.position.y)
        except KeyError:
            return None

    def moveToCell(self, cell: Cell) -> None:
        self.position.x = cell.location.x
        self.position.y = cell.location.y
        self.world = cell.worldID

    def moveToPos(self, x: int, y: int) -> None:
        self.position.x = x
        self.position.y = y

    def moveToWorldPos(self, worldID: str, x: int, y: int) -> None:
        self.world = worldID
        self.position.x = x
        self.position.y = y

    def toDict(self) -> dict:
        return {
            'world': self.world,
            'position': [self.position.x, self.position.y],
        }

    def fromDict(self, data: dict) -> None:
        self.world = data['world']
        x, y = data['position']
        self.position = Point2I(x, y)

#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawPoint2I']
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawPoint2I(Savable):
    TAG = 'point2i'
    _XMLID_ATTR_X_ATTRIBUTE: str = 'x'
    _XMLID_ATTR_Y_ATTRIBUTE: str = 'y'

    def __init__(self) -> None:
        super().__init__()
        self.x: int = 0
        self.y: int = 0

    def overrideAttrs(self, x_attribute: _Optional_str = None, y_attribute: _Optional_str = None) -> None:
        if x_attribute is not None:
            self._XMLID_ATTR_X_ATTRIBUTE = x_attribute
        if y_attribute is not None:
            self._XMLID_ATTR_Y_ATTRIBUTE = y_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_X_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_X_ATTRIBUTE, 'x')
        else:
            self.x = int(value)
        value = e.get(self._XMLID_ATTR_Y_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_Y_ATTRIBUTE, 'y')
        else:
            self.y = int(value)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_X_ATTRIBUTE] = str(self.x)
        e.attrib[self._XMLID_ATTR_Y_ATTRIBUTE] = str(self.y)
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['x'] = int(self.x)
        data['y'] = int(self.y)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'x' not in data:
            raise KeyRequiredException(self, data, 'x', 'x')
        self.x = int(data['x'])
        if 'y' not in data:
            raise KeyRequiredException(self, data, 'y', 'y')
        self.y = int(data['y'])

from typing import Optional
from lilitools.saves.math._savables.point2i import RawPoint2I


class Point2I(RawPoint2I):

    def __init__(self, x: Optional[int] = None, y: Optional[int] = None) -> None:
        super().__init__()
        self.x: int = x or 0
        self.y: int = y or 0

    def __str__(self) -> str:
        return repr((self.x, self.y))

    def __add__(self, other: object) -> 'Point2I':
        if hasattr(other, 'x') and hasattr(other, 'y'):
            return Point2I(self.x + other.x, self.y + other.y)
        elif isinstance(other, int):
            return Point2I(self.x + other, self.y + other)
        raise NotImplementedError()

    def __radd__(self, other: object) -> 'Point2I':
        return self.__add__(other)

    def __sub__(self, other: object) -> 'Point2I':
        if hasattr(other, 'x') and hasattr(other, 'y'):
            return Point2I(self.x - other.x, self.y - other.y)
        elif isinstance(other, int):
            return Point2I(self.x - other, self.y - other)
        raise NotImplementedError()

    def __rsub__(self, other: object) -> 'Point2I':
        if hasattr(other, 'x') and hasattr(other, 'y'):
            return Point2I(other.x - self.x, other.y - self.y)
        elif isinstance(other, int):
            return Point2I(other - self.x, other - self.y)
        raise NotImplementedError()

    def __mult__(self, other: object) -> 'Point2I':
        if isinstance(other, int):
            return Point2I(self.x * other, self.y * other)
        raise NotImplementedError()

    def __rmult__(self, other: object) -> 'Point2I':
        return self.__mult__(other)

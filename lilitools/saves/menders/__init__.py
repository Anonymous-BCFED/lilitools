from typing import TYPE_CHECKING, Iterable, Type

from frozenlist import FrozenList
from lxml import etree

from lilitools.saves.menders.fix_condoms_with_no_cum import CondomCumFixer

from .base import BaseXMLMender
from .litter_id_dedupe import LitterIDDeduper

__all__ = [
    'mendCharacterXML',
    'mendNPCXML',
    'mendPlayerXML',
    'mendSaveXML',
]


MENDERS_FOR_SAVES: FrozenList[Type[BaseXMLMender]] = FrozenList([CondomCumFixer])
MENDERS_FOR_CHARACTERS: FrozenList[Type[BaseXMLMender]] = FrozenList([
    #LitterIDDeduper, 
])


def _mendXMLFromList(e: etree._Element, l: Iterable[Type[BaseXMLMender]]) -> None:
    for mt in l:
        mt(e)


def mendSaveXML(e: etree._Element) -> None:
    if isinstance(e, etree._ElementTree):
        e = e.getroot()
    mendPlayerXML(e.find('playerCharacter'))
    for npc in e.findall('NPC'):
        mendNPCXML(npc)


def mendPlayerXML(e: etree._Element) -> None:
    if isinstance(e, etree._ElementTree):
        e = e.getroot()
    mendCharacterXML(e)


def mendNPCXML(e: etree._Element) -> None:
    if isinstance(e, etree._ElementTree):
        e = e.getroot()
    mendCharacterXML(e)


def mendCharacterXML(e: etree._Element) -> None:
    if isinstance(e, etree._ElementTree):
        e = e.getroot()
    if (ec := e.find('exportedCharacter')) is not None:
        e = ec
    if (c := e.find('character')) is not None:
        _mendXMLFromList(c, MENDERS_FOR_CHARACTERS)

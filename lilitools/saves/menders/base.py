
from types import TracebackType
from typing import Any, Optional, Type, Union

from lxml import etree

from lilitools.logging import ClickWrap


class MenderDebugMode:
    VALUE: bool = False

    def __init__(self) -> None:
        self.VALUE = True

    def __enter__(self) -> None:
        pass

    def __exit__(
        self,
        exc_type: Union[Type[BaseException], None],
        exc_val: Optional[BaseException],
        exc_tb: Optional[TracebackType],
    ) -> None:
        self.VALUE = False


def _in_debug_mode() -> bool:
    return MenderDebugMode.VALUE


class BaseMender:
    def __init__(self) -> None:
        self._logger = ClickWrap(self.__class__.__name__ if _in_debug_mode() else '')

    def debug(self, msg: Any) -> ClickWrap:
        return self._logger.debug(msg)

    def info(self, msg: Any) -> ClickWrap:
        return self._logger.info(msg)

    def warn(self, msg: Any) -> ClickWrap:
        return self._logger.warn(msg)

    def error(self, msg: Any) -> ClickWrap:
        return self._logger.error(msg)

    def success(self, msg: Any) -> ClickWrap:
        return self._logger.success(msg)


class BaseXMLMender(BaseMender):
    def __init__(self, e: etree._Element) -> None:
        super().__init__()
        self.tree: etree._ElementTree = e.getroottree()

    def elementToString(self, e: etree._Element) -> str:
        return etree.tostring(e, encoding='unicode')

    def getElementPath(self, e: etree._Element) -> str:
        return self.tree.getpath(e)

    def removeElement(self, e: etree._Element) -> None:
        p = self.getElementPath(e)
        e.getparent().remove(e)
        self.debug(f'Removed element at {p}')

    def addElement(self, parent: etree._Element, e: etree._Element) -> etree._Element:
        parent.append(e)
        p = self.getElementPath(e)
        self.debug(f'Appended element {p}')
        return e

    def getXMLValue(self, parent: etree._Element, tag: str) -> Optional[str]:
        if (el := parent.find(tag)) is not None:
            return el.attrib.get('value')
        return None

    def createXMLValue(self, parent: etree._Element, tag: str, value: str) -> etree._Element:
        nv = etree.SubElement(parent, tag, {'value': value})
        p = self.getElementPath(parent)
        self.debug(f'Appended element {self.elementToString(nv)} to {p}')
        return nv

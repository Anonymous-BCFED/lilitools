
from lxml import etree

from lilitools.saves.character.body.body import Body
from lilitools.saves.character.fluids.cum import Cum
from lilitools.saves.character.fluids.stored_fluid import StoredFluid

from .base import BaseXMLMender


class CondomCumFixer(BaseXMLMender):
    def __init__(self, e: etree._Element) -> None:
        super().__init__(e)

        condom: etree._Element
        for condom in e.xpath('//item[@id="CONDOM_USED"]'):
            if (fse := condom.find("fluidStored")) is None:
                fs = StoredFluid()
                fs.bestial = False
                fs.charactersFluidID = "-9999,idfk"
                fs.body = Body()
                fs.cum = Cum()
                fs.cum.flavour = "CUM"
                fs.cum.type = "CUM_HUMAN"
                fs.cum.modifiers = set()
                fs.millilitres = "1"
                condom.append(fc.toXML())
                self.warn(
                    f"Fixed used condom at {self.getElementPath(condom)} - Added StoredFluid"
                )
            else:
                if fse.find("fluidCum") is None:
                    fc = Cum()
                    fc.flavour = "CUM"
                    fc.type = "CUM_HUMAN"
                    fc.modifiers = set()
                    fse.append(fc.toXML("fluidCum"))
                    self.warn(
                        f"Fixed used condom StoredFluid at {self.getElementPath(fse)} - Added Cum"
                    )

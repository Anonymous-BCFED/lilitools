from typing import Dict, Set
from .base import BaseXMLMender
from lxml import etree
class LitterIDDeduper(BaseXMLMender):
    def __init__(self, e: etree._Element) -> None:
        super().__init__(e)
        
        found: Dict[str, Set[etree._Element]] = {}
        for birthedLittersElement in e.xpath('//birthedLitters'):
            found = {}
            # wombholder = (birthedLittersElement
            #               .parent() #pregnancy
            #               .parent() # character
            # )
            for litter in birthedLittersElement:
                lid = litter.find('id').text
                if lid not in found.keys():
                    found[lid]=set(litter)
                else:
                    found[lid].add(litter)
                    self.warn(f'Duplicated birthed litter ID {lid!r} at {self.getElementPath(litter.find("id"))} (TODO: fix?)')

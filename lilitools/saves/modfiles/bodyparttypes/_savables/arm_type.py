#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional, Set
from lxml import etree
from lilitools.saves.character.body.arms import Arms
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.modfiles.savable_object_definition import SavableObjectDefinition
__all__ = ['RawArmType']
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawArmType(SavableObjectDefinition[Arms]):
    TAG = 'arm'
    _XMLID_TAG_BODYDESCRIPTION_ELEMENT: str = 'bodyDescription'
    _XMLID_TAG_COVERINGTYPE_ELEMENT: str = 'coveringType'
    _XMLID_TAG_DESCRIPTORSFEMININE_CHILD: str = 'descriptor'
    _XMLID_TAG_DESCRIPTORSFEMININE_PARENT: str = 'descriptorsFeminine'
    _XMLID_TAG_DESCRIPTORSMASCULINE_CHILD: str = 'descriptor'
    _XMLID_TAG_DESCRIPTORSMASCULINE_PARENT: str = 'descriptorsMasculine'
    _XMLID_TAG_FINGERDESCRIPTORSFEMININE_CHILD: str = 'descriptor'
    _XMLID_TAG_FINGERDESCRIPTORSFEMININE_PARENT: str = 'fingerDescriptorsFeminine'
    _XMLID_TAG_FINGERDESCRIPTORSMASCULINE_CHILD: str = 'descriptor'
    _XMLID_TAG_FINGERDESCRIPTORSMASCULINE_PARENT: str = 'fingerDescriptorsMasculine'
    _XMLID_TAG_FINGERNAMEPLURAL_ELEMENT: str = 'fingerNamePlural'
    _XMLID_TAG_FINGERNAMESINGULAR_ELEMENT: str = 'fingerName'
    _XMLID_TAG_HANDDESCRIPTORSFEMININE_CHILD: str = 'descriptor'
    _XMLID_TAG_HANDDESCRIPTORSFEMININE_PARENT: str = 'handDescriptorsFeminine'
    _XMLID_TAG_HANDDESCRIPTORSMASCULINE_CHILD: str = 'descriptor'
    _XMLID_TAG_HANDDESCRIPTORSMASCULINE_PARENT: str = 'handDescriptorsMasculine'
    _XMLID_TAG_HANDNAMEPLURAL_ELEMENT: str = 'handNamePlural'
    _XMLID_TAG_HANDNAMESINGULAR_ELEMENT: str = 'handName'
    _XMLID_TAG_NAMEPLURAL_ELEMENT: str = 'namePlural'
    _XMLID_TAG_NAMESINGULAR_ELEMENT: str = 'name'
    _XMLID_TAG_RACE_ELEMENT: str = 'race'
    _XMLID_TAG_TAGS_CHILD: str = 'tag'
    _XMLID_TAG_TAGS_PARENT: str = 'tags'
    _XMLID_TAG_TRANSFORMATIONDESCRIPTION_ELEMENT: str = 'transformationDescription'
    _XMLID_TAG_TRANSFORMATIONNAME_ELEMENT: str = 'transformationName'

    def __init__(self) -> None:
        super().__init__()
        self.race: str = ''  # Element
        self.coveringType: str = ''  # Element
        self.transformationName: Optional[str] = None  # Element
        self.flags: Set[str] = set()
        self.tags: Optional[Set[str]] = set()
        self.nameSingular: str = ''  # Element
        self.namePlural: str = ''  # Element
        self.descriptorsMasculine: Optional[Set[str]] = set()
        self.descriptorsFeminine: Optional[Set[str]] = set()
        self.handNameSingular: str = ''  # Element
        self.handNamePlural: str = ''  # Element
        self.handDescriptorsMasculine: Optional[Set[str]] = set()
        self.handDescriptorsFeminine: Optional[Set[str]] = set()
        self.fingerNameSingular: str = ''  # Element
        self.fingerNamePlural: str = ''  # Element
        self.fingerDescriptorsMasculine: Optional[Set[str]] = set()
        self.fingerDescriptorsFeminine: Optional[Set[str]] = set()
        self.transformationDescription: str = ''  # Element
        self.bodyDescription: str = ''  # Element

    def overrideTags(self, bodyDescription_element: _Optional_str = None, coveringType_element: _Optional_str = None, descriptorsFeminine_child: _Optional_str = None, descriptorsFeminine_parent: _Optional_str = None, descriptorsMasculine_child: _Optional_str = None, descriptorsMasculine_parent: _Optional_str = None, fingerDescriptorsFeminine_child: _Optional_str = None, fingerDescriptorsFeminine_parent: _Optional_str = None, fingerDescriptorsMasculine_child: _Optional_str = None, fingerDescriptorsMasculine_parent: _Optional_str = None, fingerNamePlural_element: _Optional_str = None, fingerNameSingular_element: _Optional_str = None, handDescriptorsFeminine_child: _Optional_str = None, handDescriptorsFeminine_parent: _Optional_str = None, handDescriptorsMasculine_child: _Optional_str = None, handDescriptorsMasculine_parent: _Optional_str = None, handNamePlural_element: _Optional_str = None, handNameSingular_element: _Optional_str = None, namePlural_element: _Optional_str = None, nameSingular_element: _Optional_str = None, race_element: _Optional_str = None, tags_child: _Optional_str = None, tags_parent: _Optional_str = None, transformationDescription_element: _Optional_str = None, transformationName_element: _Optional_str = None) -> None:
        if bodyDescription_element is not None:
            self._XMLID_TAG_BODYDESCRIPTION_ELEMENT = bodyDescription_element
        if coveringType_element is not None:
            self._XMLID_TAG_COVERINGTYPE_ELEMENT = coveringType_element
        if descriptorsFeminine_child is not None:
            self._XMLID_TAG_DESCRIPTORSFEMININE_CHILD = descriptorsFeminine_child
        if descriptorsFeminine_parent is not None:
            self._XMLID_TAG_DESCRIPTORSFEMININE_PARENT = descriptorsFeminine_parent
        if descriptorsMasculine_child is not None:
            self._XMLID_TAG_DESCRIPTORSMASCULINE_CHILD = descriptorsMasculine_child
        if descriptorsMasculine_parent is not None:
            self._XMLID_TAG_DESCRIPTORSMASCULINE_PARENT = descriptorsMasculine_parent
        if fingerDescriptorsFeminine_child is not None:
            self._XMLID_TAG_FINGERDESCRIPTORSFEMININE_CHILD = fingerDescriptorsFeminine_child
        if fingerDescriptorsFeminine_parent is not None:
            self._XMLID_TAG_FINGERDESCRIPTORSFEMININE_PARENT = fingerDescriptorsFeminine_parent
        if fingerDescriptorsMasculine_child is not None:
            self._XMLID_TAG_FINGERDESCRIPTORSMASCULINE_CHILD = fingerDescriptorsMasculine_child
        if fingerDescriptorsMasculine_parent is not None:
            self._XMLID_TAG_FINGERDESCRIPTORSMASCULINE_PARENT = fingerDescriptorsMasculine_parent
        if fingerNamePlural_element is not None:
            self._XMLID_TAG_FINGERNAMEPLURAL_ELEMENT = fingerNamePlural_element
        if fingerNameSingular_element is not None:
            self._XMLID_TAG_FINGERNAMESINGULAR_ELEMENT = fingerNameSingular_element
        if handDescriptorsFeminine_child is not None:
            self._XMLID_TAG_HANDDESCRIPTORSFEMININE_CHILD = handDescriptorsFeminine_child
        if handDescriptorsFeminine_parent is not None:
            self._XMLID_TAG_HANDDESCRIPTORSFEMININE_PARENT = handDescriptorsFeminine_parent
        if handDescriptorsMasculine_child is not None:
            self._XMLID_TAG_HANDDESCRIPTORSMASCULINE_CHILD = handDescriptorsMasculine_child
        if handDescriptorsMasculine_parent is not None:
            self._XMLID_TAG_HANDDESCRIPTORSMASCULINE_PARENT = handDescriptorsMasculine_parent
        if handNamePlural_element is not None:
            self._XMLID_TAG_HANDNAMEPLURAL_ELEMENT = handNamePlural_element
        if handNameSingular_element is not None:
            self._XMLID_TAG_HANDNAMESINGULAR_ELEMENT = handNameSingular_element
        if namePlural_element is not None:
            self._XMLID_TAG_NAMEPLURAL_ELEMENT = namePlural_element
        if nameSingular_element is not None:
            self._XMLID_TAG_NAMESINGULAR_ELEMENT = nameSingular_element
        if race_element is not None:
            self._XMLID_TAG_RACE_ELEMENT = race_element
        if tags_child is not None:
            self._XMLID_TAG_TAGS_CHILD = tags_child
        if tags_parent is not None:
            self._XMLID_TAG_TAGS_PARENT = tags_parent
        if transformationDescription_element is not None:
            self._XMLID_TAG_TRANSFORMATIONDESCRIPTION_ELEMENT = transformationDescription_element
        if transformationName_element is not None:
            self._XMLID_TAG_TRANSFORMATIONNAME_ELEMENT = transformationName_element

    def _fromXML_flags(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromXML_flags()')

    def _toXML_flags(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toXML_flags()')

    def _fromDict_flags(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromDict_flags()')

    def _toDict_flags(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toDict_flags()')

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        lp: etree._Element
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_RACE_ELEMENT)) is not None:
            self.race = sf.text
        else:
            raise ElementRequiredException(self, e, 'race', self._XMLID_TAG_RACE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_COVERINGTYPE_ELEMENT)) is not None:
            self.coveringType = sf.text
        else:
            raise ElementRequiredException(self, e, 'coveringType', self._XMLID_TAG_COVERINGTYPE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_TRANSFORMATIONNAME_ELEMENT)) is not None:
            self.transformationName = sf.text
        self._fromXML_flags(e)
        if (lf := e.find(self._XMLID_TAG_TAGS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_TAGS_CHILD):
                    self.tags.add(lc.text)
        if (sf := e.find(self._XMLID_TAG_NAMESINGULAR_ELEMENT)) is not None:
            self.nameSingular = sf.text
        else:
            raise ElementRequiredException(self, e, 'nameSingular', self._XMLID_TAG_NAMESINGULAR_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_NAMEPLURAL_ELEMENT)) is not None:
            self.namePlural = sf.text
        else:
            raise ElementRequiredException(self, e, 'namePlural', self._XMLID_TAG_NAMEPLURAL_ELEMENT)
        if (lf := e.find(self._XMLID_TAG_DESCRIPTORSMASCULINE_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_DESCRIPTORSMASCULINE_CHILD):
                    self.descriptorsMasculine.add(lc.text)
        if (lf := e.find(self._XMLID_TAG_DESCRIPTORSFEMININE_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_DESCRIPTORSFEMININE_CHILD):
                    self.descriptorsFeminine.add(lc.text)
        if (sf := e.find(self._XMLID_TAG_HANDNAMESINGULAR_ELEMENT)) is not None:
            self.handNameSingular = sf.text
        else:
            raise ElementRequiredException(self, e, 'handNameSingular', self._XMLID_TAG_HANDNAMESINGULAR_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_HANDNAMEPLURAL_ELEMENT)) is not None:
            self.handNamePlural = sf.text
        else:
            raise ElementRequiredException(self, e, 'handNamePlural', self._XMLID_TAG_HANDNAMEPLURAL_ELEMENT)
        if (lf := e.find(self._XMLID_TAG_HANDDESCRIPTORSMASCULINE_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_HANDDESCRIPTORSMASCULINE_CHILD):
                    self.handDescriptorsMasculine.add(lc.text)
        if (lf := e.find(self._XMLID_TAG_HANDDESCRIPTORSFEMININE_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_HANDDESCRIPTORSFEMININE_CHILD):
                    self.handDescriptorsFeminine.add(lc.text)
        if (sf := e.find(self._XMLID_TAG_FINGERNAMESINGULAR_ELEMENT)) is not None:
            self.fingerNameSingular = sf.text
        else:
            raise ElementRequiredException(self, e, 'fingerNameSingular', self._XMLID_TAG_FINGERNAMESINGULAR_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_FINGERNAMEPLURAL_ELEMENT)) is not None:
            self.fingerNamePlural = sf.text
        else:
            raise ElementRequiredException(self, e, 'fingerNamePlural', self._XMLID_TAG_FINGERNAMEPLURAL_ELEMENT)
        if (lf := e.find(self._XMLID_TAG_FINGERDESCRIPTORSMASCULINE_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_FINGERDESCRIPTORSMASCULINE_CHILD):
                    self.fingerDescriptorsMasculine.add(lc.text)
        if (lf := e.find(self._XMLID_TAG_FINGERDESCRIPTORSFEMININE_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_FINGERDESCRIPTORSFEMININE_CHILD):
                    self.fingerDescriptorsFeminine.add(lc.text)
        if (sf := e.find(self._XMLID_TAG_TRANSFORMATIONDESCRIPTION_ELEMENT)) is not None:
            self.transformationDescription = sf.text
        else:
            raise ElementRequiredException(self, e, 'transformationDescription', self._XMLID_TAG_TRANSFORMATIONDESCRIPTION_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_BODYDESCRIPTION_ELEMENT)) is not None:
            self.bodyDescription = sf.text
        else:
            raise ElementRequiredException(self, e, 'bodyDescription', self._XMLID_TAG_BODYDESCRIPTION_ELEMENT)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        etree.SubElement(e, self._XMLID_TAG_RACE_ELEMENT).text = str(self.race)
        etree.SubElement(e, self._XMLID_TAG_COVERINGTYPE_ELEMENT).text = str(self.coveringType)
        if self.transformationName is not None:
            etree.SubElement(e, self._XMLID_TAG_TRANSFORMATIONNAME_ELEMENT).text = str(self.transformationName)
        self._toXML_flags(e)
        if self.tags is not None and len(self.tags) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_TAGS_PARENT)
            for lv in sorted(self.tags, key=lambda x: x):
                etree.SubElement(lp, self._XMLID_TAG_TAGS_CHILD, {}).text = lv
        etree.SubElement(e, self._XMLID_TAG_NAMESINGULAR_ELEMENT).text = str(self.nameSingular)
        etree.SubElement(e, self._XMLID_TAG_NAMEPLURAL_ELEMENT).text = str(self.namePlural)
        if self.descriptorsMasculine is not None and len(self.descriptorsMasculine) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_DESCRIPTORSMASCULINE_PARENT)
            for lv in sorted(self.descriptorsMasculine, key=lambda x: x):
                etree.SubElement(lp, self._XMLID_TAG_DESCRIPTORSMASCULINE_CHILD, {}).text = lv
        if self.descriptorsFeminine is not None and len(self.descriptorsFeminine) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_DESCRIPTORSFEMININE_PARENT)
            for lv in sorted(self.descriptorsFeminine, key=lambda x: x):
                etree.SubElement(lp, self._XMLID_TAG_DESCRIPTORSFEMININE_CHILD, {}).text = lv
        etree.SubElement(e, self._XMLID_TAG_HANDNAMESINGULAR_ELEMENT).text = str(self.handNameSingular)
        etree.SubElement(e, self._XMLID_TAG_HANDNAMEPLURAL_ELEMENT).text = str(self.handNamePlural)
        if self.handDescriptorsMasculine is not None and len(self.handDescriptorsMasculine) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_HANDDESCRIPTORSMASCULINE_PARENT)
            for lv in sorted(self.handDescriptorsMasculine, key=lambda x: x):
                etree.SubElement(lp, self._XMLID_TAG_HANDDESCRIPTORSMASCULINE_CHILD, {}).text = lv
        if self.handDescriptorsFeminine is not None and len(self.handDescriptorsFeminine) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_HANDDESCRIPTORSFEMININE_PARENT)
            for lv in sorted(self.handDescriptorsFeminine, key=lambda x: x):
                etree.SubElement(lp, self._XMLID_TAG_HANDDESCRIPTORSFEMININE_CHILD, {}).text = lv
        etree.SubElement(e, self._XMLID_TAG_FINGERNAMESINGULAR_ELEMENT).text = str(self.fingerNameSingular)
        etree.SubElement(e, self._XMLID_TAG_FINGERNAMEPLURAL_ELEMENT).text = str(self.fingerNamePlural)
        if self.fingerDescriptorsMasculine is not None and len(self.fingerDescriptorsMasculine) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_FINGERDESCRIPTORSMASCULINE_PARENT)
            for lv in sorted(self.fingerDescriptorsMasculine, key=lambda x: x):
                etree.SubElement(lp, self._XMLID_TAG_FINGERDESCRIPTORSMASCULINE_CHILD, {}).text = lv
        if self.fingerDescriptorsFeminine is not None and len(self.fingerDescriptorsFeminine) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_FINGERDESCRIPTORSFEMININE_PARENT)
            for lv in sorted(self.fingerDescriptorsFeminine, key=lambda x: x):
                etree.SubElement(lp, self._XMLID_TAG_FINGERDESCRIPTORSFEMININE_CHILD, {}).text = lv
        etree.SubElement(e, self._XMLID_TAG_TRANSFORMATIONDESCRIPTION_ELEMENT).text = str(self.transformationDescription)
        etree.SubElement(e, self._XMLID_TAG_BODYDESCRIPTION_ELEMENT).text = str(self.bodyDescription)
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['race'] = str(self.race)
        data['coveringType'] = str(self.coveringType)
        if self.transformationName is not None:
            data['transformationName'] = str(self.transformationName)
        self._toDict_flags(data)
        if self.tags is None or len(self.tags) > 0:
            data['tags'] = list()
        else:
            lc = list()
            if len(self.tags) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['tags'] = lc
        data['nameSingular'] = str(self.nameSingular)
        data['namePlural'] = str(self.namePlural)
        if self.descriptorsMasculine is None or len(self.descriptorsMasculine) > 0:
            data['descriptorsMasculine'] = list()
        else:
            lc = list()
            if len(self.descriptorsMasculine) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['descriptorsMasculine'] = lc
        if self.descriptorsFeminine is None or len(self.descriptorsFeminine) > 0:
            data['descriptorsFeminine'] = list()
        else:
            lc = list()
            if len(self.descriptorsFeminine) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['descriptorsFeminine'] = lc
        data['handNameSingular'] = str(self.handNameSingular)
        data['handNamePlural'] = str(self.handNamePlural)
        if self.handDescriptorsMasculine is None or len(self.handDescriptorsMasculine) > 0:
            data['handDescriptorsMasculine'] = list()
        else:
            lc = list()
            if len(self.handDescriptorsMasculine) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['handDescriptorsMasculine'] = lc
        if self.handDescriptorsFeminine is None or len(self.handDescriptorsFeminine) > 0:
            data['handDescriptorsFeminine'] = list()
        else:
            lc = list()
            if len(self.handDescriptorsFeminine) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['handDescriptorsFeminine'] = lc
        data['fingerNameSingular'] = str(self.fingerNameSingular)
        data['fingerNamePlural'] = str(self.fingerNamePlural)
        if self.fingerDescriptorsMasculine is None or len(self.fingerDescriptorsMasculine) > 0:
            data['fingerDescriptorsMasculine'] = list()
        else:
            lc = list()
            if len(self.fingerDescriptorsMasculine) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['fingerDescriptorsMasculine'] = lc
        if self.fingerDescriptorsFeminine is None or len(self.fingerDescriptorsFeminine) > 0:
            data['fingerDescriptorsFeminine'] = list()
        else:
            lc = list()
            if len(self.fingerDescriptorsFeminine) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['fingerDescriptorsFeminine'] = lc
        data['transformationDescription'] = str(self.transformationDescription)
        data['bodyDescription'] = str(self.bodyDescription)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('race')) is not None:
            self.race = data['race']
        else:
            raise KeyRequiredException(self, data, 'race', 'race')
        if (sf := data.get('coveringType')) is not None:
            self.coveringType = data['coveringType']
        else:
            raise KeyRequiredException(self, data, 'coveringType', 'coveringType')
        if (sf := data.get('transformationName')) is not None:
            self.transformationName = data['transformationName']
        else:
            self.transformationName = None
        self._fromDict_flags(data)
        if (lv := self.tags) is not None:
            for le in lv:
                self.tags.add(str(le))
        else:
            self.tags = set()
        if (sf := data.get('nameSingular')) is not None:
            self.nameSingular = data['nameSingular']
        else:
            raise KeyRequiredException(self, data, 'nameSingular', 'nameSingular')
        if (sf := data.get('namePlural')) is not None:
            self.namePlural = data['namePlural']
        else:
            raise KeyRequiredException(self, data, 'namePlural', 'namePlural')
        if (lv := self.descriptorsMasculine) is not None:
            for le in lv:
                self.descriptorsMasculine.add(str(le))
        else:
            self.descriptorsMasculine = set()
        if (lv := self.descriptorsFeminine) is not None:
            for le in lv:
                self.descriptorsFeminine.add(str(le))
        else:
            self.descriptorsFeminine = set()
        if (sf := data.get('handNameSingular')) is not None:
            self.handNameSingular = data['handNameSingular']
        else:
            raise KeyRequiredException(self, data, 'handNameSingular', 'handNameSingular')
        if (sf := data.get('handNamePlural')) is not None:
            self.handNamePlural = data['handNamePlural']
        else:
            raise KeyRequiredException(self, data, 'handNamePlural', 'handNamePlural')
        if (lv := self.handDescriptorsMasculine) is not None:
            for le in lv:
                self.handDescriptorsMasculine.add(str(le))
        else:
            self.handDescriptorsMasculine = set()
        if (lv := self.handDescriptorsFeminine) is not None:
            for le in lv:
                self.handDescriptorsFeminine.add(str(le))
        else:
            self.handDescriptorsFeminine = set()
        if (sf := data.get('fingerNameSingular')) is not None:
            self.fingerNameSingular = data['fingerNameSingular']
        else:
            raise KeyRequiredException(self, data, 'fingerNameSingular', 'fingerNameSingular')
        if (sf := data.get('fingerNamePlural')) is not None:
            self.fingerNamePlural = data['fingerNamePlural']
        else:
            raise KeyRequiredException(self, data, 'fingerNamePlural', 'fingerNamePlural')
        if (lv := self.fingerDescriptorsMasculine) is not None:
            for le in lv:
                self.fingerDescriptorsMasculine.add(str(le))
        else:
            self.fingerDescriptorsMasculine = set()
        if (lv := self.fingerDescriptorsFeminine) is not None:
            for le in lv:
                self.fingerDescriptorsFeminine.add(str(le))
        else:
            self.fingerDescriptorsFeminine = set()
        if (sf := data.get('transformationDescription')) is not None:
            self.transformationDescription = data['transformationDescription']
        else:
            raise KeyRequiredException(self, data, 'transformationDescription', 'transformationDescription')
        if (sf := data.get('bodyDescription')) is not None:
            self.bodyDescription = data['bodyDescription']
        else:
            raise KeyRequiredException(self, data, 'bodyDescription', 'bodyDescription')

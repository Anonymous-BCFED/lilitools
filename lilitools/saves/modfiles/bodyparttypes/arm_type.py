from typing import Optional, Set
from lilitools.saves.character.body.arms import Arms
from lilitools.saves.modfiles.bodyparttypes._savables.arm_type import RawArmType
from lxml import etree

from lilitools.saves.savable import XML2BOOL

__all__ = ['ArmType']


class ArmType(RawArmType):
    INSTANCE_TYPE = Arms

    def __init__(self,
                 id: Optional[str] = None,
                 nameSingular: Optional[str] = None,
                 namePlural: Optional[str] = None,
                 race: Optional[str] = None,
                 coveringType: Optional[str] = None,
                 transformationName: Optional[str] = None,
                 tags: Optional[Set[str]] = None,
                 flags: Optional[Set[str]] = None,
                 ) -> None:
        super().__init__()
        self.id: str = id or ''
        self.nameSingular = nameSingular or ''
        self.namePlural = namePlural or ''
        self.race = race or ''
        self.coveringType = coveringType or ''
        self.transformationName = transformationName
        self.tags: Set[str] = tags or set()
        self.flags: Set[str] = flags or set()

    def _fromXML_flags(self, e: etree._Element) -> None:
        uha: Optional[etree._Element]
        af: Optional[etree._Element]
        if (uha := e.find('underarmHairAllowed')) is not None:
            if XML2BOOL[uha.text]:
                self.tags.add('underarmHairAllowed')
        if (af := e.find('allowsFlight')) is not None:
            if XML2BOOL[af.text]:
                self.tags.add('allowsFlight')

    def _toXML_flags(self, e: etree._Element) -> None:
        raise NotImplementedError()

    def instantiate(self) -> Arms:
        o = super().instantiate()
        o.type = self.id
        o.rows = 1
        o.underarmHair = 'underarmHairAllowed' in self.tags
        return o

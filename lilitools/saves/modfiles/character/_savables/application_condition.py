#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL, Savable
__all__ = ['RawApplicationCondition']
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawApplicationCondition(Savable):
    TAG = 'applicationCondition'
    _XMLID_ATTR_SHORTCONDITIONALCHECK_ATTRIBUTE: str = 'shortConditionalCheck'

    def __init__(self) -> None:
        super().__init__()
        self.shortConditionalCheck: bool = False
        self.text: str = ''  # CDATA

    def overrideAttrs(self, shortConditionalCheck_attribute: _Optional_str = None) -> None:
        if shortConditionalCheck_attribute is not None:
            self._XMLID_ATTR_SHORTCONDITIONALCHECK_ATTRIBUTE = shortConditionalCheck_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_SHORTCONDITIONALCHECK_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_SHORTCONDITIONALCHECK_ATTRIBUTE, 'shortConditionalCheck')
        else:
            self.shortConditionalCheck = XML2BOOL[value]
        self.text = e.text

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_SHORTCONDITIONALCHECK_ATTRIBUTE] = str(self.shortConditionalCheck).lower()
        e.text = etree.CDATA(str(self.text))
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['shortConditionalCheck'] = bool(self.shortConditionalCheck)
        data['text'] = self.text
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'shortConditionalCheck' not in data:
            raise KeyRequiredException(self, data, 'shortConditionalCheck', 'shortConditionalCheck')
        self.shortConditionalCheck = bool(data['shortConditionalCheck'])
        self.text = data['text']

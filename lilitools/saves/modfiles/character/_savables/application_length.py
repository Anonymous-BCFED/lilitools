#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL, Savable
__all__ = ['RawApplicationLength']
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawApplicationLength(Savable):
    TAG = 'applicationLength'
    _XMLID_ATTR_CONSTANTREFRESH_ATTRIBUTE: str = 'constantRefresh'

    def __init__(self) -> None:
        super().__init__()
        self.constantRefresh: bool = False
        self.text: str = ''  # CDATA

    def overrideAttrs(self, constantRefresh_attribute: _Optional_str = None) -> None:
        if constantRefresh_attribute is not None:
            self._XMLID_ATTR_CONSTANTREFRESH_ATTRIBUTE = constantRefresh_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_CONSTANTREFRESH_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_CONSTANTREFRESH_ATTRIBUTE, 'constantRefresh')
        else:
            self.constantRefresh = XML2BOOL[value]
        self.text = e.text

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_CONSTANTREFRESH_ATTRIBUTE] = str(self.constantRefresh).lower()
        e.text = etree.CDATA(str(self.text))
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['constantRefresh'] = bool(self.constantRefresh)
        data['text'] = self.text
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'constantRefresh' not in data:
            raise KeyRequiredException(self, data, 'constantRefresh', 'constantRefresh')
        self.constantRefresh = bool(data['constantRefresh'])
        self.text = data['text']

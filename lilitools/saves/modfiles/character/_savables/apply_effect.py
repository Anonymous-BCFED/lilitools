#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawApplyEffect']
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawApplyEffect(Savable):
    TAG = 'applyEffect'
    _XMLID_ATTR_INTERVAL_ATTRIBUTE: str = 'interval'

    def __init__(self) -> None:
        super().__init__()
        self.interval: int = 0
        self.text: str = ''  # CDATA

    def overrideAttrs(self, interval_attribute: _Optional_str = None) -> None:
        if interval_attribute is not None:
            self._XMLID_ATTR_INTERVAL_ATTRIBUTE = interval_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_INTERVAL_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_INTERVAL_ATTRIBUTE, 'interval')
        else:
            self.interval = int(value)
        self.text = e.text

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_INTERVAL_ATTRIBUTE] = str(self.interval)
        e.text = etree.CDATA(str(self.text))
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['interval'] = int(self.interval)
        data['text'] = self.text
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'interval' not in data:
            raise KeyRequiredException(self, data, 'interval', 'interval')
        self.interval = int(data['interval'])
        self.text = data['text']

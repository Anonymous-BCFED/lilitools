#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.enums.tf_modifier import ETFModifier
from lilitools.saves.enums.tf_potency import ETFPotency
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawClothingItemEffect']
## from [LT]/src/com/lilithsthrone/game/inventory/enchanting/ItemEffect.java: public Element saveAsXML(Element parentElement, Document doc) { @ bjXWtVUMrBG5oAXmLyGN0aJ3cE/qKqmK65A5WNhtj63MMVtTJ8y1aUpQxNKfxc69FkF+Hj8i4HrhwBZ4viDUNg==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawClothingItemEffect(Savable):
    TAG = 'effect'
    _XMLID_ATTR_LIMIT_ATTRIBUTE: str = 'limit'
    _XMLID_ATTR_MOD1_ATTRIBUTE: str = 'mod1'
    _XMLID_ATTR_MOD2_ATTRIBUTE: str = 'mod2'
    _XMLID_ATTR_POTENCY_ATTRIBUTE: str = 'potency'
    _XMLID_ATTR_TIMER_ATTRIBUTE: str = 'timer'
    _XMLID_ATTR_TYPE_ATTRIBUTE: str = 'type'

    def __init__(self) -> None:
        super().__init__()
        self.type: str = ''
        self.mod1: Optional[ETFModifier] = None
        self.mod2: Optional[ETFModifier] = None
        self.potency: Optional[ETFPotency] = None
        self.limit: Optional[int] = None
        self.timer: Optional[int] = None

    def overrideAttrs(self, limit_attribute: _Optional_str = None, mod1_attribute: _Optional_str = None, mod2_attribute: _Optional_str = None, potency_attribute: _Optional_str = None, timer_attribute: _Optional_str = None, type_attribute: _Optional_str = None) -> None:
        if limit_attribute is not None:
            self._XMLID_ATTR_LIMIT_ATTRIBUTE = limit_attribute
        if mod1_attribute is not None:
            self._XMLID_ATTR_MOD1_ATTRIBUTE = mod1_attribute
        if mod2_attribute is not None:
            self._XMLID_ATTR_MOD2_ATTRIBUTE = mod2_attribute
        if potency_attribute is not None:
            self._XMLID_ATTR_POTENCY_ATTRIBUTE = potency_attribute
        if timer_attribute is not None:
            self._XMLID_ATTR_TIMER_ATTRIBUTE = timer_attribute
        if type_attribute is not None:
            self._XMLID_ATTR_TYPE_ATTRIBUTE = type_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_TYPE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_TYPE_ATTRIBUTE, 'type')
        else:
            self.type = str(value)
        value = e.get(self._XMLID_ATTR_MOD1_ATTRIBUTE, None)
        if value is None:
            self.mod1 = None
        else:
            if value == 'null':
                self.mod1 = None
            else:
                self.mod1 = ETFModifier[value]
        value = e.get(self._XMLID_ATTR_MOD2_ATTRIBUTE, None)
        if value is None:
            self.mod2 = None
        else:
            if value == 'null':
                self.mod2 = None
            else:
                self.mod2 = ETFModifier[value]
        value = e.get(self._XMLID_ATTR_POTENCY_ATTRIBUTE, None)
        if value is None:
            self.potency = None
        else:
            if value == 'null':
                self.potency = None
            else:
                self.potency = ETFPotency[value]
        value = e.get(self._XMLID_ATTR_LIMIT_ATTRIBUTE, None)
        if value is None:
            self.limit = None
        else:
            self.limit = int(value)
        value = e.get(self._XMLID_ATTR_TIMER_ATTRIBUTE, None)
        if value is None:
            self.timer = None
        else:
            self.timer = int(value)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_TYPE_ATTRIBUTE] = str(self.type)
        if self.mod1 is None:
            e.attrib[self._XMLID_ATTR_MOD1_ATTRIBUTE] = 'null'
        else:
            e.attrib[self._XMLID_ATTR_MOD1_ATTRIBUTE] = self.mod1.name
        if self.mod2 is None:
            e.attrib[self._XMLID_ATTR_MOD2_ATTRIBUTE] = 'null'
        else:
            e.attrib[self._XMLID_ATTR_MOD2_ATTRIBUTE] = self.mod2.name
        if self.potency is None:
            e.attrib[self._XMLID_ATTR_POTENCY_ATTRIBUTE] = 'null'
        else:
            e.attrib[self._XMLID_ATTR_POTENCY_ATTRIBUTE] = self.potency.name
        if self.limit is not None:
            e.attrib[self._XMLID_ATTR_LIMIT_ATTRIBUTE] = str(self.limit)
        if self.timer is not None:
            e.attrib[self._XMLID_ATTR_TIMER_ATTRIBUTE] = str(self.timer)
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['type'] = self.type
        if self.mod1 is None:
            data['mod1'] = 'null'
        else:
            data['mod1'] = self.mod1.name
        if self.mod2 is None:
            data['mod2'] = 'null'
        else:
            data['mod2'] = self.mod2.name
        if self.potency is None:
            data['potency'] = 'null'
        else:
            data['potency'] = self.potency.name
        if self.limit is not None:
            data['limit'] = int(self.limit)
        if self.timer is not None:
            data['timer'] = int(self.timer)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'type' not in data:
            raise KeyRequiredException(self, data, 'type', 'type')
        self.type = str(data['type'])
        self.mod1 = ETFModifier[data['mod1']] if 'mod1' in data else None
        self.mod2 = ETFModifier[data['mod2']] if 'mod2' in data else None
        self.potency = ETFPotency[data['potency']] if 'potency' in data else None
        self.limit = int(data['limit']) if 'limit' in data else None
        self.timer = int(data['timer']) if 'timer' in data else None

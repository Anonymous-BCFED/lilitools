#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, List, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.modfiles.character._savables.application_condition import RawApplicationCondition
from lilitools.saves.modfiles.character._savables.application_length import RawApplicationLength
from lilitools.saves.modfiles.character._savables.apply_effect import RawApplyEffect
from lilitools.saves.modfiles.character.clothing.core_attributes import ClothingCoreAttributes
from lilitools.saves.savable import Savable
__all__ = ['RawClothingType']
## from [LT]/src/com/lilithsthrone/game/inventory/clothing/AbstractClothingType.java: public AbstractClothingType(File clothingXMLFile, String author) throws XMLLoadException { // Be sure to catch this exception correctly - if it's thrown mod is invalid and should not be continued to load @ 0H2efZueZVnr2NDv4NlSBX6L9ADDyDw9IeAbZJd91ki340+clNZmyNj8g7TdOt0grqTVXq8RzpgOnMk9pB+t3w==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawClothingType(Savable):
    TAG = 'clothing'
    _XMLID_ATTR_APPLICATIONCONDITION_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_APPLICATIONLENGTH_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_APPLYEFFECT_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_COREATTRIBUTES_ATTRIBUTE: str = 'value'
    _XMLID_TAG_APPLICATIONCONDITION_ELEMENT: str = 'applicationCondition'
    _XMLID_TAG_APPLICATIONLENGTH_ELEMENT: str = 'applicationLength'
    _XMLID_TAG_APPLYEFFECT_ELEMENT: str = 'applyEffect'
    _XMLID_TAG_APPLYPOSTREMOVALEFFECT_ELEMENT: str = 'applyPostRemovalEffect'
    _XMLID_TAG_APPLYREMOVALEFFECT_ELEMENT: str = 'applyRemovalEffect'
    _XMLID_TAG_COREATTRIBUTES_ELEMENT: str = 'coreAttributes'
    _XMLID_TAG_EXTRAEFFECTS_CHILD: str = 'effect'
    _XMLID_TAG_EXTRAEFFECTS_PARENT: str = 'extraEffects'

    def __init__(self) -> None:
        super().__init__()
        self.coreAttributes: ClothingCoreAttributes = ClothingCoreAttributes()  # Element
        self.extraEffects: Optional[List[str]] = list()
        self.applicationCondition: Optional[RawApplicationCondition] = None  # Element
        self.applicationLength: Optional[RawApplicationLength] = None  # Element
        self.applyEffect: Optional[RawApplyEffect] = None  # Element
        self.applyRemovalEffect: Optional[str] = None  # Element
        self.applyPostRemovalEffect: Optional[str] = None  # Element

    def overrideAttrs(self, applicationCondition_attribute: _Optional_str = None, applicationLength_attribute: _Optional_str = None, applyEffect_attribute: _Optional_str = None, coreAttributes_attribute: _Optional_str = None) -> None:
        if applicationCondition_attribute is not None:
            self._XMLID_ATTR_APPLICATIONCONDITION_ATTRIBUTE = applicationCondition_attribute
        if applicationLength_attribute is not None:
            self._XMLID_ATTR_APPLICATIONLENGTH_ATTRIBUTE = applicationLength_attribute
        if applyEffect_attribute is not None:
            self._XMLID_ATTR_APPLYEFFECT_ATTRIBUTE = applyEffect_attribute
        if coreAttributes_attribute is not None:
            self._XMLID_ATTR_COREATTRIBUTES_ATTRIBUTE = coreAttributes_attribute

    def overrideTags(self, applicationCondition_element: _Optional_str = None, applicationLength_element: _Optional_str = None, applyEffect_element: _Optional_str = None, applyPostRemovalEffect_element: _Optional_str = None, applyRemovalEffect_element: _Optional_str = None, coreAttributes_element: _Optional_str = None, extraEffects_child: _Optional_str = None, extraEffects_parent: _Optional_str = None) -> None:
        if applicationCondition_element is not None:
            self._XMLID_TAG_APPLICATIONCONDITION_ELEMENT = applicationCondition_element
        if applicationLength_element is not None:
            self._XMLID_TAG_APPLICATIONLENGTH_ELEMENT = applicationLength_element
        if applyEffect_element is not None:
            self._XMLID_TAG_APPLYEFFECT_ELEMENT = applyEffect_element
        if applyPostRemovalEffect_element is not None:
            self._XMLID_TAG_APPLYPOSTREMOVALEFFECT_ELEMENT = applyPostRemovalEffect_element
        if applyRemovalEffect_element is not None:
            self._XMLID_TAG_APPLYREMOVALEFFECT_ELEMENT = applyRemovalEffect_element
        if coreAttributes_element is not None:
            self._XMLID_TAG_COREATTRIBUTES_ELEMENT = coreAttributes_element
        if extraEffects_child is not None:
            self._XMLID_TAG_EXTRAEFFECTS_CHILD = extraEffects_child
        if extraEffects_parent is not None:
            self._XMLID_TAG_EXTRAEFFECTS_PARENT = extraEffects_parent

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        lp: etree._Element
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_COREATTRIBUTES_ELEMENT)) is not None:
            self.coreAttributes = ClothingCoreAttributes()
            self.coreAttributes.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'coreAttributes', self._XMLID_TAG_COREATTRIBUTES_ELEMENT)
        if (lf := e.find(self._XMLID_TAG_EXTRAEFFECTS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_EXTRAEFFECTS_CHILD):
                    self.extraEffects.append(lc.text)
        if (sf := e.find(self._XMLID_TAG_APPLICATIONCONDITION_ELEMENT)) is not None:
            self.applicationCondition = RawApplicationCondition()
            self.applicationCondition.fromXML(sf)
        if (sf := e.find(self._XMLID_TAG_APPLICATIONLENGTH_ELEMENT)) is not None:
            self.applicationLength = RawApplicationLength()
            self.applicationLength.fromXML(sf)
        if (sf := e.find(self._XMLID_TAG_APPLYEFFECT_ELEMENT)) is not None:
            self.applyEffect = RawApplyEffect()
            self.applyEffect.fromXML(sf)
        if (sf := e.find(self._XMLID_TAG_APPLYREMOVALEFFECT_ELEMENT)) is not None:
            self.applyRemovalEffect = sf.text
        if (sf := e.find(self._XMLID_TAG_APPLYPOSTREMOVALEFFECT_ELEMENT)) is not None:
            self.applyPostRemovalEffect = sf.text

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        e.append(self.coreAttributes.toXML(self._XMLID_TAG_COREATTRIBUTES_ELEMENT))
        if self.extraEffects is not None and len(self.extraEffects) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_EXTRAEFFECTS_PARENT)
            for lv in self.extraEffects:
                etree.SubElement(lp, self._XMLID_TAG_EXTRAEFFECTS_CHILD, {}).text = lv
        if self.applicationCondition is not None:
            e.append(self.applicationCondition.toXML(self._XMLID_TAG_APPLICATIONCONDITION_ELEMENT))
        if self.applicationLength is not None:
            e.append(self.applicationLength.toXML(self._XMLID_TAG_APPLICATIONLENGTH_ELEMENT))
        if self.applyEffect is not None:
            e.append(self.applyEffect.toXML(self._XMLID_TAG_APPLYEFFECT_ELEMENT))
        if self.applyRemovalEffect is not None:
            etree.SubElement(e, self._XMLID_TAG_APPLYREMOVALEFFECT_ELEMENT).text = str(self.applyRemovalEffect)
        if self.applyPostRemovalEffect is not None:
            etree.SubElement(e, self._XMLID_TAG_APPLYPOSTREMOVALEFFECT_ELEMENT).text = str(self.applyPostRemovalEffect)
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['coreAttributes'] = self.coreAttributes.toDict()
        if self.extraEffects is None or len(self.extraEffects) > 0:
            data['extraEffects'] = list()
        else:
            lc = list()
            if len(self.extraEffects) > 0:
                for lv in lc:
                    lc.append(str(lv))
            data['extraEffects'] = lc
        if self.applicationCondition is not None:
            data['applicationCondition'] = self.applicationCondition.toDict()
        if self.applicationLength is not None:
            data['applicationLength'] = self.applicationLength.toDict()
        if self.applyEffect is not None:
            data['applyEffect'] = self.applyEffect.toDict()
        if self.applyRemovalEffect is not None:
            data['applyRemovalEffect'] = str(self.applyRemovalEffect)
        if self.applyPostRemovalEffect is not None:
            data['applyPostRemovalEffect'] = str(self.applyPostRemovalEffect)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('coreAttributes')) is not None:
            self.coreAttributes = ClothingCoreAttributes()
            self.coreAttributes.fromDict(data['coreAttributes'])
        else:
            raise KeyRequiredException(self, data, 'coreAttributes', 'coreAttributes')
        if (lv := self.extraEffects) is not None:
            for le in lv:
                self.extraEffects.append(str(le))
        else:
            self.extraEffects = list()
        if (sf := data.get('applicationCondition')) is not None:
            self.applicationCondition = RawApplicationCondition()
            self.applicationCondition.fromDict(data['applicationCondition'])
        else:
            self.applicationCondition = None
        if (sf := data.get('applicationLength')) is not None:
            self.applicationLength = RawApplicationLength()
            self.applicationLength.fromDict(data['applicationLength'])
        else:
            self.applicationLength = None
        if (sf := data.get('applyEffect')) is not None:
            self.applyEffect = RawApplyEffect()
            self.applyEffect.fromDict(data['applyEffect'])
        else:
            self.applyEffect = None
        if (sf := data.get('applyRemovalEffect')) is not None:
            self.applyRemovalEffect = data['applyRemovalEffect']
        else:
            self.applyRemovalEffect = None
        if (sf := data.get('applyPostRemovalEffect')) is not None:
            self.applyPostRemovalEffect = data['applyPostRemovalEffect']
        else:
            self.applyPostRemovalEffect = None

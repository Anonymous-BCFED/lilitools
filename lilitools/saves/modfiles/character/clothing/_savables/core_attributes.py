#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from collections import OrderedDict
from typing import Any, Dict, List, Optional, Set
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.inventory.blocked_parts import BlockedParts
from lilitools.saves.inventoryslot import EInventorySlot
from lilitools.saves.items.enums.rarity import ERarity
from lilitools.saves.modfiles.character.clothing.clothing_item_effect import ClothingItemEffect
from lilitools.saves.savable import XML2BOOL, Savable
__all__ = ['RawClothingCoreAttributes']
## from [LT]/src/com/lilithsthrone/game/inventory/clothing/AbstractClothingType.java: public AbstractClothingType(File clothingXMLFile, String author) throws XMLLoadException { // Be sure to catch this exception correctly - if it's thrown mod is invalid and should not be continued to load @ 0H2efZueZVnr2NDv4NlSBX6L9ADDyDw9IeAbZJd91ki340+clNZmyNj8g7TdOt0grqTVXq8RzpgOnMk9pB+t3w==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawClothingCoreAttributes(Savable):
    TAG = 'coreAttributes'
    _XMLID_ATTR_APPENDCOLOURNAME_ATTRIBUTE: str = 'appendColourName'
    _XMLID_ATTR_ISPLURAL_ATTRIBUTE: str = 'pluralByDefault'
    _XMLID_TAG_APPENDCOLOURNAME_ELEMENT: str = 'name'
    _XMLID_TAG_AUTHORTAG_ELEMENT: str = 'authorTag'
    _XMLID_TAG_BASEVALUE_ELEMENT: str = 'baseValue'
    _XMLID_TAG_CLOTHINGAUTHORTAG_ELEMENT: str = 'clothingAuthorTag'
    _XMLID_TAG_CLOTHINGSET_ELEMENT: str = 'clothingSet'
    _XMLID_TAG_DESCRIPTION_ELEMENT: str = 'description'
    _XMLID_TAG_DETERMINER_ELEMENT: str = 'determiner'
    _XMLID_TAG_EFFECTS_PARENT: str = 'effects'
    _XMLID_TAG_EFFECTS_VALUEELEM: Optional[str] = None
    _XMLID_TAG_FEMININITY_ELEMENT: str = 'femininity'
    _XMLID_TAG_INCOMPATIBLESLOTS_CHILD: str = 'slot'
    _XMLID_TAG_INCOMPATIBLESLOTS_PARENT: str = 'incompatibleSlots'
    _XMLID_TAG_ISPLURAL_ELEMENT: str = 'namePlural'
    _XMLID_TAG_NAMEPLURAL_ELEMENT: str = 'namePlural'
    _XMLID_TAG_NAMESINGULAR_ELEMENT: str = 'name'
    _XMLID_TAG_PHYSICALRESISTANCE_ELEMENT: str = 'physicalResistance'
    _XMLID_TAG_RARITY_ELEMENT: str = 'rarity'

    def __init__(self) -> None:
        super().__init__()
        self.nameSingular: str = ''  # Element
        self.namePlural: str = ''  # Element
        self.description: str = ''  # Element
        self.determiner: str = ''  # Element
        self.appendColourName: bool = False  # Element
        self.isPlural: bool = False  # Element
        self.baseValue: Optional[int] = None  # Element
        self.physicalResistance: float = 0.  # Element
        self.rarity: ERarity = ERarity(0)  # Element
        self.equipSlots: List[EInventorySlot] = list()
        self.clothingAuthorTag: str = ''  # Element
        self.authorTag: str = ''  # Element
        self.femininity: str = ''  # Element
        self.effects: List[ClothingItemEffect] = list()
        self.blockedPartsList: OrderedDict[str, Set[BlockedParts]] = OrderedDict()
        self.incompatibleSlots: Set[EInventorySlot] = set()
        self.itemTags: OrderedDict[EInventorySlot, Set[str]] = OrderedDict()
        self.displacementDescriptions: OrderedDict[EInventorySlot, OrderedDict[str, OrderedDict[str, str]]] = OrderedDict()
        self.clothingSet: Optional[str] = None  # Element

    def overrideAttrs(self, appendColourName_attribute: _Optional_str = None, isPlural_attribute: _Optional_str = None) -> None:
        if appendColourName_attribute is not None:
            self._XMLID_ATTR_APPENDCOLOURNAME_ATTRIBUTE = appendColourName_attribute
        if isPlural_attribute is not None:
            self._XMLID_ATTR_ISPLURAL_ATTRIBUTE = isPlural_attribute

    def overrideTags(self, appendColourName_element: _Optional_str = None, authorTag_element: _Optional_str = None, baseValue_element: _Optional_str = None, clothingAuthorTag_element: _Optional_str = None, clothingSet_element: _Optional_str = None, description_element: _Optional_str = None, determiner_element: _Optional_str = None, effects_parent: _Optional_str = None, effects_valueelem: _Optional_str = None, femininity_element: _Optional_str = None, incompatibleSlots_child: _Optional_str = None, incompatibleSlots_parent: _Optional_str = None, isPlural_element: _Optional_str = None, namePlural_element: _Optional_str = None, nameSingular_element: _Optional_str = None, physicalResistance_element: _Optional_str = None, rarity_element: _Optional_str = None) -> None:
        if appendColourName_element is not None:
            self._XMLID_TAG_APPENDCOLOURNAME_ELEMENT = appendColourName_element
        if authorTag_element is not None:
            self._XMLID_TAG_AUTHORTAG_ELEMENT = authorTag_element
        if baseValue_element is not None:
            self._XMLID_TAG_BASEVALUE_ELEMENT = baseValue_element
        if clothingAuthorTag_element is not None:
            self._XMLID_TAG_CLOTHINGAUTHORTAG_ELEMENT = clothingAuthorTag_element
        if clothingSet_element is not None:
            self._XMLID_TAG_CLOTHINGSET_ELEMENT = clothingSet_element
        if description_element is not None:
            self._XMLID_TAG_DESCRIPTION_ELEMENT = description_element
        if determiner_element is not None:
            self._XMLID_TAG_DETERMINER_ELEMENT = determiner_element
        if effects_parent is not None:
            self._XMLID_TAG_EFFECTS_PARENT = effects_parent
        if effects_valueelem is not None:
            self._XMLID_TAG_EFFECTS_VALUEELEM = effects_valueelem
        if femininity_element is not None:
            self._XMLID_TAG_FEMININITY_ELEMENT = femininity_element
        if incompatibleSlots_child is not None:
            self._XMLID_TAG_INCOMPATIBLESLOTS_CHILD = incompatibleSlots_child
        if incompatibleSlots_parent is not None:
            self._XMLID_TAG_INCOMPATIBLESLOTS_PARENT = incompatibleSlots_parent
        if isPlural_element is not None:
            self._XMLID_TAG_ISPLURAL_ELEMENT = isPlural_element
        if namePlural_element is not None:
            self._XMLID_TAG_NAMEPLURAL_ELEMENT = namePlural_element
        if nameSingular_element is not None:
            self._XMLID_TAG_NAMESINGULAR_ELEMENT = nameSingular_element
        if physicalResistance_element is not None:
            self._XMLID_TAG_PHYSICALRESISTANCE_ELEMENT = physicalResistance_element
        if rarity_element is not None:
            self._XMLID_TAG_RARITY_ELEMENT = rarity_element

    def _fromXML_equipSlots(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromXML_equipSlots()')

    def _toXML_equipSlots(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toXML_equipSlots()')

    def _fromDict_equipSlots(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromDict_equipSlots()')

    def _toDict_equipSlots(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toDict_equipSlots()')

    def _fromXML_blockedPartsList(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromXML_blockedPartsList()')

    def _toXML_blockedPartsList(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toXML_blockedPartsList()')

    def _fromDict_blockedPartsList(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromDict_blockedPartsList()')

    def _toDict_blockedPartsList(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toDict_blockedPartsList()')

    def _fromXML_itemTags(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromXML_itemTags()')

    def _toXML_itemTags(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toXML_itemTags()')

    def _fromDict_itemTags(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromDict_itemTags()')

    def _toDict_itemTags(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toDict_itemTags()')

    def _fromXML_displacementDescriptions(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromXML_displacementDescriptions()')

    def _toXML_displacementDescriptions(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toXML_displacementDescriptions()')

    def _fromDict_displacementDescriptions(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromDict_displacementDescriptions()')

    def _toDict_displacementDescriptions(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toDict_displacementDescriptions()')

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        lv: Savable
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_NAMESINGULAR_ELEMENT)) is not None:
            self.nameSingular = sf.text
        else:
            raise ElementRequiredException(self, e, 'nameSingular', self._XMLID_TAG_NAMESINGULAR_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_NAMEPLURAL_ELEMENT)) is not None:
            self.namePlural = sf.text
        else:
            raise ElementRequiredException(self, e, 'namePlural', self._XMLID_TAG_NAMEPLURAL_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_DESCRIPTION_ELEMENT)) is not None:
            self.description = sf.text
        else:
            raise ElementRequiredException(self, e, 'description', self._XMLID_TAG_DESCRIPTION_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_DETERMINER_ELEMENT)) is not None:
            self.determiner = sf.text
        else:
            raise ElementRequiredException(self, e, 'determiner', self._XMLID_TAG_DETERMINER_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_APPENDCOLOURNAME_ELEMENT)) is not None:
            self.appendColourName = XML2BOOL[sf.attrib.get(self._XMLID_ATTR_APPENDCOLOURNAME_ATTRIBUTE, 'false')]
        else:
            self.appendColourName = False
        if (sf := e.find(self._XMLID_TAG_ISPLURAL_ELEMENT)) is not None:
            if self._XMLID_ATTR_ISPLURAL_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_ISPLURAL_ATTRIBUTE, 'isPlural')
            self.isPlural = XML2BOOL[sf.attrib[self._XMLID_ATTR_ISPLURAL_ATTRIBUTE]]
        else:
            raise ElementRequiredException(self, e, 'isPlural', self._XMLID_TAG_ISPLURAL_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_BASEVALUE_ELEMENT)) is not None:
            self.baseValue = int(sf.text)
        if (sf := e.find(self._XMLID_TAG_PHYSICALRESISTANCE_ELEMENT)) is not None:
            self.physicalResistance = float(sf.text)
        else:
            raise ElementRequiredException(self, e, 'physicalResistance', self._XMLID_TAG_PHYSICALRESISTANCE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_RARITY_ELEMENT)) is not None:
            self.rarity = ERarity[sf.text]
        else:
            raise ElementRequiredException(self, e, 'rarity', self._XMLID_TAG_RARITY_ELEMENT)
        self._fromXML_equipSlots(e)
        if (sf := e.find(self._XMLID_TAG_CLOTHINGAUTHORTAG_ELEMENT)) is not None:
            self.clothingAuthorTag = sf.text
        else:
            self.clothingAuthorTag = ''
        if (sf := e.find(self._XMLID_TAG_AUTHORTAG_ELEMENT)) is not None:
            self.authorTag = sf.text
        else:
            self.authorTag = ''
        if (sf := e.find(self._XMLID_TAG_FEMININITY_ELEMENT)) is not None:
            self.femininity = sf.text
        else:
            raise ElementRequiredException(self, e, 'femininity', self._XMLID_TAG_FEMININITY_ELEMENT)
        if (lf := e.find(self._XMLID_TAG_EFFECTS_PARENT)) is not None:
            if lf is not None:
                for lc in lf:
                    lv = ClothingItemEffect()
                    lv.fromXML(lc)
                    self.effects.append(lv)
        else:
            self.effects = list()
        self._fromXML_blockedPartsList(e)
        if (lf := e.find(self._XMLID_TAG_INCOMPATIBLESLOTS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_INCOMPATIBLESLOTS_CHILD):
                    self.incompatibleSlots.add(EInventorySlot[lc.text])
        else:
            self.incompatibleSlots = set()
        self._fromXML_itemTags(e)
        self._fromXML_displacementDescriptions(e)
        if (sf := e.find(self._XMLID_TAG_CLOTHINGSET_ELEMENT)) is not None:
            self.clothingSet = sf.text

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        etree.SubElement(e, self._XMLID_TAG_NAMESINGULAR_ELEMENT).text = str(self.nameSingular)
        etree.SubElement(e, self._XMLID_TAG_NAMEPLURAL_ELEMENT).text = str(self.namePlural)
        etree.SubElement(e, self._XMLID_TAG_DESCRIPTION_ELEMENT).text = str(self.description)
        etree.SubElement(e, self._XMLID_TAG_DETERMINER_ELEMENT).text = str(self.determiner)
        if self.appendColourName is not None and self.appendColourName != False:
            etree.SubElement(e, self._XMLID_TAG_APPENDCOLOURNAME_ELEMENT, {self._XMLID_ATTR_APPENDCOLOURNAME_ATTRIBUTE: str(self.appendColourName).lower()})
        etree.SubElement(e, self._XMLID_TAG_ISPLURAL_ELEMENT, {self._XMLID_ATTR_ISPLURAL_ATTRIBUTE: str(self.isPlural).lower()})
        if self.baseValue is not None:
            etree.SubElement(e, self._XMLID_TAG_BASEVALUE_ELEMENT).text = str(self.baseValue)
        etree.SubElement(e, self._XMLID_TAG_PHYSICALRESISTANCE_ELEMENT).text = str(self.physicalResistance)
        etree.SubElement(e, self._XMLID_TAG_RARITY_ELEMENT).text = self.rarity.name
        self._toXML_equipSlots(e)
        if self.clothingAuthorTag is not None and self.clothingAuthorTag != '':
            etree.SubElement(e, self._XMLID_TAG_CLOTHINGAUTHORTAG_ELEMENT).text = str(self.clothingAuthorTag)
        if self.authorTag is not None and self.authorTag != '':
            etree.SubElement(e, self._XMLID_TAG_AUTHORTAG_ELEMENT).text = str(self.authorTag)
        etree.SubElement(e, self._XMLID_TAG_FEMININITY_ELEMENT).text = str(self.femininity)
        lp = etree.SubElement(e, self._XMLID_TAG_EFFECTS_PARENT)
        for lv in self.effects:
            lp.append(lv.toXML())
        self._toXML_blockedPartsList(e)
        lp = etree.SubElement(e, self._XMLID_TAG_INCOMPATIBLESLOTS_PARENT)
        for lv in sorted(self.incompatibleSlots, key=lambda x: x.name):
            etree.SubElement(lp, self._XMLID_TAG_INCOMPATIBLESLOTS_CHILD, {}).text = lv.name
        self._toXML_itemTags(e)
        self._toXML_displacementDescriptions(e)
        if self.clothingSet is not None:
            etree.SubElement(e, self._XMLID_TAG_CLOTHINGSET_ELEMENT).text = str(self.clothingSet)
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['nameSingular'] = str(self.nameSingular)
        data['namePlural'] = str(self.namePlural)
        data['description'] = str(self.description)
        data['determiner'] = str(self.determiner)
        if self.appendColourName is not None:
            data['appendColourName'] = bool(self.appendColourName)
        data['isPlural'] = bool(self.isPlural)
        if self.baseValue is not None:
            data['baseValue'] = int(self.baseValue)
        data['physicalResistance'] = float(self.physicalResistance)
        data['rarity'] = self.rarity.name
        self._toDict_equipSlots(data)
        if self.clothingAuthorTag is not None:
            data['clothingAuthorTag'] = str(self.clothingAuthorTag)
        if self.authorTag is not None:
            data['authorTag'] = str(self.authorTag)
        data['femininity'] = str(self.femininity)
        if self.effects is None or len(self.effects) > 0:
            data['effects'] = list()
        else:
            lc = list()
            if len(self.effects) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['effects'] = lc
        self._toDict_blockedPartsList(data)
        if self.incompatibleSlots is None or len(self.incompatibleSlots) > 0:
            data['incompatibleSlots'] = list()
        else:
            lc = list()
            if len(self.incompatibleSlots) > 0:
                for lv in sorted(lc, lambda x: x.name):
                    lc.append(lv.name)
            data['incompatibleSlots'] = lc
        self._toDict_itemTags(data)
        self._toDict_displacementDescriptions(data)
        if self.clothingSet is not None:
            data['clothingSet'] = str(self.clothingSet)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('nameSingular')) is not None:
            self.nameSingular = data['nameSingular']
        else:
            raise KeyRequiredException(self, data, 'nameSingular', 'nameSingular')
        if (sf := data.get('namePlural')) is not None:
            self.namePlural = data['namePlural']
        else:
            raise KeyRequiredException(self, data, 'namePlural', 'namePlural')
        if (sf := data.get('description')) is not None:
            self.description = data['description']
        else:
            raise KeyRequiredException(self, data, 'description', 'description')
        if (sf := data.get('determiner')) is not None:
            self.determiner = data['determiner']
        else:
            raise KeyRequiredException(self, data, 'determiner', 'determiner')
        if (sf := data.get('appendColourName')) is not None:
            self.appendColourName = data['appendColourName']
        else:
            self.appendColourName = False
        if (sf := data.get('isPlural')) is not None:
            self.isPlural = data['isPlural']
        else:
            raise KeyRequiredException(self, data, 'isPlural', 'isPlural')
        if (sf := data.get('baseValue')) is not None:
            self.baseValue = data['baseValue']
        else:
            self.baseValue = None
        if (sf := data.get('physicalResistance')) is not None:
            self.physicalResistance = data['physicalResistance']
        else:
            raise KeyRequiredException(self, data, 'physicalResistance', 'physicalResistance')
        if (sf := data.get('rarity')) is not None:
            self.rarity = data['rarity'].name
        else:
            raise KeyRequiredException(self, data, 'rarity', 'rarity')
        self._fromDict_equipSlots(data)
        if (sf := data.get('clothingAuthorTag')) is not None:
            self.clothingAuthorTag = data['clothingAuthorTag']
        else:
            self.clothingAuthorTag = ''
        if (sf := data.get('authorTag')) is not None:
            self.authorTag = data['authorTag']
        else:
            self.authorTag = ''
        if (sf := data.get('femininity')) is not None:
            self.femininity = data['femininity']
        else:
            raise KeyRequiredException(self, data, 'femininity', 'femininity')
        if (lv := self.effects) is not None:
            for le in lv:
                self.effects.append(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'effects', 'effects')
        self._fromDict_blockedPartsList(data)
        if (lv := self.incompatibleSlots) is not None:
            for le in lv:
                self.incompatibleSlots.add(le.name)
        else:
            raise KeyRequiredException(self, data, 'incompatibleSlots', 'incompatibleSlots')
        self._fromDict_itemTags(data)
        self._fromDict_displacementDescriptions(data)
        if (sf := data.get('clothingSet')) is not None:
            self.clothingSet = data['clothingSet']
        else:
            self.clothingSet = None

#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL, Savable
__all__ = ['RawClothingName']
## from [LT]/src/com/lilithsthrone/game/inventory/clothing/AbstractClothingType.java: public AbstractClothingType(File clothingXMLFile, String author) throws XMLLoadException { // Be sure to catch this exception correctly - if it's thrown mod is invalid and should not be continued to load @ 0H2efZueZVnr2NDv4NlSBX6L9ADDyDw9IeAbZJd91ki340+clNZmyNj8g7TdOt0grqTVXq8RzpgOnMk9pB+t3w==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawClothingName(Savable):
    TAG = 'name'
    _XMLID_ATTR_APPENDCOLOURNAME_ATTRIBUTE: str = 'appendColourName'

    def __init__(self) -> None:
        super().__init__()
        self.appendColourName: bool = True
        self.text: str = ''  # CDATA

    def overrideAttrs(self, appendColourName_attribute: _Optional_str = None) -> None:
        if appendColourName_attribute is not None:
            self._XMLID_ATTR_APPENDCOLOURNAME_ATTRIBUTE = appendColourName_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_APPENDCOLOURNAME_ATTRIBUTE, None)
        if value is None:
            self.appendColourName = True
        else:
            self.appendColourName = XML2BOOL[value]
        self.text = e.text

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        if self.appendColourName != True:
            e.attrib[self._XMLID_ATTR_APPENDCOLOURNAME_ATTRIBUTE] = str(self.appendColourName).lower()
        e.text = etree.CDATA(str(self.text))
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        if self.appendColourName != True:
            data['appendColourName'] = bool(self.appendColourName)
        data['text'] = self.text
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        self.appendColourName = bool(data['appendColourName']) if 'appendColourName' in data else None
        self.text = data['text']

#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL, Savable
__all__ = ['RawClothingPluralName']
## from [LT]/src/com/lilithsthrone/game/inventory/clothing/AbstractClothingType.java: public AbstractClothingType(File clothingXMLFile, String author) throws XMLLoadException { // Be sure to catch this exception correctly - if it's thrown mod is invalid and should not be continued to load @ 0H2efZueZVnr2NDv4NlSBX6L9ADDyDw9IeAbZJd91ki340+clNZmyNj8g7TdOt0grqTVXq8RzpgOnMk9pB+t3w==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawClothingPluralName(Savable):
    TAG = 'namePlural'
    _XMLID_ATTR_PLURALBYDEFAULT_ATTRIBUTE: str = 'pluralByDefault'

    def __init__(self) -> None:
        super().__init__()
        self.pluralByDefault: bool = False
        self.text: str = ''  # CDATA

    def overrideAttrs(self, pluralByDefault_attribute: _Optional_str = None) -> None:
        if pluralByDefault_attribute is not None:
            self._XMLID_ATTR_PLURALBYDEFAULT_ATTRIBUTE = pluralByDefault_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_PLURALBYDEFAULT_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_PLURALBYDEFAULT_ATTRIBUTE, 'pluralByDefault')
        else:
            self.pluralByDefault = XML2BOOL[value]
        self.text = e.text

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_PLURALBYDEFAULT_ATTRIBUTE] = str(self.pluralByDefault).lower()
        e.text = etree.CDATA(str(self.text))
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['pluralByDefault'] = bool(self.pluralByDefault)
        data['text'] = self.text
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'pluralByDefault' not in data:
            raise KeyRequiredException(self, data, 'pluralByDefault', 'pluralByDefault')
        self.pluralByDefault = bool(data['pluralByDefault'])
        self.text = data['text']

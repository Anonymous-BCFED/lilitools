from frozendict import frozendict
from lilitools.saves.items.item_effect import ItemEffect
from lilitools.saves.modfiles.character.clothing._savables.clothing_item_effect import (
    RawClothingItemEffect,
    _etree__Element,
)
from lxml import etree

__all__ = ["ClothingItemEffect"]

_REPLACE_TYPES: frozendict[str, str] = frozendict(
    {
        "ATTRIBUTE_STRENGTH": "ATTRIBUTE_PHYSIQUE",
        "ATTRIBUTE_FITNESS": "ATTRIBUTE_PHYSIQUE",
        "ATTRIBUTE_INTELLIGENCE": "ATTRIBUTE_ARCANE",
    }
)
_REPLACE_MOD2: frozendict[str, str] = frozendict(
    {
        "TF_MOD_FETISH_SEEDER": "TF_MOD_FETISH_IMPREGNATION",
        "TF_MOD_FETISH_BROODMOTHER": "TF_MOD_FETISH_PREGNANCY",
        "CRITICAL_CHANCE": "CRITICAL_DAMAGE",
        "CLOTHING_ANTI_SELF_TRANSFORMATION": "CLOTHING_SERVITUDE",
        "TF_MOD_ORIFICE_DEEP": "TF_MOD_DEPTH",
        "TF_MOD_ORIFICE_DEEP_2": "TF_MOD_DEPTH_2",
    }
)


class ClothingItemEffect(RawClothingItemEffect):
    def fromXML(self, e: etree._Element) -> None:
        if "itemEffectType" in e.attrib.keys():
            e.attrib["type"] = e.attrib.pop("itemEffectType")
        if e.attrib["type"] in _REPLACE_TYPES.keys():
            e.attrib["type"] = _REPLACE_TYPES[e.attrib["type"]]
        if "primaryModifier" in e.attrib.keys():
            e.attrib["mod1"] = e.attrib.pop("primaryModifier")
        if "secondaryModifier" in e.attrib.keys():
            e.attrib["mod2"] = e.attrib.pop("secondaryModifier")
        if e.attrib["mod2"] in _REPLACE_MOD2.keys():
            e.attrib["mod2"] = _REPLACE_MOD2[e.attrib["mod2"]]

        match e.attrib["mod1"]:
            case "DAMAGE_ATTACK" | "RESISTANCE_ATTACK":
                return
            case "CLOTHING_SEALING":
                e.attrib["mod1"] = "CLOTHING_SPECIAL"
                e.attrib["mod2"] = "CLOTHING_SEALING"
            case "CLOTHING_ENSLAVEMENT":
                e.attrib["mod1"] = "CLOTHING_SPECIAL"
                e.attrib["mod2"] = "CLOTHING_ENSLAVEMENT"

        super().fromXML(e)

    def toItemEffect(self) -> ItemEffect:
        return ItemEffect(
            type=self.type,
            mod1=self.mod1,
            mod2=self.mod2,
            potency=self.potency,
            limit=self.limit,
            timer=self.timer,
        )

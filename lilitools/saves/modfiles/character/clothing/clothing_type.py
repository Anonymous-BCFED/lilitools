from typing import List, Optional, Set

from lxml import etree

from lilitools.saves.items.item_effect import ItemEffect
from lilitools.saves.modfiles.character.clothing._savables.clothing_item_effect import (
    RawClothingItemEffect,
)
from lilitools.saves.modfiles.character.clothing._savables.clothing_type import (
    RawClothingType,
)

# o\['([^']+)'\] *= *'([^']+)'
# $1: Optional[$2] = None

__all__ = ["ClothingType"]


class ClothingType(RawClothingType):
    def __init__(
        self,
        id: Optional[str] = None,
        baseValue: Optional[int] = None,
        determiner: Optional[str] = None,
        nameSingular: Optional[str] = None,
        namePlural: Optional[str] = None,
        isPlural: Optional[bool] = None,
        pathName: Optional[str] = None,
        description: Optional[str] = None,
        generatedColourPossibilities: Optional[List[Set[str]]] = None,
        allColourPossibilities: Optional[List[Set[str]]] = None,
        effects: Optional[List[ItemEffect]] = None,
    ) -> None:
        super().__init__()
        self.id: str = id or ""
        self.coreAttributes.baseValue = baseValue or 0
        self.coreAttributes.determiner = determiner or "a"
        self.coreAttributes.nameSingular = nameSingular or ""
        self.coreAttributes.namePlural = namePlural or ""
        self.coreAttributes.isPlural = isPlural if isPlural is not None else False
        self.pathName: Optional[str] = pathName
        self.coreAttributes.description = description or ""
        self.generatedColourPossibilities: List[Set[str]] = (
            generatedColourPossibilities or []
        )
        self.allColourPossibilities: List[Set[str]] = allColourPossibilities or []
        self.extraEffects: List[ItemEffect] = effects or []

    def loadColours(self) -> None:
        if self.allColourPossibilities is None or self.allColourPossibilities is None:
            self.allColourPossibilities = []
            self.generatedColourPossibilities = []
            for colourInfo in self.coreAttributes.colourReplacements:
                if colourInfo is None:
                    continue
                self.allColourPossibilities.append(colourInfo.allColours)
                self.generatedColourPossibilities.append(colourInfo.defaultColours)

    def getPhysicalResistance(self) -> float:
        return self.coreAttributes.physicalResistance

    def getGeneratedColourPossibilities(self) -> List[Set[str]]:
        return self.generatedColourPossibilities

    def getAllColourPossibilities(self) -> List[Set[str]]:
        return self.allColourPossibilities

    def getGeneratedOrAllColourPossibilities(self) -> List[Set[str]]:
        return self.generatedColourPossibilities if len(self.generatedColourPossibilities)>0 else self.allColourPossibilities

    def fromXML(self, e: etree._Element) -> None:
        self.allColourPossibilities = None
        self.generatedColourPossibilities = None
        if (ca:=e.find('coreAtributes')) is not None:
            ca.tag = 'coreAttributes'
        super().fromXML(e)
        self.loadColours()

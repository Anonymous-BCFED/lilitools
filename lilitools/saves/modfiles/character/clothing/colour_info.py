from typing import List, Optional, Set
from lilitools.saves.colour_preset_lists import DEBUG_ALL, getColoursByPresetId
from lilitools.saves.exceptions import AttributeRequiredException

from lilitools.saves.savable import XML2BOOL, Savable
from lxml import etree

COLOUR_REPLACEMENTS: List[List[str]] = [
    ["#ff2a2a", "#ff5555", "#ff8080", "#ffaaaa", "#ffd5d5"],
    ["#ff7f2a", "#ff9955", "#ffb380", "#ffccaa", "#ffe6d5"],
    ["#ffd42a", "#ffdd55", "#ffe680", "#ffeeaa", "#fff6d5"],
    ["#abc837", "#bcd35f", "#cdde87", "#dde9af", "#eef4d7"],
]


class ColourInfo:
    def __init__(self) -> None:
        self.preset: Optional[str] = None
        self.defined: bool = False
        self.recolouringAllowed: bool = False
        self.copyColourIndex: Optional[int] = None
        self.defaultColours: List[str] = []
        self.extraColours: List[str] = []
        self._allColours: Optional[List[str]] = None

    def _precalculate(self) -> None:
        if self._allColours is None:
            # Looks overcomplicated, but it's needed to keep ordering correct
            self._allColours = list(
                DEBUG_ALL
                - (DEBUG_ALL - (set(self.defaultColours) | set(self.extraColours)))
            )

            # Fix bug:
            if len(self._allColours):
                if self._allColours[0] == "CLOTHING_GREEN_LIME":
                    self._allColours = self._allColours[1:]

    @property
    def allColours(self) -> List[str]:
        self._precalculate()
        return self._allColours

    def _readColoursFromElement(self, e: etree._Element) -> List[str]:
        ## from [LT]/src/com/lilithsthrone/game/inventory/clothing/AbstractClothingType.java: Function< Element, List<Colour> > getColoursFromElement = (colorsElement) -> { //Helper function to get the colors depending on if it's a specified group or a list of individual colors @ syLnvyFDmXg9SAm7oRd/tFhxELFrA1P/v7frTklBOAo4PWOqKOmdxjkts6qsFFCIAiLh5akVZK8stUt79oT1Fg==
        self.defined = True
        result: List[str] = []
        if "values" not in e.attrib:
            for colour in e.findall("colour"):
                if colour.text is None:
                    result.append(colour.text)
        else:
            return list(getColoursByPresetId(e.attrib["values"]))
        return result

    def fromCustomColourXML(self, e: etree._Element) -> None:
        self.defined = True
        self.replacementStrings = []
        # Inno does a dumb and has this loop infinitely. No.
        for i in range(100):
            ci = f"c{i}"
            if ci in e.attrib:
                self.replacementStrings.append(e.attrib[ci])

        self.recolouringAllowed = XML2BOOL[e.attrib.get("recolouringAllowed", "true")]
        if "copyColourIndex" in e.attrib:
            self.copyColourIndex = int(e.attrib["copyColourIndex"])

        """
        List<Colour> defaultColours = getColoursFromElement
            .apply(e.getMandatoryFirstOf("defaultColours"));
        """
        if (eDefaultColours := e.find("defaultColours")) is not None:
            self.defaultColours = self._readColoursFromElement(eDefaultColours)
        else:
            raise AttributeRequiredException(
                self, e, "defaultColours", "defaultColours"
            )

        """
        List<Colour> extraColours = getColoursFromElement
            .apply(e.getMandatoryFirstOf("extraColours"));
        """
        if (eExtraColours := e.find("extraColours")) is not None:
            self.extraColours = self._readColoursFromElement(eExtraColours)
        else:
            raise AttributeRequiredException(self, e, "extraColours", "extraColours")

    def fromLegacyXML(
        self, e: etree._Element, baseName: str, colourrepl: List[str]
    ) -> None:
        """
        boolean primaryRecolourAllowed = true;
        if(coreAttributes.getOptionalFirstOf("primaryColours").isPresent() && !coreAttributes.getMandatoryFirstOf("primaryColours").getAttribute("recolouringAllowed").isEmpty()) {
            primaryRecolourAllowed = Boolean.valueOf(coreAttributes.getMandatoryFirstOf("primaryColours").getAttribute("recolouringAllowed"));
        }
        if(coreAttributes.getOptionalFirstOf("primaryColours").isPresent() && !coreAttributes.getMandatoryFirstOf("primaryColours").getAttribute("copyColourIndex").isEmpty()) {
            copyGenerationColours.put(0, Integer.valueOf(coreAttributes.getMandatoryFirstOf("primaryColours").getAttribute("copyColourIndex")));
        }
        List<Colour> importedPrimaryColours = coreAttributes.getOptionalFirstOf("primaryColours")
            .map(getColoursFromElement::apply)
            .orElseGet(ArrayList::new);
        List<Colour> importedPrimaryColoursDye = coreAttributes.getOptionalFirstOf("primaryColoursDye")
            .map(getColoursFromElement::apply)
            .orElseGet(ArrayList::new);
        """
        self.replacementStrings: List[str] = colourrepl
        child: etree._Element
        defaultColoursElement: Optional[etree._Element] = e.find(f"{baseName}Colours")
        extraColoursElement: Optional[etree._Element] = e.find(f"{baseName}ColoursDye")
        self._xmlMainColours = []
        self._xmlDyeColours = []
        if defaultColoursElement is not None:
            for child in defaultColoursElement:
                self._xmlMainColours.append(child.text or "")
            if extraColoursElement is not None:
                for child in extraColoursElement:
                    self._xmlDyeColours.append(child.text or "")
            self.recolouringAllowed = XML2BOOL[
                defaultColoursElement.attrib.get("recolouringAllowed", "true")
            ]
            if "copyColourIndex" in defaultColoursElement.attrib:
                self.copyColourIndex = int(
                    defaultColoursElement.attrib["copyColourIndex"]
                )
            self.defaultColours = self._readColoursFromElement(defaultColoursElement)
            if extraColoursElement is None:
                self.extraColours = self._readColoursFromElement(extraColoursElement)

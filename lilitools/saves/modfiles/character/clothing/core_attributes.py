from collections import OrderedDict
from typing import Any, Dict, FrozenSet, List, Set, Tuple

from lxml import etree

from lilitools.consts import PRESERVE_SAVE_ORDERING
from lilitools.saves.inventory.blocked_parts import BlockedParts
from lilitools.saves.inventoryslot import EInventorySlot
from lilitools.saves.items.item_effect import ItemEffect
from lilitools.saves.modfiles.character.clothing._savables.core_attributes import (
    RawClothingCoreAttributes,
)
from lilitools.saves.modfiles.character.clothing.colour_info import (
    COLOUR_REPLACEMENTS,
    ColourInfo,
)

DISPLACEMENT_TAG_ELEMENTS: FrozenSet[str] = frozenset(
    {
        "replacementText",
        "displacementText",
    }
)
DISPLACEMENT_TAG_SUBELEMENTS: FrozenSet[str] = frozenset(
    {
        "playerNPC",
        "playerNPCRough",
        "playerSelf",
        "NPCPlayer",
        "NPCPlayerRough",
        "NPCSelf",
        "NPCOtherNPC",
        "NPCOtherNPCRough",
        "self",
        "other",
        "otherRough",
    }
)


class ClothingCoreAttributes(RawClothingCoreAttributes):
    def __init__(self) -> None:
        super().__init__()
        self.colourReplacements: List[ColourInfo] = []

    def _fromXML_colourInfo(
        self,
        e: etree._Element,
        basename: str,
        replacements: List[str],
    ) -> None:
        if (eColour := e.find(f"{basename}Colours")) is not None:
            # print(str(eColour))
            ci = ColourInfo()
            ci.fromLegacyXML(e, basename, replacements)
            self.colourReplacements.append(ci)

    def fromXML(self, e: etree._Element) -> None:

        eCustomColours: etree._Element
        eCustomColour: etree._Element

        super().fromXML(e)

        # print(repr(e.getchildren()))
        self._fromXML_colourInfo(e, "primary", COLOUR_REPLACEMENTS[0])
        self._fromXML_colourInfo(e, "secondary", COLOUR_REPLACEMENTS[1])
        self._fromXML_colourInfo(e, "tertiary", COLOUR_REPLACEMENTS[2])

        if (eCustomColours := e.find("customColours")) is not None:
            for eCustomColour in eCustomColours.findall("customColour"):
                ci = ColourInfo()
                ci.fromCustomColourXML(eCustomColour)
                self.colourReplacements.append(ci)

    def _fromXML_blockedPartsList(self, e: etree._Element) -> None:
        for bple in e.findall("blockedPartsList"):
            slot = bple.attrib.get("slot",self.equipSlots[0])
            bpl: Set[BlockedParts] = set()
            for bpe in bple.findall("blockedParts"):
                bp = BlockedParts()
                bp.fromXML(bpe)
                bpl.add(bp)
            self.blockedPartsList[slot] = bpl

    def _toXML_blockedPartsList(self, e: etree._Element) -> None:
        iterlist = list(self.blockedPartsList.items())
        if not PRESERVE_SAVE_ORDERING:
            iterlist.sort(key=lambda t: t[0])
        for slot, bpl in iterlist:
            bple = etree.SubElement(e, "blockedPartsList", {"slot": slot})
            for bp in (
                bpl
                if PRESERVE_SAVE_ORDERING
                else sorted(bpl, key=lambda x: x.displacementType.name)
            ):
                bple.append(bp.toXML("blockedParts"))

    def _fromDict_blockedPartsList(self, data: Dict[str, Any]) -> None:
        self.blockedPartsList = {}
        if (bpl := data["blockedPartsList"]) is not None:
            for slot, partlist in bpl.items():
                parts: Set[BlockedParts] = set()
                for partdata in partlist:
                    bp = BlockedParts()
                    bp.fromDict(partdata)
                    parts.add(bp)
                self.blockedPartsList[slot] = parts

    def _toDict_blockedPartsList(self, data: Dict[str, Any]) -> None:
        o = {}
        for slot, partset in self.blockedPartsList.items():
            o[slot] = [
                p.toDict()
                for p in sorted(partset, key=lambda x: x.displacementType.name)
            ]
        data["blockedPartsList"] = o

    def _fromXML_itemTags(self, e: etree._Element) -> None:
        slot: EInventorySlot
        te: etree._Element
        for itemtags in e.findall("itemTags"):
            if "slot" not in itemtags.attrib.keys():
                for slot in self.equipSlots:
                    if slot not in self.itemTags:
                        self.itemTags[slot] = set()
                    for te in itemtags.findall("tag"):
                        self.itemTags[slot].add(te.text)
                for te in itemtags.findall("tag"):
                    match te.text:
                        case "DILDO_TINY":
                            self.penetrationOtherLength = 8
                        case "DILDO_AVERAGE":
                            self.penetrationOtherLength = 15
                        case "DILDO_LARGE":
                            self.penetrationOtherLength = 25
                        case "DILDO_HUGE":
                            self.penetrationOtherLength = 35
                        case "DILDO_ENORMOUS":
                            self.penetrationOtherLength = 45
                        case "DILDO_GIGANTIC":
                            self.penetrationOtherLength = 55
                        case "DILDO_STALLION":
                            self.penetrationOtherLength = 81
                        case _:
                            continue
                    self.penetrationSelfGirth = 2
                    for slot in self.equipSlots:
                        self.itemTags[slot].add("DILDO_OTHER")
            else:
                self.itemTags[EInventorySlot[itemtags.attrib["slot"]]] = set(
                    [e.text for e in itemtags.findall("tag")]
                )

    def _toXML_itemTags(self, e: etree._Element) -> None:
        for slot, tags in sorted(self.itemTags.items(), key=lambda x: x[0].name):
            if len(tags) == 0:
                continue
            itemTags = etree.SubElement(e, "itemTags", {"slot": slot.name})
            for tag in sorted(tags):
                etree.SubElement(itemTags, "tag", {}).text = tag

    def _fromDict_itemTags(self, data: Dict[str, Any]) -> None:
        self.itemTags = {}
        for slot, tags in data["itemTags"].items():
            self.itemTags[slot] = set(tags)

    def _toDict_itemTags(self, data: Dict[str, Any]) -> None:
        o: Dict[str, List[str]] = {}
        for slot, tags in sorted(self.itemTags.items(), key=lambda x: x[0].name):
            if len(tags) == 0:
                continue
            o[slot] = sorted(tags)
        data["itemTags"] = o

    def __fromXML_displacementTypeTag(self, e: etree._Element, dtteName: str) -> None:
        dle: etree._Element
        for dle in e.findall(dtteName):
            type = dle.attrib["type"]
            slot = EInventorySlot[
                dle.attrib.get("slot", next(iter(self.equipSlots)).name)
            ]
            if slot not in self.displacementDescriptions.keys():
                self.displacementDescriptions[slot] = OrderedDict({type: OrderedDict()})
            if type not in self.displacementDescriptions[slot].keys():
                self.displacementDescriptions[slot][type] = OrderedDict()
            umap: Dict[str, str] = self.displacementDescriptions[slot][type]
            for tagName in DISPLACEMENT_TAG_SUBELEMENTS:
                foundData: Dict[Tuple[str, str], str] = OrderedDict()
                for dts in dle.findall(tagName):
                    foundData[(dtteName, dts.tag)] = dts.text
                if len(foundData) > 0:
                    umap.update(foundData)

    def _fromXML_displacementDescriptions(self, e: etree._Element) -> None:
        for dtteName in DISPLACEMENT_TAG_ELEMENTS:
            self.__fromXML_displacementTypeTag(e, dtteName)

    def _fromDict_displacementDescriptions(self, data: Dict[str, Any]) -> None:
        self.displacementDescriptions = dict()
        for slotid, typesdata in data["displacementDescriptions"].items():
            slot = EInventorySlot[slotid]
            types = {}
            for typeid, tags in typesdata.items():
                tdict = {}
                for tagKey, taglist in tags:
                    tdict[tagKey] = [str(x) for x in taglist]
                types[typeid] = tdict
            self.displacementDescriptions[slot] = types

    def _toDict_displacementDescriptions(self, data: Dict[str, Any]) -> None:
        o = {}
        for slot, slotdata in self.displacementDescriptions.items():
            slots = {}
            for typeid, typedata in slotdata.items():
                types = {}
                for umapKey, umapValues in typedata.items():
                    types[umapKey] = [str(v) for v in umapValues]
                slots[typeid] = types
            o[slot.name] = slots
        data["displacementDescriptions"] = o

    def _fromXML_equipSlots(self, e: etree._Element) -> None:
        self.equipSlots = []
        if (slots := e.find("equipSlots")) is not None:
            for slot in slots.findall("slot"):
                self.equipSlots.append(EInventorySlot[slot.text])
        elif (slot := e.find("slot")) is not None:
            self.equipSlots.append(EInventorySlot[slot.text])

    def _toXML_equipSlots(self, e: etree._Element) -> None:
        raise NotImplementedError()

    def _fromDict_equipSlots(self, data: Dict[str, Any]) -> None:
        self.equipSlots = [EInventorySlot[x] for x in data["equipSlots"]]

    def _toDict_equipSlots(self, data: Dict[str, Any]) -> None:
        data["equipSlots"] = [x.name for x in self.equipSlots]

    def getItemEffects(self) -> List[ItemEffect]:
        return [x.toItemEffect() for x in self.effects]

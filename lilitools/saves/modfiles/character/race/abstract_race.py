from __future__ import annotations

from typing import TYPE_CHECKING, Dict, Set

from lxml import etree

from lilitools.saves.character.body.enums.leg_configuration import ELegConfiguration
from lilitools.saves.enums.furry_preference import EFurryPreference
from lilitools.saves.modfiles.character.race.affinity import EAffinity
from lilitools.saves.modfiles.character.race.disposition import EDisposition
from lilitools.saves.modfiles.character.race.leg_configuration_affinity import LegConfigurationAffinity
from lilitools.saves.modfiles.character.race.racial_class import ERacialClass
from lilitools.saves.savable import XML2BOOL, Savable

if TYPE_CHECKING:
    from lilitools.saves.character.body.body import Body
    from lilitools.saves.modfiles.character.race.abstract_subspecies import AbstractSubspecies


class AbstractRace(Savable):
    ID: str
    def __init__(self) -> None:
        self.isFromMod: bool = False
        self.isFromExternalFile: bool = False

        self.racialBodyId: str = ""
        self.raceChangeString: str = ""

        self.name: str = ""
        self.namePlural: str = ""
        self.nameSillyMode: str = ""
        self.namePluralSillyMode: str = ""
        self.nameFeralSingular: Dict[LegConfigurationAffinity, str] = {}
        self.nameFeralPlural: Dict[LegConfigurationAffinity, str] = {}
        self.defaultTransformName: str = ""

        self.colour: str = ""

        self.disposition: EDisposition = EDisposition.NEUTRAL
        self.racialClass: ERacialClass = ERacialClass.MAMMAL

        self.preferredCombatBehaviour: str = ""
        self.numberOfOffspringLow: int = 0
        self.numberOfOffspringHigh: int = 0
        self.chanceForMaleOffspring: float = 0.0
        self.defaultFemininePreference: EFurryPreference = EFurryPreference.NORMAL
        self.defaultMasculinePreference: EFurryPreference = EFurryPreference.NORMAL
        self.affectedByFurryPreference: bool = False
        self.racialFetishModifiers: Dict[str, Dict[str, int]] = {}

        self.subspecies: Set[AbstractSubspecies] = set()

        self.hasFeralPartsAvailable: bool = True
        self.ableToSelfTransform: bool = False
        self.flyingRace: bool = False

    def fromXML(self, e: etree._Element) -> None:
        self.isFromExternalFile = True
        self.racialBodyId = e.find("racialBody").text
        self.raceChangeString = e.find("raceChangeString").text
        self.name = e.find("name").text
        self.namePlural = e.find("namePlural").text
        self.defaultTransformName = e.find("defaultTransformName").text
        self.colour = e.find("colour").text
        self.disposition = EDisposition[e.find("disposition").text.upper()]
        self.racialClass = ERacialClass[e.find("racialClass").text.upper()]
        self.chanceForMaleOffspring = float(e.find("chanceForMaleOffspring").text)
        self.numberOfOffspringHigh = int(e.find("numberOfOffspringHigh").text)
        self.numberOfOffspringLow = int(e.find("numberOfOffspringLow").text)
        self.affectedByFurryPreference = XML2BOOL[
            e.find("affectedByFurryPreference").text
        ]
        self.hasFeralPartsAvailable = XML2BOOL[e.find("feralPartsAvailable").text]
        self.ableToSelfTransform = XML2BOOL[e.find("ableToSelfTransform").text]
        self.flyingRace = XML2BOOL[e.find("flyingRace").text]

        self.preferredCombatBehaviour = "BALANCED"
        if (se := e.find("combatBehaviour")) is not None:
            self.preferredCombatBehaviour = se.text
        self.nameSillyMode = self.name
        if (se := e.find("nameSillyMode")) is not None:
            self.nameSillyMode = se.text
        self.namePluralSillyMode = self.namePlural
        if (se := e.find("namePluralSillyMode")) is not None:
            self.namePluralSillyMode = se.text

        self.nameFeralSingular = self._loadXML_feralNameList(e, "nameFeral")
        self.nameFeralPlural = self._loadXML_feralNameList(e, "nameFeralPlural")

        self.racialFetishModifiers={}
        if (se:=e.find('racialFetishModifiers')) is not None:
            for sse in se.findall('fetish'):
                weights:Dict[str,int]={}
                for wk in ('love','like','dislike','hate'):
                    if wk in sse.attrib.keys():
                        weights[wk]=int(sse[wk])
                self.racialFetishModifiers[sse.text]=weights

    def _loadXML_feralNameList(
        self, e: etree._Element, tag: str
    ) -> Dict[LegConfigurationAffinity, str]:
        o: Dict[LegConfigurationAffinity, str] = {}
        firstFeralName: str = ""
        if (se := e.find(tag)) is not None:
            for sse in se.findall("name"):
                name = sse.text
                if firstFeralName == "":
                    firstFeralName = name
                if not sse.attrib.has_key("affinity"):
                    for affinity in EAffinity:
                        ca = LegConfigurationAffinity(
                            ELegConfiguration[sse["legConfiguration"]], affinity
                        )
                        o[ca] = name
                else:
                    ca = LegConfigurationAffinity(
                        ELegConfiguration[sse["legConfiguration"]],
                        EAffinity[sse["affinity"]],
                    )
                    o[ca] = name

        for affinity in EAffinity:
            ca = LegConfigurationAffinity(
                ELegConfiguration.BIPEDAL,
                EAffinity[sse["affinity"]],
            )
            if ca not in o.keys():
                o[ca] = name
        return o

    # def getRacialBody(self)->AbstractRacialBody:
    def getName(self, body: Body, feral: bool) -> str:
        if feral:
            return self.getFeralName(
                LegConfigurationAffinity(
                    body.legs.configuration,
                    (
                        body.getHalfDemonSubspecies().getAffinity(body)
                        if body.getHalfDemonSubspecies() is not None
                        else body.getSubspecies().getAffinity(body)
                    ),
                )
            )
        return self.name

    def getFeralName(self, lca: LegConfigurationAffinity, plural: bool) -> str:
        collection: Dict[LegConfigurationAffinity, str]
        if plural:
            collection = self.nameFeralPlural
        else:
            collection = self.nameFeralSingular
        return collection.get(
            lca,
            LegConfigurationAffinity(ELegConfiguration.BIPEDAL, EAffinity.AMPHIBIOUS),
        )

from __future__ import annotations

from typing import TYPE_CHECKING, Dict, List, Set

from lilitools.saves.character.body.enums.leg_configuration import ELegConfiguration
from lilitools.saves.character.enums.perk_category import EPerkCategory
from lilitools.saves.character.enums.personality_trait import EPersonalityTrait
from lilitools.saves.modfiles.character.race.affinity import EAffinity
from lilitools.saves.modfiles.character.race.leg_configuration_affinity import LegConfigurationAffinity
from lilitools.saves.modfiles.character.race.nocturnality import ENocturnality
from lilitools.saves.modfiles.character.race.races import Race
from lilitools.saves.modfiles.character.race.subspecies_flags import ESubspeciesFlags

if TYPE_CHECKING:
    from lilitools.saves.character.body.body import Body
    from lilitools.saves.modfiles.character.race.abstract_race import AbstractRace


class AbstractSubspecies:
    def __init__(self) -> None:
        self.isFromMod: bool = False
        self.isFromXML: bool = False

        self.isMainSubspecies: bool = False

        self.baseSlaveValue: int = 0
        self.subspeciesOverridePriority: int = 0
        
        self.hasShortStature: bool=False
        self.isBipedalSubspecies: bool=False
        self.isWinged: bool=False
        self.doesNotAge: bool=False
        
        self.personalityTraitOverrides: Dict[EPersonalityTrait, float]={}

        self.applySubspeciesChanges:str=''
        self.subspeciesWeighting:str=''

        self.attributeItemId:str=''
        self.transformativeItemId:str=''

        self.anthroNames:Dict[ELegConfiguration,Set[str]]={}
        self.anthroNamesSillyMode:Dict[ELegConfiguration,Set[str]]={}
        self.halfDemonNames:Dict[ELegConfiguration,Set[str]]={}

        # self.feralAttributes:FeralAttributes=FeralAttributes()

        self.nocturnality:ENocturnality=ENocturnality()

        self.statusEffectDescription:str=''
        self.perkWeightingFeminine:Dict[EPerkCategory,int]={}
        self.perkWeightingMasculine:Dict[EPerkCategory,int]={}
        self.statusEffectAttributeModifiers:Dict[str,int]={}
        self.extraEffects:List[str]=[]

        self.race:'AbstractRace' = None
        self.affinity:EAffinity=EAffinity.AMPHIBIOUS
        # self.subspeciesPreferenceDefault:ESubspeciesPreference=
        self.description:str=''

        self.subspeciesFlags: ESubspeciesFlags = ESubspeciesFlags.NONE

    def getFeralName(self, body:'Body')->str:
        if body is not None:
            r = body.legs.getRace()
            lc = body.legs.configuration
            match lc:
                case ELegConfiguration.BIPEDAL:
                    return 'demon'
                case _:
                    if r in ('HUMAN', 'DEMON'):
                        return Race.DEMON.getFeralName(LegConfigurationAffinity(lc,self.getAffinity()),False)
                    else:
                        return 'demonic-'+Race.GetByID(r).getName(body,True)
        return 'demon'

    def getAffinity(self, body: 'Body') -> EAffinity:
        return EAffinity.AMPHIBIOUS

    def getWeighting(self, body: 'Body', race: 'AbstractRace') -> float:
        return 0.0

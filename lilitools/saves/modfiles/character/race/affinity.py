from enum import IntEnum


class EAffinity(IntEnum):
    AQUATIC = 0
    AMPHIBIOUS = 1
    TERRESTRIAL = 2

from enum import IntEnum


class EDisposition(IntEnum):
    CIVILIZED = 0
    NEUTRAL = 1
    UNPREDICTABLE = 2
    SAVAGE = 3

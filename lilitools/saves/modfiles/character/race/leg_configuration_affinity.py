from typing import Dict, NamedTuple, Type

from lilitools.saves.character.body.enums.leg_configuration import ELegConfiguration
from lilitools.saves.modfiles.character.race.affinity import EAffinity


class LegConfigurationAffinity(NamedTuple):
    legConfiguration: ELegConfiguration
    affinity: EAffinity

    @classmethod
    def getFeralNamesMap(cls:Type['LegConfigurationAffinity'], legConf:ELegConfiguration,feralName:str)->Dict['LegConfigurationAffinity',str]:
        return {cls(legConf,a):feralName for a in EAffinity}
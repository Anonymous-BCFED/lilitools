from enum import IntEnum


class ENocturnality(IntEnum):
    DIURNAL = 0
    NOCTURNAL = 1
    CREPUSCULAR = 2
    MATUTINAL = 3
    VESPERTINE = 4
    CATHEMERAL = 5

from typing import TYPE_CHECKING, Dict, List, Optional

from lilitools.saves.modfiles.character.race.races.angel import AngelRace
from lilitools.saves.modfiles.character.race.races.demon import DemonRace
from lilitools.saves.modfiles.character.race.races.human import HumanRace
from lilitools.saves.modfiles.character.race.abstract_race import AbstractRace
__all__ = ["Race", "ALL_RACES"]


class Race:
    HUMAN = HumanRace()
    ANGEL = AngelRace()
    DEMON = DemonRace()

    @classmethod
    def GetByID(cls, id: str) -> Optional["AbstractRace"]:
        return ALL_RACES_BY_ID.get(id)


ALL_RACES: List["AbstractRace"] = [Race.HUMAN, Race.ANGEL, Race.DEMON]
ALL_RACES_BY_ID: Dict[str, "AbstractRace"] = {r.ID: r for r in ALL_RACES}


from lilitools.saves.enums.furry_preference import EFurryPreference
from lilitools.saves.modfiles.character.race.abstract_race import AbstractRace
from lilitools.saves.modfiles.character.race.disposition import EDisposition
from lilitools.saves.modfiles.character.race.racial_class import ERacialClass


## from [LT]/src/com/lilithsthrone/game/character/race/Race.java: public static AbstractRace ANGEL = new AbstractRace("angel", @ ta8wpVU0P6kjJRjZjuakM4NRFqDsYGB4g0Zw2hWgl195er//NJ8jA8RyvZ0CFVn8RkdB1wYFWD5Hv6ehz/+AAQ==
class AngelRace(AbstractRace):
    ID='ANGEL'
    def __init__(self) -> None:
        super().__init__()

        self.name = "angel"
        self.namePlural = "angels"
        self.nameFeralSingular = {}
        self.nameFeralPlural = {}
        self.defaultTransformName = "angelic"
        self.colour = "CLOTHING_WHITE"
        self.disposition = EDisposition.CIVILIZED
        self.racialClass = ERacialClass.MAMMAL
        self.preferredCombatBehaviour = "SPELLS"
        self.chanceForMaleOffspring = 0.25
        self.numberOfOffspringHigh = 1
        self.numberOfOffspringLow = 1
        self.defaultFemininePreference = EFurryPreference.NORMAL
        self.defaultMasculinePreference = EFurryPreference.NORMAL
        self.affectedByFurryPreference = False

        self.subspecies = set()

        self.ableToSelfTransform=True
        self.hasFeralPartsAvailable = False

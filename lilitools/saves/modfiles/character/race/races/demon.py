from typing import TYPE_CHECKING, Dict

from lilitools.saves.character.body.enums.leg_configuration import ELegConfiguration
from lilitools.saves.enums.furry_preference import EFurryPreference
from lilitools.saves.modfiles.character.race.abstract_race import AbstractRace
from lilitools.saves.modfiles.character.race.affinity import EAffinity
from lilitools.saves.modfiles.character.race.disposition import EDisposition
from lilitools.saves.modfiles.character.race.leg_configuration_affinity import LegConfigurationAffinity
from lilitools.saves.modfiles.character.race.racial_class import ERacialClass

if TYPE_CHECKING:
    from lilitools.saves.character.body.body import Body


## from [LT]/src/com/lilithsthrone/game/character/race/Race.java: public static AbstractRace DEMON = new AbstractRace("demon", @ 2F4gRLvEWowOuKiqrFONJMemFgDG7/U+unM+MJfEpO5krcJKxR8WicrAXaF0G8BXVs/MM4qemIxhpj9LBr3dkw==
class DemonRace(AbstractRace):
    ID = "DEMON"
    def __init__(self) -> None:
        super().__init__()

        self.name = "demon"
        self.namePlural = "demons"
        self.nameFeralSingular = {}
        # fmt:off
        [self.nameFeralSingular.update(x) for x in [
            LegConfigurationAffinity.getFeralNamesMap(ELegConfiguration.BIPEDAL, 'demon'),
            LegConfigurationAffinity.getFeralNamesMap(ELegConfiguration.ARACHNID, 'demonic-spider'),
            LegConfigurationAffinity.getFeralNamesMap(ELegConfiguration.CEPHALOPOD, 'demonic-octopus'),
            LegConfigurationAffinity.getFeralNamesMap(ELegConfiguration.QUADRUPEDAL, 'demonic-horse'),
            LegConfigurationAffinity.getFeralNamesMap(ELegConfiguration.AVIAN, 'demonic-eagle'),
            LegConfigurationAffinity.getFeralNamesMap(ELegConfiguration.TAIL, 'demonic-fish'),
            LegConfigurationAffinity.getFeralNamesMap(ELegConfiguration.TAIL_LONG, 'demonic-snake'),
        ]]
        # fmt:on
        self.nameFeralSingular[
            LegConfigurationAffinity(ELegConfiguration.TAIL_LONG, EAffinity.AQUATIC)
        ] = "demonic-sea-serpent"
        self.nameFeralPlural = {}
        # fmt:off
        [self.nameFeralPlural.update(x) for x in [
            LegConfigurationAffinity.getFeralNamesMap(ELegConfiguration.BIPEDAL, 'demons'),
            LegConfigurationAffinity.getFeralNamesMap(ELegConfiguration.ARACHNID, 'demonic-spiders'),
            LegConfigurationAffinity.getFeralNamesMap(ELegConfiguration.CEPHALOPOD, 'demonic-octopuses'),
            LegConfigurationAffinity.getFeralNamesMap(ELegConfiguration.QUADRUPEDAL, 'demonic-horses'),
            LegConfigurationAffinity.getFeralNamesMap(ELegConfiguration.AVIAN, 'demonic-eagles'),
            LegConfigurationAffinity.getFeralNamesMap(ELegConfiguration.TAIL, 'demonic-fish'),
            LegConfigurationAffinity.getFeralNamesMap(ELegConfiguration.TAIL_LONG, 'demonic-snakes'),
        ]]
        # fmt:on
        self.nameFeralPlural[
            LegConfigurationAffinity(ELegConfiguration.TAIL_LONG, EAffinity.AQUATIC)
        ] = "demonic-sea-serpents"
        self.defaultTransformName = "demonic"
        self.colour = "RACE_HUMAN"
        self.disposition = EDisposition.CIVILIZED
        self.racialClass = ERacialClass.MAMMAL
        self.preferredCombatBehaviour = "BALANCED"
        self.chanceForMaleOffspring = 0.5
        self.numberOfOffspringHigh = 1
        self.numberOfOffspringLow = 1
        self.defaultFemininePreference = EFurryPreference.NORMAL
        self.defaultMasculinePreference = EFurryPreference.NORMAL
        self.affectedByFurryPreference = False

        self.subspecies = set()

        self.hasFeralPartsAvailable = False

    def getName(self, body: 'Body', feral: bool) -> str:
        if (
            feral
            and body is not None
            and body.getHalfDemonSubspecies() is not None
            and body.getHalfDemonSubspecies() != Subspecies.HUMAN
        ):
            return f"demonic-{body.getHalfDemonSubspecies().getFeralName(body)}"
        return super().getName(body, feral)

    def getNamePlural(self, body: 'Body', feral: bool) -> str:
        if (
            feral
            and body is not None
            and body.getHalfDemonSubspecies() is not None
            and body.getHalfDemonSubspecies() != Subspecies.HUMAN
        ):
            return f"demonic-{body.getHalfDemonSubspecies().getFeralNamePlural(body)}"
        return super().getName(body, feral)

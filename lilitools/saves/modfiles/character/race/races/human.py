from lilitools.saves.enums.furry_preference import EFurryPreference
from lilitools.saves.modfiles.character.race.abstract_race import AbstractRace
from lilitools.saves.modfiles.character.race.disposition import EDisposition
from lilitools.saves.modfiles.character.race.racial_class import ERacialClass


## from [LT]/src/com/lilithsthrone/game/character/race/Race.java: public static AbstractRace HUMAN = new AbstractRace("human", @ I2zpLEGttbGdz2qXa/dPTh/avpv7YKWqt22SlQcOFNzwjmZw6g2NtG5hMptkpU/1cPmkew4u65RbJu1eIJE/vw==
class HumanRace(AbstractRace):
    ID = "DEMON"

    def __init__(self) -> None:
        super().__init__()

        self.name = "human"
        self.namePlural = "humans"
        self.nameFeralSingular = {}
        self.nameFeralPlural = {}
        self.defaultTransformName = "human"
        self.colour = 'RACE_HUMAN'
        self.disposition = EDisposition.CIVILIZED
        self.racialClass = ERacialClass.MAMMAL
        self.preferredCombatBehaviour = 'BALANCED'
        self.chanceForMaleOffspring=0.5
        self.numberOfOffspringHigh=1
        self.numberOfOffspringLow=1
        self.defaultFemininePreference=EFurryPreference.NORMAL
        self.defaultMasculinePreference=EFurryPreference.NORMAL
        self.affectedByFurryPreference=False

        self.subspecies=set()

        self.hasFeralPartsAvailable=False

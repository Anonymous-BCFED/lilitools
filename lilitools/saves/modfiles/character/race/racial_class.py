from enum import Enum


class ERacialClass(Enum):
    MAMMAL = True, True
    BIRD = True, True
    REPTILE = False, False
    AMPHIBIAN = False, False
    FISH = False, False
    INSECT = False, False
    OTHER = True, True

    def __init__(self, anthroHair: bool, anthroBreasts: bool) -> None:
        self.anthroHair = anthroHair
        self.anthroBreasts = anthroBreasts

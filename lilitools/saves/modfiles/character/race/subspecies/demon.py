from __future__ import annotations

from typing import TYPE_CHECKING, Dict

from lilitools.saves.character.body.enums.leg_configuration import ELegConfiguration
from lilitools.saves.character.enums.personality_trait import EPersonalityTrait
from lilitools.saves.modfiles.character.race.abstract_subspecies import AbstractSubspecies
from lilitools.saves.modfiles.character.race.affinity import EAffinity
from lilitools.saves.modfiles.character.race.leg_configuration_affinity import (
    LegConfigurationAffinity,
)

if TYPE_CHECKING:
    from lilitools.saves.character.body.body import Body
    from lilitools.saves.modfiles.character.race.abstract_race import AbstractRace


class Demon(AbstractSubspecies):
    ID = 'DEMON'
    def __init__(self) -> None:
        self.baseSlaveValue = 120000
        self.isMainSubspecies=True
        self.name = 'demon'
        self.namePlural = 'demons'
        self.namePlural = 'demons'
        self.plural = 'demons'

    def getFeralName(self, body: "Body") -> str:
        from lilitools.saves.modfiles.character.race.races import Race
        if body is not None:
            r = body.legs.getRace()
            lc = body.legs.configuration
            match lc:
                case ELegConfiguration.BIPEDAL:
                    return "demon"
                case _:
                    if r in ("HUMAN", "DEMON"):
                        return Race.DEMON.getFeralName(
                            LegConfigurationAffinity(lc, self.getAffinity()), False
                        )
                    else:
                        return "demonic-" + Race.GetByID(r).getName(body, True)
        return "demon"

    def getAffinity(self, body: "Body") -> EAffinity:
        return EAffinity.AMPHIBIOUS

    def getWeighting(self, body: "Body", race: AbstractRace) -> float:
        return 0.0

from enum import IntFlag


class ESubspeciesFlags(IntFlag):
    NONE = 0
    HIDDEN_FROM_PREFERENCES = 1
    DISABLE_FURRY_PREFERENCE = 2
    DISABLE_SPAWN_PREFERENCE = 4

    def __str__(self) -> str:
        if self == ESubspeciesFlags.NONE:
            return "NONE"
        return ", ".join(
            [
                x.name
                for x in ESubspeciesFlags
                if x.value.bit_count() == 1 and (self & x) == x
            ]
        )

    def __repr__(self) -> str:
        if self == ESubspeciesFlags.NONE:
            return "ESubspeciesFlags.NONE"
        return "|".join(
            [
                f"ESubspeciesFlags.{x.name}"
                for x in ESubspeciesFlags
                if x.value.bit_count() == 1 and (self & x) == x
            ]
        )

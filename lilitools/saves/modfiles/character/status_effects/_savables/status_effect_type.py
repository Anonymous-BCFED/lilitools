#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, List, Optional
from lxml import etree
from lilitools.saves.character.enums.attribute import EAttribute
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.modfiles.character._savables.application_condition import RawApplicationCondition
from lilitools.saves.modfiles.character._savables.application_length import RawApplicationLength
from lilitools.saves.modfiles.character._savables.apply_effect import RawApplyEffect
from lilitools.saves.savable import XML2BOOL, Savable
__all__ = ['RawStatusEffectType']
## from [LT]/src/com/lilithsthrone/game/character/effects/AbstractStatusEffect.java: public AbstractStatusEffect(File XMLFile, String author, boolean mod) { @ VrGPhgkZikeGkzT8cHCuXs9cOHz2xki+sVkhXXNnFPYBRHKYzoNk38bWWXpdLWCwNvqfci0Ile4rpsALrUeHXw==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawStatusEffectType(Savable):
    TAG = 'statusEffect'
    _XMLID_ATTR_APPLICATIONCONDITION_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_APPLICATIONLENGTH_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_APPLYEFFECT_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_ATTRIBUTEMODIFIERS_VALUEATTR: str = 'value'
    _XMLID_TAG_APPLICATIONCONDITION_ELEMENT: str = 'applicationCondition'
    _XMLID_TAG_APPLICATIONLENGTH_ELEMENT: str = 'applicationLength'
    _XMLID_TAG_APPLYEFFECT_ELEMENT: str = 'applyEffect'
    _XMLID_TAG_APPLYPOSTREMOVALEFFECT_ELEMENT: str = 'applyPostRemovalEffect'
    _XMLID_TAG_APPLYREMOVALEFFECT_ELEMENT: str = 'applyRemovalEffect'
    _XMLID_TAG_ATTRIBUTEMODIFIERS_CHILD: str = 'modifier'
    _XMLID_TAG_ATTRIBUTEMODIFIERS_PARENT: str = 'attributeModifiers'
    _XMLID_TAG_BENEFICIAL_ELEMENT: str = 'beneficial'
    _XMLID_TAG_CATEGORY_ELEMENT: str = 'category'
    _XMLID_TAG_COLOURPRIMARY_ELEMENT: str = 'colourPrimary'
    _XMLID_TAG_COLOURSECONDARY_ELEMENT: str = 'colourSecondary'
    _XMLID_TAG_COLOURTERTIARY_ELEMENT: str = 'colourTertiary'
    _XMLID_TAG_COMBATEFFECT_ELEMENT: str = 'combatEffect'
    _XMLID_TAG_COMBATMOVES_CHILD: str = 'move'
    _XMLID_TAG_COMBATMOVES_PARENT: str = 'combatMoves'
    _XMLID_TAG_DESCRIPTION_ELEMENT: str = 'description'
    _XMLID_TAG_EXTRAEFFECTS_CHILD: str = 'effect'
    _XMLID_TAG_EXTRAEFFECTS_PARENT: str = 'extraEffects'
    _XMLID_TAG_IMAGENAME_ELEMENT: str = 'imageName'
    _XMLID_TAG_NAME_ELEMENT: str = 'name'
    _XMLID_TAG_RENDERINEFFECTSPANEL_ELEMENT: str = 'renderInEffectsPanel'
    _XMLID_TAG_RENDERINGPRIORITY_ELEMENT: str = 'renderingPriority'
    _XMLID_TAG_SEXEFFECT_ELEMENT: str = 'sexEffect'
    _XMLID_TAG_SPELLS_CHILD: str = 'spell'
    _XMLID_TAG_SPELLS_PARENT: str = 'spells'
    _XMLID_TAG_TAGS_CHILD: str = 'tag'
    _XMLID_TAG_TAGS_PARENT: str = 'tags'

    def __init__(self) -> None:
        super().__init__()
        self.category: str = ''  # Element
        self.renderingPriority: int = 0  # Element
        self.beneficial: str = ''  # Element
        self.renderInEffectsPanel: bool = False  # Element
        self.combatEffect: bool = False  # Element
        self.sexEffect: bool = False  # Element
        self.tags: Optional[List[str]] = list()
        self.name: str = ''  # Element
        self.description: str = ''  # Element
        self.imageName: str = ''  # Element
        self.colourPrimary: str = ''  # Element
        self.colourSecondary: Optional[str] = None  # Element
        self.colourTertiary: Optional[str] = None  # Element
        self.colourPrimary: str = ''  # Element
        self.attributeModifiers: Dict[EAttribute, float] = {}
        self.combatMoves: Optional[List[str]] = list()
        self.spells: Optional[List[str]] = list()
        self.extraEffects: Optional[List[str]] = list()
        self.applicationCondition: RawApplicationCondition = RawApplicationCondition()  # Element
        self.applicationLength: Optional[RawApplicationLength] = None  # Element
        self.applyEffect: Optional[RawApplyEffect] = None  # Element
        self.applyRemovalEffect: str = ''  # Element
        self.applyPostRemovalEffect: str = ''  # Element

    def overrideAttrs(self, applicationCondition_attribute: _Optional_str = None, applicationLength_attribute: _Optional_str = None, applyEffect_attribute: _Optional_str = None, attributeModifiers_valueattr: _Optional_str = None) -> None:
        if applicationCondition_attribute is not None:
            self._XMLID_ATTR_APPLICATIONCONDITION_ATTRIBUTE = applicationCondition_attribute
        if applicationLength_attribute is not None:
            self._XMLID_ATTR_APPLICATIONLENGTH_ATTRIBUTE = applicationLength_attribute
        if applyEffect_attribute is not None:
            self._XMLID_ATTR_APPLYEFFECT_ATTRIBUTE = applyEffect_attribute
        if attributeModifiers_valueattr is not None:
            self._XMLID_ATTR_ATTRIBUTEMODIFIERS_VALUEATTR = attributeModifiers_valueattr

    def overrideTags(self, applicationCondition_element: _Optional_str = None, applicationLength_element: _Optional_str = None, applyEffect_element: _Optional_str = None, applyPostRemovalEffect_element: _Optional_str = None, applyRemovalEffect_element: _Optional_str = None, attributeModifiers_child: _Optional_str = None, attributeModifiers_parent: _Optional_str = None, beneficial_element: _Optional_str = None, category_element: _Optional_str = None, colourPrimary_element: _Optional_str = None, colourSecondary_element: _Optional_str = None, colourTertiary_element: _Optional_str = None, combatEffect_element: _Optional_str = None, combatMoves_child: _Optional_str = None, combatMoves_parent: _Optional_str = None, description_element: _Optional_str = None, extraEffects_child: _Optional_str = None, extraEffects_parent: _Optional_str = None, imageName_element: _Optional_str = None, name_element: _Optional_str = None, renderInEffectsPanel_element: _Optional_str = None, renderingPriority_element: _Optional_str = None, sexEffect_element: _Optional_str = None, spells_child: _Optional_str = None, spells_parent: _Optional_str = None, tags_child: _Optional_str = None, tags_parent: _Optional_str = None) -> None:
        if applicationCondition_element is not None:
            self._XMLID_TAG_APPLICATIONCONDITION_ELEMENT = applicationCondition_element
        if applicationLength_element is not None:
            self._XMLID_TAG_APPLICATIONLENGTH_ELEMENT = applicationLength_element
        if applyEffect_element is not None:
            self._XMLID_TAG_APPLYEFFECT_ELEMENT = applyEffect_element
        if applyPostRemovalEffect_element is not None:
            self._XMLID_TAG_APPLYPOSTREMOVALEFFECT_ELEMENT = applyPostRemovalEffect_element
        if applyRemovalEffect_element is not None:
            self._XMLID_TAG_APPLYREMOVALEFFECT_ELEMENT = applyRemovalEffect_element
        if attributeModifiers_child is not None:
            self._XMLID_TAG_ATTRIBUTEMODIFIERS_CHILD = attributeModifiers_child
        if attributeModifiers_parent is not None:
            self._XMLID_TAG_ATTRIBUTEMODIFIERS_PARENT = attributeModifiers_parent
        if beneficial_element is not None:
            self._XMLID_TAG_BENEFICIAL_ELEMENT = beneficial_element
        if category_element is not None:
            self._XMLID_TAG_CATEGORY_ELEMENT = category_element
        if colourPrimary_element is not None:
            self._XMLID_TAG_COLOURPRIMARY_ELEMENT = colourPrimary_element
        if colourSecondary_element is not None:
            self._XMLID_TAG_COLOURSECONDARY_ELEMENT = colourSecondary_element
        if colourTertiary_element is not None:
            self._XMLID_TAG_COLOURTERTIARY_ELEMENT = colourTertiary_element
        if combatEffect_element is not None:
            self._XMLID_TAG_COMBATEFFECT_ELEMENT = combatEffect_element
        if combatMoves_child is not None:
            self._XMLID_TAG_COMBATMOVES_CHILD = combatMoves_child
        if combatMoves_parent is not None:
            self._XMLID_TAG_COMBATMOVES_PARENT = combatMoves_parent
        if description_element is not None:
            self._XMLID_TAG_DESCRIPTION_ELEMENT = description_element
        if extraEffects_child is not None:
            self._XMLID_TAG_EXTRAEFFECTS_CHILD = extraEffects_child
        if extraEffects_parent is not None:
            self._XMLID_TAG_EXTRAEFFECTS_PARENT = extraEffects_parent
        if imageName_element is not None:
            self._XMLID_TAG_IMAGENAME_ELEMENT = imageName_element
        if name_element is not None:
            self._XMLID_TAG_NAME_ELEMENT = name_element
        if renderInEffectsPanel_element is not None:
            self._XMLID_TAG_RENDERINEFFECTSPANEL_ELEMENT = renderInEffectsPanel_element
        if renderingPriority_element is not None:
            self._XMLID_TAG_RENDERINGPRIORITY_ELEMENT = renderingPriority_element
        if sexEffect_element is not None:
            self._XMLID_TAG_SEXEFFECT_ELEMENT = sexEffect_element
        if spells_child is not None:
            self._XMLID_TAG_SPELLS_CHILD = spells_child
        if spells_parent is not None:
            self._XMLID_TAG_SPELLS_PARENT = spells_parent
        if tags_child is not None:
            self._XMLID_TAG_TAGS_CHILD = tags_child
        if tags_parent is not None:
            self._XMLID_TAG_TAGS_PARENT = tags_parent

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        dc: etree._Element
        df: etree._Element
        lk: str
        lp: etree._Element
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_CATEGORY_ELEMENT)) is not None:
            self.category = sf.text
        else:
            raise ElementRequiredException(self, e, 'category', self._XMLID_TAG_CATEGORY_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_RENDERINGPRIORITY_ELEMENT)) is not None:
            self.renderingPriority = int(sf.text)
        else:
            raise ElementRequiredException(self, e, 'renderingPriority', self._XMLID_TAG_RENDERINGPRIORITY_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_BENEFICIAL_ELEMENT)) is not None:
            self.beneficial = sf.text
        else:
            raise ElementRequiredException(self, e, 'beneficial', self._XMLID_TAG_BENEFICIAL_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_RENDERINEFFECTSPANEL_ELEMENT)) is not None:
            self.renderInEffectsPanel = XML2BOOL[sf.text]
        else:
            raise ElementRequiredException(self, e, 'renderInEffectsPanel', self._XMLID_TAG_RENDERINEFFECTSPANEL_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_COMBATEFFECT_ELEMENT)) is not None:
            self.combatEffect = XML2BOOL[sf.text]
        else:
            raise ElementRequiredException(self, e, 'combatEffect', self._XMLID_TAG_COMBATEFFECT_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_SEXEFFECT_ELEMENT)) is not None:
            self.sexEffect = XML2BOOL[sf.text]
        else:
            raise ElementRequiredException(self, e, 'sexEffect', self._XMLID_TAG_SEXEFFECT_ELEMENT)
        if (lf := e.find(self._XMLID_TAG_TAGS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_TAGS_CHILD):
                    self.tags.append(lc.text)
        if (sf := e.find(self._XMLID_TAG_NAME_ELEMENT)) is not None:
            self.name = sf.text
        else:
            raise ElementRequiredException(self, e, 'name', self._XMLID_TAG_NAME_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_DESCRIPTION_ELEMENT)) is not None:
            self.description = sf.text
        else:
            raise ElementRequiredException(self, e, 'description', self._XMLID_TAG_DESCRIPTION_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_IMAGENAME_ELEMENT)) is not None:
            self.imageName = sf.text
        else:
            raise ElementRequiredException(self, e, 'imageName', self._XMLID_TAG_IMAGENAME_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_COLOURPRIMARY_ELEMENT)) is not None:
            self.colourPrimary = sf.text
        else:
            raise ElementRequiredException(self, e, 'colourPrimary', self._XMLID_TAG_COLOURPRIMARY_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_COLOURSECONDARY_ELEMENT)) is not None:
            self.colourSecondary = sf.text
        if (sf := e.find(self._XMLID_TAG_COLOURTERTIARY_ELEMENT)) is not None:
            self.colourTertiary = sf.text
        if (sf := e.find(self._XMLID_TAG_COLOURPRIMARY_ELEMENT)) is not None:
            self.colourPrimary = sf.text
        else:
            raise ElementRequiredException(self, e, 'colourPrimary', self._XMLID_TAG_COLOURPRIMARY_ELEMENT)
        if (df := e.find(self._XMLID_TAG_ATTRIBUTEMODIFIERS_PARENT)) is not None:
            for dc in df.iterfind(self._XMLID_TAG_ATTRIBUTEMODIFIERS_CHILD):
                self.attributeModifiers[EAttribute[dc.text]] = float(dc.attrib['value'])
        if (lf := e.find(self._XMLID_TAG_COMBATMOVES_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_COMBATMOVES_CHILD):
                    self.combatMoves.append(lc.text)
        if (lf := e.find(self._XMLID_TAG_SPELLS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_SPELLS_CHILD):
                    self.spells.append(lc.text)
        if (lf := e.find(self._XMLID_TAG_EXTRAEFFECTS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_EXTRAEFFECTS_CHILD):
                    self.extraEffects.append(lc.text)
        if (sf := e.find(self._XMLID_TAG_APPLICATIONCONDITION_ELEMENT)) is not None:
            self.applicationCondition = RawApplicationCondition()
            self.applicationCondition.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'applicationCondition', self._XMLID_TAG_APPLICATIONCONDITION_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_APPLICATIONLENGTH_ELEMENT)) is not None:
            self.applicationLength = RawApplicationLength()
            self.applicationLength.fromXML(sf)
        if (sf := e.find(self._XMLID_TAG_APPLYEFFECT_ELEMENT)) is not None:
            self.applyEffect = RawApplyEffect()
            self.applyEffect.fromXML(sf)
        if (sf := e.find(self._XMLID_TAG_APPLYREMOVALEFFECT_ELEMENT)) is not None:
            self.applyRemovalEffect = sf.text
        else:
            raise ElementRequiredException(self, e, 'applyRemovalEffect', self._XMLID_TAG_APPLYREMOVALEFFECT_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_APPLYPOSTREMOVALEFFECT_ELEMENT)) is not None:
            self.applyPostRemovalEffect = sf.text
        else:
            raise ElementRequiredException(self, e, 'applyPostRemovalEffect', self._XMLID_TAG_APPLYPOSTREMOVALEFFECT_ELEMENT)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        dK: Any
        dV: Any
        dc: etree._Element
        dp: etree._Element
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        etree.SubElement(e, self._XMLID_TAG_CATEGORY_ELEMENT).text = str(self.category)
        etree.SubElement(e, self._XMLID_TAG_RENDERINGPRIORITY_ELEMENT).text = str(self.renderingPriority)
        etree.SubElement(e, self._XMLID_TAG_BENEFICIAL_ELEMENT).text = str(self.beneficial)
        etree.SubElement(e, self._XMLID_TAG_RENDERINEFFECTSPANEL_ELEMENT).text = str(self.renderInEffectsPanel).lower()
        etree.SubElement(e, self._XMLID_TAG_COMBATEFFECT_ELEMENT).text = str(self.combatEffect).lower()
        etree.SubElement(e, self._XMLID_TAG_SEXEFFECT_ELEMENT).text = str(self.sexEffect).lower()
        if self.tags is not None and len(self.tags) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_TAGS_PARENT)
            for lv in self.tags:
                etree.SubElement(lp, self._XMLID_TAG_TAGS_CHILD, {}).text = lv
        etree.SubElement(e, self._XMLID_TAG_NAME_ELEMENT).text = str(self.name)
        etree.SubElement(e, self._XMLID_TAG_DESCRIPTION_ELEMENT).text = str(self.description)
        etree.SubElement(e, self._XMLID_TAG_IMAGENAME_ELEMENT).text = str(self.imageName)
        etree.SubElement(e, self._XMLID_TAG_COLOURPRIMARY_ELEMENT).text = str(self.colourPrimary)
        if self.colourSecondary is not None:
            etree.SubElement(e, self._XMLID_TAG_COLOURSECONDARY_ELEMENT).text = str(self.colourSecondary)
        if self.colourTertiary is not None:
            etree.SubElement(e, self._XMLID_TAG_COLOURTERTIARY_ELEMENT).text = str(self.colourTertiary)
        etree.SubElement(e, self._XMLID_TAG_COLOURPRIMARY_ELEMENT).text = str(self.colourPrimary)
        if self.attributeModifiers is not None and len(self.attributeModifiers) > 0:
            dp = etree.SubElement(e, self._XMLID_TAG_ATTRIBUTEMODIFIERS_PARENT)
            for dK, dV in sorted(self.attributeModifiers.items(), key=lambda t: t[0].name):
                dc = etree.SubElement(dp, self._XMLID_TAG_ATTRIBUTEMODIFIERS_CHILD)
                dc.text = dK.name
                dc.attrib['value'] = str(dV)
        if self.combatMoves is not None and len(self.combatMoves) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_COMBATMOVES_PARENT)
            for lv in self.combatMoves:
                etree.SubElement(lp, self._XMLID_TAG_COMBATMOVES_CHILD, {}).text = lv
        if self.spells is not None and len(self.spells) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_SPELLS_PARENT)
            for lv in self.spells:
                etree.SubElement(lp, self._XMLID_TAG_SPELLS_CHILD, {}).text = lv
        if self.extraEffects is not None and len(self.extraEffects) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_EXTRAEFFECTS_PARENT)
            for lv in self.extraEffects:
                etree.SubElement(lp, self._XMLID_TAG_EXTRAEFFECTS_CHILD, {}).text = lv
        e.append(self.applicationCondition.toXML(self._XMLID_TAG_APPLICATIONCONDITION_ELEMENT))
        if self.applicationLength is not None:
            e.append(self.applicationLength.toXML(self._XMLID_TAG_APPLICATIONLENGTH_ELEMENT))
        if self.applyEffect is not None:
            e.append(self.applyEffect.toXML(self._XMLID_TAG_APPLYEFFECT_ELEMENT))
        etree.SubElement(e, self._XMLID_TAG_APPLYREMOVALEFFECT_ELEMENT).text = str(self.applyRemovalEffect)
        etree.SubElement(e, self._XMLID_TAG_APPLYPOSTREMOVALEFFECT_ELEMENT).text = str(self.applyPostRemovalEffect)
        return e

    def toDict(self) -> Dict[str, Any]:
        dK: Any
        dV: Any
        data: Dict[str, Any] = super().toDict()
        dp: Dict[str, Any]
        data['category'] = str(self.category)
        data['renderingPriority'] = int(self.renderingPriority)
        data['beneficial'] = str(self.beneficial)
        data['renderInEffectsPanel'] = bool(self.renderInEffectsPanel)
        data['combatEffect'] = bool(self.combatEffect)
        data['sexEffect'] = bool(self.sexEffect)
        if self.tags is None or len(self.tags) > 0:
            data['tags'] = list()
        else:
            lc = list()
            if len(self.tags) > 0:
                for lv in lc:
                    lc.append(str(lv))
            data['tags'] = lc
        data['name'] = str(self.name)
        data['description'] = str(self.description)
        data['imageName'] = str(self.imageName)
        data['colourPrimary'] = str(self.colourPrimary)
        if self.colourSecondary is not None:
            data['colourSecondary'] = str(self.colourSecondary)
        if self.colourTertiary is not None:
            data['colourTertiary'] = str(self.colourTertiary)
        data['colourPrimary'] = str(self.colourPrimary)
        if self.attributeModifiers is not None and len(self.attributeModifiers) > 0:
            dp = {}
            for dK, dV in sorted(self.attributeModifiers.items(), key=lambda t: t[0].name):
                sk = dK.name
                sv = dV
                dp[sk] = sv
            data['attributeModifiers'] = dp
        if self.combatMoves is None or len(self.combatMoves) > 0:
            data['combatMoves'] = list()
        else:
            lc = list()
            if len(self.combatMoves) > 0:
                for lv in lc:
                    lc.append(str(lv))
            data['combatMoves'] = lc
        if self.spells is None or len(self.spells) > 0:
            data['spells'] = list()
        else:
            lc = list()
            if len(self.spells) > 0:
                for lv in lc:
                    lc.append(str(lv))
            data['spells'] = lc
        if self.extraEffects is None or len(self.extraEffects) > 0:
            data['extraEffects'] = list()
        else:
            lc = list()
            if len(self.extraEffects) > 0:
                for lv in lc:
                    lc.append(str(lv))
            data['extraEffects'] = lc
        data['applicationCondition'] = self.applicationCondition.toDict()
        if self.applicationLength is not None:
            data['applicationLength'] = self.applicationLength.toDict()
        if self.applyEffect is not None:
            data['applyEffect'] = self.applyEffect.toDict()
        data['applyRemovalEffect'] = str(self.applyRemovalEffect)
        data['applyPostRemovalEffect'] = str(self.applyPostRemovalEffect)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('category')) is not None:
            self.category = data['category']
        else:
            raise KeyRequiredException(self, data, 'category', 'category')
        if (sf := data.get('renderingPriority')) is not None:
            self.renderingPriority = data['renderingPriority']
        else:
            raise KeyRequiredException(self, data, 'renderingPriority', 'renderingPriority')
        if (sf := data.get('beneficial')) is not None:
            self.beneficial = data['beneficial']
        else:
            raise KeyRequiredException(self, data, 'beneficial', 'beneficial')
        if (sf := data.get('renderInEffectsPanel')) is not None:
            self.renderInEffectsPanel = data['renderInEffectsPanel']
        else:
            raise KeyRequiredException(self, data, 'renderInEffectsPanel', 'renderInEffectsPanel')
        if (sf := data.get('combatEffect')) is not None:
            self.combatEffect = data['combatEffect']
        else:
            raise KeyRequiredException(self, data, 'combatEffect', 'combatEffect')
        if (sf := data.get('sexEffect')) is not None:
            self.sexEffect = data['sexEffect']
        else:
            raise KeyRequiredException(self, data, 'sexEffect', 'sexEffect')
        if (lv := self.tags) is not None:
            for le in lv:
                self.tags.append(str(le))
        else:
            self.tags = list()
        if (sf := data.get('name')) is not None:
            self.name = data['name']
        else:
            raise KeyRequiredException(self, data, 'name', 'name')
        if (sf := data.get('description')) is not None:
            self.description = data['description']
        else:
            raise KeyRequiredException(self, data, 'description', 'description')
        if (sf := data.get('imageName')) is not None:
            self.imageName = data['imageName']
        else:
            raise KeyRequiredException(self, data, 'imageName', 'imageName')
        if (sf := data.get('colourPrimary')) is not None:
            self.colourPrimary = data['colourPrimary']
        else:
            raise KeyRequiredException(self, data, 'colourPrimary', 'colourPrimary')
        if (sf := data.get('colourSecondary')) is not None:
            self.colourSecondary = data['colourSecondary']
        else:
            self.colourSecondary = None
        if (sf := data.get('colourTertiary')) is not None:
            self.colourTertiary = data['colourTertiary']
        else:
            self.colourTertiary = None
        if (sf := data.get('colourPrimary')) is not None:
            self.colourPrimary = data['colourPrimary']
        else:
            raise KeyRequiredException(self, data, 'colourPrimary', 'colourPrimary')
        if (df := data.get('attributeModifiers')) is not None:
            for sk, sv in df.items():
                self.attributeModifiers[EAttribute[sk]] = sv
        if (lv := self.combatMoves) is not None:
            for le in lv:
                self.combatMoves.append(str(le))
        else:
            self.combatMoves = list()
        if (lv := self.spells) is not None:
            for le in lv:
                self.spells.append(str(le))
        else:
            self.spells = list()
        if (lv := self.extraEffects) is not None:
            for le in lv:
                self.extraEffects.append(str(le))
        else:
            self.extraEffects = list()
        if (sf := data.get('applicationCondition')) is not None:
            self.applicationCondition = RawApplicationCondition()
            self.applicationCondition.fromDict(data['applicationCondition'])
        else:
            raise KeyRequiredException(self, data, 'applicationCondition', 'applicationCondition')
        if (sf := data.get('applicationLength')) is not None:
            self.applicationLength = RawApplicationLength()
            self.applicationLength.fromDict(data['applicationLength'])
        else:
            self.applicationLength = None
        if (sf := data.get('applyEffect')) is not None:
            self.applyEffect = RawApplyEffect()
            self.applyEffect.fromDict(data['applyEffect'])
        else:
            self.applyEffect = None
        if (sf := data.get('applyRemovalEffect')) is not None:
            self.applyRemovalEffect = data['applyRemovalEffect']
        else:
            raise KeyRequiredException(self, data, 'applyRemovalEffect', 'applyRemovalEffect')
        if (sf := data.get('applyPostRemovalEffect')) is not None:
            self.applyPostRemovalEffect = data['applyPostRemovalEffect']
        else:
            raise KeyRequiredException(self, data, 'applyPostRemovalEffect', 'applyPostRemovalEffect')

from lilitools.saves.modfiles.character.status_effects._savables.status_effect_type import RawStatusEffectType


class StatusEffectType(RawStatusEffectType):
    pass
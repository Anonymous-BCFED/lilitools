#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawSubSpeciesType']
## from [LT]/src/com/lilithsthrone/game/character/race/AbstractSubspecies.java: public AbstractSubspecies(File XMLFile, String author, boolean mod) { @ m8gczffvfl0DbFMl8MiCONtqjihRGEZw06VfB5CfnBtOxRdI8XeTVMK0HLee7xHl59Bw0z4AB9We7H7CTfNmrw==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawSubSpeciesType(Savable):
    TAG = 'subspecies'
    _XMLID_TAG_AFFINITY_ELEMENT: str = 'affinity'
    _XMLID_TAG_BASESLAVEVALUE_ELEMENT: str = 'baseSlaveValue'
    _XMLID_TAG_MAINSUBSPECIES_ELEMENT: str = 'mainSubspecies'
    _XMLID_TAG_RACE_ELEMENT: str = 'race'

    def __init__(self) -> None:
        super().__init__()
        self.race: str = ''  # Element
        self.affinity: str = 'AMPHIBIOUS'  # Element
        self.mainSubspecies: str = ''  # Element
        self.baseSlaveValue: int = 0  # Element

    def overrideTags(self, affinity_element: _Optional_str = None, baseSlaveValue_element: _Optional_str = None, mainSubspecies_element: _Optional_str = None, race_element: _Optional_str = None) -> None:
        if affinity_element is not None:
            self._XMLID_TAG_AFFINITY_ELEMENT = affinity_element
        if baseSlaveValue_element is not None:
            self._XMLID_TAG_BASESLAVEVALUE_ELEMENT = baseSlaveValue_element
        if mainSubspecies_element is not None:
            self._XMLID_TAG_MAINSUBSPECIES_ELEMENT = mainSubspecies_element
        if race_element is not None:
            self._XMLID_TAG_RACE_ELEMENT = race_element

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_RACE_ELEMENT)) is not None:
            self.race = sf.text
        else:
            raise ElementRequiredException(self, e, 'race', self._XMLID_TAG_RACE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_AFFINITY_ELEMENT)) is not None:
            self.affinity = sf.text
        else:
            self.affinity = 'AMPHIBIOUS'
        if (sf := e.find(self._XMLID_TAG_MAINSUBSPECIES_ELEMENT)) is not None:
            self.mainSubspecies = sf.text
        else:
            raise ElementRequiredException(self, e, 'mainSubspecies', self._XMLID_TAG_MAINSUBSPECIES_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_BASESLAVEVALUE_ELEMENT)) is not None:
            self.baseSlaveValue = int(sf.text)
        else:
            raise ElementRequiredException(self, e, 'baseSlaveValue', self._XMLID_TAG_BASESLAVEVALUE_ELEMENT)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        etree.SubElement(e, self._XMLID_TAG_RACE_ELEMENT).text = str(self.race)
        if self.affinity is not None and self.affinity != 'AMPHIBIOUS':
            etree.SubElement(e, self._XMLID_TAG_AFFINITY_ELEMENT).text = str(self.affinity)
        etree.SubElement(e, self._XMLID_TAG_MAINSUBSPECIES_ELEMENT).text = str(self.mainSubspecies)
        etree.SubElement(e, self._XMLID_TAG_BASESLAVEVALUE_ELEMENT).text = str(self.baseSlaveValue)
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['race'] = str(self.race)
        if self.affinity is not None and self.affinity != 'AMPHIBIOUS':
            data['affinity'] = str(self.affinity)
        data['mainSubspecies'] = str(self.mainSubspecies)
        data['baseSlaveValue'] = int(self.baseSlaveValue)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('race')) is not None:
            self.race = data['race']
        else:
            raise KeyRequiredException(self, data, 'race', 'race')
        if (sf := data.get('affinity')) is not None:
            self.affinity = data['affinity']
        else:
            self.affinity = 'AMPHIBIOUS'
        if (sf := data.get('mainSubspecies')) is not None:
            self.mainSubspecies = data['mainSubspecies']
        else:
            raise KeyRequiredException(self, data, 'mainSubspecies', 'mainSubspecies')
        if (sf := data.get('baseSlaveValue')) is not None:
            self.baseSlaveValue = data['baseSlaveValue']
        else:
            raise KeyRequiredException(self, data, 'baseSlaveValue', 'baseSlaveValue')

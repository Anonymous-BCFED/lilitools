from typing import Generic, Type, TypeVar
from lilitools.saves.savable import Savable

T = TypeVar('T')
class SavableObjectDefinition(Generic[T], Savable):
    INSTANCE_TYPE: Type[T] = None
    def instantiate(self) -> T:
        return self.INSTANCE_TYPE()
from enum import Enum
from pathlib import Path
from typing import Any, Dict, Generator, Generic, List, Optional, Set, Type, TypeVar

from lxml import etree

from lilitools.logging import ClickWrap
from lilitools.saves.modfiles.savable_object_definition import SavableObjectDefinition
from lilitools.utils import are_equal_case_insensitive_strings

click = ClickWrap()

MOD_DIR = Path("res") / "mods"


class ModFileEntry:
    def __init__(self, author: str, id: str, file: Path, is_mod: bool) -> None:
        self.author: str = author
        self.id: str = id
        self.file: Path = file
        self.is_mod: bool = is_mod

    def serialize(self) -> dict:
        return {
            "id": self.id,
            "author": self.author,
            "is_mod": self.is_mod,
            "file": str(self.file.absolute()),
        }


def populateMapFiles(
    o: Dict[str, Dict[str, Path]],
    modAuthorName: str,
    idPrefix: str,
    directory: Path,
    filterFolderName: Optional[str] = None,
    filterPathName: Optional[str] = None,
) -> None:
    ## from [LT]/src/com/lilithsthrone/utils/Util.java: private static Map<String, Map<String, File>> populateMapFiles(String modAuthorName, String idPrefix, File directory, Map<String, Map<String, File>> returnMap, String filterFolderName, String filterPathName) { @ C27cORepkxRwdCtyKD68JabgFJToTs0LfwbCYpW480jdYKvOd/HY/c+zpiPzpKa14CT6Jv/4BTOMnWmJG+AwPQ==

    # if(filterFolderName==null || filterFolderName.equalsIgnoreCase(directory.getName())) {
    #     File[] innerDirectoryListing = directory.listFiles((path, filename) -> filename.toLowerCase().endsWith(".xml"));

    #     if(innerDirectoryListing != null) {
    #         for(File innerChild : innerDirectoryListing) {
    #             if(filterPathName==null || filterPathName.equalsIgnoreCase(innerChild.getName().split("\\.")[0])) {
    #                 try {
    #                     String id = (idPrefix!=null?idPrefix:"")+innerChild.getName().split("\\.")[0];
    #                     returnMap.get(modAuthorName).put(id, innerChild);
    #                 } catch(Exception ex) {
    #                     System.err.println("Loading external mod files failed at Util.getExternalModFilesById()");
    #                     System.err.println("File path: "+innerChild.getAbsolutePath());
    #                     ex.printStackTrace();
    #                 }
    #             }
    #         }
    #     }
    # }
    idPrefixOrEmpty = "" if idPrefix is None else idPrefix

    if filterFolderName is None or are_equal_case_insensitive_strings(
        filterFolderName, directory.name
    ):
        for innerChild in directory.glob("*.xml"):
            if filterPathName is None or are_equal_case_insensitive_strings(
                filterPathName, innerChild.stem
            ):  # stem is the name without the extension
                try:
                    id = idPrefixOrEmpty + innerChild.stem
                    o[modAuthorName][id] = innerChild
                except Exception as e:
                    click.error(
                        f"Failed to add external file {innerChild.absolute()} to data dictionary."
                    )
                    click.error(repr(e))

    # File[] additionalDirectories =  directory.listFiles();

    # if(additionalDirectories != null) {
    #     for(File f : additionalDirectories) {
    #         if(f.isDirectory()) {
    #             populateMapFiles(modAuthorName, (idPrefix!=null?idPrefix:"")+f.getName()+"_", f, returnMap, filterFolderName, filterPathName);
    #         }
    #     }
    # }
    for f in directory.iterdir():
        if f.is_dir():
            populateMapFiles(
                o,
                modAuthorName,
                f"{idPrefixOrEmpty}{f.name}_",
                f,
                filterFolderName,
                filterPathName,
            )

    # return returnMap;
    # return o


def getExternalFilesById(
    startDir: Path,
    filterFolderName: Optional[str] = None,
    filterPathName: Optional[str] = None,
) -> Dict[str, Dict[str, Path]]:
    ## from [LT]/src/com/lilithsthrone/utils/Util.java: public static Map<String, Map<String, File>> getExternalFilesById(String containingFolderId, String filterFolderName, String filterPathName) { @ fl2RSsja5oOO4bqwEozBPuwipkTbm/6J8elgVLx7XdnjIHcYgg/0Pk4Y2+T5KmXIxLtCQRQZpvnOnei72ihjCA==
    o: Dict[str, Dict[str, Path]] = {}
    if startDir.is_dir():
        for authordir in startDir.iterdir():
            if authordir.is_dir():
                authorID = authordir.name
                o[authorID] = {}
                populateMapFiles(
                    o,
                    authorID,
                    authorID + "_",
                    authordir,
                    filterFolderName,
                    filterPathName,
                )
    return o


def getExternalModFilesById(
    startDir: Path,
    containingDir: Path,
    filterFolderName: Optional[str] = None,
    filterPathName: Optional[str] = None,
) -> Dict[str, Dict[str, Path]]:
    ## from [LT]/src/com/lilithsthrone/utils/Util.java: public static Map<String, Map<String, File>> getExternalModFilesById(String containingFolderId, String filterFolderName, String filterPathName) { @ axmJxjNsNWdMe7aofYuID1kSacKI3YJs7dqNwtRW0ssv94siWoP79ThGzglG2rQd3gl1lW8qDCObeyz3H/2b8Q==
    o: Dict[str, Dict[str, Path]] = {}

    # print("Reading mods from", startDir)
    if startDir.is_dir():
        for authordir in startDir.iterdir():
            # print("Reading mod from", authordir)
            if authordir.is_dir():
                authorID = authordir.name
                objdir = authordir / containingDir
                if not objdir.is_dir():
                    continue
                o[authorID] = {}
                populateMapFiles(
                    o,
                    authorID,
                    authorID + "_",
                    objdir,
                    filterFolderName,
                    filterPathName,
                )
    return o


def mkassetid(pathchunks: List[str], file: Path) -> str:
    # author_path_to_file
    return "_".join(pathchunks + [file.stem])


def _getFileRecordsIn(
    assets: List[ModFileEntry],
    author: str,
    currentPath: List[str],
    directory: Path,
    filterFolderName: Optional[str] = None,
    filterPathName: Optional[str] = None,
) -> None:
    desiredFolder = filterFolderName is None or are_equal_case_insensitive_strings(
        filterFolderName, directory.name
    )
    for entry in directory.iterdir():
        if entry.is_file():
            if not desiredFolder:
                continue
            if filterPathName is None or are_equal_case_insensitive_strings(
                filterPathName, entry.stem
            ):
                assets.append(
                    ModFileEntry(author, mkassetid(currentPath, entry), entry, True)
                )
        elif entry.is_dir():
            _getFileRecordsIn(
                assets,
                author,
                currentPath + [entry.name],
                entry,
                filterFolderName,
                filterPathName,
            )


def getExternalFileRecordsById(
    startDir: Path,
    filterFolderName: Optional[str] = None,
    filterPathName: Optional[str] = None,
) -> List[ModFileEntry]:
    assert (
        startDir.is_dir()
    ), f"Directory {startDir.absolute()} either does not exist, or is not a directory."
    assets: List[ModFileEntry] = []
    for authorDir in startDir.iterdir():
        if not authorDir.is_dir():
            continue
        authorID: str = authorDir.name
        _getFileRecordsIn(
            assets, authorID, [authorID], authorDir, filterFolderName, filterPathName
        )
    return assets


BASEDIRS = {
    ".",
    "dist",
}
T = TypeVar("T", bound=SavableObjectDefinition)


class MFELookupTable(Generic[T]):
    DEBUG: bool = False
    RES_SUB_DIR: Path = None
    FILTER_RES_FOLDER_NAME: Optional[str] = None
    FILTER_RES_PATH_NAME: Optional[Path] = None
    MOD_SUB_DIR: Path = None
    FILTER_MOD_FOLDER_NAME: Optional[str] = None
    FILTER_MOD_PATH_NAME: Optional[Path] = None
    FILTER_ROOT_ELEMENT: Optional[str] = None

    def __init__(
        self,
        mfe_builtins: Type[Enum],
        resSubDir: Optional[Path] = None,
        savable_type: Optional[Type[T]] = None,
    ) -> None:
        self.allMFEs: Set[ModFileEntry] = set()
        self.mfesById: Dict[str, ModFileEntry] = {}
        self.entByID: Dict[str, T] = {}
        self.savable_type: Optional[Type[T]] = savable_type

        if resSubDir is not None:
            for basedir in BASEDIRS:
                basedResDir = self.mkPathToBasedResDir(Path(basedir) / resSubDir)
                if basedResDir.is_dir():
                    self.load(mfe_builtins, basedResDir, Path(basedir) / "res" / "mods")
                    break

    def mkPathToBasedResDir(self, path: Path) -> Path:
        return path

    def addMFE(self, author: str, id: str, path: Path, is_mod: bool) -> None:
        if self.DEBUG:
            print(f"addMFE({author=}, {id=}, {path=}, {is_mod=})")
        mfe = ModFileEntry(author, id, path, is_mod)
        self.mfesById[mfe.id.casefold()] = mfe
        self.allMFEs.add(mfe)

    def addBuiltin(self, author: str, id: str, e: T) -> None:
        if self.DEBUG:
            print(f"addBuiltinMFE({author=}, {id=})")
        mfe = ModFileEntry(author, id, None, False)
        self.mfesById[mfe.id.casefold()] = mfe
        self.allMFEs.add(mfe)
        self.entByID[id.casefold()] = e

    def load(
        self,
        mfe_builtins: Type[Enum],
        resSubDir: Path,
        modsDir: Path,
    ) -> None:
        if self.DEBUG:
            print(
                f"{self.__class__.__name__}.load({mfe_builtins}, {resSubDir}, {modsDir}) called"
            )
        mfe: ModFileEntry
        # for mfe in getExternalFileRecordsById(
        #     resSubDir, filterPathName=self.FILTER_PATH_NAME
        # ):
        #     if self.FILTER_ROOT_ELEMENT is not None:
        #         try:
        #             doc = etree.parse(str(mfe.file))
        #             if doc.getroot().tag != self.FILTER_ROOT_ELEMENT:
        #                 continue
        #         except:
        #             continue

        #     self.mfesById[mfe.id] = mfe
        #     self.allMFEs.add(mfe)

        modMFECount = 0
        for author, items in getExternalModFilesById(
            modsDir,
            self.MOD_SUB_DIR,
            filterFolderName=self.FILTER_MOD_FOLDER_NAME,
            filterPathName=self.FILTER_MOD_PATH_NAME,
        ).items():
            for id_, path in items.items():
                if self.FILTER_ROOT_ELEMENT is not None:
                    try:
                        doc = etree.parse(
                            str(mfe.file), parser=etree.XMLParser(remove_comments=True)
                        )
                        if doc.getroot().tag != self.FILTER_ROOT_ELEMENT:
                            continue
                    except:
                        continue
                self.addMFE(author, id_, path, True)
                modMFECount += 1

        stockMFECount = 0
        for author, items in getExternalFilesById(
            resSubDir,
            filterFolderName=self.FILTER_RES_FOLDER_NAME,
            filterPathName=self.FILTER_RES_PATH_NAME,
        ).items():
            for id_, path in items.items():
                if self.FILTER_ROOT_ELEMENT is not None:
                    try:
                        doc = etree.parse(
                            str(mfe.file), parser=etree.XMLParser(remove_comments=True)
                        )
                        if doc.getroot().tag != self.FILTER_ROOT_ELEMENT:
                            continue
                    except:
                        continue
                self.addMFE(author, id_, path, False)
                stockMFECount += 1

        for e in mfe_builtins:
            self.handleBuiltinsEnumEntry(e)
        builtinMFECount = len(self.allMFEs) - (modMFECount + stockMFECount)

        if self.DEBUG:
            print(
                f"{self.__class__.__name__}: Loaded {len(self.allMFEs)} MFEs (built-ins: {builtinMFECount}, external: {stockMFECount}, mods: {modMFECount})"
            )
        # from ruamel.yaml import YAML as Yaml

        # YAML = Yaml(typ="rt")
        # with open(f"_LOADED_{self.__class__.__name__}.yml", "w") as f:
        #     YAML.dump_all([x.serialize() for x in self.allMFEs], f)

    def handleBuiltinsEnumEntry(self, e: Any) -> None:
        pass

    def get(self, id: str) -> Optional[T]:
        id = id.casefold()
        if id not in self.entByID.keys():
            if id not in self.mfesById.keys():
                # print(repr(self.mfesById.keys()))
                return None
            mfe = self.mfesById[id]
            o = self.savable_type()
            o.fromXML(
                etree.parse(
                    mfe.file, parser=etree.XMLParser(remove_comments=True)
                ).getroot()
            )
            o.id = mfe.id.casefold()
            self.entByID[id] = o
            return o
        return self.entByID[id]

    def all(self) -> Generator[T, None, None]:
        emitted = set()
        for id, e in self.entByID.items():
            emitted.add(id.casefold())
            yield e
        for mfe in self.allMFEs:
            if (mfe.id.casefold()) in emitted:
                continue
            o = self.savable_type()
            try:
                o.fromXML(
                    etree.parse(
                        mfe.file, parser=etree.XMLParser(remove_comments=True)
                    ).getroot()
                )
            except Exception as e:
                with click.error(f"in {mfe.file}:"):
                    raise e
            o.id = mfe.id
            yield o

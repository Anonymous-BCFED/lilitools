#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, List, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL, Savable
__all__ = ['RawWeaponColours']
## from [LT]/src/com/lilithsthrone/game/inventory/weapon/AbstractWeaponType.java: public AbstractWeaponType(File weaponXMLFile, String author, boolean mod) { @ RUP36sdnQ73d6klRmOstq3FVFj4NPI0anC1glCYhmMaueuUfNBvz9MgUwd8fYQ63b4O8aa4JKoR6qek4CknYjw==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawWeaponColours(Savable):
    TAG = 'colours'
    _XMLID_ATTR_COPYCOLOURINDEX_ATTRIBUTE: str = 'copyColourIndex'
    _XMLID_ATTR_RECOLOURINGALLOWED_ATTRIBUTE: str = 'recolouringAllowed'
    _XMLID_ATTR_VALUES_ATTRIBUTE: str = 'values'
    _XMLID_TAG_COLOURS_CHILD: str = 'colour'

    def __init__(self) -> None:
        super().__init__()
        self.recolouringAllowed: Optional[bool] = None
        self.copyColourIndex: Optional[int] = None
        self.values: Optional[str] = None
        self.colours: List[str] = list()

    def overrideAttrs(self, copyColourIndex_attribute: _Optional_str = None, recolouringAllowed_attribute: _Optional_str = None, values_attribute: _Optional_str = None) -> None:
        if copyColourIndex_attribute is not None:
            self._XMLID_ATTR_COPYCOLOURINDEX_ATTRIBUTE = copyColourIndex_attribute
        if recolouringAllowed_attribute is not None:
            self._XMLID_ATTR_RECOLOURINGALLOWED_ATTRIBUTE = recolouringAllowed_attribute
        if values_attribute is not None:
            self._XMLID_ATTR_VALUES_ATTRIBUTE = values_attribute

    def overrideTags(self, colours_child: _Optional_str = None) -> None:
        if colours_child is not None:
            self._XMLID_TAG_COLOURS_CHILD = colours_child

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        value = e.get(self._XMLID_ATTR_RECOLOURINGALLOWED_ATTRIBUTE, None)
        if value is None:
            self.recolouringAllowed = None
        else:
            self.recolouringAllowed = XML2BOOL[value]
        value = e.get(self._XMLID_ATTR_COPYCOLOURINDEX_ATTRIBUTE, None)
        if value is None:
            self.copyColourIndex = None
        else:
            self.copyColourIndex = int(value)
        value = e.get(self._XMLID_ATTR_VALUES_ATTRIBUTE, None)
        if value is None:
            self.values = None
        else:
            self.values = str(value)
        for lc in e.iterfind(self._XMLID_TAG_COLOURS_CHILD):
            self.colours.append(lc.text)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        if self.recolouringAllowed is not None:
            e.attrib[self._XMLID_ATTR_RECOLOURINGALLOWED_ATTRIBUTE] = str(self.recolouringAllowed).lower()
        if self.copyColourIndex is not None:
            e.attrib[self._XMLID_ATTR_COPYCOLOURINDEX_ATTRIBUTE] = str(self.copyColourIndex)
        if self.values is not None:
            e.attrib[self._XMLID_ATTR_VALUES_ATTRIBUTE] = str(self.values)
        for lv in self.colours:
            etree.SubElement(e, self._XMLID_TAG_COLOURS_CHILD, {}).text = lv
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        if self.recolouringAllowed is not None:
            data['recolouringAllowed'] = bool(self.recolouringAllowed)
        if self.copyColourIndex is not None:
            data['copyColourIndex'] = int(self.copyColourIndex)
        if self.values is not None:
            data['values'] = self.values
        if self.colours is None or len(self.colours) > 0:
            data['colours'] = list()
        else:
            lc = list()
            if len(self.colours) > 0:
                for lv in lc:
                    lc.append(str(lv))
            data['colours'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        self.recolouringAllowed = bool(data['recolouringAllowed']) if 'recolouringAllowed' in data else None
        self.copyColourIndex = int(data['copyColourIndex']) if 'copyColourIndex' in data else None
        self.values = str(data['values']) if 'values' in data else None
        if (lv := self.colours) is not None:
            for le in lv:
                self.colours.append(str(le))
        else:
            raise KeyRequiredException(self, data, 'colours', 'colours')

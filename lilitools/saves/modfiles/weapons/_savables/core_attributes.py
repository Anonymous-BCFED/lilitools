#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from collections import OrderedDict
from typing import Any, Dict, List, Optional, Set
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.items.enums.item_tags import EItemTags
from lilitools.saves.items.enums.rarity import ERarity
from lilitools.saves.items.item_effect import ItemEffect
from lilitools.saves.modfiles.weapons._savables.colours import RawWeaponColours
from lilitools.saves.modfiles.weapons._savables.name import RawWeaponName
from lilitools.saves.modfiles.weapons._savables.plural_name import RawWeaponPluralName
from lilitools.saves.savable import XML2BOOL, Savable
__all__ = ['RawWeaponCoreAttributes']
## from [LT]/src/com/lilithsthrone/game/inventory/weapon/AbstractWeaponType.java: public AbstractWeaponType(File weaponXMLFile, String author, boolean mod) { @ RUP36sdnQ73d6klRmOstq3FVFj4NPI0anC1glCYhmMaueuUfNBvz9MgUwd8fYQ63b4O8aa4JKoR6qek4CknYjw==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawWeaponCoreAttributes(Savable):
    TAG = 'coreAttributes'
    _XMLID_ATTR_AOE_KEYATTR: str = 'chance'
    _XMLID_ATTR_NAMEPLURAL_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_NAME_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_PRIMARYCOLOURSDYE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_PRIMARYCOLOURS_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_SECONDARYCOLOURSDYE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_SECONDARYCOLOURS_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_TERTIARYCOLOURSDYE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_TERTIARYCOLOURS_ATTRIBUTE: str = 'value'
    _XMLID_TAG_AOE_CHILD: str = 'aoe'
    _XMLID_TAG_AOE_PARENT: Optional[str] = None
    _XMLID_TAG_ARCANECOST_ELEMENT: str = 'arcaneCost'
    _XMLID_TAG_ATTACKDESCRIPTIONPREFIX_ELEMENT: str = 'attackDescriptionPrefix'
    _XMLID_TAG_ATTACKDESCRIPTOR_ELEMENT: str = 'attackDescriptor'
    _XMLID_TAG_ATTACKTOOLTIPDESCRIPTION_ELEMENT: str = 'attackTooltipDescription'
    _XMLID_TAG_AUTHORTAG_ELEMENT: str = 'authorTag'
    _XMLID_TAG_AVAILABLEDAMAGETYPES_CHILD: str = 'damageType'
    _XMLID_TAG_AVAILABLEDAMAGETYPES_PARENT: str = 'availableDamageTypes'
    _XMLID_TAG_CUSTOMCOLOURS_CHILD: str = 'customColour'
    _XMLID_TAG_CUSTOMCOLOURS_PARENT: str = 'customColours'
    _XMLID_TAG_CUSTOMCOLOURS_VALUEELEM: Optional[str] = None
    _XMLID_TAG_DAMAGEVARIANCE_ELEMENT: str = 'damageVariance'
    _XMLID_TAG_DAMAGE_ELEMENT: str = 'damage'
    _XMLID_TAG_DESCRIPTION_ELEMENT: str = 'description'
    _XMLID_TAG_DETERMINER_ELEMENT: str = 'determiner'
    _XMLID_TAG_EFFECTS_PARENT: str = 'effects'
    _XMLID_TAG_EFFECTS_VALUEELEM: Optional[str] = None
    _XMLID_TAG_EQUIPTEXT_ELEMENT: str = 'equipText'
    _XMLID_TAG_EXTRAEFFECTS_CHILD: str = 'effect'
    _XMLID_TAG_EXTRAEFFECTS_PARENT: str = 'extraEffects'
    _XMLID_TAG_IMAGEEQUIPPEDNAME_CHILD: str = 'li'
    _XMLID_TAG_IMAGEEQUIPPEDNAME_PARENT: str = 'imageEquippedName'
    _XMLID_TAG_ITEMTAGS_CHILD: str = 'itemTag'
    _XMLID_TAG_ITEMTAGS_PARENT: str = 'itemTags'
    _XMLID_TAG_MELEE_ELEMENT: str = 'melee'
    _XMLID_TAG_NAMEPLURAL_ELEMENT: str = 'namePlural'
    _XMLID_TAG_NAME_ELEMENT: str = 'name'
    _XMLID_TAG_ONCRITICALHITEFFECT_ELEMENT: str = 'onCriticalHitEffect'
    _XMLID_TAG_ONESHOTWEAPONCHANCETORECOVERAFTERCOMBAT_ELEMENT: str = 'oneShotWeaponChanceToRecoverAfterCombat'
    _XMLID_TAG_ONESHOTWEAPONCHANCETORECOVERAFTERTURN_ELEMENT: str = 'oneShotWeaponChanceToRecoverAfterTurn'
    _XMLID_TAG_ONESHOTWEAPON_ELEMENT: str = 'oneShotWeapon'
    _XMLID_TAG_ONHITEFFECT_ELEMENT: str = 'onHitEffect'
    _XMLID_TAG_PHYSICALRESISTANCE_ELEMENT: str = 'physicalResistance'
    _XMLID_TAG_PRIMARYCOLOURSDYE_ELEMENT: str = 'primaryColoursDye'
    _XMLID_TAG_PRIMARYCOLOURS_ELEMENT: str = 'primaryColours'
    _XMLID_TAG_RARITY_ELEMENT: str = 'rarity'
    _XMLID_TAG_SECONDARYCOLOURSDYE_ELEMENT: str = 'secondaryColoursDye'
    _XMLID_TAG_SECONDARYCOLOURS_ELEMENT: str = 'secondaryColours'
    _XMLID_TAG_TERTIARYCOLOURSDYE_ELEMENT: str = 'tertiaryColoursDye'
    _XMLID_TAG_TERTIARYCOLOURS_ELEMENT: str = 'tertiaryColours'
    _XMLID_TAG_TWOHANDED_ELEMENT: str = 'twoHanded'
    _XMLID_TAG_UNEQUIPTEXT_ELEMENT: str = 'unequipText'
    _XMLID_TAG_VALUE_ELEMENT: str = 'value'
    _XMLID_TAG_WEAPONAUTHORTAG_ELEMENT: str = 'weaponAuthorTag'
    _XMLID_TAG_WEAPONSET_ELEMENT: str = 'weaponSet'

    def __init__(self) -> None:
        super().__init__()
        self.itemTags: Set[EItemTags] = set()
        self.value: int = 0  # Element
        self.melee: bool = False  # Element
        self.twoHanded: bool = False  # Element
        self.oneShotWeapon: Optional[bool] = None  # Element
        self.oneShotWeaponChanceToRecoverAfterTurn: Optional[float] = None  # Element
        self.oneShotWeaponChanceToRecoverAfterCombat: Optional[float] = None  # Element
        self.determiner: str = ''  # Element
        self.name: RawWeaponName = RawWeaponName()  # Element
        self.namePlural: RawWeaponPluralName = RawWeaponPluralName()  # Element
        self.description: str = ''  # Element
        self.attackDescriptor: str = ''  # Element
        self.attackDescriptionPrefix: Optional[str] = None  # Element
        self.attackTooltipDescription: str = ''  # Element
        self.weaponAuthorTag: Optional[str] = None  # Element
        self.authorTag: Optional[str] = None  # Element
        self.equipText: str = ''  # Element
        self.unequipText: str = ''  # Element
        self.imageEquippedName: List[str] = list()
        self.damage: int = 0  # Element
        self.arcaneCost: int = 0  # Element
        self.damageVariance: str = ''  # Element
        self.aoe: Dict[int, int] = {}
        self.availableDamageTypes: Set[str] = set()
        self.spells: OrderedDict[str, Set[str]] = OrderedDict()
        self.combatMoves: OrderedDict[str, Set[str]] = OrderedDict()
        self.weaponSet: Optional[str] = None  # Element
        self.effects: List[ItemEffect] = list()
        self.extraEffects: Optional[List[str]] = list()
        self.onHitEffect: Optional[str] = None  # Element
        self.onCriticalHitEffect: Optional[str] = None  # Element
        self.rarity: ERarity = ERarity(0)  # Element
        self.physicalResistance: float = 0.  # Element
        self.primaryColours: RawWeaponColours = RawWeaponColours()  # Element
        self.primaryColoursDye: RawWeaponColours = RawWeaponColours()  # Element
        self.secondaryColours: RawWeaponColours = RawWeaponColours()  # Element
        self.secondaryColoursDye: RawWeaponColours = RawWeaponColours()  # Element
        self.tertiaryColours: RawWeaponColours = RawWeaponColours()  # Element
        self.tertiaryColoursDye: RawWeaponColours = RawWeaponColours()  # Element
        self.customColours: List[RawWeaponColours] = list()

    def overrideAttrs(self, aoe_keyattr: _Optional_str = None, name_attribute: _Optional_str = None, namePlural_attribute: _Optional_str = None, primaryColours_attribute: _Optional_str = None, primaryColoursDye_attribute: _Optional_str = None, secondaryColours_attribute: _Optional_str = None, secondaryColoursDye_attribute: _Optional_str = None, tertiaryColours_attribute: _Optional_str = None, tertiaryColoursDye_attribute: _Optional_str = None) -> None:
        if aoe_keyattr is not None:
            self._XMLID_ATTR_AOE_KEYATTR = aoe_keyattr
        if name_attribute is not None:
            self._XMLID_ATTR_NAME_ATTRIBUTE = name_attribute
        if namePlural_attribute is not None:
            self._XMLID_ATTR_NAMEPLURAL_ATTRIBUTE = namePlural_attribute
        if primaryColours_attribute is not None:
            self._XMLID_ATTR_PRIMARYCOLOURS_ATTRIBUTE = primaryColours_attribute
        if primaryColoursDye_attribute is not None:
            self._XMLID_ATTR_PRIMARYCOLOURSDYE_ATTRIBUTE = primaryColoursDye_attribute
        if secondaryColours_attribute is not None:
            self._XMLID_ATTR_SECONDARYCOLOURS_ATTRIBUTE = secondaryColours_attribute
        if secondaryColoursDye_attribute is not None:
            self._XMLID_ATTR_SECONDARYCOLOURSDYE_ATTRIBUTE = secondaryColoursDye_attribute
        if tertiaryColours_attribute is not None:
            self._XMLID_ATTR_TERTIARYCOLOURS_ATTRIBUTE = tertiaryColours_attribute
        if tertiaryColoursDye_attribute is not None:
            self._XMLID_ATTR_TERTIARYCOLOURSDYE_ATTRIBUTE = tertiaryColoursDye_attribute

    def overrideTags(self, aoe_child: _Optional_str = None, aoe_parent: _Optional_str = None, arcaneCost_element: _Optional_str = None, attackDescriptionPrefix_element: _Optional_str = None, attackDescriptor_element: _Optional_str = None, attackTooltipDescription_element: _Optional_str = None, authorTag_element: _Optional_str = None, availableDamageTypes_child: _Optional_str = None, availableDamageTypes_parent: _Optional_str = None, customColours_child: _Optional_str = None, customColours_parent: _Optional_str = None, customColours_valueelem: _Optional_str = None, damage_element: _Optional_str = None, damageVariance_element: _Optional_str = None, description_element: _Optional_str = None, determiner_element: _Optional_str = None, effects_parent: _Optional_str = None, effects_valueelem: _Optional_str = None, equipText_element: _Optional_str = None, extraEffects_child: _Optional_str = None, extraEffects_parent: _Optional_str = None, imageEquippedName_child: _Optional_str = None, imageEquippedName_parent: _Optional_str = None, itemTags_child: _Optional_str = None, itemTags_parent: _Optional_str = None, melee_element: _Optional_str = None, name_element: _Optional_str = None, namePlural_element: _Optional_str = None, onCriticalHitEffect_element: _Optional_str = None, onHitEffect_element: _Optional_str = None, oneShotWeapon_element: _Optional_str = None, oneShotWeaponChanceToRecoverAfterCombat_element: _Optional_str = None, oneShotWeaponChanceToRecoverAfterTurn_element: _Optional_str = None, physicalResistance_element: _Optional_str = None, primaryColours_element: _Optional_str = None, primaryColoursDye_element: _Optional_str = None, rarity_element: _Optional_str = None, secondaryColours_element: _Optional_str = None, secondaryColoursDye_element: _Optional_str = None, tertiaryColours_element: _Optional_str = None, tertiaryColoursDye_element: _Optional_str = None, twoHanded_element: _Optional_str = None, unequipText_element: _Optional_str = None, value_element: _Optional_str = None, weaponAuthorTag_element: _Optional_str = None, weaponSet_element: _Optional_str = None) -> None:
        if aoe_child is not None:
            self._XMLID_TAG_AOE_CHILD = aoe_child
        if aoe_parent is not None:
            self._XMLID_TAG_AOE_PARENT = aoe_parent
        if arcaneCost_element is not None:
            self._XMLID_TAG_ARCANECOST_ELEMENT = arcaneCost_element
        if attackDescriptionPrefix_element is not None:
            self._XMLID_TAG_ATTACKDESCRIPTIONPREFIX_ELEMENT = attackDescriptionPrefix_element
        if attackDescriptor_element is not None:
            self._XMLID_TAG_ATTACKDESCRIPTOR_ELEMENT = attackDescriptor_element
        if attackTooltipDescription_element is not None:
            self._XMLID_TAG_ATTACKTOOLTIPDESCRIPTION_ELEMENT = attackTooltipDescription_element
        if authorTag_element is not None:
            self._XMLID_TAG_AUTHORTAG_ELEMENT = authorTag_element
        if availableDamageTypes_child is not None:
            self._XMLID_TAG_AVAILABLEDAMAGETYPES_CHILD = availableDamageTypes_child
        if availableDamageTypes_parent is not None:
            self._XMLID_TAG_AVAILABLEDAMAGETYPES_PARENT = availableDamageTypes_parent
        if customColours_child is not None:
            self._XMLID_TAG_CUSTOMCOLOURS_CHILD = customColours_child
        if customColours_parent is not None:
            self._XMLID_TAG_CUSTOMCOLOURS_PARENT = customColours_parent
        if customColours_valueelem is not None:
            self._XMLID_TAG_CUSTOMCOLOURS_VALUEELEM = customColours_valueelem
        if damage_element is not None:
            self._XMLID_TAG_DAMAGE_ELEMENT = damage_element
        if damageVariance_element is not None:
            self._XMLID_TAG_DAMAGEVARIANCE_ELEMENT = damageVariance_element
        if description_element is not None:
            self._XMLID_TAG_DESCRIPTION_ELEMENT = description_element
        if determiner_element is not None:
            self._XMLID_TAG_DETERMINER_ELEMENT = determiner_element
        if effects_parent is not None:
            self._XMLID_TAG_EFFECTS_PARENT = effects_parent
        if effects_valueelem is not None:
            self._XMLID_TAG_EFFECTS_VALUEELEM = effects_valueelem
        if equipText_element is not None:
            self._XMLID_TAG_EQUIPTEXT_ELEMENT = equipText_element
        if extraEffects_child is not None:
            self._XMLID_TAG_EXTRAEFFECTS_CHILD = extraEffects_child
        if extraEffects_parent is not None:
            self._XMLID_TAG_EXTRAEFFECTS_PARENT = extraEffects_parent
        if imageEquippedName_child is not None:
            self._XMLID_TAG_IMAGEEQUIPPEDNAME_CHILD = imageEquippedName_child
        if imageEquippedName_parent is not None:
            self._XMLID_TAG_IMAGEEQUIPPEDNAME_PARENT = imageEquippedName_parent
        if itemTags_child is not None:
            self._XMLID_TAG_ITEMTAGS_CHILD = itemTags_child
        if itemTags_parent is not None:
            self._XMLID_TAG_ITEMTAGS_PARENT = itemTags_parent
        if melee_element is not None:
            self._XMLID_TAG_MELEE_ELEMENT = melee_element
        if name_element is not None:
            self._XMLID_TAG_NAME_ELEMENT = name_element
        if namePlural_element is not None:
            self._XMLID_TAG_NAMEPLURAL_ELEMENT = namePlural_element
        if onCriticalHitEffect_element is not None:
            self._XMLID_TAG_ONCRITICALHITEFFECT_ELEMENT = onCriticalHitEffect_element
        if onHitEffect_element is not None:
            self._XMLID_TAG_ONHITEFFECT_ELEMENT = onHitEffect_element
        if oneShotWeapon_element is not None:
            self._XMLID_TAG_ONESHOTWEAPON_ELEMENT = oneShotWeapon_element
        if oneShotWeaponChanceToRecoverAfterCombat_element is not None:
            self._XMLID_TAG_ONESHOTWEAPONCHANCETORECOVERAFTERCOMBAT_ELEMENT = oneShotWeaponChanceToRecoverAfterCombat_element
        if oneShotWeaponChanceToRecoverAfterTurn_element is not None:
            self._XMLID_TAG_ONESHOTWEAPONCHANCETORECOVERAFTERTURN_ELEMENT = oneShotWeaponChanceToRecoverAfterTurn_element
        if physicalResistance_element is not None:
            self._XMLID_TAG_PHYSICALRESISTANCE_ELEMENT = physicalResistance_element
        if primaryColours_element is not None:
            self._XMLID_TAG_PRIMARYCOLOURS_ELEMENT = primaryColours_element
        if primaryColoursDye_element is not None:
            self._XMLID_TAG_PRIMARYCOLOURSDYE_ELEMENT = primaryColoursDye_element
        if rarity_element is not None:
            self._XMLID_TAG_RARITY_ELEMENT = rarity_element
        if secondaryColours_element is not None:
            self._XMLID_TAG_SECONDARYCOLOURS_ELEMENT = secondaryColours_element
        if secondaryColoursDye_element is not None:
            self._XMLID_TAG_SECONDARYCOLOURSDYE_ELEMENT = secondaryColoursDye_element
        if tertiaryColours_element is not None:
            self._XMLID_TAG_TERTIARYCOLOURS_ELEMENT = tertiaryColours_element
        if tertiaryColoursDye_element is not None:
            self._XMLID_TAG_TERTIARYCOLOURSDYE_ELEMENT = tertiaryColoursDye_element
        if twoHanded_element is not None:
            self._XMLID_TAG_TWOHANDED_ELEMENT = twoHanded_element
        if unequipText_element is not None:
            self._XMLID_TAG_UNEQUIPTEXT_ELEMENT = unequipText_element
        if value_element is not None:
            self._XMLID_TAG_VALUE_ELEMENT = value_element
        if weaponAuthorTag_element is not None:
            self._XMLID_TAG_WEAPONAUTHORTAG_ELEMENT = weaponAuthorTag_element
        if weaponSet_element is not None:
            self._XMLID_TAG_WEAPONSET_ELEMENT = weaponSet_element

    def _fromXML_spells(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromXML_spells()')

    def _toXML_spells(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toXML_spells()')

    def _fromDict_spells(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromDict_spells()')

    def _toDict_spells(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toDict_spells()')

    def _fromXML_combatMoves(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromXML_combatMoves()')

    def _toXML_combatMoves(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toXML_combatMoves()')

    def _fromDict_combatMoves(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromDict_combatMoves()')

    def _toDict_combatMoves(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toDict_combatMoves()')

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        dc: etree._Element
        lk: str
        lp: etree._Element
        lv: Savable
        sf: etree._Element
        if (lf := e.find(self._XMLID_TAG_ITEMTAGS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_ITEMTAGS_CHILD):
                    self.itemTags.add(EItemTags[lc.text])
        else:
            self.itemTags = set()
        if (sf := e.find(self._XMLID_TAG_VALUE_ELEMENT)) is not None:
            self.value = int(sf.text)
        else:
            raise ElementRequiredException(self, e, 'value', self._XMLID_TAG_VALUE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_MELEE_ELEMENT)) is not None:
            self.melee = XML2BOOL[sf.text]
        else:
            raise ElementRequiredException(self, e, 'melee', self._XMLID_TAG_MELEE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_TWOHANDED_ELEMENT)) is not None:
            self.twoHanded = XML2BOOL[sf.text]
        else:
            raise ElementRequiredException(self, e, 'twoHanded', self._XMLID_TAG_TWOHANDED_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_ONESHOTWEAPON_ELEMENT)) is not None:
            self.oneShotWeapon = XML2BOOL[sf.text]
        if (sf := e.find(self._XMLID_TAG_ONESHOTWEAPONCHANCETORECOVERAFTERTURN_ELEMENT)) is not None:
            self.oneShotWeaponChanceToRecoverAfterTurn = float(sf.text)
        if (sf := e.find(self._XMLID_TAG_ONESHOTWEAPONCHANCETORECOVERAFTERCOMBAT_ELEMENT)) is not None:
            self.oneShotWeaponChanceToRecoverAfterCombat = float(sf.text)
        if (sf := e.find(self._XMLID_TAG_DETERMINER_ELEMENT)) is not None:
            self.determiner = sf.text
        else:
            raise ElementRequiredException(self, e, 'determiner', self._XMLID_TAG_DETERMINER_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_NAME_ELEMENT)) is not None:
            self.name = RawWeaponName()
            self.name.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'name', self._XMLID_TAG_NAME_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_NAMEPLURAL_ELEMENT)) is not None:
            self.namePlural = RawWeaponPluralName()
            self.namePlural.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'namePlural', self._XMLID_TAG_NAMEPLURAL_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_DESCRIPTION_ELEMENT)) is not None:
            self.description = sf.text
        else:
            raise ElementRequiredException(self, e, 'description', self._XMLID_TAG_DESCRIPTION_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_ATTACKDESCRIPTOR_ELEMENT)) is not None:
            self.attackDescriptor = sf.text
        else:
            raise ElementRequiredException(self, e, 'attackDescriptor', self._XMLID_TAG_ATTACKDESCRIPTOR_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_ATTACKDESCRIPTIONPREFIX_ELEMENT)) is not None:
            self.attackDescriptionPrefix = sf.text
        if (sf := e.find(self._XMLID_TAG_ATTACKTOOLTIPDESCRIPTION_ELEMENT)) is not None:
            self.attackTooltipDescription = sf.text
        else:
            raise ElementRequiredException(self, e, 'attackTooltipDescription', self._XMLID_TAG_ATTACKTOOLTIPDESCRIPTION_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_WEAPONAUTHORTAG_ELEMENT)) is not None:
            self.weaponAuthorTag = sf.text
        if (sf := e.find(self._XMLID_TAG_AUTHORTAG_ELEMENT)) is not None:
            self.authorTag = sf.text
        if (sf := e.find(self._XMLID_TAG_EQUIPTEXT_ELEMENT)) is not None:
            self.equipText = sf.text
        else:
            raise ElementRequiredException(self, e, 'equipText', self._XMLID_TAG_EQUIPTEXT_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_UNEQUIPTEXT_ELEMENT)) is not None:
            self.unequipText = sf.text
        else:
            raise ElementRequiredException(self, e, 'unequipText', self._XMLID_TAG_UNEQUIPTEXT_ELEMENT)
        if (lf := e.find(self._XMLID_TAG_IMAGEEQUIPPEDNAME_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_IMAGEEQUIPPEDNAME_CHILD):
                    self.imageEquippedName.append(lc.text)
        else:
            self.imageEquippedName = list()
        if (sf := e.find(self._XMLID_TAG_DAMAGE_ELEMENT)) is not None:
            self.damage = int(sf.text)
        else:
            raise ElementRequiredException(self, e, 'damage', self._XMLID_TAG_DAMAGE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_ARCANECOST_ELEMENT)) is not None:
            self.arcaneCost = int(sf.text)
        else:
            raise ElementRequiredException(self, e, 'arcaneCost', self._XMLID_TAG_ARCANECOST_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_DAMAGEVARIANCE_ELEMENT)) is not None:
            self.damageVariance = sf.text
        else:
            raise ElementRequiredException(self, e, 'damageVariance', self._XMLID_TAG_DAMAGEVARIANCE_ELEMENT)
        if (df := e.find(self._XMLID_TAG_AOE_PARENT)) is not None:
            for dc in df.iterfind(self._XMLID_TAG_AOE_CHILD):
                self.aoe[int(dc.attrib['chance'])] = int(dc.text)
        else:
            self.aoe = {}
        if (lf := e.find(self._XMLID_TAG_AVAILABLEDAMAGETYPES_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_AVAILABLEDAMAGETYPES_CHILD):
                    self.availableDamageTypes.add(lc.text)
        else:
            self.availableDamageTypes = set()
        self._fromXML_spells(e)
        self._fromXML_combatMoves(e)
        if (sf := e.find(self._XMLID_TAG_WEAPONSET_ELEMENT)) is not None:
            self.weaponSet = sf.text
        if (lf := e.find(self._XMLID_TAG_EFFECTS_PARENT)) is not None:
            if lf is not None:
                for lc in lf:
                    lv = ItemEffect()
                    lv.fromXML(lc)
                    self.effects.append(lv)
        else:
            self.effects = list()
        if (lf := e.find(self._XMLID_TAG_EXTRAEFFECTS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_EXTRAEFFECTS_CHILD):
                    self.extraEffects.append(lc.text)
        if (sf := e.find(self._XMLID_TAG_ONHITEFFECT_ELEMENT)) is not None:
            self.onHitEffect = sf.text
        if (sf := e.find(self._XMLID_TAG_ONCRITICALHITEFFECT_ELEMENT)) is not None:
            self.onCriticalHitEffect = sf.text
        if (sf := e.find(self._XMLID_TAG_RARITY_ELEMENT)) is not None:
            self.rarity = ERarity[sf.text]
        else:
            raise ElementRequiredException(self, e, 'rarity', self._XMLID_TAG_RARITY_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_PHYSICALRESISTANCE_ELEMENT)) is not None:
            self.physicalResistance = float(sf.text)
        else:
            raise ElementRequiredException(self, e, 'physicalResistance', self._XMLID_TAG_PHYSICALRESISTANCE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_PRIMARYCOLOURS_ELEMENT)) is not None:
            self.primaryColours = RawWeaponColours()
            self.primaryColours.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'primaryColours', self._XMLID_TAG_PRIMARYCOLOURS_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_PRIMARYCOLOURSDYE_ELEMENT)) is not None:
            self.primaryColoursDye = RawWeaponColours()
            self.primaryColoursDye.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'primaryColoursDye', self._XMLID_TAG_PRIMARYCOLOURSDYE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_SECONDARYCOLOURS_ELEMENT)) is not None:
            self.secondaryColours = RawWeaponColours()
            self.secondaryColours.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'secondaryColours', self._XMLID_TAG_SECONDARYCOLOURS_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_SECONDARYCOLOURSDYE_ELEMENT)) is not None:
            self.secondaryColoursDye = RawWeaponColours()
            self.secondaryColoursDye.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'secondaryColoursDye', self._XMLID_TAG_SECONDARYCOLOURSDYE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_TERTIARYCOLOURS_ELEMENT)) is not None:
            self.tertiaryColours = RawWeaponColours()
            self.tertiaryColours.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'tertiaryColours', self._XMLID_TAG_TERTIARYCOLOURS_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_TERTIARYCOLOURSDYE_ELEMENT)) is not None:
            self.tertiaryColoursDye = RawWeaponColours()
            self.tertiaryColoursDye.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'tertiaryColoursDye', self._XMLID_TAG_TERTIARYCOLOURSDYE_ELEMENT)
        if (lf := e.find(self._XMLID_TAG_CUSTOMCOLOURS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_CUSTOMCOLOURS_CHILD):
                    lv = RawWeaponColours()
                    lv.fromXML(lc)
                    self.customColours.append(lv)
        else:
            self.customColours = list()

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        dK: Any
        dV: Any
        dc: etree._Element
        dp: etree._Element
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        lp = etree.SubElement(e, self._XMLID_TAG_ITEMTAGS_PARENT)
        for lv in sorted(self.itemTags, key=lambda x: x.name):
            etree.SubElement(lp, self._XMLID_TAG_ITEMTAGS_CHILD, {}).text = lv.name
        etree.SubElement(e, self._XMLID_TAG_VALUE_ELEMENT).text = str(self.value)
        etree.SubElement(e, self._XMLID_TAG_MELEE_ELEMENT).text = str(self.melee).lower()
        etree.SubElement(e, self._XMLID_TAG_TWOHANDED_ELEMENT).text = str(self.twoHanded).lower()
        if self.oneShotWeapon is not None:
            etree.SubElement(e, self._XMLID_TAG_ONESHOTWEAPON_ELEMENT).text = str(self.oneShotWeapon).lower()
        if self.oneShotWeaponChanceToRecoverAfterTurn is not None:
            etree.SubElement(e, self._XMLID_TAG_ONESHOTWEAPONCHANCETORECOVERAFTERTURN_ELEMENT).text = str(self.oneShotWeaponChanceToRecoverAfterTurn)
        if self.oneShotWeaponChanceToRecoverAfterCombat is not None:
            etree.SubElement(e, self._XMLID_TAG_ONESHOTWEAPONCHANCETORECOVERAFTERCOMBAT_ELEMENT).text = str(self.oneShotWeaponChanceToRecoverAfterCombat)
        etree.SubElement(e, self._XMLID_TAG_DETERMINER_ELEMENT).text = str(self.determiner)
        e.append(self.name.toXML(self._XMLID_TAG_NAME_ELEMENT))
        e.append(self.namePlural.toXML(self._XMLID_TAG_NAMEPLURAL_ELEMENT))
        etree.SubElement(e, self._XMLID_TAG_DESCRIPTION_ELEMENT).text = str(self.description)
        etree.SubElement(e, self._XMLID_TAG_ATTACKDESCRIPTOR_ELEMENT).text = str(self.attackDescriptor)
        if self.attackDescriptionPrefix is not None:
            etree.SubElement(e, self._XMLID_TAG_ATTACKDESCRIPTIONPREFIX_ELEMENT).text = str(self.attackDescriptionPrefix)
        etree.SubElement(e, self._XMLID_TAG_ATTACKTOOLTIPDESCRIPTION_ELEMENT).text = str(self.attackTooltipDescription)
        if self.weaponAuthorTag is not None:
            etree.SubElement(e, self._XMLID_TAG_WEAPONAUTHORTAG_ELEMENT).text = str(self.weaponAuthorTag)
        if self.authorTag is not None:
            etree.SubElement(e, self._XMLID_TAG_AUTHORTAG_ELEMENT).text = str(self.authorTag)
        etree.SubElement(e, self._XMLID_TAG_EQUIPTEXT_ELEMENT).text = str(self.equipText)
        etree.SubElement(e, self._XMLID_TAG_UNEQUIPTEXT_ELEMENT).text = str(self.unequipText)
        lp = etree.SubElement(e, self._XMLID_TAG_IMAGEEQUIPPEDNAME_PARENT)
        for lv in self.imageEquippedName:
            etree.SubElement(lp, self._XMLID_TAG_IMAGEEQUIPPEDNAME_CHILD, {}).text = lv
        etree.SubElement(e, self._XMLID_TAG_DAMAGE_ELEMENT).text = str(self.damage)
        etree.SubElement(e, self._XMLID_TAG_ARCANECOST_ELEMENT).text = str(self.arcaneCost)
        etree.SubElement(e, self._XMLID_TAG_DAMAGEVARIANCE_ELEMENT).text = str(self.damageVariance)
        dp = etree.SubElement(e, self._XMLID_TAG_AOE_PARENT)
        for dK, dV in sorted(self.aoe.items(), key=lambda t: t[0]):
            dc = etree.SubElement(dp, self._XMLID_TAG_AOE_CHILD)
            dc.attrib['chance'] = str(dK)
            dc.text = str(dV)
        lp = etree.SubElement(e, self._XMLID_TAG_AVAILABLEDAMAGETYPES_PARENT)
        for lv in sorted(self.availableDamageTypes, key=lambda t: t):
            etree.SubElement(lp, self._XMLID_TAG_AVAILABLEDAMAGETYPES_CHILD, {}).text = lv
        self._toXML_spells(e)
        self._toXML_combatMoves(e)
        if self.weaponSet is not None:
            etree.SubElement(e, self._XMLID_TAG_WEAPONSET_ELEMENT).text = str(self.weaponSet)
        lp = etree.SubElement(e, self._XMLID_TAG_EFFECTS_PARENT)
        for lv in self.effects:
            lp.append(lv.toXML())
        if self.extraEffects is not None and len(self.extraEffects) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_EXTRAEFFECTS_PARENT)
            for lv in self.extraEffects:
                etree.SubElement(lp, self._XMLID_TAG_EXTRAEFFECTS_CHILD, {}).text = lv
        if self.onHitEffect is not None:
            etree.SubElement(e, self._XMLID_TAG_ONHITEFFECT_ELEMENT).text = str(self.onHitEffect)
        if self.onCriticalHitEffect is not None:
            etree.SubElement(e, self._XMLID_TAG_ONCRITICALHITEFFECT_ELEMENT).text = str(self.onCriticalHitEffect)
        etree.SubElement(e, self._XMLID_TAG_RARITY_ELEMENT).text = self.rarity.name
        etree.SubElement(e, self._XMLID_TAG_PHYSICALRESISTANCE_ELEMENT).text = str(self.physicalResistance)
        e.append(self.primaryColours.toXML(self._XMLID_TAG_PRIMARYCOLOURS_ELEMENT))
        e.append(self.primaryColoursDye.toXML(self._XMLID_TAG_PRIMARYCOLOURSDYE_ELEMENT))
        e.append(self.secondaryColours.toXML(self._XMLID_TAG_SECONDARYCOLOURS_ELEMENT))
        e.append(self.secondaryColoursDye.toXML(self._XMLID_TAG_SECONDARYCOLOURSDYE_ELEMENT))
        e.append(self.tertiaryColours.toXML(self._XMLID_TAG_TERTIARYCOLOURS_ELEMENT))
        e.append(self.tertiaryColoursDye.toXML(self._XMLID_TAG_TERTIARYCOLOURSDYE_ELEMENT))
        lp = etree.SubElement(e, self._XMLID_TAG_CUSTOMCOLOURS_PARENT)
        for lv in self.customColours:
            lp.append(lv.toXML())
        return e

    def toDict(self) -> Dict[str, Any]:
        dK: Any
        dV: Any
        data: Dict[str, Any] = super().toDict()
        dp: Dict[str, Any]
        if self.itemTags is None or len(self.itemTags) > 0:
            data['itemTags'] = list()
        else:
            lc = list()
            if len(self.itemTags) > 0:
                for lv in sorted(lc, lambda x: x.name):
                    lc.append(lv.name)
            data['itemTags'] = lc
        data['value'] = int(self.value)
        data['melee'] = bool(self.melee)
        data['twoHanded'] = bool(self.twoHanded)
        if self.oneShotWeapon is not None:
            data['oneShotWeapon'] = bool(self.oneShotWeapon)
        if self.oneShotWeaponChanceToRecoverAfterTurn is not None:
            data['oneShotWeaponChanceToRecoverAfterTurn'] = float(self.oneShotWeaponChanceToRecoverAfterTurn)
        if self.oneShotWeaponChanceToRecoverAfterCombat is not None:
            data['oneShotWeaponChanceToRecoverAfterCombat'] = float(self.oneShotWeaponChanceToRecoverAfterCombat)
        data['determiner'] = str(self.determiner)
        data['name'] = self.name.toDict()
        data['namePlural'] = self.namePlural.toDict()
        data['description'] = str(self.description)
        data['attackDescriptor'] = str(self.attackDescriptor)
        if self.attackDescriptionPrefix is not None:
            data['attackDescriptionPrefix'] = str(self.attackDescriptionPrefix)
        data['attackTooltipDescription'] = str(self.attackTooltipDescription)
        if self.weaponAuthorTag is not None:
            data['weaponAuthorTag'] = str(self.weaponAuthorTag)
        if self.authorTag is not None:
            data['authorTag'] = str(self.authorTag)
        data['equipText'] = str(self.equipText)
        data['unequipText'] = str(self.unequipText)
        if self.imageEquippedName is None or len(self.imageEquippedName) > 0:
            data['imageEquippedName'] = list()
        else:
            lc = list()
            if len(self.imageEquippedName) > 0:
                for lv in lc:
                    lc.append(str(lv))
            data['imageEquippedName'] = lc
        data['damage'] = int(self.damage)
        data['arcaneCost'] = int(self.arcaneCost)
        data['damageVariance'] = str(self.damageVariance)
        dp = {}
        for dK, dV in sorted(self.aoe.items(), key=lambda t: t[0]):
            sk = dK
            sv = dV
            dp[sk] = sv
        data['aoe'] = dp
        if self.availableDamageTypes is None or len(self.availableDamageTypes) > 0:
            data['availableDamageTypes'] = list()
        else:
            lc = list()
            if len(self.availableDamageTypes) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['availableDamageTypes'] = lc
        self._toDict_spells(data)
        self._toDict_combatMoves(data)
        if self.weaponSet is not None:
            data['weaponSet'] = str(self.weaponSet)
        if self.effects is None or len(self.effects) > 0:
            data['effects'] = list()
        else:
            lc = list()
            if len(self.effects) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['effects'] = lc
        if self.extraEffects is None or len(self.extraEffects) > 0:
            data['extraEffects'] = list()
        else:
            lc = list()
            if len(self.extraEffects) > 0:
                for lv in lc:
                    lc.append(str(lv))
            data['extraEffects'] = lc
        if self.onHitEffect is not None:
            data['onHitEffect'] = str(self.onHitEffect)
        if self.onCriticalHitEffect is not None:
            data['onCriticalHitEffect'] = str(self.onCriticalHitEffect)
        data['rarity'] = self.rarity.name
        data['physicalResistance'] = float(self.physicalResistance)
        data['primaryColours'] = self.primaryColours.toDict()
        data['primaryColoursDye'] = self.primaryColoursDye.toDict()
        data['secondaryColours'] = self.secondaryColours.toDict()
        data['secondaryColoursDye'] = self.secondaryColoursDye.toDict()
        data['tertiaryColours'] = self.tertiaryColours.toDict()
        data['tertiaryColoursDye'] = self.tertiaryColoursDye.toDict()
        if self.customColours is None or len(self.customColours) > 0:
            data['customColours'] = list()
        else:
            lc = list()
            if len(self.customColours) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['customColours'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (lv := self.itemTags) is not None:
            for le in lv:
                self.itemTags.add(le.name)
        else:
            raise KeyRequiredException(self, data, 'itemTags', 'itemTags')
        if (sf := data.get('value')) is not None:
            self.value = data['value']
        else:
            raise KeyRequiredException(self, data, 'value', 'value')
        if (sf := data.get('melee')) is not None:
            self.melee = data['melee']
        else:
            raise KeyRequiredException(self, data, 'melee', 'melee')
        if (sf := data.get('twoHanded')) is not None:
            self.twoHanded = data['twoHanded']
        else:
            raise KeyRequiredException(self, data, 'twoHanded', 'twoHanded')
        if (sf := data.get('oneShotWeapon')) is not None:
            self.oneShotWeapon = data['oneShotWeapon']
        else:
            self.oneShotWeapon = None
        if (sf := data.get('oneShotWeaponChanceToRecoverAfterTurn')) is not None:
            self.oneShotWeaponChanceToRecoverAfterTurn = data['oneShotWeaponChanceToRecoverAfterTurn']
        else:
            self.oneShotWeaponChanceToRecoverAfterTurn = None
        if (sf := data.get('oneShotWeaponChanceToRecoverAfterCombat')) is not None:
            self.oneShotWeaponChanceToRecoverAfterCombat = data['oneShotWeaponChanceToRecoverAfterCombat']
        else:
            self.oneShotWeaponChanceToRecoverAfterCombat = None
        if (sf := data.get('determiner')) is not None:
            self.determiner = data['determiner']
        else:
            raise KeyRequiredException(self, data, 'determiner', 'determiner')
        if (sf := data.get('name')) is not None:
            self.name = RawWeaponName()
            self.name.fromDict(data['name'])
        else:
            raise KeyRequiredException(self, data, 'name', 'name')
        if (sf := data.get('namePlural')) is not None:
            self.namePlural = RawWeaponPluralName()
            self.namePlural.fromDict(data['namePlural'])
        else:
            raise KeyRequiredException(self, data, 'namePlural', 'namePlural')
        if (sf := data.get('description')) is not None:
            self.description = data['description']
        else:
            raise KeyRequiredException(self, data, 'description', 'description')
        if (sf := data.get('attackDescriptor')) is not None:
            self.attackDescriptor = data['attackDescriptor']
        else:
            raise KeyRequiredException(self, data, 'attackDescriptor', 'attackDescriptor')
        if (sf := data.get('attackDescriptionPrefix')) is not None:
            self.attackDescriptionPrefix = data['attackDescriptionPrefix']
        else:
            self.attackDescriptionPrefix = None
        if (sf := data.get('attackTooltipDescription')) is not None:
            self.attackTooltipDescription = data['attackTooltipDescription']
        else:
            raise KeyRequiredException(self, data, 'attackTooltipDescription', 'attackTooltipDescription')
        if (sf := data.get('weaponAuthorTag')) is not None:
            self.weaponAuthorTag = data['weaponAuthorTag']
        else:
            self.weaponAuthorTag = None
        if (sf := data.get('authorTag')) is not None:
            self.authorTag = data['authorTag']
        else:
            self.authorTag = None
        if (sf := data.get('equipText')) is not None:
            self.equipText = data['equipText']
        else:
            raise KeyRequiredException(self, data, 'equipText', 'equipText')
        if (sf := data.get('unequipText')) is not None:
            self.unequipText = data['unequipText']
        else:
            raise KeyRequiredException(self, data, 'unequipText', 'unequipText')
        if (lv := self.imageEquippedName) is not None:
            for le in lv:
                self.imageEquippedName.append(str(le))
        else:
            raise KeyRequiredException(self, data, 'imageEquippedName', 'imageEquippedName')
        if (sf := data.get('damage')) is not None:
            self.damage = data['damage']
        else:
            raise KeyRequiredException(self, data, 'damage', 'damage')
        if (sf := data.get('arcaneCost')) is not None:
            self.arcaneCost = data['arcaneCost']
        else:
            raise KeyRequiredException(self, data, 'arcaneCost', 'arcaneCost')
        if (sf := data.get('damageVariance')) is not None:
            self.damageVariance = data['damageVariance']
        else:
            raise KeyRequiredException(self, data, 'damageVariance', 'damageVariance')
        if (df := data.get('aoe')) is not None:
            for sk, sv in df.items():
                self.aoe[sk] = sv
        else:
            self.aoe = {}
        if (lv := self.availableDamageTypes) is not None:
            for le in lv:
                self.availableDamageTypes.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'availableDamageTypes', 'availableDamageTypes')
        self._fromDict_spells(data)
        self._fromDict_combatMoves(data)
        if (sf := data.get('weaponSet')) is not None:
            self.weaponSet = data['weaponSet']
        else:
            self.weaponSet = None
        if (lv := self.effects) is not None:
            for le in lv:
                self.effects.append(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'effects', 'effects')
        if (lv := self.extraEffects) is not None:
            for le in lv:
                self.extraEffects.append(str(le))
        else:
            self.extraEffects = list()
        if (sf := data.get('onHitEffect')) is not None:
            self.onHitEffect = data['onHitEffect']
        else:
            self.onHitEffect = None
        if (sf := data.get('onCriticalHitEffect')) is not None:
            self.onCriticalHitEffect = data['onCriticalHitEffect']
        else:
            self.onCriticalHitEffect = None
        if (sf := data.get('rarity')) is not None:
            self.rarity = data['rarity'].name
        else:
            raise KeyRequiredException(self, data, 'rarity', 'rarity')
        if (sf := data.get('physicalResistance')) is not None:
            self.physicalResistance = data['physicalResistance']
        else:
            raise KeyRequiredException(self, data, 'physicalResistance', 'physicalResistance')
        if (sf := data.get('primaryColours')) is not None:
            self.primaryColours = RawWeaponColours()
            self.primaryColours.fromDict(data['primaryColours'])
        else:
            raise KeyRequiredException(self, data, 'primaryColours', 'primaryColours')
        if (sf := data.get('primaryColoursDye')) is not None:
            self.primaryColoursDye = RawWeaponColours()
            self.primaryColoursDye.fromDict(data['primaryColoursDye'])
        else:
            raise KeyRequiredException(self, data, 'primaryColoursDye', 'primaryColoursDye')
        if (sf := data.get('secondaryColours')) is not None:
            self.secondaryColours = RawWeaponColours()
            self.secondaryColours.fromDict(data['secondaryColours'])
        else:
            raise KeyRequiredException(self, data, 'secondaryColours', 'secondaryColours')
        if (sf := data.get('secondaryColoursDye')) is not None:
            self.secondaryColoursDye = RawWeaponColours()
            self.secondaryColoursDye.fromDict(data['secondaryColoursDye'])
        else:
            raise KeyRequiredException(self, data, 'secondaryColoursDye', 'secondaryColoursDye')
        if (sf := data.get('tertiaryColours')) is not None:
            self.tertiaryColours = RawWeaponColours()
            self.tertiaryColours.fromDict(data['tertiaryColours'])
        else:
            raise KeyRequiredException(self, data, 'tertiaryColours', 'tertiaryColours')
        if (sf := data.get('tertiaryColoursDye')) is not None:
            self.tertiaryColoursDye = RawWeaponColours()
            self.tertiaryColoursDye.fromDict(data['tertiaryColoursDye'])
        else:
            raise KeyRequiredException(self, data, 'tertiaryColoursDye', 'tertiaryColoursDye')
        if (lv := self.customColours) is not None:
            for le in lv:
                self.customColours.append(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'customColours', 'customColours')

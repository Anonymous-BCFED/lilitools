#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL, Savable
__all__ = ['RawWeaponName']
## from [LT]/src/com/lilithsthrone/game/inventory/weapon/AbstractWeaponType.java: public AbstractWeaponType(File weaponXMLFile, String author, boolean mod) { @ RUP36sdnQ73d6klRmOstq3FVFj4NPI0anC1glCYhmMaueuUfNBvz9MgUwd8fYQ63b4O8aa4JKoR6qek4CknYjw==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawWeaponName(Savable):
    TAG = 'name'
    _XMLID_ATTR_APPENDDAMAGENAME_ATTRIBUTE: str = 'appendDamageName'

    def __init__(self) -> None:
        super().__init__()
        self.appendDamageName: bool = True
        self.text: str = ''  # CDATA

    def overrideAttrs(self, appendDamageName_attribute: _Optional_str = None) -> None:
        if appendDamageName_attribute is not None:
            self._XMLID_ATTR_APPENDDAMAGENAME_ATTRIBUTE = appendDamageName_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_APPENDDAMAGENAME_ATTRIBUTE, None)
        if value is None:
            self.appendDamageName = True
        else:
            self.appendDamageName = XML2BOOL[value]
        self.text = e.text

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        if self.appendDamageName != True:
            e.attrib[self._XMLID_ATTR_APPENDDAMAGENAME_ATTRIBUTE] = str(self.appendDamageName).lower()
        e.text = etree.CDATA(str(self.text))
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        if self.appendDamageName != True:
            data['appendDamageName'] = bool(self.appendDamageName)
        data['text'] = self.text
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        self.appendDamageName = bool(data['appendDamageName']) if 'appendDamageName' in data else None
        self.text = data['text']

#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional, Set
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.modfiles.weapons.core_attributes import WeaponCoreAttributes
from lilitools.saves.savable import Savable
__all__ = ['RawWeaponType']
## from [LT]/src/com/lilithsthrone/game/inventory/weapon/AbstractWeaponType.java: public AbstractWeaponType(File weaponXMLFile, String author, boolean mod) { @ RUP36sdnQ73d6klRmOstq3FVFj4NPI0anC1glCYhmMaueuUfNBvz9MgUwd8fYQ63b4O8aa4JKoR6qek4CknYjw==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawWeaponType(Savable):
    TAG = 'weapon'
    _XMLID_ATTR_COREATTRIBUTES_ATTRIBUTE: str = 'value'
    _XMLID_TAG_COREATTRIBUTES_ELEMENT: str = 'coreAttributes'
    _XMLID_TAG_HITCRITICALDESCRIPTIONS_CHILD: str = 'criticalHitText'
    _XMLID_TAG_HITCRITICALDESCRIPTIONS_PARENT: str = 'hitDescriptions'
    _XMLID_TAG_HITDESCRIPTIONS_CHILD: str = 'hitText'
    _XMLID_TAG_HITDESCRIPTIONS_PARENT: str = 'hitDescriptions'
    _XMLID_TAG_MISSDESCRIPTIONS_CHILD: str = 'missText'
    _XMLID_TAG_MISSDESCRIPTIONS_PARENT: str = 'missDescriptions'
    _XMLID_TAG_ONESHOTENDTURNRECOVERYDESCRIPTIONS_CHILD: str = 'recoveryText'
    _XMLID_TAG_ONESHOTENDTURNRECOVERYDESCRIPTIONS_PARENT: str = 'oneShotEndTurnRecoveryDescriptions'

    def __init__(self) -> None:
        super().__init__()
        self.coreAttributes: WeaponCoreAttributes = WeaponCoreAttributes()  # Element
        self.hitDescriptions: Optional[Set[str]] = set()
        self.hitCriticalDescriptions: Optional[Set[str]] = set()
        self.missDescriptions: Optional[Set[str]] = set()
        self.oneShotEndTurnRecoveryDescriptions: Optional[Set[str]] = set()

    def overrideAttrs(self, coreAttributes_attribute: _Optional_str = None) -> None:
        if coreAttributes_attribute is not None:
            self._XMLID_ATTR_COREATTRIBUTES_ATTRIBUTE = coreAttributes_attribute

    def overrideTags(self, coreAttributes_element: _Optional_str = None, hitCriticalDescriptions_child: _Optional_str = None, hitCriticalDescriptions_parent: _Optional_str = None, hitDescriptions_child: _Optional_str = None, hitDescriptions_parent: _Optional_str = None, missDescriptions_child: _Optional_str = None, missDescriptions_parent: _Optional_str = None, oneShotEndTurnRecoveryDescriptions_child: _Optional_str = None, oneShotEndTurnRecoveryDescriptions_parent: _Optional_str = None) -> None:
        if coreAttributes_element is not None:
            self._XMLID_TAG_COREATTRIBUTES_ELEMENT = coreAttributes_element
        if hitCriticalDescriptions_child is not None:
            self._XMLID_TAG_HITCRITICALDESCRIPTIONS_CHILD = hitCriticalDescriptions_child
        if hitCriticalDescriptions_parent is not None:
            self._XMLID_TAG_HITCRITICALDESCRIPTIONS_PARENT = hitCriticalDescriptions_parent
        if hitDescriptions_child is not None:
            self._XMLID_TAG_HITDESCRIPTIONS_CHILD = hitDescriptions_child
        if hitDescriptions_parent is not None:
            self._XMLID_TAG_HITDESCRIPTIONS_PARENT = hitDescriptions_parent
        if missDescriptions_child is not None:
            self._XMLID_TAG_MISSDESCRIPTIONS_CHILD = missDescriptions_child
        if missDescriptions_parent is not None:
            self._XMLID_TAG_MISSDESCRIPTIONS_PARENT = missDescriptions_parent
        if oneShotEndTurnRecoveryDescriptions_child is not None:
            self._XMLID_TAG_ONESHOTENDTURNRECOVERYDESCRIPTIONS_CHILD = oneShotEndTurnRecoveryDescriptions_child
        if oneShotEndTurnRecoveryDescriptions_parent is not None:
            self._XMLID_TAG_ONESHOTENDTURNRECOVERYDESCRIPTIONS_PARENT = oneShotEndTurnRecoveryDescriptions_parent

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        lp: etree._Element
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_COREATTRIBUTES_ELEMENT)) is not None:
            self.coreAttributes = WeaponCoreAttributes()
            self.coreAttributes.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'coreAttributes', self._XMLID_TAG_COREATTRIBUTES_ELEMENT)
        if (lf := e.find(self._XMLID_TAG_HITDESCRIPTIONS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_HITDESCRIPTIONS_CHILD):
                    self.hitDescriptions.add(lc.text)
        if (lf := e.find(self._XMLID_TAG_HITCRITICALDESCRIPTIONS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_HITCRITICALDESCRIPTIONS_CHILD):
                    self.hitCriticalDescriptions.add(lc.text)
        if (lf := e.find(self._XMLID_TAG_MISSDESCRIPTIONS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_MISSDESCRIPTIONS_CHILD):
                    self.missDescriptions.add(lc.text)
        if (lf := e.find(self._XMLID_TAG_ONESHOTENDTURNRECOVERYDESCRIPTIONS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_ONESHOTENDTURNRECOVERYDESCRIPTIONS_CHILD):
                    self.oneShotEndTurnRecoveryDescriptions.add(lc.text)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        e.append(self.coreAttributes.toXML(self._XMLID_TAG_COREATTRIBUTES_ELEMENT))
        if self.hitDescriptions is not None and len(self.hitDescriptions) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_HITDESCRIPTIONS_PARENT)
            for lv in sorted(self.hitDescriptions, key=lambda x: x):
                etree.SubElement(lp, self._XMLID_TAG_HITDESCRIPTIONS_CHILD, {}).text = lv
        if self.hitCriticalDescriptions is not None and len(self.hitCriticalDescriptions) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_HITCRITICALDESCRIPTIONS_PARENT)
            for lv in sorted(self.hitCriticalDescriptions, key=lambda x: x):
                etree.SubElement(lp, self._XMLID_TAG_HITCRITICALDESCRIPTIONS_CHILD, {}).text = lv
        if self.missDescriptions is not None and len(self.missDescriptions) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_MISSDESCRIPTIONS_PARENT)
            for lv in sorted(self.missDescriptions, key=lambda x: x):
                etree.SubElement(lp, self._XMLID_TAG_MISSDESCRIPTIONS_CHILD, {}).text = lv
        if self.oneShotEndTurnRecoveryDescriptions is not None and len(self.oneShotEndTurnRecoveryDescriptions) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_ONESHOTENDTURNRECOVERYDESCRIPTIONS_PARENT)
            for lv in sorted(self.oneShotEndTurnRecoveryDescriptions, key=lambda x: x):
                etree.SubElement(lp, self._XMLID_TAG_ONESHOTENDTURNRECOVERYDESCRIPTIONS_CHILD, {}).text = lv
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['coreAttributes'] = self.coreAttributes.toDict()
        if self.hitDescriptions is None or len(self.hitDescriptions) > 0:
            data['hitDescriptions'] = list()
        else:
            lc = list()
            if len(self.hitDescriptions) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['hitDescriptions'] = lc
        if self.hitCriticalDescriptions is None or len(self.hitCriticalDescriptions) > 0:
            data['hitCriticalDescriptions'] = list()
        else:
            lc = list()
            if len(self.hitCriticalDescriptions) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['hitCriticalDescriptions'] = lc
        if self.missDescriptions is None or len(self.missDescriptions) > 0:
            data['missDescriptions'] = list()
        else:
            lc = list()
            if len(self.missDescriptions) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['missDescriptions'] = lc
        if self.oneShotEndTurnRecoveryDescriptions is None or len(self.oneShotEndTurnRecoveryDescriptions) > 0:
            data['oneShotEndTurnRecoveryDescriptions'] = list()
        else:
            lc = list()
            if len(self.oneShotEndTurnRecoveryDescriptions) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['oneShotEndTurnRecoveryDescriptions'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('coreAttributes')) is not None:
            self.coreAttributes = WeaponCoreAttributes()
            self.coreAttributes.fromDict(data['coreAttributes'])
        else:
            raise KeyRequiredException(self, data, 'coreAttributes', 'coreAttributes')
        if (lv := self.hitDescriptions) is not None:
            for le in lv:
                self.hitDescriptions.add(str(le))
        else:
            self.hitDescriptions = set()
        if (lv := self.hitCriticalDescriptions) is not None:
            for le in lv:
                self.hitCriticalDescriptions.add(str(le))
        else:
            self.hitCriticalDescriptions = set()
        if (lv := self.missDescriptions) is not None:
            for le in lv:
                self.missDescriptions.add(str(le))
        else:
            self.missDescriptions = set()
        if (lv := self.oneShotEndTurnRecoveryDescriptions) is not None:
            for le in lv:
                self.oneShotEndTurnRecoveryDescriptions.add(str(le))
        else:
            self.oneShotEndTurnRecoveryDescriptions = set()

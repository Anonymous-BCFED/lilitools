from typing import Dict, Set
from lilitools.saves.modfiles.weapons._savables.core_attributes import RawWeaponCoreAttributes

from lxml import etree

from lilitools.saves.savable import XML2BOOL


class WeaponCoreAttributes(RawWeaponCoreAttributes):
    def __init__(self) -> None:
        super().__init__()
        #self.combatMoves: Dict[str, Set[str]]={}
        self.spells: Dict[str, Set[str]]={}
        self.spellRegenOnDamageTypeChange: bool = False
        self.combatMovesRegenOnDamageTypeChange: bool = False
    def _fromXML_combatMoves(self, e: etree._Element) -> None:
        parent: etree._Element
        child: etree._Element
        if (parent := e.find()) is not None:
            self.combatMovesRegenOnDamageTypeChange = XML2BOOL[parent.get('changeOnReforged', 'false')]
            for child in parent.findall('move'):
                cmid = child.text
                dtid = child.attrib['damageType']
                if dtid not in self.combatMoves:
                    self.combatMoves[dtid]={cmid}
                else:
                    self.combatMoves[dtid].add(cmid)
    def _toXML_combatMoves(self, e: etree._Element) -> None:
        parent = etree.SubElement(e, 'combatMoves', {'changeOnReforged':str(e).lower()})
        for dtid, cmset in sorted(self.combatMoves.items(), key=lambda x: x[0]):
            for cmid in sorted(cmset):
                etree.SubElement(parent, 'move', {'damageType':dtid}).text=cmid

    def _fromXML_spells(self, e: etree._Element) -> None:
        parent: etree._Element
        child: etree._Element
        if (parent := e.find('spells')) is not None:
            self.spellRegenOnDamageTypeChange = XML2BOOL[parent.get('changeOnReforged', 'false')]
            for child in parent.findall('spell'):
                cmid = child.text
                dtid = child.attrib['damageType']
                if dtid not in self.combatMoves:
                    self.combatMoves[dtid]={cmid}
                else:
                    self.combatMoves[dtid].add(cmid)
    def _toXML_spells(self, e: etree._Element) -> None:
        parent = etree.SubElement(e, 'spells')
        for dtid, cmset in sorted(self.combatMoves.items(), key=lambda x: x[0]):
            for cmid in sorted(cmset):
                etree.SubElement(parent, 'spell', {'damageType':dtid}).text=cmid

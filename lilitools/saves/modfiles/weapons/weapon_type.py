from lilitools.saves.modfiles.weapons._savables.weapon_type import RawWeaponType


class WeaponType(RawWeaponType):
    def getPhysicalResistance(self) -> float:
        return self.coreAttributes.physicalResistance

#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawAgePreference']
## from [LT]/src/com/lilithsthrone/game/Properties.java: public void savePropertiesAsXML(){ @ vpb28U7hw0K/E6Ue3TqojZCFStBmRattjkKbyRI8rYgcjjdVJ+yYLIspFiu/C9cokBXBliqbqlCIqMVKv/pIcg==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawAgePreference(Savable):
    TAG = 'preference'
    _XMLID_ATTR_AGE_ATTRIBUTE: str = 'age'
    _XMLID_ATTR_FEMININE_ATTRIBUTE: str = 'FEMININE'
    _XMLID_ATTR_MASCULINE_ATTRIBUTE: str = 'MASCULINE'
    _XMLID_ATTR_NEUTRAL_ATTRIBUTE: str = 'NEUTRAL'

    def __init__(self) -> None:
        super().__init__()
        self.age: str = ''
        self.FEMININE: int = 0
        self.NEUTRAL: int = 0
        self.MASCULINE: int = 0

    def overrideAttrs(self, FEMININE_attribute: _Optional_str = None, MASCULINE_attribute: _Optional_str = None, NEUTRAL_attribute: _Optional_str = None, age_attribute: _Optional_str = None) -> None:
        if FEMININE_attribute is not None:
            self._XMLID_ATTR_FEMININE_ATTRIBUTE = FEMININE_attribute
        if MASCULINE_attribute is not None:
            self._XMLID_ATTR_MASCULINE_ATTRIBUTE = MASCULINE_attribute
        if NEUTRAL_attribute is not None:
            self._XMLID_ATTR_NEUTRAL_ATTRIBUTE = NEUTRAL_attribute
        if age_attribute is not None:
            self._XMLID_ATTR_AGE_ATTRIBUTE = age_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_AGE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_AGE_ATTRIBUTE, 'age')
        else:
            self.age = str(value)
        value = e.get(self._XMLID_ATTR_FEMININE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_FEMININE_ATTRIBUTE, 'FEMININE')
        else:
            self.FEMININE = int(value)
        value = e.get(self._XMLID_ATTR_NEUTRAL_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_NEUTRAL_ATTRIBUTE, 'NEUTRAL')
        else:
            self.NEUTRAL = int(value)
        value = e.get(self._XMLID_ATTR_MASCULINE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_MASCULINE_ATTRIBUTE, 'MASCULINE')
        else:
            self.MASCULINE = int(value)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_AGE_ATTRIBUTE] = str(self.age)
        e.attrib[self._XMLID_ATTR_FEMININE_ATTRIBUTE] = str(self.FEMININE)
        e.attrib[self._XMLID_ATTR_NEUTRAL_ATTRIBUTE] = str(self.NEUTRAL)
        e.attrib[self._XMLID_ATTR_MASCULINE_ATTRIBUTE] = str(self.MASCULINE)
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['age'] = self.age
        data['FEMININE'] = int(self.FEMININE)
        data['NEUTRAL'] = int(self.NEUTRAL)
        data['MASCULINE'] = int(self.MASCULINE)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'age' not in data:
            raise KeyRequiredException(self, data, 'age', 'age')
        self.age = str(data['age'])
        if 'FEMININE' not in data:
            raise KeyRequiredException(self, data, 'FEMININE', 'FEMININE')
        self.FEMININE = int(data['FEMININE'])
        if 'NEUTRAL' not in data:
            raise KeyRequiredException(self, data, 'NEUTRAL', 'NEUTRAL')
        self.NEUTRAL = int(data['NEUTRAL'])
        if 'MASCULINE' not in data:
            raise KeyRequiredException(self, data, 'MASCULINE', 'MASCULINE')
        self.MASCULINE = int(data['MASCULINE'])

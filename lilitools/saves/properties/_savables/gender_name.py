#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawGenderName']
## from [LT]/src/com/lilithsthrone/game/Properties.java: public void savePropertiesAsXML(){ @ vpb28U7hw0K/E6Ue3TqojZCFStBmRattjkKbyRI8rYgcjjdVJ+yYLIspFiu/C9cokBXBliqbqlCIqMVKv/pIcg==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawGenderName(Savable):
    TAG = 'genderName'
    _XMLID_ATTR_FEMININEVALUE_ATTRIBUTE: str = 'feminineValue'
    _XMLID_ATTR_MASCULINEVALUE_ATTRIBUTE: str = 'masculineValue'
    _XMLID_ATTR_NAME_ATTRIBUTE: str = 'name'
    _XMLID_ATTR_NEUTRALVALUE_ATTRIBUTE: str = 'neutralValue'

    def __init__(self) -> None:
        super().__init__()
        self.name: str = ''
        self.feminineValue: str = ''
        self.masculineValue: str = ''
        self.neutralValue: str = ''

    def overrideAttrs(self, feminineValue_attribute: _Optional_str = None, masculineValue_attribute: _Optional_str = None, name_attribute: _Optional_str = None, neutralValue_attribute: _Optional_str = None) -> None:
        if feminineValue_attribute is not None:
            self._XMLID_ATTR_FEMININEVALUE_ATTRIBUTE = feminineValue_attribute
        if masculineValue_attribute is not None:
            self._XMLID_ATTR_MASCULINEVALUE_ATTRIBUTE = masculineValue_attribute
        if name_attribute is not None:
            self._XMLID_ATTR_NAME_ATTRIBUTE = name_attribute
        if neutralValue_attribute is not None:
            self._XMLID_ATTR_NEUTRALVALUE_ATTRIBUTE = neutralValue_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_NAME_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_NAME_ATTRIBUTE, 'name')
        else:
            self.name = str(value)
        value = e.get(self._XMLID_ATTR_FEMININEVALUE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_FEMININEVALUE_ATTRIBUTE, 'feminineValue')
        else:
            self.feminineValue = str(value)
        value = e.get(self._XMLID_ATTR_MASCULINEVALUE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_MASCULINEVALUE_ATTRIBUTE, 'masculineValue')
        else:
            self.masculineValue = str(value)
        value = e.get(self._XMLID_ATTR_NEUTRALVALUE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_NEUTRALVALUE_ATTRIBUTE, 'neutralValue')
        else:
            self.neutralValue = str(value)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_NAME_ATTRIBUTE] = str(self.name)
        e.attrib[self._XMLID_ATTR_FEMININEVALUE_ATTRIBUTE] = str(self.feminineValue)
        e.attrib[self._XMLID_ATTR_MASCULINEVALUE_ATTRIBUTE] = str(self.masculineValue)
        e.attrib[self._XMLID_ATTR_NEUTRALVALUE_ATTRIBUTE] = str(self.neutralValue)
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['name'] = self.name
        data['feminineValue'] = self.feminineValue
        data['masculineValue'] = self.masculineValue
        data['neutralValue'] = self.neutralValue
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'name' not in data:
            raise KeyRequiredException(self, data, 'name', 'name')
        self.name = str(data['name'])
        if 'feminineValue' not in data:
            raise KeyRequiredException(self, data, 'feminineValue', 'feminineValue')
        self.feminineValue = str(data['feminineValue'])
        if 'masculineValue' not in data:
            raise KeyRequiredException(self, data, 'masculineValue', 'masculineValue')
        self.masculineValue = str(data['masculineValue'])
        if 'neutralValue' not in data:
            raise KeyRequiredException(self, data, 'neutralValue', 'neutralValue')
        self.neutralValue = str(data['neutralValue'])

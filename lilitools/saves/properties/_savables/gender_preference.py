#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawGenderPreference']
## from [LT]/src/com/lilithsthrone/game/Properties.java: public void savePropertiesAsXML(){ @ vpb28U7hw0K/E6Ue3TqojZCFStBmRattjkKbyRI8rYgcjjdVJ+yYLIspFiu/C9cokBXBliqbqlCIqMVKv/pIcg==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawGenderPreference(Savable):
    TAG = 'preference'
    _XMLID_ATTR_GENDER_ATTRIBUTE: str = 'gender'
    _XMLID_ATTR_VALUE_ATTRIBUTE: str = 'value'

    def __init__(self) -> None:
        super().__init__()
        self.gender: str = ''
        self.value: int = 0

    def overrideAttrs(self, gender_attribute: _Optional_str = None, value_attribute: _Optional_str = None) -> None:
        if gender_attribute is not None:
            self._XMLID_ATTR_GENDER_ATTRIBUTE = gender_attribute
        if value_attribute is not None:
            self._XMLID_ATTR_VALUE_ATTRIBUTE = value_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_GENDER_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_GENDER_ATTRIBUTE, 'gender')
        else:
            self.gender = str(value)
        value = e.get(self._XMLID_ATTR_VALUE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_VALUE_ATTRIBUTE, 'value')
        else:
            self.value = int(value)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_GENDER_ATTRIBUTE] = str(self.gender)
        e.attrib[self._XMLID_ATTR_VALUE_ATTRIBUTE] = str(self.value)
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['gender'] = self.gender
        data['value'] = int(self.value)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'gender' not in data:
            raise KeyRequiredException(self, data, 'gender', 'gender')
        self.gender = str(data['gender'])
        if 'value' not in data:
            raise KeyRequiredException(self, data, 'value', 'value')
        self.value = int(data['value'])

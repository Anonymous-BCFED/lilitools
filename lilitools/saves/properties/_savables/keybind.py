#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawKeyBind']
## from [LT]/src/com/lilithsthrone/game/Properties.java: public void savePropertiesAsXML(){ @ vpb28U7hw0K/E6Ue3TqojZCFStBmRattjkKbyRI8rYgcjjdVJ+yYLIspFiu/C9cokBXBliqbqlCIqMVKv/pIcg==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawKeyBind(Savable):
    TAG = 'binding'
    _XMLID_ATTR_BINDNAME_ATTRIBUTE: str = 'bindName'
    _XMLID_ATTR_PRIMARYBIND_ATTRIBUTE: str = 'primaryBind'
    _XMLID_ATTR_SECONDARYBIND_ATTRIBUTE: str = 'secondaryBind'

    def __init__(self) -> None:
        super().__init__()
        self.bindName: str = ''
        self.primaryBind: str = ''
        self.secondaryBind: str = ''

    def overrideAttrs(self, bindName_attribute: _Optional_str = None, primaryBind_attribute: _Optional_str = None, secondaryBind_attribute: _Optional_str = None) -> None:
        if bindName_attribute is not None:
            self._XMLID_ATTR_BINDNAME_ATTRIBUTE = bindName_attribute
        if primaryBind_attribute is not None:
            self._XMLID_ATTR_PRIMARYBIND_ATTRIBUTE = primaryBind_attribute
        if secondaryBind_attribute is not None:
            self._XMLID_ATTR_SECONDARYBIND_ATTRIBUTE = secondaryBind_attribute

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        value = e.get(self._XMLID_ATTR_BINDNAME_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_BINDNAME_ATTRIBUTE, 'bindName')
        else:
            self.bindName = str(value)
        value = e.get(self._XMLID_ATTR_PRIMARYBIND_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_PRIMARYBIND_ATTRIBUTE, 'primaryBind')
        else:
            self.primaryBind = str(value)
        value = e.get(self._XMLID_ATTR_SECONDARYBIND_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_SECONDARYBIND_ATTRIBUTE, 'secondaryBind')
        else:
            self.secondaryBind = str(value)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_BINDNAME_ATTRIBUTE] = str(self.bindName)
        e.attrib[self._XMLID_ATTR_PRIMARYBIND_ATTRIBUTE] = str(self.primaryBind)
        e.attrib[self._XMLID_ATTR_SECONDARYBIND_ATTRIBUTE] = str(self.secondaryBind)
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['bindName'] = self.bindName
        data['primaryBind'] = self.primaryBind
        data['secondaryBind'] = self.secondaryBind
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'bindName' not in data:
            raise KeyRequiredException(self, data, 'bindName', 'bindName')
        self.bindName = str(data['bindName'])
        if 'primaryBind' not in data:
            raise KeyRequiredException(self, data, 'primaryBind', 'primaryBind')
        self.primaryBind = str(data['primaryBind'])
        if 'secondaryBind' not in data:
            raise KeyRequiredException(self, data, 'secondaryBind', 'secondaryBind')
        self.secondaryBind = str(data['secondaryBind'])

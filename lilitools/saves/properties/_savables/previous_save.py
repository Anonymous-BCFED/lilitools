#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawPreviousSaveInfo']
## from [LT]/src/com/lilithsthrone/game/Properties.java: public void savePropertiesAsXML(){ @ vpb28U7hw0K/E6Ue3TqojZCFStBmRattjkKbyRI8rYgcjjdVJ+yYLIspFiu/C9cokBXBliqbqlCIqMVKv/pIcg==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawPreviousSaveInfo(Savable):
    TAG = 'previousSave'
    _XMLID_ATTR_ARCANEESSENCES_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_LASTQUICKSAVENAME_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_LEVEL_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_LOCATION_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_MONEY_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_NAMECOLOUR_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_NAME_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_QUEST_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_RACE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_VERSIONNUMBER_ATTRIBUTE: str = 'value'
    _XMLID_TAG_ARCANEESSENCES_ELEMENT: str = 'arcaneEssences'
    _XMLID_TAG_LASTQUICKSAVENAME_ELEMENT: str = 'lastQuickSaveName'
    _XMLID_TAG_LEVEL_ELEMENT: str = 'level'
    _XMLID_TAG_LOCATION_ELEMENT: str = 'location'
    _XMLID_TAG_MONEY_ELEMENT: str = 'money'
    _XMLID_TAG_NAMECOLOUR_ELEMENT: str = 'nameColour'
    _XMLID_TAG_NAME_ELEMENT: str = 'name'
    _XMLID_TAG_QUEST_ELEMENT: str = 'quest'
    _XMLID_TAG_RACE_ELEMENT: str = 'race'
    _XMLID_TAG_VERSIONNUMBER_ELEMENT: str = 'versionNumber'

    def __init__(self) -> None:
        super().__init__()
        self.location: str = ''  # Element
        self.nameColour: str = ''  # Element
        self.name: str = ''  # Element
        self.race: str = ''  # Element
        self.quest: str = ''  # Element
        self.level: int = 0  # Element
        self.money: int = 0  # Element
        self.arcaneEssences: int = 0  # Element
        self.versionNumber: str = ''  # Element
        self.lastQuickSaveName: str = ''  # Element

    def overrideAttrs(self, arcaneEssences_attribute: _Optional_str = None, lastQuickSaveName_attribute: _Optional_str = None, level_attribute: _Optional_str = None, location_attribute: _Optional_str = None, money_attribute: _Optional_str = None, name_attribute: _Optional_str = None, nameColour_attribute: _Optional_str = None, quest_attribute: _Optional_str = None, race_attribute: _Optional_str = None, versionNumber_attribute: _Optional_str = None) -> None:
        if arcaneEssences_attribute is not None:
            self._XMLID_ATTR_ARCANEESSENCES_ATTRIBUTE = arcaneEssences_attribute
        if lastQuickSaveName_attribute is not None:
            self._XMLID_ATTR_LASTQUICKSAVENAME_ATTRIBUTE = lastQuickSaveName_attribute
        if level_attribute is not None:
            self._XMLID_ATTR_LEVEL_ATTRIBUTE = level_attribute
        if location_attribute is not None:
            self._XMLID_ATTR_LOCATION_ATTRIBUTE = location_attribute
        if money_attribute is not None:
            self._XMLID_ATTR_MONEY_ATTRIBUTE = money_attribute
        if name_attribute is not None:
            self._XMLID_ATTR_NAME_ATTRIBUTE = name_attribute
        if nameColour_attribute is not None:
            self._XMLID_ATTR_NAMECOLOUR_ATTRIBUTE = nameColour_attribute
        if quest_attribute is not None:
            self._XMLID_ATTR_QUEST_ATTRIBUTE = quest_attribute
        if race_attribute is not None:
            self._XMLID_ATTR_RACE_ATTRIBUTE = race_attribute
        if versionNumber_attribute is not None:
            self._XMLID_ATTR_VERSIONNUMBER_ATTRIBUTE = versionNumber_attribute

    def overrideTags(self, arcaneEssences_element: _Optional_str = None, lastQuickSaveName_element: _Optional_str = None, level_element: _Optional_str = None, location_element: _Optional_str = None, money_element: _Optional_str = None, name_element: _Optional_str = None, nameColour_element: _Optional_str = None, quest_element: _Optional_str = None, race_element: _Optional_str = None, versionNumber_element: _Optional_str = None) -> None:
        if arcaneEssences_element is not None:
            self._XMLID_TAG_ARCANEESSENCES_ELEMENT = arcaneEssences_element
        if lastQuickSaveName_element is not None:
            self._XMLID_TAG_LASTQUICKSAVENAME_ELEMENT = lastQuickSaveName_element
        if level_element is not None:
            self._XMLID_TAG_LEVEL_ELEMENT = level_element
        if location_element is not None:
            self._XMLID_TAG_LOCATION_ELEMENT = location_element
        if money_element is not None:
            self._XMLID_TAG_MONEY_ELEMENT = money_element
        if name_element is not None:
            self._XMLID_TAG_NAME_ELEMENT = name_element
        if nameColour_element is not None:
            self._XMLID_TAG_NAMECOLOUR_ELEMENT = nameColour_element
        if quest_element is not None:
            self._XMLID_TAG_QUEST_ELEMENT = quest_element
        if race_element is not None:
            self._XMLID_TAG_RACE_ELEMENT = race_element
        if versionNumber_element is not None:
            self._XMLID_TAG_VERSIONNUMBER_ELEMENT = versionNumber_element

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_LOCATION_ELEMENT)) is not None:
            if self._XMLID_ATTR_LOCATION_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_LOCATION_ATTRIBUTE, 'location')
            self.location = sf.attrib[self._XMLID_ATTR_LOCATION_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'location', self._XMLID_TAG_LOCATION_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_NAMECOLOUR_ELEMENT)) is not None:
            if self._XMLID_ATTR_NAMECOLOUR_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_NAMECOLOUR_ATTRIBUTE, 'nameColour')
            self.nameColour = sf.attrib[self._XMLID_ATTR_NAMECOLOUR_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'nameColour', self._XMLID_TAG_NAMECOLOUR_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_NAME_ELEMENT)) is not None:
            if self._XMLID_ATTR_NAME_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_NAME_ATTRIBUTE, 'name')
            self.name = sf.attrib[self._XMLID_ATTR_NAME_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'name', self._XMLID_TAG_NAME_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_RACE_ELEMENT)) is not None:
            if self._XMLID_ATTR_RACE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_RACE_ATTRIBUTE, 'race')
            self.race = sf.attrib[self._XMLID_ATTR_RACE_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'race', self._XMLID_TAG_RACE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_QUEST_ELEMENT)) is not None:
            if self._XMLID_ATTR_QUEST_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_QUEST_ATTRIBUTE, 'quest')
            self.quest = sf.attrib[self._XMLID_ATTR_QUEST_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'quest', self._XMLID_TAG_QUEST_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_LEVEL_ELEMENT)) is not None:
            if self._XMLID_ATTR_LEVEL_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_LEVEL_ATTRIBUTE, 'level')
            self.level = int(sf.attrib[self._XMLID_ATTR_LEVEL_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'level', self._XMLID_TAG_LEVEL_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_MONEY_ELEMENT)) is not None:
            if self._XMLID_ATTR_MONEY_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_MONEY_ATTRIBUTE, 'money')
            self.money = int(sf.attrib[self._XMLID_ATTR_MONEY_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'money', self._XMLID_TAG_MONEY_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_ARCANEESSENCES_ELEMENT)) is not None:
            if self._XMLID_ATTR_ARCANEESSENCES_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_ARCANEESSENCES_ATTRIBUTE, 'arcaneEssences')
            self.arcaneEssences = int(sf.attrib[self._XMLID_ATTR_ARCANEESSENCES_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'arcaneEssences', self._XMLID_TAG_ARCANEESSENCES_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_VERSIONNUMBER_ELEMENT)) is not None:
            if self._XMLID_ATTR_VERSIONNUMBER_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_VERSIONNUMBER_ATTRIBUTE, 'versionNumber')
            self.versionNumber = sf.attrib[self._XMLID_ATTR_VERSIONNUMBER_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'versionNumber', self._XMLID_TAG_VERSIONNUMBER_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_LASTQUICKSAVENAME_ELEMENT)) is not None:
            if self._XMLID_ATTR_LASTQUICKSAVENAME_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_LASTQUICKSAVENAME_ATTRIBUTE, 'lastQuickSaveName')
            self.lastQuickSaveName = sf.attrib[self._XMLID_ATTR_LASTQUICKSAVENAME_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'lastQuickSaveName', self._XMLID_TAG_LASTQUICKSAVENAME_ELEMENT)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        etree.SubElement(e, self._XMLID_TAG_LOCATION_ELEMENT, {self._XMLID_ATTR_LOCATION_ATTRIBUTE: str(self.location)})
        etree.SubElement(e, self._XMLID_TAG_NAMECOLOUR_ELEMENT, {self._XMLID_ATTR_NAMECOLOUR_ATTRIBUTE: str(self.nameColour)})
        etree.SubElement(e, self._XMLID_TAG_NAME_ELEMENT, {self._XMLID_ATTR_NAME_ATTRIBUTE: str(self.name)})
        etree.SubElement(e, self._XMLID_TAG_RACE_ELEMENT, {self._XMLID_ATTR_RACE_ATTRIBUTE: str(self.race)})
        etree.SubElement(e, self._XMLID_TAG_QUEST_ELEMENT, {self._XMLID_ATTR_QUEST_ATTRIBUTE: str(self.quest)})
        etree.SubElement(e, self._XMLID_TAG_LEVEL_ELEMENT, {self._XMLID_ATTR_LEVEL_ATTRIBUTE: str(self.level)})
        etree.SubElement(e, self._XMLID_TAG_MONEY_ELEMENT, {self._XMLID_ATTR_MONEY_ATTRIBUTE: str(self.money)})
        etree.SubElement(e, self._XMLID_TAG_ARCANEESSENCES_ELEMENT, {self._XMLID_ATTR_ARCANEESSENCES_ATTRIBUTE: str(self.arcaneEssences)})
        etree.SubElement(e, self._XMLID_TAG_VERSIONNUMBER_ELEMENT, {self._XMLID_ATTR_VERSIONNUMBER_ATTRIBUTE: str(self.versionNumber)})
        etree.SubElement(e, self._XMLID_TAG_LASTQUICKSAVENAME_ELEMENT, {self._XMLID_ATTR_LASTQUICKSAVENAME_ATTRIBUTE: str(self.lastQuickSaveName)})
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['location'] = str(self.location)
        data['nameColour'] = str(self.nameColour)
        data['name'] = str(self.name)
        data['race'] = str(self.race)
        data['quest'] = str(self.quest)
        data['level'] = int(self.level)
        data['money'] = int(self.money)
        data['arcaneEssences'] = int(self.arcaneEssences)
        data['versionNumber'] = str(self.versionNumber)
        data['lastQuickSaveName'] = str(self.lastQuickSaveName)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('location')) is not None:
            self.location = data['location']
        else:
            raise KeyRequiredException(self, data, 'location', 'location')
        if (sf := data.get('nameColour')) is not None:
            self.nameColour = data['nameColour']
        else:
            raise KeyRequiredException(self, data, 'nameColour', 'nameColour')
        if (sf := data.get('name')) is not None:
            self.name = data['name']
        else:
            raise KeyRequiredException(self, data, 'name', 'name')
        if (sf := data.get('race')) is not None:
            self.race = data['race']
        else:
            raise KeyRequiredException(self, data, 'race', 'race')
        if (sf := data.get('quest')) is not None:
            self.quest = data['quest']
        else:
            raise KeyRequiredException(self, data, 'quest', 'quest')
        if (sf := data.get('level')) is not None:
            self.level = data['level']
        else:
            raise KeyRequiredException(self, data, 'level', 'level')
        if (sf := data.get('money')) is not None:
            self.money = data['money']
        else:
            raise KeyRequiredException(self, data, 'money', 'money')
        if (sf := data.get('arcaneEssences')) is not None:
            self.arcaneEssences = data['arcaneEssences']
        else:
            raise KeyRequiredException(self, data, 'arcaneEssences', 'arcaneEssences')
        if (sf := data.get('versionNumber')) is not None:
            self.versionNumber = data['versionNumber']
        else:
            raise KeyRequiredException(self, data, 'versionNumber', 'versionNumber')
        if (sf := data.get('lastQuickSaveName')) is not None:
            self.lastQuickSaveName = data['lastQuickSaveName']
        else:
            raise KeyRequiredException(self, data, 'lastQuickSaveName', 'lastQuickSaveName')

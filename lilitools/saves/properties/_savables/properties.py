#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, List, Optional, Set
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.properties.age_preference import AgePreference
from lilitools.saves.properties.fetish_preference import FetishPreference
from lilitools.saves.properties.gender_name import GenderName
from lilitools.saves.properties.gender_preference import GenderPreference
from lilitools.saves.properties.gender_pronoun import GenderPronoun
from lilitools.saves.properties.keybind import KeyBind
from lilitools.saves.properties.orientation_preference import OrientationPreference
from lilitools.saves.properties.previous_save import PreviousSaveInfo
from lilitools.saves.properties.settings import GameSettings
from lilitools.saves.properties.skin_colour_preference import SkinColourPreference
from lilitools.saves.properties.subspecies_preference import SubspeciesPreference
from lilitools.saves.savable import Savable
__all__ = ['RawGameProperties']
## from [LT]/src/com/lilithsthrone/game/Properties.java: public void savePropertiesAsXML(){ @ vpb28U7hw0K/E6Ue3TqojZCFStBmRattjkKbyRI8rYgcjjdVJ+yYLIspFiu/C9cokBXBliqbqlCIqMVKv/pIcg==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawGameProperties(Savable):
    TAG = 'properties'
    _XMLID_ATTR_PREVIOUSSAVE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_SETTINGS_ATTRIBUTE: str = 'value'
    _XMLID_TAG_AGEPREFERENCES_CHILD: str = 'preference'
    _XMLID_TAG_AGEPREFERENCES_PARENT: str = 'agePreferences'
    _XMLID_TAG_AGEPREFERENCES_VALUEELEM: Optional[str] = None
    _XMLID_TAG_CLOTHINGDISCOVERED_CHILD: str = 'type'
    _XMLID_TAG_CLOTHINGDISCOVERED_PARENT: str = 'clothingDiscovered'
    _XMLID_TAG_FETISHPREFERENCES_CHILD: str = 'preference'
    _XMLID_TAG_FETISHPREFERENCES_PARENT: str = 'fetishPreferences'
    _XMLID_TAG_FETISHPREFERENCES_VALUEELEM: Optional[str] = None
    _XMLID_TAG_GENDERNAMES_CHILD: str = 'genderName'
    _XMLID_TAG_GENDERNAMES_PARENT: str = 'genderNames'
    _XMLID_TAG_GENDERNAMES_VALUEELEM: Optional[str] = None
    _XMLID_TAG_GENDERPREFERENCES_CHILD: str = 'preference'
    _XMLID_TAG_GENDERPREFERENCES_PARENT: str = 'genderPreferences'
    _XMLID_TAG_GENDERPREFERENCES_VALUEELEM: Optional[str] = None
    _XMLID_TAG_GENDERPRONOUNS_CHILD: str = 'pronoun'
    _XMLID_TAG_GENDERPRONOUNS_PARENT: str = 'genderPronouns'
    _XMLID_TAG_GENDERPRONOUNS_VALUEELEM: Optional[str] = None
    _XMLID_TAG_ITEMSDISCOVERED_CHILD: str = 'type'
    _XMLID_TAG_ITEMSDISCOVERED_PARENT: str = 'itemsDiscovered'
    _XMLID_TAG_KEYBINDS_CHILD: str = 'binding'
    _XMLID_TAG_KEYBINDS_PARENT: str = 'keyBinds'
    _XMLID_TAG_KEYBINDS_VALUEELEM: Optional[str] = None
    _XMLID_TAG_ORIENTATIONPREFERENCES_CHILD: str = 'preference'
    _XMLID_TAG_ORIENTATIONPREFERENCES_PARENT: str = 'orientationPreferences'
    _XMLID_TAG_ORIENTATIONPREFERENCES_VALUEELEM: Optional[str] = None
    _XMLID_TAG_PREVIOUSSAVE_ELEMENT: str = 'previousSave'
    _XMLID_TAG_PROPERTYVALUES_CHILD: str = 'propertyValue'
    _XMLID_TAG_PROPERTYVALUES_PARENT: str = 'propertyValues'
    _XMLID_TAG_RACESDISCOVEREDADVANCED_CHILD: str = 'type'
    _XMLID_TAG_RACESDISCOVEREDADVANCED_PARENT: str = 'racesDiscoveredAdvanced'
    _XMLID_TAG_RACESDISCOVERED_CHILD: str = 'type'
    _XMLID_TAG_RACESDISCOVERED_PARENT: str = 'racesDiscovered'
    _XMLID_TAG_SETTINGS_ELEMENT: str = 'settings'
    _XMLID_TAG_SKINCOLOURPREFERENCES_CHILD: str = 'preference'
    _XMLID_TAG_SKINCOLOURPREFERENCES_PARENT: str = 'skinColourPreferences'
    _XMLID_TAG_SKINCOLOURPREFERENCES_VALUEELEM: Optional[str] = None
    _XMLID_TAG_WEAPONSDISCOVERED_CHILD: str = 'type'
    _XMLID_TAG_WEAPONSDISCOVERED_PARENT: str = 'weaponsDiscovered'

    def __init__(self) -> None:
        super().__init__()
        self.previousSave: PreviousSaveInfo = PreviousSaveInfo()  # Element
        self.propertyValues: Set[str] = set()
        self.settings: GameSettings = GameSettings()  # Element
        self.keyBinds: List[KeyBind] = list()
        self.genderNames: List[GenderName] = list()
        self.genderPronouns: List[GenderPronoun] = list()
        self.genderPreferences: List[GenderPreference] = list()
        self.orientationPreferences: List[OrientationPreference] = list()
        self.fetishPreferences: List[FetishPreference] = list()
        self.agePreferences: List[AgePreference] = list()
        self.subspeciesPreferences: Dict[str, SubspeciesPreference] = {}
        self.skinColourPreferences: List[SkinColourPreference] = list()
        self.itemsDiscovered: List[str] = list()
        self.weaponsDiscovered: List[str] = list()
        self.clothingDiscovered: List[str] = list()
        self.racesDiscovered: List[str] = list()
        self.racesDiscoveredAdvanced: List[str] = list()

    def overrideAttrs(self, previousSave_attribute: _Optional_str = None, settings_attribute: _Optional_str = None) -> None:
        if previousSave_attribute is not None:
            self._XMLID_ATTR_PREVIOUSSAVE_ATTRIBUTE = previousSave_attribute
        if settings_attribute is not None:
            self._XMLID_ATTR_SETTINGS_ATTRIBUTE = settings_attribute

    def overrideTags(self, agePreferences_child: _Optional_str = None, agePreferences_parent: _Optional_str = None, agePreferences_valueelem: _Optional_str = None, clothingDiscovered_child: _Optional_str = None, clothingDiscovered_parent: _Optional_str = None, fetishPreferences_child: _Optional_str = None, fetishPreferences_parent: _Optional_str = None, fetishPreferences_valueelem: _Optional_str = None, genderNames_child: _Optional_str = None, genderNames_parent: _Optional_str = None, genderNames_valueelem: _Optional_str = None, genderPreferences_child: _Optional_str = None, genderPreferences_parent: _Optional_str = None, genderPreferences_valueelem: _Optional_str = None, genderPronouns_child: _Optional_str = None, genderPronouns_parent: _Optional_str = None, genderPronouns_valueelem: _Optional_str = None, itemsDiscovered_child: _Optional_str = None, itemsDiscovered_parent: _Optional_str = None, keyBinds_child: _Optional_str = None, keyBinds_parent: _Optional_str = None, keyBinds_valueelem: _Optional_str = None, orientationPreferences_child: _Optional_str = None, orientationPreferences_parent: _Optional_str = None, orientationPreferences_valueelem: _Optional_str = None, previousSave_element: _Optional_str = None, propertyValues_child: _Optional_str = None, propertyValues_parent: _Optional_str = None, racesDiscovered_child: _Optional_str = None, racesDiscovered_parent: _Optional_str = None, racesDiscoveredAdvanced_child: _Optional_str = None, racesDiscoveredAdvanced_parent: _Optional_str = None, settings_element: _Optional_str = None, skinColourPreferences_child: _Optional_str = None, skinColourPreferences_parent: _Optional_str = None, skinColourPreferences_valueelem: _Optional_str = None, weaponsDiscovered_child: _Optional_str = None, weaponsDiscovered_parent: _Optional_str = None) -> None:
        if agePreferences_child is not None:
            self._XMLID_TAG_AGEPREFERENCES_CHILD = agePreferences_child
        if agePreferences_parent is not None:
            self._XMLID_TAG_AGEPREFERENCES_PARENT = agePreferences_parent
        if agePreferences_valueelem is not None:
            self._XMLID_TAG_AGEPREFERENCES_VALUEELEM = agePreferences_valueelem
        if clothingDiscovered_child is not None:
            self._XMLID_TAG_CLOTHINGDISCOVERED_CHILD = clothingDiscovered_child
        if clothingDiscovered_parent is not None:
            self._XMLID_TAG_CLOTHINGDISCOVERED_PARENT = clothingDiscovered_parent
        if fetishPreferences_child is not None:
            self._XMLID_TAG_FETISHPREFERENCES_CHILD = fetishPreferences_child
        if fetishPreferences_parent is not None:
            self._XMLID_TAG_FETISHPREFERENCES_PARENT = fetishPreferences_parent
        if fetishPreferences_valueelem is not None:
            self._XMLID_TAG_FETISHPREFERENCES_VALUEELEM = fetishPreferences_valueelem
        if genderNames_child is not None:
            self._XMLID_TAG_GENDERNAMES_CHILD = genderNames_child
        if genderNames_parent is not None:
            self._XMLID_TAG_GENDERNAMES_PARENT = genderNames_parent
        if genderNames_valueelem is not None:
            self._XMLID_TAG_GENDERNAMES_VALUEELEM = genderNames_valueelem
        if genderPreferences_child is not None:
            self._XMLID_TAG_GENDERPREFERENCES_CHILD = genderPreferences_child
        if genderPreferences_parent is not None:
            self._XMLID_TAG_GENDERPREFERENCES_PARENT = genderPreferences_parent
        if genderPreferences_valueelem is not None:
            self._XMLID_TAG_GENDERPREFERENCES_VALUEELEM = genderPreferences_valueelem
        if genderPronouns_child is not None:
            self._XMLID_TAG_GENDERPRONOUNS_CHILD = genderPronouns_child
        if genderPronouns_parent is not None:
            self._XMLID_TAG_GENDERPRONOUNS_PARENT = genderPronouns_parent
        if genderPronouns_valueelem is not None:
            self._XMLID_TAG_GENDERPRONOUNS_VALUEELEM = genderPronouns_valueelem
        if itemsDiscovered_child is not None:
            self._XMLID_TAG_ITEMSDISCOVERED_CHILD = itemsDiscovered_child
        if itemsDiscovered_parent is not None:
            self._XMLID_TAG_ITEMSDISCOVERED_PARENT = itemsDiscovered_parent
        if keyBinds_child is not None:
            self._XMLID_TAG_KEYBINDS_CHILD = keyBinds_child
        if keyBinds_parent is not None:
            self._XMLID_TAG_KEYBINDS_PARENT = keyBinds_parent
        if keyBinds_valueelem is not None:
            self._XMLID_TAG_KEYBINDS_VALUEELEM = keyBinds_valueelem
        if orientationPreferences_child is not None:
            self._XMLID_TAG_ORIENTATIONPREFERENCES_CHILD = orientationPreferences_child
        if orientationPreferences_parent is not None:
            self._XMLID_TAG_ORIENTATIONPREFERENCES_PARENT = orientationPreferences_parent
        if orientationPreferences_valueelem is not None:
            self._XMLID_TAG_ORIENTATIONPREFERENCES_VALUEELEM = orientationPreferences_valueelem
        if previousSave_element is not None:
            self._XMLID_TAG_PREVIOUSSAVE_ELEMENT = previousSave_element
        if propertyValues_child is not None:
            self._XMLID_TAG_PROPERTYVALUES_CHILD = propertyValues_child
        if propertyValues_parent is not None:
            self._XMLID_TAG_PROPERTYVALUES_PARENT = propertyValues_parent
        if racesDiscovered_child is not None:
            self._XMLID_TAG_RACESDISCOVERED_CHILD = racesDiscovered_child
        if racesDiscovered_parent is not None:
            self._XMLID_TAG_RACESDISCOVERED_PARENT = racesDiscovered_parent
        if racesDiscoveredAdvanced_child is not None:
            self._XMLID_TAG_RACESDISCOVEREDADVANCED_CHILD = racesDiscoveredAdvanced_child
        if racesDiscoveredAdvanced_parent is not None:
            self._XMLID_TAG_RACESDISCOVEREDADVANCED_PARENT = racesDiscoveredAdvanced_parent
        if settings_element is not None:
            self._XMLID_TAG_SETTINGS_ELEMENT = settings_element
        if skinColourPreferences_child is not None:
            self._XMLID_TAG_SKINCOLOURPREFERENCES_CHILD = skinColourPreferences_child
        if skinColourPreferences_parent is not None:
            self._XMLID_TAG_SKINCOLOURPREFERENCES_PARENT = skinColourPreferences_parent
        if skinColourPreferences_valueelem is not None:
            self._XMLID_TAG_SKINCOLOURPREFERENCES_VALUEELEM = skinColourPreferences_valueelem
        if weaponsDiscovered_child is not None:
            self._XMLID_TAG_WEAPONSDISCOVERED_CHILD = weaponsDiscovered_child
        if weaponsDiscovered_parent is not None:
            self._XMLID_TAG_WEAPONSDISCOVERED_PARENT = weaponsDiscovered_parent

    def _fromXML_subspeciesPreferences(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromXML_subspeciesPreferences()')

    def _toXML_subspeciesPreferences(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toXML_subspeciesPreferences()')

    def _fromDict_subspeciesPreferences(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromDict_subspeciesPreferences()')

    def _toDict_subspeciesPreferences(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toDict_subspeciesPreferences()')

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        lv: Savable
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_PREVIOUSSAVE_ELEMENT)) is not None:
            self.previousSave = PreviousSaveInfo()
            self.previousSave.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'previousSave', self._XMLID_TAG_PREVIOUSSAVE_ELEMENT)
        if (lf := e.find(self._XMLID_TAG_PROPERTYVALUES_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_PROPERTYVALUES_CHILD):
                    self.propertyValues.add(lc.text)
        else:
            self.propertyValues = set()
        if (sf := e.find(self._XMLID_TAG_SETTINGS_ELEMENT)) is not None:
            self.settings = GameSettings()
            self.settings.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'settings', self._XMLID_TAG_SETTINGS_ELEMENT)
        if (lf := e.find(self._XMLID_TAG_KEYBINDS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_KEYBINDS_CHILD):
                    lv = KeyBind()
                    lv.fromXML(lc)
                    self.keyBinds.append(lv)
        else:
            self.keyBinds = list()
        if (lf := e.find(self._XMLID_TAG_GENDERNAMES_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_GENDERNAMES_CHILD):
                    lv = GenderName()
                    lv.fromXML(lc)
                    self.genderNames.append(lv)
        else:
            self.genderNames = list()
        if (lf := e.find(self._XMLID_TAG_GENDERPRONOUNS_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_GENDERPRONOUNS_CHILD):
                    lv = GenderPronoun()
                    lv.fromXML(lc)
                    self.genderPronouns.append(lv)
        else:
            self.genderPronouns = list()
        if (lf := e.find(self._XMLID_TAG_GENDERPREFERENCES_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_GENDERPREFERENCES_CHILD):
                    lv = GenderPreference()
                    lv.fromXML(lc)
                    self.genderPreferences.append(lv)
        else:
            self.genderPreferences = list()
        if (lf := e.find(self._XMLID_TAG_ORIENTATIONPREFERENCES_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_ORIENTATIONPREFERENCES_CHILD):
                    lv = OrientationPreference()
                    lv.fromXML(lc)
                    self.orientationPreferences.append(lv)
        else:
            self.orientationPreferences = list()
        if (lf := e.find(self._XMLID_TAG_FETISHPREFERENCES_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_FETISHPREFERENCES_CHILD):
                    lv = FetishPreference()
                    lv.fromXML(lc)
                    self.fetishPreferences.append(lv)
        else:
            self.fetishPreferences = list()
        if (lf := e.find(self._XMLID_TAG_AGEPREFERENCES_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_AGEPREFERENCES_CHILD):
                    lv = AgePreference()
                    lv.fromXML(lc)
                    self.agePreferences.append(lv)
        else:
            self.agePreferences = list()
        self._fromXML_subspeciesPreferences(e)
        if (lf := e.find(self._XMLID_TAG_SKINCOLOURPREFERENCES_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_SKINCOLOURPREFERENCES_CHILD):
                    lv = SkinColourPreference()
                    lv.fromXML(lc)
                    self.skinColourPreferences.append(lv)
        else:
            self.skinColourPreferences = list()
        if (lf := e.find(self._XMLID_TAG_ITEMSDISCOVERED_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_ITEMSDISCOVERED_CHILD):
                    self.itemsDiscovered.append(lc.text)
        else:
            self.itemsDiscovered = list()
        if (lf := e.find(self._XMLID_TAG_WEAPONSDISCOVERED_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_WEAPONSDISCOVERED_CHILD):
                    self.weaponsDiscovered.append(lc.text)
        else:
            self.weaponsDiscovered = list()
        if (lf := e.find(self._XMLID_TAG_CLOTHINGDISCOVERED_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_CLOTHINGDISCOVERED_CHILD):
                    self.clothingDiscovered.append(lc.text)
        else:
            self.clothingDiscovered = list()
        if (lf := e.find(self._XMLID_TAG_RACESDISCOVERED_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_RACESDISCOVERED_CHILD):
                    self.racesDiscovered.append(lc.text)
        else:
            self.racesDiscovered = list()
        if (lf := e.find(self._XMLID_TAG_RACESDISCOVEREDADVANCED_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_RACESDISCOVEREDADVANCED_CHILD):
                    self.racesDiscoveredAdvanced.append(lc.text)
        else:
            self.racesDiscoveredAdvanced = list()

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        e.append(self.previousSave.toXML(self._XMLID_TAG_PREVIOUSSAVE_ELEMENT))
        lp = etree.SubElement(e, self._XMLID_TAG_PROPERTYVALUES_PARENT)
        for lv in sorted(self.propertyValues, key=lambda t: t):
            etree.SubElement(lp, self._XMLID_TAG_PROPERTYVALUES_CHILD, {}).text = lv
        e.append(self.settings.toXML(self._XMLID_TAG_SETTINGS_ELEMENT))
        lp = etree.SubElement(e, self._XMLID_TAG_KEYBINDS_PARENT)
        for lv in self.keyBinds:
            lp.append(lv.toXML())
        lp = etree.SubElement(e, self._XMLID_TAG_GENDERNAMES_PARENT)
        for lv in self.genderNames:
            lp.append(lv.toXML())
        lp = etree.SubElement(e, self._XMLID_TAG_GENDERPRONOUNS_PARENT)
        for lv in self.genderPronouns:
            lp.append(lv.toXML())
        lp = etree.SubElement(e, self._XMLID_TAG_GENDERPREFERENCES_PARENT)
        for lv in self.genderPreferences:
            lp.append(lv.toXML())
        lp = etree.SubElement(e, self._XMLID_TAG_ORIENTATIONPREFERENCES_PARENT)
        for lv in self.orientationPreferences:
            lp.append(lv.toXML())
        lp = etree.SubElement(e, self._XMLID_TAG_FETISHPREFERENCES_PARENT)
        for lv in self.fetishPreferences:
            lp.append(lv.toXML())
        lp = etree.SubElement(e, self._XMLID_TAG_AGEPREFERENCES_PARENT)
        for lv in self.agePreferences:
            lp.append(lv.toXML())
        self._toXML_subspeciesPreferences(e)
        lp = etree.SubElement(e, self._XMLID_TAG_SKINCOLOURPREFERENCES_PARENT)
        for lv in self.skinColourPreferences:
            lp.append(lv.toXML())
        lp = etree.SubElement(e, self._XMLID_TAG_ITEMSDISCOVERED_PARENT)
        for lv in self.itemsDiscovered:
            etree.SubElement(lp, self._XMLID_TAG_ITEMSDISCOVERED_CHILD, {}).text = lv
        lp = etree.SubElement(e, self._XMLID_TAG_WEAPONSDISCOVERED_PARENT)
        for lv in self.weaponsDiscovered:
            etree.SubElement(lp, self._XMLID_TAG_WEAPONSDISCOVERED_CHILD, {}).text = lv
        lp = etree.SubElement(e, self._XMLID_TAG_CLOTHINGDISCOVERED_PARENT)
        for lv in self.clothingDiscovered:
            etree.SubElement(lp, self._XMLID_TAG_CLOTHINGDISCOVERED_CHILD, {}).text = lv
        lp = etree.SubElement(e, self._XMLID_TAG_RACESDISCOVERED_PARENT)
        for lv in self.racesDiscovered:
            etree.SubElement(lp, self._XMLID_TAG_RACESDISCOVERED_CHILD, {}).text = lv
        lp = etree.SubElement(e, self._XMLID_TAG_RACESDISCOVEREDADVANCED_PARENT)
        for lv in self.racesDiscoveredAdvanced:
            etree.SubElement(lp, self._XMLID_TAG_RACESDISCOVEREDADVANCED_CHILD, {}).text = lv
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['previousSave'] = self.previousSave.toDict()
        if self.propertyValues is None or len(self.propertyValues) > 0:
            data['propertyValues'] = list()
        else:
            lc = list()
            if len(self.propertyValues) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['propertyValues'] = lc
        data['settings'] = self.settings.toDict()
        if self.keyBinds is None or len(self.keyBinds) > 0:
            data['keyBinds'] = list()
        else:
            lc = list()
            if len(self.keyBinds) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['keyBinds'] = lc
        if self.genderNames is None or len(self.genderNames) > 0:
            data['genderNames'] = list()
        else:
            lc = list()
            if len(self.genderNames) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['genderNames'] = lc
        if self.genderPronouns is None or len(self.genderPronouns) > 0:
            data['genderPronouns'] = list()
        else:
            lc = list()
            if len(self.genderPronouns) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['genderPronouns'] = lc
        if self.genderPreferences is None or len(self.genderPreferences) > 0:
            data['genderPreferences'] = list()
        else:
            lc = list()
            if len(self.genderPreferences) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['genderPreferences'] = lc
        if self.orientationPreferences is None or len(self.orientationPreferences) > 0:
            data['orientationPreferences'] = list()
        else:
            lc = list()
            if len(self.orientationPreferences) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['orientationPreferences'] = lc
        if self.fetishPreferences is None or len(self.fetishPreferences) > 0:
            data['fetishPreferences'] = list()
        else:
            lc = list()
            if len(self.fetishPreferences) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['fetishPreferences'] = lc
        if self.agePreferences is None or len(self.agePreferences) > 0:
            data['agePreferences'] = list()
        else:
            lc = list()
            if len(self.agePreferences) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['agePreferences'] = lc
        self._toDict_subspeciesPreferences(data)
        if self.skinColourPreferences is None or len(self.skinColourPreferences) > 0:
            data['skinColourPreferences'] = list()
        else:
            lc = list()
            if len(self.skinColourPreferences) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['skinColourPreferences'] = lc
        if self.itemsDiscovered is None or len(self.itemsDiscovered) > 0:
            data['itemsDiscovered'] = list()
        else:
            lc = list()
            if len(self.itemsDiscovered) > 0:
                for lv in lc:
                    lc.append(str(lv))
            data['itemsDiscovered'] = lc
        if self.weaponsDiscovered is None or len(self.weaponsDiscovered) > 0:
            data['weaponsDiscovered'] = list()
        else:
            lc = list()
            if len(self.weaponsDiscovered) > 0:
                for lv in lc:
                    lc.append(str(lv))
            data['weaponsDiscovered'] = lc
        if self.clothingDiscovered is None or len(self.clothingDiscovered) > 0:
            data['clothingDiscovered'] = list()
        else:
            lc = list()
            if len(self.clothingDiscovered) > 0:
                for lv in lc:
                    lc.append(str(lv))
            data['clothingDiscovered'] = lc
        if self.racesDiscovered is None or len(self.racesDiscovered) > 0:
            data['racesDiscovered'] = list()
        else:
            lc = list()
            if len(self.racesDiscovered) > 0:
                for lv in lc:
                    lc.append(str(lv))
            data['racesDiscovered'] = lc
        if self.racesDiscoveredAdvanced is None or len(self.racesDiscoveredAdvanced) > 0:
            data['racesDiscoveredAdvanced'] = list()
        else:
            lc = list()
            if len(self.racesDiscoveredAdvanced) > 0:
                for lv in lc:
                    lc.append(str(lv))
            data['racesDiscoveredAdvanced'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('previousSave')) is not None:
            self.previousSave = PreviousSaveInfo()
            self.previousSave.fromDict(data['previousSave'])
        else:
            raise KeyRequiredException(self, data, 'previousSave', 'previousSave')
        if (lv := self.propertyValues) is not None:
            for le in lv:
                self.propertyValues.add(str(le))
        else:
            raise KeyRequiredException(self, data, 'propertyValues', 'propertyValues')
        if (sf := data.get('settings')) is not None:
            self.settings = GameSettings()
            self.settings.fromDict(data['settings'])
        else:
            raise KeyRequiredException(self, data, 'settings', 'settings')
        if (lv := self.keyBinds) is not None:
            for le in lv:
                self.keyBinds.append(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'keyBinds', 'keyBinds')
        if (lv := self.genderNames) is not None:
            for le in lv:
                self.genderNames.append(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'genderNames', 'genderNames')
        if (lv := self.genderPronouns) is not None:
            for le in lv:
                self.genderPronouns.append(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'genderPronouns', 'genderPronouns')
        if (lv := self.genderPreferences) is not None:
            for le in lv:
                self.genderPreferences.append(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'genderPreferences', 'genderPreferences')
        if (lv := self.orientationPreferences) is not None:
            for le in lv:
                self.orientationPreferences.append(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'orientationPreferences', 'orientationPreferences')
        if (lv := self.fetishPreferences) is not None:
            for le in lv:
                self.fetishPreferences.append(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'fetishPreferences', 'fetishPreferences')
        if (lv := self.agePreferences) is not None:
            for le in lv:
                self.agePreferences.append(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'agePreferences', 'agePreferences')
        self._fromDict_subspeciesPreferences(data)
        if (lv := self.skinColourPreferences) is not None:
            for le in lv:
                self.skinColourPreferences.append(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'skinColourPreferences', 'skinColourPreferences')
        if (lv := self.itemsDiscovered) is not None:
            for le in lv:
                self.itemsDiscovered.append(str(le))
        else:
            raise KeyRequiredException(self, data, 'itemsDiscovered', 'itemsDiscovered')
        if (lv := self.weaponsDiscovered) is not None:
            for le in lv:
                self.weaponsDiscovered.append(str(le))
        else:
            raise KeyRequiredException(self, data, 'weaponsDiscovered', 'weaponsDiscovered')
        if (lv := self.clothingDiscovered) is not None:
            for le in lv:
                self.clothingDiscovered.append(str(le))
        else:
            raise KeyRequiredException(self, data, 'clothingDiscovered', 'clothingDiscovered')
        if (lv := self.racesDiscovered) is not None:
            for le in lv:
                self.racesDiscovered.append(str(le))
        else:
            raise KeyRequiredException(self, data, 'racesDiscovered', 'racesDiscovered')
        if (lv := self.racesDiscoveredAdvanced) is not None:
            for le in lv:
                self.racesDiscoveredAdvanced.append(str(le))
        else:
            raise KeyRequiredException(self, data, 'racesDiscoveredAdvanced', 'racesDiscoveredAdvanced')

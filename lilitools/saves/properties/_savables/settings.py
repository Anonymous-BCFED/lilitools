#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.character.enums.androgynous_identification import EAndrogynousIdentification
from lilitools.saves.enums.difficulty_level import EDifficultyLevel
from lilitools.saves.enums.forced_fetish_tendency import EForcedFetishTendency
from lilitools.saves.enums.forced_tf_tendency import EForcedTFTendency
from lilitools.saves.enums.furry_preference import EFurryPreference
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawGameSettings']
## from [LT]/src/com/lilithsthrone/game/Properties.java: public void savePropertiesAsXML(){ @ vpb28U7hw0K/E6Ue3TqojZCFStBmRattjkKbyRI8rYgcjjdVJ+yYLIspFiu/C9cokBXBliqbqlCIqMVKv/pIcg==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawGameSettings(Savable):
    TAG = 'settings'
    _XMLID_ATTR_AIBLUNDERRATE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_ANDROGYNOUSIDENTIFICATION_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_AUTOSAVEFREQUENCY_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_BADENDTITLE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_BREASTSIZEPREFERENCE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_BYPASSSEXACTIONS_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_CLOTHINGFEMININITYLEVEL_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_DIFFICULTYLEVEL_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_FONTSIZE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_FORCEDFETISHPERCENTAGE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_FORCEDFETISHTENDENCY_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_FORCEDTFPERCENTAGE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_FORCEDTFPREFERENCE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_FORCEDTFTENDENCY_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_FULLEXPOSUREDESCRIPTIONS_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_HALFDEMONSPAWNRATE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_HUMANSPAWNRATE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_MULTIBREASTS_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_PENISSIZEPREFERENCE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_PREFERREDARTIST_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_PREGNANCYBREASTGROWTHLIMIT_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_PREGNANCYBREASTGROWTHVARIANCE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_PREGNANCYBREASTGROWTH_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_PREGNANCYDURATION_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_PREGNANCYLACTATIONINCREASEVARIANCE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_PREGNANCYLACTATIONINCREASE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_PREGNANCYLACTATIONLIMIT_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_PREGNANCYUDDERGROWTHLIMIT_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_PREGNANCYUDDERGROWTH_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_PREGNANCYUDDERLACTATIONINCREASE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_PREGNANCYUDDERLACTATIONLIMIT_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_RANDOMRACEPERCENTAGE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_TAURFURRYLEVEL_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_TAURSPAWNRATE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_TRAPPENISSIZEPREFERENCE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_UDDERSIZEPREFERENCE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_UDDERS_ATTRIBUTE: str = 'value'
    _XMLID_TAG_AIBLUNDERRATE_ELEMENT: str = 'AIblunderRate'
    _XMLID_TAG_ANDROGYNOUSIDENTIFICATION_ELEMENT: str = 'androgynousIdentification'
    _XMLID_TAG_AUTOSAVEFREQUENCY_ELEMENT: str = 'autoSaveFrequency'
    _XMLID_TAG_BADENDTITLE_ELEMENT: str = 'badEndTitle'
    _XMLID_TAG_BREASTSIZEPREFERENCE_ELEMENT: str = 'breastSizePreference'
    _XMLID_TAG_BYPASSSEXACTIONS_ELEMENT: str = 'bypassSexActions'
    _XMLID_TAG_CLOTHINGFEMININITYLEVEL_ELEMENT: str = 'clothingFemininityLevel'
    _XMLID_TAG_DIFFICULTYLEVEL_ELEMENT: str = 'difficultyLevel'
    _XMLID_TAG_FONTSIZE_ELEMENT: str = 'fontSize'
    _XMLID_TAG_FORCEDFETISHPERCENTAGE_ELEMENT: str = 'forcedFetishPercentage'
    _XMLID_TAG_FORCEDFETISHTENDENCY_ELEMENT: str = 'forcedFetishTendency'
    _XMLID_TAG_FORCEDTFPERCENTAGE_ELEMENT: str = 'forcedTFPercentage'
    _XMLID_TAG_FORCEDTFPREFERENCE_ELEMENT: str = 'forcedTFPreference'
    _XMLID_TAG_FORCEDTFTENDENCY_ELEMENT: str = 'forcedTFTendency'
    _XMLID_TAG_FULLEXPOSUREDESCRIPTIONS_ELEMENT: str = 'fullExposureDescriptions'
    _XMLID_TAG_HALFDEMONSPAWNRATE_ELEMENT: str = 'halfDemonSpawnRate'
    _XMLID_TAG_HUMANSPAWNRATE_ELEMENT: str = 'humanSpawnRate'
    _XMLID_TAG_MULTIBREASTS_ELEMENT: str = 'multiBreasts'
    _XMLID_TAG_PENISSIZEPREFERENCE_ELEMENT: str = 'penisSizePreference'
    _XMLID_TAG_PREFERREDARTIST_ELEMENT: str = 'preferredArtist'
    _XMLID_TAG_PREGNANCYBREASTGROWTHLIMIT_ELEMENT: str = 'pregnancyBreastGrowthLimit'
    _XMLID_TAG_PREGNANCYBREASTGROWTHVARIANCE_ELEMENT: str = 'pregnancyBreastGrowthVariance'
    _XMLID_TAG_PREGNANCYBREASTGROWTH_ELEMENT: str = 'pregnancyBreastGrowth'
    _XMLID_TAG_PREGNANCYDURATION_ELEMENT: str = 'pregnancyDuration'
    _XMLID_TAG_PREGNANCYLACTATIONINCREASEVARIANCE_ELEMENT: str = 'pregnancyLactationIncreaseVariance'
    _XMLID_TAG_PREGNANCYLACTATIONINCREASE_ELEMENT: str = 'pregnancyLactationIncrease'
    _XMLID_TAG_PREGNANCYLACTATIONLIMIT_ELEMENT: str = 'pregnancyLactationLimit'
    _XMLID_TAG_PREGNANCYUDDERGROWTHLIMIT_ELEMENT: str = 'pregnancyUdderGrowthLimit'
    _XMLID_TAG_PREGNANCYUDDERGROWTH_ELEMENT: str = 'pregnancyUdderGrowth'
    _XMLID_TAG_PREGNANCYUDDERLACTATIONINCREASE_ELEMENT: str = 'pregnancyUdderLactationIncrease'
    _XMLID_TAG_PREGNANCYUDDERLACTATIONLIMIT_ELEMENT: str = 'pregnancyUdderLactationLimit'
    _XMLID_TAG_RANDOMRACEPERCENTAGE_ELEMENT: str = 'randomRacePercentage'
    _XMLID_TAG_TAURFURRYLEVEL_ELEMENT: str = 'taurFurryLevel'
    _XMLID_TAG_TAURSPAWNRATE_ELEMENT: str = 'taurSpawnRate'
    _XMLID_TAG_TRAPPENISSIZEPREFERENCE_ELEMENT: str = 'trapPenisSizePreference'
    _XMLID_TAG_UDDERSIZEPREFERENCE_ELEMENT: str = 'udderSizePreference'
    _XMLID_TAG_UDDERS_ELEMENT: str = 'udders'

    def __init__(self) -> None:
        super().__init__()
        self.fontSize: int = 0  # Element
        self.preferredArtist: str = ''  # Element
        self.badEndTitle: Optional[str] = None  # Element
        self.androgynousIdentification: EAndrogynousIdentification = EAndrogynousIdentification(0)  # Element
        self.humanSpawnRate: int = 0  # Element
        self.taurSpawnRate: int = 0  # Element
        self.halfDemonSpawnRate: int = 0  # Element
        self.taurFurryLevel: int = 0  # Element
        self.multiBreasts: int = 0  # Element
        self.udders: int = 0  # Element
        self.autoSaveFrequency: int = 0  # Element
        self.bypassSexActions: int = 0  # Element
        self.fullExposureDescriptions: int = 0  # Element
        self.pregnancyDuration: int = 0  # Element
        self.forcedTFPercentage: int = 0  # Element
        self.randomRacePercentage: float = 0.  # Element
        self.pregnancyBreastGrowthVariance: int = 0  # Element
        self.pregnancyBreastGrowth: int = 0  # Element
        self.pregnancyUdderGrowth: int = 0  # Element
        self.pregnancyBreastGrowthLimit: int = 0  # Element
        self.pregnancyUdderGrowthLimit: int = 0  # Element
        self.pregnancyLactationIncreaseVariance: int = 0  # Element
        self.pregnancyLactationIncrease: int = 0  # Element
        self.pregnancyUdderLactationIncrease: int = 0  # Element
        self.pregnancyLactationLimit: int = 0  # Element
        self.pregnancyUdderLactationLimit: int = 0  # Element
        self.breastSizePreference: int = 0  # Element
        self.udderSizePreference: int = 0  # Element
        self.penisSizePreference: int = 0  # Element
        self.trapPenisSizePreference: int = 0  # Element
        self.clothingFemininityLevel: int = 0  # Element
        self.forcedFetishPercentage: int = 0  # Element
        self.difficultyLevel: EDifficultyLevel = EDifficultyLevel.NORMAL  # Element
        self.AIblunderRate: float = 0.  # Element
        self.forcedTFPreference: EFurryPreference = EFurryPreference(0)  # Element
        self.forcedTFTendency: EForcedTFTendency = EForcedTFTendency(0)  # Element
        self.forcedFetishTendency: EForcedFetishTendency = EForcedFetishTendency(0)  # Element

    def overrideAttrs(self, AIblunderRate_attribute: _Optional_str = None, androgynousIdentification_attribute: _Optional_str = None, autoSaveFrequency_attribute: _Optional_str = None, badEndTitle_attribute: _Optional_str = None, breastSizePreference_attribute: _Optional_str = None, bypassSexActions_attribute: _Optional_str = None, clothingFemininityLevel_attribute: _Optional_str = None, difficultyLevel_attribute: _Optional_str = None, fontSize_attribute: _Optional_str = None, forcedFetishPercentage_attribute: _Optional_str = None, forcedFetishTendency_attribute: _Optional_str = None, forcedTFPercentage_attribute: _Optional_str = None, forcedTFPreference_attribute: _Optional_str = None, forcedTFTendency_attribute: _Optional_str = None, fullExposureDescriptions_attribute: _Optional_str = None, halfDemonSpawnRate_attribute: _Optional_str = None, humanSpawnRate_attribute: _Optional_str = None, multiBreasts_attribute: _Optional_str = None, penisSizePreference_attribute: _Optional_str = None, preferredArtist_attribute: _Optional_str = None, pregnancyBreastGrowth_attribute: _Optional_str = None, pregnancyBreastGrowthLimit_attribute: _Optional_str = None, pregnancyBreastGrowthVariance_attribute: _Optional_str = None, pregnancyDuration_attribute: _Optional_str = None, pregnancyLactationIncrease_attribute: _Optional_str = None, pregnancyLactationIncreaseVariance_attribute: _Optional_str = None, pregnancyLactationLimit_attribute: _Optional_str = None, pregnancyUdderGrowth_attribute: _Optional_str = None, pregnancyUdderGrowthLimit_attribute: _Optional_str = None, pregnancyUdderLactationIncrease_attribute: _Optional_str = None, pregnancyUdderLactationLimit_attribute: _Optional_str = None, randomRacePercentage_attribute: _Optional_str = None, taurFurryLevel_attribute: _Optional_str = None, taurSpawnRate_attribute: _Optional_str = None, trapPenisSizePreference_attribute: _Optional_str = None, udderSizePreference_attribute: _Optional_str = None, udders_attribute: _Optional_str = None) -> None:
        if AIblunderRate_attribute is not None:
            self._XMLID_ATTR_AIBLUNDERRATE_ATTRIBUTE = AIblunderRate_attribute
        if androgynousIdentification_attribute is not None:
            self._XMLID_ATTR_ANDROGYNOUSIDENTIFICATION_ATTRIBUTE = androgynousIdentification_attribute
        if autoSaveFrequency_attribute is not None:
            self._XMLID_ATTR_AUTOSAVEFREQUENCY_ATTRIBUTE = autoSaveFrequency_attribute
        if badEndTitle_attribute is not None:
            self._XMLID_ATTR_BADENDTITLE_ATTRIBUTE = badEndTitle_attribute
        if breastSizePreference_attribute is not None:
            self._XMLID_ATTR_BREASTSIZEPREFERENCE_ATTRIBUTE = breastSizePreference_attribute
        if bypassSexActions_attribute is not None:
            self._XMLID_ATTR_BYPASSSEXACTIONS_ATTRIBUTE = bypassSexActions_attribute
        if clothingFemininityLevel_attribute is not None:
            self._XMLID_ATTR_CLOTHINGFEMININITYLEVEL_ATTRIBUTE = clothingFemininityLevel_attribute
        if difficultyLevel_attribute is not None:
            self._XMLID_ATTR_DIFFICULTYLEVEL_ATTRIBUTE = difficultyLevel_attribute
        if fontSize_attribute is not None:
            self._XMLID_ATTR_FONTSIZE_ATTRIBUTE = fontSize_attribute
        if forcedFetishPercentage_attribute is not None:
            self._XMLID_ATTR_FORCEDFETISHPERCENTAGE_ATTRIBUTE = forcedFetishPercentage_attribute
        if forcedFetishTendency_attribute is not None:
            self._XMLID_ATTR_FORCEDFETISHTENDENCY_ATTRIBUTE = forcedFetishTendency_attribute
        if forcedTFPercentage_attribute is not None:
            self._XMLID_ATTR_FORCEDTFPERCENTAGE_ATTRIBUTE = forcedTFPercentage_attribute
        if forcedTFPreference_attribute is not None:
            self._XMLID_ATTR_FORCEDTFPREFERENCE_ATTRIBUTE = forcedTFPreference_attribute
        if forcedTFTendency_attribute is not None:
            self._XMLID_ATTR_FORCEDTFTENDENCY_ATTRIBUTE = forcedTFTendency_attribute
        if fullExposureDescriptions_attribute is not None:
            self._XMLID_ATTR_FULLEXPOSUREDESCRIPTIONS_ATTRIBUTE = fullExposureDescriptions_attribute
        if halfDemonSpawnRate_attribute is not None:
            self._XMLID_ATTR_HALFDEMONSPAWNRATE_ATTRIBUTE = halfDemonSpawnRate_attribute
        if humanSpawnRate_attribute is not None:
            self._XMLID_ATTR_HUMANSPAWNRATE_ATTRIBUTE = humanSpawnRate_attribute
        if multiBreasts_attribute is not None:
            self._XMLID_ATTR_MULTIBREASTS_ATTRIBUTE = multiBreasts_attribute
        if penisSizePreference_attribute is not None:
            self._XMLID_ATTR_PENISSIZEPREFERENCE_ATTRIBUTE = penisSizePreference_attribute
        if preferredArtist_attribute is not None:
            self._XMLID_ATTR_PREFERREDARTIST_ATTRIBUTE = preferredArtist_attribute
        if pregnancyBreastGrowth_attribute is not None:
            self._XMLID_ATTR_PREGNANCYBREASTGROWTH_ATTRIBUTE = pregnancyBreastGrowth_attribute
        if pregnancyBreastGrowthLimit_attribute is not None:
            self._XMLID_ATTR_PREGNANCYBREASTGROWTHLIMIT_ATTRIBUTE = pregnancyBreastGrowthLimit_attribute
        if pregnancyBreastGrowthVariance_attribute is not None:
            self._XMLID_ATTR_PREGNANCYBREASTGROWTHVARIANCE_ATTRIBUTE = pregnancyBreastGrowthVariance_attribute
        if pregnancyDuration_attribute is not None:
            self._XMLID_ATTR_PREGNANCYDURATION_ATTRIBUTE = pregnancyDuration_attribute
        if pregnancyLactationIncrease_attribute is not None:
            self._XMLID_ATTR_PREGNANCYLACTATIONINCREASE_ATTRIBUTE = pregnancyLactationIncrease_attribute
        if pregnancyLactationIncreaseVariance_attribute is not None:
            self._XMLID_ATTR_PREGNANCYLACTATIONINCREASEVARIANCE_ATTRIBUTE = pregnancyLactationIncreaseVariance_attribute
        if pregnancyLactationLimit_attribute is not None:
            self._XMLID_ATTR_PREGNANCYLACTATIONLIMIT_ATTRIBUTE = pregnancyLactationLimit_attribute
        if pregnancyUdderGrowth_attribute is not None:
            self._XMLID_ATTR_PREGNANCYUDDERGROWTH_ATTRIBUTE = pregnancyUdderGrowth_attribute
        if pregnancyUdderGrowthLimit_attribute is not None:
            self._XMLID_ATTR_PREGNANCYUDDERGROWTHLIMIT_ATTRIBUTE = pregnancyUdderGrowthLimit_attribute
        if pregnancyUdderLactationIncrease_attribute is not None:
            self._XMLID_ATTR_PREGNANCYUDDERLACTATIONINCREASE_ATTRIBUTE = pregnancyUdderLactationIncrease_attribute
        if pregnancyUdderLactationLimit_attribute is not None:
            self._XMLID_ATTR_PREGNANCYUDDERLACTATIONLIMIT_ATTRIBUTE = pregnancyUdderLactationLimit_attribute
        if randomRacePercentage_attribute is not None:
            self._XMLID_ATTR_RANDOMRACEPERCENTAGE_ATTRIBUTE = randomRacePercentage_attribute
        if taurFurryLevel_attribute is not None:
            self._XMLID_ATTR_TAURFURRYLEVEL_ATTRIBUTE = taurFurryLevel_attribute
        if taurSpawnRate_attribute is not None:
            self._XMLID_ATTR_TAURSPAWNRATE_ATTRIBUTE = taurSpawnRate_attribute
        if trapPenisSizePreference_attribute is not None:
            self._XMLID_ATTR_TRAPPENISSIZEPREFERENCE_ATTRIBUTE = trapPenisSizePreference_attribute
        if udderSizePreference_attribute is not None:
            self._XMLID_ATTR_UDDERSIZEPREFERENCE_ATTRIBUTE = udderSizePreference_attribute
        if udders_attribute is not None:
            self._XMLID_ATTR_UDDERS_ATTRIBUTE = udders_attribute

    def overrideTags(self, AIblunderRate_element: _Optional_str = None, androgynousIdentification_element: _Optional_str = None, autoSaveFrequency_element: _Optional_str = None, badEndTitle_element: _Optional_str = None, breastSizePreference_element: _Optional_str = None, bypassSexActions_element: _Optional_str = None, clothingFemininityLevel_element: _Optional_str = None, difficultyLevel_element: _Optional_str = None, fontSize_element: _Optional_str = None, forcedFetishPercentage_element: _Optional_str = None, forcedFetishTendency_element: _Optional_str = None, forcedTFPercentage_element: _Optional_str = None, forcedTFPreference_element: _Optional_str = None, forcedTFTendency_element: _Optional_str = None, fullExposureDescriptions_element: _Optional_str = None, halfDemonSpawnRate_element: _Optional_str = None, humanSpawnRate_element: _Optional_str = None, multiBreasts_element: _Optional_str = None, penisSizePreference_element: _Optional_str = None, preferredArtist_element: _Optional_str = None, pregnancyBreastGrowth_element: _Optional_str = None, pregnancyBreastGrowthLimit_element: _Optional_str = None, pregnancyBreastGrowthVariance_element: _Optional_str = None, pregnancyDuration_element: _Optional_str = None, pregnancyLactationIncrease_element: _Optional_str = None, pregnancyLactationIncreaseVariance_element: _Optional_str = None, pregnancyLactationLimit_element: _Optional_str = None, pregnancyUdderGrowth_element: _Optional_str = None, pregnancyUdderGrowthLimit_element: _Optional_str = None, pregnancyUdderLactationIncrease_element: _Optional_str = None, pregnancyUdderLactationLimit_element: _Optional_str = None, randomRacePercentage_element: _Optional_str = None, taurFurryLevel_element: _Optional_str = None, taurSpawnRate_element: _Optional_str = None, trapPenisSizePreference_element: _Optional_str = None, udderSizePreference_element: _Optional_str = None, udders_element: _Optional_str = None) -> None:
        if AIblunderRate_element is not None:
            self._XMLID_TAG_AIBLUNDERRATE_ELEMENT = AIblunderRate_element
        if androgynousIdentification_element is not None:
            self._XMLID_TAG_ANDROGYNOUSIDENTIFICATION_ELEMENT = androgynousIdentification_element
        if autoSaveFrequency_element is not None:
            self._XMLID_TAG_AUTOSAVEFREQUENCY_ELEMENT = autoSaveFrequency_element
        if badEndTitle_element is not None:
            self._XMLID_TAG_BADENDTITLE_ELEMENT = badEndTitle_element
        if breastSizePreference_element is not None:
            self._XMLID_TAG_BREASTSIZEPREFERENCE_ELEMENT = breastSizePreference_element
        if bypassSexActions_element is not None:
            self._XMLID_TAG_BYPASSSEXACTIONS_ELEMENT = bypassSexActions_element
        if clothingFemininityLevel_element is not None:
            self._XMLID_TAG_CLOTHINGFEMININITYLEVEL_ELEMENT = clothingFemininityLevel_element
        if difficultyLevel_element is not None:
            self._XMLID_TAG_DIFFICULTYLEVEL_ELEMENT = difficultyLevel_element
        if fontSize_element is not None:
            self._XMLID_TAG_FONTSIZE_ELEMENT = fontSize_element
        if forcedFetishPercentage_element is not None:
            self._XMLID_TAG_FORCEDFETISHPERCENTAGE_ELEMENT = forcedFetishPercentage_element
        if forcedFetishTendency_element is not None:
            self._XMLID_TAG_FORCEDFETISHTENDENCY_ELEMENT = forcedFetishTendency_element
        if forcedTFPercentage_element is not None:
            self._XMLID_TAG_FORCEDTFPERCENTAGE_ELEMENT = forcedTFPercentage_element
        if forcedTFPreference_element is not None:
            self._XMLID_TAG_FORCEDTFPREFERENCE_ELEMENT = forcedTFPreference_element
        if forcedTFTendency_element is not None:
            self._XMLID_TAG_FORCEDTFTENDENCY_ELEMENT = forcedTFTendency_element
        if fullExposureDescriptions_element is not None:
            self._XMLID_TAG_FULLEXPOSUREDESCRIPTIONS_ELEMENT = fullExposureDescriptions_element
        if halfDemonSpawnRate_element is not None:
            self._XMLID_TAG_HALFDEMONSPAWNRATE_ELEMENT = halfDemonSpawnRate_element
        if humanSpawnRate_element is not None:
            self._XMLID_TAG_HUMANSPAWNRATE_ELEMENT = humanSpawnRate_element
        if multiBreasts_element is not None:
            self._XMLID_TAG_MULTIBREASTS_ELEMENT = multiBreasts_element
        if penisSizePreference_element is not None:
            self._XMLID_TAG_PENISSIZEPREFERENCE_ELEMENT = penisSizePreference_element
        if preferredArtist_element is not None:
            self._XMLID_TAG_PREFERREDARTIST_ELEMENT = preferredArtist_element
        if pregnancyBreastGrowth_element is not None:
            self._XMLID_TAG_PREGNANCYBREASTGROWTH_ELEMENT = pregnancyBreastGrowth_element
        if pregnancyBreastGrowthLimit_element is not None:
            self._XMLID_TAG_PREGNANCYBREASTGROWTHLIMIT_ELEMENT = pregnancyBreastGrowthLimit_element
        if pregnancyBreastGrowthVariance_element is not None:
            self._XMLID_TAG_PREGNANCYBREASTGROWTHVARIANCE_ELEMENT = pregnancyBreastGrowthVariance_element
        if pregnancyDuration_element is not None:
            self._XMLID_TAG_PREGNANCYDURATION_ELEMENT = pregnancyDuration_element
        if pregnancyLactationIncrease_element is not None:
            self._XMLID_TAG_PREGNANCYLACTATIONINCREASE_ELEMENT = pregnancyLactationIncrease_element
        if pregnancyLactationIncreaseVariance_element is not None:
            self._XMLID_TAG_PREGNANCYLACTATIONINCREASEVARIANCE_ELEMENT = pregnancyLactationIncreaseVariance_element
        if pregnancyLactationLimit_element is not None:
            self._XMLID_TAG_PREGNANCYLACTATIONLIMIT_ELEMENT = pregnancyLactationLimit_element
        if pregnancyUdderGrowth_element is not None:
            self._XMLID_TAG_PREGNANCYUDDERGROWTH_ELEMENT = pregnancyUdderGrowth_element
        if pregnancyUdderGrowthLimit_element is not None:
            self._XMLID_TAG_PREGNANCYUDDERGROWTHLIMIT_ELEMENT = pregnancyUdderGrowthLimit_element
        if pregnancyUdderLactationIncrease_element is not None:
            self._XMLID_TAG_PREGNANCYUDDERLACTATIONINCREASE_ELEMENT = pregnancyUdderLactationIncrease_element
        if pregnancyUdderLactationLimit_element is not None:
            self._XMLID_TAG_PREGNANCYUDDERLACTATIONLIMIT_ELEMENT = pregnancyUdderLactationLimit_element
        if randomRacePercentage_element is not None:
            self._XMLID_TAG_RANDOMRACEPERCENTAGE_ELEMENT = randomRacePercentage_element
        if taurFurryLevel_element is not None:
            self._XMLID_TAG_TAURFURRYLEVEL_ELEMENT = taurFurryLevel_element
        if taurSpawnRate_element is not None:
            self._XMLID_TAG_TAURSPAWNRATE_ELEMENT = taurSpawnRate_element
        if trapPenisSizePreference_element is not None:
            self._XMLID_TAG_TRAPPENISSIZEPREFERENCE_ELEMENT = trapPenisSizePreference_element
        if udderSizePreference_element is not None:
            self._XMLID_TAG_UDDERSIZEPREFERENCE_ELEMENT = udderSizePreference_element
        if udders_element is not None:
            self._XMLID_TAG_UDDERS_ELEMENT = udders_element

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_FONTSIZE_ELEMENT)) is not None:
            if self._XMLID_ATTR_FONTSIZE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_FONTSIZE_ATTRIBUTE, 'fontSize')
            self.fontSize = int(sf.attrib[self._XMLID_ATTR_FONTSIZE_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'fontSize', self._XMLID_TAG_FONTSIZE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_PREFERREDARTIST_ELEMENT)) is not None:
            if self._XMLID_ATTR_PREFERREDARTIST_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_PREFERREDARTIST_ATTRIBUTE, 'preferredArtist')
            self.preferredArtist = sf.attrib[self._XMLID_ATTR_PREFERREDARTIST_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'preferredArtist', self._XMLID_TAG_PREFERREDARTIST_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_BADENDTITLE_ELEMENT)) is not None:
            self.badEndTitle = sf.attrib.get(self._XMLID_ATTR_BADENDTITLE_ATTRIBUTE, None)
        if (sf := e.find(self._XMLID_TAG_ANDROGYNOUSIDENTIFICATION_ELEMENT)) is not None:
            if self._XMLID_ATTR_ANDROGYNOUSIDENTIFICATION_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_ANDROGYNOUSIDENTIFICATION_ATTRIBUTE, 'androgynousIdentification')
            self.androgynousIdentification = EAndrogynousIdentification[sf.attrib[self._XMLID_ATTR_ANDROGYNOUSIDENTIFICATION_ATTRIBUTE]]
        else:
            raise ElementRequiredException(self, e, 'androgynousIdentification', self._XMLID_TAG_ANDROGYNOUSIDENTIFICATION_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_HUMANSPAWNRATE_ELEMENT)) is not None:
            if self._XMLID_ATTR_HUMANSPAWNRATE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_HUMANSPAWNRATE_ATTRIBUTE, 'humanSpawnRate')
            self.humanSpawnRate = int(sf.attrib[self._XMLID_ATTR_HUMANSPAWNRATE_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'humanSpawnRate', self._XMLID_TAG_HUMANSPAWNRATE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_TAURSPAWNRATE_ELEMENT)) is not None:
            if self._XMLID_ATTR_TAURSPAWNRATE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_TAURSPAWNRATE_ATTRIBUTE, 'taurSpawnRate')
            self.taurSpawnRate = int(sf.attrib[self._XMLID_ATTR_TAURSPAWNRATE_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'taurSpawnRate', self._XMLID_TAG_TAURSPAWNRATE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_HALFDEMONSPAWNRATE_ELEMENT)) is not None:
            if self._XMLID_ATTR_HALFDEMONSPAWNRATE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_HALFDEMONSPAWNRATE_ATTRIBUTE, 'halfDemonSpawnRate')
            self.halfDemonSpawnRate = int(sf.attrib[self._XMLID_ATTR_HALFDEMONSPAWNRATE_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'halfDemonSpawnRate', self._XMLID_TAG_HALFDEMONSPAWNRATE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_TAURFURRYLEVEL_ELEMENT)) is not None:
            if self._XMLID_ATTR_TAURFURRYLEVEL_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_TAURFURRYLEVEL_ATTRIBUTE, 'taurFurryLevel')
            self.taurFurryLevel = int(sf.attrib[self._XMLID_ATTR_TAURFURRYLEVEL_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'taurFurryLevel', self._XMLID_TAG_TAURFURRYLEVEL_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_MULTIBREASTS_ELEMENT)) is not None:
            if self._XMLID_ATTR_MULTIBREASTS_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_MULTIBREASTS_ATTRIBUTE, 'multiBreasts')
            self.multiBreasts = int(sf.attrib[self._XMLID_ATTR_MULTIBREASTS_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'multiBreasts', self._XMLID_TAG_MULTIBREASTS_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_UDDERS_ELEMENT)) is not None:
            if self._XMLID_ATTR_UDDERS_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_UDDERS_ATTRIBUTE, 'udders')
            self.udders = int(sf.attrib[self._XMLID_ATTR_UDDERS_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'udders', self._XMLID_TAG_UDDERS_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_AUTOSAVEFREQUENCY_ELEMENT)) is not None:
            if self._XMLID_ATTR_AUTOSAVEFREQUENCY_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_AUTOSAVEFREQUENCY_ATTRIBUTE, 'autoSaveFrequency')
            self.autoSaveFrequency = int(sf.attrib[self._XMLID_ATTR_AUTOSAVEFREQUENCY_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'autoSaveFrequency', self._XMLID_TAG_AUTOSAVEFREQUENCY_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_BYPASSSEXACTIONS_ELEMENT)) is not None:
            if self._XMLID_ATTR_BYPASSSEXACTIONS_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_BYPASSSEXACTIONS_ATTRIBUTE, 'bypassSexActions')
            self.bypassSexActions = int(sf.attrib[self._XMLID_ATTR_BYPASSSEXACTIONS_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'bypassSexActions', self._XMLID_TAG_BYPASSSEXACTIONS_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_FULLEXPOSUREDESCRIPTIONS_ELEMENT)) is not None:
            if self._XMLID_ATTR_FULLEXPOSUREDESCRIPTIONS_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_FULLEXPOSUREDESCRIPTIONS_ATTRIBUTE, 'fullExposureDescriptions')
            self.fullExposureDescriptions = int(sf.attrib[self._XMLID_ATTR_FULLEXPOSUREDESCRIPTIONS_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'fullExposureDescriptions', self._XMLID_TAG_FULLEXPOSUREDESCRIPTIONS_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_PREGNANCYDURATION_ELEMENT)) is not None:
            if self._XMLID_ATTR_PREGNANCYDURATION_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_PREGNANCYDURATION_ATTRIBUTE, 'pregnancyDuration')
            self.pregnancyDuration = int(sf.attrib[self._XMLID_ATTR_PREGNANCYDURATION_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'pregnancyDuration', self._XMLID_TAG_PREGNANCYDURATION_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_FORCEDTFPERCENTAGE_ELEMENT)) is not None:
            if self._XMLID_ATTR_FORCEDTFPERCENTAGE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_FORCEDTFPERCENTAGE_ATTRIBUTE, 'forcedTFPercentage')
            self.forcedTFPercentage = int(sf.attrib[self._XMLID_ATTR_FORCEDTFPERCENTAGE_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'forcedTFPercentage', self._XMLID_TAG_FORCEDTFPERCENTAGE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_RANDOMRACEPERCENTAGE_ELEMENT)) is not None:
            if self._XMLID_ATTR_RANDOMRACEPERCENTAGE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_RANDOMRACEPERCENTAGE_ATTRIBUTE, 'randomRacePercentage')
            self.randomRacePercentage = float(sf.attrib[self._XMLID_ATTR_RANDOMRACEPERCENTAGE_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'randomRacePercentage', self._XMLID_TAG_RANDOMRACEPERCENTAGE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_PREGNANCYBREASTGROWTHVARIANCE_ELEMENT)) is not None:
            if self._XMLID_ATTR_PREGNANCYBREASTGROWTHVARIANCE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_PREGNANCYBREASTGROWTHVARIANCE_ATTRIBUTE, 'pregnancyBreastGrowthVariance')
            self.pregnancyBreastGrowthVariance = int(sf.attrib[self._XMLID_ATTR_PREGNANCYBREASTGROWTHVARIANCE_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'pregnancyBreastGrowthVariance', self._XMLID_TAG_PREGNANCYBREASTGROWTHVARIANCE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_PREGNANCYBREASTGROWTH_ELEMENT)) is not None:
            if self._XMLID_ATTR_PREGNANCYBREASTGROWTH_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_PREGNANCYBREASTGROWTH_ATTRIBUTE, 'pregnancyBreastGrowth')
            self.pregnancyBreastGrowth = int(sf.attrib[self._XMLID_ATTR_PREGNANCYBREASTGROWTH_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'pregnancyBreastGrowth', self._XMLID_TAG_PREGNANCYBREASTGROWTH_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_PREGNANCYUDDERGROWTH_ELEMENT)) is not None:
            if self._XMLID_ATTR_PREGNANCYUDDERGROWTH_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_PREGNANCYUDDERGROWTH_ATTRIBUTE, 'pregnancyUdderGrowth')
            self.pregnancyUdderGrowth = int(sf.attrib[self._XMLID_ATTR_PREGNANCYUDDERGROWTH_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'pregnancyUdderGrowth', self._XMLID_TAG_PREGNANCYUDDERGROWTH_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_PREGNANCYBREASTGROWTHLIMIT_ELEMENT)) is not None:
            if self._XMLID_ATTR_PREGNANCYBREASTGROWTHLIMIT_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_PREGNANCYBREASTGROWTHLIMIT_ATTRIBUTE, 'pregnancyBreastGrowthLimit')
            self.pregnancyBreastGrowthLimit = int(sf.attrib[self._XMLID_ATTR_PREGNANCYBREASTGROWTHLIMIT_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'pregnancyBreastGrowthLimit', self._XMLID_TAG_PREGNANCYBREASTGROWTHLIMIT_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_PREGNANCYUDDERGROWTHLIMIT_ELEMENT)) is not None:
            if self._XMLID_ATTR_PREGNANCYUDDERGROWTHLIMIT_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_PREGNANCYUDDERGROWTHLIMIT_ATTRIBUTE, 'pregnancyUdderGrowthLimit')
            self.pregnancyUdderGrowthLimit = int(sf.attrib[self._XMLID_ATTR_PREGNANCYUDDERGROWTHLIMIT_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'pregnancyUdderGrowthLimit', self._XMLID_TAG_PREGNANCYUDDERGROWTHLIMIT_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_PREGNANCYLACTATIONINCREASEVARIANCE_ELEMENT)) is not None:
            if self._XMLID_ATTR_PREGNANCYLACTATIONINCREASEVARIANCE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_PREGNANCYLACTATIONINCREASEVARIANCE_ATTRIBUTE, 'pregnancyLactationIncreaseVariance')
            self.pregnancyLactationIncreaseVariance = int(sf.attrib[self._XMLID_ATTR_PREGNANCYLACTATIONINCREASEVARIANCE_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'pregnancyLactationIncreaseVariance', self._XMLID_TAG_PREGNANCYLACTATIONINCREASEVARIANCE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_PREGNANCYLACTATIONINCREASE_ELEMENT)) is not None:
            if self._XMLID_ATTR_PREGNANCYLACTATIONINCREASE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_PREGNANCYLACTATIONINCREASE_ATTRIBUTE, 'pregnancyLactationIncrease')
            self.pregnancyLactationIncrease = int(sf.attrib[self._XMLID_ATTR_PREGNANCYLACTATIONINCREASE_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'pregnancyLactationIncrease', self._XMLID_TAG_PREGNANCYLACTATIONINCREASE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_PREGNANCYUDDERLACTATIONINCREASE_ELEMENT)) is not None:
            if self._XMLID_ATTR_PREGNANCYUDDERLACTATIONINCREASE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_PREGNANCYUDDERLACTATIONINCREASE_ATTRIBUTE, 'pregnancyUdderLactationIncrease')
            self.pregnancyUdderLactationIncrease = int(sf.attrib[self._XMLID_ATTR_PREGNANCYUDDERLACTATIONINCREASE_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'pregnancyUdderLactationIncrease', self._XMLID_TAG_PREGNANCYUDDERLACTATIONINCREASE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_PREGNANCYLACTATIONLIMIT_ELEMENT)) is not None:
            if self._XMLID_ATTR_PREGNANCYLACTATIONLIMIT_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_PREGNANCYLACTATIONLIMIT_ATTRIBUTE, 'pregnancyLactationLimit')
            self.pregnancyLactationLimit = int(sf.attrib[self._XMLID_ATTR_PREGNANCYLACTATIONLIMIT_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'pregnancyLactationLimit', self._XMLID_TAG_PREGNANCYLACTATIONLIMIT_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_PREGNANCYUDDERLACTATIONLIMIT_ELEMENT)) is not None:
            if self._XMLID_ATTR_PREGNANCYUDDERLACTATIONLIMIT_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_PREGNANCYUDDERLACTATIONLIMIT_ATTRIBUTE, 'pregnancyUdderLactationLimit')
            self.pregnancyUdderLactationLimit = int(sf.attrib[self._XMLID_ATTR_PREGNANCYUDDERLACTATIONLIMIT_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'pregnancyUdderLactationLimit', self._XMLID_TAG_PREGNANCYUDDERLACTATIONLIMIT_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_BREASTSIZEPREFERENCE_ELEMENT)) is not None:
            if self._XMLID_ATTR_BREASTSIZEPREFERENCE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_BREASTSIZEPREFERENCE_ATTRIBUTE, 'breastSizePreference')
            self.breastSizePreference = int(sf.attrib[self._XMLID_ATTR_BREASTSIZEPREFERENCE_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'breastSizePreference', self._XMLID_TAG_BREASTSIZEPREFERENCE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_UDDERSIZEPREFERENCE_ELEMENT)) is not None:
            if self._XMLID_ATTR_UDDERSIZEPREFERENCE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_UDDERSIZEPREFERENCE_ATTRIBUTE, 'udderSizePreference')
            self.udderSizePreference = int(sf.attrib[self._XMLID_ATTR_UDDERSIZEPREFERENCE_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'udderSizePreference', self._XMLID_TAG_UDDERSIZEPREFERENCE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_PENISSIZEPREFERENCE_ELEMENT)) is not None:
            if self._XMLID_ATTR_PENISSIZEPREFERENCE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_PENISSIZEPREFERENCE_ATTRIBUTE, 'penisSizePreference')
            self.penisSizePreference = int(sf.attrib[self._XMLID_ATTR_PENISSIZEPREFERENCE_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'penisSizePreference', self._XMLID_TAG_PENISSIZEPREFERENCE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_TRAPPENISSIZEPREFERENCE_ELEMENT)) is not None:
            if self._XMLID_ATTR_TRAPPENISSIZEPREFERENCE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_TRAPPENISSIZEPREFERENCE_ATTRIBUTE, 'trapPenisSizePreference')
            self.trapPenisSizePreference = int(sf.attrib[self._XMLID_ATTR_TRAPPENISSIZEPREFERENCE_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'trapPenisSizePreference', self._XMLID_TAG_TRAPPENISSIZEPREFERENCE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_CLOTHINGFEMININITYLEVEL_ELEMENT)) is not None:
            if self._XMLID_ATTR_CLOTHINGFEMININITYLEVEL_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_CLOTHINGFEMININITYLEVEL_ATTRIBUTE, 'clothingFemininityLevel')
            self.clothingFemininityLevel = int(sf.attrib[self._XMLID_ATTR_CLOTHINGFEMININITYLEVEL_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'clothingFemininityLevel', self._XMLID_TAG_CLOTHINGFEMININITYLEVEL_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_FORCEDFETISHPERCENTAGE_ELEMENT)) is not None:
            if self._XMLID_ATTR_FORCEDFETISHPERCENTAGE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_FORCEDFETISHPERCENTAGE_ATTRIBUTE, 'forcedFetishPercentage')
            self.forcedFetishPercentage = int(sf.attrib[self._XMLID_ATTR_FORCEDFETISHPERCENTAGE_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'forcedFetishPercentage', self._XMLID_TAG_FORCEDFETISHPERCENTAGE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_DIFFICULTYLEVEL_ELEMENT)) is not None:
            if self._XMLID_ATTR_DIFFICULTYLEVEL_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_DIFFICULTYLEVEL_ATTRIBUTE, 'difficultyLevel')
            self.difficultyLevel = EDifficultyLevel[sf.attrib[self._XMLID_ATTR_DIFFICULTYLEVEL_ATTRIBUTE]]
        else:
            raise ElementRequiredException(self, e, 'difficultyLevel', self._XMLID_TAG_DIFFICULTYLEVEL_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_AIBLUNDERRATE_ELEMENT)) is not None:
            if self._XMLID_ATTR_AIBLUNDERRATE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_AIBLUNDERRATE_ATTRIBUTE, 'AIblunderRate')
            self.AIblunderRate = float(sf.attrib[self._XMLID_ATTR_AIBLUNDERRATE_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'AIblunderRate', self._XMLID_TAG_AIBLUNDERRATE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_FORCEDTFPREFERENCE_ELEMENT)) is not None:
            if self._XMLID_ATTR_FORCEDTFPREFERENCE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_FORCEDTFPREFERENCE_ATTRIBUTE, 'forcedTFPreference')
            self.forcedTFPreference = EFurryPreference[sf.attrib[self._XMLID_ATTR_FORCEDTFPREFERENCE_ATTRIBUTE]]
        else:
            raise ElementRequiredException(self, e, 'forcedTFPreference', self._XMLID_TAG_FORCEDTFPREFERENCE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_FORCEDTFTENDENCY_ELEMENT)) is not None:
            if self._XMLID_ATTR_FORCEDTFTENDENCY_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_FORCEDTFTENDENCY_ATTRIBUTE, 'forcedTFTendency')
            self.forcedTFTendency = EForcedTFTendency[sf.attrib[self._XMLID_ATTR_FORCEDTFTENDENCY_ATTRIBUTE]]
        else:
            raise ElementRequiredException(self, e, 'forcedTFTendency', self._XMLID_TAG_FORCEDTFTENDENCY_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_FORCEDFETISHTENDENCY_ELEMENT)) is not None:
            if self._XMLID_ATTR_FORCEDFETISHTENDENCY_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_FORCEDFETISHTENDENCY_ATTRIBUTE, 'forcedFetishTendency')
            self.forcedFetishTendency = EForcedFetishTendency[sf.attrib[self._XMLID_ATTR_FORCEDFETISHTENDENCY_ATTRIBUTE]]
        else:
            raise ElementRequiredException(self, e, 'forcedFetishTendency', self._XMLID_TAG_FORCEDFETISHTENDENCY_ELEMENT)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        etree.SubElement(e, self._XMLID_TAG_FONTSIZE_ELEMENT, {self._XMLID_ATTR_FONTSIZE_ATTRIBUTE: str(self.fontSize)})
        etree.SubElement(e, self._XMLID_TAG_PREFERREDARTIST_ELEMENT, {self._XMLID_ATTR_PREFERREDARTIST_ATTRIBUTE: str(self.preferredArtist)})
        if self.badEndTitle is not None:
            etree.SubElement(e, self._XMLID_TAG_BADENDTITLE_ELEMENT, {self._XMLID_ATTR_BADENDTITLE_ATTRIBUTE: str(self.badEndTitle)})
        etree.SubElement(e, self._XMLID_TAG_ANDROGYNOUSIDENTIFICATION_ELEMENT, {self._XMLID_ATTR_ANDROGYNOUSIDENTIFICATION_ATTRIBUTE: self.androgynousIdentification.name})
        etree.SubElement(e, self._XMLID_TAG_HUMANSPAWNRATE_ELEMENT, {self._XMLID_ATTR_HUMANSPAWNRATE_ATTRIBUTE: str(self.humanSpawnRate)})
        etree.SubElement(e, self._XMLID_TAG_TAURSPAWNRATE_ELEMENT, {self._XMLID_ATTR_TAURSPAWNRATE_ATTRIBUTE: str(self.taurSpawnRate)})
        etree.SubElement(e, self._XMLID_TAG_HALFDEMONSPAWNRATE_ELEMENT, {self._XMLID_ATTR_HALFDEMONSPAWNRATE_ATTRIBUTE: str(self.halfDemonSpawnRate)})
        etree.SubElement(e, self._XMLID_TAG_TAURFURRYLEVEL_ELEMENT, {self._XMLID_ATTR_TAURFURRYLEVEL_ATTRIBUTE: str(self.taurFurryLevel)})
        etree.SubElement(e, self._XMLID_TAG_MULTIBREASTS_ELEMENT, {self._XMLID_ATTR_MULTIBREASTS_ATTRIBUTE: str(self.multiBreasts)})
        etree.SubElement(e, self._XMLID_TAG_UDDERS_ELEMENT, {self._XMLID_ATTR_UDDERS_ATTRIBUTE: str(self.udders)})
        etree.SubElement(e, self._XMLID_TAG_AUTOSAVEFREQUENCY_ELEMENT, {self._XMLID_ATTR_AUTOSAVEFREQUENCY_ATTRIBUTE: str(self.autoSaveFrequency)})
        etree.SubElement(e, self._XMLID_TAG_BYPASSSEXACTIONS_ELEMENT, {self._XMLID_ATTR_BYPASSSEXACTIONS_ATTRIBUTE: str(self.bypassSexActions)})
        etree.SubElement(e, self._XMLID_TAG_FULLEXPOSUREDESCRIPTIONS_ELEMENT, {self._XMLID_ATTR_FULLEXPOSUREDESCRIPTIONS_ATTRIBUTE: str(self.fullExposureDescriptions)})
        etree.SubElement(e, self._XMLID_TAG_PREGNANCYDURATION_ELEMENT, {self._XMLID_ATTR_PREGNANCYDURATION_ATTRIBUTE: str(self.pregnancyDuration)})
        etree.SubElement(e, self._XMLID_TAG_FORCEDTFPERCENTAGE_ELEMENT, {self._XMLID_ATTR_FORCEDTFPERCENTAGE_ATTRIBUTE: str(self.forcedTFPercentage)})
        etree.SubElement(e, self._XMLID_TAG_RANDOMRACEPERCENTAGE_ELEMENT, {self._XMLID_ATTR_RANDOMRACEPERCENTAGE_ATTRIBUTE: str(self.randomRacePercentage)})
        etree.SubElement(e, self._XMLID_TAG_PREGNANCYBREASTGROWTHVARIANCE_ELEMENT, {self._XMLID_ATTR_PREGNANCYBREASTGROWTHVARIANCE_ATTRIBUTE: str(self.pregnancyBreastGrowthVariance)})
        etree.SubElement(e, self._XMLID_TAG_PREGNANCYBREASTGROWTH_ELEMENT, {self._XMLID_ATTR_PREGNANCYBREASTGROWTH_ATTRIBUTE: str(self.pregnancyBreastGrowth)})
        etree.SubElement(e, self._XMLID_TAG_PREGNANCYUDDERGROWTH_ELEMENT, {self._XMLID_ATTR_PREGNANCYUDDERGROWTH_ATTRIBUTE: str(self.pregnancyUdderGrowth)})
        etree.SubElement(e, self._XMLID_TAG_PREGNANCYBREASTGROWTHLIMIT_ELEMENT, {self._XMLID_ATTR_PREGNANCYBREASTGROWTHLIMIT_ATTRIBUTE: str(self.pregnancyBreastGrowthLimit)})
        etree.SubElement(e, self._XMLID_TAG_PREGNANCYUDDERGROWTHLIMIT_ELEMENT, {self._XMLID_ATTR_PREGNANCYUDDERGROWTHLIMIT_ATTRIBUTE: str(self.pregnancyUdderGrowthLimit)})
        etree.SubElement(e, self._XMLID_TAG_PREGNANCYLACTATIONINCREASEVARIANCE_ELEMENT, {self._XMLID_ATTR_PREGNANCYLACTATIONINCREASEVARIANCE_ATTRIBUTE: str(self.pregnancyLactationIncreaseVariance)})
        etree.SubElement(e, self._XMLID_TAG_PREGNANCYLACTATIONINCREASE_ELEMENT, {self._XMLID_ATTR_PREGNANCYLACTATIONINCREASE_ATTRIBUTE: str(self.pregnancyLactationIncrease)})
        etree.SubElement(e, self._XMLID_TAG_PREGNANCYUDDERLACTATIONINCREASE_ELEMENT, {self._XMLID_ATTR_PREGNANCYUDDERLACTATIONINCREASE_ATTRIBUTE: str(self.pregnancyUdderLactationIncrease)})
        etree.SubElement(e, self._XMLID_TAG_PREGNANCYLACTATIONLIMIT_ELEMENT, {self._XMLID_ATTR_PREGNANCYLACTATIONLIMIT_ATTRIBUTE: str(self.pregnancyLactationLimit)})
        etree.SubElement(e, self._XMLID_TAG_PREGNANCYUDDERLACTATIONLIMIT_ELEMENT, {self._XMLID_ATTR_PREGNANCYUDDERLACTATIONLIMIT_ATTRIBUTE: str(self.pregnancyUdderLactationLimit)})
        etree.SubElement(e, self._XMLID_TAG_BREASTSIZEPREFERENCE_ELEMENT, {self._XMLID_ATTR_BREASTSIZEPREFERENCE_ATTRIBUTE: str(self.breastSizePreference)})
        etree.SubElement(e, self._XMLID_TAG_UDDERSIZEPREFERENCE_ELEMENT, {self._XMLID_ATTR_UDDERSIZEPREFERENCE_ATTRIBUTE: str(self.udderSizePreference)})
        etree.SubElement(e, self._XMLID_TAG_PENISSIZEPREFERENCE_ELEMENT, {self._XMLID_ATTR_PENISSIZEPREFERENCE_ATTRIBUTE: str(self.penisSizePreference)})
        etree.SubElement(e, self._XMLID_TAG_TRAPPENISSIZEPREFERENCE_ELEMENT, {self._XMLID_ATTR_TRAPPENISSIZEPREFERENCE_ATTRIBUTE: str(self.trapPenisSizePreference)})
        etree.SubElement(e, self._XMLID_TAG_CLOTHINGFEMININITYLEVEL_ELEMENT, {self._XMLID_ATTR_CLOTHINGFEMININITYLEVEL_ATTRIBUTE: str(self.clothingFemininityLevel)})
        etree.SubElement(e, self._XMLID_TAG_FORCEDFETISHPERCENTAGE_ELEMENT, {self._XMLID_ATTR_FORCEDFETISHPERCENTAGE_ATTRIBUTE: str(self.forcedFetishPercentage)})
        etree.SubElement(e, self._XMLID_TAG_DIFFICULTYLEVEL_ELEMENT, {self._XMLID_ATTR_DIFFICULTYLEVEL_ATTRIBUTE: self.difficultyLevel.name})
        etree.SubElement(e, self._XMLID_TAG_AIBLUNDERRATE_ELEMENT, {self._XMLID_ATTR_AIBLUNDERRATE_ATTRIBUTE: str(self.AIblunderRate)})
        etree.SubElement(e, self._XMLID_TAG_FORCEDTFPREFERENCE_ELEMENT, {self._XMLID_ATTR_FORCEDTFPREFERENCE_ATTRIBUTE: self.forcedTFPreference.name})
        etree.SubElement(e, self._XMLID_TAG_FORCEDTFTENDENCY_ELEMENT, {self._XMLID_ATTR_FORCEDTFTENDENCY_ATTRIBUTE: self.forcedTFTendency.name})
        etree.SubElement(e, self._XMLID_TAG_FORCEDFETISHTENDENCY_ELEMENT, {self._XMLID_ATTR_FORCEDFETISHTENDENCY_ATTRIBUTE: self.forcedFetishTendency.name})
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['fontSize'] = int(self.fontSize)
        data['preferredArtist'] = str(self.preferredArtist)
        if self.badEndTitle is not None:
            data['badEndTitle'] = str(self.badEndTitle)
        data['androgynousIdentification'] = self.androgynousIdentification.name
        data['humanSpawnRate'] = int(self.humanSpawnRate)
        data['taurSpawnRate'] = int(self.taurSpawnRate)
        data['halfDemonSpawnRate'] = int(self.halfDemonSpawnRate)
        data['taurFurryLevel'] = int(self.taurFurryLevel)
        data['multiBreasts'] = int(self.multiBreasts)
        data['udders'] = int(self.udders)
        data['autoSaveFrequency'] = int(self.autoSaveFrequency)
        data['bypassSexActions'] = int(self.bypassSexActions)
        data['fullExposureDescriptions'] = int(self.fullExposureDescriptions)
        data['pregnancyDuration'] = int(self.pregnancyDuration)
        data['forcedTFPercentage'] = int(self.forcedTFPercentage)
        data['randomRacePercentage'] = float(self.randomRacePercentage)
        data['pregnancyBreastGrowthVariance'] = int(self.pregnancyBreastGrowthVariance)
        data['pregnancyBreastGrowth'] = int(self.pregnancyBreastGrowth)
        data['pregnancyUdderGrowth'] = int(self.pregnancyUdderGrowth)
        data['pregnancyBreastGrowthLimit'] = int(self.pregnancyBreastGrowthLimit)
        data['pregnancyUdderGrowthLimit'] = int(self.pregnancyUdderGrowthLimit)
        data['pregnancyLactationIncreaseVariance'] = int(self.pregnancyLactationIncreaseVariance)
        data['pregnancyLactationIncrease'] = int(self.pregnancyLactationIncrease)
        data['pregnancyUdderLactationIncrease'] = int(self.pregnancyUdderLactationIncrease)
        data['pregnancyLactationLimit'] = int(self.pregnancyLactationLimit)
        data['pregnancyUdderLactationLimit'] = int(self.pregnancyUdderLactationLimit)
        data['breastSizePreference'] = int(self.breastSizePreference)
        data['udderSizePreference'] = int(self.udderSizePreference)
        data['penisSizePreference'] = int(self.penisSizePreference)
        data['trapPenisSizePreference'] = int(self.trapPenisSizePreference)
        data['clothingFemininityLevel'] = int(self.clothingFemininityLevel)
        data['forcedFetishPercentage'] = int(self.forcedFetishPercentage)
        data['difficultyLevel'] = self.difficultyLevel.name
        data['AIblunderRate'] = float(self.AIblunderRate)
        data['forcedTFPreference'] = self.forcedTFPreference.name
        data['forcedTFTendency'] = self.forcedTFTendency.name
        data['forcedFetishTendency'] = self.forcedFetishTendency.name
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('fontSize')) is not None:
            self.fontSize = data['fontSize']
        else:
            raise KeyRequiredException(self, data, 'fontSize', 'fontSize')
        if (sf := data.get('preferredArtist')) is not None:
            self.preferredArtist = data['preferredArtist']
        else:
            raise KeyRequiredException(self, data, 'preferredArtist', 'preferredArtist')
        if (sf := data.get('badEndTitle')) is not None:
            self.badEndTitle = data['badEndTitle']
        else:
            self.badEndTitle = None
        if (sf := data.get('androgynousIdentification')) is not None:
            self.androgynousIdentification = data['androgynousIdentification'].name
        else:
            raise KeyRequiredException(self, data, 'androgynousIdentification', 'androgynousIdentification')
        if (sf := data.get('humanSpawnRate')) is not None:
            self.humanSpawnRate = data['humanSpawnRate']
        else:
            raise KeyRequiredException(self, data, 'humanSpawnRate', 'humanSpawnRate')
        if (sf := data.get('taurSpawnRate')) is not None:
            self.taurSpawnRate = data['taurSpawnRate']
        else:
            raise KeyRequiredException(self, data, 'taurSpawnRate', 'taurSpawnRate')
        if (sf := data.get('halfDemonSpawnRate')) is not None:
            self.halfDemonSpawnRate = data['halfDemonSpawnRate']
        else:
            raise KeyRequiredException(self, data, 'halfDemonSpawnRate', 'halfDemonSpawnRate')
        if (sf := data.get('taurFurryLevel')) is not None:
            self.taurFurryLevel = data['taurFurryLevel']
        else:
            raise KeyRequiredException(self, data, 'taurFurryLevel', 'taurFurryLevel')
        if (sf := data.get('multiBreasts')) is not None:
            self.multiBreasts = data['multiBreasts']
        else:
            raise KeyRequiredException(self, data, 'multiBreasts', 'multiBreasts')
        if (sf := data.get('udders')) is not None:
            self.udders = data['udders']
        else:
            raise KeyRequiredException(self, data, 'udders', 'udders')
        if (sf := data.get('autoSaveFrequency')) is not None:
            self.autoSaveFrequency = data['autoSaveFrequency']
        else:
            raise KeyRequiredException(self, data, 'autoSaveFrequency', 'autoSaveFrequency')
        if (sf := data.get('bypassSexActions')) is not None:
            self.bypassSexActions = data['bypassSexActions']
        else:
            raise KeyRequiredException(self, data, 'bypassSexActions', 'bypassSexActions')
        if (sf := data.get('fullExposureDescriptions')) is not None:
            self.fullExposureDescriptions = data['fullExposureDescriptions']
        else:
            raise KeyRequiredException(self, data, 'fullExposureDescriptions', 'fullExposureDescriptions')
        if (sf := data.get('pregnancyDuration')) is not None:
            self.pregnancyDuration = data['pregnancyDuration']
        else:
            raise KeyRequiredException(self, data, 'pregnancyDuration', 'pregnancyDuration')
        if (sf := data.get('forcedTFPercentage')) is not None:
            self.forcedTFPercentage = data['forcedTFPercentage']
        else:
            raise KeyRequiredException(self, data, 'forcedTFPercentage', 'forcedTFPercentage')
        if (sf := data.get('randomRacePercentage')) is not None:
            self.randomRacePercentage = data['randomRacePercentage']
        else:
            raise KeyRequiredException(self, data, 'randomRacePercentage', 'randomRacePercentage')
        if (sf := data.get('pregnancyBreastGrowthVariance')) is not None:
            self.pregnancyBreastGrowthVariance = data['pregnancyBreastGrowthVariance']
        else:
            raise KeyRequiredException(self, data, 'pregnancyBreastGrowthVariance', 'pregnancyBreastGrowthVariance')
        if (sf := data.get('pregnancyBreastGrowth')) is not None:
            self.pregnancyBreastGrowth = data['pregnancyBreastGrowth']
        else:
            raise KeyRequiredException(self, data, 'pregnancyBreastGrowth', 'pregnancyBreastGrowth')
        if (sf := data.get('pregnancyUdderGrowth')) is not None:
            self.pregnancyUdderGrowth = data['pregnancyUdderGrowth']
        else:
            raise KeyRequiredException(self, data, 'pregnancyUdderGrowth', 'pregnancyUdderGrowth')
        if (sf := data.get('pregnancyBreastGrowthLimit')) is not None:
            self.pregnancyBreastGrowthLimit = data['pregnancyBreastGrowthLimit']
        else:
            raise KeyRequiredException(self, data, 'pregnancyBreastGrowthLimit', 'pregnancyBreastGrowthLimit')
        if (sf := data.get('pregnancyUdderGrowthLimit')) is not None:
            self.pregnancyUdderGrowthLimit = data['pregnancyUdderGrowthLimit']
        else:
            raise KeyRequiredException(self, data, 'pregnancyUdderGrowthLimit', 'pregnancyUdderGrowthLimit')
        if (sf := data.get('pregnancyLactationIncreaseVariance')) is not None:
            self.pregnancyLactationIncreaseVariance = data['pregnancyLactationIncreaseVariance']
        else:
            raise KeyRequiredException(self, data, 'pregnancyLactationIncreaseVariance', 'pregnancyLactationIncreaseVariance')
        if (sf := data.get('pregnancyLactationIncrease')) is not None:
            self.pregnancyLactationIncrease = data['pregnancyLactationIncrease']
        else:
            raise KeyRequiredException(self, data, 'pregnancyLactationIncrease', 'pregnancyLactationIncrease')
        if (sf := data.get('pregnancyUdderLactationIncrease')) is not None:
            self.pregnancyUdderLactationIncrease = data['pregnancyUdderLactationIncrease']
        else:
            raise KeyRequiredException(self, data, 'pregnancyUdderLactationIncrease', 'pregnancyUdderLactationIncrease')
        if (sf := data.get('pregnancyLactationLimit')) is not None:
            self.pregnancyLactationLimit = data['pregnancyLactationLimit']
        else:
            raise KeyRequiredException(self, data, 'pregnancyLactationLimit', 'pregnancyLactationLimit')
        if (sf := data.get('pregnancyUdderLactationLimit')) is not None:
            self.pregnancyUdderLactationLimit = data['pregnancyUdderLactationLimit']
        else:
            raise KeyRequiredException(self, data, 'pregnancyUdderLactationLimit', 'pregnancyUdderLactationLimit')
        if (sf := data.get('breastSizePreference')) is not None:
            self.breastSizePreference = data['breastSizePreference']
        else:
            raise KeyRequiredException(self, data, 'breastSizePreference', 'breastSizePreference')
        if (sf := data.get('udderSizePreference')) is not None:
            self.udderSizePreference = data['udderSizePreference']
        else:
            raise KeyRequiredException(self, data, 'udderSizePreference', 'udderSizePreference')
        if (sf := data.get('penisSizePreference')) is not None:
            self.penisSizePreference = data['penisSizePreference']
        else:
            raise KeyRequiredException(self, data, 'penisSizePreference', 'penisSizePreference')
        if (sf := data.get('trapPenisSizePreference')) is not None:
            self.trapPenisSizePreference = data['trapPenisSizePreference']
        else:
            raise KeyRequiredException(self, data, 'trapPenisSizePreference', 'trapPenisSizePreference')
        if (sf := data.get('clothingFemininityLevel')) is not None:
            self.clothingFemininityLevel = data['clothingFemininityLevel']
        else:
            raise KeyRequiredException(self, data, 'clothingFemininityLevel', 'clothingFemininityLevel')
        if (sf := data.get('forcedFetishPercentage')) is not None:
            self.forcedFetishPercentage = data['forcedFetishPercentage']
        else:
            raise KeyRequiredException(self, data, 'forcedFetishPercentage', 'forcedFetishPercentage')
        if (sf := data.get('difficultyLevel')) is not None:
            self.difficultyLevel = data['difficultyLevel'].name
        else:
            raise KeyRequiredException(self, data, 'difficultyLevel', 'difficultyLevel')
        if (sf := data.get('AIblunderRate')) is not None:
            self.AIblunderRate = data['AIblunderRate']
        else:
            raise KeyRequiredException(self, data, 'AIblunderRate', 'AIblunderRate')
        if (sf := data.get('forcedTFPreference')) is not None:
            self.forcedTFPreference = data['forcedTFPreference'].name
        else:
            raise KeyRequiredException(self, data, 'forcedTFPreference', 'forcedTFPreference')
        if (sf := data.get('forcedTFTendency')) is not None:
            self.forcedTFTendency = data['forcedTFTendency'].name
        else:
            raise KeyRequiredException(self, data, 'forcedTFTendency', 'forcedTFTendency')
        if (sf := data.get('forcedFetishTendency')) is not None:
            self.forcedFetishTendency = data['forcedFetishTendency'].name
        else:
            raise KeyRequiredException(self, data, 'forcedFetishTendency', 'forcedFetishTendency')

from lilitools.saves.properties._savables.age_preference import RawAgePreference
__all__ = ['AgePreference']


class AgePreference(RawAgePreference):
    pass

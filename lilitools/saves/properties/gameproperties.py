from typing import Any, Dict, Optional
from lxml import etree

from lilitools.saves.properties._savables.properties import RawGameProperties
from lilitools.saves.properties.subspecies_preference import SubspeciesPreference

__all__ = ["GameProperties"]


class GameProperties(RawGameProperties):
    INSTANCE_LOADING: Optional["GameProperties"] = None
    INSTANCE: Optional["GameProperties"] = None

    def __init__(self) -> None:
        super().__init__()
        self.show_warnings_during_load: bool = False
        self.load_quietly: bool = False

        self.subspeciesPreferences: Dict[str, SubspeciesPreference] = {}

    def fromXML(self, e: etree._Element) -> None:
        self.INSTANCE_LOADING = self
        super().fromXML(e)
        self.INSTANCE_LOADING = None
        self.INSTANCE = self

    def fromXMLBase(self, e: etree.ElementBase) -> None:
        if isinstance(e, etree._ElementTree):
            e = e.getroot()
        self.fromXML(e)

    def _fromXML_subspeciesPreferences(self, e: etree._Element) -> None:
        if (subspeciesPreferences := e.find("subspeciesPreferences")) is not None:
            for pref in subspeciesPreferences:
                subspecies = pref.attrib["subspecies"]
                if subspecies not in self.subspeciesPreferences:
                    ssp = SubspeciesPreference()
                    self.subspeciesPreferences[subspecies] = ssp
                ssp = self.subspeciesPreferences[subspecies]
                match pref.tag:
                    case "preferenceFeminine":
                        ssp.preferenceFeminine.fromXML(pref)
                    case "preferenceMasculine":
                        ssp.preferenceMasculine.fromXML(pref)
                    case _:
                        raise ValueError(
                            f"Unhandled tag {pref.tag} (expecting preferenceFeminine, preferenceMasculine)"
                        )

    def _toXML_subspeciesPreferences(self, e: etree._Element) -> None:
        subspeciesPreferences = etree.SubElement(e, "subspeciesPreferences")
        for k, v in sorted(self.subspeciesPreferences.items(), key=lambda x: x[0]):
            v.toXML(subspeciesPreferences)

    def _toDict_subspeciesPreferences(self, data: Dict[str, Any]) -> None:
        o = {}
        for ssID, ssp in self.subspeciesPreferences.items():
            o[ssID] = ssp.toDict()
        data["subspeciesPreferences"] = o

    def _fromDict_subspeciesPreferences(self, data: Dict[str, Any]) -> None:
        self.subspeciesPreferences = {}
        if (sspDict := data["subspeciesPreferences"]) is not None:
            for ssID, sspData in sspDict.items():
                ssp = SubspeciesPreference()
                ssp.fromDict(sspData)
                self.subspeciesPreferences[ssID] = ssp

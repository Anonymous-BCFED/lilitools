from lilitools.saves.properties._savables.gender_name import RawGenderName

__all__ = ['GenderName']


class GenderName(RawGenderName):
    pass

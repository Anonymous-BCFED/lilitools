from lilitools.saves.properties._savables.gender_preference import RawGenderPreference
__all__ = ['GenderPreference']


class GenderPreference(RawGenderPreference):
    pass

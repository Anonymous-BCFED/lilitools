
from lilitools.saves.properties._savables.gender_pronoun import RawGenderPronoun
__all__ = ['GenderPronoun']


class GenderPronoun(RawGenderPronoun):
    pass

from lilitools.saves.properties._savables.keybind import RawKeyBind
__all__ = ['KeyBind']


class KeyBind(RawKeyBind):
    pass

from lilitools.saves.properties._savables.orientation_preference import RawOrientationPreference
__all__ = ['OrientationPreference']


class OrientationPreference(RawOrientationPreference):
    pass

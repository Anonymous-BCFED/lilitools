__all__ = ['PreviousSaveInfo']
from lilitools.saves.properties._savables.previous_save import RawPreviousSaveInfo


class PreviousSaveInfo(RawPreviousSaveInfo):
    pass

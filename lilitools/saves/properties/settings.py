from lilitools.saves.properties._savables.settings import RawGameSettings
__all__ = ['GameSettings']


class GameSettings(RawGameSettings):
    pass

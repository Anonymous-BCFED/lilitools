from lilitools.saves.properties._savables.skin_colour_preference import RawSkinColourPreference

__all__ = ['SkinColourPreference']


class SkinColourPreference(RawSkinColourPreference):
    pass

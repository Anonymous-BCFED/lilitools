"""
Because:
 <subspeciesPreferences>
    <preferenceFeminine furryPreference="NORMAL" preference="FOUR_ABUNDANT" subspecies="ALLIGATOR_MORPH"/>
    <preferenceMasculine furryPreference="NORMAL" preference="FOUR_ABUNDANT" subspecies="ALLIGATOR_MORPH"/>
    <preferenceFeminine furryPreference="NORMAL" preference="FOUR_ABUNDANT" subspecies="ANGEL"/>
    <preferenceMasculine furryPreference="NORMAL" preference="FOUR_ABUNDANT" subspecies="ANGEL"/>
    <preferenceFeminine furryPreference="NORMAL" preference="TWO_AVERAGE" subspecies="innoxia_badger_subspecies_badger"/>
    <preferenceMasculine furryPreference="NORMAL" preference="TWO_AVERAGE" subspecies="innoxia_badger_subspecies_badger"/>
    <preferenceFeminine furryPreference="NORMAL" preference="FOUR_ABUNDANT" subspecies="BAT_MORPH"/>
    <preferenceMasculine furryPreference="NORMAL" preference="FOUR_ABUNDANT" subspecies="BAT_MORPH"/>
"""

from typing import Any, Dict
from lilitools.saves.enums.furry_preference import EFurryPreference
from lilitools.saves.enums.subspecies_preference import ESubspeciesPreference
from lilitools.saves.exceptions import AttributeRequiredException
from lilitools.saves.savable import Savable
from lxml import etree


class SubspeciesGenderPreference:
    def __init__(self, tag: str) -> None:
        self.tag: str = tag
        self.subspecies: str = ""
        self.preference: ESubspeciesPreference = ESubspeciesPreference(0)
        self.furryPreference: EFurryPreference = EFurryPreference(0)

    def fromXML(self, e: etree._Element) -> None:
        value = e.get("subspecies", None)
        if value is None:
            raise AttributeRequiredException(self, e, "subspecies", e.tag)
        else:
            self.subspecies = str(value)
        value = e.get("preference", None)
        if value is None:
            raise AttributeRequiredException(self, e, "preference", e.tag)
        else:
            self.preference = ESubspeciesPreference[str(value)]
        value = e.get("furryPreference", None)
        if value is None:
            raise AttributeRequiredException(self, e, "furryPreference", e.tag)
        else:
            self.furryPreference = EFurryPreference[str(value)]

    def toXML(self, subspecies: str) -> etree._Element:
        return etree.Element(
            self.tag,
            {
                "subspecies": subspecies,
                "preference": self.preference.name,
                "furryPreference": self.furryPreference.name,
            },
        )

    def fromDict(self, data: Dict[str, Any], subspecies: str) -> None:
        self.subspecies = subspecies
        self.preference = ESubspeciesPreference[data["preference"]]
        self.furryPreference = EFurryPreference[data["furryPreference"]]

    def toDict(self) -> Dict[str, Any]:
        return {
            "subspecies": self.subspecies,
            "preference": self.preference.name,
            "furryPreference": self.furryPreference.name,
        }


class SubspeciesPreference:
    def __init__(self) -> None:
        self.subspecies: str = ""
        self.preferenceFeminine: SubspeciesGenderPreference = (
            SubspeciesGenderPreference("preferenceFeminine")
        )
        self.preferenceMasculine: SubspeciesGenderPreference = (
            SubspeciesGenderPreference("preferenceMasculine")
        )

    def toXML(self, e: etree._Element) -> None:
        e.append(self.preferenceFeminine.toXML(self.subspecies))
        e.append(self.preferenceMasculine.toXML(self.subspecies))

    def toDict(self) -> Dict[str, Any]:
        return {
            "subspecies": self.subspecies,
            "feminine": self.preferenceFeminine.toDict(self.subspecies),
            "masculine": self.preferenceMasculine.toDict(self.subspecies),
        }

    def fromDict(self, data: Dict[str, Any]) -> None:
        self.subspecies = data["subspecies"]
        self.preferenceFeminine = SubspeciesGenderPreference("preferenceFeminine")
        self.preferenceFeminine.fromDict(data["feminine"], self.subspecies)
        self.preferenceMasculine = SubspeciesGenderPreference("preferenceMasculine")
        self.preferenceMasculine.fromDict(data["masculine"], self.subspecies)

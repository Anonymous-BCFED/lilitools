#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.savable import Savable
__all__ = ['RawQuestUpdates']
## from [LT]/src/com/lilithsthrone/game/character/PlayerCharacter.java: public Element saveAsXML(Element parentElement, Document doc) { @ sBS0CtsgLnnbSc82BIV+fwR5ta5F0fIcwARiSmPFyUfGDlGC/PW8odPH7ZSdA0mCb6EhDaL4Qc8faQlc4W6sRA==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawQuestUpdates(Savable):
    TAG = 'questUpdates'

    def __init__(self) -> None:
        super().__init__()

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        return super().toXML(tagOverride)

    def toDict(self) -> Dict[str, Any]:
        return super().toDict()

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)

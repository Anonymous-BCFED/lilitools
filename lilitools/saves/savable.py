from enum import Enum
from typing import Any, Callable, Dict, List, Optional, Tuple, Type
from frozendict import frozendict

from lxml import etree


def name2enum(etype: Type[Enum]) -> Callable[[str], Enum]:
    def inner(value: str) -> Enum:
        return etype[value]

    return inner


def enum2name(etype: Type[Enum]) -> Callable[[Enum], str]:
    def inner(value: Enum) -> str:
        return value.name

    return inner


XML2BOOL: frozendict[str, bool] = frozendict({"true": True, "false": False})


class Savable:
    TAG: str = ""
    # ATTRMAP: List[TAttrBinding] = []

    def __init__(self) -> None:
        pass

    def overrideAttrs(self) -> None:
        pass

    def overrideTags(self) -> None:
        pass

    def toXML(self, tagOverride: Optional[str] = None) -> etree._Element:
        e = etree.Element(tagOverride or self.TAG)
        # if len(self.ATTRMAP):
        #     for k, a, conv, _, optional in self.ATTRMAP:
        #         av = getattr(self, k, None)
        #         if av is not None:
        #             e.attrib[a] = conv(av)
        #         else:
        #             assert optional, f'Savable {self.__class__.__name__} attr {k} -> {a} is None and not optional!'
        return e

    def fromXML(self, e: etree._Element) -> None:
        # print(etree.tostring(e))
        # if len(self.ATTRMAP):
        #     for k, a, _, deconv, optional in self.ATTRMAP:
        #         #print(self.TAG, a, e.attrib.get(k))
        #         v = e.get(a, None)
        #         if v is not None:
        #             setattr(self, k, deconv(v))
        #         else:
        #             assert optional, f'Savable {self.__class__.__name__} attr {k} -> {a} is None and not optional!'
        pass

    def toDict(self) -> Dict[str, Any]:
        return {}

    def fromDict(self, data: Dict[str, Any]) -> None:
        pass

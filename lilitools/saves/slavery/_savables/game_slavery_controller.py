#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from collections import OrderedDict
from typing import Any, Dict, Optional, Set
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
from lilitools.saves.slavery.enums.slave_jobs import ESlaveJob
from lilitools.saves.slavery.milking_room import MilkingRoom
__all__ = ['RawGameSlaveryController']
## from [LT]/src/com/lilithsthrone/game/occupantManagement/OccupancyUtil.java: public Element saveAsXML(Element parentElement, Document doc) { @ HT/4lUEZlJozQ3hRoEi7DxeUYo69JylkWYyrEAud6pGqByZHuSVTbpEAFGqVu2YpiNpTgcP6SfJ3WjUvclaGnw==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawGameSlaveryController(Savable):
    TAG = 'slavery'
    _XMLID_ATTR_GENERATEDINCOME_ATTRIBUTE: str = 'generatedIncome'
    _XMLID_ATTR_GENERATEDUPKEEP_ATTRIBUTE: str = 'generatedUpkeep'
    _XMLID_TAG_MILKINGROOMS_CHILD: str = 'milkingRoom'
    _XMLID_TAG_MILKINGROOMS_VALUEELEM: Optional[str] = None

    def __init__(self) -> None:
        super().__init__()
        self.generatedIncome: int = 0
        self.generatedUpkeep: int = 0
        self.slavesAtJob: OrderedDict[ESlaveJob, Set[str]] = OrderedDict({k: set() for k in ESlaveJob})
        self.milkingRooms: Set[MilkingRoom] = set()

    def overrideAttrs(self, generatedIncome_attribute: _Optional_str = None, generatedUpkeep_attribute: _Optional_str = None) -> None:
        if generatedIncome_attribute is not None:
            self._XMLID_ATTR_GENERATEDINCOME_ATTRIBUTE = generatedIncome_attribute
        if generatedUpkeep_attribute is not None:
            self._XMLID_ATTR_GENERATEDUPKEEP_ATTRIBUTE = generatedUpkeep_attribute

    def overrideTags(self, milkingRooms_child: _Optional_str = None, milkingRooms_valueelem: _Optional_str = None) -> None:
        if milkingRooms_child is not None:
            self._XMLID_TAG_MILKINGROOMS_CHILD = milkingRooms_child
        if milkingRooms_valueelem is not None:
            self._XMLID_TAG_MILKINGROOMS_VALUEELEM = milkingRooms_valueelem

    def _fromXML_slavesAtJob(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromXML_slavesAtJob()')

    def _toXML_slavesAtJob(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toXML_slavesAtJob()')

    def _fromDict_slavesAtJob(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromDict_slavesAtJob()')

    def _toDict_slavesAtJob(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toDict_slavesAtJob()')

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        lv: Savable
        value = e.get(self._XMLID_ATTR_GENERATEDINCOME_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_GENERATEDINCOME_ATTRIBUTE, 'generatedIncome')
        else:
            self.generatedIncome = int(value)
        value = e.get(self._XMLID_ATTR_GENERATEDUPKEEP_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_GENERATEDUPKEEP_ATTRIBUTE, 'generatedUpkeep')
        else:
            self.generatedUpkeep = int(value)
        self._fromXML_slavesAtJob(e)
        for lc in e.iterfind(self._XMLID_TAG_MILKINGROOMS_CHILD):
            lv = MilkingRoom()
            lv.fromXML(lc)
            self.milkingRooms.add(lv)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        e.attrib[self._XMLID_ATTR_GENERATEDINCOME_ATTRIBUTE] = str(self.generatedIncome)
        e.attrib[self._XMLID_ATTR_GENERATEDUPKEEP_ATTRIBUTE] = str(self.generatedUpkeep)
        self._toXML_slavesAtJob(e)
        for lv in sorted(self.milkingRooms, key=lambda x: (x.worldType, x.x, x.y)):
            e.append(lv.toXML())
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['generatedIncome'] = int(self.generatedIncome)
        data['generatedUpkeep'] = int(self.generatedUpkeep)
        self._toDict_slavesAtJob(data)
        if self.milkingRooms is None or len(self.milkingRooms) > 0:
            data['milkingRooms'] = list()
        else:
            lc = list()
            if len(self.milkingRooms) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['milkingRooms'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'generatedIncome' not in data:
            raise KeyRequiredException(self, data, 'generatedIncome', 'generatedIncome')
        self.generatedIncome = int(data['generatedIncome'])
        if 'generatedUpkeep' not in data:
            raise KeyRequiredException(self, data, 'generatedUpkeep', 'generatedUpkeep')
        self.generatedUpkeep = int(data['generatedUpkeep'])
        self._fromDict_slavesAtJob(data)
        if (lv := self.milkingRooms) is not None:
            for le in lv:
                self.milkingRooms.add(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'milkingRooms', 'milkingRooms')

#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from collections import OrderedDict
from typing import Any, Dict, List, Optional, Set
from lxml import etree
from lilitools.saves.character.fluids.stored_fluid import StoredFluid
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.items.abstract_clothing import AbstractClothing
from lilitools.saves.savable import Savable
__all__ = ['RawMilkingRoom']
## from [LT]/src/com/lilithsthrone/game/occupantManagement/MilkingRoom.java: public Element saveAsXML(Element parentElement, Document doc) { @ S/yFHITBuAgZ7DHHwIlylDPOblaVDKCHV4/X7VnBVOfoPPeSYAaAhGRKzD1ITxoA1kfsDe80VioEq0k68H9flg==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawMilkingRoom(Savable):
    TAG = 'milkingRoom'
    _XMLID_ATTR_WORLDTYPE_ATTRIBUTE: str = 'worldType'
    _XMLID_ATTR_X_ATTRIBUTE: str = 'x'
    _XMLID_ATTR_Y_ATTRIBUTE: str = 'y'
    _XMLID_TAG_FLUIDSSTORED_CHILD: str = 'fluidStored'
    _XMLID_TAG_FLUIDSSTORED_VALUEELEM: Optional[str] = None

    def __init__(self) -> None:
        super().__init__()
        self.worldType: str = ''
        self.x: int = 0
        self.y: int = 0
        self.fluidsStored: Set[StoredFluid] = set()
        self.clothingRemovedForMilking: OrderedDict[str, List[AbstractClothing]] = OrderedDict()

    def overrideAttrs(self, worldType_attribute: _Optional_str = None, x_attribute: _Optional_str = None, y_attribute: _Optional_str = None) -> None:
        if worldType_attribute is not None:
            self._XMLID_ATTR_WORLDTYPE_ATTRIBUTE = worldType_attribute
        if x_attribute is not None:
            self._XMLID_ATTR_X_ATTRIBUTE = x_attribute
        if y_attribute is not None:
            self._XMLID_ATTR_Y_ATTRIBUTE = y_attribute

    def overrideTags(self, fluidsStored_child: _Optional_str = None, fluidsStored_valueelem: _Optional_str = None) -> None:
        if fluidsStored_child is not None:
            self._XMLID_TAG_FLUIDSSTORED_CHILD = fluidsStored_child
        if fluidsStored_valueelem is not None:
            self._XMLID_TAG_FLUIDSSTORED_VALUEELEM = fluidsStored_valueelem

    def _fromXML_clothingRemovedForMilking(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromXML_clothingRemovedForMilking()')

    def _toXML_clothingRemovedForMilking(self, e: etree._Element) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toXML_clothingRemovedForMilking()')

    def _fromDict_clothingRemovedForMilking(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._fromDict_clothingRemovedForMilking()')

    def _toDict_clothingRemovedForMilking(self, data: Dict[str, Any]) -> None:
        raise NotImplementedError('Not implemented: ' + self.__class__.__name__ + '._toDict_clothingRemovedForMilking()')

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        lv: Savable
        value = e.get(self._XMLID_ATTR_WORLDTYPE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_WORLDTYPE_ATTRIBUTE, 'worldType')
        else:
            self.worldType = str(value)
        value = e.get(self._XMLID_ATTR_X_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_X_ATTRIBUTE, 'x')
        else:
            self.x = int(value)
        value = e.get(self._XMLID_ATTR_Y_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_Y_ATTRIBUTE, 'y')
        else:
            self.y = int(value)
        for lc in e.iterfind(self._XMLID_TAG_FLUIDSSTORED_CHILD):
            lv = StoredFluid()
            lv.fromXML(lc)
            self.fluidsStored.add(lv)
        self._fromXML_clothingRemovedForMilking(e)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        e.attrib[self._XMLID_ATTR_WORLDTYPE_ATTRIBUTE] = str(self.worldType)
        e.attrib[self._XMLID_ATTR_X_ATTRIBUTE] = str(self.x)
        e.attrib[self._XMLID_ATTR_Y_ATTRIBUTE] = str(self.y)
        for lv in sorted(self.fluidsStored, key=lambda x: (x.charactersFluidID, x.cum is not None, x.girlCum is not None, x.milk is not None)):
            e.append(lv.toXML())
        self._toXML_clothingRemovedForMilking(e)
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['worldType'] = self.worldType
        data['x'] = int(self.x)
        data['y'] = int(self.y)
        if self.fluidsStored is None or len(self.fluidsStored) > 0:
            data['fluidsStored'] = list()
        else:
            lc = list()
            if len(self.fluidsStored) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['fluidsStored'] = lc
        self._toDict_clothingRemovedForMilking(data)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'worldType' not in data:
            raise KeyRequiredException(self, data, 'worldType', 'worldType')
        self.worldType = str(data['worldType'])
        if 'x' not in data:
            raise KeyRequiredException(self, data, 'x', 'x')
        self.x = int(data['x'])
        if 'y' not in data:
            raise KeyRequiredException(self, data, 'y', 'y')
        self.y = int(data['y'])
        if (lv := self.fluidsStored) is not None:
            for le in lv:
                self.fluidsStored.add(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'fluidsStored', 'fluidsStored')
        self._fromDict_clothingRemovedForMilking(data)

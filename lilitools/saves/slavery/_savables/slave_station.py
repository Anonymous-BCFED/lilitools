#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.math.point2i import Point2I
from lilitools.saves.savable import Savable
__all__ = ['RawSlaveStation']
## from [LT]/src/com/lilithsthrone/game/character/GameCharacter.java: public Element saveAsXML(Element parentElement, Document doc) { @ Ra1rfw7eKd5WT2JXoGa/zLFvb/XgZlpPNZbY2BZwgdTA/t0DkASy5ADAtBbX2DvSTxI43cQAGTPbPsMVcAHyRQ==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawSlaveStation(Savable):
    TAG = 'slaveStation'
    _XMLID_ATTR_LOCATION_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_WORLDTYPE_ATTRIBUTE: str = 'value'
    _XMLID_TAG_LOCATION_ELEMENT: str = 'location'
    _XMLID_TAG_WORLDTYPE_ELEMENT: str = 'worldType'

    def __init__(self) -> None:
        super().__init__()
        self.worldType: str = ''  # Element
        self.location: Point2I = Point2I()  # Element

    def overrideAttrs(self, location_attribute: _Optional_str = None, worldType_attribute: _Optional_str = None) -> None:
        if location_attribute is not None:
            self._XMLID_ATTR_LOCATION_ATTRIBUTE = location_attribute
        if worldType_attribute is not None:
            self._XMLID_ATTR_WORLDTYPE_ATTRIBUTE = worldType_attribute

    def overrideTags(self, location_element: _Optional_str = None, worldType_element: _Optional_str = None) -> None:
        if location_element is not None:
            self._XMLID_TAG_LOCATION_ELEMENT = location_element
        if worldType_element is not None:
            self._XMLID_TAG_WORLDTYPE_ELEMENT = worldType_element

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_WORLDTYPE_ELEMENT)) is not None:
            if self._XMLID_ATTR_WORLDTYPE_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_WORLDTYPE_ATTRIBUTE, 'worldType')
            self.worldType = sf.attrib[self._XMLID_ATTR_WORLDTYPE_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'worldType', self._XMLID_TAG_WORLDTYPE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_LOCATION_ELEMENT)) is not None:
            self.location = Point2I()
            self.location.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'location', self._XMLID_TAG_LOCATION_ELEMENT)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        etree.SubElement(e, self._XMLID_TAG_WORLDTYPE_ELEMENT, {self._XMLID_ATTR_WORLDTYPE_ATTRIBUTE: str(self.worldType)})
        e.append(self.location.toXML(self._XMLID_TAG_LOCATION_ELEMENT))
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['worldType'] = str(self.worldType)
        data['location'] = self.location.toDict()
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('worldType')) is not None:
            self.worldType = data['worldType']
        else:
            raise KeyRequiredException(self, data, 'worldType', 'worldType')
        if (sf := data.get('location')) is not None:
            self.location = Point2I()
            self.location.fromDict(data['location'])
        else:
            raise KeyRequiredException(self, data, 'location', 'location')

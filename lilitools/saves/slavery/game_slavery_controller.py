import collections
from typing import Any, Dict, List, Optional, OrderedDict, Set

from lxml import etree
from lilitools.consts import PRESERVE_SAVE_ORDERING

from lilitools.saves.slavery._savables.game_slavery_controller import (
    RawGameSlaveryController,
)
from lilitools.saves.slavery.enums.slave_jobs import ESlaveJob


class GameSlaveryController(RawGameSlaveryController):

    def getAllWorkingSlaves(self) -> Set[str]:
        o = set()
        for slaves in self.slavesAtJob.values():
            o.update(slaves)
        return o

    # Placeholder implementations
    if PRESERVE_SAVE_ORDERING:

        def _fromXML_slavesAtJob(self, e: etree._Element) -> None:
            slavesAtJob: Optional[etree._Element]
            slavesElement: etree._Element
            idEl: etree._Element
            newSlavesAtJob: OrderedDict[str, List[str]] = collections.OrderedDict()
            if (slavesAtJob := e.find("slavesAtJob")) is not None:
                for slavesElement in slavesAtJob.findall("slaves"):
                    job: ESlaveJob = ESlaveJob[slavesElement.attrib["job"]]
                    newSlavesAtJob[job] = list()
                    for idEl in slavesElement.findall("id"):
                        newSlavesAtJob[job].append(idEl.text or "")
            self.slavesAtJob = newSlavesAtJob

        def _toXML_slavesAtJob(self, e: etree._Element) -> None:
            slavesAtJob = etree.SubElement(e, "slavesAtJob")
            for slaveJob, slaves in self.slavesAtJob.items():
                if len(slaves):
                    slavesInJob = etree.SubElement(
                        slavesAtJob, "slaves", {"job": slaveJob.name}
                    )
                    for slave in slaves:
                        etree.SubElement(slavesInJob, "id", {}).text = slave

    else:

        def _fromXML_slavesAtJob(self, e: etree._Element) -> None:
            slavesAtJob: Optional[etree._Element]
            slavesElement: etree._Element
            idEl: etree._Element
            if (slavesAtJob := e.find("slavesAtJob")) is not None:
                for slavesElement in slavesAtJob.findall("slaves"):
                    job: ESlaveJob = ESlaveJob[slavesElement.attrib["job"]]
                    self.slavesAtJob[job] = set()
                    for idEl in slavesElement.findall("id"):
                        self.slavesAtJob[job].add(idEl.text or "")

        def _toXML_slavesAtJob(self, e: etree._Element) -> None:
            slavesAtJob = etree.SubElement(e, "slavesAtJob")
            for slaveJob, slaves in sorted(
                self.slavesAtJob.items(), key=lambda t: t[0].name
            ):
                if len(slaves):
                    slavesInJob = etree.SubElement(
                        slavesAtJob, "slaves", {"job": slaveJob.name}
                    )
                    for slave in sorted(slaves):
                        etree.SubElement(slavesInJob, "id", {}).text = slave

    def _fromDict_slavesAtJob(self, data: Dict[str, Any]) -> None:
        self.slavesAtJob = {}
        if (slavesAtJob := data.get("slavesAtJob")) is not None:
            for job, slaves in slavesAtJob.items():
                if len(slaves):
                    self.slavesAtJob[ESlaveJob[job]] = set(str(x) for x in slaves)

    def _toDict_slavesAtJob(self, data: Dict[str, Any]) -> None:
        if len(self.slavesAtJob):
            o: Dict[str, List[str]] = {}
            for job, slaves in sorted(
                self.slavesAtJob.items(), key=lambda t: t[0].name
            ):
                if len(slaves):
                    o[job.name] = list(sorted(slaves))
            data["slavesAtJob"] = o

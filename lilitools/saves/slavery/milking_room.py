from typing import Any, Dict, List, Set

from lxml import etree
from lilitools.consts import PRESERVE_SAVE_ORDERING

from lilitools.saves.items.abstract_clothing import AbstractClothing
from lilitools.saves.slavery._savables.milking_room import RawMilkingRoom


class MilkingRoom(RawMilkingRoom):
    if PRESERVE_SAVE_ORDERING:

        def _toXML_clothingRemovedForMilking(self, e: etree._Element) -> None:
            if len(self.clothingRemovedForMilking):
                charsWithSavedClothing = etree.SubElement(
                    e, "clothingRemovedForMilking"
                )
                charID: str
                clothes: List[AbstractClothing]
                clothing: AbstractClothing
                for charID, clothes in self.clothingRemovedForMilking.items():
                    charClothing = etree.SubElement(
                        charsWithSavedClothing, "clothingCharacter", {"id": charID}
                    )
                    for clothing in clothes:
                        charClothing.append(clothing.toXML())

    else:

        def _toXML_clothingRemovedForMilking(self, e: etree._Element) -> None:
            if len(self.clothingRemovedForMilking):
                charsWithSavedClothing = etree.SubElement(
                    e, "clothingRemovedForMilking"
                )
                charID: str
                clothes: List[AbstractClothing]
                clothing: AbstractClothing
                for charID, clothes in sorted(
                    self.clothingRemovedForMilking.items(), key=lambda t: t[0]
                ):
                    charClothing = etree.SubElement(
                        charsWithSavedClothing, "clothingCharacter", {"id": charID}
                    )
                    for clothing in sorted(clothes, key=lambda x: (x.id, x.name)):
                        charClothing.append(clothing.toXML())

    def _fromXML_clothingRemovedForMilking(self, e: etree._Element) -> None:
        ccElem: etree._Element
        if (crfmElem := e.find("clothingRemovedForMilking")) is not None:
            for ccElem in crfmElem.getchildren():
                clothingSet = list()
                for clothingElement in ccElem.getchildren():
                    c = AbstractClothing()
                    c.fromXML(clothingElement)
                    clothingSet.append(c)
                self.clothingRemovedForMilking[ccElem.attrib["id"]] = clothingSet

    def _toDict_clothingRemovedForMilking(self, data: Dict[str, Any]) -> None:
        if len(self.clothingRemovedForMilking):
            out: Dict[str, List[dict]] = {}
            for charID, clothes in sorted(
                self.clothingRemovedForMilking.items(), key=lambda t: t[0]
            ):
                cset: List[dict] = {}
                for clothing in sorted(clothes, key=lambda x: (x.id, x.name)):
                    cset.append(clothing.toDict())
                out[charID] = cset
            data["clothingRemovedForMilking"] = out

    def _fromDict_clothingRemovedForMilking(self, data: Dict[str, Any]) -> None:
        self.clothingRemovedForMilking = {}
        if (csets := data.get("clothingRemovedForMilking")) is not None:
            for charID, csetData in csets.items():
                cset: List[AbstractClothing] = list()
                for cdata in csetData:
                    c = AbstractClothing()
                    c.fromDict(cdata)
                    cset.append(c)
                self.clothingRemovedForMilking[charID] = cset

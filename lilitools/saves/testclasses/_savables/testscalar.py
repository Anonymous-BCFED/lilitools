#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.date import Date
from lilitools.saves.enums.weather import EWeather
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.savable import XML2BOOL, Savable
__all__ = ['RawTestScalar']
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawTestScalar(Savable):
    TAG = 'testScalar'
    _XMLID_ATTR_BOOLEAN_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_DATE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_FLOAT_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_INTEGER_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_INTFROMTEXT_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_OPTIONALSTRING_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_STRING_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_WEATHER_ATTRIBUTE: str = 'value'
    _XMLID_TAG_BOOLEAN_ELEMENT: str = 'boolean'
    _XMLID_TAG_DATE_ELEMENT: str = 'date'
    _XMLID_TAG_FLOAT_ELEMENT: str = 'float'
    _XMLID_TAG_INTEGER_ELEMENT: str = 'integer'
    _XMLID_TAG_INTFROMTEXT_ELEMENT: str = 'intFromText'
    _XMLID_TAG_OPTIONALSTRING_ELEMENT: str = 'optionalString'
    _XMLID_TAG_STRING_ELEMENT: str = 'string'
    _XMLID_TAG_WEATHER_ELEMENT: str = 'weather'

    def __init__(self) -> None:
        super().__init__()
        self.string: str = ''  # Element
        self.integer: int = 0  # Element
        self.float: float = 0.  # Element
        self.boolean: bool = False  # Element
        self.optionalString: Optional[str] = None  # Element
        self.weather: EWeather = EWeather(0)  # Element
        self.date: Date = Date()  # Element
        self.intFromText: int = 0  # Element

    def overrideAttrs(self, boolean_attribute: _Optional_str = None, date_attribute: _Optional_str = None, float_attribute: _Optional_str = None, intFromText_attribute: _Optional_str = None, integer_attribute: _Optional_str = None, optionalString_attribute: _Optional_str = None, string_attribute: _Optional_str = None, weather_attribute: _Optional_str = None) -> None:
        if boolean_attribute is not None:
            self._XMLID_ATTR_BOOLEAN_ATTRIBUTE = boolean_attribute
        if date_attribute is not None:
            self._XMLID_ATTR_DATE_ATTRIBUTE = date_attribute
        if float_attribute is not None:
            self._XMLID_ATTR_FLOAT_ATTRIBUTE = float_attribute
        if intFromText_attribute is not None:
            self._XMLID_ATTR_INTFROMTEXT_ATTRIBUTE = intFromText_attribute
        if integer_attribute is not None:
            self._XMLID_ATTR_INTEGER_ATTRIBUTE = integer_attribute
        if optionalString_attribute is not None:
            self._XMLID_ATTR_OPTIONALSTRING_ATTRIBUTE = optionalString_attribute
        if string_attribute is not None:
            self._XMLID_ATTR_STRING_ATTRIBUTE = string_attribute
        if weather_attribute is not None:
            self._XMLID_ATTR_WEATHER_ATTRIBUTE = weather_attribute

    def overrideTags(self, boolean_element: _Optional_str = None, date_element: _Optional_str = None, float_element: _Optional_str = None, intFromText_element: _Optional_str = None, integer_element: _Optional_str = None, optionalString_element: _Optional_str = None, string_element: _Optional_str = None, weather_element: _Optional_str = None) -> None:
        if boolean_element is not None:
            self._XMLID_TAG_BOOLEAN_ELEMENT = boolean_element
        if date_element is not None:
            self._XMLID_TAG_DATE_ELEMENT = date_element
        if float_element is not None:
            self._XMLID_TAG_FLOAT_ELEMENT = float_element
        if intFromText_element is not None:
            self._XMLID_TAG_INTFROMTEXT_ELEMENT = intFromText_element
        if integer_element is not None:
            self._XMLID_TAG_INTEGER_ELEMENT = integer_element
        if optionalString_element is not None:
            self._XMLID_TAG_OPTIONALSTRING_ELEMENT = optionalString_element
        if string_element is not None:
            self._XMLID_TAG_STRING_ELEMENT = string_element
        if weather_element is not None:
            self._XMLID_TAG_WEATHER_ELEMENT = weather_element

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        sf: etree._Element
        if (sf := e.find(self._XMLID_TAG_STRING_ELEMENT)) is not None:
            if self._XMLID_ATTR_STRING_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_STRING_ATTRIBUTE, 'string')
            self.string = sf.attrib[self._XMLID_ATTR_STRING_ATTRIBUTE]
        else:
            raise ElementRequiredException(self, e, 'string', self._XMLID_TAG_STRING_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_INTEGER_ELEMENT)) is not None:
            if self._XMLID_ATTR_INTEGER_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_INTEGER_ATTRIBUTE, 'integer')
            self.integer = int(sf.attrib[self._XMLID_ATTR_INTEGER_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'integer', self._XMLID_TAG_INTEGER_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_FLOAT_ELEMENT)) is not None:
            if self._XMLID_ATTR_FLOAT_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_FLOAT_ATTRIBUTE, 'float')
            self.float = float(sf.attrib[self._XMLID_ATTR_FLOAT_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'float', self._XMLID_TAG_FLOAT_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_BOOLEAN_ELEMENT)) is not None:
            if self._XMLID_ATTR_BOOLEAN_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_BOOLEAN_ATTRIBUTE, 'boolean')
            self.boolean = XML2BOOL[sf.attrib[self._XMLID_ATTR_BOOLEAN_ATTRIBUTE]]
        else:
            raise ElementRequiredException(self, e, 'boolean', self._XMLID_TAG_BOOLEAN_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_OPTIONALSTRING_ELEMENT)) is not None:
            self.optionalString = sf.attrib.get(self._XMLID_ATTR_OPTIONALSTRING_ATTRIBUTE, None)
        if (sf := e.find(self._XMLID_TAG_WEATHER_ELEMENT)) is not None:
            if self._XMLID_ATTR_WEATHER_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_WEATHER_ATTRIBUTE, 'weather')
            self.weather = EWeather[sf.attrib[self._XMLID_ATTR_WEATHER_ATTRIBUTE]]
        else:
            raise ElementRequiredException(self, e, 'weather', self._XMLID_TAG_WEATHER_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_DATE_ELEMENT)) is not None:
            self.date = Date()
            self.date.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'date', self._XMLID_TAG_DATE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_INTFROMTEXT_ELEMENT)) is not None:
            if self._XMLID_ATTR_INTFROMTEXT_ATTRIBUTE not in sf.attrib:
                raise AttributeRequiredException(self, sf, self._XMLID_ATTR_INTFROMTEXT_ATTRIBUTE, 'intFromText')
            self.intFromText = int(sf.attrib[self._XMLID_ATTR_INTFROMTEXT_ATTRIBUTE])
        else:
            raise ElementRequiredException(self, e, 'intFromText', self._XMLID_TAG_INTFROMTEXT_ELEMENT)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        etree.SubElement(e, self._XMLID_TAG_STRING_ELEMENT, {self._XMLID_ATTR_STRING_ATTRIBUTE: str(self.string)})
        etree.SubElement(e, self._XMLID_TAG_INTEGER_ELEMENT, {self._XMLID_ATTR_INTEGER_ATTRIBUTE: str(self.integer)})
        etree.SubElement(e, self._XMLID_TAG_FLOAT_ELEMENT, {self._XMLID_ATTR_FLOAT_ATTRIBUTE: str(self.float)})
        etree.SubElement(e, self._XMLID_TAG_BOOLEAN_ELEMENT, {self._XMLID_ATTR_BOOLEAN_ATTRIBUTE: str(self.boolean).lower()})
        if self.optionalString is not None:
            etree.SubElement(e, self._XMLID_TAG_OPTIONALSTRING_ELEMENT, {self._XMLID_ATTR_OPTIONALSTRING_ATTRIBUTE: str(self.optionalString)})
        etree.SubElement(e, self._XMLID_TAG_WEATHER_ELEMENT, {self._XMLID_ATTR_WEATHER_ATTRIBUTE: self.weather.name})
        e.append(self.date.toXML(self._XMLID_TAG_DATE_ELEMENT))
        etree.SubElement(e, self._XMLID_TAG_INTFROMTEXT_ELEMENT, {self._XMLID_ATTR_INTFROMTEXT_ATTRIBUTE: str(self.intFromText)})
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['string'] = str(self.string)
        data['integer'] = int(self.integer)
        data['float'] = float(self.float)
        data['boolean'] = bool(self.boolean)
        if self.optionalString is not None:
            data['optionalString'] = str(self.optionalString)
        data['weather'] = self.weather.name
        data['date'] = self.date.toDict()
        data['intFromText'] = int(self.intFromText)
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if (sf := data.get('string')) is not None:
            self.string = data['string']
        else:
            raise KeyRequiredException(self, data, 'string', 'string')
        if (sf := data.get('integer')) is not None:
            self.integer = data['integer']
        else:
            raise KeyRequiredException(self, data, 'integer', 'integer')
        if (sf := data.get('float')) is not None:
            self.float = data['float']
        else:
            raise KeyRequiredException(self, data, 'float', 'float')
        if (sf := data.get('boolean')) is not None:
            self.boolean = data['boolean']
        else:
            raise KeyRequiredException(self, data, 'boolean', 'boolean')
        if (sf := data.get('optionalString')) is not None:
            self.optionalString = data['optionalString']
        else:
            self.optionalString = None
        if (sf := data.get('weather')) is not None:
            self.weather = data['weather'].name
        else:
            raise KeyRequiredException(self, data, 'weather', 'weather')
        if (sf := data.get('date')) is not None:
            self.date = Date()
            self.date.fromDict(data['date'])
        else:
            raise KeyRequiredException(self, data, 'date', 'date')
        if (sf := data.get('intFromText')) is not None:
            self.intFromText = data['intFromText']
        else:
            raise KeyRequiredException(self, data, 'intFromText', 'intFromText')

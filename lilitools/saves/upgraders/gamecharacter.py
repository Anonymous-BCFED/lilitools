from typing import Any, Dict, Optional, Set

from lxml import etree

from lilitools.liliths_throne_consts import LILITHS_THRONE_VERSION
from lilitools.logging import ClickWrap
from lilitools.saves.character.enums.fetish_desire import EFetishDesire
from lilitools.saves.character.fetish_entry import FetishEntry
from lilitools.saves.upgraders.utils import isVersionOlderThan

from ._upgrader import BaseXMLUpgrader

click = ClickWrap()


class GameCharacterExportUpgrader(BaseXMLUpgrader):
    def __init__(self, e: etree._Element) -> None:
        super().__init__(e)
        if (pc := e.find('playerCharacter')) is not None:
            e = pc
        if (ec := e.find('exportedCharacter')) is not None:
            e = ec
        if (c := e.find('character')) is not None:
            e = c
        versionElement: Optional[etree._Element] = e.xpath('//core/version')[0]
        self.version: Optional[str] = None
        if versionElement is None:
            self.error(f'Could not find //core/version, which means this file is either *very* old or *very* corrupt.  This file is likely going to break when parsed. Have fun!')
        else:
            self.version = versionElement.attrib['value']
            if (self.version != LILITHS_THRONE_VERSION):
                self.warn(f'Old export detected. {self.getElementPath(versionElement)} = {self.version!r}')

                if isVersionOlderThan(self.version, "0.3.5.1"):
                    self.upgrade_to_0_3_5_1(e)
                if isVersionOlderThan(self.version, "0.3.5.6"):
                    self.upgrade_to_0_3_5_6(e)
                self.checkName(e)
                if isVersionOlderThan(self.version, "0.4.5.7"):
                    self.upgrade_to_0_4_5_7(e)
                if isVersionOlderThan(self.version, "0.4.6.2"):
                    self.upgrade_to_0_4_6_2(e)
                # if isVersionOlderThan(self.version, "0.4.9.9"):
                #     self.upgrade_to_0_4_9_9(e)

    def checkName(self, e: etree._Element) -> None:
        name: etree._Element = e.find('core').find('name')
        if (nv := name.get('value')) is not None:
            del name.attrib['value']
            name.attrib['nameMasculine'] = nv
            name.attrib['nameAndrogynous'] = nv
            name.attrib['nameFeminine'] = nv
            self.success(f'{self.getElementPath(name)} updated')

    def upgrade_to_0_3_5_1(self, e: etree._Element) -> None:
        with self.info('Upgrading GameCharacter to 0.3.5.1...'):
            for child in e.find('personality'):
                if child.tag == 'trait':
                    self.removeElement(child)

    def upgrade_to_0_3_5_6(self, e: etree._Element) -> None:
        with self.info('Upgrading GameCharacter to 0.3.5.6...'):
            if (lastTimeHadSex := self.getXMLValue(e, 'lastTimeHadSex')) is not None:
                lastTimeHadSex = int(lastTimeHadSex)
            if (lastTimeOrgasmed := self.getXMLValue(e, 'lastTimeOrgasmed')) is not None:
                lastTimeOrgasmed = int(lastTimeOrgasmed)
            secondModifier: int = 1
            if isVersionOlderThan(self.version, "0.3.14"):
                secondModifier = 60
            if lastTimeHadSex is not None:
                e.find('lastTimeHadSex').attrib['value'] = str(lastTimeHadSex * secondModifier)
            if lastTimeOrgasmed is not None:
                e.find('lastTimeOrgasmed').attrib['value'] = str(lastTimeOrgasmed * secondModifier)
            self.success('Success!')

    def upgrade_to_0_4_6_2(self, e: etree._Element) -> None:
        with self.info('Upgrading GameCharacter to 0.4.6.2...'):
            self._upgrade_fetishes_to_0_4_6_2(e)
            self._upgrade_areasKnownByCharacters_to_0_4_6_2(e)
            self.success('Success!')

    def _upgrade_areasKnownByCharacters_to_0_4_6_2(self, e: etree._Element) -> None:
        # if (akbc := e.find('areasKnownByCharacters')) is None or len(akbc.findall('area')) == 0 or len(akbc.xpath('//character'))==0:
        #     self.info('areasKnownByCharacters... (skipped)')
        #     return
        if (akbc := e.find('areasKnownByCharacters')) is not None:
            with self.info('Areas Known By Characters...'):
                for areaElem in akbc.findall('area'):
                    charsKnowingOfThisArea: Set[str] = set()
                    for characterElem in areaElem.findall('character'):
                        charsKnowingOfThisArea.add(characterElem.attrib['id'])
                        self.removeElement(characterElem)
                    for characterElem in areaElem.findall('c'):
                        charsKnowingOfThisArea.add(characterElem.text)
                        self.removeElement(characterElem)
                    self.info(f"{areaElem['type']}: " + ','.join(sorted(charsKnowingOfThisArea)))
                    for charid in sorted(charsKnowingOfThisArea):
                        self.addElement(areaElem, etree.Element('c')).text = charid
            self.success('Success!')

    def _upgrade_fetishes_to_0_4_6_2(self, e: etree._Element) -> None:
        fcoll: Dict[str, FetishEntry] = {}
        fetishes: etree._Element = e.find('fetishes')
        with click.debug(f'Fetishes...'):
            for fetish in fetishes.findall('fetish'):
                if 'type' in fetish.attrib:
                    fe = FetishEntry()
                    fe.id = fetish.attrib['type']
                    fe.hasFetish = (fetish.attrib['value'] == 'true') if 'value' in fetish.attrib else True
                    if fe.id == 'FETISH_NON_CON':
                        self.warn('Dropped non-con fetish.')
                        continue
                    fcoll[fe.id] = fe

            desires: etree._Element = e.find('fetishDesire')
            for desire in desires.findall('entry'):
                ftype = desire.attrib['fetish']
                fe = fcoll[ftype] if ftype in fcoll.keys() else FetishEntry()
                fe.id = ftype
                fe.desire = EFetishDesire[desire.attrib['desire']]
                if fe.id == 'FETISH_NON_CON':
                    self.warn('Dropped non-con desire.')
                    continue
                fcoll[fe.id] = fe

            desireXP: etree._Element = e.find('fetishExperience')
            for entry in desireXP.findall('entry'):
                ftype = entry.attrib['fetish']
                fe = fcoll[ftype] if ftype in fcoll.keys() else FetishEntry()
                fe.id = ftype
                fe.xp = int(entry.attrib['experience'])
                if fe.id == 'FETISH_NON_CON':
                    self.warn('Dropped non-con experience.')
                    continue
                fcoll[fe.id] = fe

            self.removeElement(fetishes)
            self.removeElement(desires)
            self.removeElement(desireXP)

            newfetishes = etree.Element('fetishes')
            for fe in fcoll.values():
                newfetishes.append(fe.toXML())
            self.addElement(e, newfetishes)

            #etree.dump(newfetishes,pretty_print=True)

    def upgrade_to_0_4_5_7(self, e: etree._Element) -> None:
        with self.info('Upgrading GameCharacter to 0.4.5.7...'):
            self._upgrade_slavePermissions_to_0_4_5_7(e)
            self.success('Success!')

    def _upgrade_slavePermissions_to_0_4_5_7(self, e: etree._Element) -> None:
        permission: etree._Element
        setting: etree._Element
        sps: etree._Element
        PREGNANCY_CASEFOLDED: str = 'PREGNANCY'.casefold()
        for permission in e.xpath('.//slavePermissionSettings/permission'):
            if permission.attrib['type'].casefold() == PREGNANCY_CASEFOLDED:
                permission.attrib['type'] = 'PILLS'
            for setting in permission.findall('setting'):
                if setting.attrib['value'].casefold().startswith(PREGNANCY_CASEFOLDED+"_"):
                    setting.attrib['value'] = 'PILLS_' + setting.attrib['value'][10:]
        pregnancy:Optional[etree._Element] = None
        for sps in e.xpath('.//slavePermissionSettings'):
            for permission in sps.findall('permission'):
                if permission.attrib['type'].casefold() == PREGNANCY_CASEFOLDED:
                    pregnancy = permission
                    break
            if pregnancy is None:
                pregnancy=etree.SubElement(sps, 'permission', {'type':'PREGNANCY'})
            etree.SubElement(pregnancy, 'setting', {'value':'PREGNANCY_ALLOW_BIRTHING'})
            etree.SubElement(pregnancy, 'setting', {'value':'PREGNANCY_ALLOW_EGG_LAYING'})
                    

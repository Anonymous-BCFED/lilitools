from lxml import etree

from lilitools.logging import ClickWrap
from lilitools.saves.upgraders.gamecharacter import GameCharacterExportUpgrader
from lilitools.saves.upgraders.utils import isVersionOlderThan

click = ClickWrap()


class NPCExportUpgrader(GameCharacterExportUpgrader):
    def __init__(self, e: etree._Element) -> None:
        super().__init__(e)
        if self.version is None:
            return
        if isinstance(e, etree._ElementTree):
            e = e.getroot()
        if (ec := e.find('exportedCharacter')) is not None:
            e = ec
        if (ec := e.find('character')) is not None:
            e = ec
        npcspecific = e.find('npcSpecific')
        if npcspecific is not None:
            if isVersionOlderThan(self.version, "0.4.4"):
                self.upgrade_to_0_4_4(npcspecific)
            if isVersionOlderThan(self.version, "0.4.6.3"):
                self.upgrade_to_0_4_6_3(npcspecific)

    def upgrade_to_0_4_4(self, n: etree._Element) -> None:
        with self.info('Upgrading NPC to 0.4.4...'):
            if n.find('playerSurrenderCount') is None:
                self.createXMLValue(n, 'playerSurrenderCount', '0')
            self.success('Success!')

    def upgrade_to_0_4_6_3(self, e: etree._Element) -> None:
        with self.info('Upgrading NPC to 0.4.6.3...'):
            if e.find('generateExtraItems') is None:
                self.createXMLValue(e, 'generateExtraItems', 'false')
            if e.find('generateDisposableClothing') is None:
                self.createXMLValue(e, 'generateDisposableClothing', 'false')
            if e.find('generateExtraClothing') is None:
                self.createXMLValue(e, 'generateExtraClothing', 'false')
            self.success('Success!')

from lxml import etree

from lilitools.saves.upgraders.gamecharacter import GameCharacterExportUpgrader


class PlayerCharacterExportUpgrader(GameCharacterExportUpgrader):
    def __init__(self, e: etree._Element) -> None:
        super().__init__(e)
        if self.version is None:
            return
        if isinstance(e, etree._ElementTree):
            e = e.getroot()
        if (ec := e.find('exportedCharacter')) is not None:
            e = ec
        if (c := e.find('character')) is not None:
            pass  # Do playerCharacter-specific shit here

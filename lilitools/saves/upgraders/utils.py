'''

	
	public static boolean isVersionOlderThan(String versionToCheck, String versionToCheckAgainst) {
		String[] v1 = versionToCheck.split("\\.");
		String[] v2 = versionToCheckAgainst.split("\\.");
		
		try {
			int maxLength = (v1.length > v2.length) ? v1.length : v2.length;
			for (int i = 0; i < maxLength; i++) {
				int v1i;
				int v2i;
				
				if(v1[1].charAt(0)=='1') { // Versions prior to 0.2.x used an old system of the format: 0.1.10.1 being a lower version than 0.1.9.1:
					v1i = (i < v1.length) ? Integer.valueOf((v1[i]+"00").substring(0, 3)) : 0;
					v2i = (i < v2.length) ? Integer.valueOf((v2[i]+"00").substring(0, 3)) : 0;
					
				} else { // Versions of 0.2.x and higher use a new system of the format: 0.2.10.1 being a higher version than 0.2.9.1:
					v1i = (i < v1.length) ? Integer.valueOf(v1[i]) : 0;
					v2i = (i < v2.length) ? Integer.valueOf(v2[i]) : 0;
				}
				
				if (v1i < v2i) {
					return true;
				} else if (v1i > v2i) {
					return false;
				} 
			}
			
		} catch(Exception ex) {
			return true;
		}
		
		return false;
	}
'''


def isVersionOlderThan(a: str, b: str) -> bool:
    v1 = a.split('.')
    v2 = b.split('.')
    v1len = len(v1)
    v2len = len(v2)
    maxlen = max(v1len, v2len)
    try:
        # if(v1[1].charAt(0)=='1') { // Versions prior to 0.2.x used an old system of the format: 0.1.10.1 being a lower version than 0.1.9.1:
        fuckedcmp = v1[0] == '0' and int(v1[1]) <= 1
        for i in range(maxlen):
            v1i: int
            v2i: int
            if fuckedcmp:
                v1i = int((v1[i] + '00')[0:3]) if i < v1len else 0
                v2i = int((v2[i] + '00')[0:3]) if i < v2len else 0
            else:
                v1i = int(v1[i]) if i < v1len else 0
                v2i = int(v2[i]) if i < v2len else 0
            if v1i < v2i:
                return True
            elif v1i > v2i:
                return False
    except:
        return True
    return False

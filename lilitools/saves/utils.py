import contextlib
import os
from pathlib import Path
from typing import BinaryIO, Optional, Sequence, Union

from lxml import etree
from lilitools.logging import ClickWrap

from lilitools.saves.game import Game
from lilitools.saves.menders import mendSaveXML
from lilitools.saves.properties.gameproperties import GameProperties

from ruamel.yaml import YAML as Yaml

click = ClickWrap()


def loadSaveFrom(
    file: Union[str, Path, BinaryIO], show_warnings: bool = False, quiet: bool = False
) -> Game:
    properties: Optional[GameProperties] = None
    if isinstance(file, (str, Path)):
        propsfile = Path(file).parent.parent / "properties.xml"
        if propsfile.is_file():
            properties = loadPropertiesFrom(propsfile)
        else:
            click.warn(
                f"Unable to find {propsfile}. Game.properties will be None, which may cause buggy behaviour."
            )

    tree = etree.parse(file, parser=etree.XMLParser(remove_comments=True))
    mendSaveXML(tree)
    game = Game()
    if isinstance(file, Path):
        file = str(file)
    game.show_warnings_during_load = show_warnings and not quiet
    game.load_quietly = quiet
    game.properties = properties
    game.fromXML(tree)
    return game


def loadPropertiesFrom(
    file: Union[str, Path, BinaryIO], show_warnings: bool = False, quiet: bool = False
) -> GameProperties:
    tree = etree.parse(file, parser=etree.XMLParser(remove_comments=True))
    # mendPropertiesXML(tree)
    properties = GameProperties()
    if isinstance(file, Path):
        file = str(file)
    properties.show_warnings_during_load = show_warnings and not quiet
    properties.load_quietly = quiet
    properties.fromXML(tree)
    return properties


def minify_xml(root: etree._ElementTree) -> None:
    for elem in root.iter("*"):
        if elem.text is not None:
            elem.text = elem.text.strip()
        if elem.tail is not None:
            elem.tail = elem.tail.strip()


def sort_xml_attributes(root: etree._Element) -> None:
    for el in root.iter():
        attrib = el.attrib
        if len(attrib) > 1:
            attributes = sorted(attrib.items())
            attrib.clear()
            attrib.update(attributes)


def saveToYAML(
    game: Game,
    file: Union[str, Path],
    show_warnings: bool = False,
    minify: bool = False,
    sort_attrs: bool = False,
    quiet: bool = True,
) -> None:
    if isinstance(file, str):
        file = Path(file)
    with click.info(f"Writing {path}...") if not quiet else contextlib.nullcontext():
        if not quiet:
            click.info("Serializing...")
        data = game.toDict()
        tmp = file.with_suffix('.tmp.yml')
        YAML=Yaml(typ='safe')
        YAML.default_flow_style=False
        if not quiet:
            click.info("Writing to disk...")
        with tmp.open('w') as f:
            YAML.dump(data, f)
        os.rename(tmp, file)
        if not quiet:
            click.success("DONE!")


def saveToFile(
    game: Game,
    file: Union[str, Path],
    show_warnings: bool = False,
    minify: bool = True,
    sort_attrs: bool = False,
    quiet: bool = True,
) -> None:
    if isinstance(file, str):
        file = Path(file)
    e: etree._Element = game.toXML()
    writeRootToFile(file, e, minify=minify, sort_attrs=sort_attrs, quiet=quiet)


def writeRootToFile(
    path: Path,
    e: etree._Element,
    minify: bool = False,
    sort_attrs: bool = False,
    quiet: bool = True,
) -> None:
    tree = etree.ElementTree(e)
    tree.docinfo.clear()
    with click.info(f"Writing {path}...") if not quiet else contextlib.nullcontext():
        if minify:
            if not quiet:
                click.info("Minifying...")
            minify_xml(tree)
        else:
            if not quiet:
                click.info("Indenting...")
            etree.indent(tree, space="  ")
        if sort_attrs:
            if not quiet:
                click.info("Sorting...")
            sort_xml_attributes(tree)
        if not quiet:
            click.info("Writing to disk...")
        tmp = path.with_suffix(".xml.tmp")
        tree.write(str(tmp), encoding="utf-8", pretty_print=not minify)
        os.replace(path.with_suffix(".xml.tmp"), path)
        if not quiet:
            click.success("DONE!")


def andify_str(seq: Sequence[str], and_word: str = "and") -> str:
    o: str = ""
    p: str
    i: int
    seqlen: int = len(seq)
    for i, p in enumerate(seq):
        if i > 0:
            o += ", "
            if i == seqlen - 1:
                o += and_word
        o += p
    return o


def orify_str(seq: Sequence[str]) -> str:
    return andify_str(seq, and_word="or")

# ruamel
def rec_sort(d):
    if isinstance(d, dict):
        res = dict()
        for k in sorted(d.keys()):
            res[k] = rec_sort(d[k])
        return res
    if isinstance(d, list):
        for idx, elem in enumerate(d):
            d[idx] = rec_sort(elem)
    return d

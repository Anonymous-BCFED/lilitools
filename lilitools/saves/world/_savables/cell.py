#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, ElementRequiredException, KeyRequiredException
from lilitools.saves.inventory.character_inventory import CharacterInventory
from lilitools.saves.math.point2i import Point2I
from lilitools.saves.savable import XML2BOOL, Savable
from lilitools.saves.world.generic_place import GenericPlace
__all__ = ['RawCell']
## from [LT]/src/com/lilithsthrone/world/Cell.java: public Element saveAsXML(Element parentElement, Document doc) { @ UFjkFI05W6KWoatQ0YcDqGVU+pJRv1UfkPPVt6DEEVHKNfhQxPeqR5/aYZ56bkbd0vGjJF22XaFg9DJOiFZX5g==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawCell(Savable):
    TAG = 'cell'
    _XMLID_ATTR_CHARACTERINVENTORY_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_DISCOVERED_ATTRIBUTE: str = 'discovered'
    _XMLID_ATTR_LOCATION_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_PLACE_ATTRIBUTE: str = 'value'
    _XMLID_ATTR_TRAVELLEDTO_ATTRIBUTE: str = 'travelledTo'
    _XMLID_TAG_CHARACTERINVENTORY_ELEMENT: str = 'characterInventory'
    _XMLID_TAG_LOCATION_ELEMENT: str = 'location'
    _XMLID_TAG_PLACE_ELEMENT: str = 'place'

    def __init__(self) -> None:
        super().__init__()
        self.discovered: bool = False
        self.travelledTo: bool = False
        self.location: Point2I = Point2I()  # Element
        self.place: GenericPlace = GenericPlace()  # Element
        self.characterInventory: Optional[CharacterInventory] = None  # Element

    def overrideAttrs(self, characterInventory_attribute: _Optional_str = None, discovered_attribute: _Optional_str = None, location_attribute: _Optional_str = None, place_attribute: _Optional_str = None, travelledTo_attribute: _Optional_str = None) -> None:
        if characterInventory_attribute is not None:
            self._XMLID_ATTR_CHARACTERINVENTORY_ATTRIBUTE = characterInventory_attribute
        if discovered_attribute is not None:
            self._XMLID_ATTR_DISCOVERED_ATTRIBUTE = discovered_attribute
        if location_attribute is not None:
            self._XMLID_ATTR_LOCATION_ATTRIBUTE = location_attribute
        if place_attribute is not None:
            self._XMLID_ATTR_PLACE_ATTRIBUTE = place_attribute
        if travelledTo_attribute is not None:
            self._XMLID_ATTR_TRAVELLEDTO_ATTRIBUTE = travelledTo_attribute

    def overrideTags(self, characterInventory_element: _Optional_str = None, location_element: _Optional_str = None, place_element: _Optional_str = None) -> None:
        if characterInventory_element is not None:
            self._XMLID_TAG_CHARACTERINVENTORY_ELEMENT = characterInventory_element
        if location_element is not None:
            self._XMLID_TAG_LOCATION_ELEMENT = location_element
        if place_element is not None:
            self._XMLID_TAG_PLACE_ELEMENT = place_element

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        sf: etree._Element
        value = e.get(self._XMLID_ATTR_DISCOVERED_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_DISCOVERED_ATTRIBUTE, 'discovered')
        else:
            self.discovered = XML2BOOL[value]
        value = e.get(self._XMLID_ATTR_TRAVELLEDTO_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_TRAVELLEDTO_ATTRIBUTE, 'travelledTo')
        else:
            self.travelledTo = XML2BOOL[value]
        if (sf := e.find(self._XMLID_TAG_LOCATION_ELEMENT)) is not None:
            self.location = Point2I()
            self.location.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'location', self._XMLID_TAG_LOCATION_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_PLACE_ELEMENT)) is not None:
            self.place = GenericPlace()
            self.place.fromXML(sf)
        else:
            raise ElementRequiredException(self, e, 'place', self._XMLID_TAG_PLACE_ELEMENT)
        if (sf := e.find(self._XMLID_TAG_CHARACTERINVENTORY_ELEMENT)) is not None:
            self.characterInventory = CharacterInventory()
            self.characterInventory.fromXML(sf)

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        e.attrib[self._XMLID_ATTR_DISCOVERED_ATTRIBUTE] = str(self.discovered).lower()
        e.attrib[self._XMLID_ATTR_TRAVELLEDTO_ATTRIBUTE] = str(self.travelledTo).lower()
        e.append(self.location.toXML(self._XMLID_TAG_LOCATION_ELEMENT))
        e.append(self.place.toXML(self._XMLID_TAG_PLACE_ELEMENT))
        if self.characterInventory is not None:
            e.append(self.characterInventory.toXML(self._XMLID_TAG_CHARACTERINVENTORY_ELEMENT))
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['discovered'] = bool(self.discovered)
        data['travelledTo'] = bool(self.travelledTo)
        data['location'] = self.location.toDict()
        data['place'] = self.place.toDict()
        if self.characterInventory is not None:
            data['characterInventory'] = self.characterInventory.toDict()
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'discovered' not in data:
            raise KeyRequiredException(self, data, 'discovered', 'discovered')
        self.discovered = bool(data['discovered'])
        if 'travelledTo' not in data:
            raise KeyRequiredException(self, data, 'travelledTo', 'travelledTo')
        self.travelledTo = bool(data['travelledTo'])
        if (sf := data.get('location')) is not None:
            self.location = Point2I()
            self.location.fromDict(data['location'])
        else:
            raise KeyRequiredException(self, data, 'location', 'location')
        if (sf := data.get('place')) is not None:
            self.place = GenericPlace()
            self.place.fromDict(data['place'])
        else:
            raise KeyRequiredException(self, data, 'place', 'place')
        if (sf := data.get('characterInventory')) is not None:
            self.characterInventory = CharacterInventory()
            self.characterInventory.fromDict(data['characterInventory'])
        else:
            self.characterInventory = None

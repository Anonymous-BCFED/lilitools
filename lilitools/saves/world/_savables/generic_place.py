#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, Optional, Set
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
__all__ = ['RawGenericPlace']
## from [LT]/src/com/lilithsthrone/world/places/GenericPlace.java: public Element saveAsXML(Element parentElement, Document doc) { @ JyeTRj+Di4UYBOZTYhL0JMrPO3WRSZ8ohAmLcM+DTc5AUfKTiPec0YI+6Zuh8yMjZ20aChfUtV669niEP5aPzw==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawGenericPlace(Savable):
    TAG = 'place'
    _XMLID_ATTR_NAME_ATTRIBUTE: str = 'name'
    _XMLID_ATTR_PLACEUPGRADES_VALUEATTR: str = 'type'
    _XMLID_ATTR_TYPE_ATTRIBUTE: str = 'type'
    _XMLID_TAG_PLACEUPGRADES_CHILD: str = 'upgrade'
    _XMLID_TAG_PLACEUPGRADES_PARENT: str = 'placeUpgrades'

    def __init__(self) -> None:
        super().__init__()
        self.name: Optional[str] = None
        self.type: str = ''
        self.placeUpgrades: Optional[Set[str]] = set()

    def overrideAttrs(self, name_attribute: _Optional_str = None, placeUpgrades_valueattr: _Optional_str = None, type_attribute: _Optional_str = None) -> None:
        if name_attribute is not None:
            self._XMLID_ATTR_NAME_ATTRIBUTE = name_attribute
        if placeUpgrades_valueattr is not None:
            self._XMLID_ATTR_PLACEUPGRADES_VALUEATTR = placeUpgrades_valueattr
        if type_attribute is not None:
            self._XMLID_ATTR_TYPE_ATTRIBUTE = type_attribute

    def overrideTags(self, placeUpgrades_child: _Optional_str = None, placeUpgrades_parent: _Optional_str = None) -> None:
        if placeUpgrades_child is not None:
            self._XMLID_TAG_PLACEUPGRADES_CHILD = placeUpgrades_child
        if placeUpgrades_parent is not None:
            self._XMLID_TAG_PLACEUPGRADES_PARENT = placeUpgrades_parent

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        lp: etree._Element
        value = e.get(self._XMLID_ATTR_NAME_ATTRIBUTE, None)
        if value is None:
            self.name = None
        else:
            self.name = str(value)
        value = e.get(self._XMLID_ATTR_TYPE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_TYPE_ATTRIBUTE, 'type')
        else:
            self.type = str(value)
        if (lf := e.find(self._XMLID_TAG_PLACEUPGRADES_PARENT)) is not None:
            if lf is not None:
                for lc in lf.iterfind(self._XMLID_TAG_PLACEUPGRADES_CHILD):
                    self.placeUpgrades.add(lc.attrib[self._XMLID_ATTR_PLACEUPGRADES_VALUEATTR])

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        if self.name is not None:
            e.attrib[self._XMLID_ATTR_NAME_ATTRIBUTE] = str(self.name)
        e.attrib[self._XMLID_ATTR_TYPE_ATTRIBUTE] = str(self.type)
        if self.placeUpgrades is not None and len(self.placeUpgrades) > 0:
            lp = etree.SubElement(e, self._XMLID_TAG_PLACEUPGRADES_PARENT)
            for lv in sorted(self.placeUpgrades, key=str):
                etree.SubElement(lp, self._XMLID_TAG_PLACEUPGRADES_CHILD, {self._XMLID_ATTR_PLACEUPGRADES_VALUEATTR: lv})
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        if self.name is not None:
            data['name'] = self.name
        data['type'] = self.type
        if self.placeUpgrades is None or len(self.placeUpgrades) > 0:
            data['placeUpgrades'] = list()
        else:
            lc = list()
            if len(self.placeUpgrades) > 0:
                for lv in sorted(lc, lambda x: str(x)):
                    lc.append(str(lv))
            data['placeUpgrades'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        self.name = str(data['name']) if 'name' in data else None
        if 'type' not in data:
            raise KeyRequiredException(self, data, 'type', 'type')
        self.type = str(data['type'])
        if (lv := self.placeUpgrades) is not None:
            for le in lv:
                self.placeUpgrades.add(str(le))
        else:
            self.placeUpgrades = set()

#################################################
# WARNING: @GENERATED CODE; DO NOT EDIT BY HAND #
#   BY devtools/analysis/savable_generator.py   #
#  FROM devtools/analysis/savable_generator.py  #
#          Serialization for a savable          #
#################################################
from typing import Any, Dict, List, Optional
from lxml import etree
from lilitools.saves.exceptions import AttributeRequiredException, KeyRequiredException
from lilitools.saves.savable import Savable
from lilitools.saves.world.cell import Cell
__all__ = ['RawWorld']
## from [LT]/src/com/lilithsthrone/world/World.java: public Element saveAsXML(Element parentElement, Document doc) { @ sKUNXyWwf716EGkbMTHOup2EpWyvGVrgV1JAynnat2JwwHGN2M1T8/gQKBV8fnuD82ELKDxCKdmwGGFMHxvErQ==
_Optional_str = Optional[str]
_etree__Element = etree._Element


class RawWorld(Savable):
    TAG = 'world'
    _XMLID_ATTR_HEIGHT_ATTRIBUTE: str = 'height'
    _XMLID_ATTR_TYPE_ATTRIBUTE: str = 'worldType'
    _XMLID_ATTR_WIDTH_ATTRIBUTE: str = 'width'
    _XMLID_TAG_CELLS_PARENT: str = 'grid'
    _XMLID_TAG_CELLS_VALUEELEM: Optional[str] = None

    def __init__(self) -> None:
        super().__init__()
        self.type: str = ''
        self.width: int = 0
        self.height: int = 0
        self.cells: List[Cell] = list()

    def overrideAttrs(self, height_attribute: _Optional_str = None, type_attribute: _Optional_str = None, width_attribute: _Optional_str = None) -> None:
        if height_attribute is not None:
            self._XMLID_ATTR_HEIGHT_ATTRIBUTE = height_attribute
        if type_attribute is not None:
            self._XMLID_ATTR_TYPE_ATTRIBUTE = type_attribute
        if width_attribute is not None:
            self._XMLID_ATTR_WIDTH_ATTRIBUTE = width_attribute

    def overrideTags(self, cells_parent: _Optional_str = None, cells_valueelem: _Optional_str = None) -> None:
        if cells_parent is not None:
            self._XMLID_TAG_CELLS_PARENT = cells_parent
        if cells_valueelem is not None:
            self._XMLID_TAG_CELLS_VALUEELEM = cells_valueelem

    def fromXML(self, e: _etree__Element) -> None:
        super().fromXML(e)
        lk: str
        lv: Savable
        value = e.get(self._XMLID_ATTR_TYPE_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_TYPE_ATTRIBUTE, 'type')
        else:
            self.type = str(value)
        value = e.get(self._XMLID_ATTR_WIDTH_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_WIDTH_ATTRIBUTE, 'width')
        else:
            self.width = int(value)
        value = e.get(self._XMLID_ATTR_HEIGHT_ATTRIBUTE, None)
        if value is None:
            raise AttributeRequiredException(self, e, self._XMLID_ATTR_HEIGHT_ATTRIBUTE, 'height')
        else:
            self.height = int(value)
        if (lf := e.find(self._XMLID_TAG_CELLS_PARENT)) is not None:
            if lf is not None:
                for lc in lf:
                    lv = Cell()
                    lv.fromXML(lc)
                    self.cells.append(lv)
        else:
            self.cells = list()

    def toXML(self, tagOverride: _Optional_str = None) -> etree._Element:
        e: _etree__Element = super().toXML(tagOverride)
        lc: etree._Element
        lp: etree._Element
        e.attrib[self._XMLID_ATTR_TYPE_ATTRIBUTE] = str(self.type)
        e.attrib[self._XMLID_ATTR_WIDTH_ATTRIBUTE] = str(self.width)
        e.attrib[self._XMLID_ATTR_HEIGHT_ATTRIBUTE] = str(self.height)
        lp = etree.SubElement(e, self._XMLID_TAG_CELLS_PARENT)
        for lv in self.cells:
            lp.append(lv.toXML())
        return e

    def toDict(self) -> Dict[str, Any]:
        data: Dict[str, Any] = super().toDict()
        data['type'] = self.type
        data['width'] = int(self.width)
        data['height'] = int(self.height)
        if self.cells is None or len(self.cells) > 0:
            data['cells'] = list()
        else:
            lc = list()
            if len(self.cells) > 0:
                for lv in lc:
                    lc.append(lv.toDict())
            data['cells'] = lc
        return data

    def fromDict(self, data: Dict[str, Any]) -> None:
        super().fromDict(data)
        if 'type' not in data:
            raise KeyRequiredException(self, data, 'type', 'type')
        self.type = str(data['type'])
        if 'width' not in data:
            raise KeyRequiredException(self, data, 'width', 'width')
        self.width = int(data['width'])
        if 'height' not in data:
            raise KeyRequiredException(self, data, 'height', 'height')
        self.height = int(data['height'])
        if (lv := self.cells) is not None:
            for le in lv:
                self.cells.append(le.toDict())
        else:
            raise KeyRequiredException(self, data, 'cells', 'cells')

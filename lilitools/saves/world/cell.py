from __future__ import annotations

from typing import Optional, Set, Type

from lxml import etree

from lilitools.saves.inventory.character_inventory import CharacterInventory
from lilitools.saves.world._savables.cell import RawCell
from lilitools.saves.world.enums.place_upgrade import EPlaceUpgrade


class Cell(RawCell):
    def __init__(self) -> None:
        super().__init__()
        self.worldID: str = ""
        #: Used for some room IDs.
        self.place_counter_position: Optional[int] = None

    def clone(self, ctype: Optional[Type[Cell]] = None) -> "Cell":
        c = (ctype or Cell)()
        c.discovered = self.discovered
        c.characterInventory = (
            self.characterInventory.clone()
            if self.characterInventory is not None
            else None
        )
        c.location.x = self.location.x
        c.location.y = self.location.y
        c.place = self.place.clone()
        c.travelledTo = self.travelledTo
        c.worldID = self.worldID
        return c

    def toXML(self, tagOverride: Optional[str] = None) -> etree._Element:
        e = super().toXML(tagOverride)
        # Hack
        if self.characterInventory is None or self.characterInventory.isEmpty():
            if (ie := e.find("characterInventory")) is not None:
                e.remove(ie)
        return e

    def fromXML(self, e: etree._Element) -> None:
        super().fromXML(e)

    def getName(self) -> str:
        if self.place is not None and self.place.name is not None:
            return self.place.name
        return str(self)

    def __str__(self) -> str:
        return f"{self.worldID}@{self.location.x},{self.location.y}"

    def getOrInstantiateInventory(self) -> CharacterInventory:
        if self.characterInventory is None:
            self.characterInventory = CharacterInventory()
        return self.characterInventory

    def _eupgrade2str(self, v: EPlaceUpgrade | str) -> str:
        if isinstance(v, EPlaceUpgrade):
            return v.name
        return v

    def addUpgrade(self, upgrade: EPlaceUpgrade | str) -> None:
        _upgradeID: str = self._eupgrade2str(upgrade)
        if self.place.placeUpgrades is not None:
            self.place.placeUpgrades.add(_upgradeID)
        else:
            self.place.placeUpgrades = set(_upgradeID)

    def removeUpgrade(self, upgrade: EPlaceUpgrade | str) -> None:
        _upgradeID: str = self._eupgrade2str(upgrade)
        if self.place.placeUpgrades is not None:
            self.place.placeUpgrades.remove(_upgradeID)

    def setBaseUpgrade(self, upgrade: EPlaceUpgrade | str) -> None:
        _upgradeID: str = self._eupgrade2str(upgrade)
        self.place.placeUpgrades = set(_upgradeID)

    def clearUpgrades(self) -> None:
        self.place.placeUpgrades = set()

    @property
    def upgrades(self) -> Set[EPlaceUpgrade]:
        return (
            {EPlaceUpgrade[x] for x in self.place.placeUpgrades}
            if self.place.placeUpgrades
            else set()
        )

    @upgrades.setter
    def upgrades(self, value: Set[EPlaceUpgrade]) -> None:
        self.place.placeUpgrades = {v.name for v in value}

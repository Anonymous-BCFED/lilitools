from enum import IntEnum
__all__ = ['EWorldType']


class EWorldType(IntEnum):
    EMPTY = 0
    WORLD_MAP = 1
    DOMINION = 2
    MUSEUM = 3
    MUSEUM_LOST = 4
    LILAYAS_HOUSE_GROUND_FLOOR = 5
    LILAYAS_HOUSE_FIRST_FLOOR = 6
    ZARANIX_HOUSE_FIRST_FLOOR = 7
    ZARANIX_HOUSE_GROUND_FLOOR = 8
    HARPY_NEST = 9
    SLAVER_ALLEY = 10
    BOUNTY_HUNTER_LODGE = 11
    BOUNTY_HUNTER_LODGE_UPSTAIRS = 12
    SHOPPING_ARCADE = 13
    TEXTILES_WAREHOUSE = 14
    ENFORCER_HQ = 15
    ENFORCER_WAREHOUSE = 16
    CITY_HALL = 17
    HOME_IMPROVEMENTS = 18
    DOMINION_EXPRESS = 19
    ANGELS_KISS_GROUND_FLOOR = 20
    ANGELS_KISS_FIRST_FLOOR = 21
    NIGHTLIFE_CLUB = 22
    DADDYS_APARTMENT = 23
    FELICIA_APARTMENT = 24
    HELENAS_APARTMENT = 25
    NYANS_APARTMENT = 26
    SUBMISSION = 27
    LYSSIETH_PALACE = 28
    IMP_FORTRESS_ALPHA = 29
    IMP_FORTRESS_DEMON = 30
    IMP_FORTRESS_FEMALES = 31
    IMP_FORTRESS_MALES = 32
    BAT_CAVERNS = 33
    SLIME_QUEENS_LAIR_GROUND_FLOOR = 34
    SLIME_QUEENS_LAIR_FIRST_FLOOR = 35
    GAMBLING_DEN = 36
    RAT_WARRENS = 37
    REBEL_BASE = 38

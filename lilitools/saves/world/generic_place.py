from __future__ import annotations
from lilitools.saves.world._savables.generic_place import RawGenericPlace


class GenericPlace(RawGenericPlace):
    def clone(self) -> GenericPlace:
        o = GenericPlace()
        o.name = self.name
        o.type = self.type
        o.placeUpgrades = {x for x in self.placeUpgrades} if self.placeUpgrades else None
        return o
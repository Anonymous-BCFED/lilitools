from typing import Dict, Optional, Set, Tuple

from lxml import etree

from lilitools.saves.world._savables.world import RawWorld
from lilitools.saves.world.cell import Cell
from lilitools.saves.world.enums.place_type import EPlaceType

from logging import getLogger  # isort:skip

log = getLogger(__name__)

DEBUG = False


class World(RawWorld):
    def __init__(self) -> None:
        super().__init__()

        self.cellsByPos: Dict[Tuple[int, int], Cell] = {}

    def clear(self) -> None:
        for x in range(self.width):
            for y in range(self.height):
                c = Cell()
                c.location.x = x
                c.location.y = y
                c.place.type = EPlaceType.GENERIC_IMPASSABLE.name
                self.cells.append(c)
                self.cellsByPos[x, y] = c
        if DEBUG:
            print(f"Initialized, len(cells)={len(self.cells)}")

    def get(self, x: int, y: Optional[int] = None) -> Cell:
        if y is None:
            return self.cells[x]
        else:
            return self.cellsByPos[x, y]

    def has(self, x: int, y: Optional[int] = None) -> bool:
        if y is None:
            return 0 <= x < len(self.cells)
        else:
            return (x, y) in self.cellsByPos

    def set(self, x: int, y: int, cell: Cell) -> None:
        oldcell = self.cellsByPos[x, y]
        i = self.cells.index(oldcell)
        cell.location.x = x
        cell.location.y = y
        cell.worldID = self.type
        self.cells[i] = cell
        self.cellsByPos[x, y] = cell

    def fromXML(self, e: etree._Element, show_warnings: bool = False) -> None:
        super().fromXML(e)

        for cell in self.cells:
            cell.worldID = self.type
            self.cellsByPos[cell.location.x, cell.location.y] = cell

        self.update_place_counters()

    def update_place_counters(self) -> None:
        """
        Used in some room names, e.g. Garden Room FG-##
        """
        possiblePlaces: Set[str] = set([c.place.type for c in self.cells])
        placeCounts: Dict[str, int] = {p: 0 for p in possiblePlaces}
        for x in range(self.width):
            for h in range(self.height):
                coord = x, self.height - 1 - h
                if coord in self.cellsByPos:
                    c = self.cellsByPos[coord]
                    placeCounts[c.place.type] += 1
                    c.place_counter_position = placeCounts[c.place.type]

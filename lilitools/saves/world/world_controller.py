from functools import lru_cache
from typing import Dict, Optional

from lxml import etree
from lilitools.consts import PRESERVE_SAVE_ORDERING

from lilitools.logging import ClickWrap
from lilitools.saves.savable import Savable
from lilitools.saves.world.cell import Cell
from lilitools.saves.world.world import World

click = ClickWrap()


class WorldController(Savable):
    TAG = 'maps'

    def __init__(self) -> None:
        super().__init__()

        self.worlds: Dict[str, World] = {}

    def __getitem__(self, key: str) -> Optional[World]:
        return self.worlds[key]

    def __setitem__(self, key: str, value: World) -> None:
        self.worlds[key] = value

    def getCellByWorldCoords(self, worldID: str, x: int, y: int) -> Cell:
        return self.worlds[worldID].get(x, y)

    def setCellByWorldCoords(self, worldID: str, x: int, y: int, cell:Cell) -> None:
        return self.worlds[worldID].set(x, y,cell)

    @lru_cache(maxsize=1)
    def getPlayerRoom(self) -> Cell:
        # LILAYA_HOME_ROOM_PLAYER
        cell = self.getCellByWorldCoords('LILAYAS_HOUSE_FIRST_FLOOR', x=4, y=3)
        if cell is None or cell.place is None or cell.place.type != 'LILAYA_HOME_ROOM_PLAYER':
            click.warn('Failed to locate LILAYA_HOME_ROOM_PLAYER by coordinates.  Trying via type (slow)...')
            # Fallback to a slower search
            for c in self.worlds['LILAYAS_HOUSE_FIRST_FLOOR'].cells:
                if c is not None and c.place is not None and c.place.type == 'LILAYA_HOME_ROOM_PLAYER':
                    click.warn(f"Found at LILAYAS_HOUSE_FIRST_FLOOR {c.location.x}, {c.location.y}")
                    return c
        return cell

    def fromXML(self, e: etree._Element, show_warnings: bool = False) -> None:
        wld: etree._Element
        for wld in e:
            w = World()
            w.fromXML(wld, show_warnings=show_warnings)
            self.worlds[w.type] = w

    def toXML(self, tagOverride: Optional[str] = None) -> etree._Element:
        e = super().toXML(tagOverride)

        worlds = list(self.worlds.items())
        if not PRESERVE_SAVE_ORDERING:
            worlds.sort(key=lambda t: t[0])
        for k, map in worlds:
            e.append(map.toXML())

        return e

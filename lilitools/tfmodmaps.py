from typing import Dict

from lilitools.saves.character.body.enums.tongue_modifier import ETongueModifier
from lilitools.saves.character.enums.penetrator_modifier import EPenetratorModifier
from lilitools.saves.enums.tf_modifier import ETFModifier

TF2PEN: Dict[ETFModifier, EPenetratorModifier] = {
    ETFModifier.TF_MOD_PENIS_BARBED: EPenetratorModifier.BARBED,
    ETFModifier.TF_MOD_PENIS_BLUNT: EPenetratorModifier.BLUNT,
    ETFModifier.TF_MOD_PENIS_FLARED: EPenetratorModifier.FLARED,
    ETFModifier.TF_MOD_PENIS_KNOTTED: EPenetratorModifier.KNOTTED,
    ETFModifier.TF_MOD_PENIS_OVIPOSITOR: EPenetratorModifier.OVIPOSITOR,
    ETFModifier.TF_MOD_PENIS_PREHENSILE: EPenetratorModifier.PREHENSILE,
    ETFModifier.TF_MOD_PENIS_RIBBED: EPenetratorModifier.RIBBED,
    ETFModifier.TF_MOD_PENIS_SHEATHED: EPenetratorModifier.SHEATHED,
    ETFModifier.TF_MOD_PENIS_TAPERED: EPenetratorModifier.TAPERED,
    ETFModifier.TF_MOD_PENIS_TENTACLED: EPenetratorModifier.TENTACLED,
    ETFModifier.TF_MOD_PENIS_VEINY: EPenetratorModifier.VEINY,
}
PEN2TF: Dict[EPenetratorModifier, ETFModifier] = {
    EPenetratorModifier.BARBED: ETFModifier.TF_MOD_PENIS_BARBED,
    EPenetratorModifier.BLUNT: ETFModifier.TF_MOD_PENIS_BLUNT,
    EPenetratorModifier.FLARED: ETFModifier.TF_MOD_PENIS_FLARED,
    EPenetratorModifier.KNOTTED: ETFModifier.TF_MOD_PENIS_KNOTTED,
    EPenetratorModifier.OVIPOSITOR: ETFModifier.TF_MOD_PENIS_OVIPOSITOR,
    EPenetratorModifier.PREHENSILE: ETFModifier.TF_MOD_PENIS_PREHENSILE,
    EPenetratorModifier.RIBBED: ETFModifier.TF_MOD_PENIS_RIBBED,
    EPenetratorModifier.SHEATHED: ETFModifier.TF_MOD_PENIS_SHEATHED,
    EPenetratorModifier.TAPERED: ETFModifier.TF_MOD_PENIS_TAPERED,
    EPenetratorModifier.TENTACLED: ETFModifier.TF_MOD_PENIS_TENTACLED,
    EPenetratorModifier.VEINY: ETFModifier.TF_MOD_PENIS_VEINY,
}
TONGUE2TF: Dict[ETongueModifier, ETFModifier] = {
    ETongueModifier.RIBBED: ETFModifier.TF_MOD_TONGUE_RIBBED,
    ETongueModifier.TENTACLED: ETFModifier.TF_MOD_TONGUE_TENTACLED,
    ETongueModifier.BIFURCATED: ETFModifier.TF_MOD_TONGUE_BIFURCATED,
    ETongueModifier.WIDE: ETFModifier.TF_MOD_TONGUE_WIDE,
    ETongueModifier.FLAT: ETFModifier.TF_MOD_TONGUE_FLAT,
    ETongueModifier.STRONG: ETFModifier.TF_MOD_TONGUE_STRONG,
    # ETongueModifier.TAPERED: ETFModifier.TF_MOD_TONGUE_TAPERED, //???
}
TF2TONGUE: Dict[ETFModifier, ETongueModifier] = {
    ETFModifier.TF_MOD_TONGUE_RIBBED: ETongueModifier.RIBBED,
    ETFModifier.TF_MOD_TONGUE_TENTACLED: ETongueModifier.TENTACLED,
    ETFModifier.TF_MOD_TONGUE_BIFURCATED: ETongueModifier.BIFURCATED,
    ETFModifier.TF_MOD_TONGUE_WIDE: ETongueModifier.WIDE,
    ETFModifier.TF_MOD_TONGUE_FLAT: ETongueModifier.FLAT,
    ETFModifier.TF_MOD_TONGUE_STRONG: ETongueModifier.STRONG,
    # ETFModifier.TF_MOD_TONGUE_TAPERED: ETongueModifier.TAPERED,
}

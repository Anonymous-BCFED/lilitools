from enum import Enum
from typing import Any, Literal, Optional, Set


class InvalidValueError(Exception):
    pass


def e2str(e: Optional[Enum]) -> Optional[str]:
    return None if e is None else e.name


def bool2yesno(istrue: bool) -> Literal["Yes", "No"]:
    return "Yes" if istrue else "No"


def bool2yn(istrue: bool) -> Literal["Y", "N"]:
    return "Y" if istrue else "N"


def bool2truefalse(istrue: bool) -> Literal["True", "False"]:
    return "True" if istrue else "False"


def bool2tf(istrue: bool) -> Literal["T", "F"]:
    return "T" if istrue else "F"


def _str2bool(value: str, trueval: str, falseval: str) -> bool:
    if value == trueval:
        return True
    if value == falseval:
        return False
    raise InvalidValueError(value)


def yn2bool(value: str) -> bool:
    return _str2bool(value, "y", "n")


def yesno2bool(value: str) -> bool:
    return _str2bool(value, "yes", "no")


def tf2bool(value: str) -> bool:
    return _str2bool(value, "t", "f")


def truefalse2bool(value: str) -> bool:
    return _str2bool(value, "true", "false")


def clampInt(value: int, min_: int, max_: int) -> int:
    return min(max(value, min_), max_)


def clampFloat(value: float, min_: float, max_: float) -> float:
    return min(max(value, min_), max_)


def are_equal_case_insensitive_strings(a: str, b: str) -> bool:
    return a.casefold() == b.casefold()


def anyMatchIn(a: Set[Any], b: Set[Any]) -> bool:
    if len(a) == 0 or len(b) == 0:
        return False
    return len(a.intersection(b)) > 0

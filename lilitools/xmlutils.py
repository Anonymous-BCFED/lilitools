from enum import Enum, IntEnum
from typing import (
    Any,
    Callable,
    Dict,
    Iterable,
    List,
    Optional,
    Set,
    Tuple,
    Type,
    Union,
)

from lxml import etree

from lilitools.consts import PRESERVE_SAVE_ORDERING
from lilitools.saves.exceptions import (
    AttributeRequiredException,
    CannotSerializeToXML,
    ElementRequiredException,
    KeyRequiredException,
)


def py2xmlstr(data: Any) -> str:
    if data is None:
        return "null"
    if isinstance(data, str):
        return data
    elif isinstance(data, (int, float)):
        return str(data)
    elif isinstance(data, bool):
        return "true" if data else "false"
    else:
        raise CannotSerializeToXML(data)


def xmlstr2py(
    value: str,
    type: Optional[Callable[[str], Any]] = None,
    enum_name: Optional[Enum] = None,
    enum_value: Optional[Enum] = None,
) -> Optional[Any]:
    if type == bool:

        def type(x):
            return x == "true"

    elif type is None or type == str:

        def type(x):
            return x

    if enum_name is not None:

        def type(x):
            return enum_name[x]

    if enum_value is not None:
        if isinstance(enum_value, IntEnum):
            type = int

        def type(x):
            return enum_value(type(x))

    return type(value)


def makeElement(
    parent: etree._Element,
    tag: str,
    attrs: Dict[str, str] = {},
    text: Optional[Union[str, etree.CDATA]] = None,
) -> etree._Element:
    e = etree.SubElement(parent, tag, attrs, None)
    if text:
        e.text = text
    return e


def appendElementValue(parent: etree._Element, tag: str, value: Optional[Any]) -> None:
    etree.SubElement(parent, tag, {"value": py2xmlstr(value)})


def makeElementList(
    parent: etree._Element,
    tag: str,
    entryTag: str,
    entries: List[Union[str, etree._Element]],
) -> etree._Element:
    le = makeElement(parent, tag)
    for entry in entries:
        ee = makeElement(le, entryTag)
        if isinstance(entry, etree.ElementBase):
            ee.append(entry)
        else:
            ee.text = str(entry)
    return le


def getValueFromElementText(
    parent: etree._Element,
    tag: str,
    default: Optional[Any] = None,
    required: bool = False,
    type: Optional[Callable[[str], Any]] = None,
    enum_name: Optional[Enum] = None,
    enum_value: Optional[Enum] = None,
) -> Optional[Any]:
    if type == bool:

        def type(x):
            return x == "true"

    if enum_name is not None:

        def type(x):
            return enum_name[x]

    if enum_value is not None:

        def type(x):
            return enum_value(x)

    if (e := parent.find(tag)) is not None:
        value = e.text
        if type is not None:
            value = type(value)
        return value
    else:
        if required:
            raise ElementRequiredException("xmlutils", parent, "?", tag)
        return default


def getValueFromElementAttribute(
    parent: etree._Element,
    tag: str,
    attr: str = "value",
    default: Optional[Any] = None,
    required: bool = False,
    type: Optional[Callable[[str], Any]] = None,
    enum_name: Optional[Enum] = None,
    enum_value: Optional[Enum] = None,
) -> Optional[Any]:
    if type == bool:

        def type(x):
            return x == "true"

    if enum_name is not None:

        def type(x):
            return enum_name[x]

    if enum_value is not None:

        def type(x):
            return enum_value(x)

    value: Any = None
    if (e := parent.find(tag)) is not None:
        if attr not in e.attrib:
            if required:
                raise AttributeRequiredException("xmlutils", e, attr, "?")
            return default
        value = e.attrib[attr]
        if type is not None:
            value = type(value)
        return value
    else:
        if required:
            raise ElementRequiredException("xmlutils", parent, "?", tag)
        return default


def serializeIndexedList(
    data: List[Any],
    parent_elem_name: str,
    child_elem_name: str,
    index_attr_name: str,
    value_attr_name: Optional[str] = None,
    serializer: Optional[Callable[[Any], str]] = None,
) -> etree._Element:
    """
    Serializes to a form like:

    <parent_elem_name count="n">
        <!-- if value_attr_name is not None: -->
        <child_elem_name index_attr_name="#" value_attr_name="item value"/>
        <!-- else -->
        <child_elem_name index_attr_name="#">item value</child_elem_name>
        <!-- ... -->
    </parent>
    """
    if serializer is None:
        serializer = str
    e = etree.Element(parent_elem_name)
    e.attrib["count"] = str(len(data))
    for i, le in enumerate(data):
        se = etree.SubElement(e, child_elem_name, {index_attr_name: str(i)})
        les: str = serializer(le)
        if value_attr_name is None:
            se.text = les
        else:
            se.attrib[value_attr_name] = les
    return e


def deserializeIndexedList(
    parent: etree._Element,
    child_elem_name: str,
    index_name: str,
    get_value: Optional[Callable[[etree._Element], Any]] = None,
) -> List[Any]:
    """
    Deserializes from a form like:

    <parent>
        <!-- if value_attr_name is not None: -->
        <child_elem_name index_attr_name="#" value_attr_name="item value"/>
        <!-- else -->
        <child_elem_name index_attr_name="#">item value</child_elem_name>
        <!-- ... -->
    </parent>
    """
    if get_value is None:

        def get_value(x):
            return x.text

    tmp: List[Tuple[int, Any]] = []
    i: int
    child: etree._Element
    for child in parent.findall(child_elem_name):
        i = int(child.attrib[index_name])
        tmp.append((i, get_value(child)))
    o: List[Any] = [None] * len(tmp)
    for i, v in tmp:
        o[i] = v
    return o


def serializeDict(
    data: Dict[str, Any],
    parent_elem_name: str,
    child_elem_name: str,
    key_attr_name: str,
    value_attr_name: Optional[str] = None,
    convert_key: Optional[Callable[[Any], str]] = None,
    convert_value: Optional[Callable[[Any], str]] = None,
) -> etree._Element:
    """
    Serializes to a form like:

    <parent_elem_name count="n">
        <!-- if value_attr_name is not None -->
        <child_elem_name key_attr_name="key" value_attr_name="value"/>
        <!-- else -->
        <child_elem_name key_attr_name="key">value</child_elem_name>
    </parent_elem_name>
    """
    if convert_key is None:
        convert_key = py2xmlstr
    if convert_value is None:
        convert_value = py2xmlstr
    e = etree.Element(parent_elem_name)
    e.attrib["count"] = str(len(data))
    for k, v in data.items():
        se = etree.SubElement(e, child_elem_name, {key_attr_name: convert_key(k)})
        vs: str = convert_value(v)
        if value_attr_name is None:
            se.text = vs
        else:
            se.attrib[value_attr_name] = vs
    return e


def deserializeDict(
    e: etree._Element,
    child_elem_name: str,
    key_attr_name: str,
    value_attr_name: Optional[str] = None,
    convert_key: Optional[Callable[[str], Any]] = None,
    convert_value: Optional[Callable[[str], Any]] = None,
) -> Dict[Any, Any]:
    """
    Deserializes from a form like:

    <e>
        <!-- if value_attr_name is not None -->
        <child_elem_name key_attr_name="key" value_attr_name="value"/>
        <!-- else -->
        <child_elem_name key_attr_name="key">value</child_elem_name>
    </e>
    """
    if convert_key is None:
        convert_key = str
    if convert_value is None:
        convert_value = str
    o = {}
    for childElem in e.findall(child_elem_name):
        key: Any = convert_key(childElem.attrib[key_attr_name])
        value: Any = convert_value(
            str(
                childElem.attrib[value_attr_name]
                if value_attr_name is not None
                else childElem.text
            )
        )
        o[key] = value
    return o


def serializeList(
    data: List[Any],
    parent_elem_name: str,
    child_elem_name: str,
    value_attr_name: Optional[str] = None,
    serializer: Optional[Callable[[Any], str]] = None,
) -> etree._Element:
    """
    Serialize to a form like:

    <parent_elem_name count="n">
        <!-- if value_attr_name is not None -->
        <child_elem_name value_attr_name="item"/>
        <!-- else -->
        <child_elem_name>item</child_elem_name>
    </parent_elem_name>
    """
    if serializer is None:
        serializer = str
    e = etree.Element(parent_elem_name)
    e.attrib["count"] = str(len(data))
    for le in data:
        se = etree.SubElement(e, child_elem_name)
        les: str = serializer(le)
        if value_attr_name is None:
            se.text = les
        else:
            se.attrib[value_attr_name] = les
    return e


def deserializeList(
    e: etree._Element,
    child_elem_name: str,
    value_attr_name: Optional[str] = None,
    convert_value: Optional[Callable[[str], Any]] = None,
) -> List[Any]:
    """
    Deserialize to a form like:

    <e>
        <!-- if value_attr_name is not None -->
        <child_elem_name value_attr_name="item"/>
        <!-- else -->
        <child_elem_name>item</child_elem_name>
    </e>
    """
    if convert_value is None:
        convert_value = str
    o = []
    for childElem in e.findall(child_elem_name):
        value: Any = convert_value(
            str(
                childElem.attrib[value_attr_name]
                if value_attr_name is not None
                else childElem.text
            )
        )
        o.append(value)
    return o

def serializeModifierLikeSet(
    data: Iterable[Union[str, Enum]],
    elem_name: str,
    enum_name: Optional[Type[Enum]] = None,
) -> etree._Element:
    """
    Serialize to a form like:

    <e ENUM_A="true" ENUM_B="true"/>
    """
    if len(data) == 0:
        return etree.Element(elem_name)
    return etree.Element(
        elem_name,
        {(k.name if isinstance(k, Enum) else k): "true" for k in data},
    )


if PRESERVE_SAVE_ORDERING:

    def deserializeModifierLikeSet(
        parent: etree._Element,
        child_elem_name: str,
        enum_name: Optional[Type[Enum]] = None,
    ) -> Set[Union[str, Enum]]:
        """
        Deserialize from a form like:

        <e ENUM_A="true" ENUM_B="true"/>
        """
        k: str
        f = parent.find(child_elem_name)
        if f is None:
            return set()
        if enum_name is not None:
            return set([enum_name[k] for k in f.attrib.keys()])
        else:
            return set([k for k in f.attrib.keys()])

else:

    def deserializeModifierLikeSet(
        parent: etree._Element,
        child_elem_name: str,
        enum_name: Optional[Type[Enum]] = None,
    ) -> Set[Union[str, Enum]]:
        """
        Deserialize from a form like:

        <e ENUM_A="true" ENUM_B="true"/>
        """
        k: str
        if enum_name is not None:
            return set(
                [enum_name[k] for k in parent.find(child_elem_name).attrib.keys()]
            )
        else:
            return set([k for k in parent.find(child_elem_name).attrib.keys()])


def mergetags(left: etree._Element, right: etree._Element) -> None:
    left.attrib.update(right.attrib)
    for tag in right.getchildren():
        left.append(tag)

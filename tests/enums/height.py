import unittest

from lilitools.saves.character.height import EHeight


class EHeightTests(unittest.TestCase):
    def test_height_enchantmentlimit2height(self) -> None:
        inp: int = 43
        expected: int = 165
        actual: int = EHeight.enchantmentLimit2Height(inp)
        self.assertEqual(expected, actual)

    def test_height_height2enchantmentlimit(self) -> None:
        inp: int = 165
        expected: int = 43
        actual: int = EHeight.height2enchantmentLimit(inp)
        self.assertEqual(expected, actual)

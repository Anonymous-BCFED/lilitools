import unittest

from _dev_regex import (
    REG_SIGREF,
    REG_VERSION_SEARCH,
    REG_VERSION_STR,
    REG_JAVA_FLOAT,
    REG_JAVA_INT,
    REG_ENUM,
    REG_ALL_CLOTHING,
    REG_DOCS_DATA,
)


class RegexTests(unittest.TestCase):
    def test_version_search(self) -> None:
        subj: str = 'public static final String VERSION_NUMBER = "0.4.7";'
        self.assertRegex(subj, REG_VERSION_SEARCH)

    def test_version_str(self) -> None:
        subj: str = 'public static final String VERSION_NUMBER = "0.4.7";'
        self.assertRegex(subj, REG_VERSION_STR)

    def test_sigref(self) -> None:
        subj: str = (
            "## from [LT]/src/com/lilithsthrone/utils/colours/ColourListPresets.java: static { @ 5/0L4lKiENUXHsAQiJa4dddaTsl2z9sx4L463YLrXrReu3Ueqi3qUnbEHRt0+yu9s/VQ/P7fD2DHSHPC4KbtyA=="
        )
        self.assertRegex(subj, REG_SIGREF)

    def test_java_int(self) -> None:
        self.assertRegex("1", REG_JAVA_INT)
        self.assertRegex("-1", REG_JAVA_INT)
        self.assertRegex("1_000_324", REG_JAVA_INT)

    def test_java_float(self) -> None:
        self.assertRegex("1f", REG_JAVA_FLOAT)
        self.assertRegex("1060435.0", REG_JAVA_FLOAT)

    def test_enum(self) -> None:
        self.assertRegex("public enum CombatBehaviour {", REG_ENUM)

    def test_clothing(self) -> None:
        self.assertRegex("CLOTHING_GREY_DARK", REG_ALL_CLOTHING)
        self.assertRegex("CLOTHING_BLACK", REG_ALL_CLOTHING)

    def test_data(self) -> None:
        self.assertRegex("<@data table_main_room_regex {} @>", REG_DOCS_DATA)

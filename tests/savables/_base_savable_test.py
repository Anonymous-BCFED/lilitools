from pathlib import Path
import unittest
from collections import Counter
from typing import Collection, Set, Type

from lxml import etree

from lilitools.saves.savable import Savable


class BaseSavableTest(unittest.TestCase):
    def parseToElement(self, xmlstr: str) -> etree._Element:
        return etree.fromstring(xmlstr)

    def parseToSavable(self, xmlstr: str, savable_type: Type[Savable]) -> Savable:
        s = savable_type()
        s.fromXML(self.parseToElement(xmlstr))
        return s

    def parseFileToSavable(self, filename: Path, savable_type: Type[Savable]) -> Savable:
        s = savable_type()
        s.fromXML(self.parseToElement(filename.read_text()))
        return s

    def xml2str(self, element: etree._Element) -> str:
        return etree.tostring(element, encoding='utf-8')

    def assertAttrEquals(self, element: etree._Element, attr: str, expectedValue: str) -> None:
        self.assertIn(attr, element.attrib)
        self.assertEqual(expectedValue, element.attrib[attr])

    def assertAttrNotExists(self, element: etree._Element, attr: str) -> None:
        self.assertNotIn(attr, element.attrib)

    def assertElementExists(self, parent: etree._Element, elem: str) -> None:
        self.assertIsNotNone(parent.find(elem))

    def assertAttrsMustExist(self, parent: etree._Element, attrnames: Collection[str]) -> None:
        self.assertTrue(all((x in parent.attrib.keys()) for x in attrnames), set(attrnames)-set(parent.attrib.keys()))
    def assertOnlyTheseAttrsMayExist(self, parent: etree._Element, attrnames: Collection[str]) -> None:
        self.assertFalse(any((x not in attrnames) for x in parent.attrib.keys()), set(parent.attrib.keys()) - set(attrnames))
    def assertAttrMustExist(self, parent: etree._Element, attrname: str) -> None:
        self.assertIn(attrname, parent.attrib.keys())
    def assertNoAttrs(self, parent: etree._Element) -> None:
        self.assertEqual(len(parent.attrib), 0)

    def assertChildrenMustExist(self, parent:etree._Element, tags: Set[str]) -> None:
        occurs = Counter([x.tag for x in parent])
        self.assertTrue(all((t in occurs.keys()) for t in tags), set(tags)-set(occurs.keys()))

    def assertOnlyTheseChildrenMayExist(self, parent: etree._Element, tags: Set[str]) -> None:
        occurs = Counter([x.tag for x in parent])
        self.assertFalse(any((t not in tags) for t in occurs.keys()))

    def assertChildMustOccurOnlyOnce(self, parent: etree._Element, tag: str) -> None:
        occurs = Counter([x.tag for x in parent])
        self.assertEqual(occurs[tag], 1)

    def assertChildMustOccurAtLeastOnce(self, parent: etree._Element, tag: str) -> None:
        self.assertChildMustOccurAtLeast(parent,tag,1)

    def assertChildMustOccurAtLeast(self, parent: etree._Element, tag: str, lowerBound: int) -> None:
        occurs = Counter([x.tag for x in parent])
        self.assertGreaterEqual(occurs[tag], lowerBound)

    def assertNoChildren(self, parent: etree._Element) -> None:
        self.assertEqual(len(parent.getchildren()), 0)

from typing import AbstractSet
from lilitools.saves.character.body.antenna import Antenna
from tests.savables._base_savable_test import BaseSavableTest


class AntennaTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset({"antennaePerRow", "length", "rows", "type"})
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({"antennaePerRow", "length", "rows", "type"})
    REQD_CHILDREN: AbstractSet[str] = frozenset()
    ALLOWED_CHILDREN: AbstractSet[str] = frozenset()

    def setUp(self) -> None:
        """<antennae antennaePerRow="2" length="22" rows="1" type="NONE"/>"""
        self.antennae = Antenna()
        self.antennae.antennaePerRow = 2
        self.antennae.length = 22
        self.antennae.rows = 1
        self.antennae.type = "NONE"

    def test_antennae_toxml(self) -> None:
        e = self.antennae.toXML()
        self.assertAttrsMustExist(e, self.REQD_ATTRS)
        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertAttrEquals(e, "antennaePerRow", "2")
        self.assertAttrEquals(e, "length", "22")
        self.assertAttrEquals(e, "rows", "1")
        self.assertAttrEquals(e, "type", "NONE")

    def test_antennae_fromxml(self) -> None:
        o: Antenna = self.parseToSavable('<antennae antennaePerRow="2" length="22" rows="1" type="NONE"/>', Antenna)
        self.assertEqual(o.antennaePerRow, 2)
        self.assertEqual(o.length, 22)
        self.assertEqual(o.rows, 1)
        self.assertEqual(o.type, "NONE")

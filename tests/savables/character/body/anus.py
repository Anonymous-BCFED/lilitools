from typing import AbstractSet
from lilitools.saves.character.body.anus import Anus
from lilitools.saves.character.enums.body_hair import EBodyHair
from lilitools.saves.character.enums.orifice_modifier import EOrificeModifier
from tests.savables._base_savable_test import BaseSavableTest


class AnusTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset({"assHair", "bleached", "capacity", "depth", "elasticity", "plasticity", "stretchedCapacity", "virgin", "wetness"})
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({"assHair", "bleached", "capacity", "depth", "elasticity", "plasticity", "stretchedCapacity", "virgin", "wetness"})
    REQD_CHILDREN: AbstractSet[str] = frozenset({"anusModifiers"})
    ALLOWED_CHILDREN: AbstractSet[str] = frozenset({"anusModifiers"})

    def setUp(self) -> None:
        """<anus assHair="ZERO_NONE" bleached="true" capacity="10.5" depth="4" elasticity="5" plasticity="4" stretchedCapacity="10.5" virgin="false" wetness="0">
            <anusModifiers MUSCLE_CONTROL="true" PUFFY="true" RIBBED="true"/>
        </anus>"""
        self.anus = Anus()
        self.anus.assHair = EBodyHair.ZERO_NONE
        self.anus.bleached = True
        self.anus.orifice.capacity = 10.5
        self.anus.orifice.depth = 4
        self.anus.orifice.elasticity = 5
        self.anus.orifice.plasticity = 4
        self.anus.orifice.stretchedCapacity = 10.5
        self.anus.orifice.orificeVirgin = False
        self.anus.orifice.wetness = 0
        self.anus.orifice.orificeModifiers = {EOrificeModifier.MUSCLE_CONTROL, EOrificeModifier.PUFFY, EOrificeModifier.RIBBED}

    def test_anus_toxml(self) -> None:
        e = self.anus.toXML()

        self.assertAttrsMustExist(e, self.REQD_ATTRS)
        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertChildrenMustExist(e, self.REQD_CHILDREN)
        self.assertOnlyTheseChildrenMayExist(e, self.ALLOWED_CHILDREN)
        self.assertChildMustOccurOnlyOnce(e, "anusModifiers")
        self.assertAttrEquals(e, "assHair", "ZERO_NONE")
        self.assertAttrEquals(e, "bleached", "true")
        self.assertAttrEquals(e, "capacity", "10.5")
        self.assertAttrEquals(e, "depth", "4")
        self.assertAttrEquals(e, "elasticity", "5")
        self.assertAttrEquals(e, "plasticity", "4")
        self.assertAttrEquals(e, "stretchedCapacity", "10.5")
        self.assertAttrEquals(e, "virgin", "false")
        self.assertAttrEquals(e, "wetness", "0")
        self.assertIsNotNone((m := e.find("anusModifiers")))
        self.assertAttrEquals(m, "MUSCLE_CONTROL", "true")
        self.assertAttrEquals(m, "PUFFY", "true")
        self.assertAttrEquals(m, "RIBBED", "true")
        self.assertAttrEquals(m, "TENTACLED", "true")

    def test_anus_fromxml(self) -> None:
        o: Anus = self.parseToSavable(
            """<anus assHair="ZERO_NONE" bleached="true" capacity="10.5" depth="4" elasticity="5" plasticity="4" stretchedCapacity="10.5" virgin="false" wetness="0">
    <anusModifiers MUSCLE_CONTROL="true" PUFFY="true" RIBBED="true" TENTACLED="true"/>
</anus>""",
            Anus,
        )
        self.assertEqual(o.assHair, EBodyHair.ZERO_NONE)
        self.assertEqual(o.bleached, True)
        self.assertEqual(o.orifice.capacity, 10.5)
        self.assertEqual(o.orifice.depth, 4)
        self.assertEqual(o.orifice.elasticity, 5)
        self.assertEqual(o.orifice.plasticity, 4)
        self.assertEqual(o.orifice.stretchedCapacity, 10.5)
        self.assertEqual(o.orifice.orificeVirgin, False)
        self.assertEqual(o.orifice.wetness, 0)
        self.assertSetEqual(o.orifice.orificeModifiers, {EOrificeModifier.MUSCLE_CONTROL, EOrificeModifier.PUFFY, EOrificeModifier.RIBBED, EOrificeModifier.TENTACLED})

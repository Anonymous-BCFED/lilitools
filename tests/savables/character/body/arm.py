from typing import AbstractSet
from lilitools.saves.character.body.arms import Arms
from lilitools.saves.character.enums.body_hair import EBodyHair
from tests.savables._base_savable_test import BaseSavableTest


class ArmsTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset({"rows", "type", "underarmHair"})
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({"rows", "type", "underarmHair"})
    REQD_CHILDREN: AbstractSet[str] = frozenset()
    ALLOWED_CHILDREN: AbstractSet[str] = frozenset()

    def setUp(self) -> None:
        """<arm rows="1" type="HUMAN" underarmHair="ZERO_NONE"/>"""
        self.arm = Arms()
        self.arm.rows = 1
        self.arm.type = 'HUMAN'
        self.arm.underarmHair = EBodyHair.ZERO_NONE

    def test_arm_toxml(self) -> None:
        e = self.arm.toXML()
        self.assertAttrsMustExist(e, self.REQD_ATTRS)
        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertAttrEquals(e, "rows", "1")
        self.assertAttrEquals(e, "type", "HUMAN")
        self.assertAttrEquals(e, "underarmHair", "ZERO_NONE")

    def test_arm_fromxml(self) -> None:
        o: Arms = self.parseToSavable('<arm rows="1" type="HUMAN" underarmHair="ZERO_NONE"/>', Arms)
        self.assertEqual(o.rows, 1)
        self.assertEqual(o.type, 'HUMAN')
        self.assertEqual(o.underarmHair, EBodyHair.ZERO_NONE)

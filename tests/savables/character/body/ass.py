from typing import AbstractSet
from lilitools.saves.character.body.ass import Ass
from tests.savables._base_savable_test import BaseSavableTest


class AssTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset({"assSize", "hipSize", "type"})
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({"assSize", "hipSize", "type"})
    REQD_CHILDREN: AbstractSet[str] = frozenset()
    ALLOWED_CHILDREN: AbstractSet[str] = frozenset()

    def setUp(self) -> None:
        """<ass assSize="4" hipSize="4" type="HUMAN"/>"""
        self.ass = Ass()
        self.ass.assSize = 4
        self.ass.hipSize = 4
        self.ass.type = "HUMAN"

    def test_ass_toxml(self) -> None:
        e = self.ass.toXML()
        self.assertAttrsMustExist(e, self.REQD_ATTRS)
        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertAttrEquals(e, "assSize", "4")
        self.assertAttrEquals(e, "hipSize", "4")
        self.assertAttrEquals(e, "type", "HUMAN")

    def test_ass_fromxml(self) -> None:
        o: Ass = self.parseToSavable('<ass assSize="4" hipSize="4" type="HUMAN"/>', Ass)
        self.assertEqual(o.assSize, 4)
        self.assertEqual(o.hipSize, 4)
        self.assertEqual(o.type, "HUMAN")

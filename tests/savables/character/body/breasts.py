from typing import AbstractSet
from lilitools.saves.character.body.breasts import Breasts
from lilitools.saves.character.enums.breast_shape import EBreastShape
from tests.savables._base_savable_test import BaseSavableTest


class BreastsTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset({"milkRegeneration", "milkStorage", "nippleCountPerBreast", "rows", "shape", "size", "storedMilk", "type"})
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({"milkRegeneration", "milkStorage", "nippleCountPerBreast", "rows", "shape", "size", "storedMilk", "type"})
    REQD_CHILDREN: AbstractSet[str] = frozenset()
    ALLOWED_CHILDREN: AbstractSet[str] = frozenset()

    def setUp(self) -> None:
        """<breasts milkRegeneration="500" milkStorage="0" nippleCountPerBreast="1" rows="1" shape="POINTY" size="8" storedMilk="0.0" type="HUMAN"/>"""
        self.breasts = Breasts()
        self.breasts.milkRegeneration = 500
        self.breasts.milkStorage = 0
        self.breasts.nippleCountPerBreast = 1
        self.breasts.rows = 1
        self.breasts.shape = EBreastShape.POINTY
        self.breasts.size = 8
        self.breasts.storedMilk = 0.0
        self.breasts.type = "HUMAN"

    def test_breasts_toxml(self) -> None:
        e = self.breasts.toXML()
        self.assertAttrsMustExist(e, self.REQD_ATTRS)
        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertAttrEquals(e, "milkRegeneration", "500")
        self.assertAttrEquals(e, "milkStorage", "0")
        self.assertAttrEquals(e, "nippleCountPerBreast", "1")
        self.assertAttrEquals(e, "rows", "1")
        self.assertAttrEquals(e, "shape", "POINTY")
        self.assertAttrEquals(e, "size", "8")
        self.assertAttrEquals(e, "storedMilk", "0.0")
        self.assertAttrEquals(e, "type", "HUMAN")

    def test_breasts_fromxml(self) -> None:
        o: Breasts = self.parseToSavable('<breasts milkRegeneration="500" milkStorage="0" nippleCountPerBreast="1" rows="1" shape="POINTY" size="8" storedMilk="0.0" type="HUMAN"/>', Breasts)
        self.assertEqual(o.milkRegeneration, 500)
        self.assertEqual(o.milkStorage, 0)
        self.assertEqual(o.nippleCountPerBreast, 1)
        self.assertEqual(o.rows, 1)
        self.assertEqual(o.shape, EBreastShape.POINTY)
        self.assertEqual(o.size, 8)
        self.assertEqual(o.storedMilk, 0.0)
        self.assertEqual(o.type, "HUMAN")

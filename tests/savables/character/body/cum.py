from typing import AbstractSet
from lilitools.saves.character.fluids.cum import Cum
from lilitools.saves.character.fluids.enums.fluid_modifier import EFluidModifier
from tests.savables._base_savable_test import BaseSavableTest


class CumTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset({"flavour", "type"})
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({"flavour", "type"})
    REQD_CHILDREN: AbstractSet[str] = frozenset({"cumModifiers"})
    ALLOWED_CHILDREN: AbstractSet[str] = frozenset({"cumModifiers"})

    def setUp(self) -> None:
        """<cum flavour="CUM" type="CUM_HUMAN">
            <cumModifiers ADDICTIVE="true" MUSKY="true" SLIMY="true" STICKY="true"/>
        </cum>"""
        self.cum = Cum()
        self.cum.flavour = "CUM"
        self.cum.type = "CUM_HUMAN"
        self.cum.modifiers = {EFluidModifier.ADDICTIVE, EFluidModifier.MUSKY, EFluidModifier.SLIMY, EFluidModifier.STICKY}

    def test_cum_toxml(self) -> None:
        e = self.cum.toXML()
        self.assertAttrsMustExist(e, self.REQD_ATTRS)
        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertChildrenMustExist(e, self.REQD_CHILDREN)
        self.assertOnlyTheseChildrenMayExist(e, self.ALLOWED_CHILDREN)

        self.assertChildMustOccurOnlyOnce(e, "cumModifiers")

        self.assertAttrEquals(e, "flavour", "CUM")
        self.assertAttrEquals(e, "type", "CUM_HUMAN")
        self.assertIsNotNone((m := e.find("cumModifiers")))
        self.assertAttrEquals(m, "ADDICTIVE", "true")
        self.assertAttrEquals(m, "MUSKY", "true")
        self.assertAttrEquals(m, "SLIMY", "true")
        self.assertAttrEquals(m, "STICKY", "true")

    def test_cum_fromxml(self) -> None:
        o: Cum = self.parseToSavable(
            """<cum flavour="CUM" type="CUM_HUMAN">
    <cumModifiers ADDICTIVE="true" MUSKY="true" SLIMY="true" STICKY="true"/>
</cum>""",
            Cum,
        )
        self.assertEqual(o.flavour, "CUM")
        self.assertEqual(o.type, "CUM_HUMAN")
        self.assertSetEqual(self.cum.modifiers, {EFluidModifier.ADDICTIVE, EFluidModifier.MUSKY, EFluidModifier.SLIMY, EFluidModifier.STICKY})

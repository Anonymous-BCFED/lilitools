from typing import AbstractSet

from lilitools.saves.character.body.ears import Ears
from tests.savables._base_savable_test import BaseSavableTest


class EarsTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset({"pierced", "type"})
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({"pierced", "type"})
    REQD_CHILDREN: AbstractSet[str] = frozenset()
    ALLOWED_CHILDREN: AbstractSet[str] = frozenset()

    def setUp(self) -> None:
        """<ear pierced="true" type="HORSE_MORPH"/>"""
        self.ear = Ears()
        self.ear.pierced = True
        self.ear.type = "HORSE_MORPH"

    def test_ear_toxml(self) -> None:
        e = self.ear.toXML()
        self.assertAttrsMustExist(e, self.REQD_ATTRS)
        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertAttrEquals(e, "pierced", "true")
        self.assertAttrEquals(e, "type", "HORSE_MORPH")

    def test_ear_fromxml(self) -> None:
        o: Ears = self.parseToSavable('<ear pierced="true" type="HORSE_MORPH"/>', Ears)
        self.assertEqual(o.pierced, True)
        self.assertEqual(o.type, "HORSE_MORPH")

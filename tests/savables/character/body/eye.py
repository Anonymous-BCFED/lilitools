from typing import AbstractSet
from lilitools.saves.character.body.eyes import Eyes
from lilitools.saves.character.body.enums.eye_shape import EEyeShape
from tests.savables._base_savable_test import BaseSavableTest


class EyesTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset({"eyePairs", "irisShape", "pupilShape", "type"})
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({"eyePairs", "irisShape", "pupilShape", "type"})
    REQD_CHILDREN: AbstractSet[str] = frozenset()
    ALLOWED_CHILDREN: AbstractSet[str] = frozenset()

    def setUp(self) -> None:
        """<eye eyePairs="1" irisShape="ROUND" pupilShape="HORIZONTAL" type="HORSE_MORPH"/>"""
        self.eye = Eyes()
        self.eye.eyePairs = 1
        self.eye.irisShape = EEyeShape.ROUND
        self.eye.pupilShape = EEyeShape.HORIZONTAL
        self.eye.type = "HORSE_MORPH"

    def test_eye_toxml(self) -> None:
        e = self.eye.toXML()
        self.assertAttrsMustExist(e, self.REQD_ATTRS)
        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertAttrEquals(e, "eyePairs", "1")
        self.assertAttrEquals(e, "irisShape", "ROUND")
        self.assertAttrEquals(e, "pupilShape", "HORIZONTAL")
        self.assertAttrEquals(e, "type", "HORSE_MORPH")

    def test_eye_fromxml(self) -> None:
        o: Eyes = self.parseToSavable('<eye eyePairs="1" irisShape="ROUND" pupilShape="HORIZONTAL" type="HORSE_MORPH"/>', Eyes)
        self.assertEqual(o.eyePairs, 1)
        self.assertEqual(o.irisShape, EEyeShape.ROUND)
        self.assertEqual(o.pupilShape, EEyeShape.HORIZONTAL)
        self.assertEqual(o.type, "HORSE_MORPH")

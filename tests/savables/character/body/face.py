from typing import AbstractSet
from lilitools.saves.character.body.face import Face
from lilitools.saves.character.enums.body_hair import EBodyHair
from tests.savables._base_savable_test import BaseSavableTest


class FaceTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset({"facialHair", "piercedNose", "type"})
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({"facialHair", "piercedNose", "type"})
    REQD_CHILDREN: AbstractSet[str] = frozenset()
    ALLOWED_CHILDREN: AbstractSet[str] = frozenset()

    def setUp(self) -> None:
        """<face facialHair="ZERO_NONE" piercedNose="true" type="HUMAN"/>"""
        self.face = Face()
        self.face.facialHair = EBodyHair.ZERO_NONE
        self.face.piercedNose = True
        self.face.type = "HUMAN"

    def test_face_toxml(self) -> None:
        e = self.face.toXML()
        self.assertAttrsMustExist(e, self.REQD_ATTRS)
        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertAttrEquals(e, "facialHair", "ZERO_NONE")
        self.assertAttrEquals(e, "piercedNose", "true")
        self.assertAttrEquals(e, "type", "HUMAN")

    def test_face_fromxml(self) -> None:
        o: Face = self.parseToSavable('<face facialHair="ZERO_NONE" piercedNose="true" type="HUMAN"/>', Face)
        self.assertEqual(o.facialHair, EBodyHair.ZERO_NONE)
        self.assertEqual(o.piercedNose, True)
        self.assertEqual(o.type, "HUMAN")

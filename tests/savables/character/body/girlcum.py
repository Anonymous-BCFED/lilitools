from typing import AbstractSet
from lilitools.saves.character.fluids.girlcum import GirlCum
from lilitools.saves.character.fluids.enums.fluid_modifier import EFluidModifier
from tests.savables._base_savable_test import BaseSavableTest


class GirlCumTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset({"flavour", "type"})
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({"flavour", "type"})
    REQD_CHILDREN: AbstractSet[str] = frozenset({"girlcumModifiers"})
    ALLOWED_CHILDREN: AbstractSet[str] = frozenset({"girlcumModifiers"})

    def setUp(self) -> None:
        """<girlcum flavour="GIRL_CUM" type="GIRL_CUM_HUMAN">
            <girlcumModifiers ADDICTIVE="true" MUSKY="true" SLIMY="true"/>
        </girlcum>"""
        self.girlcum = GirlCum()
        self.girlcum.flavour = "GIRL_CUM"
        self.girlcum.type = "GIRL_CUM_HUMAN"
        self.girlcum.modifiers = {EFluidModifier.ADDICTIVE, EFluidModifier.MUSKY, EFluidModifier.SLIMY}

    def test_girlcum_toxml(self) -> None:
        e = self.girlcum.toXML()
        self.assertAttrsMustExist(e, self.REQD_ATTRS)
        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertChildrenMustExist(e, self.REQD_CHILDREN)
        self.assertOnlyTheseChildrenMayExist(e, self.ALLOWED_CHILDREN)
        self.assertChildMustOccurOnlyOnce(e, "girlcumModifiers")
        self.assertAttrEquals(e, "flavour", "GIRL_CUM")
        self.assertAttrEquals(e, "type", "GIRL_CUM_HUMAN")
        self.assertIsNotNone((m := e.find("girlcumModifiers")))
        self.assertAttrEquals(m, "ADDICTIVE", "true")
        self.assertAttrEquals(m, "MUSKY", "true")
        self.assertAttrEquals(m, "SLIMY", "true")

    def test_girlcum_fromxml(self) -> None:
        o: GirlCum = self.parseToSavable(
            """<girlcum flavour="GIRL_CUM" type="GIRL_CUM_HUMAN">
    <girlcumModifiers ADDICTIVE="true" MUSKY="true" SLIMY="true"/>
</girlcum>""",
            GirlCum,
        )
        self.assertEqual(o.flavour, "GIRL_CUM")
        self.assertEqual(o.type, "GIRL_CUM_HUMAN")
        self.assertSetEqual(self.girlcum.modifiers, {EFluidModifier.ADDICTIVE, EFluidModifier.MUSKY, EFluidModifier.SLIMY})

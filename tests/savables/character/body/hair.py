from typing import AbstractSet
from lilitools.saves.character.body.hair import Hair
from lilitools.saves.character.body.enums.hair_style import EHairStyle
from tests.savables._base_savable_test import BaseSavableTest


class HairTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset({"hairStyle", "length", "neckFluff", "type"})
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({"hairStyle", "length", "neckFluff", "type"})
    REQD_CHILDREN: AbstractSet[str] = frozenset()
    ALLOWED_CHILDREN: AbstractSet[str] = frozenset()

    def setUp(self) -> None:
        """<hair hairStyle="BUN" length="45" neckFluff="false" type="HORSE_MORPH"/>"""
        self.hair = Hair()
        self.hair.hairStyle = EHairStyle.BUN
        self.hair.length = 45
        self.hair.neckFluff = False
        self.hair.type = "HORSE_MORPH"

    def test_hair_toxml(self) -> None:
        e = self.hair.toXML()
        self.assertAttrsMustExist(e, self.REQD_ATTRS)
        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertAttrEquals(e, "hairStyle", "BUN")
        self.assertAttrEquals(e, "length", "45")
        self.assertAttrEquals(e, "neckFluff", "false")
        self.assertAttrEquals(e, "type", "HORSE_MORPH")

    def test_hair_fromxml(self) -> None:
        o: Hair = self.parseToSavable('<hair hairStyle="BUN" length="45" neckFluff="false" type="HORSE_MORPH"/>', Hair)
        self.assertEqual(o.hairStyle, EHairStyle.BUN)
        self.assertEqual(o.length, 45)
        self.assertEqual(o.neckFluff, False)
        self.assertEqual(o.type, "HORSE_MORPH")

from typing import AbstractSet
from lilitools.saves.character.body.horns import Horns
from tests.savables._base_savable_test import BaseSavableTest


class HornsTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset({"hornsPerRow", "length", "rows", "type"})
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({"hornsPerRow", "length", "rows", "type"})
    REQD_CHILDREN: AbstractSet[str] = frozenset()
    ALLOWED_CHILDREN: AbstractSet[str] = frozenset()

    def setUp(self) -> None:
        """<horn hornsPerRow="1" length="2" rows="1" type="NONE"/>"""
        self.horn = Horns()
        self.horn.hornsPerRow = 1
        self.horn.length = 2
        self.horn.rows = 1
        self.horn.type = "NONE"

    def test_horn_toxml(self) -> None:
        e = self.horn.toXML()
        self.assertAttrsMustExist(e, self.REQD_ATTRS)
        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertAttrEquals(e, "hornsPerRow", "1")
        self.assertAttrEquals(e, "length", "2")
        self.assertAttrEquals(e, "rows", "1")
        self.assertAttrEquals(e, "type", "NONE")

    def test_horn_fromxml(self) -> None:
        o: Horns = self.parseToSavable('<horn hornsPerRow="1" length="2" rows="1" type="NONE"/>', Horns)
        self.assertEqual(o.hornsPerRow, 1)
        self.assertEqual(o.length, 2)
        self.assertEqual(o.rows, 1)
        self.assertEqual(o.type, "NONE")

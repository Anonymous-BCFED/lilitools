from typing import AbstractSet
from lilitools.saves.character.body.legs import Legs
from lilitools.saves.character.body.enums.leg_configuration import ELegConfiguration
from lilitools.saves.character.body.enums.foot_structure import EFootStructure
from tests.savables._base_savable_test import BaseSavableTest


class LegsTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset({"configuration", "footStructure", "tailLength", "type"})
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({"configuration", "footStructure", "tailLength", "type"})
    REQD_CHILDREN: AbstractSet[str] = frozenset()
    ALLOWED_CHILDREN: AbstractSet[str] = frozenset()

    def setUp(self) -> None:
        """<leg configuration="BIPEDAL" footStructure="PLANTIGRADE" tailLength="5.0" type="HUMAN"/>"""
        self.leg = Legs()
        self.leg.configuration = ELegConfiguration.BIPEDAL
        self.leg.footStructure = EFootStructure.PLANTIGRADE
        self.leg.tailLength = 5.0
        self.leg.type = "HUMAN"

    def test_leg_toxml(self) -> None:
        e = self.leg.toXML()
        self.assertAttrsMustExist(e, self.REQD_ATTRS)
        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertAttrEquals(e, "configuration", "BIPEDAL")
        self.assertAttrEquals(e, "footStructure", "PLANTIGRADE")
        self.assertAttrEquals(e, "tailLength", "5.0")
        self.assertAttrEquals(e, "type", "HUMAN")

    def test_leg_fromxml(self) -> None:
        o: Legs = self.parseToSavable('<leg configuration="BIPEDAL" footStructure="PLANTIGRADE" tailLength="5.0" type="HUMAN"/>', Legs)
        self.assertEqual(o.configuration, ELegConfiguration.BIPEDAL)
        self.assertEqual(o.footStructure, EFootStructure.PLANTIGRADE)
        self.assertEqual(o.tailLength, 5.0)
        self.assertEqual(o.type, "HUMAN")

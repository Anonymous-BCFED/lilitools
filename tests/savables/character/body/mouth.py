from typing import AbstractSet
from lilitools.saves.character.body.mouth import Mouth
from lilitools.saves.character.enums.orifice_modifier import EOrificeModifier
from tests.savables._base_savable_test import BaseSavableTest


class MouthTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset({"capacity", "depth", "elasticity", "lipSize", "piercedLip", "plasticity", "stretchedCapacity", "virgin", "wetness"})
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({"capacity", "depth", "elasticity", "lipSize", "piercedLip", "plasticity", "stretchedCapacity", "virgin", "wetness"})
    REQD_CHILDREN: AbstractSet[str] = frozenset({"mouthModifiers"})
    ALLOWED_CHILDREN: AbstractSet[str] = frozenset({"mouthModifiers"})

    def setUp(self) -> None:
        """<mouth capacity="10.5" depth="2" elasticity="4" lipSize="2" piercedLip="true" plasticity="3" stretchedCapacity="10.5" virgin="false" wetness="3">
            <mouthModifiers/>
        </mouth>"""
        self.mouth = Mouth()
        self.mouth.orifice.capacity = 10.5
        self.mouth.orifice.depth = 2
        self.mouth.orifice.elasticity = 4
        self.mouth.lipSize = 2
        self.mouth.piercedLip = True
        self.mouth.orifice.plasticity = 3
        self.mouth.orifice.stretchedCapacity = 10.5
        self.mouth.orifice.orificeVirgin = False
        self.mouth.orifice.wetness = 3
        self.mouth.orifice.orificeModifiers = {
            EOrificeModifier.MUSCLE_CONTROL,
            EOrificeModifier.RIBBED,
            EOrificeModifier.TENTACLED,
        }

    def test_mouth_toxml(self) -> None:
        e = self.mouth.toXML()
        self.assertAttrsMustExist(e, self.REQD_ATTRS)
        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertChildrenMustExist(e, self.REQD_CHILDREN)
        self.assertOnlyTheseChildrenMayExist(e, self.ALLOWED_CHILDREN)
        self.assertChildMustOccurOnlyOnce(e, "mouthModifiers")
        self.assertAttrEquals(e, "capacity", "10.5")
        self.assertAttrEquals(e, "depth", "2")
        self.assertAttrEquals(e, "elasticity", "4")
        self.assertAttrEquals(e, "lipSize", "2")
        self.assertAttrEquals(e, "piercedLip", "true")
        self.assertAttrEquals(e, "plasticity", "3")
        self.assertAttrEquals(e, "stretchedCapacity", "10.5")
        self.assertAttrEquals(e, "virgin", "false")
        self.assertAttrEquals(e, "wetness", "3")
        self.assertIsNotNone((m := e.find("mouthModifiers")))
        self.assertAttrEquals(m, "MUSCLE_CONTROL", "true")
        self.assertAttrEquals(m, "RIBBED", "true")
        self.assertAttrEquals(m, "TENTACLED", "true")

    def test_mouth_fromxml(self) -> None:
        o: Mouth = self.parseToSavable(
            """<mouth capacity="10.5" depth="2" elasticity="4" lipSize="2" piercedLip="true" plasticity="3" stretchedCapacity="10.5" virgin="false" wetness="3">
    <mouthModifiers/>
</mouth>""",
            Mouth,
        )
        self.assertEqual(o.orifice.capacity, 10.5)
        self.assertEqual(o.orifice.depth, 2)
        self.assertEqual(o.orifice.elasticity, 4)
        self.assertEqual(o.lipSize, 2)
        self.assertEqual(o.piercedLip, True)
        self.assertEqual(o.orifice.plasticity, 3)
        self.assertEqual(o.orifice.stretchedCapacity, 10.5)
        self.assertEqual(o.orifice.orificeVirgin, False)
        self.assertEqual(o.orifice.wetness, 3)
        self.assertSetEqual(self.mouth.orifice.orificeModifiers, {EOrificeModifier.MUSCLE_CONTROL})

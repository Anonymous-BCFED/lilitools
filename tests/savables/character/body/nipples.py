from typing import AbstractSet
from lilitools.saves.character.body.nipples import Nipples
from lilitools.saves.character.enums.orifice_modifier import EOrificeModifier
from lilitools.saves.character.body.enums.areolae_shape import EAreolaeShape
from lilitools.saves.character.body.enums.nipple_shape import ENippleShape
from tests.savables._base_savable_test import BaseSavableTest
class NipplesTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset({"areolaeShape", "areolaeSize", "capacity", "depth", "elasticity", "nippleShape", "nippleSize", "pierced", "plasticity", "stretchedCapacity", "virgin"})
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({"areolaeShape", "areolaeSize", "capacity", "depth", "elasticity", "nippleShape", "nippleSize", "pierced", "plasticity", "stretchedCapacity", "virgin"})
    REQD_CHILDREN: AbstractSet[str] = frozenset({"nippleModifiers"})
    ALLOWED_CHILDREN: AbstractSet[str] = frozenset({"nippleModifiers"})

    def setUp(self) -> None:
        """<nipples areolaeShape="NORMAL" areolaeSize="2" capacity="0.5" depth="2" elasticity="3" nippleShape="NORMAL" nippleSize="3" pierced="true" plasticity="3" stretchedCapacity="0.5" virgin="true">
            <nippleModifiers PUFFY="true"/>
        </nipples>"""
        self.nipples = Nipples()
        self.nipples.areolaeShape=EAreolaeShape.NORMAL
        self.nipples.areolaeSize=2
        self.nipples.orifice.capacity=0.5
        self.nipples.orifice.depth=2
        self.nipples.orifice.elasticity = 3
        self.nipples.nippleShape=ENippleShape.NORMAL
        self.nipples.nippleSize=3
        self.nipples.pierced=True
        self.nipples.orifice.plasticity=3
        self.nipples.orifice.stretchedCapacity=0.5
        self.nipples.orifice.orificeVirgin = True
        self.nipples.orifice.orificeModifiers = {EOrificeModifier.PUFFY}

    def test_nipples_toxml(self) -> None:
        e = self.nipples.toXML()
        self.assertAttrsMustExist(e, self.REQD_ATTRS)
        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertChildrenMustExist(e, self.REQD_CHILDREN)
        self.assertOnlyTheseChildrenMayExist(e, self.ALLOWED_CHILDREN)
        self.assertChildMustOccurOnlyOnce(e, "nippleModifiers")
        self.assertAttrEquals(e, "areolaeShape", "NORMAL")
        self.assertAttrEquals(e, "areolaeSize", "2")
        self.assertAttrEquals(e, "capacity", "0.5")
        self.assertAttrEquals(e, "depth", "2")
        self.assertAttrEquals(e, "elasticity", "3")
        self.assertAttrEquals(e, "nippleShape", "NORMAL")
        self.assertAttrEquals(e, "nippleSize", "3")
        self.assertAttrEquals(e, "pierced", "true")
        self.assertAttrEquals(e, "plasticity", "3")
        self.assertAttrEquals(e, "stretchedCapacity", "0.5")
        self.assertAttrEquals(e, "virgin", "true")
        self.assertIsNotNone((m := e.find("nippleModifiers")))
        self.assertAttrEquals(m, "PUFFY", "true")

    def test_nipples_fromxml(self) -> None:
        o: Nipples = self.parseToSavable(
            """<nipples areolaeShape="NORMAL" areolaeSize="2" capacity="0.5" depth="2" elasticity="3" nippleShape="NORMAL" nippleSize="3" pierced="true" plasticity="3" stretchedCapacity="0.5" virgin="true">
    <nippleModifiers PUFFY="true"/>
</nipples>""",
            Nipples,
        )
        self.assertEqual(o.areolaeShape, EAreolaeShape.NORMAL)
        self.assertEqual(o.areolaeSize, 2)
        self.assertEqual(o.orifice.capacity, 0.5)
        self.assertEqual(o.orifice.depth, 2)
        self.assertEqual(o.orifice.elasticity, 3)
        self.assertEqual(o.nippleShape, ENippleShape.NORMAL)
        self.assertEqual(o.nippleSize, 3)
        self.assertEqual(o.pierced, True)
        self.assertEqual(o.orifice.plasticity, 3)
        self.assertEqual(o.orifice.stretchedCapacity, 0.5)
        self.assertEqual(o.orifice.orificeVirgin, True)
        self.assertSetEqual(self.nipples.orifice.orificeModifiers, {EOrificeModifier.PUFFY})

from typing import AbstractSet
from lilitools.saves.character.body.penis import Penis
from lilitools.saves.character.enums.orifice_modifier import EOrificeModifier
from lilitools.saves.character.enums.penetrator_modifier import EPenetratorModifier
from tests.savables._base_savable_test import BaseSavableTest
class PenisTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset({"capacity", "depth", "elasticity", "girth", "pierced", "plasticity", "size", "stretchedCapacity", "type", "urethraVirgin", "virgin"})
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({"capacity", "depth", "elasticity", "girth", "pierced", "plasticity", "size", "stretchedCapacity", "type", "urethraVirgin", "virgin"})
    REQD_CHILDREN: AbstractSet[str] = frozenset({"penisModifiers", "urethraModifiers"})
    ALLOWED_CHILDREN: AbstractSet[str] = frozenset({"penisModifiers", "urethraModifiers"})

    def setUp(self) -> None:
        """<penis capacity="0.0" depth="2" elasticity="0" girth="5" pierced="false" plasticity="3" size="23" stretchedCapacity="0.0" type="NONE" urethraVirgin="true" virgin="true">
            <penisModifiers BARBED="true" BLUNT="true" FLARED="true" KNOTTED="true" PREHENSILE="true" RIBBED="true" SHEATHED="true" TAPERED="true" TENTACLED="true" VEINY="true"/>
            <urethraModifiers/>
        </penis>"""
        self.penis = Penis()
        self.penis.urethra.capacity=0.0
        self.penis.urethra.depth=2
        self.penis.urethra.elasticity=0
        self.penis.penetrator.girth=5
        self.penis.pierced=False
        self.penis.urethra.plasticity = 3
        self.penis.penetrator.length = 23
        self.penis.urethra.stretchedCapacity = 0.0
        self.penis.type='NONE'
        self.penis.urethra.orificeVirgin = True
        self.penis.penetrator.penetratorVirgin = True
        self.penis.penetrator.penetratorModifiers = {
            EPenetratorModifier.BARBED,
            EPenetratorModifier.BLUNT,
            EPenetratorModifier.FLARED,
            EPenetratorModifier.KNOTTED,
            EPenetratorModifier.PREHENSILE,
            EPenetratorModifier.RIBBED,
            EPenetratorModifier.SHEATHED,
            EPenetratorModifier.TAPERED,
            EPenetratorModifier.TENTACLED,
            EPenetratorModifier.VEINY,
        }

    def test_penis_toxml(self) -> None:
        e = self.penis.toXML()
        self.assertAttrsMustExist(e, self.REQD_ATTRS)
        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertChildrenMustExist(e, self.REQD_CHILDREN)
        self.assertOnlyTheseChildrenMayExist(e, self.ALLOWED_CHILDREN)
        self.assertChildMustOccurOnlyOnce(e, "penisModifiers")
        self.assertChildMustOccurOnlyOnce(e, "urethraModifiers")
        self.assertAttrEquals(e, "capacity", "0.0")
        self.assertAttrEquals(e, "depth", "2")
        self.assertAttrEquals(e, "elasticity", "0")
        self.assertAttrEquals(e, "girth", "5")
        self.assertAttrEquals(e, "pierced", "false")
        self.assertAttrEquals(e, "plasticity", "3")
        self.assertAttrEquals(e, "size", "23")
        self.assertAttrEquals(e, "stretchedCapacity", "0.0")
        self.assertAttrEquals(e, "type", "NONE")
        self.assertAttrEquals(e, "urethraVirgin", "true")
        self.assertAttrEquals(e, "virgin", "true")
        self.assertIsNotNone((m := e.find("penisModifiers")))
        self.assertAttrEquals(m, "BARBED", "true")
        self.assertAttrEquals(m, "BLUNT", "true")
        self.assertAttrEquals(m, "FLARED", "true")
        self.assertAttrEquals(m, "KNOTTED", "true")
        self.assertAttrEquals(m, "PREHENSILE", "true")
        self.assertAttrEquals(m, "RIBBED", "true")
        self.assertAttrEquals(m, "SHEATHED", "true")
        self.assertAttrEquals(m, "TAPERED", "true")
        self.assertAttrEquals(m, "TENTACLED", "true")
        self.assertAttrEquals(m, "VEINY", "true")
        self.assertIsNotNone((m := e.find("urethraModifiers")))

    def test_penis_fromxml(self) -> None:
        o: Penis = self.parseToSavable(
            """<penis capacity="0.0" depth="2" elasticity="0" girth="5" pierced="false" plasticity="3" size="23" stretchedCapacity="0.0" type="NONE" urethraVirgin="true" virgin="true">
    <penisModifiers BARBED="true" BLUNT="true" FLARED="true" KNOTTED="true" PREHENSILE="true" RIBBED="true" SHEATHED="true" TAPERED="true" TENTACLED="true" VEINY="true"/>
    <urethraModifiers/>
</penis>""",
            Penis,
        )
        self.assertEqual(o.urethra.capacity, 0.)
        self.assertEqual(o.urethra.depth, 2)
        self.assertEqual(o.urethra.elasticity, 0)
        self.assertEqual(o.penetrator.girth, 5)
        self.assertEqual(o.pierced, False)
        self.assertEqual(o.urethra.plasticity, 3)
        self.assertEqual(o.penetrator.length, 23)
        self.assertEqual(o.urethra.stretchedCapacity, 0.)
        self.assertEqual(o.type, "NONE")
        self.assertEqual(o.urethra.orificeVirgin, True)
        self.assertEqual(o.penetrator.penetratorVirgin, True)
        self.assertSetEqual(
            self.penis.penetrator.penetratorModifiers,
            {
                EPenetratorModifier.BARBED,
                EPenetratorModifier.BLUNT,
                EPenetratorModifier.FLARED,
                EPenetratorModifier.KNOTTED,
                EPenetratorModifier.PREHENSILE,
                EPenetratorModifier.RIBBED,
                EPenetratorModifier.SHEATHED,
                EPenetratorModifier.TAPERED,
                EPenetratorModifier.TENTACLED,
                EPenetratorModifier.VEINY,
            },
        )

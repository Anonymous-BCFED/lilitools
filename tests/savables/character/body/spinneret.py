from typing import AbstractSet
from lilitools.saves.character.body.spinneret import Spinneret
from tests.savables._base_savable_test import BaseSavableTest


class SpinneretTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset({"capacity", "depth", "elasticity", "plasticity", "stretchedCapacity", "virgin", "wetness"})
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({"capacity", "depth", "elasticity", "plasticity", "stretchedCapacity", "virgin", "wetness"})
    REQD_CHILDREN: AbstractSet[str] = frozenset({"spinneretModifiers"})
    ALLOWED_CHILDREN: AbstractSet[str] = frozenset({"spinneretModifiers"})

    def setUp(self) -> None:
        """<spinneret capacity="1.5" depth="2" elasticity="4" plasticity="3" stretchedCapacity="1.5" virgin="true" wetness="1">
            <spinneretModifiers/>
        </spinneret>"""
        self.spinneret = Spinneret()
        self.spinneret.orifice.capacity = 1.5
        self.spinneret.orifice.depth = 2
        self.spinneret.orifice.elasticity = 4
        self.spinneret.orifice.plasticity = 3
        self.spinneret.orifice.stretchedCapacity = 1.5
        self.spinneret.orifice.orificeVirgin = True
        self.spinneret.orifice.wetness = 1

    def test_spinneret_toxml(self) -> None:
        e = self.spinneret.toXML()
        self.assertAttrsMustExist(e, self.REQD_ATTRS)
        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertChildrenMustExist(e, self.REQD_CHILDREN)
        self.assertOnlyTheseChildrenMayExist(e, self.ALLOWED_CHILDREN)
        self.assertChildMustOccurOnlyOnce(e, "spinneretModifiers")
        self.assertAttrEquals(e, "capacity", "1.5")
        self.assertAttrEquals(e, "depth", "2")
        self.assertAttrEquals(e, "elasticity", "4")
        self.assertAttrEquals(e, "plasticity", "3")
        self.assertAttrEquals(e, "stretchedCapacity", "1.5")
        self.assertAttrEquals(e, "virgin", "true")
        self.assertAttrEquals(e, "wetness", "1")
        self.assertIsNotNone((m := e.find("spinneretModifiers")))

    def test_spinneret_fromxml(self) -> None:
        o: Spinneret = self.parseToSavable(
            """<spinneret capacity="1.5" depth="2" elasticity="4" plasticity="3" stretchedCapacity="1.5" virgin="true" wetness="1">
    <spinneretModifiers/>
</spinneret>""",
            Spinneret,
        )
        self.assertEqual(o.orifice.capacity, 1.5)
        self.assertEqual(o.orifice.depth, 2)
        self.assertEqual(o.orifice.elasticity, 4)
        self.assertEqual(o.orifice.plasticity, 3)
        self.assertEqual(o.orifice.stretchedCapacity, 1.5)
        self.assertEqual(o.orifice.orificeVirgin, True)
        self.assertEqual(o.orifice.wetness, 1)

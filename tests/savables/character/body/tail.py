from typing import AbstractSet
from lilitools.saves.character.body.tail import Tail
from tests.savables._base_savable_test import BaseSavableTest


class TailTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset({"count", "girth", "length", "type"})
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({"count", "girth", "length", "type"})
    REQD_CHILDREN: AbstractSet[str] = frozenset()
    ALLOWED_CHILDREN: AbstractSet[str] = frozenset()

    def setUp(self) -> None:
        """<tail count="1" girth="3" length="0.3" type="HORSE_MORPH"/>"""
        self.tail = Tail()
        self.tail.count = 1
        self.tail.girth = 3
        self.tail.length = 0.3
        self.tail.type = "HORSE_MORPH"

    def test_tail_toxml(self) -> None:
        e = self.tail.toXML()
        self.assertAttrsMustExist(e, self.REQD_ATTRS)
        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertAttrEquals(e, "count", "1")
        self.assertAttrEquals(e, "girth", "3")
        self.assertAttrEquals(e, "length", "0.3")
        self.assertAttrEquals(e, "type", "HORSE_MORPH")

    def test_tail_fromxml(self) -> None:
        o: Tail = self.parseToSavable('<tail count="1" girth="3" length="0.3" type="HORSE_MORPH"/>', Tail)
        self.assertEqual(o.count, 1)
        self.assertEqual(o.girth, 3)
        self.assertEqual(o.length, 0.3)
        self.assertEqual(o.type, "HORSE_MORPH")

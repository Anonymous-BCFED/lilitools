from typing import AbstractSet
from lilitools.saves.character.body.tentacles import Tentacles
from tests.savables._base_savable_test import BaseSavableTest


class TentaclesTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset({"count", "girth", "length", "type"})
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({"count", "girth", "length", "type"})
    REQD_CHILDREN: AbstractSet[str] = frozenset()
    ALLOWED_CHILDREN: AbstractSet[str] = frozenset()

    def setUp(self) -> None:
        """<tentacle count="1" girth="3" length="0.0" type="NONE"/>"""
        self.tentacle = Tentacles()
        self.tentacle.count = 1
        self.tentacle.girth = 3
        self.tentacle.length = 0.0
        self.tentacle.type = "NONE"

    def test_tentacle_toxml(self) -> None:
        e = self.tentacle.toXML()
        self.assertAttrsMustExist(e, self.REQD_ATTRS)
        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertAttrEquals(e, "count", "1")
        self.assertAttrEquals(e, "girth", "3")
        self.assertAttrEquals(e, "length", "0.0")
        self.assertAttrEquals(e, "type", "NONE")

    def test_tentacle_fromxml(self) -> None:
        o: Tentacles = self.parseToSavable('<tentacle count="1" girth="3" length="0.0" type="NONE"/>', Tentacles)
        self.assertEqual(o.count, 1)
        self.assertEqual(o.girth, 3)
        self.assertEqual(o.length, 0.0)
        self.assertEqual(o.type, "NONE")

from typing import AbstractSet
from lilitools.saves.character.body.testicles import Testicles
from tests.savables._base_savable_test import BaseSavableTest


class TesticlesTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset({"cumExpulsion", "cumRegeneration", "cumStorage", "internal", "numberOfTesticles", "storedCum", "testicleSize"})
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({"cumExpulsion", "cumRegeneration", "cumStorage", "internal", "numberOfTesticles", "storedCum", "testicleSize"})
    REQD_CHILDREN: AbstractSet[str] = frozenset()
    ALLOWED_CHILDREN: AbstractSet[str] = frozenset()

    def setUp(self) -> None:
        """<testicles cumExpulsion="60" cumRegeneration="10000" cumStorage="65" internal="false" numberOfTesticles="2" storedCum="65.0" testicleSize="4"/>"""
        self.testicles = Testicles()
        self.testicles.cumExpulsion = 60
        self.testicles.cumRegeneration = 10000
        self.testicles.cumStorage = 65
        self.testicles.internal = False
        self.testicles.numberOfTesticles = 2
        self.testicles.storedCum = 65.0
        self.testicles.testicleSize = 4

    def test_testicles_toxml(self) -> None:
        e = self.testicles.toXML()
        self.assertAttrsMustExist(e, self.REQD_ATTRS)
        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertAttrEquals(e, "cumExpulsion", "60")
        self.assertAttrEquals(e, "cumRegeneration", "10000")
        self.assertAttrEquals(e, "cumStorage", "65")
        self.assertAttrEquals(e, "internal", "false")
        self.assertAttrEquals(e, "numberOfTesticles", "2")
        self.assertAttrEquals(e, "storedCum", "65.0")
        self.assertAttrEquals(e, "testicleSize", "4")

    def test_testicles_fromxml(self) -> None:
        o: Testicles = self.parseToSavable('<testicles cumExpulsion="60" cumRegeneration="10000" cumStorage="65" internal="false" numberOfTesticles="2" storedCum="65.0" testicleSize="4"/>', Testicles)
        self.assertEqual(o.cumExpulsion, 60)
        self.assertEqual(o.cumRegeneration, 10000)
        self.assertEqual(o.cumStorage, 65)
        self.assertEqual(o.internal, False)
        self.assertEqual(o.numberOfTesticles, 2)
        self.assertEqual(o.storedCum, 65.0)
        self.assertEqual(o.testicleSize, 4)

from typing import AbstractSet
from lilitools.saves.character.body.tongue import Tongue
from lilitools.saves.character.enums.penetrator_modifier import EPenetratorModifier
from lilitools.saves.character.body.enums.tongue_modifier import ETongueModifier
from tests.savables._base_savable_test import BaseSavableTest
class TongueTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset({"piercedTongue", "tongueLength"})
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({"piercedTongue", "tongueLength"})
    REQD_CHILDREN: AbstractSet[str] = frozenset({"tongueModifiers"})
    ALLOWED_CHILDREN: AbstractSet[str] = frozenset({"tongueModifiers"})

    def setUp(self) -> None:
        """<tongue piercedTongue="true" tongueLength="2">
            <tongueModifiers FLAT="true" STRONG="true" TAPERED="true" WIDE="true"/>
        </tongue>"""
        self.tongue = Tongue()
        self.tongue.pierced = True
        self.tongue.length = 2
        self.tongue.modifiers = {ETongueModifier.FLAT, ETongueModifier.STRONG, ETongueModifier.TAPERED, ETongueModifier.WIDE}


    def test_tongue_toxml(self) -> None:
        e = self.tongue.toXML()
        self.assertAttrsMustExist(e, self.REQD_ATTRS)
        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertChildrenMustExist(e, self.REQD_CHILDREN)
        self.assertOnlyTheseChildrenMayExist(e, self.ALLOWED_CHILDREN)
        self.assertChildMustOccurOnlyOnce(e, "tongueModifiers")
        self.assertAttrEquals(e, "piercedTongue", "true")
        self.assertAttrEquals(e, "tongueLength", "2")
        self.assertIsNotNone((m := e.find("tongueModifiers")))
        self.assertAttrEquals(m, "FLAT", "true")
        self.assertAttrEquals(m, "STRONG", "true")
        self.assertAttrEquals(m, "TAPERED", "true")
        self.assertAttrEquals(m, "WIDE", "true")

    def test_tongue_fromxml(self) -> None:
        o: Tongue = self.parseToSavable(
            """<tongue piercedTongue="true" tongueLength="2">
    <tongueModifiers FLAT="true" STRONG="true" TAPERED="true" WIDE="true"/>
</tongue>""",
            Tongue,
        )
        self.assertEqual(o.pierced, True)
        self.assertEqual(o.length, 2)
        self.assertSetEqual(o.modifiers, {ETongueModifier.FLAT, ETongueModifier.STRONG, ETongueModifier.TAPERED, ETongueModifier.WIDE})

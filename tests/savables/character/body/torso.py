from typing import AbstractSet
from lilitools.saves.character.body.torso import Torso
from tests.savables._base_savable_test import BaseSavableTest


class TorsoTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset({"type"})
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({"type"})
    REQD_CHILDREN: AbstractSet[str] = frozenset()
    ALLOWED_CHILDREN: AbstractSet[str] = frozenset()

    def setUp(self) -> None:
        """<torso type="HUMAN"/>"""
        self.torso = Torso()
        self.torso.type = "HUMAN"

    def test_torso_toxml(self) -> None:
        e = self.torso.toXML()
        self.assertAttrsMustExist(e, self.REQD_ATTRS)
        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertAttrEquals(e, "type", "HUMAN")

    def test_torso_fromxml(self) -> None:
        o: Torso = self.parseToSavable('<torso type="HUMAN"/>', Torso)
        self.assertEqual(o.type, "HUMAN")

from typing import AbstractSet
from lilitools.saves.character.body.vagina import Vagina
from lilitools.saves.character.enums.orifice_modifier import EOrificeModifier
from tests.savables._base_savable_test import BaseSavableTest


class VaginaTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset(
        {
            "capacity",
            "clitGirth",
            "clitSize",
            "depth",
            "eggLayer",
            "elasticity",
            "hymen",
            "labiaSize",
            "pierced",
            "plasticity",
            "squirter",
            "stretchedCapacity",
            "type",
            "urethraCapacity",
            "urethraDepth",
            "urethraElasticity",
            "urethraPlasticity",
            "urethraStretchedCapacity",
            "urethraVirgin",
            "virgin",
            "wetness",
        }
    )
    ALLOWED_ATTRS: AbstractSet[str] = frozenset(
        {
            "capacity",
            "clitGirth",
            "clitSize",
            "depth",
            "eggLayer",
            "elasticity",
            "hymen",
            "labiaSize",
            "pierced",
            "plasticity",
            "squirter",
            "stretchedCapacity",
            "type",
            "urethraCapacity",
            "urethraDepth",
            "urethraElasticity",
            "urethraPlasticity",
            "urethraStretchedCapacity",
            "urethraVirgin",
            "virgin",
            "wetness",
        }
    )
    REQD_CHILDREN: AbstractSet[str] = frozenset({"clitModifiers", "urethraModifiers", "vaginaModifiers"})
    ALLOWED_CHILDREN: AbstractSet[str] = frozenset({"clitModifiers", "urethraModifiers", "vaginaModifiers"})

    def setUp(self) -> None:
        """<vagina capacity="10.5" clitGirth="3" clitSize="0" depth="3" eggLayer="false" elasticity="5" hymen="false" labiaSize="4" pierced="true" plasticity="4" squirter="true" stretchedCapacity="10.5" type="HUMAN" urethraCapacity="0.0" urethraDepth="2" urethraElasticity="0" urethraPlasticity="3" urethraStretchedCapacity="0.0" urethraVirgin="true" virgin="false" wetness="4">
            <clitModifiers/>
            <vaginaModifiers MUSCLE_CONTROL="true" PUFFY="true" RIBBED="true" TENTACLED="true"/>
            <urethraModifiers/>
        </vagina>"""
        self.vagina = Vagina()
        self.vagina.orifice.capacity = 10.5
        self.vagina.clit.girth = 3
        self.vagina.clit.length = 0
        self.vagina.orifice.depth = 3
        self.vagina.eggLayer = False
        self.vagina.orifice.elasticity = 5
        self.vagina.hymen = False
        self.vagina.labiaSize = 4
        self.vagina.pierced = True
        self.vagina.orifice.plasticity = 4
        self.vagina.squirter = True
        self.vagina.orifice.stretchedCapacity = 10.5
        self.vagina.type = "HUMAN"
        self.vagina.urethra.capacity = 0.0
        self.vagina.urethra.depth = 2
        self.vagina.urethra.elasticity = 0
        self.vagina.urethra.plasticity = 3
        self.vagina.urethra.stretchedCapacity = 0.0
        self.vagina.urethra.orificeVirgin = True
        self.vagina.orifice.orificeVirgin = False
        self.vagina.orifice.wetness = 4
        self.vagina.orifice.orificeModifiers = {
            EOrificeModifier.MUSCLE_CONTROL,
            EOrificeModifier.PUFFY,
            EOrificeModifier.RIBBED,
            EOrificeModifier.TENTACLED,
        }

    def test_vagina_toxml(self) -> None:
        e = self.vagina.toXML()
        self.assertAttrsMustExist(e, self.REQD_ATTRS)
        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertChildrenMustExist(e, self.REQD_CHILDREN)
        self.assertOnlyTheseChildrenMayExist(e, self.ALLOWED_CHILDREN)
        self.assertChildMustOccurOnlyOnce(e, "vaginaModifiers")
        self.assertChildMustOccurOnlyOnce(e, "clitModifiers")
        self.assertChildMustOccurOnlyOnce(e, "urethraModifiers")
        self.assertAttrEquals(e, "capacity", "10.5")
        self.assertAttrEquals(e, "clitGirth", "3")
        self.assertAttrEquals(e, "clitSize", "0")
        self.assertAttrEquals(e, "depth", "3")
        self.assertAttrEquals(e, "eggLayer", "false")
        self.assertAttrEquals(e, "elasticity", "5")
        self.assertAttrEquals(e, "hymen", "false")
        self.assertAttrEquals(e, "labiaSize", "4")
        self.assertAttrEquals(e, "pierced", "true")
        self.assertAttrEquals(e, "plasticity", "4")
        self.assertAttrEquals(e, "squirter", "true")
        self.assertAttrEquals(e, "stretchedCapacity", "10.5")
        self.assertAttrEquals(e, "type", "HUMAN")
        self.assertAttrEquals(e, "urethraCapacity", "0.0")
        self.assertAttrEquals(e, "urethraDepth", "2")
        self.assertAttrEquals(e, "urethraElasticity", "0")
        self.assertAttrEquals(e, "urethraPlasticity", "3")
        self.assertAttrEquals(e, "urethraStretchedCapacity", "0.0")
        self.assertAttrEquals(e, "urethraVirgin", "true")
        self.assertAttrEquals(e, "virgin", "false")
        self.assertAttrEquals(e, "wetness", "4")
        self.assertIsNotNone((m := e.find("clitModifiers")))
        self.assertIsNotNone((m := e.find("vaginaModifiers")))
        self.assertAttrEquals(m, "MUSCLE_CONTROL", "true")
        self.assertAttrEquals(m, "PUFFY", "true")
        self.assertAttrEquals(m, "RIBBED", "true")
        self.assertAttrEquals(m, "TENTACLED", "true")
        self.assertIsNotNone((m := e.find("urethraModifiers")))

    def test_vagina_fromxml(self) -> None:
        o: Vagina = self.parseToSavable(
            """<vagina capacity="10.5" clitGirth="3" clitSize="0" depth="3" eggLayer="false" elasticity="5" hymen="false" labiaSize="4" pierced="true" plasticity="4" squirter="true" stretchedCapacity="10.5" type="HUMAN" urethraCapacity="0.0" urethraDepth="2" urethraElasticity="0" urethraPlasticity="3" urethraStretchedCapacity="0.0" urethraVirgin="true" virgin="false" wetness="4">
    <clitModifiers/>
    <vaginaModifiers MUSCLE_CONTROL="true" PUFFY="true" RIBBED="true" TENTACLED="true"/>
    <urethraModifiers/>
</vagina>""",
            Vagina,
        )
        self.assertEqual(o.orifice.capacity, 10.5)
        self.assertEqual(o.clit.girth, 3)
        self.assertEqual(o.clit.length, 0)
        self.assertEqual(o.orifice.depth, 3)
        self.assertEqual(o.eggLayer, False)
        self.assertEqual(o.orifice.elasticity, 5)
        self.assertEqual(o.hymen, False)
        self.assertEqual(o.labiaSize, 4)
        self.assertEqual(o.pierced, True)
        self.assertEqual(o.orifice.plasticity, 4)
        self.assertEqual(o.squirter, True)
        self.assertEqual(o.orifice.stretchedCapacity, 10.5)
        self.assertEqual(o.type, "HUMAN")
        self.assertEqual(o.urethra.capacity, 0.0)
        self.assertEqual(o.urethra.depth, 2)
        self.assertEqual(o.urethra.elasticity, 0)
        self.assertEqual(o.urethra.plasticity, 3)
        self.assertEqual(o.urethra.stretchedCapacity, 0.0)
        self.assertEqual(o.urethra.orificeVirgin, True)
        self.assertEqual(o.orifice.orificeVirgin, False)
        self.assertEqual(o.orifice.wetness, 4)
        self.assertSetEqual(
            self.vagina.orifice.orificeModifiers,
            {
                EOrificeModifier.MUSCLE_CONTROL,
                EOrificeModifier.PUFFY,
                EOrificeModifier.RIBBED,
                EOrificeModifier.TENTACLED,
            },
        )

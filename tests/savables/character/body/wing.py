from typing import AbstractSet
from lilitools.saves.character.body.wings import Wings
from tests.savables._base_savable_test import BaseSavableTest


class WingsTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset({"size", "type"})
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({"size", "type"})
    REQD_CHILDREN: AbstractSet[str] = frozenset()
    ALLOWED_CHILDREN: AbstractSet[str] = frozenset()

    def setUp(self) -> None:
        """<wing size="0" type="NONE"/>"""
        self.wing = Wings()
        self.wing.size = 0
        self.wing.type = "NONE"

    def test_wing_toxml(self) -> None:
        e = self.wing.toXML()
        self.assertAttrsMustExist(e, self.REQD_ATTRS)
        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertAttrEquals(e, "size", "0")
        self.assertAttrEquals(e, "type", "NONE")

    def test_wing_fromxml(self) -> None:
        o: Wings = self.parseToSavable('<wing size="0" type="NONE"/>', Wings)
        self.assertEqual(o.size, 0)
        self.assertEqual(o.type, "NONE")

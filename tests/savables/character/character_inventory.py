from pathlib import Path
from typing import AbstractSet

from lxml import etree

from lilitools.saves.inventory.character_inventory import CharacterInventory
from tests.savables._base_savable_test import BaseSavableTest


class CharacterInventoryTests(BaseSavableTest):
    ALLOWED_ATTRS: AbstractSet[str] = frozenset()
    REQD_ATTRS: AbstractSet[str] = frozenset()

    TEST_DATA_FILE = Path('tests') / 'data' / 'character' / 'character_inventory' / 'full.xml'

    def test_characterinventory_can_deserialize(self) -> None:
        ci: CharacterInventory = self.parseFileToSavable(self.TEST_DATA_FILE, CharacterInventory)

        self.assertEqual(len(ci.clothingEquipped), 7)
        self.assertEqual(len(ci.clothingInInventory), 0)
        self.assertEqual(len(ci.dirtySlots), 0)
        self.assertEqual(len(ci.itemsInInventory), 1)
        self.assertEqual(len(ci.mainWeapons), 1)
        self.assertEqual(len(ci.offhandWeapons), 0)
        self.assertEqual(len(ci.unlockKeyMap), 0)
        self.assertEqual(len(ci.weaponsInInventory), 0)
        self.assertEqual(ci.essenceCount, 0)
        self.assertEqual(ci.money, 5000)
        self.assertEqual(ci.maxInventorySpace, 150)


from typing import AbstractSet

from lilitools.saves.core_info import CoreInfo
from lilitools.saves.date import Date
from lilitools.saves.enums.weather import EWeather
from tests.savables._base_savable_test import BaseSavableTest


class CoreInfoTests(BaseSavableTest):
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({
        'id',
        'gatheringStormDurationInSeconds',
        'lastAutoSaveTime',
        'nextStormTimeInSeconds',
        'secondsPassed',
        'version',
        'weather',
        'weatherTimeRemainingInSeconds',
    })
    REQD_ATTRS: AbstractSet[str] = frozenset({
        'id',
        'gatheringStormDurationInSeconds',
        'lastAutoSaveTime',
        'nextStormTimeInSeconds',
        'secondsPassed',
        'version',
        'weather',
        'weatherTimeRemainingInSeconds',
    })

    def test_coreinfo_fromxml(self) -> None:
        input = '''<coreInfo gatheringStormDurationInSeconds="0" id="1685621686" lastAutoSaveTime="74850" nextStormTimeInSeconds="297240" secondsPassed="78450" version="0.4.8.7" weather="MAGIC_STORM" weatherTimeRemainingInSeconds="14400">
    <date dayOfMonth="9" hour="0" minute="0" month="10" year="2026"/>
  </coreInfo>'''
        ci: CoreInfo = self.parseToSavable(input, CoreInfo)

        self.assertEqual(1685621686, ci.id)
        self.assertEqual(0, ci.gatheringStormDurationInSeconds)
        self.assertEqual(74850, ci.lastAutoSaveTime)
        self.assertEqual(297240, ci.nextStormTimeInSeconds)
        self.assertEqual(78450, ci.secondsPassed)
        self.assertEqual("0.4.8.7", ci.version)
        self.assertEqual(EWeather.MAGIC_STORM, ci.weather)
        self.assertEqual(14400, ci.weatherTimeRemainingInSeconds)

        self.assertIsInstance(ci.date, Date)

    def test_coreinfo_toxml(self) -> None:
        ci = CoreInfo()
        ci.id = 1685621686
        ci.gatheringStormDurationInSeconds = 21480
        ci.lastAutoSaveTime = 7648611
        ci.nextStormTimeInSeconds = 7614775
        ci.secondsPassed = 7649531
        ci.version = '0.4.2.3'
        ci.weather = EWeather.MAGIC_STORM
        ci.weatherTimeRemainingInSeconds = 20810

        e = ci.toXML()

        self.assertChildMustOccurOnlyOnce(e, 'date')

        self.assertAttrEquals(e, 'id', '1685621686')
        self.assertAttrEquals(e, 'gatheringStormDurationInSeconds', '21480')
        self.assertAttrEquals(e, 'lastAutoSaveTime', '7648611')
        self.assertAttrEquals(e, 'nextStormTimeInSeconds', '7614775')
        self.assertAttrEquals(e, 'secondsPassed', '7649531')
        self.assertAttrEquals(e, 'version', '0.4.2.3')
        self.assertAttrEquals(e, 'weather', 'MAGIC_STORM')
        self.assertAttrEquals(e, 'weatherTimeRemainingInSeconds', '20810')

        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertAttrsMustExist(e, self.REQD_ATTRS)

        self.assertChildMustOccurOnlyOnce(e, 'date')
        self.assertOnlyTheseChildrenMayExist(e, {'date'})

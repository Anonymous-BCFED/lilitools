from typing import AbstractSet

from lilitools.saves.date import Date

from ._base_savable_test import BaseSavableTest


class DateTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset({
        'dayOfMonth',
        'hour',
        'minute',
        'month',
        'year',
    })
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({
        'dayOfMonth',
        'hour',
        'minute',
        'month',
        'year',
    })

    def test_date_fromxml(self) -> None:
        xml = '''<date dayOfMonth="2" hour="0" minute="0" month="10" year="2024"/>'''
        date: Date = self.parseToSavable(xml, Date)

        self.assertEqual(date.datetime.day, 2)
        self.assertEqual(date.datetime.hour, 0)
        self.assertEqual(date.datetime.minute, 0)
        self.assertEqual(date.datetime.month, 10)
        self.assertEqual(date.datetime.year, 2024)

    def test_date_toxml(self) -> None:
        date = Date(2024, 2, 10, 0, 0)

        e = date.toXML('date')

        self.assertAttrEquals(e, 'dayOfMonth', '2')
        self.assertAttrEquals(e, 'hour', '0')
        self.assertAttrEquals(e, 'minute', '0')
        self.assertAttrEquals(e, 'month', '10')
        self.assertAttrEquals(e, 'year', '2024')
        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertAttrsMustExist(e, self.REQD_ATTRS)
        self.assertNoChildren(e)

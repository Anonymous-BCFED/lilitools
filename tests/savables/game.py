from typing import AbstractSet, FrozenSet

from lilitools.saves.character.npc import NPC
from lilitools.saves.game import Game

from ._base_savable_test import BaseSavableTest


class GameTests(BaseSavableTest):
    ALLOWED_CHILDREN: FrozenSet[str] = frozenset({
        'coreInfo',
        'savedInventories',
        'savedEnforcers',
        'slavery',
        'dialogueFlags',
        'eventLog',
        'slaveryEventLog',
        'maps',
        'playerCharacter',
        'NPC',
        'OffspringSeed',
    })
    REQD_CHILDREN: FrozenSet[str] = frozenset({
        'coreInfo',
        'savedInventories',
        'savedEnforcers',
        'slavery',
        'dialogueFlags',
        'eventLog',
        'slaveryEventLog',
        'maps',
        'playerCharacter',
        'NPC',
    })
    def test_game_toxml(self) -> None:
        g = Game()
        npc = NPC()
        npc.core.id = '-1,Clown'
        npc.core.name.androgynous = 'Clown'
        npc.core.name.feminine = 'Clown'
        npc.core.name.masculine = 'Clown'
        g.addNPC(npc)
        e = g.toXML()

        self.assertNoAttrs(e)

        self.assertChildMustOccurOnlyOnce(e, 'coreInfo')
        self.assertChildMustOccurOnlyOnce(e, 'savedInventories')
        self.assertChildMustOccurOnlyOnce(e, 'savedEnforcers')
        self.assertChildMustOccurOnlyOnce(e, 'slavery')
        self.assertChildMustOccurOnlyOnce(e, 'dialogueFlags')
        self.assertChildMustOccurOnlyOnce(e, 'eventLog')
        self.assertChildMustOccurOnlyOnce(e, 'slaveryEventLog')
        self.assertChildMustOccurOnlyOnce(e, 'maps')
        self.assertChildMustOccurOnlyOnce(e, 'playerCharacter')
        self.assertChildMustOccurAtLeastOnce(e, 'NPC')
        self.assertOnlyTheseChildrenMayExist(e, self.ALLOWED_CHILDREN)
        self.assertChildrenMustExist(e, self.REQD_CHILDREN)

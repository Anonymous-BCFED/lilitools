from typing import AbstractSet, cast

from lilitools.saves.character.fluids.cum import Cum
from lilitools.saves.items.item_effect import ItemEffect
from lilitools.saves.items.itemtypes.used_condom import UsedCondomItemType

from ..._base_savable_test import BaseSavableTest

DATA: str = '<item colour="CLOTHING_RED" count="1" cumProvider="PlayerCharacter" id="CONDOM_USED" millilitresStored="6">'\
    + '<itemEffects>'\
    + '<effect limit="0" mod1="null" mod2="null" potency="null" timer="0" type="USED_CONDOM_DRINK"/>'\
    + '</itemEffects>'\
    + '<cum>'\
    + '<cum flavour="CUM" type="CUM_HUMAN">'\
    + '<cumModifiers SLIMY="true" STICKY="true"/>'\
    + '</cum>'\
    + '</cum>'\
    + '</item>'


class UsedCondomTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset({
        'colour',
        'cumProvider',
        'id',
        'millilitresStored',
    })
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({
        'colour',
        'cumProvider',
        'id',
        'millilitresStored',
        'count'
    })

    def test_usedcondom_fromxml(self) -> None:
        condom: UsedCondomItemType = cast(UsedCondomItemType, self.parseToSavable(DATA, UsedCondomItemType))

        self.assertEqual(condom.colour, 'CLOTHING_RED')
        self.assertEqual(condom.cumProvider, 'PlayerCharacter')
        self.assertEqual(condom.id, 'CONDOM_USED')
        self.assertEqual(condom.millilitresStored, 6)
        self.assertEqual(len(condom.itemEffects), 1)
        self.assertEqual(condom.itemEffects[0].type, 'USED_CONDOM_DRINK')
        self.assertEqual(len(condom.cum), 1)
        self.assertEqual(condom.cum[0].type, 'CUM_HUMAN')

    def test_usedcondom_toxml(self) -> None:
        c = UsedCondomItemType()
        c.id = 'CONDOM_USED'
        c.colour = 'CLOTHING_RED'
        c.cumProvider = 'PlayerCharacter'
        c.millilitresStored = 6
        c.cum.append(Cum())
        c.itemEffects.append(ItemEffect(type='USED_CONDOM_DRINK'))

        x = c.toXML()

        self.assertOnlyTheseAttrsMayExist(x, self.ALLOWED_ATTRS)
        self.assertAttrsMustExist(x, self.REQD_ATTRS)
        self.assertAttrEquals(x, 'colour', 'CLOTHING_RED')
        self.assertAttrEquals(x, 'cumProvider', 'PlayerCharacter')
        self.assertAttrEquals(x, 'id', 'CONDOM_USED')
        self.assertAttrEquals(x, 'millilitresStored', '6')
        self.assertChildMustOccurOnlyOnce(x, 'itemEffects')
        self.assertChildMustOccurOnlyOnce(x, 'cum')

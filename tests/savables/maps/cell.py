from typing import AbstractSet
from lilitools.saves.world.cell import Cell
from tests.savables._base_savable_test import BaseSavableTest
class CellTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset({"discovered", "travelledTo"})
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({"discovered", "travelledTo"})
    REQD_CHILDREN: AbstractSet[str] = frozenset()
    ALLOWED_CHILDREN: AbstractSet[str] = frozenset({"characterInventory", "location", "place"})

    def setUp(self) -> None:
        """<cell discovered="true" travelledTo="true">
            <location x="2" y="0"/>
            <place type="DADDY_APARTMENT_BEDROOM"/>
        </cell>"""
        self.cell = Cell()
        self.cell.discovered = True
        self.cell.travelledTo = True

    def test_cell_toxml(self) -> None:
        e = self.cell.toXML()
        self.assertAttrsMustExist(e, self.REQD_ATTRS)
        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertOnlyTheseChildrenMayExist(e, self.ALLOWED_CHILDREN)
        #self.assertChildMustOccurOnlyOnce(e, "characterInventory")
        self.assertChildMustOccurOnlyOnce(e, "location")
        self.assertChildMustOccurOnlyOnce(e, "place")
        self.assertAttrEquals(e, "discovered", "true")
        self.assertAttrEquals(e, "travelledTo", "true")

    def test_cell_fromxml(self) -> None:
        o: Cell = self.parseToSavable(
            """<cell discovered="true" travelledTo="true">
    <location x="2" y="0"/>
    <place type="DADDY_APARTMENT_BEDROOM"/>
</cell>""",
            Cell,
        )
        self.assertEqual(o.discovered, True)
        self.assertEqual(o.travelledTo, True)

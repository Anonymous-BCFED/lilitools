from typing import AbstractSet
from lilitools.saves.world.world_controller import WorldController
from lilitools.saves.world.world import World
from tests.savables._base_savable_test import BaseSavableTest


class WorldControllerTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset()
    ALLOWED_ATTRS: AbstractSet[str] = frozenset()
    REQD_CHILDREN: AbstractSet[str] = frozenset({"world"})
    ALLOWED_CHILDREN: AbstractSet[str] = frozenset({"world"})

    def setUp(self) -> None:
        """<maps>
            <world height="2" width="3" worldType="DADDYS_APARTMENT">
                <grid>
                    <cell discovered="true" travelledTo="true">
                        <location x="0" y="0"/>
                        <place type="DADDY_APARTMENT_ENTRANCE"/>
                    </cell>
                    <cell discovered="true" travelledTo="true">
                        <location x="1" y="0"/>
                        <place type="DADDY_APARTMENT_LOUNGE"/>
                    </cell>
                    <cell discovered="true" travelledTo="true">
                        <location x="1" y="1"/>
                        <place type="DADDY_APARTMENT_KITCHEN"/>
                    </cell>
                    <cell discovered="true" travelledTo="true">
                        <location x="2" y="0"/>
                        <place type="DADDY_APARTMENT_BEDROOM"/>
                    </cell>
                </grid>
            </world>
        </maps>"""
        self.maps = WorldController()
        w = World()
        w.type = 'DADDYS_APARTMENT'
        w.width = 3
        w.height = 2
        w.clear()
        c = w.get(0, 0)
        c.discovered = True
        c.travelledTo = True
        c.place.type = 'DADDY_APARTMENT_ENTRANCE'
        c = w.get(1, 0)
        c.discovered = True
        c.travelledTo = True
        c.place.type = 'DADDY_APARTMENT_LOUNGE'
        c = w.get(1, 1)
        c.discovered = True
        c.travelledTo = True
        c.place.type = 'DADDY_APARTMENT_KITCHEN'
        c = w.get(2, 0)
        c.discovered = True
        c.travelledTo = True
        c.place.type = 'DADDY_APARTMENT_BEDROOM'
        self.maps.worlds['DADDYS_APARTMENT'] = w

    def test_maps_toxml(self) -> None:
        e = self.maps.toXML()
        self.assertChildrenMustExist(e, self.REQD_CHILDREN)
        self.assertOnlyTheseChildrenMayExist(e, self.ALLOWED_CHILDREN)
        self.assertChildMustOccurAtLeastOnce(e, "world")

    def test_maps_fromxml(self) -> None:
        o: WorldController = self.parseToSavable(
            """<maps>
    <world height="2" width="3" worldType="DADDYS_APARTMENT">
        <grid>
            <cell discovered="true" travelledTo="true">
                <location x="0" y="0"/>
                <place type="DADDY_APARTMENT_ENTRANCE"/>
            </cell>
            <cell discovered="true" travelledTo="true">
                <location x="1" y="0"/>
                <place type="DADDY_APARTMENT_LOUNGE"/>
            </cell>
            <cell discovered="true" travelledTo="true">
                <location x="1" y="1"/>
                <place type="DADDY_APARTMENT_KITCHEN"/>
            </cell>
            <cell discovered="true" travelledTo="true">
                <location x="2" y="0"/>
                <place type="DADDY_APARTMENT_BEDROOM"/>
            </cell>
        </grid>
    </world>
</maps>""",
            WorldController,
        )
        self.assertEqual(len(o.worlds), 1)

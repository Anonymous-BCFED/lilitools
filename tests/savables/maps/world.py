from typing import AbstractSet
from lilitools.saves.world.world import World
from tests.savables._base_savable_test import BaseSavableTest
class WorldTests(BaseSavableTest):
    REQD_ATTRS: AbstractSet[str] = frozenset({"height", "width", "worldType"})
    ALLOWED_ATTRS: AbstractSet[str] = frozenset({"height", "width", "worldType"})
    REQD_CHILDREN: AbstractSet[str] = frozenset({'grid'})
    ALLOWED_CHILDREN: AbstractSet[str] = frozenset({"grid"})

    def setUp(self) -> None:
        """<world height="2" width="3" worldType="DADDYS_APARTMENT">
            <grid>
                <cell discovered="true" travelledTo="true">
                    <location x="0" y="0"/>
                    <place type="DADDY_APARTMENT_ENTRANCE"/>
                </cell>
                <cell discovered="true" travelledTo="true">
                    <location x="1" y="0"/>
                    <place type="DADDY_APARTMENT_LOUNGE"/>
                </cell>
                <cell discovered="true" travelledTo="true">
                    <location x="1" y="1"/>
                    <place type="DADDY_APARTMENT_KITCHEN"/>
                </cell>
                <cell discovered="true" travelledTo="true">
                    <location x="2" y="0"/>
                    <place type="DADDY_APARTMENT_BEDROOM"/>
                </cell>
            </grid>
        </world>"""
        self.world = World()
        self.world.height = 2
        self.world.width = 3
        self.world.type = "DADDYS_APARTMENT"

    def test_world_toxml(self) -> None:
        e = self.world.toXML()
        self.assertAttrsMustExist(e, self.REQD_ATTRS)
        self.assertOnlyTheseAttrsMayExist(e, self.ALLOWED_ATTRS)
        self.assertOnlyTheseChildrenMayExist(e, self.ALLOWED_CHILDREN)
        self.assertChildMustOccurOnlyOnce(e, "grid")
        self.assertAttrEquals(e, "height", "2")
        self.assertAttrEquals(e, "width", "3")
        self.assertAttrEquals(e, "worldType", "DADDYS_APARTMENT")

    def test_world_fromxml(self) -> None:
        o: World = self.parseToSavable(
            """<world height="2" width="3" worldType="DADDYS_APARTMENT">
    <grid>
        <cell discovered="true" travelledTo="true">
            <location x="0" y="0"/>
            <place type="DADDY_APARTMENT_ENTRANCE"/>
        </cell>
        <cell discovered="true" travelledTo="true">
            <location x="1" y="0"/>
            <place type="DADDY_APARTMENT_LOUNGE"/>
        </cell>
        <cell discovered="true" travelledTo="true">
            <location x="1" y="1"/>
            <place type="DADDY_APARTMENT_KITCHEN"/>
        </cell>
        <cell discovered="true" travelledTo="true">
            <location x="2" y="0"/>
            <place type="DADDY_APARTMENT_BEDROOM"/>
        </cell>
    </grid>
</world>""",
            World,
        )
        self.assertEqual(o.height, 2)
        self.assertEqual(o.width, 3)
        self.assertEqual(o.type, "DADDYS_APARTMENT")

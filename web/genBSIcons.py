import os
from pathlib import Path
from typing import Dict, List, Tuple
import human_readable

BSICON_PATH = Path('node_modules') / 'bootstrap-icons'
BSICONS_PATH = Path('node_modules') / 'bootstrap-icons' / 'icons'
COFFEE_PATH = Path('src') / 'coffee' / '_GEN_bsicons.coffee'
TYPESCRIPT_PATH = Path('src') / 'typescript' / '_GEN_bsicons.ts'

files: List[Tuple[str, str, str]] = []
#oid = 0
path: Path
written = set()
n = 0
for path in BSICONS_PATH.glob('*.svg'):
    #oid = f'bsIcon{oid:04X}'
    oid = 'BS_ICON_' + path.stem.upper().replace('-', '_')
    files += [(path.stem, '' + str(path.relative_to(Path('node_modules'))), oid)]
    #oid += 1
    n += 1
print(f'Found {n:,} icons')


# with COFFEE_PATH.open('w') as f:
#     f.write('# @GENERATED\n')
#     for stem, path, oid in sorted(files, key=lambda x: x[0]):
#         f.write(f'import {oid} from {path!r}\n')
#     f.write('export BOOTSTRAP_ICONS =\n')
#     for stem, path, oid in sorted(files, key=lambda x: x[0]):
#         f.write(f'    "{stem}": {oid}\n')
#     f.write("export {\n")
#     for stem, path, oid in sorted(files, key=lambda x: x[0]):
#         f.write(f'    {oid},\n')
#     f.write('}\n')
# print(f'Emitted {human_readable.file_size(os.path.getsize(COFFEE_PATH))} to {COFFEE_PATH}')


with TYPESCRIPT_PATH.open('w') as f:
    f.write('// @GENERATED\n')
    for stem, path, oid in sorted(files, key=lambda x: x[0]):
        f.write(f'import {oid} from {path!r}\n')
    f.write('export const BOOTSTRAP_ICONS = {\n')
    for stem, path, oid in sorted(files, key=lambda x: x[0]):
        f.write(f'    "{stem}": {oid},\n')
    f.write('}\n')
    f.write("export {\n")
    for stem, path, oid in sorted(files, key=lambda x: x[0]):
        f.write(f'    {oid},\n')
    f.write('}\n')
print(f'Emitted {human_readable.file_size(os.path.getsize(TYPESCRIPT_PATH))} to {TYPESCRIPT_PATH}')

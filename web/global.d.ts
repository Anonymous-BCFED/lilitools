import { LoadingDialog } from "./controls/dialogs/loading";
import { NavBar } from "./controls/nav";
declare global {
  interface Window {
    reloadData: (cb: Action?) => void;
    dumpSlaves: () => void;
    setPage: (pageClass: any, $container: JQuery) => void;
    currentPage: Page | null;
    currentPageClass: any | null;
    slaveEditor: SlaveEditor | null;
    navbar: NavBar | null;
    toasts: ToastManager | null;
  }
}
type Action = () => void;
declare const reloadData: (cb: Action?) => void;
declare const dumpSlaves: () => void;
declare const setPage: (pageClass: any, $container: JQuery) => void;
declare var currentPage: Page | null;
declare var currentPageClass: any | null;
declare var slaveEditor: SlaveEditor | null;
declare var navbar: JQuery | null;
declare var toasts: ToastManager | null;
declare module "*.svg" {
  const content: any;
  export default content;
}
// @GENERATED
const BS_ICON_0_CIRCLE: string = require("bootstrap-icons/icons/0-circle.svg");
const BS_ICON_0_CIRCLE_FILL: string = require("bootstrap-icons/icons/0-circle-fill.svg");
const BS_ICON_0_SQUARE: string = require("bootstrap-icons/icons/0-square.svg");
const BS_ICON_0_SQUARE_FILL: string = require("bootstrap-icons/icons/0-square-fill.svg");
const BS_ICON_1_CIRCLE: string = require("bootstrap-icons/icons/1-circle.svg");
const BS_ICON_1_CIRCLE_FILL: string = require("bootstrap-icons/icons/1-circle-fill.svg");
const BS_ICON_1_SQUARE: string = require("bootstrap-icons/icons/1-square.svg");
const BS_ICON_1_SQUARE_FILL: string = require("bootstrap-icons/icons/1-square-fill.svg");
const BS_ICON_123: string = require("bootstrap-icons/icons/123.svg");
const BS_ICON_2_CIRCLE: string = require("bootstrap-icons/icons/2-circle.svg");
const BS_ICON_2_CIRCLE_FILL: string = require("bootstrap-icons/icons/2-circle-fill.svg");
const BS_ICON_2_SQUARE: string = require("bootstrap-icons/icons/2-square.svg");
const BS_ICON_2_SQUARE_FILL: string = require("bootstrap-icons/icons/2-square-fill.svg");
const BS_ICON_3_CIRCLE: string = require("bootstrap-icons/icons/3-circle.svg");
const BS_ICON_3_CIRCLE_FILL: string = require("bootstrap-icons/icons/3-circle-fill.svg");
const BS_ICON_3_SQUARE: string = require("bootstrap-icons/icons/3-square.svg");
const BS_ICON_3_SQUARE_FILL: string = require("bootstrap-icons/icons/3-square-fill.svg");
const BS_ICON_4_CIRCLE: string = require("bootstrap-icons/icons/4-circle.svg");
const BS_ICON_4_CIRCLE_FILL: string = require("bootstrap-icons/icons/4-circle-fill.svg");
const BS_ICON_4_SQUARE: string = require("bootstrap-icons/icons/4-square.svg");
const BS_ICON_4_SQUARE_FILL: string = require("bootstrap-icons/icons/4-square-fill.svg");
const BS_ICON_5_CIRCLE: string = require("bootstrap-icons/icons/5-circle.svg");
const BS_ICON_5_CIRCLE_FILL: string = require("bootstrap-icons/icons/5-circle-fill.svg");
const BS_ICON_5_SQUARE: string = require("bootstrap-icons/icons/5-square.svg");
const BS_ICON_5_SQUARE_FILL: string = require("bootstrap-icons/icons/5-square-fill.svg");
const BS_ICON_6_CIRCLE: string = require("bootstrap-icons/icons/6-circle.svg");
const BS_ICON_6_CIRCLE_FILL: string = require("bootstrap-icons/icons/6-circle-fill.svg");
const BS_ICON_6_SQUARE: string = require("bootstrap-icons/icons/6-square.svg");
const BS_ICON_6_SQUARE_FILL: string = require("bootstrap-icons/icons/6-square-fill.svg");
const BS_ICON_7_CIRCLE: string = require("bootstrap-icons/icons/7-circle.svg");
const BS_ICON_7_CIRCLE_FILL: string = require("bootstrap-icons/icons/7-circle-fill.svg");
const BS_ICON_7_SQUARE: string = require("bootstrap-icons/icons/7-square.svg");
const BS_ICON_7_SQUARE_FILL: string = require("bootstrap-icons/icons/7-square-fill.svg");
const BS_ICON_8_CIRCLE: string = require("bootstrap-icons/icons/8-circle.svg");
const BS_ICON_8_CIRCLE_FILL: string = require("bootstrap-icons/icons/8-circle-fill.svg");
const BS_ICON_8_SQUARE: string = require("bootstrap-icons/icons/8-square.svg");
const BS_ICON_8_SQUARE_FILL: string = require("bootstrap-icons/icons/8-square-fill.svg");
const BS_ICON_9_CIRCLE: string = require("bootstrap-icons/icons/9-circle.svg");
const BS_ICON_9_CIRCLE_FILL: string = require("bootstrap-icons/icons/9-circle-fill.svg");
const BS_ICON_9_SQUARE: string = require("bootstrap-icons/icons/9-square.svg");
const BS_ICON_9_SQUARE_FILL: string = require("bootstrap-icons/icons/9-square-fill.svg");
const BS_ICON_ACTIVITY: string = require("bootstrap-icons/icons/activity.svg");
const BS_ICON_AIRPLANE: string = require("bootstrap-icons/icons/airplane.svg");
const BS_ICON_AIRPLANE_ENGINES: string = require("bootstrap-icons/icons/airplane-engines.svg");
const BS_ICON_AIRPLANE_ENGINES_FILL: string = require("bootstrap-icons/icons/airplane-engines-fill.svg");
const BS_ICON_AIRPLANE_FILL: string = require("bootstrap-icons/icons/airplane-fill.svg");
const BS_ICON_ALARM: string = require("bootstrap-icons/icons/alarm.svg");
const BS_ICON_ALARM_FILL: string = require("bootstrap-icons/icons/alarm-fill.svg");
const BS_ICON_ALEXA: string = require("bootstrap-icons/icons/alexa.svg");
const BS_ICON_ALIGN_BOTTOM: string = require("bootstrap-icons/icons/align-bottom.svg");
const BS_ICON_ALIGN_CENTER: string = require("bootstrap-icons/icons/align-center.svg");
const BS_ICON_ALIGN_END: string = require("bootstrap-icons/icons/align-end.svg");
const BS_ICON_ALIGN_MIDDLE: string = require("bootstrap-icons/icons/align-middle.svg");
const BS_ICON_ALIGN_START: string = require("bootstrap-icons/icons/align-start.svg");
const BS_ICON_ALIGN_TOP: string = require("bootstrap-icons/icons/align-top.svg");
const BS_ICON_ALIPAY: string = require("bootstrap-icons/icons/alipay.svg");
const BS_ICON_ALPHABET: string = require("bootstrap-icons/icons/alphabet.svg");
const BS_ICON_ALPHABET_UPPERCASE: string = require("bootstrap-icons/icons/alphabet-uppercase.svg");
const BS_ICON_ALT: string = require("bootstrap-icons/icons/alt.svg");
const BS_ICON_AMAZON: string = require("bootstrap-icons/icons/amazon.svg");
const BS_ICON_AMD: string = require("bootstrap-icons/icons/amd.svg");
const BS_ICON_ANDROID: string = require("bootstrap-icons/icons/android.svg");
const BS_ICON_ANDROID2: string = require("bootstrap-icons/icons/android2.svg");
const BS_ICON_APP: string = require("bootstrap-icons/icons/app.svg");
const BS_ICON_APP_INDICATOR: string = require("bootstrap-icons/icons/app-indicator.svg");
const BS_ICON_APPLE: string = require("bootstrap-icons/icons/apple.svg");
const BS_ICON_ARCHIVE: string = require("bootstrap-icons/icons/archive.svg");
const BS_ICON_ARCHIVE_FILL: string = require("bootstrap-icons/icons/archive-fill.svg");
const BS_ICON_ARROW_90DEG_DOWN: string = require("bootstrap-icons/icons/arrow-90deg-down.svg");
const BS_ICON_ARROW_90DEG_LEFT: string = require("bootstrap-icons/icons/arrow-90deg-left.svg");
const BS_ICON_ARROW_90DEG_RIGHT: string = require("bootstrap-icons/icons/arrow-90deg-right.svg");
const BS_ICON_ARROW_90DEG_UP: string = require("bootstrap-icons/icons/arrow-90deg-up.svg");
const BS_ICON_ARROW_BAR_DOWN: string = require("bootstrap-icons/icons/arrow-bar-down.svg");
const BS_ICON_ARROW_BAR_LEFT: string = require("bootstrap-icons/icons/arrow-bar-left.svg");
const BS_ICON_ARROW_BAR_RIGHT: string = require("bootstrap-icons/icons/arrow-bar-right.svg");
const BS_ICON_ARROW_BAR_UP: string = require("bootstrap-icons/icons/arrow-bar-up.svg");
const BS_ICON_ARROW_CLOCKWISE: string = require("bootstrap-icons/icons/arrow-clockwise.svg");
const BS_ICON_ARROW_COUNTERCLOCKWISE: string = require("bootstrap-icons/icons/arrow-counterclockwise.svg");
const BS_ICON_ARROW_DOWN: string = require("bootstrap-icons/icons/arrow-down.svg");
const BS_ICON_ARROW_DOWN_CIRCLE: string = require("bootstrap-icons/icons/arrow-down-circle.svg");
const BS_ICON_ARROW_DOWN_CIRCLE_FILL: string = require("bootstrap-icons/icons/arrow-down-circle-fill.svg");
const BS_ICON_ARROW_DOWN_LEFT: string = require("bootstrap-icons/icons/arrow-down-left.svg");
const BS_ICON_ARROW_DOWN_LEFT_CIRCLE: string = require("bootstrap-icons/icons/arrow-down-left-circle.svg");
const BS_ICON_ARROW_DOWN_LEFT_CIRCLE_FILL: string = require("bootstrap-icons/icons/arrow-down-left-circle-fill.svg");
const BS_ICON_ARROW_DOWN_LEFT_SQUARE: string = require("bootstrap-icons/icons/arrow-down-left-square.svg");
const BS_ICON_ARROW_DOWN_LEFT_SQUARE_FILL: string = require("bootstrap-icons/icons/arrow-down-left-square-fill.svg");
const BS_ICON_ARROW_DOWN_RIGHT: string = require("bootstrap-icons/icons/arrow-down-right.svg");
const BS_ICON_ARROW_DOWN_RIGHT_CIRCLE: string = require("bootstrap-icons/icons/arrow-down-right-circle.svg");
const BS_ICON_ARROW_DOWN_RIGHT_CIRCLE_FILL: string = require("bootstrap-icons/icons/arrow-down-right-circle-fill.svg");
const BS_ICON_ARROW_DOWN_RIGHT_SQUARE: string = require("bootstrap-icons/icons/arrow-down-right-square.svg");
const BS_ICON_ARROW_DOWN_RIGHT_SQUARE_FILL: string = require("bootstrap-icons/icons/arrow-down-right-square-fill.svg");
const BS_ICON_ARROW_DOWN_SHORT: string = require("bootstrap-icons/icons/arrow-down-short.svg");
const BS_ICON_ARROW_DOWN_SQUARE: string = require("bootstrap-icons/icons/arrow-down-square.svg");
const BS_ICON_ARROW_DOWN_SQUARE_FILL: string = require("bootstrap-icons/icons/arrow-down-square-fill.svg");
const BS_ICON_ARROW_DOWN_UP: string = require("bootstrap-icons/icons/arrow-down-up.svg");
const BS_ICON_ARROW_LEFT: string = require("bootstrap-icons/icons/arrow-left.svg");
const BS_ICON_ARROW_LEFT_CIRCLE: string = require("bootstrap-icons/icons/arrow-left-circle.svg");
const BS_ICON_ARROW_LEFT_CIRCLE_FILL: string = require("bootstrap-icons/icons/arrow-left-circle-fill.svg");
const BS_ICON_ARROW_LEFT_RIGHT: string = require("bootstrap-icons/icons/arrow-left-right.svg");
const BS_ICON_ARROW_LEFT_SHORT: string = require("bootstrap-icons/icons/arrow-left-short.svg");
const BS_ICON_ARROW_LEFT_SQUARE: string = require("bootstrap-icons/icons/arrow-left-square.svg");
const BS_ICON_ARROW_LEFT_SQUARE_FILL: string = require("bootstrap-icons/icons/arrow-left-square-fill.svg");
const BS_ICON_ARROW_REPEAT: string = require("bootstrap-icons/icons/arrow-repeat.svg");
const BS_ICON_ARROW_RETURN_LEFT: string = require("bootstrap-icons/icons/arrow-return-left.svg");
const BS_ICON_ARROW_RETURN_RIGHT: string = require("bootstrap-icons/icons/arrow-return-right.svg");
const BS_ICON_ARROW_RIGHT: string = require("bootstrap-icons/icons/arrow-right.svg");
const BS_ICON_ARROW_RIGHT_CIRCLE: string = require("bootstrap-icons/icons/arrow-right-circle.svg");
const BS_ICON_ARROW_RIGHT_CIRCLE_FILL: string = require("bootstrap-icons/icons/arrow-right-circle-fill.svg");
const BS_ICON_ARROW_RIGHT_SHORT: string = require("bootstrap-icons/icons/arrow-right-short.svg");
const BS_ICON_ARROW_RIGHT_SQUARE: string = require("bootstrap-icons/icons/arrow-right-square.svg");
const BS_ICON_ARROW_RIGHT_SQUARE_FILL: string = require("bootstrap-icons/icons/arrow-right-square-fill.svg");
const BS_ICON_ARROW_THROUGH_HEART: string = require("bootstrap-icons/icons/arrow-through-heart.svg");
const BS_ICON_ARROW_THROUGH_HEART_FILL: string = require("bootstrap-icons/icons/arrow-through-heart-fill.svg");
const BS_ICON_ARROW_UP: string = require("bootstrap-icons/icons/arrow-up.svg");
const BS_ICON_ARROW_UP_CIRCLE: string = require("bootstrap-icons/icons/arrow-up-circle.svg");
const BS_ICON_ARROW_UP_CIRCLE_FILL: string = require("bootstrap-icons/icons/arrow-up-circle-fill.svg");
const BS_ICON_ARROW_UP_LEFT: string = require("bootstrap-icons/icons/arrow-up-left.svg");
const BS_ICON_ARROW_UP_LEFT_CIRCLE: string = require("bootstrap-icons/icons/arrow-up-left-circle.svg");
const BS_ICON_ARROW_UP_LEFT_CIRCLE_FILL: string = require("bootstrap-icons/icons/arrow-up-left-circle-fill.svg");
const BS_ICON_ARROW_UP_LEFT_SQUARE: string = require("bootstrap-icons/icons/arrow-up-left-square.svg");
const BS_ICON_ARROW_UP_LEFT_SQUARE_FILL: string = require("bootstrap-icons/icons/arrow-up-left-square-fill.svg");
const BS_ICON_ARROW_UP_RIGHT: string = require("bootstrap-icons/icons/arrow-up-right.svg");
const BS_ICON_ARROW_UP_RIGHT_CIRCLE: string = require("bootstrap-icons/icons/arrow-up-right-circle.svg");
const BS_ICON_ARROW_UP_RIGHT_CIRCLE_FILL: string = require("bootstrap-icons/icons/arrow-up-right-circle-fill.svg");
const BS_ICON_ARROW_UP_RIGHT_SQUARE: string = require("bootstrap-icons/icons/arrow-up-right-square.svg");
const BS_ICON_ARROW_UP_RIGHT_SQUARE_FILL: string = require("bootstrap-icons/icons/arrow-up-right-square-fill.svg");
const BS_ICON_ARROW_UP_SHORT: string = require("bootstrap-icons/icons/arrow-up-short.svg");
const BS_ICON_ARROW_UP_SQUARE: string = require("bootstrap-icons/icons/arrow-up-square.svg");
const BS_ICON_ARROW_UP_SQUARE_FILL: string = require("bootstrap-icons/icons/arrow-up-square-fill.svg");
const BS_ICON_ARROWS: string = require("bootstrap-icons/icons/arrows.svg");
const BS_ICON_ARROWS_ANGLE_CONTRACT: string = require("bootstrap-icons/icons/arrows-angle-contract.svg");
const BS_ICON_ARROWS_ANGLE_EXPAND: string = require("bootstrap-icons/icons/arrows-angle-expand.svg");
const BS_ICON_ARROWS_COLLAPSE: string = require("bootstrap-icons/icons/arrows-collapse.svg");
const BS_ICON_ARROWS_COLLAPSE_VERTICAL: string = require("bootstrap-icons/icons/arrows-collapse-vertical.svg");
const BS_ICON_ARROWS_EXPAND: string = require("bootstrap-icons/icons/arrows-expand.svg");
const BS_ICON_ARROWS_EXPAND_VERTICAL: string = require("bootstrap-icons/icons/arrows-expand-vertical.svg");
const BS_ICON_ARROWS_FULLSCREEN: string = require("bootstrap-icons/icons/arrows-fullscreen.svg");
const BS_ICON_ARROWS_MOVE: string = require("bootstrap-icons/icons/arrows-move.svg");
const BS_ICON_ARROWS_VERTICAL: string = require("bootstrap-icons/icons/arrows-vertical.svg");
const BS_ICON_ASPECT_RATIO: string = require("bootstrap-icons/icons/aspect-ratio.svg");
const BS_ICON_ASPECT_RATIO_FILL: string = require("bootstrap-icons/icons/aspect-ratio-fill.svg");
const BS_ICON_ASTERISK: string = require("bootstrap-icons/icons/asterisk.svg");
const BS_ICON_AT: string = require("bootstrap-icons/icons/at.svg");
const BS_ICON_AWARD: string = require("bootstrap-icons/icons/award.svg");
const BS_ICON_AWARD_FILL: string = require("bootstrap-icons/icons/award-fill.svg");
const BS_ICON_BACK: string = require("bootstrap-icons/icons/back.svg");
const BS_ICON_BACKPACK: string = require("bootstrap-icons/icons/backpack.svg");
const BS_ICON_BACKPACK_FILL: string = require("bootstrap-icons/icons/backpack-fill.svg");
const BS_ICON_BACKPACK2: string = require("bootstrap-icons/icons/backpack2.svg");
const BS_ICON_BACKPACK2_FILL: string = require("bootstrap-icons/icons/backpack2-fill.svg");
const BS_ICON_BACKPACK3: string = require("bootstrap-icons/icons/backpack3.svg");
const BS_ICON_BACKPACK3_FILL: string = require("bootstrap-icons/icons/backpack3-fill.svg");
const BS_ICON_BACKPACK4: string = require("bootstrap-icons/icons/backpack4.svg");
const BS_ICON_BACKPACK4_FILL: string = require("bootstrap-icons/icons/backpack4-fill.svg");
const BS_ICON_BACKSPACE: string = require("bootstrap-icons/icons/backspace.svg");
const BS_ICON_BACKSPACE_FILL: string = require("bootstrap-icons/icons/backspace-fill.svg");
const BS_ICON_BACKSPACE_REVERSE: string = require("bootstrap-icons/icons/backspace-reverse.svg");
const BS_ICON_BACKSPACE_REVERSE_FILL: string = require("bootstrap-icons/icons/backspace-reverse-fill.svg");
const BS_ICON_BADGE_3D: string = require("bootstrap-icons/icons/badge-3d.svg");
const BS_ICON_BADGE_3D_FILL: string = require("bootstrap-icons/icons/badge-3d-fill.svg");
const BS_ICON_BADGE_4K: string = require("bootstrap-icons/icons/badge-4k.svg");
const BS_ICON_BADGE_4K_FILL: string = require("bootstrap-icons/icons/badge-4k-fill.svg");
const BS_ICON_BADGE_8K: string = require("bootstrap-icons/icons/badge-8k.svg");
const BS_ICON_BADGE_8K_FILL: string = require("bootstrap-icons/icons/badge-8k-fill.svg");
const BS_ICON_BADGE_AD: string = require("bootstrap-icons/icons/badge-ad.svg");
const BS_ICON_BADGE_AD_FILL: string = require("bootstrap-icons/icons/badge-ad-fill.svg");
const BS_ICON_BADGE_AR: string = require("bootstrap-icons/icons/badge-ar.svg");
const BS_ICON_BADGE_AR_FILL: string = require("bootstrap-icons/icons/badge-ar-fill.svg");
const BS_ICON_BADGE_CC: string = require("bootstrap-icons/icons/badge-cc.svg");
const BS_ICON_BADGE_CC_FILL: string = require("bootstrap-icons/icons/badge-cc-fill.svg");
const BS_ICON_BADGE_HD: string = require("bootstrap-icons/icons/badge-hd.svg");
const BS_ICON_BADGE_HD_FILL: string = require("bootstrap-icons/icons/badge-hd-fill.svg");
const BS_ICON_BADGE_SD: string = require("bootstrap-icons/icons/badge-sd.svg");
const BS_ICON_BADGE_SD_FILL: string = require("bootstrap-icons/icons/badge-sd-fill.svg");
const BS_ICON_BADGE_TM: string = require("bootstrap-icons/icons/badge-tm.svg");
const BS_ICON_BADGE_TM_FILL: string = require("bootstrap-icons/icons/badge-tm-fill.svg");
const BS_ICON_BADGE_VO: string = require("bootstrap-icons/icons/badge-vo.svg");
const BS_ICON_BADGE_VO_FILL: string = require("bootstrap-icons/icons/badge-vo-fill.svg");
const BS_ICON_BADGE_VR: string = require("bootstrap-icons/icons/badge-vr.svg");
const BS_ICON_BADGE_VR_FILL: string = require("bootstrap-icons/icons/badge-vr-fill.svg");
const BS_ICON_BADGE_WC: string = require("bootstrap-icons/icons/badge-wc.svg");
const BS_ICON_BADGE_WC_FILL: string = require("bootstrap-icons/icons/badge-wc-fill.svg");
const BS_ICON_BAG: string = require("bootstrap-icons/icons/bag.svg");
const BS_ICON_BAG_CHECK: string = require("bootstrap-icons/icons/bag-check.svg");
const BS_ICON_BAG_CHECK_FILL: string = require("bootstrap-icons/icons/bag-check-fill.svg");
const BS_ICON_BAG_DASH: string = require("bootstrap-icons/icons/bag-dash.svg");
const BS_ICON_BAG_DASH_FILL: string = require("bootstrap-icons/icons/bag-dash-fill.svg");
const BS_ICON_BAG_FILL: string = require("bootstrap-icons/icons/bag-fill.svg");
const BS_ICON_BAG_HEART: string = require("bootstrap-icons/icons/bag-heart.svg");
const BS_ICON_BAG_HEART_FILL: string = require("bootstrap-icons/icons/bag-heart-fill.svg");
const BS_ICON_BAG_PLUS: string = require("bootstrap-icons/icons/bag-plus.svg");
const BS_ICON_BAG_PLUS_FILL: string = require("bootstrap-icons/icons/bag-plus-fill.svg");
const BS_ICON_BAG_X: string = require("bootstrap-icons/icons/bag-x.svg");
const BS_ICON_BAG_X_FILL: string = require("bootstrap-icons/icons/bag-x-fill.svg");
const BS_ICON_BALLOON: string = require("bootstrap-icons/icons/balloon.svg");
const BS_ICON_BALLOON_FILL: string = require("bootstrap-icons/icons/balloon-fill.svg");
const BS_ICON_BALLOON_HEART: string = require("bootstrap-icons/icons/balloon-heart.svg");
const BS_ICON_BALLOON_HEART_FILL: string = require("bootstrap-icons/icons/balloon-heart-fill.svg");
const BS_ICON_BAN: string = require("bootstrap-icons/icons/ban.svg");
const BS_ICON_BAN_FILL: string = require("bootstrap-icons/icons/ban-fill.svg");
const BS_ICON_BANDAID: string = require("bootstrap-icons/icons/bandaid.svg");
const BS_ICON_BANDAID_FILL: string = require("bootstrap-icons/icons/bandaid-fill.svg");
const BS_ICON_BANK: string = require("bootstrap-icons/icons/bank.svg");
const BS_ICON_BANK2: string = require("bootstrap-icons/icons/bank2.svg");
const BS_ICON_BAR_CHART: string = require("bootstrap-icons/icons/bar-chart.svg");
const BS_ICON_BAR_CHART_FILL: string = require("bootstrap-icons/icons/bar-chart-fill.svg");
const BS_ICON_BAR_CHART_LINE: string = require("bootstrap-icons/icons/bar-chart-line.svg");
const BS_ICON_BAR_CHART_LINE_FILL: string = require("bootstrap-icons/icons/bar-chart-line-fill.svg");
const BS_ICON_BAR_CHART_STEPS: string = require("bootstrap-icons/icons/bar-chart-steps.svg");
const BS_ICON_BASKET: string = require("bootstrap-icons/icons/basket.svg");
const BS_ICON_BASKET_FILL: string = require("bootstrap-icons/icons/basket-fill.svg");
const BS_ICON_BASKET2: string = require("bootstrap-icons/icons/basket2.svg");
const BS_ICON_BASKET2_FILL: string = require("bootstrap-icons/icons/basket2-fill.svg");
const BS_ICON_BASKET3: string = require("bootstrap-icons/icons/basket3.svg");
const BS_ICON_BASKET3_FILL: string = require("bootstrap-icons/icons/basket3-fill.svg");
const BS_ICON_BATTERY: string = require("bootstrap-icons/icons/battery.svg");
const BS_ICON_BATTERY_CHARGING: string = require("bootstrap-icons/icons/battery-charging.svg");
const BS_ICON_BATTERY_FULL: string = require("bootstrap-icons/icons/battery-full.svg");
const BS_ICON_BATTERY_HALF: string = require("bootstrap-icons/icons/battery-half.svg");
const BS_ICON_BEHANCE: string = require("bootstrap-icons/icons/behance.svg");
const BS_ICON_BELL: string = require("bootstrap-icons/icons/bell.svg");
const BS_ICON_BELL_FILL: string = require("bootstrap-icons/icons/bell-fill.svg");
const BS_ICON_BELL_SLASH: string = require("bootstrap-icons/icons/bell-slash.svg");
const BS_ICON_BELL_SLASH_FILL: string = require("bootstrap-icons/icons/bell-slash-fill.svg");
const BS_ICON_BEZIER: string = require("bootstrap-icons/icons/bezier.svg");
const BS_ICON_BEZIER2: string = require("bootstrap-icons/icons/bezier2.svg");
const BS_ICON_BICYCLE: string = require("bootstrap-icons/icons/bicycle.svg");
const BS_ICON_BING: string = require("bootstrap-icons/icons/bing.svg");
const BS_ICON_BINOCULARS: string = require("bootstrap-icons/icons/binoculars.svg");
const BS_ICON_BINOCULARS_FILL: string = require("bootstrap-icons/icons/binoculars-fill.svg");
const BS_ICON_BLOCKQUOTE_LEFT: string = require("bootstrap-icons/icons/blockquote-left.svg");
const BS_ICON_BLOCKQUOTE_RIGHT: string = require("bootstrap-icons/icons/blockquote-right.svg");
const BS_ICON_BLUETOOTH: string = require("bootstrap-icons/icons/bluetooth.svg");
const BS_ICON_BODY_TEXT: string = require("bootstrap-icons/icons/body-text.svg");
const BS_ICON_BOOK: string = require("bootstrap-icons/icons/book.svg");
const BS_ICON_BOOK_FILL: string = require("bootstrap-icons/icons/book-fill.svg");
const BS_ICON_BOOK_HALF: string = require("bootstrap-icons/icons/book-half.svg");
const BS_ICON_BOOKMARK: string = require("bootstrap-icons/icons/bookmark.svg");
const BS_ICON_BOOKMARK_CHECK: string = require("bootstrap-icons/icons/bookmark-check.svg");
const BS_ICON_BOOKMARK_CHECK_FILL: string = require("bootstrap-icons/icons/bookmark-check-fill.svg");
const BS_ICON_BOOKMARK_DASH: string = require("bootstrap-icons/icons/bookmark-dash.svg");
const BS_ICON_BOOKMARK_DASH_FILL: string = require("bootstrap-icons/icons/bookmark-dash-fill.svg");
const BS_ICON_BOOKMARK_FILL: string = require("bootstrap-icons/icons/bookmark-fill.svg");
const BS_ICON_BOOKMARK_HEART: string = require("bootstrap-icons/icons/bookmark-heart.svg");
const BS_ICON_BOOKMARK_HEART_FILL: string = require("bootstrap-icons/icons/bookmark-heart-fill.svg");
const BS_ICON_BOOKMARK_PLUS: string = require("bootstrap-icons/icons/bookmark-plus.svg");
const BS_ICON_BOOKMARK_PLUS_FILL: string = require("bootstrap-icons/icons/bookmark-plus-fill.svg");
const BS_ICON_BOOKMARK_STAR: string = require("bootstrap-icons/icons/bookmark-star.svg");
const BS_ICON_BOOKMARK_STAR_FILL: string = require("bootstrap-icons/icons/bookmark-star-fill.svg");
const BS_ICON_BOOKMARK_X: string = require("bootstrap-icons/icons/bookmark-x.svg");
const BS_ICON_BOOKMARK_X_FILL: string = require("bootstrap-icons/icons/bookmark-x-fill.svg");
const BS_ICON_BOOKMARKS: string = require("bootstrap-icons/icons/bookmarks.svg");
const BS_ICON_BOOKMARKS_FILL: string = require("bootstrap-icons/icons/bookmarks-fill.svg");
const BS_ICON_BOOKSHELF: string = require("bootstrap-icons/icons/bookshelf.svg");
const BS_ICON_BOOMBOX: string = require("bootstrap-icons/icons/boombox.svg");
const BS_ICON_BOOMBOX_FILL: string = require("bootstrap-icons/icons/boombox-fill.svg");
const BS_ICON_BOOTSTRAP: string = require("bootstrap-icons/icons/bootstrap.svg");
const BS_ICON_BOOTSTRAP_FILL: string = require("bootstrap-icons/icons/bootstrap-fill.svg");
const BS_ICON_BOOTSTRAP_REBOOT: string = require("bootstrap-icons/icons/bootstrap-reboot.svg");
const BS_ICON_BORDER: string = require("bootstrap-icons/icons/border.svg");
const BS_ICON_BORDER_ALL: string = require("bootstrap-icons/icons/border-all.svg");
const BS_ICON_BORDER_BOTTOM: string = require("bootstrap-icons/icons/border-bottom.svg");
const BS_ICON_BORDER_CENTER: string = require("bootstrap-icons/icons/border-center.svg");
const BS_ICON_BORDER_INNER: string = require("bootstrap-icons/icons/border-inner.svg");
const BS_ICON_BORDER_LEFT: string = require("bootstrap-icons/icons/border-left.svg");
const BS_ICON_BORDER_MIDDLE: string = require("bootstrap-icons/icons/border-middle.svg");
const BS_ICON_BORDER_OUTER: string = require("bootstrap-icons/icons/border-outer.svg");
const BS_ICON_BORDER_RIGHT: string = require("bootstrap-icons/icons/border-right.svg");
const BS_ICON_BORDER_STYLE: string = require("bootstrap-icons/icons/border-style.svg");
const BS_ICON_BORDER_TOP: string = require("bootstrap-icons/icons/border-top.svg");
const BS_ICON_BORDER_WIDTH: string = require("bootstrap-icons/icons/border-width.svg");
const BS_ICON_BOUNDING_BOX: string = require("bootstrap-icons/icons/bounding-box.svg");
const BS_ICON_BOUNDING_BOX_CIRCLES: string = require("bootstrap-icons/icons/bounding-box-circles.svg");
const BS_ICON_BOX: string = require("bootstrap-icons/icons/box.svg");
const BS_ICON_BOX_ARROW_DOWN: string = require("bootstrap-icons/icons/box-arrow-down.svg");
const BS_ICON_BOX_ARROW_DOWN_LEFT: string = require("bootstrap-icons/icons/box-arrow-down-left.svg");
const BS_ICON_BOX_ARROW_DOWN_RIGHT: string = require("bootstrap-icons/icons/box-arrow-down-right.svg");
const BS_ICON_BOX_ARROW_IN_DOWN: string = require("bootstrap-icons/icons/box-arrow-in-down.svg");
const BS_ICON_BOX_ARROW_IN_DOWN_LEFT: string = require("bootstrap-icons/icons/box-arrow-in-down-left.svg");
const BS_ICON_BOX_ARROW_IN_DOWN_RIGHT: string = require("bootstrap-icons/icons/box-arrow-in-down-right.svg");
const BS_ICON_BOX_ARROW_IN_LEFT: string = require("bootstrap-icons/icons/box-arrow-in-left.svg");
const BS_ICON_BOX_ARROW_IN_RIGHT: string = require("bootstrap-icons/icons/box-arrow-in-right.svg");
const BS_ICON_BOX_ARROW_IN_UP: string = require("bootstrap-icons/icons/box-arrow-in-up.svg");
const BS_ICON_BOX_ARROW_IN_UP_LEFT: string = require("bootstrap-icons/icons/box-arrow-in-up-left.svg");
const BS_ICON_BOX_ARROW_IN_UP_RIGHT: string = require("bootstrap-icons/icons/box-arrow-in-up-right.svg");
const BS_ICON_BOX_ARROW_LEFT: string = require("bootstrap-icons/icons/box-arrow-left.svg");
const BS_ICON_BOX_ARROW_RIGHT: string = require("bootstrap-icons/icons/box-arrow-right.svg");
const BS_ICON_BOX_ARROW_UP: string = require("bootstrap-icons/icons/box-arrow-up.svg");
const BS_ICON_BOX_ARROW_UP_LEFT: string = require("bootstrap-icons/icons/box-arrow-up-left.svg");
const BS_ICON_BOX_ARROW_UP_RIGHT: string = require("bootstrap-icons/icons/box-arrow-up-right.svg");
const BS_ICON_BOX_FILL: string = require("bootstrap-icons/icons/box-fill.svg");
const BS_ICON_BOX_SEAM: string = require("bootstrap-icons/icons/box-seam.svg");
const BS_ICON_BOX_SEAM_FILL: string = require("bootstrap-icons/icons/box-seam-fill.svg");
const BS_ICON_BOX2: string = require("bootstrap-icons/icons/box2.svg");
const BS_ICON_BOX2_FILL: string = require("bootstrap-icons/icons/box2-fill.svg");
const BS_ICON_BOX2_HEART: string = require("bootstrap-icons/icons/box2-heart.svg");
const BS_ICON_BOX2_HEART_FILL: string = require("bootstrap-icons/icons/box2-heart-fill.svg");
const BS_ICON_BOXES: string = require("bootstrap-icons/icons/boxes.svg");
const BS_ICON_BRACES: string = require("bootstrap-icons/icons/braces.svg");
const BS_ICON_BRACES_ASTERISK: string = require("bootstrap-icons/icons/braces-asterisk.svg");
const BS_ICON_BRICKS: string = require("bootstrap-icons/icons/bricks.svg");
const BS_ICON_BRIEFCASE: string = require("bootstrap-icons/icons/briefcase.svg");
const BS_ICON_BRIEFCASE_FILL: string = require("bootstrap-icons/icons/briefcase-fill.svg");
const BS_ICON_BRIGHTNESS_ALT_HIGH: string = require("bootstrap-icons/icons/brightness-alt-high.svg");
const BS_ICON_BRIGHTNESS_ALT_HIGH_FILL: string = require("bootstrap-icons/icons/brightness-alt-high-fill.svg");
const BS_ICON_BRIGHTNESS_ALT_LOW: string = require("bootstrap-icons/icons/brightness-alt-low.svg");
const BS_ICON_BRIGHTNESS_ALT_LOW_FILL: string = require("bootstrap-icons/icons/brightness-alt-low-fill.svg");
const BS_ICON_BRIGHTNESS_HIGH: string = require("bootstrap-icons/icons/brightness-high.svg");
const BS_ICON_BRIGHTNESS_HIGH_FILL: string = require("bootstrap-icons/icons/brightness-high-fill.svg");
const BS_ICON_BRIGHTNESS_LOW: string = require("bootstrap-icons/icons/brightness-low.svg");
const BS_ICON_BRIGHTNESS_LOW_FILL: string = require("bootstrap-icons/icons/brightness-low-fill.svg");
const BS_ICON_BRILLIANCE: string = require("bootstrap-icons/icons/brilliance.svg");
const BS_ICON_BROADCAST: string = require("bootstrap-icons/icons/broadcast.svg");
const BS_ICON_BROADCAST_PIN: string = require("bootstrap-icons/icons/broadcast-pin.svg");
const BS_ICON_BROWSER_CHROME: string = require("bootstrap-icons/icons/browser-chrome.svg");
const BS_ICON_BROWSER_EDGE: string = require("bootstrap-icons/icons/browser-edge.svg");
const BS_ICON_BROWSER_FIREFOX: string = require("bootstrap-icons/icons/browser-firefox.svg");
const BS_ICON_BROWSER_SAFARI: string = require("bootstrap-icons/icons/browser-safari.svg");
const BS_ICON_BRUSH: string = require("bootstrap-icons/icons/brush.svg");
const BS_ICON_BRUSH_FILL: string = require("bootstrap-icons/icons/brush-fill.svg");
const BS_ICON_BUCKET: string = require("bootstrap-icons/icons/bucket.svg");
const BS_ICON_BUCKET_FILL: string = require("bootstrap-icons/icons/bucket-fill.svg");
const BS_ICON_BUG: string = require("bootstrap-icons/icons/bug.svg");
const BS_ICON_BUG_FILL: string = require("bootstrap-icons/icons/bug-fill.svg");
const BS_ICON_BUILDING: string = require("bootstrap-icons/icons/building.svg");
const BS_ICON_BUILDING_ADD: string = require("bootstrap-icons/icons/building-add.svg");
const BS_ICON_BUILDING_CHECK: string = require("bootstrap-icons/icons/building-check.svg");
const BS_ICON_BUILDING_DASH: string = require("bootstrap-icons/icons/building-dash.svg");
const BS_ICON_BUILDING_DOWN: string = require("bootstrap-icons/icons/building-down.svg");
const BS_ICON_BUILDING_EXCLAMATION: string = require("bootstrap-icons/icons/building-exclamation.svg");
const BS_ICON_BUILDING_FILL: string = require("bootstrap-icons/icons/building-fill.svg");
const BS_ICON_BUILDING_FILL_ADD: string = require("bootstrap-icons/icons/building-fill-add.svg");
const BS_ICON_BUILDING_FILL_CHECK: string = require("bootstrap-icons/icons/building-fill-check.svg");
const BS_ICON_BUILDING_FILL_DASH: string = require("bootstrap-icons/icons/building-fill-dash.svg");
const BS_ICON_BUILDING_FILL_DOWN: string = require("bootstrap-icons/icons/building-fill-down.svg");
const BS_ICON_BUILDING_FILL_EXCLAMATION: string = require("bootstrap-icons/icons/building-fill-exclamation.svg");
const BS_ICON_BUILDING_FILL_GEAR: string = require("bootstrap-icons/icons/building-fill-gear.svg");
const BS_ICON_BUILDING_FILL_LOCK: string = require("bootstrap-icons/icons/building-fill-lock.svg");
const BS_ICON_BUILDING_FILL_SLASH: string = require("bootstrap-icons/icons/building-fill-slash.svg");
const BS_ICON_BUILDING_FILL_UP: string = require("bootstrap-icons/icons/building-fill-up.svg");
const BS_ICON_BUILDING_FILL_X: string = require("bootstrap-icons/icons/building-fill-x.svg");
const BS_ICON_BUILDING_GEAR: string = require("bootstrap-icons/icons/building-gear.svg");
const BS_ICON_BUILDING_LOCK: string = require("bootstrap-icons/icons/building-lock.svg");
const BS_ICON_BUILDING_SLASH: string = require("bootstrap-icons/icons/building-slash.svg");
const BS_ICON_BUILDING_UP: string = require("bootstrap-icons/icons/building-up.svg");
const BS_ICON_BUILDING_X: string = require("bootstrap-icons/icons/building-x.svg");
const BS_ICON_BUILDINGS: string = require("bootstrap-icons/icons/buildings.svg");
const BS_ICON_BUILDINGS_FILL: string = require("bootstrap-icons/icons/buildings-fill.svg");
const BS_ICON_BULLSEYE: string = require("bootstrap-icons/icons/bullseye.svg");
const BS_ICON_BUS_FRONT: string = require("bootstrap-icons/icons/bus-front.svg");
const BS_ICON_BUS_FRONT_FILL: string = require("bootstrap-icons/icons/bus-front-fill.svg");
const BS_ICON_C_CIRCLE: string = require("bootstrap-icons/icons/c-circle.svg");
const BS_ICON_C_CIRCLE_FILL: string = require("bootstrap-icons/icons/c-circle-fill.svg");
const BS_ICON_C_SQUARE: string = require("bootstrap-icons/icons/c-square.svg");
const BS_ICON_C_SQUARE_FILL: string = require("bootstrap-icons/icons/c-square-fill.svg");
const BS_ICON_CAKE: string = require("bootstrap-icons/icons/cake.svg");
const BS_ICON_CAKE_FILL: string = require("bootstrap-icons/icons/cake-fill.svg");
const BS_ICON_CAKE2: string = require("bootstrap-icons/icons/cake2.svg");
const BS_ICON_CAKE2_FILL: string = require("bootstrap-icons/icons/cake2-fill.svg");
const BS_ICON_CALCULATOR: string = require("bootstrap-icons/icons/calculator.svg");
const BS_ICON_CALCULATOR_FILL: string = require("bootstrap-icons/icons/calculator-fill.svg");
const BS_ICON_CALENDAR: string = require("bootstrap-icons/icons/calendar.svg");
const BS_ICON_CALENDAR_CHECK: string = require("bootstrap-icons/icons/calendar-check.svg");
const BS_ICON_CALENDAR_CHECK_FILL: string = require("bootstrap-icons/icons/calendar-check-fill.svg");
const BS_ICON_CALENDAR_DATE: string = require("bootstrap-icons/icons/calendar-date.svg");
const BS_ICON_CALENDAR_DATE_FILL: string = require("bootstrap-icons/icons/calendar-date-fill.svg");
const BS_ICON_CALENDAR_DAY: string = require("bootstrap-icons/icons/calendar-day.svg");
const BS_ICON_CALENDAR_DAY_FILL: string = require("bootstrap-icons/icons/calendar-day-fill.svg");
const BS_ICON_CALENDAR_EVENT: string = require("bootstrap-icons/icons/calendar-event.svg");
const BS_ICON_CALENDAR_EVENT_FILL: string = require("bootstrap-icons/icons/calendar-event-fill.svg");
const BS_ICON_CALENDAR_FILL: string = require("bootstrap-icons/icons/calendar-fill.svg");
const BS_ICON_CALENDAR_HEART: string = require("bootstrap-icons/icons/calendar-heart.svg");
const BS_ICON_CALENDAR_HEART_FILL: string = require("bootstrap-icons/icons/calendar-heart-fill.svg");
const BS_ICON_CALENDAR_MINUS: string = require("bootstrap-icons/icons/calendar-minus.svg");
const BS_ICON_CALENDAR_MINUS_FILL: string = require("bootstrap-icons/icons/calendar-minus-fill.svg");
const BS_ICON_CALENDAR_MONTH: string = require("bootstrap-icons/icons/calendar-month.svg");
const BS_ICON_CALENDAR_MONTH_FILL: string = require("bootstrap-icons/icons/calendar-month-fill.svg");
const BS_ICON_CALENDAR_PLUS: string = require("bootstrap-icons/icons/calendar-plus.svg");
const BS_ICON_CALENDAR_PLUS_FILL: string = require("bootstrap-icons/icons/calendar-plus-fill.svg");
const BS_ICON_CALENDAR_RANGE: string = require("bootstrap-icons/icons/calendar-range.svg");
const BS_ICON_CALENDAR_RANGE_FILL: string = require("bootstrap-icons/icons/calendar-range-fill.svg");
const BS_ICON_CALENDAR_WEEK: string = require("bootstrap-icons/icons/calendar-week.svg");
const BS_ICON_CALENDAR_WEEK_FILL: string = require("bootstrap-icons/icons/calendar-week-fill.svg");
const BS_ICON_CALENDAR_X: string = require("bootstrap-icons/icons/calendar-x.svg");
const BS_ICON_CALENDAR_X_FILL: string = require("bootstrap-icons/icons/calendar-x-fill.svg");
const BS_ICON_CALENDAR2: string = require("bootstrap-icons/icons/calendar2.svg");
const BS_ICON_CALENDAR2_CHECK: string = require("bootstrap-icons/icons/calendar2-check.svg");
const BS_ICON_CALENDAR2_CHECK_FILL: string = require("bootstrap-icons/icons/calendar2-check-fill.svg");
const BS_ICON_CALENDAR2_DATE: string = require("bootstrap-icons/icons/calendar2-date.svg");
const BS_ICON_CALENDAR2_DATE_FILL: string = require("bootstrap-icons/icons/calendar2-date-fill.svg");
const BS_ICON_CALENDAR2_DAY: string = require("bootstrap-icons/icons/calendar2-day.svg");
const BS_ICON_CALENDAR2_DAY_FILL: string = require("bootstrap-icons/icons/calendar2-day-fill.svg");
const BS_ICON_CALENDAR2_EVENT: string = require("bootstrap-icons/icons/calendar2-event.svg");
const BS_ICON_CALENDAR2_EVENT_FILL: string = require("bootstrap-icons/icons/calendar2-event-fill.svg");
const BS_ICON_CALENDAR2_FILL: string = require("bootstrap-icons/icons/calendar2-fill.svg");
const BS_ICON_CALENDAR2_HEART: string = require("bootstrap-icons/icons/calendar2-heart.svg");
const BS_ICON_CALENDAR2_HEART_FILL: string = require("bootstrap-icons/icons/calendar2-heart-fill.svg");
const BS_ICON_CALENDAR2_MINUS: string = require("bootstrap-icons/icons/calendar2-minus.svg");
const BS_ICON_CALENDAR2_MINUS_FILL: string = require("bootstrap-icons/icons/calendar2-minus-fill.svg");
const BS_ICON_CALENDAR2_MONTH: string = require("bootstrap-icons/icons/calendar2-month.svg");
const BS_ICON_CALENDAR2_MONTH_FILL: string = require("bootstrap-icons/icons/calendar2-month-fill.svg");
const BS_ICON_CALENDAR2_PLUS: string = require("bootstrap-icons/icons/calendar2-plus.svg");
const BS_ICON_CALENDAR2_PLUS_FILL: string = require("bootstrap-icons/icons/calendar2-plus-fill.svg");
const BS_ICON_CALENDAR2_RANGE: string = require("bootstrap-icons/icons/calendar2-range.svg");
const BS_ICON_CALENDAR2_RANGE_FILL: string = require("bootstrap-icons/icons/calendar2-range-fill.svg");
const BS_ICON_CALENDAR2_WEEK: string = require("bootstrap-icons/icons/calendar2-week.svg");
const BS_ICON_CALENDAR2_WEEK_FILL: string = require("bootstrap-icons/icons/calendar2-week-fill.svg");
const BS_ICON_CALENDAR2_X: string = require("bootstrap-icons/icons/calendar2-x.svg");
const BS_ICON_CALENDAR2_X_FILL: string = require("bootstrap-icons/icons/calendar2-x-fill.svg");
const BS_ICON_CALENDAR3: string = require("bootstrap-icons/icons/calendar3.svg");
const BS_ICON_CALENDAR3_EVENT: string = require("bootstrap-icons/icons/calendar3-event.svg");
const BS_ICON_CALENDAR3_EVENT_FILL: string = require("bootstrap-icons/icons/calendar3-event-fill.svg");
const BS_ICON_CALENDAR3_FILL: string = require("bootstrap-icons/icons/calendar3-fill.svg");
const BS_ICON_CALENDAR3_RANGE: string = require("bootstrap-icons/icons/calendar3-range.svg");
const BS_ICON_CALENDAR3_RANGE_FILL: string = require("bootstrap-icons/icons/calendar3-range-fill.svg");
const BS_ICON_CALENDAR3_WEEK: string = require("bootstrap-icons/icons/calendar3-week.svg");
const BS_ICON_CALENDAR3_WEEK_FILL: string = require("bootstrap-icons/icons/calendar3-week-fill.svg");
const BS_ICON_CALENDAR4: string = require("bootstrap-icons/icons/calendar4.svg");
const BS_ICON_CALENDAR4_EVENT: string = require("bootstrap-icons/icons/calendar4-event.svg");
const BS_ICON_CALENDAR4_RANGE: string = require("bootstrap-icons/icons/calendar4-range.svg");
const BS_ICON_CALENDAR4_WEEK: string = require("bootstrap-icons/icons/calendar4-week.svg");
const BS_ICON_CAMERA: string = require("bootstrap-icons/icons/camera.svg");
const BS_ICON_CAMERA_FILL: string = require("bootstrap-icons/icons/camera-fill.svg");
const BS_ICON_CAMERA_REELS: string = require("bootstrap-icons/icons/camera-reels.svg");
const BS_ICON_CAMERA_REELS_FILL: string = require("bootstrap-icons/icons/camera-reels-fill.svg");
const BS_ICON_CAMERA_VIDEO: string = require("bootstrap-icons/icons/camera-video.svg");
const BS_ICON_CAMERA_VIDEO_FILL: string = require("bootstrap-icons/icons/camera-video-fill.svg");
const BS_ICON_CAMERA_VIDEO_OFF: string = require("bootstrap-icons/icons/camera-video-off.svg");
const BS_ICON_CAMERA_VIDEO_OFF_FILL: string = require("bootstrap-icons/icons/camera-video-off-fill.svg");
const BS_ICON_CAMERA2: string = require("bootstrap-icons/icons/camera2.svg");
const BS_ICON_CAPSLOCK: string = require("bootstrap-icons/icons/capslock.svg");
const BS_ICON_CAPSLOCK_FILL: string = require("bootstrap-icons/icons/capslock-fill.svg");
const BS_ICON_CAPSULE: string = require("bootstrap-icons/icons/capsule.svg");
const BS_ICON_CAPSULE_PILL: string = require("bootstrap-icons/icons/capsule-pill.svg");
const BS_ICON_CAR_FRONT: string = require("bootstrap-icons/icons/car-front.svg");
const BS_ICON_CAR_FRONT_FILL: string = require("bootstrap-icons/icons/car-front-fill.svg");
const BS_ICON_CARD_CHECKLIST: string = require("bootstrap-icons/icons/card-checklist.svg");
const BS_ICON_CARD_HEADING: string = require("bootstrap-icons/icons/card-heading.svg");
const BS_ICON_CARD_IMAGE: string = require("bootstrap-icons/icons/card-image.svg");
const BS_ICON_CARD_LIST: string = require("bootstrap-icons/icons/card-list.svg");
const BS_ICON_CARD_TEXT: string = require("bootstrap-icons/icons/card-text.svg");
const BS_ICON_CARET_DOWN: string = require("bootstrap-icons/icons/caret-down.svg");
const BS_ICON_CARET_DOWN_FILL: string = require("bootstrap-icons/icons/caret-down-fill.svg");
const BS_ICON_CARET_DOWN_SQUARE: string = require("bootstrap-icons/icons/caret-down-square.svg");
const BS_ICON_CARET_DOWN_SQUARE_FILL: string = require("bootstrap-icons/icons/caret-down-square-fill.svg");
const BS_ICON_CARET_LEFT: string = require("bootstrap-icons/icons/caret-left.svg");
const BS_ICON_CARET_LEFT_FILL: string = require("bootstrap-icons/icons/caret-left-fill.svg");
const BS_ICON_CARET_LEFT_SQUARE: string = require("bootstrap-icons/icons/caret-left-square.svg");
const BS_ICON_CARET_LEFT_SQUARE_FILL: string = require("bootstrap-icons/icons/caret-left-square-fill.svg");
const BS_ICON_CARET_RIGHT: string = require("bootstrap-icons/icons/caret-right.svg");
const BS_ICON_CARET_RIGHT_FILL: string = require("bootstrap-icons/icons/caret-right-fill.svg");
const BS_ICON_CARET_RIGHT_SQUARE: string = require("bootstrap-icons/icons/caret-right-square.svg");
const BS_ICON_CARET_RIGHT_SQUARE_FILL: string = require("bootstrap-icons/icons/caret-right-square-fill.svg");
const BS_ICON_CARET_UP: string = require("bootstrap-icons/icons/caret-up.svg");
const BS_ICON_CARET_UP_FILL: string = require("bootstrap-icons/icons/caret-up-fill.svg");
const BS_ICON_CARET_UP_SQUARE: string = require("bootstrap-icons/icons/caret-up-square.svg");
const BS_ICON_CARET_UP_SQUARE_FILL: string = require("bootstrap-icons/icons/caret-up-square-fill.svg");
const BS_ICON_CART: string = require("bootstrap-icons/icons/cart.svg");
const BS_ICON_CART_CHECK: string = require("bootstrap-icons/icons/cart-check.svg");
const BS_ICON_CART_CHECK_FILL: string = require("bootstrap-icons/icons/cart-check-fill.svg");
const BS_ICON_CART_DASH: string = require("bootstrap-icons/icons/cart-dash.svg");
const BS_ICON_CART_DASH_FILL: string = require("bootstrap-icons/icons/cart-dash-fill.svg");
const BS_ICON_CART_FILL: string = require("bootstrap-icons/icons/cart-fill.svg");
const BS_ICON_CART_PLUS: string = require("bootstrap-icons/icons/cart-plus.svg");
const BS_ICON_CART_PLUS_FILL: string = require("bootstrap-icons/icons/cart-plus-fill.svg");
const BS_ICON_CART_X: string = require("bootstrap-icons/icons/cart-x.svg");
const BS_ICON_CART_X_FILL: string = require("bootstrap-icons/icons/cart-x-fill.svg");
const BS_ICON_CART2: string = require("bootstrap-icons/icons/cart2.svg");
const BS_ICON_CART3: string = require("bootstrap-icons/icons/cart3.svg");
const BS_ICON_CART4: string = require("bootstrap-icons/icons/cart4.svg");
const BS_ICON_CASH: string = require("bootstrap-icons/icons/cash.svg");
const BS_ICON_CASH_COIN: string = require("bootstrap-icons/icons/cash-coin.svg");
const BS_ICON_CASH_STACK: string = require("bootstrap-icons/icons/cash-stack.svg");
const BS_ICON_CASSETTE: string = require("bootstrap-icons/icons/cassette.svg");
const BS_ICON_CASSETTE_FILL: string = require("bootstrap-icons/icons/cassette-fill.svg");
const BS_ICON_CAST: string = require("bootstrap-icons/icons/cast.svg");
const BS_ICON_CC_CIRCLE: string = require("bootstrap-icons/icons/cc-circle.svg");
const BS_ICON_CC_CIRCLE_FILL: string = require("bootstrap-icons/icons/cc-circle-fill.svg");
const BS_ICON_CC_SQUARE: string = require("bootstrap-icons/icons/cc-square.svg");
const BS_ICON_CC_SQUARE_FILL: string = require("bootstrap-icons/icons/cc-square-fill.svg");
const BS_ICON_CHAT: string = require("bootstrap-icons/icons/chat.svg");
const BS_ICON_CHAT_DOTS: string = require("bootstrap-icons/icons/chat-dots.svg");
const BS_ICON_CHAT_DOTS_FILL: string = require("bootstrap-icons/icons/chat-dots-fill.svg");
const BS_ICON_CHAT_FILL: string = require("bootstrap-icons/icons/chat-fill.svg");
const BS_ICON_CHAT_HEART: string = require("bootstrap-icons/icons/chat-heart.svg");
const BS_ICON_CHAT_HEART_FILL: string = require("bootstrap-icons/icons/chat-heart-fill.svg");
const BS_ICON_CHAT_LEFT: string = require("bootstrap-icons/icons/chat-left.svg");
const BS_ICON_CHAT_LEFT_DOTS: string = require("bootstrap-icons/icons/chat-left-dots.svg");
const BS_ICON_CHAT_LEFT_DOTS_FILL: string = require("bootstrap-icons/icons/chat-left-dots-fill.svg");
const BS_ICON_CHAT_LEFT_FILL: string = require("bootstrap-icons/icons/chat-left-fill.svg");
const BS_ICON_CHAT_LEFT_HEART: string = require("bootstrap-icons/icons/chat-left-heart.svg");
const BS_ICON_CHAT_LEFT_HEART_FILL: string = require("bootstrap-icons/icons/chat-left-heart-fill.svg");
const BS_ICON_CHAT_LEFT_QUOTE: string = require("bootstrap-icons/icons/chat-left-quote.svg");
const BS_ICON_CHAT_LEFT_QUOTE_FILL: string = require("bootstrap-icons/icons/chat-left-quote-fill.svg");
const BS_ICON_CHAT_LEFT_TEXT: string = require("bootstrap-icons/icons/chat-left-text.svg");
const BS_ICON_CHAT_LEFT_TEXT_FILL: string = require("bootstrap-icons/icons/chat-left-text-fill.svg");
const BS_ICON_CHAT_QUOTE: string = require("bootstrap-icons/icons/chat-quote.svg");
const BS_ICON_CHAT_QUOTE_FILL: string = require("bootstrap-icons/icons/chat-quote-fill.svg");
const BS_ICON_CHAT_RIGHT: string = require("bootstrap-icons/icons/chat-right.svg");
const BS_ICON_CHAT_RIGHT_DOTS: string = require("bootstrap-icons/icons/chat-right-dots.svg");
const BS_ICON_CHAT_RIGHT_DOTS_FILL: string = require("bootstrap-icons/icons/chat-right-dots-fill.svg");
const BS_ICON_CHAT_RIGHT_FILL: string = require("bootstrap-icons/icons/chat-right-fill.svg");
const BS_ICON_CHAT_RIGHT_HEART: string = require("bootstrap-icons/icons/chat-right-heart.svg");
const BS_ICON_CHAT_RIGHT_HEART_FILL: string = require("bootstrap-icons/icons/chat-right-heart-fill.svg");
const BS_ICON_CHAT_RIGHT_QUOTE: string = require("bootstrap-icons/icons/chat-right-quote.svg");
const BS_ICON_CHAT_RIGHT_QUOTE_FILL: string = require("bootstrap-icons/icons/chat-right-quote-fill.svg");
const BS_ICON_CHAT_RIGHT_TEXT: string = require("bootstrap-icons/icons/chat-right-text.svg");
const BS_ICON_CHAT_RIGHT_TEXT_FILL: string = require("bootstrap-icons/icons/chat-right-text-fill.svg");
const BS_ICON_CHAT_SQUARE: string = require("bootstrap-icons/icons/chat-square.svg");
const BS_ICON_CHAT_SQUARE_DOTS: string = require("bootstrap-icons/icons/chat-square-dots.svg");
const BS_ICON_CHAT_SQUARE_DOTS_FILL: string = require("bootstrap-icons/icons/chat-square-dots-fill.svg");
const BS_ICON_CHAT_SQUARE_FILL: string = require("bootstrap-icons/icons/chat-square-fill.svg");
const BS_ICON_CHAT_SQUARE_HEART: string = require("bootstrap-icons/icons/chat-square-heart.svg");
const BS_ICON_CHAT_SQUARE_HEART_FILL: string = require("bootstrap-icons/icons/chat-square-heart-fill.svg");
const BS_ICON_CHAT_SQUARE_QUOTE: string = require("bootstrap-icons/icons/chat-square-quote.svg");
const BS_ICON_CHAT_SQUARE_QUOTE_FILL: string = require("bootstrap-icons/icons/chat-square-quote-fill.svg");
const BS_ICON_CHAT_SQUARE_TEXT: string = require("bootstrap-icons/icons/chat-square-text.svg");
const BS_ICON_CHAT_SQUARE_TEXT_FILL: string = require("bootstrap-icons/icons/chat-square-text-fill.svg");
const BS_ICON_CHAT_TEXT: string = require("bootstrap-icons/icons/chat-text.svg");
const BS_ICON_CHAT_TEXT_FILL: string = require("bootstrap-icons/icons/chat-text-fill.svg");
const BS_ICON_CHECK: string = require("bootstrap-icons/icons/check.svg");
const BS_ICON_CHECK_ALL: string = require("bootstrap-icons/icons/check-all.svg");
const BS_ICON_CHECK_CIRCLE: string = require("bootstrap-icons/icons/check-circle.svg");
const BS_ICON_CHECK_CIRCLE_FILL: string = require("bootstrap-icons/icons/check-circle-fill.svg");
const BS_ICON_CHECK_LG: string = require("bootstrap-icons/icons/check-lg.svg");
const BS_ICON_CHECK_SQUARE: string = require("bootstrap-icons/icons/check-square.svg");
const BS_ICON_CHECK_SQUARE_FILL: string = require("bootstrap-icons/icons/check-square-fill.svg");
const BS_ICON_CHECK2: string = require("bootstrap-icons/icons/check2.svg");
const BS_ICON_CHECK2_ALL: string = require("bootstrap-icons/icons/check2-all.svg");
const BS_ICON_CHECK2_CIRCLE: string = require("bootstrap-icons/icons/check2-circle.svg");
const BS_ICON_CHECK2_SQUARE: string = require("bootstrap-icons/icons/check2-square.svg");
const BS_ICON_CHEVRON_BAR_CONTRACT: string = require("bootstrap-icons/icons/chevron-bar-contract.svg");
const BS_ICON_CHEVRON_BAR_DOWN: string = require("bootstrap-icons/icons/chevron-bar-down.svg");
const BS_ICON_CHEVRON_BAR_EXPAND: string = require("bootstrap-icons/icons/chevron-bar-expand.svg");
const BS_ICON_CHEVRON_BAR_LEFT: string = require("bootstrap-icons/icons/chevron-bar-left.svg");
const BS_ICON_CHEVRON_BAR_RIGHT: string = require("bootstrap-icons/icons/chevron-bar-right.svg");
const BS_ICON_CHEVRON_BAR_UP: string = require("bootstrap-icons/icons/chevron-bar-up.svg");
const BS_ICON_CHEVRON_COMPACT_DOWN: string = require("bootstrap-icons/icons/chevron-compact-down.svg");
const BS_ICON_CHEVRON_COMPACT_LEFT: string = require("bootstrap-icons/icons/chevron-compact-left.svg");
const BS_ICON_CHEVRON_COMPACT_RIGHT: string = require("bootstrap-icons/icons/chevron-compact-right.svg");
const BS_ICON_CHEVRON_COMPACT_UP: string = require("bootstrap-icons/icons/chevron-compact-up.svg");
const BS_ICON_CHEVRON_CONTRACT: string = require("bootstrap-icons/icons/chevron-contract.svg");
const BS_ICON_CHEVRON_DOUBLE_DOWN: string = require("bootstrap-icons/icons/chevron-double-down.svg");
const BS_ICON_CHEVRON_DOUBLE_LEFT: string = require("bootstrap-icons/icons/chevron-double-left.svg");
const BS_ICON_CHEVRON_DOUBLE_RIGHT: string = require("bootstrap-icons/icons/chevron-double-right.svg");
const BS_ICON_CHEVRON_DOUBLE_UP: string = require("bootstrap-icons/icons/chevron-double-up.svg");
const BS_ICON_CHEVRON_DOWN: string = require("bootstrap-icons/icons/chevron-down.svg");
const BS_ICON_CHEVRON_EXPAND: string = require("bootstrap-icons/icons/chevron-expand.svg");
const BS_ICON_CHEVRON_LEFT: string = require("bootstrap-icons/icons/chevron-left.svg");
const BS_ICON_CHEVRON_RIGHT: string = require("bootstrap-icons/icons/chevron-right.svg");
const BS_ICON_CHEVRON_UP: string = require("bootstrap-icons/icons/chevron-up.svg");
const BS_ICON_CIRCLE: string = require("bootstrap-icons/icons/circle.svg");
const BS_ICON_CIRCLE_FILL: string = require("bootstrap-icons/icons/circle-fill.svg");
const BS_ICON_CIRCLE_HALF: string = require("bootstrap-icons/icons/circle-half.svg");
const BS_ICON_CIRCLE_SQUARE: string = require("bootstrap-icons/icons/circle-square.svg");
const BS_ICON_CLIPBOARD: string = require("bootstrap-icons/icons/clipboard.svg");
const BS_ICON_CLIPBOARD_CHECK: string = require("bootstrap-icons/icons/clipboard-check.svg");
const BS_ICON_CLIPBOARD_CHECK_FILL: string = require("bootstrap-icons/icons/clipboard-check-fill.svg");
const BS_ICON_CLIPBOARD_DATA: string = require("bootstrap-icons/icons/clipboard-data.svg");
const BS_ICON_CLIPBOARD_DATA_FILL: string = require("bootstrap-icons/icons/clipboard-data-fill.svg");
const BS_ICON_CLIPBOARD_FILL: string = require("bootstrap-icons/icons/clipboard-fill.svg");
const BS_ICON_CLIPBOARD_HEART: string = require("bootstrap-icons/icons/clipboard-heart.svg");
const BS_ICON_CLIPBOARD_HEART_FILL: string = require("bootstrap-icons/icons/clipboard-heart-fill.svg");
const BS_ICON_CLIPBOARD_MINUS: string = require("bootstrap-icons/icons/clipboard-minus.svg");
const BS_ICON_CLIPBOARD_MINUS_FILL: string = require("bootstrap-icons/icons/clipboard-minus-fill.svg");
const BS_ICON_CLIPBOARD_PLUS: string = require("bootstrap-icons/icons/clipboard-plus.svg");
const BS_ICON_CLIPBOARD_PLUS_FILL: string = require("bootstrap-icons/icons/clipboard-plus-fill.svg");
const BS_ICON_CLIPBOARD_PULSE: string = require("bootstrap-icons/icons/clipboard-pulse.svg");
const BS_ICON_CLIPBOARD_X: string = require("bootstrap-icons/icons/clipboard-x.svg");
const BS_ICON_CLIPBOARD_X_FILL: string = require("bootstrap-icons/icons/clipboard-x-fill.svg");
const BS_ICON_CLIPBOARD2: string = require("bootstrap-icons/icons/clipboard2.svg");
const BS_ICON_CLIPBOARD2_CHECK: string = require("bootstrap-icons/icons/clipboard2-check.svg");
const BS_ICON_CLIPBOARD2_CHECK_FILL: string = require("bootstrap-icons/icons/clipboard2-check-fill.svg");
const BS_ICON_CLIPBOARD2_DATA: string = require("bootstrap-icons/icons/clipboard2-data.svg");
const BS_ICON_CLIPBOARD2_DATA_FILL: string = require("bootstrap-icons/icons/clipboard2-data-fill.svg");
const BS_ICON_CLIPBOARD2_FILL: string = require("bootstrap-icons/icons/clipboard2-fill.svg");
const BS_ICON_CLIPBOARD2_HEART: string = require("bootstrap-icons/icons/clipboard2-heart.svg");
const BS_ICON_CLIPBOARD2_HEART_FILL: string = require("bootstrap-icons/icons/clipboard2-heart-fill.svg");
const BS_ICON_CLIPBOARD2_MINUS: string = require("bootstrap-icons/icons/clipboard2-minus.svg");
const BS_ICON_CLIPBOARD2_MINUS_FILL: string = require("bootstrap-icons/icons/clipboard2-minus-fill.svg");
const BS_ICON_CLIPBOARD2_PLUS: string = require("bootstrap-icons/icons/clipboard2-plus.svg");
const BS_ICON_CLIPBOARD2_PLUS_FILL: string = require("bootstrap-icons/icons/clipboard2-plus-fill.svg");
const BS_ICON_CLIPBOARD2_PULSE: string = require("bootstrap-icons/icons/clipboard2-pulse.svg");
const BS_ICON_CLIPBOARD2_PULSE_FILL: string = require("bootstrap-icons/icons/clipboard2-pulse-fill.svg");
const BS_ICON_CLIPBOARD2_X: string = require("bootstrap-icons/icons/clipboard2-x.svg");
const BS_ICON_CLIPBOARD2_X_FILL: string = require("bootstrap-icons/icons/clipboard2-x-fill.svg");
const BS_ICON_CLOCK: string = require("bootstrap-icons/icons/clock.svg");
const BS_ICON_CLOCK_FILL: string = require("bootstrap-icons/icons/clock-fill.svg");
const BS_ICON_CLOCK_HISTORY: string = require("bootstrap-icons/icons/clock-history.svg");
const BS_ICON_CLOUD: string = require("bootstrap-icons/icons/cloud.svg");
const BS_ICON_CLOUD_ARROW_DOWN: string = require("bootstrap-icons/icons/cloud-arrow-down.svg");
const BS_ICON_CLOUD_ARROW_DOWN_FILL: string = require("bootstrap-icons/icons/cloud-arrow-down-fill.svg");
const BS_ICON_CLOUD_ARROW_UP: string = require("bootstrap-icons/icons/cloud-arrow-up.svg");
const BS_ICON_CLOUD_ARROW_UP_FILL: string = require("bootstrap-icons/icons/cloud-arrow-up-fill.svg");
const BS_ICON_CLOUD_CHECK: string = require("bootstrap-icons/icons/cloud-check.svg");
const BS_ICON_CLOUD_CHECK_FILL: string = require("bootstrap-icons/icons/cloud-check-fill.svg");
const BS_ICON_CLOUD_DOWNLOAD: string = require("bootstrap-icons/icons/cloud-download.svg");
const BS_ICON_CLOUD_DOWNLOAD_FILL: string = require("bootstrap-icons/icons/cloud-download-fill.svg");
const BS_ICON_CLOUD_DRIZZLE: string = require("bootstrap-icons/icons/cloud-drizzle.svg");
const BS_ICON_CLOUD_DRIZZLE_FILL: string = require("bootstrap-icons/icons/cloud-drizzle-fill.svg");
const BS_ICON_CLOUD_FILL: string = require("bootstrap-icons/icons/cloud-fill.svg");
const BS_ICON_CLOUD_FOG: string = require("bootstrap-icons/icons/cloud-fog.svg");
const BS_ICON_CLOUD_FOG_FILL: string = require("bootstrap-icons/icons/cloud-fog-fill.svg");
const BS_ICON_CLOUD_FOG2: string = require("bootstrap-icons/icons/cloud-fog2.svg");
const BS_ICON_CLOUD_FOG2_FILL: string = require("bootstrap-icons/icons/cloud-fog2-fill.svg");
const BS_ICON_CLOUD_HAIL: string = require("bootstrap-icons/icons/cloud-hail.svg");
const BS_ICON_CLOUD_HAIL_FILL: string = require("bootstrap-icons/icons/cloud-hail-fill.svg");
const BS_ICON_CLOUD_HAZE: string = require("bootstrap-icons/icons/cloud-haze.svg");
const BS_ICON_CLOUD_HAZE_FILL: string = require("bootstrap-icons/icons/cloud-haze-fill.svg");
const BS_ICON_CLOUD_HAZE2: string = require("bootstrap-icons/icons/cloud-haze2.svg");
const BS_ICON_CLOUD_HAZE2_FILL: string = require("bootstrap-icons/icons/cloud-haze2-fill.svg");
const BS_ICON_CLOUD_LIGHTNING: string = require("bootstrap-icons/icons/cloud-lightning.svg");
const BS_ICON_CLOUD_LIGHTNING_FILL: string = require("bootstrap-icons/icons/cloud-lightning-fill.svg");
const BS_ICON_CLOUD_LIGHTNING_RAIN: string = require("bootstrap-icons/icons/cloud-lightning-rain.svg");
const BS_ICON_CLOUD_LIGHTNING_RAIN_FILL: string = require("bootstrap-icons/icons/cloud-lightning-rain-fill.svg");
const BS_ICON_CLOUD_MINUS: string = require("bootstrap-icons/icons/cloud-minus.svg");
const BS_ICON_CLOUD_MINUS_FILL: string = require("bootstrap-icons/icons/cloud-minus-fill.svg");
const BS_ICON_CLOUD_MOON: string = require("bootstrap-icons/icons/cloud-moon.svg");
const BS_ICON_CLOUD_MOON_FILL: string = require("bootstrap-icons/icons/cloud-moon-fill.svg");
const BS_ICON_CLOUD_PLUS: string = require("bootstrap-icons/icons/cloud-plus.svg");
const BS_ICON_CLOUD_PLUS_FILL: string = require("bootstrap-icons/icons/cloud-plus-fill.svg");
const BS_ICON_CLOUD_RAIN: string = require("bootstrap-icons/icons/cloud-rain.svg");
const BS_ICON_CLOUD_RAIN_FILL: string = require("bootstrap-icons/icons/cloud-rain-fill.svg");
const BS_ICON_CLOUD_RAIN_HEAVY: string = require("bootstrap-icons/icons/cloud-rain-heavy.svg");
const BS_ICON_CLOUD_RAIN_HEAVY_FILL: string = require("bootstrap-icons/icons/cloud-rain-heavy-fill.svg");
const BS_ICON_CLOUD_SLASH: string = require("bootstrap-icons/icons/cloud-slash.svg");
const BS_ICON_CLOUD_SLASH_FILL: string = require("bootstrap-icons/icons/cloud-slash-fill.svg");
const BS_ICON_CLOUD_SLEET: string = require("bootstrap-icons/icons/cloud-sleet.svg");
const BS_ICON_CLOUD_SLEET_FILL: string = require("bootstrap-icons/icons/cloud-sleet-fill.svg");
const BS_ICON_CLOUD_SNOW: string = require("bootstrap-icons/icons/cloud-snow.svg");
const BS_ICON_CLOUD_SNOW_FILL: string = require("bootstrap-icons/icons/cloud-snow-fill.svg");
const BS_ICON_CLOUD_SUN: string = require("bootstrap-icons/icons/cloud-sun.svg");
const BS_ICON_CLOUD_SUN_FILL: string = require("bootstrap-icons/icons/cloud-sun-fill.svg");
const BS_ICON_CLOUD_UPLOAD: string = require("bootstrap-icons/icons/cloud-upload.svg");
const BS_ICON_CLOUD_UPLOAD_FILL: string = require("bootstrap-icons/icons/cloud-upload-fill.svg");
const BS_ICON_CLOUDS: string = require("bootstrap-icons/icons/clouds.svg");
const BS_ICON_CLOUDS_FILL: string = require("bootstrap-icons/icons/clouds-fill.svg");
const BS_ICON_CLOUDY: string = require("bootstrap-icons/icons/cloudy.svg");
const BS_ICON_CLOUDY_FILL: string = require("bootstrap-icons/icons/cloudy-fill.svg");
const BS_ICON_CODE: string = require("bootstrap-icons/icons/code.svg");
const BS_ICON_CODE_SLASH: string = require("bootstrap-icons/icons/code-slash.svg");
const BS_ICON_CODE_SQUARE: string = require("bootstrap-icons/icons/code-square.svg");
const BS_ICON_COIN: string = require("bootstrap-icons/icons/coin.svg");
const BS_ICON_COLLECTION: string = require("bootstrap-icons/icons/collection.svg");
const BS_ICON_COLLECTION_FILL: string = require("bootstrap-icons/icons/collection-fill.svg");
const BS_ICON_COLLECTION_PLAY: string = require("bootstrap-icons/icons/collection-play.svg");
const BS_ICON_COLLECTION_PLAY_FILL: string = require("bootstrap-icons/icons/collection-play-fill.svg");
const BS_ICON_COLUMNS: string = require("bootstrap-icons/icons/columns.svg");
const BS_ICON_COLUMNS_GAP: string = require("bootstrap-icons/icons/columns-gap.svg");
const BS_ICON_COMMAND: string = require("bootstrap-icons/icons/command.svg");
const BS_ICON_COMPASS: string = require("bootstrap-icons/icons/compass.svg");
const BS_ICON_COMPASS_FILL: string = require("bootstrap-icons/icons/compass-fill.svg");
const BS_ICON_CONE: string = require("bootstrap-icons/icons/cone.svg");
const BS_ICON_CONE_STRIPED: string = require("bootstrap-icons/icons/cone-striped.svg");
const BS_ICON_CONTROLLER: string = require("bootstrap-icons/icons/controller.svg");
const BS_ICON_COOKIE: string = require("bootstrap-icons/icons/cookie.svg");
const BS_ICON_COPY: string = require("bootstrap-icons/icons/copy.svg");
const BS_ICON_CPU: string = require("bootstrap-icons/icons/cpu.svg");
const BS_ICON_CPU_FILL: string = require("bootstrap-icons/icons/cpu-fill.svg");
const BS_ICON_CREDIT_CARD: string = require("bootstrap-icons/icons/credit-card.svg");
const BS_ICON_CREDIT_CARD_2_BACK: string = require("bootstrap-icons/icons/credit-card-2-back.svg");
const BS_ICON_CREDIT_CARD_2_BACK_FILL: string = require("bootstrap-icons/icons/credit-card-2-back-fill.svg");
const BS_ICON_CREDIT_CARD_2_FRONT: string = require("bootstrap-icons/icons/credit-card-2-front.svg");
const BS_ICON_CREDIT_CARD_2_FRONT_FILL: string = require("bootstrap-icons/icons/credit-card-2-front-fill.svg");
const BS_ICON_CREDIT_CARD_FILL: string = require("bootstrap-icons/icons/credit-card-fill.svg");
const BS_ICON_CROP: string = require("bootstrap-icons/icons/crop.svg");
const BS_ICON_CROSSHAIR: string = require("bootstrap-icons/icons/crosshair.svg");
const BS_ICON_CROSSHAIR2: string = require("bootstrap-icons/icons/crosshair2.svg");
const BS_ICON_CUP: string = require("bootstrap-icons/icons/cup.svg");
const BS_ICON_CUP_FILL: string = require("bootstrap-icons/icons/cup-fill.svg");
const BS_ICON_CUP_HOT: string = require("bootstrap-icons/icons/cup-hot.svg");
const BS_ICON_CUP_HOT_FILL: string = require("bootstrap-icons/icons/cup-hot-fill.svg");
const BS_ICON_CUP_STRAW: string = require("bootstrap-icons/icons/cup-straw.svg");
const BS_ICON_CURRENCY_BITCOIN: string = require("bootstrap-icons/icons/currency-bitcoin.svg");
const BS_ICON_CURRENCY_DOLLAR: string = require("bootstrap-icons/icons/currency-dollar.svg");
const BS_ICON_CURRENCY_EURO: string = require("bootstrap-icons/icons/currency-euro.svg");
const BS_ICON_CURRENCY_EXCHANGE: string = require("bootstrap-icons/icons/currency-exchange.svg");
const BS_ICON_CURRENCY_POUND: string = require("bootstrap-icons/icons/currency-pound.svg");
const BS_ICON_CURRENCY_RUPEE: string = require("bootstrap-icons/icons/currency-rupee.svg");
const BS_ICON_CURRENCY_YEN: string = require("bootstrap-icons/icons/currency-yen.svg");
const BS_ICON_CURSOR: string = require("bootstrap-icons/icons/cursor.svg");
const BS_ICON_CURSOR_FILL: string = require("bootstrap-icons/icons/cursor-fill.svg");
const BS_ICON_CURSOR_TEXT: string = require("bootstrap-icons/icons/cursor-text.svg");
const BS_ICON_DASH: string = require("bootstrap-icons/icons/dash.svg");
const BS_ICON_DASH_CIRCLE: string = require("bootstrap-icons/icons/dash-circle.svg");
const BS_ICON_DASH_CIRCLE_DOTTED: string = require("bootstrap-icons/icons/dash-circle-dotted.svg");
const BS_ICON_DASH_CIRCLE_FILL: string = require("bootstrap-icons/icons/dash-circle-fill.svg");
const BS_ICON_DASH_LG: string = require("bootstrap-icons/icons/dash-lg.svg");
const BS_ICON_DASH_SQUARE: string = require("bootstrap-icons/icons/dash-square.svg");
const BS_ICON_DASH_SQUARE_DOTTED: string = require("bootstrap-icons/icons/dash-square-dotted.svg");
const BS_ICON_DASH_SQUARE_FILL: string = require("bootstrap-icons/icons/dash-square-fill.svg");
const BS_ICON_DATABASE: string = require("bootstrap-icons/icons/database.svg");
const BS_ICON_DATABASE_ADD: string = require("bootstrap-icons/icons/database-add.svg");
const BS_ICON_DATABASE_CHECK: string = require("bootstrap-icons/icons/database-check.svg");
const BS_ICON_DATABASE_DASH: string = require("bootstrap-icons/icons/database-dash.svg");
const BS_ICON_DATABASE_DOWN: string = require("bootstrap-icons/icons/database-down.svg");
const BS_ICON_DATABASE_EXCLAMATION: string = require("bootstrap-icons/icons/database-exclamation.svg");
const BS_ICON_DATABASE_FILL: string = require("bootstrap-icons/icons/database-fill.svg");
const BS_ICON_DATABASE_FILL_ADD: string = require("bootstrap-icons/icons/database-fill-add.svg");
const BS_ICON_DATABASE_FILL_CHECK: string = require("bootstrap-icons/icons/database-fill-check.svg");
const BS_ICON_DATABASE_FILL_DASH: string = require("bootstrap-icons/icons/database-fill-dash.svg");
const BS_ICON_DATABASE_FILL_DOWN: string = require("bootstrap-icons/icons/database-fill-down.svg");
const BS_ICON_DATABASE_FILL_EXCLAMATION: string = require("bootstrap-icons/icons/database-fill-exclamation.svg");
const BS_ICON_DATABASE_FILL_GEAR: string = require("bootstrap-icons/icons/database-fill-gear.svg");
const BS_ICON_DATABASE_FILL_LOCK: string = require("bootstrap-icons/icons/database-fill-lock.svg");
const BS_ICON_DATABASE_FILL_SLASH: string = require("bootstrap-icons/icons/database-fill-slash.svg");
const BS_ICON_DATABASE_FILL_UP: string = require("bootstrap-icons/icons/database-fill-up.svg");
const BS_ICON_DATABASE_FILL_X: string = require("bootstrap-icons/icons/database-fill-x.svg");
const BS_ICON_DATABASE_GEAR: string = require("bootstrap-icons/icons/database-gear.svg");
const BS_ICON_DATABASE_LOCK: string = require("bootstrap-icons/icons/database-lock.svg");
const BS_ICON_DATABASE_SLASH: string = require("bootstrap-icons/icons/database-slash.svg");
const BS_ICON_DATABASE_UP: string = require("bootstrap-icons/icons/database-up.svg");
const BS_ICON_DATABASE_X: string = require("bootstrap-icons/icons/database-x.svg");
const BS_ICON_DEVICE_HDD: string = require("bootstrap-icons/icons/device-hdd.svg");
const BS_ICON_DEVICE_HDD_FILL: string = require("bootstrap-icons/icons/device-hdd-fill.svg");
const BS_ICON_DEVICE_SSD: string = require("bootstrap-icons/icons/device-ssd.svg");
const BS_ICON_DEVICE_SSD_FILL: string = require("bootstrap-icons/icons/device-ssd-fill.svg");
const BS_ICON_DIAGRAM_2: string = require("bootstrap-icons/icons/diagram-2.svg");
const BS_ICON_DIAGRAM_2_FILL: string = require("bootstrap-icons/icons/diagram-2-fill.svg");
const BS_ICON_DIAGRAM_3: string = require("bootstrap-icons/icons/diagram-3.svg");
const BS_ICON_DIAGRAM_3_FILL: string = require("bootstrap-icons/icons/diagram-3-fill.svg");
const BS_ICON_DIAMOND: string = require("bootstrap-icons/icons/diamond.svg");
const BS_ICON_DIAMOND_FILL: string = require("bootstrap-icons/icons/diamond-fill.svg");
const BS_ICON_DIAMOND_HALF: string = require("bootstrap-icons/icons/diamond-half.svg");
const BS_ICON_DICE_1: string = require("bootstrap-icons/icons/dice-1.svg");
const BS_ICON_DICE_1_FILL: string = require("bootstrap-icons/icons/dice-1-fill.svg");
const BS_ICON_DICE_2: string = require("bootstrap-icons/icons/dice-2.svg");
const BS_ICON_DICE_2_FILL: string = require("bootstrap-icons/icons/dice-2-fill.svg");
const BS_ICON_DICE_3: string = require("bootstrap-icons/icons/dice-3.svg");
const BS_ICON_DICE_3_FILL: string = require("bootstrap-icons/icons/dice-3-fill.svg");
const BS_ICON_DICE_4: string = require("bootstrap-icons/icons/dice-4.svg");
const BS_ICON_DICE_4_FILL: string = require("bootstrap-icons/icons/dice-4-fill.svg");
const BS_ICON_DICE_5: string = require("bootstrap-icons/icons/dice-5.svg");
const BS_ICON_DICE_5_FILL: string = require("bootstrap-icons/icons/dice-5-fill.svg");
const BS_ICON_DICE_6: string = require("bootstrap-icons/icons/dice-6.svg");
const BS_ICON_DICE_6_FILL: string = require("bootstrap-icons/icons/dice-6-fill.svg");
const BS_ICON_DISC: string = require("bootstrap-icons/icons/disc.svg");
const BS_ICON_DISC_FILL: string = require("bootstrap-icons/icons/disc-fill.svg");
const BS_ICON_DISCORD: string = require("bootstrap-icons/icons/discord.svg");
const BS_ICON_DISPLAY: string = require("bootstrap-icons/icons/display.svg");
const BS_ICON_DISPLAY_FILL: string = require("bootstrap-icons/icons/display-fill.svg");
const BS_ICON_DISPLAYPORT: string = require("bootstrap-icons/icons/displayport.svg");
const BS_ICON_DISPLAYPORT_FILL: string = require("bootstrap-icons/icons/displayport-fill.svg");
const BS_ICON_DISTRIBUTE_HORIZONTAL: string = require("bootstrap-icons/icons/distribute-horizontal.svg");
const BS_ICON_DISTRIBUTE_VERTICAL: string = require("bootstrap-icons/icons/distribute-vertical.svg");
const BS_ICON_DOOR_CLOSED: string = require("bootstrap-icons/icons/door-closed.svg");
const BS_ICON_DOOR_CLOSED_FILL: string = require("bootstrap-icons/icons/door-closed-fill.svg");
const BS_ICON_DOOR_OPEN: string = require("bootstrap-icons/icons/door-open.svg");
const BS_ICON_DOOR_OPEN_FILL: string = require("bootstrap-icons/icons/door-open-fill.svg");
const BS_ICON_DOT: string = require("bootstrap-icons/icons/dot.svg");
const BS_ICON_DOWNLOAD: string = require("bootstrap-icons/icons/download.svg");
const BS_ICON_DPAD: string = require("bootstrap-icons/icons/dpad.svg");
const BS_ICON_DPAD_FILL: string = require("bootstrap-icons/icons/dpad-fill.svg");
const BS_ICON_DRIBBBLE: string = require("bootstrap-icons/icons/dribbble.svg");
const BS_ICON_DROPBOX: string = require("bootstrap-icons/icons/dropbox.svg");
const BS_ICON_DROPLET: string = require("bootstrap-icons/icons/droplet.svg");
const BS_ICON_DROPLET_FILL: string = require("bootstrap-icons/icons/droplet-fill.svg");
const BS_ICON_DROPLET_HALF: string = require("bootstrap-icons/icons/droplet-half.svg");
const BS_ICON_DUFFLE: string = require("bootstrap-icons/icons/duffle.svg");
const BS_ICON_DUFFLE_FILL: string = require("bootstrap-icons/icons/duffle-fill.svg");
const BS_ICON_EAR: string = require("bootstrap-icons/icons/ear.svg");
const BS_ICON_EAR_FILL: string = require("bootstrap-icons/icons/ear-fill.svg");
const BS_ICON_EARBUDS: string = require("bootstrap-icons/icons/earbuds.svg");
const BS_ICON_EASEL: string = require("bootstrap-icons/icons/easel.svg");
const BS_ICON_EASEL_FILL: string = require("bootstrap-icons/icons/easel-fill.svg");
const BS_ICON_EASEL2: string = require("bootstrap-icons/icons/easel2.svg");
const BS_ICON_EASEL2_FILL: string = require("bootstrap-icons/icons/easel2-fill.svg");
const BS_ICON_EASEL3: string = require("bootstrap-icons/icons/easel3.svg");
const BS_ICON_EASEL3_FILL: string = require("bootstrap-icons/icons/easel3-fill.svg");
const BS_ICON_EGG: string = require("bootstrap-icons/icons/egg.svg");
const BS_ICON_EGG_FILL: string = require("bootstrap-icons/icons/egg-fill.svg");
const BS_ICON_EGG_FRIED: string = require("bootstrap-icons/icons/egg-fried.svg");
const BS_ICON_EJECT: string = require("bootstrap-icons/icons/eject.svg");
const BS_ICON_EJECT_FILL: string = require("bootstrap-icons/icons/eject-fill.svg");
const BS_ICON_EMOJI_ANGRY: string = require("bootstrap-icons/icons/emoji-angry.svg");
const BS_ICON_EMOJI_ANGRY_FILL: string = require("bootstrap-icons/icons/emoji-angry-fill.svg");
const BS_ICON_EMOJI_ASTONISHED: string = require("bootstrap-icons/icons/emoji-astonished.svg");
const BS_ICON_EMOJI_ASTONISHED_FILL: string = require("bootstrap-icons/icons/emoji-astonished-fill.svg");
const BS_ICON_EMOJI_DIZZY: string = require("bootstrap-icons/icons/emoji-dizzy.svg");
const BS_ICON_EMOJI_DIZZY_FILL: string = require("bootstrap-icons/icons/emoji-dizzy-fill.svg");
const BS_ICON_EMOJI_EXPRESSIONLESS: string = require("bootstrap-icons/icons/emoji-expressionless.svg");
const BS_ICON_EMOJI_EXPRESSIONLESS_FILL: string = require("bootstrap-icons/icons/emoji-expressionless-fill.svg");
const BS_ICON_EMOJI_FROWN: string = require("bootstrap-icons/icons/emoji-frown.svg");
const BS_ICON_EMOJI_FROWN_FILL: string = require("bootstrap-icons/icons/emoji-frown-fill.svg");
const BS_ICON_EMOJI_GRIMACE: string = require("bootstrap-icons/icons/emoji-grimace.svg");
const BS_ICON_EMOJI_GRIMACE_FILL: string = require("bootstrap-icons/icons/emoji-grimace-fill.svg");
const BS_ICON_EMOJI_GRIN: string = require("bootstrap-icons/icons/emoji-grin.svg");
const BS_ICON_EMOJI_GRIN_FILL: string = require("bootstrap-icons/icons/emoji-grin-fill.svg");
const BS_ICON_EMOJI_HEART_EYES: string = require("bootstrap-icons/icons/emoji-heart-eyes.svg");
const BS_ICON_EMOJI_HEART_EYES_FILL: string = require("bootstrap-icons/icons/emoji-heart-eyes-fill.svg");
const BS_ICON_EMOJI_KISS: string = require("bootstrap-icons/icons/emoji-kiss.svg");
const BS_ICON_EMOJI_KISS_FILL: string = require("bootstrap-icons/icons/emoji-kiss-fill.svg");
const BS_ICON_EMOJI_LAUGHING: string = require("bootstrap-icons/icons/emoji-laughing.svg");
const BS_ICON_EMOJI_LAUGHING_FILL: string = require("bootstrap-icons/icons/emoji-laughing-fill.svg");
const BS_ICON_EMOJI_NEUTRAL: string = require("bootstrap-icons/icons/emoji-neutral.svg");
const BS_ICON_EMOJI_NEUTRAL_FILL: string = require("bootstrap-icons/icons/emoji-neutral-fill.svg");
const BS_ICON_EMOJI_SMILE: string = require("bootstrap-icons/icons/emoji-smile.svg");
const BS_ICON_EMOJI_SMILE_FILL: string = require("bootstrap-icons/icons/emoji-smile-fill.svg");
const BS_ICON_EMOJI_SMILE_UPSIDE_DOWN: string = require("bootstrap-icons/icons/emoji-smile-upside-down.svg");
const BS_ICON_EMOJI_SMILE_UPSIDE_DOWN_FILL: string = require("bootstrap-icons/icons/emoji-smile-upside-down-fill.svg");
const BS_ICON_EMOJI_SUNGLASSES: string = require("bootstrap-icons/icons/emoji-sunglasses.svg");
const BS_ICON_EMOJI_SUNGLASSES_FILL: string = require("bootstrap-icons/icons/emoji-sunglasses-fill.svg");
const BS_ICON_EMOJI_SURPRISE: string = require("bootstrap-icons/icons/emoji-surprise.svg");
const BS_ICON_EMOJI_SURPRISE_FILL: string = require("bootstrap-icons/icons/emoji-surprise-fill.svg");
const BS_ICON_EMOJI_TEAR: string = require("bootstrap-icons/icons/emoji-tear.svg");
const BS_ICON_EMOJI_TEAR_FILL: string = require("bootstrap-icons/icons/emoji-tear-fill.svg");
const BS_ICON_EMOJI_WINK: string = require("bootstrap-icons/icons/emoji-wink.svg");
const BS_ICON_EMOJI_WINK_FILL: string = require("bootstrap-icons/icons/emoji-wink-fill.svg");
const BS_ICON_ENVELOPE: string = require("bootstrap-icons/icons/envelope.svg");
const BS_ICON_ENVELOPE_ARROW_DOWN: string = require("bootstrap-icons/icons/envelope-arrow-down.svg");
const BS_ICON_ENVELOPE_ARROW_DOWN_FILL: string = require("bootstrap-icons/icons/envelope-arrow-down-fill.svg");
const BS_ICON_ENVELOPE_ARROW_UP: string = require("bootstrap-icons/icons/envelope-arrow-up.svg");
const BS_ICON_ENVELOPE_ARROW_UP_FILL: string = require("bootstrap-icons/icons/envelope-arrow-up-fill.svg");
const BS_ICON_ENVELOPE_AT: string = require("bootstrap-icons/icons/envelope-at.svg");
const BS_ICON_ENVELOPE_AT_FILL: string = require("bootstrap-icons/icons/envelope-at-fill.svg");
const BS_ICON_ENVELOPE_CHECK: string = require("bootstrap-icons/icons/envelope-check.svg");
const BS_ICON_ENVELOPE_CHECK_FILL: string = require("bootstrap-icons/icons/envelope-check-fill.svg");
const BS_ICON_ENVELOPE_DASH: string = require("bootstrap-icons/icons/envelope-dash.svg");
const BS_ICON_ENVELOPE_DASH_FILL: string = require("bootstrap-icons/icons/envelope-dash-fill.svg");
const BS_ICON_ENVELOPE_EXCLAMATION: string = require("bootstrap-icons/icons/envelope-exclamation.svg");
const BS_ICON_ENVELOPE_EXCLAMATION_FILL: string = require("bootstrap-icons/icons/envelope-exclamation-fill.svg");
const BS_ICON_ENVELOPE_FILL: string = require("bootstrap-icons/icons/envelope-fill.svg");
const BS_ICON_ENVELOPE_HEART: string = require("bootstrap-icons/icons/envelope-heart.svg");
const BS_ICON_ENVELOPE_HEART_FILL: string = require("bootstrap-icons/icons/envelope-heart-fill.svg");
const BS_ICON_ENVELOPE_OPEN: string = require("bootstrap-icons/icons/envelope-open.svg");
const BS_ICON_ENVELOPE_OPEN_FILL: string = require("bootstrap-icons/icons/envelope-open-fill.svg");
const BS_ICON_ENVELOPE_OPEN_HEART: string = require("bootstrap-icons/icons/envelope-open-heart.svg");
const BS_ICON_ENVELOPE_OPEN_HEART_FILL: string = require("bootstrap-icons/icons/envelope-open-heart-fill.svg");
const BS_ICON_ENVELOPE_PAPER: string = require("bootstrap-icons/icons/envelope-paper.svg");
const BS_ICON_ENVELOPE_PAPER_FILL: string = require("bootstrap-icons/icons/envelope-paper-fill.svg");
const BS_ICON_ENVELOPE_PAPER_HEART: string = require("bootstrap-icons/icons/envelope-paper-heart.svg");
const BS_ICON_ENVELOPE_PAPER_HEART_FILL: string = require("bootstrap-icons/icons/envelope-paper-heart-fill.svg");
const BS_ICON_ENVELOPE_PLUS: string = require("bootstrap-icons/icons/envelope-plus.svg");
const BS_ICON_ENVELOPE_PLUS_FILL: string = require("bootstrap-icons/icons/envelope-plus-fill.svg");
const BS_ICON_ENVELOPE_SLASH: string = require("bootstrap-icons/icons/envelope-slash.svg");
const BS_ICON_ENVELOPE_SLASH_FILL: string = require("bootstrap-icons/icons/envelope-slash-fill.svg");
const BS_ICON_ENVELOPE_X: string = require("bootstrap-icons/icons/envelope-x.svg");
const BS_ICON_ENVELOPE_X_FILL: string = require("bootstrap-icons/icons/envelope-x-fill.svg");
const BS_ICON_ERASER: string = require("bootstrap-icons/icons/eraser.svg");
const BS_ICON_ERASER_FILL: string = require("bootstrap-icons/icons/eraser-fill.svg");
const BS_ICON_ESCAPE: string = require("bootstrap-icons/icons/escape.svg");
const BS_ICON_ETHERNET: string = require("bootstrap-icons/icons/ethernet.svg");
const BS_ICON_EV_FRONT: string = require("bootstrap-icons/icons/ev-front.svg");
const BS_ICON_EV_FRONT_FILL: string = require("bootstrap-icons/icons/ev-front-fill.svg");
const BS_ICON_EV_STATION: string = require("bootstrap-icons/icons/ev-station.svg");
const BS_ICON_EV_STATION_FILL: string = require("bootstrap-icons/icons/ev-station-fill.svg");
const BS_ICON_EXCLAMATION: string = require("bootstrap-icons/icons/exclamation.svg");
const BS_ICON_EXCLAMATION_CIRCLE: string = require("bootstrap-icons/icons/exclamation-circle.svg");
const BS_ICON_EXCLAMATION_CIRCLE_FILL: string = require("bootstrap-icons/icons/exclamation-circle-fill.svg");
const BS_ICON_EXCLAMATION_DIAMOND: string = require("bootstrap-icons/icons/exclamation-diamond.svg");
const BS_ICON_EXCLAMATION_DIAMOND_FILL: string = require("bootstrap-icons/icons/exclamation-diamond-fill.svg");
const BS_ICON_EXCLAMATION_LG: string = require("bootstrap-icons/icons/exclamation-lg.svg");
const BS_ICON_EXCLAMATION_OCTAGON: string = require("bootstrap-icons/icons/exclamation-octagon.svg");
const BS_ICON_EXCLAMATION_OCTAGON_FILL: string = require("bootstrap-icons/icons/exclamation-octagon-fill.svg");
const BS_ICON_EXCLAMATION_SQUARE: string = require("bootstrap-icons/icons/exclamation-square.svg");
const BS_ICON_EXCLAMATION_SQUARE_FILL: string = require("bootstrap-icons/icons/exclamation-square-fill.svg");
const BS_ICON_EXCLAMATION_TRIANGLE: string = require("bootstrap-icons/icons/exclamation-triangle.svg");
const BS_ICON_EXCLAMATION_TRIANGLE_FILL: string = require("bootstrap-icons/icons/exclamation-triangle-fill.svg");
const BS_ICON_EXCLUDE: string = require("bootstrap-icons/icons/exclude.svg");
const BS_ICON_EXPLICIT: string = require("bootstrap-icons/icons/explicit.svg");
const BS_ICON_EXPLICIT_FILL: string = require("bootstrap-icons/icons/explicit-fill.svg");
const BS_ICON_EXPOSURE: string = require("bootstrap-icons/icons/exposure.svg");
const BS_ICON_EYE: string = require("bootstrap-icons/icons/eye.svg");
const BS_ICON_EYE_FILL: string = require("bootstrap-icons/icons/eye-fill.svg");
const BS_ICON_EYE_SLASH: string = require("bootstrap-icons/icons/eye-slash.svg");
const BS_ICON_EYE_SLASH_FILL: string = require("bootstrap-icons/icons/eye-slash-fill.svg");
const BS_ICON_EYEDROPPER: string = require("bootstrap-icons/icons/eyedropper.svg");
const BS_ICON_EYEGLASSES: string = require("bootstrap-icons/icons/eyeglasses.svg");
const BS_ICON_FACEBOOK: string = require("bootstrap-icons/icons/facebook.svg");
const BS_ICON_FAN: string = require("bootstrap-icons/icons/fan.svg");
const BS_ICON_FAST_FORWARD: string = require("bootstrap-icons/icons/fast-forward.svg");
const BS_ICON_FAST_FORWARD_BTN: string = require("bootstrap-icons/icons/fast-forward-btn.svg");
const BS_ICON_FAST_FORWARD_BTN_FILL: string = require("bootstrap-icons/icons/fast-forward-btn-fill.svg");
const BS_ICON_FAST_FORWARD_CIRCLE: string = require("bootstrap-icons/icons/fast-forward-circle.svg");
const BS_ICON_FAST_FORWARD_CIRCLE_FILL: string = require("bootstrap-icons/icons/fast-forward-circle-fill.svg");
const BS_ICON_FAST_FORWARD_FILL: string = require("bootstrap-icons/icons/fast-forward-fill.svg");
const BS_ICON_FEATHER: string = require("bootstrap-icons/icons/feather.svg");
const BS_ICON_FEATHER2: string = require("bootstrap-icons/icons/feather2.svg");
const BS_ICON_FILE: string = require("bootstrap-icons/icons/file.svg");
const BS_ICON_FILE_ARROW_DOWN: string = require("bootstrap-icons/icons/file-arrow-down.svg");
const BS_ICON_FILE_ARROW_DOWN_FILL: string = require("bootstrap-icons/icons/file-arrow-down-fill.svg");
const BS_ICON_FILE_ARROW_UP: string = require("bootstrap-icons/icons/file-arrow-up.svg");
const BS_ICON_FILE_ARROW_UP_FILL: string = require("bootstrap-icons/icons/file-arrow-up-fill.svg");
const BS_ICON_FILE_BAR_GRAPH: string = require("bootstrap-icons/icons/file-bar-graph.svg");
const BS_ICON_FILE_BAR_GRAPH_FILL: string = require("bootstrap-icons/icons/file-bar-graph-fill.svg");
const BS_ICON_FILE_BINARY: string = require("bootstrap-icons/icons/file-binary.svg");
const BS_ICON_FILE_BINARY_FILL: string = require("bootstrap-icons/icons/file-binary-fill.svg");
const BS_ICON_FILE_BREAK: string = require("bootstrap-icons/icons/file-break.svg");
const BS_ICON_FILE_BREAK_FILL: string = require("bootstrap-icons/icons/file-break-fill.svg");
const BS_ICON_FILE_CHECK: string = require("bootstrap-icons/icons/file-check.svg");
const BS_ICON_FILE_CHECK_FILL: string = require("bootstrap-icons/icons/file-check-fill.svg");
const BS_ICON_FILE_CODE: string = require("bootstrap-icons/icons/file-code.svg");
const BS_ICON_FILE_CODE_FILL: string = require("bootstrap-icons/icons/file-code-fill.svg");
const BS_ICON_FILE_DIFF: string = require("bootstrap-icons/icons/file-diff.svg");
const BS_ICON_FILE_DIFF_FILL: string = require("bootstrap-icons/icons/file-diff-fill.svg");
const BS_ICON_FILE_EARMARK: string = require("bootstrap-icons/icons/file-earmark.svg");
const BS_ICON_FILE_EARMARK_ARROW_DOWN: string = require("bootstrap-icons/icons/file-earmark-arrow-down.svg");
const BS_ICON_FILE_EARMARK_ARROW_DOWN_FILL: string = require("bootstrap-icons/icons/file-earmark-arrow-down-fill.svg");
const BS_ICON_FILE_EARMARK_ARROW_UP: string = require("bootstrap-icons/icons/file-earmark-arrow-up.svg");
const BS_ICON_FILE_EARMARK_ARROW_UP_FILL: string = require("bootstrap-icons/icons/file-earmark-arrow-up-fill.svg");
const BS_ICON_FILE_EARMARK_BAR_GRAPH: string = require("bootstrap-icons/icons/file-earmark-bar-graph.svg");
const BS_ICON_FILE_EARMARK_BAR_GRAPH_FILL: string = require("bootstrap-icons/icons/file-earmark-bar-graph-fill.svg");
const BS_ICON_FILE_EARMARK_BINARY: string = require("bootstrap-icons/icons/file-earmark-binary.svg");
const BS_ICON_FILE_EARMARK_BINARY_FILL: string = require("bootstrap-icons/icons/file-earmark-binary-fill.svg");
const BS_ICON_FILE_EARMARK_BREAK: string = require("bootstrap-icons/icons/file-earmark-break.svg");
const BS_ICON_FILE_EARMARK_BREAK_FILL: string = require("bootstrap-icons/icons/file-earmark-break-fill.svg");
const BS_ICON_FILE_EARMARK_CHECK: string = require("bootstrap-icons/icons/file-earmark-check.svg");
const BS_ICON_FILE_EARMARK_CHECK_FILL: string = require("bootstrap-icons/icons/file-earmark-check-fill.svg");
const BS_ICON_FILE_EARMARK_CODE: string = require("bootstrap-icons/icons/file-earmark-code.svg");
const BS_ICON_FILE_EARMARK_CODE_FILL: string = require("bootstrap-icons/icons/file-earmark-code-fill.svg");
const BS_ICON_FILE_EARMARK_DIFF: string = require("bootstrap-icons/icons/file-earmark-diff.svg");
const BS_ICON_FILE_EARMARK_DIFF_FILL: string = require("bootstrap-icons/icons/file-earmark-diff-fill.svg");
const BS_ICON_FILE_EARMARK_EASEL: string = require("bootstrap-icons/icons/file-earmark-easel.svg");
const BS_ICON_FILE_EARMARK_EASEL_FILL: string = require("bootstrap-icons/icons/file-earmark-easel-fill.svg");
const BS_ICON_FILE_EARMARK_EXCEL: string = require("bootstrap-icons/icons/file-earmark-excel.svg");
const BS_ICON_FILE_EARMARK_EXCEL_FILL: string = require("bootstrap-icons/icons/file-earmark-excel-fill.svg");
const BS_ICON_FILE_EARMARK_FILL: string = require("bootstrap-icons/icons/file-earmark-fill.svg");
const BS_ICON_FILE_EARMARK_FONT: string = require("bootstrap-icons/icons/file-earmark-font.svg");
const BS_ICON_FILE_EARMARK_FONT_FILL: string = require("bootstrap-icons/icons/file-earmark-font-fill.svg");
const BS_ICON_FILE_EARMARK_IMAGE: string = require("bootstrap-icons/icons/file-earmark-image.svg");
const BS_ICON_FILE_EARMARK_IMAGE_FILL: string = require("bootstrap-icons/icons/file-earmark-image-fill.svg");
const BS_ICON_FILE_EARMARK_LOCK: string = require("bootstrap-icons/icons/file-earmark-lock.svg");
const BS_ICON_FILE_EARMARK_LOCK_FILL: string = require("bootstrap-icons/icons/file-earmark-lock-fill.svg");
const BS_ICON_FILE_EARMARK_LOCK2: string = require("bootstrap-icons/icons/file-earmark-lock2.svg");
const BS_ICON_FILE_EARMARK_LOCK2_FILL: string = require("bootstrap-icons/icons/file-earmark-lock2-fill.svg");
const BS_ICON_FILE_EARMARK_MEDICAL: string = require("bootstrap-icons/icons/file-earmark-medical.svg");
const BS_ICON_FILE_EARMARK_MEDICAL_FILL: string = require("bootstrap-icons/icons/file-earmark-medical-fill.svg");
const BS_ICON_FILE_EARMARK_MINUS: string = require("bootstrap-icons/icons/file-earmark-minus.svg");
const BS_ICON_FILE_EARMARK_MINUS_FILL: string = require("bootstrap-icons/icons/file-earmark-minus-fill.svg");
const BS_ICON_FILE_EARMARK_MUSIC: string = require("bootstrap-icons/icons/file-earmark-music.svg");
const BS_ICON_FILE_EARMARK_MUSIC_FILL: string = require("bootstrap-icons/icons/file-earmark-music-fill.svg");
const BS_ICON_FILE_EARMARK_PDF: string = require("bootstrap-icons/icons/file-earmark-pdf.svg");
const BS_ICON_FILE_EARMARK_PDF_FILL: string = require("bootstrap-icons/icons/file-earmark-pdf-fill.svg");
const BS_ICON_FILE_EARMARK_PERSON: string = require("bootstrap-icons/icons/file-earmark-person.svg");
const BS_ICON_FILE_EARMARK_PERSON_FILL: string = require("bootstrap-icons/icons/file-earmark-person-fill.svg");
const BS_ICON_FILE_EARMARK_PLAY: string = require("bootstrap-icons/icons/file-earmark-play.svg");
const BS_ICON_FILE_EARMARK_PLAY_FILL: string = require("bootstrap-icons/icons/file-earmark-play-fill.svg");
const BS_ICON_FILE_EARMARK_PLUS: string = require("bootstrap-icons/icons/file-earmark-plus.svg");
const BS_ICON_FILE_EARMARK_PLUS_FILL: string = require("bootstrap-icons/icons/file-earmark-plus-fill.svg");
const BS_ICON_FILE_EARMARK_POST: string = require("bootstrap-icons/icons/file-earmark-post.svg");
const BS_ICON_FILE_EARMARK_POST_FILL: string = require("bootstrap-icons/icons/file-earmark-post-fill.svg");
const BS_ICON_FILE_EARMARK_PPT: string = require("bootstrap-icons/icons/file-earmark-ppt.svg");
const BS_ICON_FILE_EARMARK_PPT_FILL: string = require("bootstrap-icons/icons/file-earmark-ppt-fill.svg");
const BS_ICON_FILE_EARMARK_RICHTEXT: string = require("bootstrap-icons/icons/file-earmark-richtext.svg");
const BS_ICON_FILE_EARMARK_RICHTEXT_FILL: string = require("bootstrap-icons/icons/file-earmark-richtext-fill.svg");
const BS_ICON_FILE_EARMARK_RULED: string = require("bootstrap-icons/icons/file-earmark-ruled.svg");
const BS_ICON_FILE_EARMARK_RULED_FILL: string = require("bootstrap-icons/icons/file-earmark-ruled-fill.svg");
const BS_ICON_FILE_EARMARK_SLIDES: string = require("bootstrap-icons/icons/file-earmark-slides.svg");
const BS_ICON_FILE_EARMARK_SLIDES_FILL: string = require("bootstrap-icons/icons/file-earmark-slides-fill.svg");
const BS_ICON_FILE_EARMARK_SPREADSHEET: string = require("bootstrap-icons/icons/file-earmark-spreadsheet.svg");
const BS_ICON_FILE_EARMARK_SPREADSHEET_FILL: string = require("bootstrap-icons/icons/file-earmark-spreadsheet-fill.svg");
const BS_ICON_FILE_EARMARK_TEXT: string = require("bootstrap-icons/icons/file-earmark-text.svg");
const BS_ICON_FILE_EARMARK_TEXT_FILL: string = require("bootstrap-icons/icons/file-earmark-text-fill.svg");
const BS_ICON_FILE_EARMARK_WORD: string = require("bootstrap-icons/icons/file-earmark-word.svg");
const BS_ICON_FILE_EARMARK_WORD_FILL: string = require("bootstrap-icons/icons/file-earmark-word-fill.svg");
const BS_ICON_FILE_EARMARK_X: string = require("bootstrap-icons/icons/file-earmark-x.svg");
const BS_ICON_FILE_EARMARK_X_FILL: string = require("bootstrap-icons/icons/file-earmark-x-fill.svg");
const BS_ICON_FILE_EARMARK_ZIP: string = require("bootstrap-icons/icons/file-earmark-zip.svg");
const BS_ICON_FILE_EARMARK_ZIP_FILL: string = require("bootstrap-icons/icons/file-earmark-zip-fill.svg");
const BS_ICON_FILE_EASEL: string = require("bootstrap-icons/icons/file-easel.svg");
const BS_ICON_FILE_EASEL_FILL: string = require("bootstrap-icons/icons/file-easel-fill.svg");
const BS_ICON_FILE_EXCEL: string = require("bootstrap-icons/icons/file-excel.svg");
const BS_ICON_FILE_EXCEL_FILL: string = require("bootstrap-icons/icons/file-excel-fill.svg");
const BS_ICON_FILE_FILL: string = require("bootstrap-icons/icons/file-fill.svg");
const BS_ICON_FILE_FONT: string = require("bootstrap-icons/icons/file-font.svg");
const BS_ICON_FILE_FONT_FILL: string = require("bootstrap-icons/icons/file-font-fill.svg");
const BS_ICON_FILE_IMAGE: string = require("bootstrap-icons/icons/file-image.svg");
const BS_ICON_FILE_IMAGE_FILL: string = require("bootstrap-icons/icons/file-image-fill.svg");
const BS_ICON_FILE_LOCK: string = require("bootstrap-icons/icons/file-lock.svg");
const BS_ICON_FILE_LOCK_FILL: string = require("bootstrap-icons/icons/file-lock-fill.svg");
const BS_ICON_FILE_LOCK2: string = require("bootstrap-icons/icons/file-lock2.svg");
const BS_ICON_FILE_LOCK2_FILL: string = require("bootstrap-icons/icons/file-lock2-fill.svg");
const BS_ICON_FILE_MEDICAL: string = require("bootstrap-icons/icons/file-medical.svg");
const BS_ICON_FILE_MEDICAL_FILL: string = require("bootstrap-icons/icons/file-medical-fill.svg");
const BS_ICON_FILE_MINUS: string = require("bootstrap-icons/icons/file-minus.svg");
const BS_ICON_FILE_MINUS_FILL: string = require("bootstrap-icons/icons/file-minus-fill.svg");
const BS_ICON_FILE_MUSIC: string = require("bootstrap-icons/icons/file-music.svg");
const BS_ICON_FILE_MUSIC_FILL: string = require("bootstrap-icons/icons/file-music-fill.svg");
const BS_ICON_FILE_PDF: string = require("bootstrap-icons/icons/file-pdf.svg");
const BS_ICON_FILE_PDF_FILL: string = require("bootstrap-icons/icons/file-pdf-fill.svg");
const BS_ICON_FILE_PERSON: string = require("bootstrap-icons/icons/file-person.svg");
const BS_ICON_FILE_PERSON_FILL: string = require("bootstrap-icons/icons/file-person-fill.svg");
const BS_ICON_FILE_PLAY: string = require("bootstrap-icons/icons/file-play.svg");
const BS_ICON_FILE_PLAY_FILL: string = require("bootstrap-icons/icons/file-play-fill.svg");
const BS_ICON_FILE_PLUS: string = require("bootstrap-icons/icons/file-plus.svg");
const BS_ICON_FILE_PLUS_FILL: string = require("bootstrap-icons/icons/file-plus-fill.svg");
const BS_ICON_FILE_POST: string = require("bootstrap-icons/icons/file-post.svg");
const BS_ICON_FILE_POST_FILL: string = require("bootstrap-icons/icons/file-post-fill.svg");
const BS_ICON_FILE_PPT: string = require("bootstrap-icons/icons/file-ppt.svg");
const BS_ICON_FILE_PPT_FILL: string = require("bootstrap-icons/icons/file-ppt-fill.svg");
const BS_ICON_FILE_RICHTEXT: string = require("bootstrap-icons/icons/file-richtext.svg");
const BS_ICON_FILE_RICHTEXT_FILL: string = require("bootstrap-icons/icons/file-richtext-fill.svg");
const BS_ICON_FILE_RULED: string = require("bootstrap-icons/icons/file-ruled.svg");
const BS_ICON_FILE_RULED_FILL: string = require("bootstrap-icons/icons/file-ruled-fill.svg");
const BS_ICON_FILE_SLIDES: string = require("bootstrap-icons/icons/file-slides.svg");
const BS_ICON_FILE_SLIDES_FILL: string = require("bootstrap-icons/icons/file-slides-fill.svg");
const BS_ICON_FILE_SPREADSHEET: string = require("bootstrap-icons/icons/file-spreadsheet.svg");
const BS_ICON_FILE_SPREADSHEET_FILL: string = require("bootstrap-icons/icons/file-spreadsheet-fill.svg");
const BS_ICON_FILE_TEXT: string = require("bootstrap-icons/icons/file-text.svg");
const BS_ICON_FILE_TEXT_FILL: string = require("bootstrap-icons/icons/file-text-fill.svg");
const BS_ICON_FILE_WORD: string = require("bootstrap-icons/icons/file-word.svg");
const BS_ICON_FILE_WORD_FILL: string = require("bootstrap-icons/icons/file-word-fill.svg");
const BS_ICON_FILE_X: string = require("bootstrap-icons/icons/file-x.svg");
const BS_ICON_FILE_X_FILL: string = require("bootstrap-icons/icons/file-x-fill.svg");
const BS_ICON_FILE_ZIP: string = require("bootstrap-icons/icons/file-zip.svg");
const BS_ICON_FILE_ZIP_FILL: string = require("bootstrap-icons/icons/file-zip-fill.svg");
const BS_ICON_FILES: string = require("bootstrap-icons/icons/files.svg");
const BS_ICON_FILES_ALT: string = require("bootstrap-icons/icons/files-alt.svg");
const BS_ICON_FILETYPE_AAC: string = require("bootstrap-icons/icons/filetype-aac.svg");
const BS_ICON_FILETYPE_AI: string = require("bootstrap-icons/icons/filetype-ai.svg");
const BS_ICON_FILETYPE_BMP: string = require("bootstrap-icons/icons/filetype-bmp.svg");
const BS_ICON_FILETYPE_CS: string = require("bootstrap-icons/icons/filetype-cs.svg");
const BS_ICON_FILETYPE_CSS: string = require("bootstrap-icons/icons/filetype-css.svg");
const BS_ICON_FILETYPE_CSV: string = require("bootstrap-icons/icons/filetype-csv.svg");
const BS_ICON_FILETYPE_DOC: string = require("bootstrap-icons/icons/filetype-doc.svg");
const BS_ICON_FILETYPE_DOCX: string = require("bootstrap-icons/icons/filetype-docx.svg");
const BS_ICON_FILETYPE_EXE: string = require("bootstrap-icons/icons/filetype-exe.svg");
const BS_ICON_FILETYPE_GIF: string = require("bootstrap-icons/icons/filetype-gif.svg");
const BS_ICON_FILETYPE_HEIC: string = require("bootstrap-icons/icons/filetype-heic.svg");
const BS_ICON_FILETYPE_HTML: string = require("bootstrap-icons/icons/filetype-html.svg");
const BS_ICON_FILETYPE_JAVA: string = require("bootstrap-icons/icons/filetype-java.svg");
const BS_ICON_FILETYPE_JPG: string = require("bootstrap-icons/icons/filetype-jpg.svg");
const BS_ICON_FILETYPE_JS: string = require("bootstrap-icons/icons/filetype-js.svg");
const BS_ICON_FILETYPE_JSON: string = require("bootstrap-icons/icons/filetype-json.svg");
const BS_ICON_FILETYPE_JSX: string = require("bootstrap-icons/icons/filetype-jsx.svg");
const BS_ICON_FILETYPE_KEY: string = require("bootstrap-icons/icons/filetype-key.svg");
const BS_ICON_FILETYPE_M4P: string = require("bootstrap-icons/icons/filetype-m4p.svg");
const BS_ICON_FILETYPE_MD: string = require("bootstrap-icons/icons/filetype-md.svg");
const BS_ICON_FILETYPE_MDX: string = require("bootstrap-icons/icons/filetype-mdx.svg");
const BS_ICON_FILETYPE_MOV: string = require("bootstrap-icons/icons/filetype-mov.svg");
const BS_ICON_FILETYPE_MP3: string = require("bootstrap-icons/icons/filetype-mp3.svg");
const BS_ICON_FILETYPE_MP4: string = require("bootstrap-icons/icons/filetype-mp4.svg");
const BS_ICON_FILETYPE_OTF: string = require("bootstrap-icons/icons/filetype-otf.svg");
const BS_ICON_FILETYPE_PDF: string = require("bootstrap-icons/icons/filetype-pdf.svg");
const BS_ICON_FILETYPE_PHP: string = require("bootstrap-icons/icons/filetype-php.svg");
const BS_ICON_FILETYPE_PNG: string = require("bootstrap-icons/icons/filetype-png.svg");
const BS_ICON_FILETYPE_PPT: string = require("bootstrap-icons/icons/filetype-ppt.svg");
const BS_ICON_FILETYPE_PPTX: string = require("bootstrap-icons/icons/filetype-pptx.svg");
const BS_ICON_FILETYPE_PSD: string = require("bootstrap-icons/icons/filetype-psd.svg");
const BS_ICON_FILETYPE_PY: string = require("bootstrap-icons/icons/filetype-py.svg");
const BS_ICON_FILETYPE_RAW: string = require("bootstrap-icons/icons/filetype-raw.svg");
const BS_ICON_FILETYPE_RB: string = require("bootstrap-icons/icons/filetype-rb.svg");
const BS_ICON_FILETYPE_SASS: string = require("bootstrap-icons/icons/filetype-sass.svg");
const BS_ICON_FILETYPE_SCSS: string = require("bootstrap-icons/icons/filetype-scss.svg");
const BS_ICON_FILETYPE_SH: string = require("bootstrap-icons/icons/filetype-sh.svg");
const BS_ICON_FILETYPE_SQL: string = require("bootstrap-icons/icons/filetype-sql.svg");
const BS_ICON_FILETYPE_SVG: string = require("bootstrap-icons/icons/filetype-svg.svg");
const BS_ICON_FILETYPE_TIFF: string = require("bootstrap-icons/icons/filetype-tiff.svg");
const BS_ICON_FILETYPE_TSX: string = require("bootstrap-icons/icons/filetype-tsx.svg");
const BS_ICON_FILETYPE_TTF: string = require("bootstrap-icons/icons/filetype-ttf.svg");
const BS_ICON_FILETYPE_TXT: string = require("bootstrap-icons/icons/filetype-txt.svg");
const BS_ICON_FILETYPE_WAV: string = require("bootstrap-icons/icons/filetype-wav.svg");
const BS_ICON_FILETYPE_WOFF: string = require("bootstrap-icons/icons/filetype-woff.svg");
const BS_ICON_FILETYPE_XLS: string = require("bootstrap-icons/icons/filetype-xls.svg");
const BS_ICON_FILETYPE_XLSX: string = require("bootstrap-icons/icons/filetype-xlsx.svg");
const BS_ICON_FILETYPE_XML: string = require("bootstrap-icons/icons/filetype-xml.svg");
const BS_ICON_FILETYPE_YML: string = require("bootstrap-icons/icons/filetype-yml.svg");
const BS_ICON_FILM: string = require("bootstrap-icons/icons/film.svg");
const BS_ICON_FILTER: string = require("bootstrap-icons/icons/filter.svg");
const BS_ICON_FILTER_CIRCLE: string = require("bootstrap-icons/icons/filter-circle.svg");
const BS_ICON_FILTER_CIRCLE_FILL: string = require("bootstrap-icons/icons/filter-circle-fill.svg");
const BS_ICON_FILTER_LEFT: string = require("bootstrap-icons/icons/filter-left.svg");
const BS_ICON_FILTER_RIGHT: string = require("bootstrap-icons/icons/filter-right.svg");
const BS_ICON_FILTER_SQUARE: string = require("bootstrap-icons/icons/filter-square.svg");
const BS_ICON_FILTER_SQUARE_FILL: string = require("bootstrap-icons/icons/filter-square-fill.svg");
const BS_ICON_FINGERPRINT: string = require("bootstrap-icons/icons/fingerprint.svg");
const BS_ICON_FIRE: string = require("bootstrap-icons/icons/fire.svg");
const BS_ICON_FLAG: string = require("bootstrap-icons/icons/flag.svg");
const BS_ICON_FLAG_FILL: string = require("bootstrap-icons/icons/flag-fill.svg");
const BS_ICON_FLOPPY: string = require("bootstrap-icons/icons/floppy.svg");
const BS_ICON_FLOPPY_FILL: string = require("bootstrap-icons/icons/floppy-fill.svg");
const BS_ICON_FLOPPY2: string = require("bootstrap-icons/icons/floppy2.svg");
const BS_ICON_FLOPPY2_FILL: string = require("bootstrap-icons/icons/floppy2-fill.svg");
const BS_ICON_FLOWER1: string = require("bootstrap-icons/icons/flower1.svg");
const BS_ICON_FLOWER2: string = require("bootstrap-icons/icons/flower2.svg");
const BS_ICON_FLOWER3: string = require("bootstrap-icons/icons/flower3.svg");
const BS_ICON_FOLDER: string = require("bootstrap-icons/icons/folder.svg");
const BS_ICON_FOLDER_CHECK: string = require("bootstrap-icons/icons/folder-check.svg");
const BS_ICON_FOLDER_FILL: string = require("bootstrap-icons/icons/folder-fill.svg");
const BS_ICON_FOLDER_MINUS: string = require("bootstrap-icons/icons/folder-minus.svg");
const BS_ICON_FOLDER_PLUS: string = require("bootstrap-icons/icons/folder-plus.svg");
const BS_ICON_FOLDER_SYMLINK: string = require("bootstrap-icons/icons/folder-symlink.svg");
const BS_ICON_FOLDER_SYMLINK_FILL: string = require("bootstrap-icons/icons/folder-symlink-fill.svg");
const BS_ICON_FOLDER_X: string = require("bootstrap-icons/icons/folder-x.svg");
const BS_ICON_FOLDER2: string = require("bootstrap-icons/icons/folder2.svg");
const BS_ICON_FOLDER2_OPEN: string = require("bootstrap-icons/icons/folder2-open.svg");
const BS_ICON_FONTS: string = require("bootstrap-icons/icons/fonts.svg");
const BS_ICON_FORWARD: string = require("bootstrap-icons/icons/forward.svg");
const BS_ICON_FORWARD_FILL: string = require("bootstrap-icons/icons/forward-fill.svg");
const BS_ICON_FRONT: string = require("bootstrap-icons/icons/front.svg");
const BS_ICON_FUEL_PUMP: string = require("bootstrap-icons/icons/fuel-pump.svg");
const BS_ICON_FUEL_PUMP_DIESEL: string = require("bootstrap-icons/icons/fuel-pump-diesel.svg");
const BS_ICON_FUEL_PUMP_DIESEL_FILL: string = require("bootstrap-icons/icons/fuel-pump-diesel-fill.svg");
const BS_ICON_FUEL_PUMP_FILL: string = require("bootstrap-icons/icons/fuel-pump-fill.svg");
const BS_ICON_FULLSCREEN: string = require("bootstrap-icons/icons/fullscreen.svg");
const BS_ICON_FULLSCREEN_EXIT: string = require("bootstrap-icons/icons/fullscreen-exit.svg");
const BS_ICON_FUNNEL: string = require("bootstrap-icons/icons/funnel.svg");
const BS_ICON_FUNNEL_FILL: string = require("bootstrap-icons/icons/funnel-fill.svg");
const BS_ICON_GEAR: string = require("bootstrap-icons/icons/gear.svg");
const BS_ICON_GEAR_FILL: string = require("bootstrap-icons/icons/gear-fill.svg");
const BS_ICON_GEAR_WIDE: string = require("bootstrap-icons/icons/gear-wide.svg");
const BS_ICON_GEAR_WIDE_CONNECTED: string = require("bootstrap-icons/icons/gear-wide-connected.svg");
const BS_ICON_GEM: string = require("bootstrap-icons/icons/gem.svg");
const BS_ICON_GENDER_AMBIGUOUS: string = require("bootstrap-icons/icons/gender-ambiguous.svg");
const BS_ICON_GENDER_FEMALE: string = require("bootstrap-icons/icons/gender-female.svg");
const BS_ICON_GENDER_MALE: string = require("bootstrap-icons/icons/gender-male.svg");
const BS_ICON_GENDER_NEUTER: string = require("bootstrap-icons/icons/gender-neuter.svg");
const BS_ICON_GENDER_TRANS: string = require("bootstrap-icons/icons/gender-trans.svg");
const BS_ICON_GEO: string = require("bootstrap-icons/icons/geo.svg");
const BS_ICON_GEO_ALT: string = require("bootstrap-icons/icons/geo-alt.svg");
const BS_ICON_GEO_ALT_FILL: string = require("bootstrap-icons/icons/geo-alt-fill.svg");
const BS_ICON_GEO_FILL: string = require("bootstrap-icons/icons/geo-fill.svg");
const BS_ICON_GIFT: string = require("bootstrap-icons/icons/gift.svg");
const BS_ICON_GIFT_FILL: string = require("bootstrap-icons/icons/gift-fill.svg");
const BS_ICON_GIT: string = require("bootstrap-icons/icons/git.svg");
const BS_ICON_GITHUB: string = require("bootstrap-icons/icons/github.svg");
const BS_ICON_GITLAB: string = require("bootstrap-icons/icons/gitlab.svg");
const BS_ICON_GLOBE: string = require("bootstrap-icons/icons/globe.svg");
const BS_ICON_GLOBE_AMERICAS: string = require("bootstrap-icons/icons/globe-americas.svg");
const BS_ICON_GLOBE_ASIA_AUSTRALIA: string = require("bootstrap-icons/icons/globe-asia-australia.svg");
const BS_ICON_GLOBE_CENTRAL_SOUTH_ASIA: string = require("bootstrap-icons/icons/globe-central-south-asia.svg");
const BS_ICON_GLOBE_EUROPE_AFRICA: string = require("bootstrap-icons/icons/globe-europe-africa.svg");
const BS_ICON_GLOBE2: string = require("bootstrap-icons/icons/globe2.svg");
const BS_ICON_GOOGLE: string = require("bootstrap-icons/icons/google.svg");
const BS_ICON_GOOGLE_PLAY: string = require("bootstrap-icons/icons/google-play.svg");
const BS_ICON_GPU_CARD: string = require("bootstrap-icons/icons/gpu-card.svg");
const BS_ICON_GRAPH_DOWN: string = require("bootstrap-icons/icons/graph-down.svg");
const BS_ICON_GRAPH_DOWN_ARROW: string = require("bootstrap-icons/icons/graph-down-arrow.svg");
const BS_ICON_GRAPH_UP: string = require("bootstrap-icons/icons/graph-up.svg");
const BS_ICON_GRAPH_UP_ARROW: string = require("bootstrap-icons/icons/graph-up-arrow.svg");
const BS_ICON_GRID: string = require("bootstrap-icons/icons/grid.svg");
const BS_ICON_GRID_1X2: string = require("bootstrap-icons/icons/grid-1x2.svg");
const BS_ICON_GRID_1X2_FILL: string = require("bootstrap-icons/icons/grid-1x2-fill.svg");
const BS_ICON_GRID_3X2: string = require("bootstrap-icons/icons/grid-3x2.svg");
const BS_ICON_GRID_3X2_GAP: string = require("bootstrap-icons/icons/grid-3x2-gap.svg");
const BS_ICON_GRID_3X2_GAP_FILL: string = require("bootstrap-icons/icons/grid-3x2-gap-fill.svg");
const BS_ICON_GRID_3X3: string = require("bootstrap-icons/icons/grid-3x3.svg");
const BS_ICON_GRID_3X3_GAP: string = require("bootstrap-icons/icons/grid-3x3-gap.svg");
const BS_ICON_GRID_3X3_GAP_FILL: string = require("bootstrap-icons/icons/grid-3x3-gap-fill.svg");
const BS_ICON_GRID_FILL: string = require("bootstrap-icons/icons/grid-fill.svg");
const BS_ICON_GRIP_HORIZONTAL: string = require("bootstrap-icons/icons/grip-horizontal.svg");
const BS_ICON_GRIP_VERTICAL: string = require("bootstrap-icons/icons/grip-vertical.svg");
const BS_ICON_H_CIRCLE: string = require("bootstrap-icons/icons/h-circle.svg");
const BS_ICON_H_CIRCLE_FILL: string = require("bootstrap-icons/icons/h-circle-fill.svg");
const BS_ICON_H_SQUARE: string = require("bootstrap-icons/icons/h-square.svg");
const BS_ICON_H_SQUARE_FILL: string = require("bootstrap-icons/icons/h-square-fill.svg");
const BS_ICON_HAMMER: string = require("bootstrap-icons/icons/hammer.svg");
const BS_ICON_HAND_INDEX: string = require("bootstrap-icons/icons/hand-index.svg");
const BS_ICON_HAND_INDEX_FILL: string = require("bootstrap-icons/icons/hand-index-fill.svg");
const BS_ICON_HAND_INDEX_THUMB: string = require("bootstrap-icons/icons/hand-index-thumb.svg");
const BS_ICON_HAND_INDEX_THUMB_FILL: string = require("bootstrap-icons/icons/hand-index-thumb-fill.svg");
const BS_ICON_HAND_THUMBS_DOWN: string = require("bootstrap-icons/icons/hand-thumbs-down.svg");
const BS_ICON_HAND_THUMBS_DOWN_FILL: string = require("bootstrap-icons/icons/hand-thumbs-down-fill.svg");
const BS_ICON_HAND_THUMBS_UP: string = require("bootstrap-icons/icons/hand-thumbs-up.svg");
const BS_ICON_HAND_THUMBS_UP_FILL: string = require("bootstrap-icons/icons/hand-thumbs-up-fill.svg");
const BS_ICON_HANDBAG: string = require("bootstrap-icons/icons/handbag.svg");
const BS_ICON_HANDBAG_FILL: string = require("bootstrap-icons/icons/handbag-fill.svg");
const BS_ICON_HASH: string = require("bootstrap-icons/icons/hash.svg");
const BS_ICON_HDD: string = require("bootstrap-icons/icons/hdd.svg");
const BS_ICON_HDD_FILL: string = require("bootstrap-icons/icons/hdd-fill.svg");
const BS_ICON_HDD_NETWORK: string = require("bootstrap-icons/icons/hdd-network.svg");
const BS_ICON_HDD_NETWORK_FILL: string = require("bootstrap-icons/icons/hdd-network-fill.svg");
const BS_ICON_HDD_RACK: string = require("bootstrap-icons/icons/hdd-rack.svg");
const BS_ICON_HDD_RACK_FILL: string = require("bootstrap-icons/icons/hdd-rack-fill.svg");
const BS_ICON_HDD_STACK: string = require("bootstrap-icons/icons/hdd-stack.svg");
const BS_ICON_HDD_STACK_FILL: string = require("bootstrap-icons/icons/hdd-stack-fill.svg");
const BS_ICON_HDMI: string = require("bootstrap-icons/icons/hdmi.svg");
const BS_ICON_HDMI_FILL: string = require("bootstrap-icons/icons/hdmi-fill.svg");
const BS_ICON_HEADPHONES: string = require("bootstrap-icons/icons/headphones.svg");
const BS_ICON_HEADSET: string = require("bootstrap-icons/icons/headset.svg");
const BS_ICON_HEADSET_VR: string = require("bootstrap-icons/icons/headset-vr.svg");
const BS_ICON_HEART: string = require("bootstrap-icons/icons/heart.svg");
const BS_ICON_HEART_ARROW: string = require("bootstrap-icons/icons/heart-arrow.svg");
const BS_ICON_HEART_FILL: string = require("bootstrap-icons/icons/heart-fill.svg");
const BS_ICON_HEART_HALF: string = require("bootstrap-icons/icons/heart-half.svg");
const BS_ICON_HEART_PULSE: string = require("bootstrap-icons/icons/heart-pulse.svg");
const BS_ICON_HEART_PULSE_FILL: string = require("bootstrap-icons/icons/heart-pulse-fill.svg");
const BS_ICON_HEARTBREAK: string = require("bootstrap-icons/icons/heartbreak.svg");
const BS_ICON_HEARTBREAK_FILL: string = require("bootstrap-icons/icons/heartbreak-fill.svg");
const BS_ICON_HEARTS: string = require("bootstrap-icons/icons/hearts.svg");
const BS_ICON_HEPTAGON: string = require("bootstrap-icons/icons/heptagon.svg");
const BS_ICON_HEPTAGON_FILL: string = require("bootstrap-icons/icons/heptagon-fill.svg");
const BS_ICON_HEPTAGON_HALF: string = require("bootstrap-icons/icons/heptagon-half.svg");
const BS_ICON_HEXAGON: string = require("bootstrap-icons/icons/hexagon.svg");
const BS_ICON_HEXAGON_FILL: string = require("bootstrap-icons/icons/hexagon-fill.svg");
const BS_ICON_HEXAGON_HALF: string = require("bootstrap-icons/icons/hexagon-half.svg");
const BS_ICON_HIGHLIGHTER: string = require("bootstrap-icons/icons/highlighter.svg");
const BS_ICON_HIGHLIGHTS: string = require("bootstrap-icons/icons/highlights.svg");
const BS_ICON_HOSPITAL: string = require("bootstrap-icons/icons/hospital.svg");
const BS_ICON_HOSPITAL_FILL: string = require("bootstrap-icons/icons/hospital-fill.svg");
const BS_ICON_HOURGLASS: string = require("bootstrap-icons/icons/hourglass.svg");
const BS_ICON_HOURGLASS_BOTTOM: string = require("bootstrap-icons/icons/hourglass-bottom.svg");
const BS_ICON_HOURGLASS_SPLIT: string = require("bootstrap-icons/icons/hourglass-split.svg");
const BS_ICON_HOURGLASS_TOP: string = require("bootstrap-icons/icons/hourglass-top.svg");
const BS_ICON_HOUSE: string = require("bootstrap-icons/icons/house.svg");
const BS_ICON_HOUSE_ADD: string = require("bootstrap-icons/icons/house-add.svg");
const BS_ICON_HOUSE_ADD_FILL: string = require("bootstrap-icons/icons/house-add-fill.svg");
const BS_ICON_HOUSE_CHECK: string = require("bootstrap-icons/icons/house-check.svg");
const BS_ICON_HOUSE_CHECK_FILL: string = require("bootstrap-icons/icons/house-check-fill.svg");
const BS_ICON_HOUSE_DASH: string = require("bootstrap-icons/icons/house-dash.svg");
const BS_ICON_HOUSE_DASH_FILL: string = require("bootstrap-icons/icons/house-dash-fill.svg");
const BS_ICON_HOUSE_DOOR: string = require("bootstrap-icons/icons/house-door.svg");
const BS_ICON_HOUSE_DOOR_FILL: string = require("bootstrap-icons/icons/house-door-fill.svg");
const BS_ICON_HOUSE_DOWN: string = require("bootstrap-icons/icons/house-down.svg");
const BS_ICON_HOUSE_DOWN_FILL: string = require("bootstrap-icons/icons/house-down-fill.svg");
const BS_ICON_HOUSE_EXCLAMATION: string = require("bootstrap-icons/icons/house-exclamation.svg");
const BS_ICON_HOUSE_EXCLAMATION_FILL: string = require("bootstrap-icons/icons/house-exclamation-fill.svg");
const BS_ICON_HOUSE_FILL: string = require("bootstrap-icons/icons/house-fill.svg");
const BS_ICON_HOUSE_GEAR: string = require("bootstrap-icons/icons/house-gear.svg");
const BS_ICON_HOUSE_GEAR_FILL: string = require("bootstrap-icons/icons/house-gear-fill.svg");
const BS_ICON_HOUSE_HEART: string = require("bootstrap-icons/icons/house-heart.svg");
const BS_ICON_HOUSE_HEART_FILL: string = require("bootstrap-icons/icons/house-heart-fill.svg");
const BS_ICON_HOUSE_LOCK: string = require("bootstrap-icons/icons/house-lock.svg");
const BS_ICON_HOUSE_LOCK_FILL: string = require("bootstrap-icons/icons/house-lock-fill.svg");
const BS_ICON_HOUSE_SLASH: string = require("bootstrap-icons/icons/house-slash.svg");
const BS_ICON_HOUSE_SLASH_FILL: string = require("bootstrap-icons/icons/house-slash-fill.svg");
const BS_ICON_HOUSE_UP: string = require("bootstrap-icons/icons/house-up.svg");
const BS_ICON_HOUSE_UP_FILL: string = require("bootstrap-icons/icons/house-up-fill.svg");
const BS_ICON_HOUSE_X: string = require("bootstrap-icons/icons/house-x.svg");
const BS_ICON_HOUSE_X_FILL: string = require("bootstrap-icons/icons/house-x-fill.svg");
const BS_ICON_HOUSES: string = require("bootstrap-icons/icons/houses.svg");
const BS_ICON_HOUSES_FILL: string = require("bootstrap-icons/icons/houses-fill.svg");
const BS_ICON_HR: string = require("bootstrap-icons/icons/hr.svg");
const BS_ICON_HURRICANE: string = require("bootstrap-icons/icons/hurricane.svg");
const BS_ICON_HYPNOTIZE: string = require("bootstrap-icons/icons/hypnotize.svg");
const BS_ICON_IMAGE: string = require("bootstrap-icons/icons/image.svg");
const BS_ICON_IMAGE_ALT: string = require("bootstrap-icons/icons/image-alt.svg");
const BS_ICON_IMAGE_FILL: string = require("bootstrap-icons/icons/image-fill.svg");
const BS_ICON_IMAGES: string = require("bootstrap-icons/icons/images.svg");
const BS_ICON_INBOX: string = require("bootstrap-icons/icons/inbox.svg");
const BS_ICON_INBOX_FILL: string = require("bootstrap-icons/icons/inbox-fill.svg");
const BS_ICON_INBOXES: string = require("bootstrap-icons/icons/inboxes.svg");
const BS_ICON_INBOXES_FILL: string = require("bootstrap-icons/icons/inboxes-fill.svg");
const BS_ICON_INCOGNITO: string = require("bootstrap-icons/icons/incognito.svg");
const BS_ICON_INDENT: string = require("bootstrap-icons/icons/indent.svg");
const BS_ICON_INFINITY: string = require("bootstrap-icons/icons/infinity.svg");
const BS_ICON_INFO: string = require("bootstrap-icons/icons/info.svg");
const BS_ICON_INFO_CIRCLE: string = require("bootstrap-icons/icons/info-circle.svg");
const BS_ICON_INFO_CIRCLE_FILL: string = require("bootstrap-icons/icons/info-circle-fill.svg");
const BS_ICON_INFO_LG: string = require("bootstrap-icons/icons/info-lg.svg");
const BS_ICON_INFO_SQUARE: string = require("bootstrap-icons/icons/info-square.svg");
const BS_ICON_INFO_SQUARE_FILL: string = require("bootstrap-icons/icons/info-square-fill.svg");
const BS_ICON_INPUT_CURSOR: string = require("bootstrap-icons/icons/input-cursor.svg");
const BS_ICON_INPUT_CURSOR_TEXT: string = require("bootstrap-icons/icons/input-cursor-text.svg");
const BS_ICON_INSTAGRAM: string = require("bootstrap-icons/icons/instagram.svg");
const BS_ICON_INTERSECT: string = require("bootstrap-icons/icons/intersect.svg");
const BS_ICON_JOURNAL: string = require("bootstrap-icons/icons/journal.svg");
const BS_ICON_JOURNAL_ALBUM: string = require("bootstrap-icons/icons/journal-album.svg");
const BS_ICON_JOURNAL_ARROW_DOWN: string = require("bootstrap-icons/icons/journal-arrow-down.svg");
const BS_ICON_JOURNAL_ARROW_UP: string = require("bootstrap-icons/icons/journal-arrow-up.svg");
const BS_ICON_JOURNAL_BOOKMARK: string = require("bootstrap-icons/icons/journal-bookmark.svg");
const BS_ICON_JOURNAL_BOOKMARK_FILL: string = require("bootstrap-icons/icons/journal-bookmark-fill.svg");
const BS_ICON_JOURNAL_CHECK: string = require("bootstrap-icons/icons/journal-check.svg");
const BS_ICON_JOURNAL_CODE: string = require("bootstrap-icons/icons/journal-code.svg");
const BS_ICON_JOURNAL_MEDICAL: string = require("bootstrap-icons/icons/journal-medical.svg");
const BS_ICON_JOURNAL_MINUS: string = require("bootstrap-icons/icons/journal-minus.svg");
const BS_ICON_JOURNAL_PLUS: string = require("bootstrap-icons/icons/journal-plus.svg");
const BS_ICON_JOURNAL_RICHTEXT: string = require("bootstrap-icons/icons/journal-richtext.svg");
const BS_ICON_JOURNAL_TEXT: string = require("bootstrap-icons/icons/journal-text.svg");
const BS_ICON_JOURNAL_X: string = require("bootstrap-icons/icons/journal-x.svg");
const BS_ICON_JOURNALS: string = require("bootstrap-icons/icons/journals.svg");
const BS_ICON_JOYSTICK: string = require("bootstrap-icons/icons/joystick.svg");
const BS_ICON_JUSTIFY: string = require("bootstrap-icons/icons/justify.svg");
const BS_ICON_JUSTIFY_LEFT: string = require("bootstrap-icons/icons/justify-left.svg");
const BS_ICON_JUSTIFY_RIGHT: string = require("bootstrap-icons/icons/justify-right.svg");
const BS_ICON_KANBAN: string = require("bootstrap-icons/icons/kanban.svg");
const BS_ICON_KANBAN_FILL: string = require("bootstrap-icons/icons/kanban-fill.svg");
const BS_ICON_KEY: string = require("bootstrap-icons/icons/key.svg");
const BS_ICON_KEY_FILL: string = require("bootstrap-icons/icons/key-fill.svg");
const BS_ICON_KEYBOARD: string = require("bootstrap-icons/icons/keyboard.svg");
const BS_ICON_KEYBOARD_FILL: string = require("bootstrap-icons/icons/keyboard-fill.svg");
const BS_ICON_LADDER: string = require("bootstrap-icons/icons/ladder.svg");
const BS_ICON_LAMP: string = require("bootstrap-icons/icons/lamp.svg");
const BS_ICON_LAMP_FILL: string = require("bootstrap-icons/icons/lamp-fill.svg");
const BS_ICON_LAPTOP: string = require("bootstrap-icons/icons/laptop.svg");
const BS_ICON_LAPTOP_FILL: string = require("bootstrap-icons/icons/laptop-fill.svg");
const BS_ICON_LAYER_BACKWARD: string = require("bootstrap-icons/icons/layer-backward.svg");
const BS_ICON_LAYER_FORWARD: string = require("bootstrap-icons/icons/layer-forward.svg");
const BS_ICON_LAYERS: string = require("bootstrap-icons/icons/layers.svg");
const BS_ICON_LAYERS_FILL: string = require("bootstrap-icons/icons/layers-fill.svg");
const BS_ICON_LAYERS_HALF: string = require("bootstrap-icons/icons/layers-half.svg");
const BS_ICON_LAYOUT_SIDEBAR: string = require("bootstrap-icons/icons/layout-sidebar.svg");
const BS_ICON_LAYOUT_SIDEBAR_INSET: string = require("bootstrap-icons/icons/layout-sidebar-inset.svg");
const BS_ICON_LAYOUT_SIDEBAR_INSET_REVERSE: string = require("bootstrap-icons/icons/layout-sidebar-inset-reverse.svg");
const BS_ICON_LAYOUT_SIDEBAR_REVERSE: string = require("bootstrap-icons/icons/layout-sidebar-reverse.svg");
const BS_ICON_LAYOUT_SPLIT: string = require("bootstrap-icons/icons/layout-split.svg");
const BS_ICON_LAYOUT_TEXT_SIDEBAR: string = require("bootstrap-icons/icons/layout-text-sidebar.svg");
const BS_ICON_LAYOUT_TEXT_SIDEBAR_REVERSE: string = require("bootstrap-icons/icons/layout-text-sidebar-reverse.svg");
const BS_ICON_LAYOUT_TEXT_WINDOW: string = require("bootstrap-icons/icons/layout-text-window.svg");
const BS_ICON_LAYOUT_TEXT_WINDOW_REVERSE: string = require("bootstrap-icons/icons/layout-text-window-reverse.svg");
const BS_ICON_LAYOUT_THREE_COLUMNS: string = require("bootstrap-icons/icons/layout-three-columns.svg");
const BS_ICON_LAYOUT_WTF: string = require("bootstrap-icons/icons/layout-wtf.svg");
const BS_ICON_LIFE_PRESERVER: string = require("bootstrap-icons/icons/life-preserver.svg");
const BS_ICON_LIGHTBULB: string = require("bootstrap-icons/icons/lightbulb.svg");
const BS_ICON_LIGHTBULB_FILL: string = require("bootstrap-icons/icons/lightbulb-fill.svg");
const BS_ICON_LIGHTBULB_OFF: string = require("bootstrap-icons/icons/lightbulb-off.svg");
const BS_ICON_LIGHTBULB_OFF_FILL: string = require("bootstrap-icons/icons/lightbulb-off-fill.svg");
const BS_ICON_LIGHTNING: string = require("bootstrap-icons/icons/lightning.svg");
const BS_ICON_LIGHTNING_CHARGE: string = require("bootstrap-icons/icons/lightning-charge.svg");
const BS_ICON_LIGHTNING_CHARGE_FILL: string = require("bootstrap-icons/icons/lightning-charge-fill.svg");
const BS_ICON_LIGHTNING_FILL: string = require("bootstrap-icons/icons/lightning-fill.svg");
const BS_ICON_LINE: string = require("bootstrap-icons/icons/line.svg");
const BS_ICON_LINK: string = require("bootstrap-icons/icons/link.svg");
const BS_ICON_LINK_45DEG: string = require("bootstrap-icons/icons/link-45deg.svg");
const BS_ICON_LINKEDIN: string = require("bootstrap-icons/icons/linkedin.svg");
const BS_ICON_LIST: string = require("bootstrap-icons/icons/list.svg");
const BS_ICON_LIST_CHECK: string = require("bootstrap-icons/icons/list-check.svg");
const BS_ICON_LIST_COLUMNS: string = require("bootstrap-icons/icons/list-columns.svg");
const BS_ICON_LIST_COLUMNS_REVERSE: string = require("bootstrap-icons/icons/list-columns-reverse.svg");
const BS_ICON_LIST_NESTED: string = require("bootstrap-icons/icons/list-nested.svg");
const BS_ICON_LIST_OL: string = require("bootstrap-icons/icons/list-ol.svg");
const BS_ICON_LIST_STARS: string = require("bootstrap-icons/icons/list-stars.svg");
const BS_ICON_LIST_TASK: string = require("bootstrap-icons/icons/list-task.svg");
const BS_ICON_LIST_UL: string = require("bootstrap-icons/icons/list-ul.svg");
const BS_ICON_LOCK: string = require("bootstrap-icons/icons/lock.svg");
const BS_ICON_LOCK_FILL: string = require("bootstrap-icons/icons/lock-fill.svg");
const BS_ICON_LUGGAGE: string = require("bootstrap-icons/icons/luggage.svg");
const BS_ICON_LUGGAGE_FILL: string = require("bootstrap-icons/icons/luggage-fill.svg");
const BS_ICON_LUNGS: string = require("bootstrap-icons/icons/lungs.svg");
const BS_ICON_LUNGS_FILL: string = require("bootstrap-icons/icons/lungs-fill.svg");
const BS_ICON_MAGIC: string = require("bootstrap-icons/icons/magic.svg");
const BS_ICON_MAGNET: string = require("bootstrap-icons/icons/magnet.svg");
const BS_ICON_MAGNET_FILL: string = require("bootstrap-icons/icons/magnet-fill.svg");
const BS_ICON_MAILBOX: string = require("bootstrap-icons/icons/mailbox.svg");
const BS_ICON_MAILBOX_FLAG: string = require("bootstrap-icons/icons/mailbox-flag.svg");
const BS_ICON_MAILBOX2: string = require("bootstrap-icons/icons/mailbox2.svg");
const BS_ICON_MAILBOX2_FLAG: string = require("bootstrap-icons/icons/mailbox2-flag.svg");
const BS_ICON_MAP: string = require("bootstrap-icons/icons/map.svg");
const BS_ICON_MAP_FILL: string = require("bootstrap-icons/icons/map-fill.svg");
const BS_ICON_MARKDOWN: string = require("bootstrap-icons/icons/markdown.svg");
const BS_ICON_MARKDOWN_FILL: string = require("bootstrap-icons/icons/markdown-fill.svg");
const BS_ICON_MARKER_TIP: string = require("bootstrap-icons/icons/marker-tip.svg");
const BS_ICON_MASK: string = require("bootstrap-icons/icons/mask.svg");
const BS_ICON_MASTODON: string = require("bootstrap-icons/icons/mastodon.svg");
const BS_ICON_MEDIUM: string = require("bootstrap-icons/icons/medium.svg");
const BS_ICON_MEGAPHONE: string = require("bootstrap-icons/icons/megaphone.svg");
const BS_ICON_MEGAPHONE_FILL: string = require("bootstrap-icons/icons/megaphone-fill.svg");
const BS_ICON_MEMORY: string = require("bootstrap-icons/icons/memory.svg");
const BS_ICON_MENU_APP: string = require("bootstrap-icons/icons/menu-app.svg");
const BS_ICON_MENU_APP_FILL: string = require("bootstrap-icons/icons/menu-app-fill.svg");
const BS_ICON_MENU_BUTTON: string = require("bootstrap-icons/icons/menu-button.svg");
const BS_ICON_MENU_BUTTON_FILL: string = require("bootstrap-icons/icons/menu-button-fill.svg");
const BS_ICON_MENU_BUTTON_WIDE: string = require("bootstrap-icons/icons/menu-button-wide.svg");
const BS_ICON_MENU_BUTTON_WIDE_FILL: string = require("bootstrap-icons/icons/menu-button-wide-fill.svg");
const BS_ICON_MENU_DOWN: string = require("bootstrap-icons/icons/menu-down.svg");
const BS_ICON_MENU_UP: string = require("bootstrap-icons/icons/menu-up.svg");
const BS_ICON_MESSENGER: string = require("bootstrap-icons/icons/messenger.svg");
const BS_ICON_META: string = require("bootstrap-icons/icons/meta.svg");
const BS_ICON_MIC: string = require("bootstrap-icons/icons/mic.svg");
const BS_ICON_MIC_FILL: string = require("bootstrap-icons/icons/mic-fill.svg");
const BS_ICON_MIC_MUTE: string = require("bootstrap-icons/icons/mic-mute.svg");
const BS_ICON_MIC_MUTE_FILL: string = require("bootstrap-icons/icons/mic-mute-fill.svg");
const BS_ICON_MICROSOFT: string = require("bootstrap-icons/icons/microsoft.svg");
const BS_ICON_MICROSOFT_TEAMS: string = require("bootstrap-icons/icons/microsoft-teams.svg");
const BS_ICON_MINECART: string = require("bootstrap-icons/icons/minecart.svg");
const BS_ICON_MINECART_LOADED: string = require("bootstrap-icons/icons/minecart-loaded.svg");
const BS_ICON_MODEM: string = require("bootstrap-icons/icons/modem.svg");
const BS_ICON_MODEM_FILL: string = require("bootstrap-icons/icons/modem-fill.svg");
const BS_ICON_MOISTURE: string = require("bootstrap-icons/icons/moisture.svg");
const BS_ICON_MOON: string = require("bootstrap-icons/icons/moon.svg");
const BS_ICON_MOON_FILL: string = require("bootstrap-icons/icons/moon-fill.svg");
const BS_ICON_MOON_STARS: string = require("bootstrap-icons/icons/moon-stars.svg");
const BS_ICON_MOON_STARS_FILL: string = require("bootstrap-icons/icons/moon-stars-fill.svg");
const BS_ICON_MORTARBOARD: string = require("bootstrap-icons/icons/mortarboard.svg");
const BS_ICON_MORTARBOARD_FILL: string = require("bootstrap-icons/icons/mortarboard-fill.svg");
const BS_ICON_MOTHERBOARD: string = require("bootstrap-icons/icons/motherboard.svg");
const BS_ICON_MOTHERBOARD_FILL: string = require("bootstrap-icons/icons/motherboard-fill.svg");
const BS_ICON_MOUSE: string = require("bootstrap-icons/icons/mouse.svg");
const BS_ICON_MOUSE_FILL: string = require("bootstrap-icons/icons/mouse-fill.svg");
const BS_ICON_MOUSE2: string = require("bootstrap-icons/icons/mouse2.svg");
const BS_ICON_MOUSE2_FILL: string = require("bootstrap-icons/icons/mouse2-fill.svg");
const BS_ICON_MOUSE3: string = require("bootstrap-icons/icons/mouse3.svg");
const BS_ICON_MOUSE3_FILL: string = require("bootstrap-icons/icons/mouse3-fill.svg");
const BS_ICON_MUSIC_NOTE: string = require("bootstrap-icons/icons/music-note.svg");
const BS_ICON_MUSIC_NOTE_BEAMED: string = require("bootstrap-icons/icons/music-note-beamed.svg");
const BS_ICON_MUSIC_NOTE_LIST: string = require("bootstrap-icons/icons/music-note-list.svg");
const BS_ICON_MUSIC_PLAYER: string = require("bootstrap-icons/icons/music-player.svg");
const BS_ICON_MUSIC_PLAYER_FILL: string = require("bootstrap-icons/icons/music-player-fill.svg");
const BS_ICON_NEWSPAPER: string = require("bootstrap-icons/icons/newspaper.svg");
const BS_ICON_NINTENDO_SWITCH: string = require("bootstrap-icons/icons/nintendo-switch.svg");
const BS_ICON_NODE_MINUS: string = require("bootstrap-icons/icons/node-minus.svg");
const BS_ICON_NODE_MINUS_FILL: string = require("bootstrap-icons/icons/node-minus-fill.svg");
const BS_ICON_NODE_PLUS: string = require("bootstrap-icons/icons/node-plus.svg");
const BS_ICON_NODE_PLUS_FILL: string = require("bootstrap-icons/icons/node-plus-fill.svg");
const BS_ICON_NOISE_REDUCTION: string = require("bootstrap-icons/icons/noise-reduction.svg");
const BS_ICON_NUT: string = require("bootstrap-icons/icons/nut.svg");
const BS_ICON_NUT_FILL: string = require("bootstrap-icons/icons/nut-fill.svg");
const BS_ICON_NVIDIA: string = require("bootstrap-icons/icons/nvidia.svg");
const BS_ICON_NVME: string = require("bootstrap-icons/icons/nvme.svg");
const BS_ICON_NVME_FILL: string = require("bootstrap-icons/icons/nvme-fill.svg");
const BS_ICON_OCTAGON: string = require("bootstrap-icons/icons/octagon.svg");
const BS_ICON_OCTAGON_FILL: string = require("bootstrap-icons/icons/octagon-fill.svg");
const BS_ICON_OCTAGON_HALF: string = require("bootstrap-icons/icons/octagon-half.svg");
const BS_ICON_OPENCOLLECTIVE: string = require("bootstrap-icons/icons/opencollective.svg");
const BS_ICON_OPTICAL_AUDIO: string = require("bootstrap-icons/icons/optical-audio.svg");
const BS_ICON_OPTICAL_AUDIO_FILL: string = require("bootstrap-icons/icons/optical-audio-fill.svg");
const BS_ICON_OPTION: string = require("bootstrap-icons/icons/option.svg");
const BS_ICON_OUTLET: string = require("bootstrap-icons/icons/outlet.svg");
const BS_ICON_P_CIRCLE: string = require("bootstrap-icons/icons/p-circle.svg");
const BS_ICON_P_CIRCLE_FILL: string = require("bootstrap-icons/icons/p-circle-fill.svg");
const BS_ICON_P_SQUARE: string = require("bootstrap-icons/icons/p-square.svg");
const BS_ICON_P_SQUARE_FILL: string = require("bootstrap-icons/icons/p-square-fill.svg");
const BS_ICON_PAINT_BUCKET: string = require("bootstrap-icons/icons/paint-bucket.svg");
const BS_ICON_PALETTE: string = require("bootstrap-icons/icons/palette.svg");
const BS_ICON_PALETTE_FILL: string = require("bootstrap-icons/icons/palette-fill.svg");
const BS_ICON_PALETTE2: string = require("bootstrap-icons/icons/palette2.svg");
const BS_ICON_PAPERCLIP: string = require("bootstrap-icons/icons/paperclip.svg");
const BS_ICON_PARAGRAPH: string = require("bootstrap-icons/icons/paragraph.svg");
const BS_ICON_PASS: string = require("bootstrap-icons/icons/pass.svg");
const BS_ICON_PASS_FILL: string = require("bootstrap-icons/icons/pass-fill.svg");
const BS_ICON_PASSPORT: string = require("bootstrap-icons/icons/passport.svg");
const BS_ICON_PASSPORT_FILL: string = require("bootstrap-icons/icons/passport-fill.svg");
const BS_ICON_PATCH_CHECK: string = require("bootstrap-icons/icons/patch-check.svg");
const BS_ICON_PATCH_CHECK_FILL: string = require("bootstrap-icons/icons/patch-check-fill.svg");
const BS_ICON_PATCH_EXCLAMATION: string = require("bootstrap-icons/icons/patch-exclamation.svg");
const BS_ICON_PATCH_EXCLAMATION_FILL: string = require("bootstrap-icons/icons/patch-exclamation-fill.svg");
const BS_ICON_PATCH_MINUS: string = require("bootstrap-icons/icons/patch-minus.svg");
const BS_ICON_PATCH_MINUS_FILL: string = require("bootstrap-icons/icons/patch-minus-fill.svg");
const BS_ICON_PATCH_PLUS: string = require("bootstrap-icons/icons/patch-plus.svg");
const BS_ICON_PATCH_PLUS_FILL: string = require("bootstrap-icons/icons/patch-plus-fill.svg");
const BS_ICON_PATCH_QUESTION: string = require("bootstrap-icons/icons/patch-question.svg");
const BS_ICON_PATCH_QUESTION_FILL: string = require("bootstrap-icons/icons/patch-question-fill.svg");
const BS_ICON_PAUSE: string = require("bootstrap-icons/icons/pause.svg");
const BS_ICON_PAUSE_BTN: string = require("bootstrap-icons/icons/pause-btn.svg");
const BS_ICON_PAUSE_BTN_FILL: string = require("bootstrap-icons/icons/pause-btn-fill.svg");
const BS_ICON_PAUSE_CIRCLE: string = require("bootstrap-icons/icons/pause-circle.svg");
const BS_ICON_PAUSE_CIRCLE_FILL: string = require("bootstrap-icons/icons/pause-circle-fill.svg");
const BS_ICON_PAUSE_FILL: string = require("bootstrap-icons/icons/pause-fill.svg");
const BS_ICON_PAYPAL: string = require("bootstrap-icons/icons/paypal.svg");
const BS_ICON_PC: string = require("bootstrap-icons/icons/pc.svg");
const BS_ICON_PC_DISPLAY: string = require("bootstrap-icons/icons/pc-display.svg");
const BS_ICON_PC_DISPLAY_HORIZONTAL: string = require("bootstrap-icons/icons/pc-display-horizontal.svg");
const BS_ICON_PC_HORIZONTAL: string = require("bootstrap-icons/icons/pc-horizontal.svg");
const BS_ICON_PCI_CARD: string = require("bootstrap-icons/icons/pci-card.svg");
const BS_ICON_PCI_CARD_NETWORK: string = require("bootstrap-icons/icons/pci-card-network.svg");
const BS_ICON_PCI_CARD_SOUND: string = require("bootstrap-icons/icons/pci-card-sound.svg");
const BS_ICON_PEACE: string = require("bootstrap-icons/icons/peace.svg");
const BS_ICON_PEACE_FILL: string = require("bootstrap-icons/icons/peace-fill.svg");
const BS_ICON_PEN: string = require("bootstrap-icons/icons/pen.svg");
const BS_ICON_PEN_FILL: string = require("bootstrap-icons/icons/pen-fill.svg");
const BS_ICON_PENCIL: string = require("bootstrap-icons/icons/pencil.svg");
const BS_ICON_PENCIL_FILL: string = require("bootstrap-icons/icons/pencil-fill.svg");
const BS_ICON_PENCIL_SQUARE: string = require("bootstrap-icons/icons/pencil-square.svg");
const BS_ICON_PENTAGON: string = require("bootstrap-icons/icons/pentagon.svg");
const BS_ICON_PENTAGON_FILL: string = require("bootstrap-icons/icons/pentagon-fill.svg");
const BS_ICON_PENTAGON_HALF: string = require("bootstrap-icons/icons/pentagon-half.svg");
const BS_ICON_PEOPLE: string = require("bootstrap-icons/icons/people.svg");
const BS_ICON_PEOPLE_FILL: string = require("bootstrap-icons/icons/people-fill.svg");
const BS_ICON_PERCENT: string = require("bootstrap-icons/icons/percent.svg");
const BS_ICON_PERSON: string = require("bootstrap-icons/icons/person.svg");
const BS_ICON_PERSON_ADD: string = require("bootstrap-icons/icons/person-add.svg");
const BS_ICON_PERSON_ARMS_UP: string = require("bootstrap-icons/icons/person-arms-up.svg");
const BS_ICON_PERSON_BADGE: string = require("bootstrap-icons/icons/person-badge.svg");
const BS_ICON_PERSON_BADGE_FILL: string = require("bootstrap-icons/icons/person-badge-fill.svg");
const BS_ICON_PERSON_BOUNDING_BOX: string = require("bootstrap-icons/icons/person-bounding-box.svg");
const BS_ICON_PERSON_CHECK: string = require("bootstrap-icons/icons/person-check.svg");
const BS_ICON_PERSON_CHECK_FILL: string = require("bootstrap-icons/icons/person-check-fill.svg");
const BS_ICON_PERSON_CIRCLE: string = require("bootstrap-icons/icons/person-circle.svg");
const BS_ICON_PERSON_DASH: string = require("bootstrap-icons/icons/person-dash.svg");
const BS_ICON_PERSON_DASH_FILL: string = require("bootstrap-icons/icons/person-dash-fill.svg");
const BS_ICON_PERSON_DOWN: string = require("bootstrap-icons/icons/person-down.svg");
const BS_ICON_PERSON_EXCLAMATION: string = require("bootstrap-icons/icons/person-exclamation.svg");
const BS_ICON_PERSON_FILL: string = require("bootstrap-icons/icons/person-fill.svg");
const BS_ICON_PERSON_FILL_ADD: string = require("bootstrap-icons/icons/person-fill-add.svg");
const BS_ICON_PERSON_FILL_CHECK: string = require("bootstrap-icons/icons/person-fill-check.svg");
const BS_ICON_PERSON_FILL_DASH: string = require("bootstrap-icons/icons/person-fill-dash.svg");
const BS_ICON_PERSON_FILL_DOWN: string = require("bootstrap-icons/icons/person-fill-down.svg");
const BS_ICON_PERSON_FILL_EXCLAMATION: string = require("bootstrap-icons/icons/person-fill-exclamation.svg");
const BS_ICON_PERSON_FILL_GEAR: string = require("bootstrap-icons/icons/person-fill-gear.svg");
const BS_ICON_PERSON_FILL_LOCK: string = require("bootstrap-icons/icons/person-fill-lock.svg");
const BS_ICON_PERSON_FILL_SLASH: string = require("bootstrap-icons/icons/person-fill-slash.svg");
const BS_ICON_PERSON_FILL_UP: string = require("bootstrap-icons/icons/person-fill-up.svg");
const BS_ICON_PERSON_FILL_X: string = require("bootstrap-icons/icons/person-fill-x.svg");
const BS_ICON_PERSON_GEAR: string = require("bootstrap-icons/icons/person-gear.svg");
const BS_ICON_PERSON_HEART: string = require("bootstrap-icons/icons/person-heart.svg");
const BS_ICON_PERSON_HEARTS: string = require("bootstrap-icons/icons/person-hearts.svg");
const BS_ICON_PERSON_LINES_FILL: string = require("bootstrap-icons/icons/person-lines-fill.svg");
const BS_ICON_PERSON_LOCK: string = require("bootstrap-icons/icons/person-lock.svg");
const BS_ICON_PERSON_PLUS: string = require("bootstrap-icons/icons/person-plus.svg");
const BS_ICON_PERSON_PLUS_FILL: string = require("bootstrap-icons/icons/person-plus-fill.svg");
const BS_ICON_PERSON_RAISED_HAND: string = require("bootstrap-icons/icons/person-raised-hand.svg");
const BS_ICON_PERSON_ROLODEX: string = require("bootstrap-icons/icons/person-rolodex.svg");
const BS_ICON_PERSON_SLASH: string = require("bootstrap-icons/icons/person-slash.svg");
const BS_ICON_PERSON_SQUARE: string = require("bootstrap-icons/icons/person-square.svg");
const BS_ICON_PERSON_STANDING: string = require("bootstrap-icons/icons/person-standing.svg");
const BS_ICON_PERSON_STANDING_DRESS: string = require("bootstrap-icons/icons/person-standing-dress.svg");
const BS_ICON_PERSON_UP: string = require("bootstrap-icons/icons/person-up.svg");
const BS_ICON_PERSON_VCARD: string = require("bootstrap-icons/icons/person-vcard.svg");
const BS_ICON_PERSON_VCARD_FILL: string = require("bootstrap-icons/icons/person-vcard-fill.svg");
const BS_ICON_PERSON_VIDEO: string = require("bootstrap-icons/icons/person-video.svg");
const BS_ICON_PERSON_VIDEO2: string = require("bootstrap-icons/icons/person-video2.svg");
const BS_ICON_PERSON_VIDEO3: string = require("bootstrap-icons/icons/person-video3.svg");
const BS_ICON_PERSON_WALKING: string = require("bootstrap-icons/icons/person-walking.svg");
const BS_ICON_PERSON_WHEELCHAIR: string = require("bootstrap-icons/icons/person-wheelchair.svg");
const BS_ICON_PERSON_WORKSPACE: string = require("bootstrap-icons/icons/person-workspace.svg");
const BS_ICON_PERSON_X: string = require("bootstrap-icons/icons/person-x.svg");
const BS_ICON_PERSON_X_FILL: string = require("bootstrap-icons/icons/person-x-fill.svg");
const BS_ICON_PHONE: string = require("bootstrap-icons/icons/phone.svg");
const BS_ICON_PHONE_FILL: string = require("bootstrap-icons/icons/phone-fill.svg");
const BS_ICON_PHONE_FLIP: string = require("bootstrap-icons/icons/phone-flip.svg");
const BS_ICON_PHONE_LANDSCAPE: string = require("bootstrap-icons/icons/phone-landscape.svg");
const BS_ICON_PHONE_LANDSCAPE_FILL: string = require("bootstrap-icons/icons/phone-landscape-fill.svg");
const BS_ICON_PHONE_VIBRATE: string = require("bootstrap-icons/icons/phone-vibrate.svg");
const BS_ICON_PHONE_VIBRATE_FILL: string = require("bootstrap-icons/icons/phone-vibrate-fill.svg");
const BS_ICON_PIE_CHART: string = require("bootstrap-icons/icons/pie-chart.svg");
const BS_ICON_PIE_CHART_FILL: string = require("bootstrap-icons/icons/pie-chart-fill.svg");
const BS_ICON_PIGGY_BANK: string = require("bootstrap-icons/icons/piggy-bank.svg");
const BS_ICON_PIGGY_BANK_FILL: string = require("bootstrap-icons/icons/piggy-bank-fill.svg");
const BS_ICON_PIN: string = require("bootstrap-icons/icons/pin.svg");
const BS_ICON_PIN_ANGLE: string = require("bootstrap-icons/icons/pin-angle.svg");
const BS_ICON_PIN_ANGLE_FILL: string = require("bootstrap-icons/icons/pin-angle-fill.svg");
const BS_ICON_PIN_FILL: string = require("bootstrap-icons/icons/pin-fill.svg");
const BS_ICON_PIN_MAP: string = require("bootstrap-icons/icons/pin-map.svg");
const BS_ICON_PIN_MAP_FILL: string = require("bootstrap-icons/icons/pin-map-fill.svg");
const BS_ICON_PINTEREST: string = require("bootstrap-icons/icons/pinterest.svg");
const BS_ICON_PIP: string = require("bootstrap-icons/icons/pip.svg");
const BS_ICON_PIP_FILL: string = require("bootstrap-icons/icons/pip-fill.svg");
const BS_ICON_PLAY: string = require("bootstrap-icons/icons/play.svg");
const BS_ICON_PLAY_BTN: string = require("bootstrap-icons/icons/play-btn.svg");
const BS_ICON_PLAY_BTN_FILL: string = require("bootstrap-icons/icons/play-btn-fill.svg");
const BS_ICON_PLAY_CIRCLE: string = require("bootstrap-icons/icons/play-circle.svg");
const BS_ICON_PLAY_CIRCLE_FILL: string = require("bootstrap-icons/icons/play-circle-fill.svg");
const BS_ICON_PLAY_FILL: string = require("bootstrap-icons/icons/play-fill.svg");
const BS_ICON_PLAYSTATION: string = require("bootstrap-icons/icons/playstation.svg");
const BS_ICON_PLUG: string = require("bootstrap-icons/icons/plug.svg");
const BS_ICON_PLUG_FILL: string = require("bootstrap-icons/icons/plug-fill.svg");
const BS_ICON_PLUGIN: string = require("bootstrap-icons/icons/plugin.svg");
const BS_ICON_PLUS: string = require("bootstrap-icons/icons/plus.svg");
const BS_ICON_PLUS_CIRCLE: string = require("bootstrap-icons/icons/plus-circle.svg");
const BS_ICON_PLUS_CIRCLE_DOTTED: string = require("bootstrap-icons/icons/plus-circle-dotted.svg");
const BS_ICON_PLUS_CIRCLE_FILL: string = require("bootstrap-icons/icons/plus-circle-fill.svg");
const BS_ICON_PLUS_LG: string = require("bootstrap-icons/icons/plus-lg.svg");
const BS_ICON_PLUS_SLASH_MINUS: string = require("bootstrap-icons/icons/plus-slash-minus.svg");
const BS_ICON_PLUS_SQUARE: string = require("bootstrap-icons/icons/plus-square.svg");
const BS_ICON_PLUS_SQUARE_DOTTED: string = require("bootstrap-icons/icons/plus-square-dotted.svg");
const BS_ICON_PLUS_SQUARE_FILL: string = require("bootstrap-icons/icons/plus-square-fill.svg");
const BS_ICON_POSTAGE: string = require("bootstrap-icons/icons/postage.svg");
const BS_ICON_POSTAGE_FILL: string = require("bootstrap-icons/icons/postage-fill.svg");
const BS_ICON_POSTAGE_HEART: string = require("bootstrap-icons/icons/postage-heart.svg");
const BS_ICON_POSTAGE_HEART_FILL: string = require("bootstrap-icons/icons/postage-heart-fill.svg");
const BS_ICON_POSTCARD: string = require("bootstrap-icons/icons/postcard.svg");
const BS_ICON_POSTCARD_FILL: string = require("bootstrap-icons/icons/postcard-fill.svg");
const BS_ICON_POSTCARD_HEART: string = require("bootstrap-icons/icons/postcard-heart.svg");
const BS_ICON_POSTCARD_HEART_FILL: string = require("bootstrap-icons/icons/postcard-heart-fill.svg");
const BS_ICON_POWER: string = require("bootstrap-icons/icons/power.svg");
const BS_ICON_PRESCRIPTION: string = require("bootstrap-icons/icons/prescription.svg");
const BS_ICON_PRESCRIPTION2: string = require("bootstrap-icons/icons/prescription2.svg");
const BS_ICON_PRINTER: string = require("bootstrap-icons/icons/printer.svg");
const BS_ICON_PRINTER_FILL: string = require("bootstrap-icons/icons/printer-fill.svg");
const BS_ICON_PROJECTOR: string = require("bootstrap-icons/icons/projector.svg");
const BS_ICON_PROJECTOR_FILL: string = require("bootstrap-icons/icons/projector-fill.svg");
const BS_ICON_PUZZLE: string = require("bootstrap-icons/icons/puzzle.svg");
const BS_ICON_PUZZLE_FILL: string = require("bootstrap-icons/icons/puzzle-fill.svg");
const BS_ICON_QR_CODE: string = require("bootstrap-icons/icons/qr-code.svg");
const BS_ICON_QR_CODE_SCAN: string = require("bootstrap-icons/icons/qr-code-scan.svg");
const BS_ICON_QUESTION: string = require("bootstrap-icons/icons/question.svg");
const BS_ICON_QUESTION_CIRCLE: string = require("bootstrap-icons/icons/question-circle.svg");
const BS_ICON_QUESTION_CIRCLE_FILL: string = require("bootstrap-icons/icons/question-circle-fill.svg");
const BS_ICON_QUESTION_DIAMOND: string = require("bootstrap-icons/icons/question-diamond.svg");
const BS_ICON_QUESTION_DIAMOND_FILL: string = require("bootstrap-icons/icons/question-diamond-fill.svg");
const BS_ICON_QUESTION_LG: string = require("bootstrap-icons/icons/question-lg.svg");
const BS_ICON_QUESTION_OCTAGON: string = require("bootstrap-icons/icons/question-octagon.svg");
const BS_ICON_QUESTION_OCTAGON_FILL: string = require("bootstrap-icons/icons/question-octagon-fill.svg");
const BS_ICON_QUESTION_SQUARE: string = require("bootstrap-icons/icons/question-square.svg");
const BS_ICON_QUESTION_SQUARE_FILL: string = require("bootstrap-icons/icons/question-square-fill.svg");
const BS_ICON_QUORA: string = require("bootstrap-icons/icons/quora.svg");
const BS_ICON_QUOTE: string = require("bootstrap-icons/icons/quote.svg");
const BS_ICON_R_CIRCLE: string = require("bootstrap-icons/icons/r-circle.svg");
const BS_ICON_R_CIRCLE_FILL: string = require("bootstrap-icons/icons/r-circle-fill.svg");
const BS_ICON_R_SQUARE: string = require("bootstrap-icons/icons/r-square.svg");
const BS_ICON_R_SQUARE_FILL: string = require("bootstrap-icons/icons/r-square-fill.svg");
const BS_ICON_RADAR: string = require("bootstrap-icons/icons/radar.svg");
const BS_ICON_RADIOACTIVE: string = require("bootstrap-icons/icons/radioactive.svg");
const BS_ICON_RAINBOW: string = require("bootstrap-icons/icons/rainbow.svg");
const BS_ICON_RECEIPT: string = require("bootstrap-icons/icons/receipt.svg");
const BS_ICON_RECEIPT_CUTOFF: string = require("bootstrap-icons/icons/receipt-cutoff.svg");
const BS_ICON_RECEPTION_0: string = require("bootstrap-icons/icons/reception-0.svg");
const BS_ICON_RECEPTION_1: string = require("bootstrap-icons/icons/reception-1.svg");
const BS_ICON_RECEPTION_2: string = require("bootstrap-icons/icons/reception-2.svg");
const BS_ICON_RECEPTION_3: string = require("bootstrap-icons/icons/reception-3.svg");
const BS_ICON_RECEPTION_4: string = require("bootstrap-icons/icons/reception-4.svg");
const BS_ICON_RECORD: string = require("bootstrap-icons/icons/record.svg");
const BS_ICON_RECORD_BTN: string = require("bootstrap-icons/icons/record-btn.svg");
const BS_ICON_RECORD_BTN_FILL: string = require("bootstrap-icons/icons/record-btn-fill.svg");
const BS_ICON_RECORD_CIRCLE: string = require("bootstrap-icons/icons/record-circle.svg");
const BS_ICON_RECORD_CIRCLE_FILL: string = require("bootstrap-icons/icons/record-circle-fill.svg");
const BS_ICON_RECORD_FILL: string = require("bootstrap-icons/icons/record-fill.svg");
const BS_ICON_RECORD2: string = require("bootstrap-icons/icons/record2.svg");
const BS_ICON_RECORD2_FILL: string = require("bootstrap-icons/icons/record2-fill.svg");
const BS_ICON_RECYCLE: string = require("bootstrap-icons/icons/recycle.svg");
const BS_ICON_REDDIT: string = require("bootstrap-icons/icons/reddit.svg");
const BS_ICON_REGEX: string = require("bootstrap-icons/icons/regex.svg");
const BS_ICON_REPEAT: string = require("bootstrap-icons/icons/repeat.svg");
const BS_ICON_REPEAT_1: string = require("bootstrap-icons/icons/repeat-1.svg");
const BS_ICON_REPLY: string = require("bootstrap-icons/icons/reply.svg");
const BS_ICON_REPLY_ALL: string = require("bootstrap-icons/icons/reply-all.svg");
const BS_ICON_REPLY_ALL_FILL: string = require("bootstrap-icons/icons/reply-all-fill.svg");
const BS_ICON_REPLY_FILL: string = require("bootstrap-icons/icons/reply-fill.svg");
const BS_ICON_REWIND: string = require("bootstrap-icons/icons/rewind.svg");
const BS_ICON_REWIND_BTN: string = require("bootstrap-icons/icons/rewind-btn.svg");
const BS_ICON_REWIND_BTN_FILL: string = require("bootstrap-icons/icons/rewind-btn-fill.svg");
const BS_ICON_REWIND_CIRCLE: string = require("bootstrap-icons/icons/rewind-circle.svg");
const BS_ICON_REWIND_CIRCLE_FILL: string = require("bootstrap-icons/icons/rewind-circle-fill.svg");
const BS_ICON_REWIND_FILL: string = require("bootstrap-icons/icons/rewind-fill.svg");
const BS_ICON_ROBOT: string = require("bootstrap-icons/icons/robot.svg");
const BS_ICON_ROCKET: string = require("bootstrap-icons/icons/rocket.svg");
const BS_ICON_ROCKET_FILL: string = require("bootstrap-icons/icons/rocket-fill.svg");
const BS_ICON_ROCKET_TAKEOFF: string = require("bootstrap-icons/icons/rocket-takeoff.svg");
const BS_ICON_ROCKET_TAKEOFF_FILL: string = require("bootstrap-icons/icons/rocket-takeoff-fill.svg");
const BS_ICON_ROUTER: string = require("bootstrap-icons/icons/router.svg");
const BS_ICON_ROUTER_FILL: string = require("bootstrap-icons/icons/router-fill.svg");
const BS_ICON_RSS: string = require("bootstrap-icons/icons/rss.svg");
const BS_ICON_RSS_FILL: string = require("bootstrap-icons/icons/rss-fill.svg");
const BS_ICON_RULERS: string = require("bootstrap-icons/icons/rulers.svg");
const BS_ICON_SAFE: string = require("bootstrap-icons/icons/safe.svg");
const BS_ICON_SAFE_FILL: string = require("bootstrap-icons/icons/safe-fill.svg");
const BS_ICON_SAFE2: string = require("bootstrap-icons/icons/safe2.svg");
const BS_ICON_SAFE2_FILL: string = require("bootstrap-icons/icons/safe2-fill.svg");
const BS_ICON_SAVE: string = require("bootstrap-icons/icons/save.svg");
const BS_ICON_SAVE_FILL: string = require("bootstrap-icons/icons/save-fill.svg");
const BS_ICON_SAVE2: string = require("bootstrap-icons/icons/save2.svg");
const BS_ICON_SAVE2_FILL: string = require("bootstrap-icons/icons/save2-fill.svg");
const BS_ICON_SCISSORS: string = require("bootstrap-icons/icons/scissors.svg");
const BS_ICON_SCOOTER: string = require("bootstrap-icons/icons/scooter.svg");
const BS_ICON_SCREWDRIVER: string = require("bootstrap-icons/icons/screwdriver.svg");
const BS_ICON_SD_CARD: string = require("bootstrap-icons/icons/sd-card.svg");
const BS_ICON_SD_CARD_FILL: string = require("bootstrap-icons/icons/sd-card-fill.svg");
const BS_ICON_SEARCH: string = require("bootstrap-icons/icons/search.svg");
const BS_ICON_SEARCH_HEART: string = require("bootstrap-icons/icons/search-heart.svg");
const BS_ICON_SEARCH_HEART_FILL: string = require("bootstrap-icons/icons/search-heart-fill.svg");
const BS_ICON_SEGMENTED_NAV: string = require("bootstrap-icons/icons/segmented-nav.svg");
const BS_ICON_SEND: string = require("bootstrap-icons/icons/send.svg");
const BS_ICON_SEND_ARROW_DOWN: string = require("bootstrap-icons/icons/send-arrow-down.svg");
const BS_ICON_SEND_ARROW_DOWN_FILL: string = require("bootstrap-icons/icons/send-arrow-down-fill.svg");
const BS_ICON_SEND_ARROW_UP: string = require("bootstrap-icons/icons/send-arrow-up.svg");
const BS_ICON_SEND_ARROW_UP_FILL: string = require("bootstrap-icons/icons/send-arrow-up-fill.svg");
const BS_ICON_SEND_CHECK: string = require("bootstrap-icons/icons/send-check.svg");
const BS_ICON_SEND_CHECK_FILL: string = require("bootstrap-icons/icons/send-check-fill.svg");
const BS_ICON_SEND_DASH: string = require("bootstrap-icons/icons/send-dash.svg");
const BS_ICON_SEND_DASH_FILL: string = require("bootstrap-icons/icons/send-dash-fill.svg");
const BS_ICON_SEND_EXCLAMATION: string = require("bootstrap-icons/icons/send-exclamation.svg");
const BS_ICON_SEND_EXCLAMATION_FILL: string = require("bootstrap-icons/icons/send-exclamation-fill.svg");
const BS_ICON_SEND_FILL: string = require("bootstrap-icons/icons/send-fill.svg");
const BS_ICON_SEND_PLUS: string = require("bootstrap-icons/icons/send-plus.svg");
const BS_ICON_SEND_PLUS_FILL: string = require("bootstrap-icons/icons/send-plus-fill.svg");
const BS_ICON_SEND_SLASH: string = require("bootstrap-icons/icons/send-slash.svg");
const BS_ICON_SEND_SLASH_FILL: string = require("bootstrap-icons/icons/send-slash-fill.svg");
const BS_ICON_SEND_X: string = require("bootstrap-icons/icons/send-x.svg");
const BS_ICON_SEND_X_FILL: string = require("bootstrap-icons/icons/send-x-fill.svg");
const BS_ICON_SERVER: string = require("bootstrap-icons/icons/server.svg");
const BS_ICON_SHADOWS: string = require("bootstrap-icons/icons/shadows.svg");
const BS_ICON_SHARE: string = require("bootstrap-icons/icons/share.svg");
const BS_ICON_SHARE_FILL: string = require("bootstrap-icons/icons/share-fill.svg");
const BS_ICON_SHIELD: string = require("bootstrap-icons/icons/shield.svg");
const BS_ICON_SHIELD_CHECK: string = require("bootstrap-icons/icons/shield-check.svg");
const BS_ICON_SHIELD_EXCLAMATION: string = require("bootstrap-icons/icons/shield-exclamation.svg");
const BS_ICON_SHIELD_FILL: string = require("bootstrap-icons/icons/shield-fill.svg");
const BS_ICON_SHIELD_FILL_CHECK: string = require("bootstrap-icons/icons/shield-fill-check.svg");
const BS_ICON_SHIELD_FILL_EXCLAMATION: string = require("bootstrap-icons/icons/shield-fill-exclamation.svg");
const BS_ICON_SHIELD_FILL_MINUS: string = require("bootstrap-icons/icons/shield-fill-minus.svg");
const BS_ICON_SHIELD_FILL_PLUS: string = require("bootstrap-icons/icons/shield-fill-plus.svg");
const BS_ICON_SHIELD_FILL_X: string = require("bootstrap-icons/icons/shield-fill-x.svg");
const BS_ICON_SHIELD_LOCK: string = require("bootstrap-icons/icons/shield-lock.svg");
const BS_ICON_SHIELD_LOCK_FILL: string = require("bootstrap-icons/icons/shield-lock-fill.svg");
const BS_ICON_SHIELD_MINUS: string = require("bootstrap-icons/icons/shield-minus.svg");
const BS_ICON_SHIELD_PLUS: string = require("bootstrap-icons/icons/shield-plus.svg");
const BS_ICON_SHIELD_SHADED: string = require("bootstrap-icons/icons/shield-shaded.svg");
const BS_ICON_SHIELD_SLASH: string = require("bootstrap-icons/icons/shield-slash.svg");
const BS_ICON_SHIELD_SLASH_FILL: string = require("bootstrap-icons/icons/shield-slash-fill.svg");
const BS_ICON_SHIELD_X: string = require("bootstrap-icons/icons/shield-x.svg");
const BS_ICON_SHIFT: string = require("bootstrap-icons/icons/shift.svg");
const BS_ICON_SHIFT_FILL: string = require("bootstrap-icons/icons/shift-fill.svg");
const BS_ICON_SHOP: string = require("bootstrap-icons/icons/shop.svg");
const BS_ICON_SHOP_WINDOW: string = require("bootstrap-icons/icons/shop-window.svg");
const BS_ICON_SHUFFLE: string = require("bootstrap-icons/icons/shuffle.svg");
const BS_ICON_SIGN_DEAD_END: string = require("bootstrap-icons/icons/sign-dead-end.svg");
const BS_ICON_SIGN_DEAD_END_FILL: string = require("bootstrap-icons/icons/sign-dead-end-fill.svg");
const BS_ICON_SIGN_DO_NOT_ENTER: string = require("bootstrap-icons/icons/sign-do-not-enter.svg");
const BS_ICON_SIGN_DO_NOT_ENTER_FILL: string = require("bootstrap-icons/icons/sign-do-not-enter-fill.svg");
const BS_ICON_SIGN_INTERSECTION: string = require("bootstrap-icons/icons/sign-intersection.svg");
const BS_ICON_SIGN_INTERSECTION_FILL: string = require("bootstrap-icons/icons/sign-intersection-fill.svg");
const BS_ICON_SIGN_INTERSECTION_SIDE: string = require("bootstrap-icons/icons/sign-intersection-side.svg");
const BS_ICON_SIGN_INTERSECTION_SIDE_FILL: string = require("bootstrap-icons/icons/sign-intersection-side-fill.svg");
const BS_ICON_SIGN_INTERSECTION_T: string = require("bootstrap-icons/icons/sign-intersection-t.svg");
const BS_ICON_SIGN_INTERSECTION_T_FILL: string = require("bootstrap-icons/icons/sign-intersection-t-fill.svg");
const BS_ICON_SIGN_INTERSECTION_Y: string = require("bootstrap-icons/icons/sign-intersection-y.svg");
const BS_ICON_SIGN_INTERSECTION_Y_FILL: string = require("bootstrap-icons/icons/sign-intersection-y-fill.svg");
const BS_ICON_SIGN_MERGE_LEFT: string = require("bootstrap-icons/icons/sign-merge-left.svg");
const BS_ICON_SIGN_MERGE_LEFT_FILL: string = require("bootstrap-icons/icons/sign-merge-left-fill.svg");
const BS_ICON_SIGN_MERGE_RIGHT: string = require("bootstrap-icons/icons/sign-merge-right.svg");
const BS_ICON_SIGN_MERGE_RIGHT_FILL: string = require("bootstrap-icons/icons/sign-merge-right-fill.svg");
const BS_ICON_SIGN_NO_LEFT_TURN: string = require("bootstrap-icons/icons/sign-no-left-turn.svg");
const BS_ICON_SIGN_NO_LEFT_TURN_FILL: string = require("bootstrap-icons/icons/sign-no-left-turn-fill.svg");
const BS_ICON_SIGN_NO_PARKING: string = require("bootstrap-icons/icons/sign-no-parking.svg");
const BS_ICON_SIGN_NO_PARKING_FILL: string = require("bootstrap-icons/icons/sign-no-parking-fill.svg");
const BS_ICON_SIGN_NO_RIGHT_TURN: string = require("bootstrap-icons/icons/sign-no-right-turn.svg");
const BS_ICON_SIGN_NO_RIGHT_TURN_FILL: string = require("bootstrap-icons/icons/sign-no-right-turn-fill.svg");
const BS_ICON_SIGN_RAILROAD: string = require("bootstrap-icons/icons/sign-railroad.svg");
const BS_ICON_SIGN_RAILROAD_FILL: string = require("bootstrap-icons/icons/sign-railroad-fill.svg");
const BS_ICON_SIGN_STOP: string = require("bootstrap-icons/icons/sign-stop.svg");
const BS_ICON_SIGN_STOP_FILL: string = require("bootstrap-icons/icons/sign-stop-fill.svg");
const BS_ICON_SIGN_STOP_LIGHTS: string = require("bootstrap-icons/icons/sign-stop-lights.svg");
const BS_ICON_SIGN_STOP_LIGHTS_FILL: string = require("bootstrap-icons/icons/sign-stop-lights-fill.svg");
const BS_ICON_SIGN_TURN_LEFT: string = require("bootstrap-icons/icons/sign-turn-left.svg");
const BS_ICON_SIGN_TURN_LEFT_FILL: string = require("bootstrap-icons/icons/sign-turn-left-fill.svg");
const BS_ICON_SIGN_TURN_RIGHT: string = require("bootstrap-icons/icons/sign-turn-right.svg");
const BS_ICON_SIGN_TURN_RIGHT_FILL: string = require("bootstrap-icons/icons/sign-turn-right-fill.svg");
const BS_ICON_SIGN_TURN_SLIGHT_LEFT: string = require("bootstrap-icons/icons/sign-turn-slight-left.svg");
const BS_ICON_SIGN_TURN_SLIGHT_LEFT_FILL: string = require("bootstrap-icons/icons/sign-turn-slight-left-fill.svg");
const BS_ICON_SIGN_TURN_SLIGHT_RIGHT: string = require("bootstrap-icons/icons/sign-turn-slight-right.svg");
const BS_ICON_SIGN_TURN_SLIGHT_RIGHT_FILL: string = require("bootstrap-icons/icons/sign-turn-slight-right-fill.svg");
const BS_ICON_SIGN_YIELD: string = require("bootstrap-icons/icons/sign-yield.svg");
const BS_ICON_SIGN_YIELD_FILL: string = require("bootstrap-icons/icons/sign-yield-fill.svg");
const BS_ICON_SIGNAL: string = require("bootstrap-icons/icons/signal.svg");
const BS_ICON_SIGNPOST: string = require("bootstrap-icons/icons/signpost.svg");
const BS_ICON_SIGNPOST_2: string = require("bootstrap-icons/icons/signpost-2.svg");
const BS_ICON_SIGNPOST_2_FILL: string = require("bootstrap-icons/icons/signpost-2-fill.svg");
const BS_ICON_SIGNPOST_FILL: string = require("bootstrap-icons/icons/signpost-fill.svg");
const BS_ICON_SIGNPOST_SPLIT: string = require("bootstrap-icons/icons/signpost-split.svg");
const BS_ICON_SIGNPOST_SPLIT_FILL: string = require("bootstrap-icons/icons/signpost-split-fill.svg");
const BS_ICON_SIM: string = require("bootstrap-icons/icons/sim.svg");
const BS_ICON_SIM_FILL: string = require("bootstrap-icons/icons/sim-fill.svg");
const BS_ICON_SIM_SLASH: string = require("bootstrap-icons/icons/sim-slash.svg");
const BS_ICON_SIM_SLASH_FILL: string = require("bootstrap-icons/icons/sim-slash-fill.svg");
const BS_ICON_SINA_WEIBO: string = require("bootstrap-icons/icons/sina-weibo.svg");
const BS_ICON_SKIP_BACKWARD: string = require("bootstrap-icons/icons/skip-backward.svg");
const BS_ICON_SKIP_BACKWARD_BTN: string = require("bootstrap-icons/icons/skip-backward-btn.svg");
const BS_ICON_SKIP_BACKWARD_BTN_FILL: string = require("bootstrap-icons/icons/skip-backward-btn-fill.svg");
const BS_ICON_SKIP_BACKWARD_CIRCLE: string = require("bootstrap-icons/icons/skip-backward-circle.svg");
const BS_ICON_SKIP_BACKWARD_CIRCLE_FILL: string = require("bootstrap-icons/icons/skip-backward-circle-fill.svg");
const BS_ICON_SKIP_BACKWARD_FILL: string = require("bootstrap-icons/icons/skip-backward-fill.svg");
const BS_ICON_SKIP_END: string = require("bootstrap-icons/icons/skip-end.svg");
const BS_ICON_SKIP_END_BTN: string = require("bootstrap-icons/icons/skip-end-btn.svg");
const BS_ICON_SKIP_END_BTN_FILL: string = require("bootstrap-icons/icons/skip-end-btn-fill.svg");
const BS_ICON_SKIP_END_CIRCLE: string = require("bootstrap-icons/icons/skip-end-circle.svg");
const BS_ICON_SKIP_END_CIRCLE_FILL: string = require("bootstrap-icons/icons/skip-end-circle-fill.svg");
const BS_ICON_SKIP_END_FILL: string = require("bootstrap-icons/icons/skip-end-fill.svg");
const BS_ICON_SKIP_FORWARD: string = require("bootstrap-icons/icons/skip-forward.svg");
const BS_ICON_SKIP_FORWARD_BTN: string = require("bootstrap-icons/icons/skip-forward-btn.svg");
const BS_ICON_SKIP_FORWARD_BTN_FILL: string = require("bootstrap-icons/icons/skip-forward-btn-fill.svg");
const BS_ICON_SKIP_FORWARD_CIRCLE: string = require("bootstrap-icons/icons/skip-forward-circle.svg");
const BS_ICON_SKIP_FORWARD_CIRCLE_FILL: string = require("bootstrap-icons/icons/skip-forward-circle-fill.svg");
const BS_ICON_SKIP_FORWARD_FILL: string = require("bootstrap-icons/icons/skip-forward-fill.svg");
const BS_ICON_SKIP_START: string = require("bootstrap-icons/icons/skip-start.svg");
const BS_ICON_SKIP_START_BTN: string = require("bootstrap-icons/icons/skip-start-btn.svg");
const BS_ICON_SKIP_START_BTN_FILL: string = require("bootstrap-icons/icons/skip-start-btn-fill.svg");
const BS_ICON_SKIP_START_CIRCLE: string = require("bootstrap-icons/icons/skip-start-circle.svg");
const BS_ICON_SKIP_START_CIRCLE_FILL: string = require("bootstrap-icons/icons/skip-start-circle-fill.svg");
const BS_ICON_SKIP_START_FILL: string = require("bootstrap-icons/icons/skip-start-fill.svg");
const BS_ICON_SKYPE: string = require("bootstrap-icons/icons/skype.svg");
const BS_ICON_SLACK: string = require("bootstrap-icons/icons/slack.svg");
const BS_ICON_SLASH: string = require("bootstrap-icons/icons/slash.svg");
const BS_ICON_SLASH_CIRCLE: string = require("bootstrap-icons/icons/slash-circle.svg");
const BS_ICON_SLASH_CIRCLE_FILL: string = require("bootstrap-icons/icons/slash-circle-fill.svg");
const BS_ICON_SLASH_LG: string = require("bootstrap-icons/icons/slash-lg.svg");
const BS_ICON_SLASH_SQUARE: string = require("bootstrap-icons/icons/slash-square.svg");
const BS_ICON_SLASH_SQUARE_FILL: string = require("bootstrap-icons/icons/slash-square-fill.svg");
const BS_ICON_SLIDERS: string = require("bootstrap-icons/icons/sliders.svg");
const BS_ICON_SLIDERS2: string = require("bootstrap-icons/icons/sliders2.svg");
const BS_ICON_SLIDERS2_VERTICAL: string = require("bootstrap-icons/icons/sliders2-vertical.svg");
const BS_ICON_SMARTWATCH: string = require("bootstrap-icons/icons/smartwatch.svg");
const BS_ICON_SNAPCHAT: string = require("bootstrap-icons/icons/snapchat.svg");
const BS_ICON_SNOW: string = require("bootstrap-icons/icons/snow.svg");
const BS_ICON_SNOW2: string = require("bootstrap-icons/icons/snow2.svg");
const BS_ICON_SNOW3: string = require("bootstrap-icons/icons/snow3.svg");
const BS_ICON_SORT_ALPHA_DOWN: string = require("bootstrap-icons/icons/sort-alpha-down.svg");
const BS_ICON_SORT_ALPHA_DOWN_ALT: string = require("bootstrap-icons/icons/sort-alpha-down-alt.svg");
const BS_ICON_SORT_ALPHA_UP: string = require("bootstrap-icons/icons/sort-alpha-up.svg");
const BS_ICON_SORT_ALPHA_UP_ALT: string = require("bootstrap-icons/icons/sort-alpha-up-alt.svg");
const BS_ICON_SORT_DOWN: string = require("bootstrap-icons/icons/sort-down.svg");
const BS_ICON_SORT_DOWN_ALT: string = require("bootstrap-icons/icons/sort-down-alt.svg");
const BS_ICON_SORT_NUMERIC_DOWN: string = require("bootstrap-icons/icons/sort-numeric-down.svg");
const BS_ICON_SORT_NUMERIC_DOWN_ALT: string = require("bootstrap-icons/icons/sort-numeric-down-alt.svg");
const BS_ICON_SORT_NUMERIC_UP: string = require("bootstrap-icons/icons/sort-numeric-up.svg");
const BS_ICON_SORT_NUMERIC_UP_ALT: string = require("bootstrap-icons/icons/sort-numeric-up-alt.svg");
const BS_ICON_SORT_UP: string = require("bootstrap-icons/icons/sort-up.svg");
const BS_ICON_SORT_UP_ALT: string = require("bootstrap-icons/icons/sort-up-alt.svg");
const BS_ICON_SOUNDWAVE: string = require("bootstrap-icons/icons/soundwave.svg");
const BS_ICON_SOURCEFORGE: string = require("bootstrap-icons/icons/sourceforge.svg");
const BS_ICON_SPEAKER: string = require("bootstrap-icons/icons/speaker.svg");
const BS_ICON_SPEAKER_FILL: string = require("bootstrap-icons/icons/speaker-fill.svg");
const BS_ICON_SPEEDOMETER: string = require("bootstrap-icons/icons/speedometer.svg");
const BS_ICON_SPEEDOMETER2: string = require("bootstrap-icons/icons/speedometer2.svg");
const BS_ICON_SPELLCHECK: string = require("bootstrap-icons/icons/spellcheck.svg");
const BS_ICON_SPOTIFY: string = require("bootstrap-icons/icons/spotify.svg");
const BS_ICON_SQUARE: string = require("bootstrap-icons/icons/square.svg");
const BS_ICON_SQUARE_FILL: string = require("bootstrap-icons/icons/square-fill.svg");
const BS_ICON_SQUARE_HALF: string = require("bootstrap-icons/icons/square-half.svg");
const BS_ICON_STACK: string = require("bootstrap-icons/icons/stack.svg");
const BS_ICON_STACK_OVERFLOW: string = require("bootstrap-icons/icons/stack-overflow.svg");
const BS_ICON_STAR: string = require("bootstrap-icons/icons/star.svg");
const BS_ICON_STAR_FILL: string = require("bootstrap-icons/icons/star-fill.svg");
const BS_ICON_STAR_HALF: string = require("bootstrap-icons/icons/star-half.svg");
const BS_ICON_STARS: string = require("bootstrap-icons/icons/stars.svg");
const BS_ICON_STEAM: string = require("bootstrap-icons/icons/steam.svg");
const BS_ICON_STICKIES: string = require("bootstrap-icons/icons/stickies.svg");
const BS_ICON_STICKIES_FILL: string = require("bootstrap-icons/icons/stickies-fill.svg");
const BS_ICON_STICKY: string = require("bootstrap-icons/icons/sticky.svg");
const BS_ICON_STICKY_FILL: string = require("bootstrap-icons/icons/sticky-fill.svg");
const BS_ICON_STOP: string = require("bootstrap-icons/icons/stop.svg");
const BS_ICON_STOP_BTN: string = require("bootstrap-icons/icons/stop-btn.svg");
const BS_ICON_STOP_BTN_FILL: string = require("bootstrap-icons/icons/stop-btn-fill.svg");
const BS_ICON_STOP_CIRCLE: string = require("bootstrap-icons/icons/stop-circle.svg");
const BS_ICON_STOP_CIRCLE_FILL: string = require("bootstrap-icons/icons/stop-circle-fill.svg");
const BS_ICON_STOP_FILL: string = require("bootstrap-icons/icons/stop-fill.svg");
const BS_ICON_STOPLIGHTS: string = require("bootstrap-icons/icons/stoplights.svg");
const BS_ICON_STOPLIGHTS_FILL: string = require("bootstrap-icons/icons/stoplights-fill.svg");
const BS_ICON_STOPWATCH: string = require("bootstrap-icons/icons/stopwatch.svg");
const BS_ICON_STOPWATCH_FILL: string = require("bootstrap-icons/icons/stopwatch-fill.svg");
const BS_ICON_STRAVA: string = require("bootstrap-icons/icons/strava.svg");
const BS_ICON_STRIPE: string = require("bootstrap-icons/icons/stripe.svg");
const BS_ICON_SUBSCRIPT: string = require("bootstrap-icons/icons/subscript.svg");
const BS_ICON_SUBSTACK: string = require("bootstrap-icons/icons/substack.svg");
const BS_ICON_SUBTRACT: string = require("bootstrap-icons/icons/subtract.svg");
const BS_ICON_SUIT_CLUB: string = require("bootstrap-icons/icons/suit-club.svg");
const BS_ICON_SUIT_CLUB_FILL: string = require("bootstrap-icons/icons/suit-club-fill.svg");
const BS_ICON_SUIT_DIAMOND: string = require("bootstrap-icons/icons/suit-diamond.svg");
const BS_ICON_SUIT_DIAMOND_FILL: string = require("bootstrap-icons/icons/suit-diamond-fill.svg");
const BS_ICON_SUIT_HEART: string = require("bootstrap-icons/icons/suit-heart.svg");
const BS_ICON_SUIT_HEART_FILL: string = require("bootstrap-icons/icons/suit-heart-fill.svg");
const BS_ICON_SUIT_SPADE: string = require("bootstrap-icons/icons/suit-spade.svg");
const BS_ICON_SUIT_SPADE_FILL: string = require("bootstrap-icons/icons/suit-spade-fill.svg");
const BS_ICON_SUITCASE: string = require("bootstrap-icons/icons/suitcase.svg");
const BS_ICON_SUITCASE_FILL: string = require("bootstrap-icons/icons/suitcase-fill.svg");
const BS_ICON_SUITCASE_LG: string = require("bootstrap-icons/icons/suitcase-lg.svg");
const BS_ICON_SUITCASE_LG_FILL: string = require("bootstrap-icons/icons/suitcase-lg-fill.svg");
const BS_ICON_SUITCASE2: string = require("bootstrap-icons/icons/suitcase2.svg");
const BS_ICON_SUITCASE2_FILL: string = require("bootstrap-icons/icons/suitcase2-fill.svg");
const BS_ICON_SUN: string = require("bootstrap-icons/icons/sun.svg");
const BS_ICON_SUN_FILL: string = require("bootstrap-icons/icons/sun-fill.svg");
const BS_ICON_SUNGLASSES: string = require("bootstrap-icons/icons/sunglasses.svg");
const BS_ICON_SUNRISE: string = require("bootstrap-icons/icons/sunrise.svg");
const BS_ICON_SUNRISE_FILL: string = require("bootstrap-icons/icons/sunrise-fill.svg");
const BS_ICON_SUNSET: string = require("bootstrap-icons/icons/sunset.svg");
const BS_ICON_SUNSET_FILL: string = require("bootstrap-icons/icons/sunset-fill.svg");
const BS_ICON_SUPERSCRIPT: string = require("bootstrap-icons/icons/superscript.svg");
const BS_ICON_SYMMETRY_HORIZONTAL: string = require("bootstrap-icons/icons/symmetry-horizontal.svg");
const BS_ICON_SYMMETRY_VERTICAL: string = require("bootstrap-icons/icons/symmetry-vertical.svg");
const BS_ICON_TABLE: string = require("bootstrap-icons/icons/table.svg");
const BS_ICON_TABLET: string = require("bootstrap-icons/icons/tablet.svg");
const BS_ICON_TABLET_FILL: string = require("bootstrap-icons/icons/tablet-fill.svg");
const BS_ICON_TABLET_LANDSCAPE: string = require("bootstrap-icons/icons/tablet-landscape.svg");
const BS_ICON_TABLET_LANDSCAPE_FILL: string = require("bootstrap-icons/icons/tablet-landscape-fill.svg");
const BS_ICON_TAG: string = require("bootstrap-icons/icons/tag.svg");
const BS_ICON_TAG_FILL: string = require("bootstrap-icons/icons/tag-fill.svg");
const BS_ICON_TAGS: string = require("bootstrap-icons/icons/tags.svg");
const BS_ICON_TAGS_FILL: string = require("bootstrap-icons/icons/tags-fill.svg");
const BS_ICON_TAXI_FRONT: string = require("bootstrap-icons/icons/taxi-front.svg");
const BS_ICON_TAXI_FRONT_FILL: string = require("bootstrap-icons/icons/taxi-front-fill.svg");
const BS_ICON_TELEGRAM: string = require("bootstrap-icons/icons/telegram.svg");
const BS_ICON_TELEPHONE: string = require("bootstrap-icons/icons/telephone.svg");
const BS_ICON_TELEPHONE_FILL: string = require("bootstrap-icons/icons/telephone-fill.svg");
const BS_ICON_TELEPHONE_FORWARD: string = require("bootstrap-icons/icons/telephone-forward.svg");
const BS_ICON_TELEPHONE_FORWARD_FILL: string = require("bootstrap-icons/icons/telephone-forward-fill.svg");
const BS_ICON_TELEPHONE_INBOUND: string = require("bootstrap-icons/icons/telephone-inbound.svg");
const BS_ICON_TELEPHONE_INBOUND_FILL: string = require("bootstrap-icons/icons/telephone-inbound-fill.svg");
const BS_ICON_TELEPHONE_MINUS: string = require("bootstrap-icons/icons/telephone-minus.svg");
const BS_ICON_TELEPHONE_MINUS_FILL: string = require("bootstrap-icons/icons/telephone-minus-fill.svg");
const BS_ICON_TELEPHONE_OUTBOUND: string = require("bootstrap-icons/icons/telephone-outbound.svg");
const BS_ICON_TELEPHONE_OUTBOUND_FILL: string = require("bootstrap-icons/icons/telephone-outbound-fill.svg");
const BS_ICON_TELEPHONE_PLUS: string = require("bootstrap-icons/icons/telephone-plus.svg");
const BS_ICON_TELEPHONE_PLUS_FILL: string = require("bootstrap-icons/icons/telephone-plus-fill.svg");
const BS_ICON_TELEPHONE_X: string = require("bootstrap-icons/icons/telephone-x.svg");
const BS_ICON_TELEPHONE_X_FILL: string = require("bootstrap-icons/icons/telephone-x-fill.svg");
const BS_ICON_TENCENT_QQ: string = require("bootstrap-icons/icons/tencent-qq.svg");
const BS_ICON_TERMINAL: string = require("bootstrap-icons/icons/terminal.svg");
const BS_ICON_TERMINAL_DASH: string = require("bootstrap-icons/icons/terminal-dash.svg");
const BS_ICON_TERMINAL_FILL: string = require("bootstrap-icons/icons/terminal-fill.svg");
const BS_ICON_TERMINAL_PLUS: string = require("bootstrap-icons/icons/terminal-plus.svg");
const BS_ICON_TERMINAL_SPLIT: string = require("bootstrap-icons/icons/terminal-split.svg");
const BS_ICON_TERMINAL_X: string = require("bootstrap-icons/icons/terminal-x.svg");
const BS_ICON_TEXT_CENTER: string = require("bootstrap-icons/icons/text-center.svg");
const BS_ICON_TEXT_INDENT_LEFT: string = require("bootstrap-icons/icons/text-indent-left.svg");
const BS_ICON_TEXT_INDENT_RIGHT: string = require("bootstrap-icons/icons/text-indent-right.svg");
const BS_ICON_TEXT_LEFT: string = require("bootstrap-icons/icons/text-left.svg");
const BS_ICON_TEXT_PARAGRAPH: string = require("bootstrap-icons/icons/text-paragraph.svg");
const BS_ICON_TEXT_RIGHT: string = require("bootstrap-icons/icons/text-right.svg");
const BS_ICON_TEXT_WRAP: string = require("bootstrap-icons/icons/text-wrap.svg");
const BS_ICON_TEXTAREA: string = require("bootstrap-icons/icons/textarea.svg");
const BS_ICON_TEXTAREA_RESIZE: string = require("bootstrap-icons/icons/textarea-resize.svg");
const BS_ICON_TEXTAREA_T: string = require("bootstrap-icons/icons/textarea-t.svg");
const BS_ICON_THERMOMETER: string = require("bootstrap-icons/icons/thermometer.svg");
const BS_ICON_THERMOMETER_HALF: string = require("bootstrap-icons/icons/thermometer-half.svg");
const BS_ICON_THERMOMETER_HIGH: string = require("bootstrap-icons/icons/thermometer-high.svg");
const BS_ICON_THERMOMETER_LOW: string = require("bootstrap-icons/icons/thermometer-low.svg");
const BS_ICON_THERMOMETER_SNOW: string = require("bootstrap-icons/icons/thermometer-snow.svg");
const BS_ICON_THERMOMETER_SUN: string = require("bootstrap-icons/icons/thermometer-sun.svg");
const BS_ICON_THREADS: string = require("bootstrap-icons/icons/threads.svg");
const BS_ICON_THREADS_FILL: string = require("bootstrap-icons/icons/threads-fill.svg");
const BS_ICON_THREE_DOTS: string = require("bootstrap-icons/icons/three-dots.svg");
const BS_ICON_THREE_DOTS_VERTICAL: string = require("bootstrap-icons/icons/three-dots-vertical.svg");
const BS_ICON_THUNDERBOLT: string = require("bootstrap-icons/icons/thunderbolt.svg");
const BS_ICON_THUNDERBOLT_FILL: string = require("bootstrap-icons/icons/thunderbolt-fill.svg");
const BS_ICON_TICKET: string = require("bootstrap-icons/icons/ticket.svg");
const BS_ICON_TICKET_DETAILED: string = require("bootstrap-icons/icons/ticket-detailed.svg");
const BS_ICON_TICKET_DETAILED_FILL: string = require("bootstrap-icons/icons/ticket-detailed-fill.svg");
const BS_ICON_TICKET_FILL: string = require("bootstrap-icons/icons/ticket-fill.svg");
const BS_ICON_TICKET_PERFORATED: string = require("bootstrap-icons/icons/ticket-perforated.svg");
const BS_ICON_TICKET_PERFORATED_FILL: string = require("bootstrap-icons/icons/ticket-perforated-fill.svg");
const BS_ICON_TIKTOK: string = require("bootstrap-icons/icons/tiktok.svg");
const BS_ICON_TOGGLE_OFF: string = require("bootstrap-icons/icons/toggle-off.svg");
const BS_ICON_TOGGLE_ON: string = require("bootstrap-icons/icons/toggle-on.svg");
const BS_ICON_TOGGLE2_OFF: string = require("bootstrap-icons/icons/toggle2-off.svg");
const BS_ICON_TOGGLE2_ON: string = require("bootstrap-icons/icons/toggle2-on.svg");
const BS_ICON_TOGGLES: string = require("bootstrap-icons/icons/toggles.svg");
const BS_ICON_TOGGLES2: string = require("bootstrap-icons/icons/toggles2.svg");
const BS_ICON_TOOLS: string = require("bootstrap-icons/icons/tools.svg");
const BS_ICON_TORNADO: string = require("bootstrap-icons/icons/tornado.svg");
const BS_ICON_TRAIN_FREIGHT_FRONT: string = require("bootstrap-icons/icons/train-freight-front.svg");
const BS_ICON_TRAIN_FREIGHT_FRONT_FILL: string = require("bootstrap-icons/icons/train-freight-front-fill.svg");
const BS_ICON_TRAIN_FRONT: string = require("bootstrap-icons/icons/train-front.svg");
const BS_ICON_TRAIN_FRONT_FILL: string = require("bootstrap-icons/icons/train-front-fill.svg");
const BS_ICON_TRAIN_LIGHTRAIL_FRONT: string = require("bootstrap-icons/icons/train-lightrail-front.svg");
const BS_ICON_TRAIN_LIGHTRAIL_FRONT_FILL: string = require("bootstrap-icons/icons/train-lightrail-front-fill.svg");
const BS_ICON_TRANSLATE: string = require("bootstrap-icons/icons/translate.svg");
const BS_ICON_TRANSPARENCY: string = require("bootstrap-icons/icons/transparency.svg");
const BS_ICON_TRASH: string = require("bootstrap-icons/icons/trash.svg");
const BS_ICON_TRASH_FILL: string = require("bootstrap-icons/icons/trash-fill.svg");
const BS_ICON_TRASH2: string = require("bootstrap-icons/icons/trash2.svg");
const BS_ICON_TRASH2_FILL: string = require("bootstrap-icons/icons/trash2-fill.svg");
const BS_ICON_TRASH3: string = require("bootstrap-icons/icons/trash3.svg");
const BS_ICON_TRASH3_FILL: string = require("bootstrap-icons/icons/trash3-fill.svg");
const BS_ICON_TREE: string = require("bootstrap-icons/icons/tree.svg");
const BS_ICON_TREE_FILL: string = require("bootstrap-icons/icons/tree-fill.svg");
const BS_ICON_TRELLO: string = require("bootstrap-icons/icons/trello.svg");
const BS_ICON_TRIANGLE: string = require("bootstrap-icons/icons/triangle.svg");
const BS_ICON_TRIANGLE_FILL: string = require("bootstrap-icons/icons/triangle-fill.svg");
const BS_ICON_TRIANGLE_HALF: string = require("bootstrap-icons/icons/triangle-half.svg");
const BS_ICON_TROPHY: string = require("bootstrap-icons/icons/trophy.svg");
const BS_ICON_TROPHY_FILL: string = require("bootstrap-icons/icons/trophy-fill.svg");
const BS_ICON_TROPICAL_STORM: string = require("bootstrap-icons/icons/tropical-storm.svg");
const BS_ICON_TRUCK: string = require("bootstrap-icons/icons/truck.svg");
const BS_ICON_TRUCK_FLATBED: string = require("bootstrap-icons/icons/truck-flatbed.svg");
const BS_ICON_TRUCK_FRONT: string = require("bootstrap-icons/icons/truck-front.svg");
const BS_ICON_TRUCK_FRONT_FILL: string = require("bootstrap-icons/icons/truck-front-fill.svg");
const BS_ICON_TSUNAMI: string = require("bootstrap-icons/icons/tsunami.svg");
const BS_ICON_TV: string = require("bootstrap-icons/icons/tv.svg");
const BS_ICON_TV_FILL: string = require("bootstrap-icons/icons/tv-fill.svg");
const BS_ICON_TWITCH: string = require("bootstrap-icons/icons/twitch.svg");
const BS_ICON_TWITTER: string = require("bootstrap-icons/icons/twitter.svg");
const BS_ICON_TWITTER_X: string = require("bootstrap-icons/icons/twitter-x.svg");
const BS_ICON_TYPE: string = require("bootstrap-icons/icons/type.svg");
const BS_ICON_TYPE_BOLD: string = require("bootstrap-icons/icons/type-bold.svg");
const BS_ICON_TYPE_H1: string = require("bootstrap-icons/icons/type-h1.svg");
const BS_ICON_TYPE_H2: string = require("bootstrap-icons/icons/type-h2.svg");
const BS_ICON_TYPE_H3: string = require("bootstrap-icons/icons/type-h3.svg");
const BS_ICON_TYPE_H4: string = require("bootstrap-icons/icons/type-h4.svg");
const BS_ICON_TYPE_H5: string = require("bootstrap-icons/icons/type-h5.svg");
const BS_ICON_TYPE_H6: string = require("bootstrap-icons/icons/type-h6.svg");
const BS_ICON_TYPE_ITALIC: string = require("bootstrap-icons/icons/type-italic.svg");
const BS_ICON_TYPE_STRIKETHROUGH: string = require("bootstrap-icons/icons/type-strikethrough.svg");
const BS_ICON_TYPE_UNDERLINE: string = require("bootstrap-icons/icons/type-underline.svg");
const BS_ICON_UBUNTU: string = require("bootstrap-icons/icons/ubuntu.svg");
const BS_ICON_UI_CHECKS: string = require("bootstrap-icons/icons/ui-checks.svg");
const BS_ICON_UI_CHECKS_GRID: string = require("bootstrap-icons/icons/ui-checks-grid.svg");
const BS_ICON_UI_RADIOS: string = require("bootstrap-icons/icons/ui-radios.svg");
const BS_ICON_UI_RADIOS_GRID: string = require("bootstrap-icons/icons/ui-radios-grid.svg");
const BS_ICON_UMBRELLA: string = require("bootstrap-icons/icons/umbrella.svg");
const BS_ICON_UMBRELLA_FILL: string = require("bootstrap-icons/icons/umbrella-fill.svg");
const BS_ICON_UNINDENT: string = require("bootstrap-icons/icons/unindent.svg");
const BS_ICON_UNION: string = require("bootstrap-icons/icons/union.svg");
const BS_ICON_UNITY: string = require("bootstrap-icons/icons/unity.svg");
const BS_ICON_UNIVERSAL_ACCESS: string = require("bootstrap-icons/icons/universal-access.svg");
const BS_ICON_UNIVERSAL_ACCESS_CIRCLE: string = require("bootstrap-icons/icons/universal-access-circle.svg");
const BS_ICON_UNLOCK: string = require("bootstrap-icons/icons/unlock.svg");
const BS_ICON_UNLOCK_FILL: string = require("bootstrap-icons/icons/unlock-fill.svg");
const BS_ICON_UPC: string = require("bootstrap-icons/icons/upc.svg");
const BS_ICON_UPC_SCAN: string = require("bootstrap-icons/icons/upc-scan.svg");
const BS_ICON_UPLOAD: string = require("bootstrap-icons/icons/upload.svg");
const BS_ICON_USB: string = require("bootstrap-icons/icons/usb.svg");
const BS_ICON_USB_C: string = require("bootstrap-icons/icons/usb-c.svg");
const BS_ICON_USB_C_FILL: string = require("bootstrap-icons/icons/usb-c-fill.svg");
const BS_ICON_USB_DRIVE: string = require("bootstrap-icons/icons/usb-drive.svg");
const BS_ICON_USB_DRIVE_FILL: string = require("bootstrap-icons/icons/usb-drive-fill.svg");
const BS_ICON_USB_FILL: string = require("bootstrap-icons/icons/usb-fill.svg");
const BS_ICON_USB_MICRO: string = require("bootstrap-icons/icons/usb-micro.svg");
const BS_ICON_USB_MICRO_FILL: string = require("bootstrap-icons/icons/usb-micro-fill.svg");
const BS_ICON_USB_MINI: string = require("bootstrap-icons/icons/usb-mini.svg");
const BS_ICON_USB_MINI_FILL: string = require("bootstrap-icons/icons/usb-mini-fill.svg");
const BS_ICON_USB_PLUG: string = require("bootstrap-icons/icons/usb-plug.svg");
const BS_ICON_USB_PLUG_FILL: string = require("bootstrap-icons/icons/usb-plug-fill.svg");
const BS_ICON_USB_SYMBOL: string = require("bootstrap-icons/icons/usb-symbol.svg");
const BS_ICON_VALENTINE: string = require("bootstrap-icons/icons/valentine.svg");
const BS_ICON_VALENTINE2: string = require("bootstrap-icons/icons/valentine2.svg");
const BS_ICON_VECTOR_PEN: string = require("bootstrap-icons/icons/vector-pen.svg");
const BS_ICON_VIEW_LIST: string = require("bootstrap-icons/icons/view-list.svg");
const BS_ICON_VIEW_STACKED: string = require("bootstrap-icons/icons/view-stacked.svg");
const BS_ICON_VIGNETTE: string = require("bootstrap-icons/icons/vignette.svg");
const BS_ICON_VIMEO: string = require("bootstrap-icons/icons/vimeo.svg");
const BS_ICON_VINYL: string = require("bootstrap-icons/icons/vinyl.svg");
const BS_ICON_VINYL_FILL: string = require("bootstrap-icons/icons/vinyl-fill.svg");
const BS_ICON_VIRUS: string = require("bootstrap-icons/icons/virus.svg");
const BS_ICON_VIRUS2: string = require("bootstrap-icons/icons/virus2.svg");
const BS_ICON_VOICEMAIL: string = require("bootstrap-icons/icons/voicemail.svg");
const BS_ICON_VOLUME_DOWN: string = require("bootstrap-icons/icons/volume-down.svg");
const BS_ICON_VOLUME_DOWN_FILL: string = require("bootstrap-icons/icons/volume-down-fill.svg");
const BS_ICON_VOLUME_MUTE: string = require("bootstrap-icons/icons/volume-mute.svg");
const BS_ICON_VOLUME_MUTE_FILL: string = require("bootstrap-icons/icons/volume-mute-fill.svg");
const BS_ICON_VOLUME_OFF: string = require("bootstrap-icons/icons/volume-off.svg");
const BS_ICON_VOLUME_OFF_FILL: string = require("bootstrap-icons/icons/volume-off-fill.svg");
const BS_ICON_VOLUME_UP: string = require("bootstrap-icons/icons/volume-up.svg");
const BS_ICON_VOLUME_UP_FILL: string = require("bootstrap-icons/icons/volume-up-fill.svg");
const BS_ICON_VR: string = require("bootstrap-icons/icons/vr.svg");
const BS_ICON_WALLET: string = require("bootstrap-icons/icons/wallet.svg");
const BS_ICON_WALLET_FILL: string = require("bootstrap-icons/icons/wallet-fill.svg");
const BS_ICON_WALLET2: string = require("bootstrap-icons/icons/wallet2.svg");
const BS_ICON_WATCH: string = require("bootstrap-icons/icons/watch.svg");
const BS_ICON_WATER: string = require("bootstrap-icons/icons/water.svg");
const BS_ICON_WEBCAM: string = require("bootstrap-icons/icons/webcam.svg");
const BS_ICON_WEBCAM_FILL: string = require("bootstrap-icons/icons/webcam-fill.svg");
const BS_ICON_WECHAT: string = require("bootstrap-icons/icons/wechat.svg");
const BS_ICON_WHATSAPP: string = require("bootstrap-icons/icons/whatsapp.svg");
const BS_ICON_WIFI: string = require("bootstrap-icons/icons/wifi.svg");
const BS_ICON_WIFI_1: string = require("bootstrap-icons/icons/wifi-1.svg");
const BS_ICON_WIFI_2: string = require("bootstrap-icons/icons/wifi-2.svg");
const BS_ICON_WIFI_OFF: string = require("bootstrap-icons/icons/wifi-off.svg");
const BS_ICON_WIKIPEDIA: string = require("bootstrap-icons/icons/wikipedia.svg");
const BS_ICON_WIND: string = require("bootstrap-icons/icons/wind.svg");
const BS_ICON_WINDOW: string = require("bootstrap-icons/icons/window.svg");
const BS_ICON_WINDOW_DASH: string = require("bootstrap-icons/icons/window-dash.svg");
const BS_ICON_WINDOW_DESKTOP: string = require("bootstrap-icons/icons/window-desktop.svg");
const BS_ICON_WINDOW_DOCK: string = require("bootstrap-icons/icons/window-dock.svg");
const BS_ICON_WINDOW_FULLSCREEN: string = require("bootstrap-icons/icons/window-fullscreen.svg");
const BS_ICON_WINDOW_PLUS: string = require("bootstrap-icons/icons/window-plus.svg");
const BS_ICON_WINDOW_SIDEBAR: string = require("bootstrap-icons/icons/window-sidebar.svg");
const BS_ICON_WINDOW_SPLIT: string = require("bootstrap-icons/icons/window-split.svg");
const BS_ICON_WINDOW_STACK: string = require("bootstrap-icons/icons/window-stack.svg");
const BS_ICON_WINDOW_X: string = require("bootstrap-icons/icons/window-x.svg");
const BS_ICON_WINDOWS: string = require("bootstrap-icons/icons/windows.svg");
const BS_ICON_WORDPRESS: string = require("bootstrap-icons/icons/wordpress.svg");
const BS_ICON_WRENCH: string = require("bootstrap-icons/icons/wrench.svg");
const BS_ICON_WRENCH_ADJUSTABLE: string = require("bootstrap-icons/icons/wrench-adjustable.svg");
const BS_ICON_WRENCH_ADJUSTABLE_CIRCLE: string = require("bootstrap-icons/icons/wrench-adjustable-circle.svg");
const BS_ICON_WRENCH_ADJUSTABLE_CIRCLE_FILL: string = require("bootstrap-icons/icons/wrench-adjustable-circle-fill.svg");
const BS_ICON_X: string = require("bootstrap-icons/icons/x.svg");
const BS_ICON_X_CIRCLE: string = require("bootstrap-icons/icons/x-circle.svg");
const BS_ICON_X_CIRCLE_FILL: string = require("bootstrap-icons/icons/x-circle-fill.svg");
const BS_ICON_X_DIAMOND: string = require("bootstrap-icons/icons/x-diamond.svg");
const BS_ICON_X_DIAMOND_FILL: string = require("bootstrap-icons/icons/x-diamond-fill.svg");
const BS_ICON_X_LG: string = require("bootstrap-icons/icons/x-lg.svg");
const BS_ICON_X_OCTAGON: string = require("bootstrap-icons/icons/x-octagon.svg");
const BS_ICON_X_OCTAGON_FILL: string = require("bootstrap-icons/icons/x-octagon-fill.svg");
const BS_ICON_X_SQUARE: string = require("bootstrap-icons/icons/x-square.svg");
const BS_ICON_X_SQUARE_FILL: string = require("bootstrap-icons/icons/x-square-fill.svg");
const BS_ICON_XBOX: string = require("bootstrap-icons/icons/xbox.svg");
const BS_ICON_YELP: string = require("bootstrap-icons/icons/yelp.svg");
const BS_ICON_YIN_YANG: string = require("bootstrap-icons/icons/yin-yang.svg");
const BS_ICON_YOUTUBE: string = require("bootstrap-icons/icons/youtube.svg");
const BS_ICON_ZOOM_IN: string = require("bootstrap-icons/icons/zoom-in.svg");
const BS_ICON_ZOOM_OUT: string = require("bootstrap-icons/icons/zoom-out.svg");
export const BOOTSTRAP_ICONS: Record<string, string> = {
    "0-circle": BS_ICON_0_CIRCLE,
    "0-circle-fill": BS_ICON_0_CIRCLE_FILL,
    "0-square": BS_ICON_0_SQUARE,
    "0-square-fill": BS_ICON_0_SQUARE_FILL,
    "1-circle": BS_ICON_1_CIRCLE,
    "1-circle-fill": BS_ICON_1_CIRCLE_FILL,
    "1-square": BS_ICON_1_SQUARE,
    "1-square-fill": BS_ICON_1_SQUARE_FILL,
    "123": BS_ICON_123,
    "2-circle": BS_ICON_2_CIRCLE,
    "2-circle-fill": BS_ICON_2_CIRCLE_FILL,
    "2-square": BS_ICON_2_SQUARE,
    "2-square-fill": BS_ICON_2_SQUARE_FILL,
    "3-circle": BS_ICON_3_CIRCLE,
    "3-circle-fill": BS_ICON_3_CIRCLE_FILL,
    "3-square": BS_ICON_3_SQUARE,
    "3-square-fill": BS_ICON_3_SQUARE_FILL,
    "4-circle": BS_ICON_4_CIRCLE,
    "4-circle-fill": BS_ICON_4_CIRCLE_FILL,
    "4-square": BS_ICON_4_SQUARE,
    "4-square-fill": BS_ICON_4_SQUARE_FILL,
    "5-circle": BS_ICON_5_CIRCLE,
    "5-circle-fill": BS_ICON_5_CIRCLE_FILL,
    "5-square": BS_ICON_5_SQUARE,
    "5-square-fill": BS_ICON_5_SQUARE_FILL,
    "6-circle": BS_ICON_6_CIRCLE,
    "6-circle-fill": BS_ICON_6_CIRCLE_FILL,
    "6-square": BS_ICON_6_SQUARE,
    "6-square-fill": BS_ICON_6_SQUARE_FILL,
    "7-circle": BS_ICON_7_CIRCLE,
    "7-circle-fill": BS_ICON_7_CIRCLE_FILL,
    "7-square": BS_ICON_7_SQUARE,
    "7-square-fill": BS_ICON_7_SQUARE_FILL,
    "8-circle": BS_ICON_8_CIRCLE,
    "8-circle-fill": BS_ICON_8_CIRCLE_FILL,
    "8-square": BS_ICON_8_SQUARE,
    "8-square-fill": BS_ICON_8_SQUARE_FILL,
    "9-circle": BS_ICON_9_CIRCLE,
    "9-circle-fill": BS_ICON_9_CIRCLE_FILL,
    "9-square": BS_ICON_9_SQUARE,
    "9-square-fill": BS_ICON_9_SQUARE_FILL,
    "activity": BS_ICON_ACTIVITY,
    "airplane": BS_ICON_AIRPLANE,
    "airplane-engines": BS_ICON_AIRPLANE_ENGINES,
    "airplane-engines-fill": BS_ICON_AIRPLANE_ENGINES_FILL,
    "airplane-fill": BS_ICON_AIRPLANE_FILL,
    "alarm": BS_ICON_ALARM,
    "alarm-fill": BS_ICON_ALARM_FILL,
    "alexa": BS_ICON_ALEXA,
    "align-bottom": BS_ICON_ALIGN_BOTTOM,
    "align-center": BS_ICON_ALIGN_CENTER,
    "align-end": BS_ICON_ALIGN_END,
    "align-middle": BS_ICON_ALIGN_MIDDLE,
    "align-start": BS_ICON_ALIGN_START,
    "align-top": BS_ICON_ALIGN_TOP,
    "alipay": BS_ICON_ALIPAY,
    "alphabet": BS_ICON_ALPHABET,
    "alphabet-uppercase": BS_ICON_ALPHABET_UPPERCASE,
    "alt": BS_ICON_ALT,
    "amazon": BS_ICON_AMAZON,
    "amd": BS_ICON_AMD,
    "android": BS_ICON_ANDROID,
    "android2": BS_ICON_ANDROID2,
    "app": BS_ICON_APP,
    "app-indicator": BS_ICON_APP_INDICATOR,
    "apple": BS_ICON_APPLE,
    "archive": BS_ICON_ARCHIVE,
    "archive-fill": BS_ICON_ARCHIVE_FILL,
    "arrow-90deg-down": BS_ICON_ARROW_90DEG_DOWN,
    "arrow-90deg-left": BS_ICON_ARROW_90DEG_LEFT,
    "arrow-90deg-right": BS_ICON_ARROW_90DEG_RIGHT,
    "arrow-90deg-up": BS_ICON_ARROW_90DEG_UP,
    "arrow-bar-down": BS_ICON_ARROW_BAR_DOWN,
    "arrow-bar-left": BS_ICON_ARROW_BAR_LEFT,
    "arrow-bar-right": BS_ICON_ARROW_BAR_RIGHT,
    "arrow-bar-up": BS_ICON_ARROW_BAR_UP,
    "arrow-clockwise": BS_ICON_ARROW_CLOCKWISE,
    "arrow-counterclockwise": BS_ICON_ARROW_COUNTERCLOCKWISE,
    "arrow-down": BS_ICON_ARROW_DOWN,
    "arrow-down-circle": BS_ICON_ARROW_DOWN_CIRCLE,
    "arrow-down-circle-fill": BS_ICON_ARROW_DOWN_CIRCLE_FILL,
    "arrow-down-left": BS_ICON_ARROW_DOWN_LEFT,
    "arrow-down-left-circle": BS_ICON_ARROW_DOWN_LEFT_CIRCLE,
    "arrow-down-left-circle-fill": BS_ICON_ARROW_DOWN_LEFT_CIRCLE_FILL,
    "arrow-down-left-square": BS_ICON_ARROW_DOWN_LEFT_SQUARE,
    "arrow-down-left-square-fill": BS_ICON_ARROW_DOWN_LEFT_SQUARE_FILL,
    "arrow-down-right": BS_ICON_ARROW_DOWN_RIGHT,
    "arrow-down-right-circle": BS_ICON_ARROW_DOWN_RIGHT_CIRCLE,
    "arrow-down-right-circle-fill": BS_ICON_ARROW_DOWN_RIGHT_CIRCLE_FILL,
    "arrow-down-right-square": BS_ICON_ARROW_DOWN_RIGHT_SQUARE,
    "arrow-down-right-square-fill": BS_ICON_ARROW_DOWN_RIGHT_SQUARE_FILL,
    "arrow-down-short": BS_ICON_ARROW_DOWN_SHORT,
    "arrow-down-square": BS_ICON_ARROW_DOWN_SQUARE,
    "arrow-down-square-fill": BS_ICON_ARROW_DOWN_SQUARE_FILL,
    "arrow-down-up": BS_ICON_ARROW_DOWN_UP,
    "arrow-left": BS_ICON_ARROW_LEFT,
    "arrow-left-circle": BS_ICON_ARROW_LEFT_CIRCLE,
    "arrow-left-circle-fill": BS_ICON_ARROW_LEFT_CIRCLE_FILL,
    "arrow-left-right": BS_ICON_ARROW_LEFT_RIGHT,
    "arrow-left-short": BS_ICON_ARROW_LEFT_SHORT,
    "arrow-left-square": BS_ICON_ARROW_LEFT_SQUARE,
    "arrow-left-square-fill": BS_ICON_ARROW_LEFT_SQUARE_FILL,
    "arrow-repeat": BS_ICON_ARROW_REPEAT,
    "arrow-return-left": BS_ICON_ARROW_RETURN_LEFT,
    "arrow-return-right": BS_ICON_ARROW_RETURN_RIGHT,
    "arrow-right": BS_ICON_ARROW_RIGHT,
    "arrow-right-circle": BS_ICON_ARROW_RIGHT_CIRCLE,
    "arrow-right-circle-fill": BS_ICON_ARROW_RIGHT_CIRCLE_FILL,
    "arrow-right-short": BS_ICON_ARROW_RIGHT_SHORT,
    "arrow-right-square": BS_ICON_ARROW_RIGHT_SQUARE,
    "arrow-right-square-fill": BS_ICON_ARROW_RIGHT_SQUARE_FILL,
    "arrow-through-heart": BS_ICON_ARROW_THROUGH_HEART,
    "arrow-through-heart-fill": BS_ICON_ARROW_THROUGH_HEART_FILL,
    "arrow-up": BS_ICON_ARROW_UP,
    "arrow-up-circle": BS_ICON_ARROW_UP_CIRCLE,
    "arrow-up-circle-fill": BS_ICON_ARROW_UP_CIRCLE_FILL,
    "arrow-up-left": BS_ICON_ARROW_UP_LEFT,
    "arrow-up-left-circle": BS_ICON_ARROW_UP_LEFT_CIRCLE,
    "arrow-up-left-circle-fill": BS_ICON_ARROW_UP_LEFT_CIRCLE_FILL,
    "arrow-up-left-square": BS_ICON_ARROW_UP_LEFT_SQUARE,
    "arrow-up-left-square-fill": BS_ICON_ARROW_UP_LEFT_SQUARE_FILL,
    "arrow-up-right": BS_ICON_ARROW_UP_RIGHT,
    "arrow-up-right-circle": BS_ICON_ARROW_UP_RIGHT_CIRCLE,
    "arrow-up-right-circle-fill": BS_ICON_ARROW_UP_RIGHT_CIRCLE_FILL,
    "arrow-up-right-square": BS_ICON_ARROW_UP_RIGHT_SQUARE,
    "arrow-up-right-square-fill": BS_ICON_ARROW_UP_RIGHT_SQUARE_FILL,
    "arrow-up-short": BS_ICON_ARROW_UP_SHORT,
    "arrow-up-square": BS_ICON_ARROW_UP_SQUARE,
    "arrow-up-square-fill": BS_ICON_ARROW_UP_SQUARE_FILL,
    "arrows": BS_ICON_ARROWS,
    "arrows-angle-contract": BS_ICON_ARROWS_ANGLE_CONTRACT,
    "arrows-angle-expand": BS_ICON_ARROWS_ANGLE_EXPAND,
    "arrows-collapse": BS_ICON_ARROWS_COLLAPSE,
    "arrows-collapse-vertical": BS_ICON_ARROWS_COLLAPSE_VERTICAL,
    "arrows-expand": BS_ICON_ARROWS_EXPAND,
    "arrows-expand-vertical": BS_ICON_ARROWS_EXPAND_VERTICAL,
    "arrows-fullscreen": BS_ICON_ARROWS_FULLSCREEN,
    "arrows-move": BS_ICON_ARROWS_MOVE,
    "arrows-vertical": BS_ICON_ARROWS_VERTICAL,
    "aspect-ratio": BS_ICON_ASPECT_RATIO,
    "aspect-ratio-fill": BS_ICON_ASPECT_RATIO_FILL,
    "asterisk": BS_ICON_ASTERISK,
    "at": BS_ICON_AT,
    "award": BS_ICON_AWARD,
    "award-fill": BS_ICON_AWARD_FILL,
    "back": BS_ICON_BACK,
    "backpack": BS_ICON_BACKPACK,
    "backpack-fill": BS_ICON_BACKPACK_FILL,
    "backpack2": BS_ICON_BACKPACK2,
    "backpack2-fill": BS_ICON_BACKPACK2_FILL,
    "backpack3": BS_ICON_BACKPACK3,
    "backpack3-fill": BS_ICON_BACKPACK3_FILL,
    "backpack4": BS_ICON_BACKPACK4,
    "backpack4-fill": BS_ICON_BACKPACK4_FILL,
    "backspace": BS_ICON_BACKSPACE,
    "backspace-fill": BS_ICON_BACKSPACE_FILL,
    "backspace-reverse": BS_ICON_BACKSPACE_REVERSE,
    "backspace-reverse-fill": BS_ICON_BACKSPACE_REVERSE_FILL,
    "badge-3d": BS_ICON_BADGE_3D,
    "badge-3d-fill": BS_ICON_BADGE_3D_FILL,
    "badge-4k": BS_ICON_BADGE_4K,
    "badge-4k-fill": BS_ICON_BADGE_4K_FILL,
    "badge-8k": BS_ICON_BADGE_8K,
    "badge-8k-fill": BS_ICON_BADGE_8K_FILL,
    "badge-ad": BS_ICON_BADGE_AD,
    "badge-ad-fill": BS_ICON_BADGE_AD_FILL,
    "badge-ar": BS_ICON_BADGE_AR,
    "badge-ar-fill": BS_ICON_BADGE_AR_FILL,
    "badge-cc": BS_ICON_BADGE_CC,
    "badge-cc-fill": BS_ICON_BADGE_CC_FILL,
    "badge-hd": BS_ICON_BADGE_HD,
    "badge-hd-fill": BS_ICON_BADGE_HD_FILL,
    "badge-sd": BS_ICON_BADGE_SD,
    "badge-sd-fill": BS_ICON_BADGE_SD_FILL,
    "badge-tm": BS_ICON_BADGE_TM,
    "badge-tm-fill": BS_ICON_BADGE_TM_FILL,
    "badge-vo": BS_ICON_BADGE_VO,
    "badge-vo-fill": BS_ICON_BADGE_VO_FILL,
    "badge-vr": BS_ICON_BADGE_VR,
    "badge-vr-fill": BS_ICON_BADGE_VR_FILL,
    "badge-wc": BS_ICON_BADGE_WC,
    "badge-wc-fill": BS_ICON_BADGE_WC_FILL,
    "bag": BS_ICON_BAG,
    "bag-check": BS_ICON_BAG_CHECK,
    "bag-check-fill": BS_ICON_BAG_CHECK_FILL,
    "bag-dash": BS_ICON_BAG_DASH,
    "bag-dash-fill": BS_ICON_BAG_DASH_FILL,
    "bag-fill": BS_ICON_BAG_FILL,
    "bag-heart": BS_ICON_BAG_HEART,
    "bag-heart-fill": BS_ICON_BAG_HEART_FILL,
    "bag-plus": BS_ICON_BAG_PLUS,
    "bag-plus-fill": BS_ICON_BAG_PLUS_FILL,
    "bag-x": BS_ICON_BAG_X,
    "bag-x-fill": BS_ICON_BAG_X_FILL,
    "balloon": BS_ICON_BALLOON,
    "balloon-fill": BS_ICON_BALLOON_FILL,
    "balloon-heart": BS_ICON_BALLOON_HEART,
    "balloon-heart-fill": BS_ICON_BALLOON_HEART_FILL,
    "ban": BS_ICON_BAN,
    "ban-fill": BS_ICON_BAN_FILL,
    "bandaid": BS_ICON_BANDAID,
    "bandaid-fill": BS_ICON_BANDAID_FILL,
    "bank": BS_ICON_BANK,
    "bank2": BS_ICON_BANK2,
    "bar-chart": BS_ICON_BAR_CHART,
    "bar-chart-fill": BS_ICON_BAR_CHART_FILL,
    "bar-chart-line": BS_ICON_BAR_CHART_LINE,
    "bar-chart-line-fill": BS_ICON_BAR_CHART_LINE_FILL,
    "bar-chart-steps": BS_ICON_BAR_CHART_STEPS,
    "basket": BS_ICON_BASKET,
    "basket-fill": BS_ICON_BASKET_FILL,
    "basket2": BS_ICON_BASKET2,
    "basket2-fill": BS_ICON_BASKET2_FILL,
    "basket3": BS_ICON_BASKET3,
    "basket3-fill": BS_ICON_BASKET3_FILL,
    "battery": BS_ICON_BATTERY,
    "battery-charging": BS_ICON_BATTERY_CHARGING,
    "battery-full": BS_ICON_BATTERY_FULL,
    "battery-half": BS_ICON_BATTERY_HALF,
    "behance": BS_ICON_BEHANCE,
    "bell": BS_ICON_BELL,
    "bell-fill": BS_ICON_BELL_FILL,
    "bell-slash": BS_ICON_BELL_SLASH,
    "bell-slash-fill": BS_ICON_BELL_SLASH_FILL,
    "bezier": BS_ICON_BEZIER,
    "bezier2": BS_ICON_BEZIER2,
    "bicycle": BS_ICON_BICYCLE,
    "bing": BS_ICON_BING,
    "binoculars": BS_ICON_BINOCULARS,
    "binoculars-fill": BS_ICON_BINOCULARS_FILL,
    "blockquote-left": BS_ICON_BLOCKQUOTE_LEFT,
    "blockquote-right": BS_ICON_BLOCKQUOTE_RIGHT,
    "bluetooth": BS_ICON_BLUETOOTH,
    "body-text": BS_ICON_BODY_TEXT,
    "book": BS_ICON_BOOK,
    "book-fill": BS_ICON_BOOK_FILL,
    "book-half": BS_ICON_BOOK_HALF,
    "bookmark": BS_ICON_BOOKMARK,
    "bookmark-check": BS_ICON_BOOKMARK_CHECK,
    "bookmark-check-fill": BS_ICON_BOOKMARK_CHECK_FILL,
    "bookmark-dash": BS_ICON_BOOKMARK_DASH,
    "bookmark-dash-fill": BS_ICON_BOOKMARK_DASH_FILL,
    "bookmark-fill": BS_ICON_BOOKMARK_FILL,
    "bookmark-heart": BS_ICON_BOOKMARK_HEART,
    "bookmark-heart-fill": BS_ICON_BOOKMARK_HEART_FILL,
    "bookmark-plus": BS_ICON_BOOKMARK_PLUS,
    "bookmark-plus-fill": BS_ICON_BOOKMARK_PLUS_FILL,
    "bookmark-star": BS_ICON_BOOKMARK_STAR,
    "bookmark-star-fill": BS_ICON_BOOKMARK_STAR_FILL,
    "bookmark-x": BS_ICON_BOOKMARK_X,
    "bookmark-x-fill": BS_ICON_BOOKMARK_X_FILL,
    "bookmarks": BS_ICON_BOOKMARKS,
    "bookmarks-fill": BS_ICON_BOOKMARKS_FILL,
    "bookshelf": BS_ICON_BOOKSHELF,
    "boombox": BS_ICON_BOOMBOX,
    "boombox-fill": BS_ICON_BOOMBOX_FILL,
    "bootstrap": BS_ICON_BOOTSTRAP,
    "bootstrap-fill": BS_ICON_BOOTSTRAP_FILL,
    "bootstrap-reboot": BS_ICON_BOOTSTRAP_REBOOT,
    "border": BS_ICON_BORDER,
    "border-all": BS_ICON_BORDER_ALL,
    "border-bottom": BS_ICON_BORDER_BOTTOM,
    "border-center": BS_ICON_BORDER_CENTER,
    "border-inner": BS_ICON_BORDER_INNER,
    "border-left": BS_ICON_BORDER_LEFT,
    "border-middle": BS_ICON_BORDER_MIDDLE,
    "border-outer": BS_ICON_BORDER_OUTER,
    "border-right": BS_ICON_BORDER_RIGHT,
    "border-style": BS_ICON_BORDER_STYLE,
    "border-top": BS_ICON_BORDER_TOP,
    "border-width": BS_ICON_BORDER_WIDTH,
    "bounding-box": BS_ICON_BOUNDING_BOX,
    "bounding-box-circles": BS_ICON_BOUNDING_BOX_CIRCLES,
    "box": BS_ICON_BOX,
    "box-arrow-down": BS_ICON_BOX_ARROW_DOWN,
    "box-arrow-down-left": BS_ICON_BOX_ARROW_DOWN_LEFT,
    "box-arrow-down-right": BS_ICON_BOX_ARROW_DOWN_RIGHT,
    "box-arrow-in-down": BS_ICON_BOX_ARROW_IN_DOWN,
    "box-arrow-in-down-left": BS_ICON_BOX_ARROW_IN_DOWN_LEFT,
    "box-arrow-in-down-right": BS_ICON_BOX_ARROW_IN_DOWN_RIGHT,
    "box-arrow-in-left": BS_ICON_BOX_ARROW_IN_LEFT,
    "box-arrow-in-right": BS_ICON_BOX_ARROW_IN_RIGHT,
    "box-arrow-in-up": BS_ICON_BOX_ARROW_IN_UP,
    "box-arrow-in-up-left": BS_ICON_BOX_ARROW_IN_UP_LEFT,
    "box-arrow-in-up-right": BS_ICON_BOX_ARROW_IN_UP_RIGHT,
    "box-arrow-left": BS_ICON_BOX_ARROW_LEFT,
    "box-arrow-right": BS_ICON_BOX_ARROW_RIGHT,
    "box-arrow-up": BS_ICON_BOX_ARROW_UP,
    "box-arrow-up-left": BS_ICON_BOX_ARROW_UP_LEFT,
    "box-arrow-up-right": BS_ICON_BOX_ARROW_UP_RIGHT,
    "box-fill": BS_ICON_BOX_FILL,
    "box-seam": BS_ICON_BOX_SEAM,
    "box-seam-fill": BS_ICON_BOX_SEAM_FILL,
    "box2": BS_ICON_BOX2,
    "box2-fill": BS_ICON_BOX2_FILL,
    "box2-heart": BS_ICON_BOX2_HEART,
    "box2-heart-fill": BS_ICON_BOX2_HEART_FILL,
    "boxes": BS_ICON_BOXES,
    "braces": BS_ICON_BRACES,
    "braces-asterisk": BS_ICON_BRACES_ASTERISK,
    "bricks": BS_ICON_BRICKS,
    "briefcase": BS_ICON_BRIEFCASE,
    "briefcase-fill": BS_ICON_BRIEFCASE_FILL,
    "brightness-alt-high": BS_ICON_BRIGHTNESS_ALT_HIGH,
    "brightness-alt-high-fill": BS_ICON_BRIGHTNESS_ALT_HIGH_FILL,
    "brightness-alt-low": BS_ICON_BRIGHTNESS_ALT_LOW,
    "brightness-alt-low-fill": BS_ICON_BRIGHTNESS_ALT_LOW_FILL,
    "brightness-high": BS_ICON_BRIGHTNESS_HIGH,
    "brightness-high-fill": BS_ICON_BRIGHTNESS_HIGH_FILL,
    "brightness-low": BS_ICON_BRIGHTNESS_LOW,
    "brightness-low-fill": BS_ICON_BRIGHTNESS_LOW_FILL,
    "brilliance": BS_ICON_BRILLIANCE,
    "broadcast": BS_ICON_BROADCAST,
    "broadcast-pin": BS_ICON_BROADCAST_PIN,
    "browser-chrome": BS_ICON_BROWSER_CHROME,
    "browser-edge": BS_ICON_BROWSER_EDGE,
    "browser-firefox": BS_ICON_BROWSER_FIREFOX,
    "browser-safari": BS_ICON_BROWSER_SAFARI,
    "brush": BS_ICON_BRUSH,
    "brush-fill": BS_ICON_BRUSH_FILL,
    "bucket": BS_ICON_BUCKET,
    "bucket-fill": BS_ICON_BUCKET_FILL,
    "bug": BS_ICON_BUG,
    "bug-fill": BS_ICON_BUG_FILL,
    "building": BS_ICON_BUILDING,
    "building-add": BS_ICON_BUILDING_ADD,
    "building-check": BS_ICON_BUILDING_CHECK,
    "building-dash": BS_ICON_BUILDING_DASH,
    "building-down": BS_ICON_BUILDING_DOWN,
    "building-exclamation": BS_ICON_BUILDING_EXCLAMATION,
    "building-fill": BS_ICON_BUILDING_FILL,
    "building-fill-add": BS_ICON_BUILDING_FILL_ADD,
    "building-fill-check": BS_ICON_BUILDING_FILL_CHECK,
    "building-fill-dash": BS_ICON_BUILDING_FILL_DASH,
    "building-fill-down": BS_ICON_BUILDING_FILL_DOWN,
    "building-fill-exclamation": BS_ICON_BUILDING_FILL_EXCLAMATION,
    "building-fill-gear": BS_ICON_BUILDING_FILL_GEAR,
    "building-fill-lock": BS_ICON_BUILDING_FILL_LOCK,
    "building-fill-slash": BS_ICON_BUILDING_FILL_SLASH,
    "building-fill-up": BS_ICON_BUILDING_FILL_UP,
    "building-fill-x": BS_ICON_BUILDING_FILL_X,
    "building-gear": BS_ICON_BUILDING_GEAR,
    "building-lock": BS_ICON_BUILDING_LOCK,
    "building-slash": BS_ICON_BUILDING_SLASH,
    "building-up": BS_ICON_BUILDING_UP,
    "building-x": BS_ICON_BUILDING_X,
    "buildings": BS_ICON_BUILDINGS,
    "buildings-fill": BS_ICON_BUILDINGS_FILL,
    "bullseye": BS_ICON_BULLSEYE,
    "bus-front": BS_ICON_BUS_FRONT,
    "bus-front-fill": BS_ICON_BUS_FRONT_FILL,
    "c-circle": BS_ICON_C_CIRCLE,
    "c-circle-fill": BS_ICON_C_CIRCLE_FILL,
    "c-square": BS_ICON_C_SQUARE,
    "c-square-fill": BS_ICON_C_SQUARE_FILL,
    "cake": BS_ICON_CAKE,
    "cake-fill": BS_ICON_CAKE_FILL,
    "cake2": BS_ICON_CAKE2,
    "cake2-fill": BS_ICON_CAKE2_FILL,
    "calculator": BS_ICON_CALCULATOR,
    "calculator-fill": BS_ICON_CALCULATOR_FILL,
    "calendar": BS_ICON_CALENDAR,
    "calendar-check": BS_ICON_CALENDAR_CHECK,
    "calendar-check-fill": BS_ICON_CALENDAR_CHECK_FILL,
    "calendar-date": BS_ICON_CALENDAR_DATE,
    "calendar-date-fill": BS_ICON_CALENDAR_DATE_FILL,
    "calendar-day": BS_ICON_CALENDAR_DAY,
    "calendar-day-fill": BS_ICON_CALENDAR_DAY_FILL,
    "calendar-event": BS_ICON_CALENDAR_EVENT,
    "calendar-event-fill": BS_ICON_CALENDAR_EVENT_FILL,
    "calendar-fill": BS_ICON_CALENDAR_FILL,
    "calendar-heart": BS_ICON_CALENDAR_HEART,
    "calendar-heart-fill": BS_ICON_CALENDAR_HEART_FILL,
    "calendar-minus": BS_ICON_CALENDAR_MINUS,
    "calendar-minus-fill": BS_ICON_CALENDAR_MINUS_FILL,
    "calendar-month": BS_ICON_CALENDAR_MONTH,
    "calendar-month-fill": BS_ICON_CALENDAR_MONTH_FILL,
    "calendar-plus": BS_ICON_CALENDAR_PLUS,
    "calendar-plus-fill": BS_ICON_CALENDAR_PLUS_FILL,
    "calendar-range": BS_ICON_CALENDAR_RANGE,
    "calendar-range-fill": BS_ICON_CALENDAR_RANGE_FILL,
    "calendar-week": BS_ICON_CALENDAR_WEEK,
    "calendar-week-fill": BS_ICON_CALENDAR_WEEK_FILL,
    "calendar-x": BS_ICON_CALENDAR_X,
    "calendar-x-fill": BS_ICON_CALENDAR_X_FILL,
    "calendar2": BS_ICON_CALENDAR2,
    "calendar2-check": BS_ICON_CALENDAR2_CHECK,
    "calendar2-check-fill": BS_ICON_CALENDAR2_CHECK_FILL,
    "calendar2-date": BS_ICON_CALENDAR2_DATE,
    "calendar2-date-fill": BS_ICON_CALENDAR2_DATE_FILL,
    "calendar2-day": BS_ICON_CALENDAR2_DAY,
    "calendar2-day-fill": BS_ICON_CALENDAR2_DAY_FILL,
    "calendar2-event": BS_ICON_CALENDAR2_EVENT,
    "calendar2-event-fill": BS_ICON_CALENDAR2_EVENT_FILL,
    "calendar2-fill": BS_ICON_CALENDAR2_FILL,
    "calendar2-heart": BS_ICON_CALENDAR2_HEART,
    "calendar2-heart-fill": BS_ICON_CALENDAR2_HEART_FILL,
    "calendar2-minus": BS_ICON_CALENDAR2_MINUS,
    "calendar2-minus-fill": BS_ICON_CALENDAR2_MINUS_FILL,
    "calendar2-month": BS_ICON_CALENDAR2_MONTH,
    "calendar2-month-fill": BS_ICON_CALENDAR2_MONTH_FILL,
    "calendar2-plus": BS_ICON_CALENDAR2_PLUS,
    "calendar2-plus-fill": BS_ICON_CALENDAR2_PLUS_FILL,
    "calendar2-range": BS_ICON_CALENDAR2_RANGE,
    "calendar2-range-fill": BS_ICON_CALENDAR2_RANGE_FILL,
    "calendar2-week": BS_ICON_CALENDAR2_WEEK,
    "calendar2-week-fill": BS_ICON_CALENDAR2_WEEK_FILL,
    "calendar2-x": BS_ICON_CALENDAR2_X,
    "calendar2-x-fill": BS_ICON_CALENDAR2_X_FILL,
    "calendar3": BS_ICON_CALENDAR3,
    "calendar3-event": BS_ICON_CALENDAR3_EVENT,
    "calendar3-event-fill": BS_ICON_CALENDAR3_EVENT_FILL,
    "calendar3-fill": BS_ICON_CALENDAR3_FILL,
    "calendar3-range": BS_ICON_CALENDAR3_RANGE,
    "calendar3-range-fill": BS_ICON_CALENDAR3_RANGE_FILL,
    "calendar3-week": BS_ICON_CALENDAR3_WEEK,
    "calendar3-week-fill": BS_ICON_CALENDAR3_WEEK_FILL,
    "calendar4": BS_ICON_CALENDAR4,
    "calendar4-event": BS_ICON_CALENDAR4_EVENT,
    "calendar4-range": BS_ICON_CALENDAR4_RANGE,
    "calendar4-week": BS_ICON_CALENDAR4_WEEK,
    "camera": BS_ICON_CAMERA,
    "camera-fill": BS_ICON_CAMERA_FILL,
    "camera-reels": BS_ICON_CAMERA_REELS,
    "camera-reels-fill": BS_ICON_CAMERA_REELS_FILL,
    "camera-video": BS_ICON_CAMERA_VIDEO,
    "camera-video-fill": BS_ICON_CAMERA_VIDEO_FILL,
    "camera-video-off": BS_ICON_CAMERA_VIDEO_OFF,
    "camera-video-off-fill": BS_ICON_CAMERA_VIDEO_OFF_FILL,
    "camera2": BS_ICON_CAMERA2,
    "capslock": BS_ICON_CAPSLOCK,
    "capslock-fill": BS_ICON_CAPSLOCK_FILL,
    "capsule": BS_ICON_CAPSULE,
    "capsule-pill": BS_ICON_CAPSULE_PILL,
    "car-front": BS_ICON_CAR_FRONT,
    "car-front-fill": BS_ICON_CAR_FRONT_FILL,
    "card-checklist": BS_ICON_CARD_CHECKLIST,
    "card-heading": BS_ICON_CARD_HEADING,
    "card-image": BS_ICON_CARD_IMAGE,
    "card-list": BS_ICON_CARD_LIST,
    "card-text": BS_ICON_CARD_TEXT,
    "caret-down": BS_ICON_CARET_DOWN,
    "caret-down-fill": BS_ICON_CARET_DOWN_FILL,
    "caret-down-square": BS_ICON_CARET_DOWN_SQUARE,
    "caret-down-square-fill": BS_ICON_CARET_DOWN_SQUARE_FILL,
    "caret-left": BS_ICON_CARET_LEFT,
    "caret-left-fill": BS_ICON_CARET_LEFT_FILL,
    "caret-left-square": BS_ICON_CARET_LEFT_SQUARE,
    "caret-left-square-fill": BS_ICON_CARET_LEFT_SQUARE_FILL,
    "caret-right": BS_ICON_CARET_RIGHT,
    "caret-right-fill": BS_ICON_CARET_RIGHT_FILL,
    "caret-right-square": BS_ICON_CARET_RIGHT_SQUARE,
    "caret-right-square-fill": BS_ICON_CARET_RIGHT_SQUARE_FILL,
    "caret-up": BS_ICON_CARET_UP,
    "caret-up-fill": BS_ICON_CARET_UP_FILL,
    "caret-up-square": BS_ICON_CARET_UP_SQUARE,
    "caret-up-square-fill": BS_ICON_CARET_UP_SQUARE_FILL,
    "cart": BS_ICON_CART,
    "cart-check": BS_ICON_CART_CHECK,
    "cart-check-fill": BS_ICON_CART_CHECK_FILL,
    "cart-dash": BS_ICON_CART_DASH,
    "cart-dash-fill": BS_ICON_CART_DASH_FILL,
    "cart-fill": BS_ICON_CART_FILL,
    "cart-plus": BS_ICON_CART_PLUS,
    "cart-plus-fill": BS_ICON_CART_PLUS_FILL,
    "cart-x": BS_ICON_CART_X,
    "cart-x-fill": BS_ICON_CART_X_FILL,
    "cart2": BS_ICON_CART2,
    "cart3": BS_ICON_CART3,
    "cart4": BS_ICON_CART4,
    "cash": BS_ICON_CASH,
    "cash-coin": BS_ICON_CASH_COIN,
    "cash-stack": BS_ICON_CASH_STACK,
    "cassette": BS_ICON_CASSETTE,
    "cassette-fill": BS_ICON_CASSETTE_FILL,
    "cast": BS_ICON_CAST,
    "cc-circle": BS_ICON_CC_CIRCLE,
    "cc-circle-fill": BS_ICON_CC_CIRCLE_FILL,
    "cc-square": BS_ICON_CC_SQUARE,
    "cc-square-fill": BS_ICON_CC_SQUARE_FILL,
    "chat": BS_ICON_CHAT,
    "chat-dots": BS_ICON_CHAT_DOTS,
    "chat-dots-fill": BS_ICON_CHAT_DOTS_FILL,
    "chat-fill": BS_ICON_CHAT_FILL,
    "chat-heart": BS_ICON_CHAT_HEART,
    "chat-heart-fill": BS_ICON_CHAT_HEART_FILL,
    "chat-left": BS_ICON_CHAT_LEFT,
    "chat-left-dots": BS_ICON_CHAT_LEFT_DOTS,
    "chat-left-dots-fill": BS_ICON_CHAT_LEFT_DOTS_FILL,
    "chat-left-fill": BS_ICON_CHAT_LEFT_FILL,
    "chat-left-heart": BS_ICON_CHAT_LEFT_HEART,
    "chat-left-heart-fill": BS_ICON_CHAT_LEFT_HEART_FILL,
    "chat-left-quote": BS_ICON_CHAT_LEFT_QUOTE,
    "chat-left-quote-fill": BS_ICON_CHAT_LEFT_QUOTE_FILL,
    "chat-left-text": BS_ICON_CHAT_LEFT_TEXT,
    "chat-left-text-fill": BS_ICON_CHAT_LEFT_TEXT_FILL,
    "chat-quote": BS_ICON_CHAT_QUOTE,
    "chat-quote-fill": BS_ICON_CHAT_QUOTE_FILL,
    "chat-right": BS_ICON_CHAT_RIGHT,
    "chat-right-dots": BS_ICON_CHAT_RIGHT_DOTS,
    "chat-right-dots-fill": BS_ICON_CHAT_RIGHT_DOTS_FILL,
    "chat-right-fill": BS_ICON_CHAT_RIGHT_FILL,
    "chat-right-heart": BS_ICON_CHAT_RIGHT_HEART,
    "chat-right-heart-fill": BS_ICON_CHAT_RIGHT_HEART_FILL,
    "chat-right-quote": BS_ICON_CHAT_RIGHT_QUOTE,
    "chat-right-quote-fill": BS_ICON_CHAT_RIGHT_QUOTE_FILL,
    "chat-right-text": BS_ICON_CHAT_RIGHT_TEXT,
    "chat-right-text-fill": BS_ICON_CHAT_RIGHT_TEXT_FILL,
    "chat-square": BS_ICON_CHAT_SQUARE,
    "chat-square-dots": BS_ICON_CHAT_SQUARE_DOTS,
    "chat-square-dots-fill": BS_ICON_CHAT_SQUARE_DOTS_FILL,
    "chat-square-fill": BS_ICON_CHAT_SQUARE_FILL,
    "chat-square-heart": BS_ICON_CHAT_SQUARE_HEART,
    "chat-square-heart-fill": BS_ICON_CHAT_SQUARE_HEART_FILL,
    "chat-square-quote": BS_ICON_CHAT_SQUARE_QUOTE,
    "chat-square-quote-fill": BS_ICON_CHAT_SQUARE_QUOTE_FILL,
    "chat-square-text": BS_ICON_CHAT_SQUARE_TEXT,
    "chat-square-text-fill": BS_ICON_CHAT_SQUARE_TEXT_FILL,
    "chat-text": BS_ICON_CHAT_TEXT,
    "chat-text-fill": BS_ICON_CHAT_TEXT_FILL,
    "check": BS_ICON_CHECK,
    "check-all": BS_ICON_CHECK_ALL,
    "check-circle": BS_ICON_CHECK_CIRCLE,
    "check-circle-fill": BS_ICON_CHECK_CIRCLE_FILL,
    "check-lg": BS_ICON_CHECK_LG,
    "check-square": BS_ICON_CHECK_SQUARE,
    "check-square-fill": BS_ICON_CHECK_SQUARE_FILL,
    "check2": BS_ICON_CHECK2,
    "check2-all": BS_ICON_CHECK2_ALL,
    "check2-circle": BS_ICON_CHECK2_CIRCLE,
    "check2-square": BS_ICON_CHECK2_SQUARE,
    "chevron-bar-contract": BS_ICON_CHEVRON_BAR_CONTRACT,
    "chevron-bar-down": BS_ICON_CHEVRON_BAR_DOWN,
    "chevron-bar-expand": BS_ICON_CHEVRON_BAR_EXPAND,
    "chevron-bar-left": BS_ICON_CHEVRON_BAR_LEFT,
    "chevron-bar-right": BS_ICON_CHEVRON_BAR_RIGHT,
    "chevron-bar-up": BS_ICON_CHEVRON_BAR_UP,
    "chevron-compact-down": BS_ICON_CHEVRON_COMPACT_DOWN,
    "chevron-compact-left": BS_ICON_CHEVRON_COMPACT_LEFT,
    "chevron-compact-right": BS_ICON_CHEVRON_COMPACT_RIGHT,
    "chevron-compact-up": BS_ICON_CHEVRON_COMPACT_UP,
    "chevron-contract": BS_ICON_CHEVRON_CONTRACT,
    "chevron-double-down": BS_ICON_CHEVRON_DOUBLE_DOWN,
    "chevron-double-left": BS_ICON_CHEVRON_DOUBLE_LEFT,
    "chevron-double-right": BS_ICON_CHEVRON_DOUBLE_RIGHT,
    "chevron-double-up": BS_ICON_CHEVRON_DOUBLE_UP,
    "chevron-down": BS_ICON_CHEVRON_DOWN,
    "chevron-expand": BS_ICON_CHEVRON_EXPAND,
    "chevron-left": BS_ICON_CHEVRON_LEFT,
    "chevron-right": BS_ICON_CHEVRON_RIGHT,
    "chevron-up": BS_ICON_CHEVRON_UP,
    "circle": BS_ICON_CIRCLE,
    "circle-fill": BS_ICON_CIRCLE_FILL,
    "circle-half": BS_ICON_CIRCLE_HALF,
    "circle-square": BS_ICON_CIRCLE_SQUARE,
    "clipboard": BS_ICON_CLIPBOARD,
    "clipboard-check": BS_ICON_CLIPBOARD_CHECK,
    "clipboard-check-fill": BS_ICON_CLIPBOARD_CHECK_FILL,
    "clipboard-data": BS_ICON_CLIPBOARD_DATA,
    "clipboard-data-fill": BS_ICON_CLIPBOARD_DATA_FILL,
    "clipboard-fill": BS_ICON_CLIPBOARD_FILL,
    "clipboard-heart": BS_ICON_CLIPBOARD_HEART,
    "clipboard-heart-fill": BS_ICON_CLIPBOARD_HEART_FILL,
    "clipboard-minus": BS_ICON_CLIPBOARD_MINUS,
    "clipboard-minus-fill": BS_ICON_CLIPBOARD_MINUS_FILL,
    "clipboard-plus": BS_ICON_CLIPBOARD_PLUS,
    "clipboard-plus-fill": BS_ICON_CLIPBOARD_PLUS_FILL,
    "clipboard-pulse": BS_ICON_CLIPBOARD_PULSE,
    "clipboard-x": BS_ICON_CLIPBOARD_X,
    "clipboard-x-fill": BS_ICON_CLIPBOARD_X_FILL,
    "clipboard2": BS_ICON_CLIPBOARD2,
    "clipboard2-check": BS_ICON_CLIPBOARD2_CHECK,
    "clipboard2-check-fill": BS_ICON_CLIPBOARD2_CHECK_FILL,
    "clipboard2-data": BS_ICON_CLIPBOARD2_DATA,
    "clipboard2-data-fill": BS_ICON_CLIPBOARD2_DATA_FILL,
    "clipboard2-fill": BS_ICON_CLIPBOARD2_FILL,
    "clipboard2-heart": BS_ICON_CLIPBOARD2_HEART,
    "clipboard2-heart-fill": BS_ICON_CLIPBOARD2_HEART_FILL,
    "clipboard2-minus": BS_ICON_CLIPBOARD2_MINUS,
    "clipboard2-minus-fill": BS_ICON_CLIPBOARD2_MINUS_FILL,
    "clipboard2-plus": BS_ICON_CLIPBOARD2_PLUS,
    "clipboard2-plus-fill": BS_ICON_CLIPBOARD2_PLUS_FILL,
    "clipboard2-pulse": BS_ICON_CLIPBOARD2_PULSE,
    "clipboard2-pulse-fill": BS_ICON_CLIPBOARD2_PULSE_FILL,
    "clipboard2-x": BS_ICON_CLIPBOARD2_X,
    "clipboard2-x-fill": BS_ICON_CLIPBOARD2_X_FILL,
    "clock": BS_ICON_CLOCK,
    "clock-fill": BS_ICON_CLOCK_FILL,
    "clock-history": BS_ICON_CLOCK_HISTORY,
    "cloud": BS_ICON_CLOUD,
    "cloud-arrow-down": BS_ICON_CLOUD_ARROW_DOWN,
    "cloud-arrow-down-fill": BS_ICON_CLOUD_ARROW_DOWN_FILL,
    "cloud-arrow-up": BS_ICON_CLOUD_ARROW_UP,
    "cloud-arrow-up-fill": BS_ICON_CLOUD_ARROW_UP_FILL,
    "cloud-check": BS_ICON_CLOUD_CHECK,
    "cloud-check-fill": BS_ICON_CLOUD_CHECK_FILL,
    "cloud-download": BS_ICON_CLOUD_DOWNLOAD,
    "cloud-download-fill": BS_ICON_CLOUD_DOWNLOAD_FILL,
    "cloud-drizzle": BS_ICON_CLOUD_DRIZZLE,
    "cloud-drizzle-fill": BS_ICON_CLOUD_DRIZZLE_FILL,
    "cloud-fill": BS_ICON_CLOUD_FILL,
    "cloud-fog": BS_ICON_CLOUD_FOG,
    "cloud-fog-fill": BS_ICON_CLOUD_FOG_FILL,
    "cloud-fog2": BS_ICON_CLOUD_FOG2,
    "cloud-fog2-fill": BS_ICON_CLOUD_FOG2_FILL,
    "cloud-hail": BS_ICON_CLOUD_HAIL,
    "cloud-hail-fill": BS_ICON_CLOUD_HAIL_FILL,
    "cloud-haze": BS_ICON_CLOUD_HAZE,
    "cloud-haze-fill": BS_ICON_CLOUD_HAZE_FILL,
    "cloud-haze2": BS_ICON_CLOUD_HAZE2,
    "cloud-haze2-fill": BS_ICON_CLOUD_HAZE2_FILL,
    "cloud-lightning": BS_ICON_CLOUD_LIGHTNING,
    "cloud-lightning-fill": BS_ICON_CLOUD_LIGHTNING_FILL,
    "cloud-lightning-rain": BS_ICON_CLOUD_LIGHTNING_RAIN,
    "cloud-lightning-rain-fill": BS_ICON_CLOUD_LIGHTNING_RAIN_FILL,
    "cloud-minus": BS_ICON_CLOUD_MINUS,
    "cloud-minus-fill": BS_ICON_CLOUD_MINUS_FILL,
    "cloud-moon": BS_ICON_CLOUD_MOON,
    "cloud-moon-fill": BS_ICON_CLOUD_MOON_FILL,
    "cloud-plus": BS_ICON_CLOUD_PLUS,
    "cloud-plus-fill": BS_ICON_CLOUD_PLUS_FILL,
    "cloud-rain": BS_ICON_CLOUD_RAIN,
    "cloud-rain-fill": BS_ICON_CLOUD_RAIN_FILL,
    "cloud-rain-heavy": BS_ICON_CLOUD_RAIN_HEAVY,
    "cloud-rain-heavy-fill": BS_ICON_CLOUD_RAIN_HEAVY_FILL,
    "cloud-slash": BS_ICON_CLOUD_SLASH,
    "cloud-slash-fill": BS_ICON_CLOUD_SLASH_FILL,
    "cloud-sleet": BS_ICON_CLOUD_SLEET,
    "cloud-sleet-fill": BS_ICON_CLOUD_SLEET_FILL,
    "cloud-snow": BS_ICON_CLOUD_SNOW,
    "cloud-snow-fill": BS_ICON_CLOUD_SNOW_FILL,
    "cloud-sun": BS_ICON_CLOUD_SUN,
    "cloud-sun-fill": BS_ICON_CLOUD_SUN_FILL,
    "cloud-upload": BS_ICON_CLOUD_UPLOAD,
    "cloud-upload-fill": BS_ICON_CLOUD_UPLOAD_FILL,
    "clouds": BS_ICON_CLOUDS,
    "clouds-fill": BS_ICON_CLOUDS_FILL,
    "cloudy": BS_ICON_CLOUDY,
    "cloudy-fill": BS_ICON_CLOUDY_FILL,
    "code": BS_ICON_CODE,
    "code-slash": BS_ICON_CODE_SLASH,
    "code-square": BS_ICON_CODE_SQUARE,
    "coin": BS_ICON_COIN,
    "collection": BS_ICON_COLLECTION,
    "collection-fill": BS_ICON_COLLECTION_FILL,
    "collection-play": BS_ICON_COLLECTION_PLAY,
    "collection-play-fill": BS_ICON_COLLECTION_PLAY_FILL,
    "columns": BS_ICON_COLUMNS,
    "columns-gap": BS_ICON_COLUMNS_GAP,
    "command": BS_ICON_COMMAND,
    "compass": BS_ICON_COMPASS,
    "compass-fill": BS_ICON_COMPASS_FILL,
    "cone": BS_ICON_CONE,
    "cone-striped": BS_ICON_CONE_STRIPED,
    "controller": BS_ICON_CONTROLLER,
    "cookie": BS_ICON_COOKIE,
    "copy": BS_ICON_COPY,
    "cpu": BS_ICON_CPU,
    "cpu-fill": BS_ICON_CPU_FILL,
    "credit-card": BS_ICON_CREDIT_CARD,
    "credit-card-2-back": BS_ICON_CREDIT_CARD_2_BACK,
    "credit-card-2-back-fill": BS_ICON_CREDIT_CARD_2_BACK_FILL,
    "credit-card-2-front": BS_ICON_CREDIT_CARD_2_FRONT,
    "credit-card-2-front-fill": BS_ICON_CREDIT_CARD_2_FRONT_FILL,
    "credit-card-fill": BS_ICON_CREDIT_CARD_FILL,
    "crop": BS_ICON_CROP,
    "crosshair": BS_ICON_CROSSHAIR,
    "crosshair2": BS_ICON_CROSSHAIR2,
    "cup": BS_ICON_CUP,
    "cup-fill": BS_ICON_CUP_FILL,
    "cup-hot": BS_ICON_CUP_HOT,
    "cup-hot-fill": BS_ICON_CUP_HOT_FILL,
    "cup-straw": BS_ICON_CUP_STRAW,
    "currency-bitcoin": BS_ICON_CURRENCY_BITCOIN,
    "currency-dollar": BS_ICON_CURRENCY_DOLLAR,
    "currency-euro": BS_ICON_CURRENCY_EURO,
    "currency-exchange": BS_ICON_CURRENCY_EXCHANGE,
    "currency-pound": BS_ICON_CURRENCY_POUND,
    "currency-rupee": BS_ICON_CURRENCY_RUPEE,
    "currency-yen": BS_ICON_CURRENCY_YEN,
    "cursor": BS_ICON_CURSOR,
    "cursor-fill": BS_ICON_CURSOR_FILL,
    "cursor-text": BS_ICON_CURSOR_TEXT,
    "dash": BS_ICON_DASH,
    "dash-circle": BS_ICON_DASH_CIRCLE,
    "dash-circle-dotted": BS_ICON_DASH_CIRCLE_DOTTED,
    "dash-circle-fill": BS_ICON_DASH_CIRCLE_FILL,
    "dash-lg": BS_ICON_DASH_LG,
    "dash-square": BS_ICON_DASH_SQUARE,
    "dash-square-dotted": BS_ICON_DASH_SQUARE_DOTTED,
    "dash-square-fill": BS_ICON_DASH_SQUARE_FILL,
    "database": BS_ICON_DATABASE,
    "database-add": BS_ICON_DATABASE_ADD,
    "database-check": BS_ICON_DATABASE_CHECK,
    "database-dash": BS_ICON_DATABASE_DASH,
    "database-down": BS_ICON_DATABASE_DOWN,
    "database-exclamation": BS_ICON_DATABASE_EXCLAMATION,
    "database-fill": BS_ICON_DATABASE_FILL,
    "database-fill-add": BS_ICON_DATABASE_FILL_ADD,
    "database-fill-check": BS_ICON_DATABASE_FILL_CHECK,
    "database-fill-dash": BS_ICON_DATABASE_FILL_DASH,
    "database-fill-down": BS_ICON_DATABASE_FILL_DOWN,
    "database-fill-exclamation": BS_ICON_DATABASE_FILL_EXCLAMATION,
    "database-fill-gear": BS_ICON_DATABASE_FILL_GEAR,
    "database-fill-lock": BS_ICON_DATABASE_FILL_LOCK,
    "database-fill-slash": BS_ICON_DATABASE_FILL_SLASH,
    "database-fill-up": BS_ICON_DATABASE_FILL_UP,
    "database-fill-x": BS_ICON_DATABASE_FILL_X,
    "database-gear": BS_ICON_DATABASE_GEAR,
    "database-lock": BS_ICON_DATABASE_LOCK,
    "database-slash": BS_ICON_DATABASE_SLASH,
    "database-up": BS_ICON_DATABASE_UP,
    "database-x": BS_ICON_DATABASE_X,
    "device-hdd": BS_ICON_DEVICE_HDD,
    "device-hdd-fill": BS_ICON_DEVICE_HDD_FILL,
    "device-ssd": BS_ICON_DEVICE_SSD,
    "device-ssd-fill": BS_ICON_DEVICE_SSD_FILL,
    "diagram-2": BS_ICON_DIAGRAM_2,
    "diagram-2-fill": BS_ICON_DIAGRAM_2_FILL,
    "diagram-3": BS_ICON_DIAGRAM_3,
    "diagram-3-fill": BS_ICON_DIAGRAM_3_FILL,
    "diamond": BS_ICON_DIAMOND,
    "diamond-fill": BS_ICON_DIAMOND_FILL,
    "diamond-half": BS_ICON_DIAMOND_HALF,
    "dice-1": BS_ICON_DICE_1,
    "dice-1-fill": BS_ICON_DICE_1_FILL,
    "dice-2": BS_ICON_DICE_2,
    "dice-2-fill": BS_ICON_DICE_2_FILL,
    "dice-3": BS_ICON_DICE_3,
    "dice-3-fill": BS_ICON_DICE_3_FILL,
    "dice-4": BS_ICON_DICE_4,
    "dice-4-fill": BS_ICON_DICE_4_FILL,
    "dice-5": BS_ICON_DICE_5,
    "dice-5-fill": BS_ICON_DICE_5_FILL,
    "dice-6": BS_ICON_DICE_6,
    "dice-6-fill": BS_ICON_DICE_6_FILL,
    "disc": BS_ICON_DISC,
    "disc-fill": BS_ICON_DISC_FILL,
    "discord": BS_ICON_DISCORD,
    "display": BS_ICON_DISPLAY,
    "display-fill": BS_ICON_DISPLAY_FILL,
    "displayport": BS_ICON_DISPLAYPORT,
    "displayport-fill": BS_ICON_DISPLAYPORT_FILL,
    "distribute-horizontal": BS_ICON_DISTRIBUTE_HORIZONTAL,
    "distribute-vertical": BS_ICON_DISTRIBUTE_VERTICAL,
    "door-closed": BS_ICON_DOOR_CLOSED,
    "door-closed-fill": BS_ICON_DOOR_CLOSED_FILL,
    "door-open": BS_ICON_DOOR_OPEN,
    "door-open-fill": BS_ICON_DOOR_OPEN_FILL,
    "dot": BS_ICON_DOT,
    "download": BS_ICON_DOWNLOAD,
    "dpad": BS_ICON_DPAD,
    "dpad-fill": BS_ICON_DPAD_FILL,
    "dribbble": BS_ICON_DRIBBBLE,
    "dropbox": BS_ICON_DROPBOX,
    "droplet": BS_ICON_DROPLET,
    "droplet-fill": BS_ICON_DROPLET_FILL,
    "droplet-half": BS_ICON_DROPLET_HALF,
    "duffle": BS_ICON_DUFFLE,
    "duffle-fill": BS_ICON_DUFFLE_FILL,
    "ear": BS_ICON_EAR,
    "ear-fill": BS_ICON_EAR_FILL,
    "earbuds": BS_ICON_EARBUDS,
    "easel": BS_ICON_EASEL,
    "easel-fill": BS_ICON_EASEL_FILL,
    "easel2": BS_ICON_EASEL2,
    "easel2-fill": BS_ICON_EASEL2_FILL,
    "easel3": BS_ICON_EASEL3,
    "easel3-fill": BS_ICON_EASEL3_FILL,
    "egg": BS_ICON_EGG,
    "egg-fill": BS_ICON_EGG_FILL,
    "egg-fried": BS_ICON_EGG_FRIED,
    "eject": BS_ICON_EJECT,
    "eject-fill": BS_ICON_EJECT_FILL,
    "emoji-angry": BS_ICON_EMOJI_ANGRY,
    "emoji-angry-fill": BS_ICON_EMOJI_ANGRY_FILL,
    "emoji-astonished": BS_ICON_EMOJI_ASTONISHED,
    "emoji-astonished-fill": BS_ICON_EMOJI_ASTONISHED_FILL,
    "emoji-dizzy": BS_ICON_EMOJI_DIZZY,
    "emoji-dizzy-fill": BS_ICON_EMOJI_DIZZY_FILL,
    "emoji-expressionless": BS_ICON_EMOJI_EXPRESSIONLESS,
    "emoji-expressionless-fill": BS_ICON_EMOJI_EXPRESSIONLESS_FILL,
    "emoji-frown": BS_ICON_EMOJI_FROWN,
    "emoji-frown-fill": BS_ICON_EMOJI_FROWN_FILL,
    "emoji-grimace": BS_ICON_EMOJI_GRIMACE,
    "emoji-grimace-fill": BS_ICON_EMOJI_GRIMACE_FILL,
    "emoji-grin": BS_ICON_EMOJI_GRIN,
    "emoji-grin-fill": BS_ICON_EMOJI_GRIN_FILL,
    "emoji-heart-eyes": BS_ICON_EMOJI_HEART_EYES,
    "emoji-heart-eyes-fill": BS_ICON_EMOJI_HEART_EYES_FILL,
    "emoji-kiss": BS_ICON_EMOJI_KISS,
    "emoji-kiss-fill": BS_ICON_EMOJI_KISS_FILL,
    "emoji-laughing": BS_ICON_EMOJI_LAUGHING,
    "emoji-laughing-fill": BS_ICON_EMOJI_LAUGHING_FILL,
    "emoji-neutral": BS_ICON_EMOJI_NEUTRAL,
    "emoji-neutral-fill": BS_ICON_EMOJI_NEUTRAL_FILL,
    "emoji-smile": BS_ICON_EMOJI_SMILE,
    "emoji-smile-fill": BS_ICON_EMOJI_SMILE_FILL,
    "emoji-smile-upside-down": BS_ICON_EMOJI_SMILE_UPSIDE_DOWN,
    "emoji-smile-upside-down-fill": BS_ICON_EMOJI_SMILE_UPSIDE_DOWN_FILL,
    "emoji-sunglasses": BS_ICON_EMOJI_SUNGLASSES,
    "emoji-sunglasses-fill": BS_ICON_EMOJI_SUNGLASSES_FILL,
    "emoji-surprise": BS_ICON_EMOJI_SURPRISE,
    "emoji-surprise-fill": BS_ICON_EMOJI_SURPRISE_FILL,
    "emoji-tear": BS_ICON_EMOJI_TEAR,
    "emoji-tear-fill": BS_ICON_EMOJI_TEAR_FILL,
    "emoji-wink": BS_ICON_EMOJI_WINK,
    "emoji-wink-fill": BS_ICON_EMOJI_WINK_FILL,
    "envelope": BS_ICON_ENVELOPE,
    "envelope-arrow-down": BS_ICON_ENVELOPE_ARROW_DOWN,
    "envelope-arrow-down-fill": BS_ICON_ENVELOPE_ARROW_DOWN_FILL,
    "envelope-arrow-up": BS_ICON_ENVELOPE_ARROW_UP,
    "envelope-arrow-up-fill": BS_ICON_ENVELOPE_ARROW_UP_FILL,
    "envelope-at": BS_ICON_ENVELOPE_AT,
    "envelope-at-fill": BS_ICON_ENVELOPE_AT_FILL,
    "envelope-check": BS_ICON_ENVELOPE_CHECK,
    "envelope-check-fill": BS_ICON_ENVELOPE_CHECK_FILL,
    "envelope-dash": BS_ICON_ENVELOPE_DASH,
    "envelope-dash-fill": BS_ICON_ENVELOPE_DASH_FILL,
    "envelope-exclamation": BS_ICON_ENVELOPE_EXCLAMATION,
    "envelope-exclamation-fill": BS_ICON_ENVELOPE_EXCLAMATION_FILL,
    "envelope-fill": BS_ICON_ENVELOPE_FILL,
    "envelope-heart": BS_ICON_ENVELOPE_HEART,
    "envelope-heart-fill": BS_ICON_ENVELOPE_HEART_FILL,
    "envelope-open": BS_ICON_ENVELOPE_OPEN,
    "envelope-open-fill": BS_ICON_ENVELOPE_OPEN_FILL,
    "envelope-open-heart": BS_ICON_ENVELOPE_OPEN_HEART,
    "envelope-open-heart-fill": BS_ICON_ENVELOPE_OPEN_HEART_FILL,
    "envelope-paper": BS_ICON_ENVELOPE_PAPER,
    "envelope-paper-fill": BS_ICON_ENVELOPE_PAPER_FILL,
    "envelope-paper-heart": BS_ICON_ENVELOPE_PAPER_HEART,
    "envelope-paper-heart-fill": BS_ICON_ENVELOPE_PAPER_HEART_FILL,
    "envelope-plus": BS_ICON_ENVELOPE_PLUS,
    "envelope-plus-fill": BS_ICON_ENVELOPE_PLUS_FILL,
    "envelope-slash": BS_ICON_ENVELOPE_SLASH,
    "envelope-slash-fill": BS_ICON_ENVELOPE_SLASH_FILL,
    "envelope-x": BS_ICON_ENVELOPE_X,
    "envelope-x-fill": BS_ICON_ENVELOPE_X_FILL,
    "eraser": BS_ICON_ERASER,
    "eraser-fill": BS_ICON_ERASER_FILL,
    "escape": BS_ICON_ESCAPE,
    "ethernet": BS_ICON_ETHERNET,
    "ev-front": BS_ICON_EV_FRONT,
    "ev-front-fill": BS_ICON_EV_FRONT_FILL,
    "ev-station": BS_ICON_EV_STATION,
    "ev-station-fill": BS_ICON_EV_STATION_FILL,
    "exclamation": BS_ICON_EXCLAMATION,
    "exclamation-circle": BS_ICON_EXCLAMATION_CIRCLE,
    "exclamation-circle-fill": BS_ICON_EXCLAMATION_CIRCLE_FILL,
    "exclamation-diamond": BS_ICON_EXCLAMATION_DIAMOND,
    "exclamation-diamond-fill": BS_ICON_EXCLAMATION_DIAMOND_FILL,
    "exclamation-lg": BS_ICON_EXCLAMATION_LG,
    "exclamation-octagon": BS_ICON_EXCLAMATION_OCTAGON,
    "exclamation-octagon-fill": BS_ICON_EXCLAMATION_OCTAGON_FILL,
    "exclamation-square": BS_ICON_EXCLAMATION_SQUARE,
    "exclamation-square-fill": BS_ICON_EXCLAMATION_SQUARE_FILL,
    "exclamation-triangle": BS_ICON_EXCLAMATION_TRIANGLE,
    "exclamation-triangle-fill": BS_ICON_EXCLAMATION_TRIANGLE_FILL,
    "exclude": BS_ICON_EXCLUDE,
    "explicit": BS_ICON_EXPLICIT,
    "explicit-fill": BS_ICON_EXPLICIT_FILL,
    "exposure": BS_ICON_EXPOSURE,
    "eye": BS_ICON_EYE,
    "eye-fill": BS_ICON_EYE_FILL,
    "eye-slash": BS_ICON_EYE_SLASH,
    "eye-slash-fill": BS_ICON_EYE_SLASH_FILL,
    "eyedropper": BS_ICON_EYEDROPPER,
    "eyeglasses": BS_ICON_EYEGLASSES,
    "facebook": BS_ICON_FACEBOOK,
    "fan": BS_ICON_FAN,
    "fast-forward": BS_ICON_FAST_FORWARD,
    "fast-forward-btn": BS_ICON_FAST_FORWARD_BTN,
    "fast-forward-btn-fill": BS_ICON_FAST_FORWARD_BTN_FILL,
    "fast-forward-circle": BS_ICON_FAST_FORWARD_CIRCLE,
    "fast-forward-circle-fill": BS_ICON_FAST_FORWARD_CIRCLE_FILL,
    "fast-forward-fill": BS_ICON_FAST_FORWARD_FILL,
    "feather": BS_ICON_FEATHER,
    "feather2": BS_ICON_FEATHER2,
    "file": BS_ICON_FILE,
    "file-arrow-down": BS_ICON_FILE_ARROW_DOWN,
    "file-arrow-down-fill": BS_ICON_FILE_ARROW_DOWN_FILL,
    "file-arrow-up": BS_ICON_FILE_ARROW_UP,
    "file-arrow-up-fill": BS_ICON_FILE_ARROW_UP_FILL,
    "file-bar-graph": BS_ICON_FILE_BAR_GRAPH,
    "file-bar-graph-fill": BS_ICON_FILE_BAR_GRAPH_FILL,
    "file-binary": BS_ICON_FILE_BINARY,
    "file-binary-fill": BS_ICON_FILE_BINARY_FILL,
    "file-break": BS_ICON_FILE_BREAK,
    "file-break-fill": BS_ICON_FILE_BREAK_FILL,
    "file-check": BS_ICON_FILE_CHECK,
    "file-check-fill": BS_ICON_FILE_CHECK_FILL,
    "file-code": BS_ICON_FILE_CODE,
    "file-code-fill": BS_ICON_FILE_CODE_FILL,
    "file-diff": BS_ICON_FILE_DIFF,
    "file-diff-fill": BS_ICON_FILE_DIFF_FILL,
    "file-earmark": BS_ICON_FILE_EARMARK,
    "file-earmark-arrow-down": BS_ICON_FILE_EARMARK_ARROW_DOWN,
    "file-earmark-arrow-down-fill": BS_ICON_FILE_EARMARK_ARROW_DOWN_FILL,
    "file-earmark-arrow-up": BS_ICON_FILE_EARMARK_ARROW_UP,
    "file-earmark-arrow-up-fill": BS_ICON_FILE_EARMARK_ARROW_UP_FILL,
    "file-earmark-bar-graph": BS_ICON_FILE_EARMARK_BAR_GRAPH,
    "file-earmark-bar-graph-fill": BS_ICON_FILE_EARMARK_BAR_GRAPH_FILL,
    "file-earmark-binary": BS_ICON_FILE_EARMARK_BINARY,
    "file-earmark-binary-fill": BS_ICON_FILE_EARMARK_BINARY_FILL,
    "file-earmark-break": BS_ICON_FILE_EARMARK_BREAK,
    "file-earmark-break-fill": BS_ICON_FILE_EARMARK_BREAK_FILL,
    "file-earmark-check": BS_ICON_FILE_EARMARK_CHECK,
    "file-earmark-check-fill": BS_ICON_FILE_EARMARK_CHECK_FILL,
    "file-earmark-code": BS_ICON_FILE_EARMARK_CODE,
    "file-earmark-code-fill": BS_ICON_FILE_EARMARK_CODE_FILL,
    "file-earmark-diff": BS_ICON_FILE_EARMARK_DIFF,
    "file-earmark-diff-fill": BS_ICON_FILE_EARMARK_DIFF_FILL,
    "file-earmark-easel": BS_ICON_FILE_EARMARK_EASEL,
    "file-earmark-easel-fill": BS_ICON_FILE_EARMARK_EASEL_FILL,
    "file-earmark-excel": BS_ICON_FILE_EARMARK_EXCEL,
    "file-earmark-excel-fill": BS_ICON_FILE_EARMARK_EXCEL_FILL,
    "file-earmark-fill": BS_ICON_FILE_EARMARK_FILL,
    "file-earmark-font": BS_ICON_FILE_EARMARK_FONT,
    "file-earmark-font-fill": BS_ICON_FILE_EARMARK_FONT_FILL,
    "file-earmark-image": BS_ICON_FILE_EARMARK_IMAGE,
    "file-earmark-image-fill": BS_ICON_FILE_EARMARK_IMAGE_FILL,
    "file-earmark-lock": BS_ICON_FILE_EARMARK_LOCK,
    "file-earmark-lock-fill": BS_ICON_FILE_EARMARK_LOCK_FILL,
    "file-earmark-lock2": BS_ICON_FILE_EARMARK_LOCK2,
    "file-earmark-lock2-fill": BS_ICON_FILE_EARMARK_LOCK2_FILL,
    "file-earmark-medical": BS_ICON_FILE_EARMARK_MEDICAL,
    "file-earmark-medical-fill": BS_ICON_FILE_EARMARK_MEDICAL_FILL,
    "file-earmark-minus": BS_ICON_FILE_EARMARK_MINUS,
    "file-earmark-minus-fill": BS_ICON_FILE_EARMARK_MINUS_FILL,
    "file-earmark-music": BS_ICON_FILE_EARMARK_MUSIC,
    "file-earmark-music-fill": BS_ICON_FILE_EARMARK_MUSIC_FILL,
    "file-earmark-pdf": BS_ICON_FILE_EARMARK_PDF,
    "file-earmark-pdf-fill": BS_ICON_FILE_EARMARK_PDF_FILL,
    "file-earmark-person": BS_ICON_FILE_EARMARK_PERSON,
    "file-earmark-person-fill": BS_ICON_FILE_EARMARK_PERSON_FILL,
    "file-earmark-play": BS_ICON_FILE_EARMARK_PLAY,
    "file-earmark-play-fill": BS_ICON_FILE_EARMARK_PLAY_FILL,
    "file-earmark-plus": BS_ICON_FILE_EARMARK_PLUS,
    "file-earmark-plus-fill": BS_ICON_FILE_EARMARK_PLUS_FILL,
    "file-earmark-post": BS_ICON_FILE_EARMARK_POST,
    "file-earmark-post-fill": BS_ICON_FILE_EARMARK_POST_FILL,
    "file-earmark-ppt": BS_ICON_FILE_EARMARK_PPT,
    "file-earmark-ppt-fill": BS_ICON_FILE_EARMARK_PPT_FILL,
    "file-earmark-richtext": BS_ICON_FILE_EARMARK_RICHTEXT,
    "file-earmark-richtext-fill": BS_ICON_FILE_EARMARK_RICHTEXT_FILL,
    "file-earmark-ruled": BS_ICON_FILE_EARMARK_RULED,
    "file-earmark-ruled-fill": BS_ICON_FILE_EARMARK_RULED_FILL,
    "file-earmark-slides": BS_ICON_FILE_EARMARK_SLIDES,
    "file-earmark-slides-fill": BS_ICON_FILE_EARMARK_SLIDES_FILL,
    "file-earmark-spreadsheet": BS_ICON_FILE_EARMARK_SPREADSHEET,
    "file-earmark-spreadsheet-fill": BS_ICON_FILE_EARMARK_SPREADSHEET_FILL,
    "file-earmark-text": BS_ICON_FILE_EARMARK_TEXT,
    "file-earmark-text-fill": BS_ICON_FILE_EARMARK_TEXT_FILL,
    "file-earmark-word": BS_ICON_FILE_EARMARK_WORD,
    "file-earmark-word-fill": BS_ICON_FILE_EARMARK_WORD_FILL,
    "file-earmark-x": BS_ICON_FILE_EARMARK_X,
    "file-earmark-x-fill": BS_ICON_FILE_EARMARK_X_FILL,
    "file-earmark-zip": BS_ICON_FILE_EARMARK_ZIP,
    "file-earmark-zip-fill": BS_ICON_FILE_EARMARK_ZIP_FILL,
    "file-easel": BS_ICON_FILE_EASEL,
    "file-easel-fill": BS_ICON_FILE_EASEL_FILL,
    "file-excel": BS_ICON_FILE_EXCEL,
    "file-excel-fill": BS_ICON_FILE_EXCEL_FILL,
    "file-fill": BS_ICON_FILE_FILL,
    "file-font": BS_ICON_FILE_FONT,
    "file-font-fill": BS_ICON_FILE_FONT_FILL,
    "file-image": BS_ICON_FILE_IMAGE,
    "file-image-fill": BS_ICON_FILE_IMAGE_FILL,
    "file-lock": BS_ICON_FILE_LOCK,
    "file-lock-fill": BS_ICON_FILE_LOCK_FILL,
    "file-lock2": BS_ICON_FILE_LOCK2,
    "file-lock2-fill": BS_ICON_FILE_LOCK2_FILL,
    "file-medical": BS_ICON_FILE_MEDICAL,
    "file-medical-fill": BS_ICON_FILE_MEDICAL_FILL,
    "file-minus": BS_ICON_FILE_MINUS,
    "file-minus-fill": BS_ICON_FILE_MINUS_FILL,
    "file-music": BS_ICON_FILE_MUSIC,
    "file-music-fill": BS_ICON_FILE_MUSIC_FILL,
    "file-pdf": BS_ICON_FILE_PDF,
    "file-pdf-fill": BS_ICON_FILE_PDF_FILL,
    "file-person": BS_ICON_FILE_PERSON,
    "file-person-fill": BS_ICON_FILE_PERSON_FILL,
    "file-play": BS_ICON_FILE_PLAY,
    "file-play-fill": BS_ICON_FILE_PLAY_FILL,
    "file-plus": BS_ICON_FILE_PLUS,
    "file-plus-fill": BS_ICON_FILE_PLUS_FILL,
    "file-post": BS_ICON_FILE_POST,
    "file-post-fill": BS_ICON_FILE_POST_FILL,
    "file-ppt": BS_ICON_FILE_PPT,
    "file-ppt-fill": BS_ICON_FILE_PPT_FILL,
    "file-richtext": BS_ICON_FILE_RICHTEXT,
    "file-richtext-fill": BS_ICON_FILE_RICHTEXT_FILL,
    "file-ruled": BS_ICON_FILE_RULED,
    "file-ruled-fill": BS_ICON_FILE_RULED_FILL,
    "file-slides": BS_ICON_FILE_SLIDES,
    "file-slides-fill": BS_ICON_FILE_SLIDES_FILL,
    "file-spreadsheet": BS_ICON_FILE_SPREADSHEET,
    "file-spreadsheet-fill": BS_ICON_FILE_SPREADSHEET_FILL,
    "file-text": BS_ICON_FILE_TEXT,
    "file-text-fill": BS_ICON_FILE_TEXT_FILL,
    "file-word": BS_ICON_FILE_WORD,
    "file-word-fill": BS_ICON_FILE_WORD_FILL,
    "file-x": BS_ICON_FILE_X,
    "file-x-fill": BS_ICON_FILE_X_FILL,
    "file-zip": BS_ICON_FILE_ZIP,
    "file-zip-fill": BS_ICON_FILE_ZIP_FILL,
    "files": BS_ICON_FILES,
    "files-alt": BS_ICON_FILES_ALT,
    "filetype-aac": BS_ICON_FILETYPE_AAC,
    "filetype-ai": BS_ICON_FILETYPE_AI,
    "filetype-bmp": BS_ICON_FILETYPE_BMP,
    "filetype-cs": BS_ICON_FILETYPE_CS,
    "filetype-css": BS_ICON_FILETYPE_CSS,
    "filetype-csv": BS_ICON_FILETYPE_CSV,
    "filetype-doc": BS_ICON_FILETYPE_DOC,
    "filetype-docx": BS_ICON_FILETYPE_DOCX,
    "filetype-exe": BS_ICON_FILETYPE_EXE,
    "filetype-gif": BS_ICON_FILETYPE_GIF,
    "filetype-heic": BS_ICON_FILETYPE_HEIC,
    "filetype-html": BS_ICON_FILETYPE_HTML,
    "filetype-java": BS_ICON_FILETYPE_JAVA,
    "filetype-jpg": BS_ICON_FILETYPE_JPG,
    "filetype-js": BS_ICON_FILETYPE_JS,
    "filetype-json": BS_ICON_FILETYPE_JSON,
    "filetype-jsx": BS_ICON_FILETYPE_JSX,
    "filetype-key": BS_ICON_FILETYPE_KEY,
    "filetype-m4p": BS_ICON_FILETYPE_M4P,
    "filetype-md": BS_ICON_FILETYPE_MD,
    "filetype-mdx": BS_ICON_FILETYPE_MDX,
    "filetype-mov": BS_ICON_FILETYPE_MOV,
    "filetype-mp3": BS_ICON_FILETYPE_MP3,
    "filetype-mp4": BS_ICON_FILETYPE_MP4,
    "filetype-otf": BS_ICON_FILETYPE_OTF,
    "filetype-pdf": BS_ICON_FILETYPE_PDF,
    "filetype-php": BS_ICON_FILETYPE_PHP,
    "filetype-png": BS_ICON_FILETYPE_PNG,
    "filetype-ppt": BS_ICON_FILETYPE_PPT,
    "filetype-pptx": BS_ICON_FILETYPE_PPTX,
    "filetype-psd": BS_ICON_FILETYPE_PSD,
    "filetype-py": BS_ICON_FILETYPE_PY,
    "filetype-raw": BS_ICON_FILETYPE_RAW,
    "filetype-rb": BS_ICON_FILETYPE_RB,
    "filetype-sass": BS_ICON_FILETYPE_SASS,
    "filetype-scss": BS_ICON_FILETYPE_SCSS,
    "filetype-sh": BS_ICON_FILETYPE_SH,
    "filetype-sql": BS_ICON_FILETYPE_SQL,
    "filetype-svg": BS_ICON_FILETYPE_SVG,
    "filetype-tiff": BS_ICON_FILETYPE_TIFF,
    "filetype-tsx": BS_ICON_FILETYPE_TSX,
    "filetype-ttf": BS_ICON_FILETYPE_TTF,
    "filetype-txt": BS_ICON_FILETYPE_TXT,
    "filetype-wav": BS_ICON_FILETYPE_WAV,
    "filetype-woff": BS_ICON_FILETYPE_WOFF,
    "filetype-xls": BS_ICON_FILETYPE_XLS,
    "filetype-xlsx": BS_ICON_FILETYPE_XLSX,
    "filetype-xml": BS_ICON_FILETYPE_XML,
    "filetype-yml": BS_ICON_FILETYPE_YML,
    "film": BS_ICON_FILM,
    "filter": BS_ICON_FILTER,
    "filter-circle": BS_ICON_FILTER_CIRCLE,
    "filter-circle-fill": BS_ICON_FILTER_CIRCLE_FILL,
    "filter-left": BS_ICON_FILTER_LEFT,
    "filter-right": BS_ICON_FILTER_RIGHT,
    "filter-square": BS_ICON_FILTER_SQUARE,
    "filter-square-fill": BS_ICON_FILTER_SQUARE_FILL,
    "fingerprint": BS_ICON_FINGERPRINT,
    "fire": BS_ICON_FIRE,
    "flag": BS_ICON_FLAG,
    "flag-fill": BS_ICON_FLAG_FILL,
    "floppy": BS_ICON_FLOPPY,
    "floppy-fill": BS_ICON_FLOPPY_FILL,
    "floppy2": BS_ICON_FLOPPY2,
    "floppy2-fill": BS_ICON_FLOPPY2_FILL,
    "flower1": BS_ICON_FLOWER1,
    "flower2": BS_ICON_FLOWER2,
    "flower3": BS_ICON_FLOWER3,
    "folder": BS_ICON_FOLDER,
    "folder-check": BS_ICON_FOLDER_CHECK,
    "folder-fill": BS_ICON_FOLDER_FILL,
    "folder-minus": BS_ICON_FOLDER_MINUS,
    "folder-plus": BS_ICON_FOLDER_PLUS,
    "folder-symlink": BS_ICON_FOLDER_SYMLINK,
    "folder-symlink-fill": BS_ICON_FOLDER_SYMLINK_FILL,
    "folder-x": BS_ICON_FOLDER_X,
    "folder2": BS_ICON_FOLDER2,
    "folder2-open": BS_ICON_FOLDER2_OPEN,
    "fonts": BS_ICON_FONTS,
    "forward": BS_ICON_FORWARD,
    "forward-fill": BS_ICON_FORWARD_FILL,
    "front": BS_ICON_FRONT,
    "fuel-pump": BS_ICON_FUEL_PUMP,
    "fuel-pump-diesel": BS_ICON_FUEL_PUMP_DIESEL,
    "fuel-pump-diesel-fill": BS_ICON_FUEL_PUMP_DIESEL_FILL,
    "fuel-pump-fill": BS_ICON_FUEL_PUMP_FILL,
    "fullscreen": BS_ICON_FULLSCREEN,
    "fullscreen-exit": BS_ICON_FULLSCREEN_EXIT,
    "funnel": BS_ICON_FUNNEL,
    "funnel-fill": BS_ICON_FUNNEL_FILL,
    "gear": BS_ICON_GEAR,
    "gear-fill": BS_ICON_GEAR_FILL,
    "gear-wide": BS_ICON_GEAR_WIDE,
    "gear-wide-connected": BS_ICON_GEAR_WIDE_CONNECTED,
    "gem": BS_ICON_GEM,
    "gender-ambiguous": BS_ICON_GENDER_AMBIGUOUS,
    "gender-female": BS_ICON_GENDER_FEMALE,
    "gender-male": BS_ICON_GENDER_MALE,
    "gender-neuter": BS_ICON_GENDER_NEUTER,
    "gender-trans": BS_ICON_GENDER_TRANS,
    "geo": BS_ICON_GEO,
    "geo-alt": BS_ICON_GEO_ALT,
    "geo-alt-fill": BS_ICON_GEO_ALT_FILL,
    "geo-fill": BS_ICON_GEO_FILL,
    "gift": BS_ICON_GIFT,
    "gift-fill": BS_ICON_GIFT_FILL,
    "git": BS_ICON_GIT,
    "github": BS_ICON_GITHUB,
    "gitlab": BS_ICON_GITLAB,
    "globe": BS_ICON_GLOBE,
    "globe-americas": BS_ICON_GLOBE_AMERICAS,
    "globe-asia-australia": BS_ICON_GLOBE_ASIA_AUSTRALIA,
    "globe-central-south-asia": BS_ICON_GLOBE_CENTRAL_SOUTH_ASIA,
    "globe-europe-africa": BS_ICON_GLOBE_EUROPE_AFRICA,
    "globe2": BS_ICON_GLOBE2,
    "google": BS_ICON_GOOGLE,
    "google-play": BS_ICON_GOOGLE_PLAY,
    "gpu-card": BS_ICON_GPU_CARD,
    "graph-down": BS_ICON_GRAPH_DOWN,
    "graph-down-arrow": BS_ICON_GRAPH_DOWN_ARROW,
    "graph-up": BS_ICON_GRAPH_UP,
    "graph-up-arrow": BS_ICON_GRAPH_UP_ARROW,
    "grid": BS_ICON_GRID,
    "grid-1x2": BS_ICON_GRID_1X2,
    "grid-1x2-fill": BS_ICON_GRID_1X2_FILL,
    "grid-3x2": BS_ICON_GRID_3X2,
    "grid-3x2-gap": BS_ICON_GRID_3X2_GAP,
    "grid-3x2-gap-fill": BS_ICON_GRID_3X2_GAP_FILL,
    "grid-3x3": BS_ICON_GRID_3X3,
    "grid-3x3-gap": BS_ICON_GRID_3X3_GAP,
    "grid-3x3-gap-fill": BS_ICON_GRID_3X3_GAP_FILL,
    "grid-fill": BS_ICON_GRID_FILL,
    "grip-horizontal": BS_ICON_GRIP_HORIZONTAL,
    "grip-vertical": BS_ICON_GRIP_VERTICAL,
    "h-circle": BS_ICON_H_CIRCLE,
    "h-circle-fill": BS_ICON_H_CIRCLE_FILL,
    "h-square": BS_ICON_H_SQUARE,
    "h-square-fill": BS_ICON_H_SQUARE_FILL,
    "hammer": BS_ICON_HAMMER,
    "hand-index": BS_ICON_HAND_INDEX,
    "hand-index-fill": BS_ICON_HAND_INDEX_FILL,
    "hand-index-thumb": BS_ICON_HAND_INDEX_THUMB,
    "hand-index-thumb-fill": BS_ICON_HAND_INDEX_THUMB_FILL,
    "hand-thumbs-down": BS_ICON_HAND_THUMBS_DOWN,
    "hand-thumbs-down-fill": BS_ICON_HAND_THUMBS_DOWN_FILL,
    "hand-thumbs-up": BS_ICON_HAND_THUMBS_UP,
    "hand-thumbs-up-fill": BS_ICON_HAND_THUMBS_UP_FILL,
    "handbag": BS_ICON_HANDBAG,
    "handbag-fill": BS_ICON_HANDBAG_FILL,
    "hash": BS_ICON_HASH,
    "hdd": BS_ICON_HDD,
    "hdd-fill": BS_ICON_HDD_FILL,
    "hdd-network": BS_ICON_HDD_NETWORK,
    "hdd-network-fill": BS_ICON_HDD_NETWORK_FILL,
    "hdd-rack": BS_ICON_HDD_RACK,
    "hdd-rack-fill": BS_ICON_HDD_RACK_FILL,
    "hdd-stack": BS_ICON_HDD_STACK,
    "hdd-stack-fill": BS_ICON_HDD_STACK_FILL,
    "hdmi": BS_ICON_HDMI,
    "hdmi-fill": BS_ICON_HDMI_FILL,
    "headphones": BS_ICON_HEADPHONES,
    "headset": BS_ICON_HEADSET,
    "headset-vr": BS_ICON_HEADSET_VR,
    "heart": BS_ICON_HEART,
    "heart-arrow": BS_ICON_HEART_ARROW,
    "heart-fill": BS_ICON_HEART_FILL,
    "heart-half": BS_ICON_HEART_HALF,
    "heart-pulse": BS_ICON_HEART_PULSE,
    "heart-pulse-fill": BS_ICON_HEART_PULSE_FILL,
    "heartbreak": BS_ICON_HEARTBREAK,
    "heartbreak-fill": BS_ICON_HEARTBREAK_FILL,
    "hearts": BS_ICON_HEARTS,
    "heptagon": BS_ICON_HEPTAGON,
    "heptagon-fill": BS_ICON_HEPTAGON_FILL,
    "heptagon-half": BS_ICON_HEPTAGON_HALF,
    "hexagon": BS_ICON_HEXAGON,
    "hexagon-fill": BS_ICON_HEXAGON_FILL,
    "hexagon-half": BS_ICON_HEXAGON_HALF,
    "highlighter": BS_ICON_HIGHLIGHTER,
    "highlights": BS_ICON_HIGHLIGHTS,
    "hospital": BS_ICON_HOSPITAL,
    "hospital-fill": BS_ICON_HOSPITAL_FILL,
    "hourglass": BS_ICON_HOURGLASS,
    "hourglass-bottom": BS_ICON_HOURGLASS_BOTTOM,
    "hourglass-split": BS_ICON_HOURGLASS_SPLIT,
    "hourglass-top": BS_ICON_HOURGLASS_TOP,
    "house": BS_ICON_HOUSE,
    "house-add": BS_ICON_HOUSE_ADD,
    "house-add-fill": BS_ICON_HOUSE_ADD_FILL,
    "house-check": BS_ICON_HOUSE_CHECK,
    "house-check-fill": BS_ICON_HOUSE_CHECK_FILL,
    "house-dash": BS_ICON_HOUSE_DASH,
    "house-dash-fill": BS_ICON_HOUSE_DASH_FILL,
    "house-door": BS_ICON_HOUSE_DOOR,
    "house-door-fill": BS_ICON_HOUSE_DOOR_FILL,
    "house-down": BS_ICON_HOUSE_DOWN,
    "house-down-fill": BS_ICON_HOUSE_DOWN_FILL,
    "house-exclamation": BS_ICON_HOUSE_EXCLAMATION,
    "house-exclamation-fill": BS_ICON_HOUSE_EXCLAMATION_FILL,
    "house-fill": BS_ICON_HOUSE_FILL,
    "house-gear": BS_ICON_HOUSE_GEAR,
    "house-gear-fill": BS_ICON_HOUSE_GEAR_FILL,
    "house-heart": BS_ICON_HOUSE_HEART,
    "house-heart-fill": BS_ICON_HOUSE_HEART_FILL,
    "house-lock": BS_ICON_HOUSE_LOCK,
    "house-lock-fill": BS_ICON_HOUSE_LOCK_FILL,
    "house-slash": BS_ICON_HOUSE_SLASH,
    "house-slash-fill": BS_ICON_HOUSE_SLASH_FILL,
    "house-up": BS_ICON_HOUSE_UP,
    "house-up-fill": BS_ICON_HOUSE_UP_FILL,
    "house-x": BS_ICON_HOUSE_X,
    "house-x-fill": BS_ICON_HOUSE_X_FILL,
    "houses": BS_ICON_HOUSES,
    "houses-fill": BS_ICON_HOUSES_FILL,
    "hr": BS_ICON_HR,
    "hurricane": BS_ICON_HURRICANE,
    "hypnotize": BS_ICON_HYPNOTIZE,
    "image": BS_ICON_IMAGE,
    "image-alt": BS_ICON_IMAGE_ALT,
    "image-fill": BS_ICON_IMAGE_FILL,
    "images": BS_ICON_IMAGES,
    "inbox": BS_ICON_INBOX,
    "inbox-fill": BS_ICON_INBOX_FILL,
    "inboxes": BS_ICON_INBOXES,
    "inboxes-fill": BS_ICON_INBOXES_FILL,
    "incognito": BS_ICON_INCOGNITO,
    "indent": BS_ICON_INDENT,
    "infinity": BS_ICON_INFINITY,
    "info": BS_ICON_INFO,
    "info-circle": BS_ICON_INFO_CIRCLE,
    "info-circle-fill": BS_ICON_INFO_CIRCLE_FILL,
    "info-lg": BS_ICON_INFO_LG,
    "info-square": BS_ICON_INFO_SQUARE,
    "info-square-fill": BS_ICON_INFO_SQUARE_FILL,
    "input-cursor": BS_ICON_INPUT_CURSOR,
    "input-cursor-text": BS_ICON_INPUT_CURSOR_TEXT,
    "instagram": BS_ICON_INSTAGRAM,
    "intersect": BS_ICON_INTERSECT,
    "journal": BS_ICON_JOURNAL,
    "journal-album": BS_ICON_JOURNAL_ALBUM,
    "journal-arrow-down": BS_ICON_JOURNAL_ARROW_DOWN,
    "journal-arrow-up": BS_ICON_JOURNAL_ARROW_UP,
    "journal-bookmark": BS_ICON_JOURNAL_BOOKMARK,
    "journal-bookmark-fill": BS_ICON_JOURNAL_BOOKMARK_FILL,
    "journal-check": BS_ICON_JOURNAL_CHECK,
    "journal-code": BS_ICON_JOURNAL_CODE,
    "journal-medical": BS_ICON_JOURNAL_MEDICAL,
    "journal-minus": BS_ICON_JOURNAL_MINUS,
    "journal-plus": BS_ICON_JOURNAL_PLUS,
    "journal-richtext": BS_ICON_JOURNAL_RICHTEXT,
    "journal-text": BS_ICON_JOURNAL_TEXT,
    "journal-x": BS_ICON_JOURNAL_X,
    "journals": BS_ICON_JOURNALS,
    "joystick": BS_ICON_JOYSTICK,
    "justify": BS_ICON_JUSTIFY,
    "justify-left": BS_ICON_JUSTIFY_LEFT,
    "justify-right": BS_ICON_JUSTIFY_RIGHT,
    "kanban": BS_ICON_KANBAN,
    "kanban-fill": BS_ICON_KANBAN_FILL,
    "key": BS_ICON_KEY,
    "key-fill": BS_ICON_KEY_FILL,
    "keyboard": BS_ICON_KEYBOARD,
    "keyboard-fill": BS_ICON_KEYBOARD_FILL,
    "ladder": BS_ICON_LADDER,
    "lamp": BS_ICON_LAMP,
    "lamp-fill": BS_ICON_LAMP_FILL,
    "laptop": BS_ICON_LAPTOP,
    "laptop-fill": BS_ICON_LAPTOP_FILL,
    "layer-backward": BS_ICON_LAYER_BACKWARD,
    "layer-forward": BS_ICON_LAYER_FORWARD,
    "layers": BS_ICON_LAYERS,
    "layers-fill": BS_ICON_LAYERS_FILL,
    "layers-half": BS_ICON_LAYERS_HALF,
    "layout-sidebar": BS_ICON_LAYOUT_SIDEBAR,
    "layout-sidebar-inset": BS_ICON_LAYOUT_SIDEBAR_INSET,
    "layout-sidebar-inset-reverse": BS_ICON_LAYOUT_SIDEBAR_INSET_REVERSE,
    "layout-sidebar-reverse": BS_ICON_LAYOUT_SIDEBAR_REVERSE,
    "layout-split": BS_ICON_LAYOUT_SPLIT,
    "layout-text-sidebar": BS_ICON_LAYOUT_TEXT_SIDEBAR,
    "layout-text-sidebar-reverse": BS_ICON_LAYOUT_TEXT_SIDEBAR_REVERSE,
    "layout-text-window": BS_ICON_LAYOUT_TEXT_WINDOW,
    "layout-text-window-reverse": BS_ICON_LAYOUT_TEXT_WINDOW_REVERSE,
    "layout-three-columns": BS_ICON_LAYOUT_THREE_COLUMNS,
    "layout-wtf": BS_ICON_LAYOUT_WTF,
    "life-preserver": BS_ICON_LIFE_PRESERVER,
    "lightbulb": BS_ICON_LIGHTBULB,
    "lightbulb-fill": BS_ICON_LIGHTBULB_FILL,
    "lightbulb-off": BS_ICON_LIGHTBULB_OFF,
    "lightbulb-off-fill": BS_ICON_LIGHTBULB_OFF_FILL,
    "lightning": BS_ICON_LIGHTNING,
    "lightning-charge": BS_ICON_LIGHTNING_CHARGE,
    "lightning-charge-fill": BS_ICON_LIGHTNING_CHARGE_FILL,
    "lightning-fill": BS_ICON_LIGHTNING_FILL,
    "line": BS_ICON_LINE,
    "link": BS_ICON_LINK,
    "link-45deg": BS_ICON_LINK_45DEG,
    "linkedin": BS_ICON_LINKEDIN,
    "list": BS_ICON_LIST,
    "list-check": BS_ICON_LIST_CHECK,
    "list-columns": BS_ICON_LIST_COLUMNS,
    "list-columns-reverse": BS_ICON_LIST_COLUMNS_REVERSE,
    "list-nested": BS_ICON_LIST_NESTED,
    "list-ol": BS_ICON_LIST_OL,
    "list-stars": BS_ICON_LIST_STARS,
    "list-task": BS_ICON_LIST_TASK,
    "list-ul": BS_ICON_LIST_UL,
    "lock": BS_ICON_LOCK,
    "lock-fill": BS_ICON_LOCK_FILL,
    "luggage": BS_ICON_LUGGAGE,
    "luggage-fill": BS_ICON_LUGGAGE_FILL,
    "lungs": BS_ICON_LUNGS,
    "lungs-fill": BS_ICON_LUNGS_FILL,
    "magic": BS_ICON_MAGIC,
    "magnet": BS_ICON_MAGNET,
    "magnet-fill": BS_ICON_MAGNET_FILL,
    "mailbox": BS_ICON_MAILBOX,
    "mailbox-flag": BS_ICON_MAILBOX_FLAG,
    "mailbox2": BS_ICON_MAILBOX2,
    "mailbox2-flag": BS_ICON_MAILBOX2_FLAG,
    "map": BS_ICON_MAP,
    "map-fill": BS_ICON_MAP_FILL,
    "markdown": BS_ICON_MARKDOWN,
    "markdown-fill": BS_ICON_MARKDOWN_FILL,
    "marker-tip": BS_ICON_MARKER_TIP,
    "mask": BS_ICON_MASK,
    "mastodon": BS_ICON_MASTODON,
    "medium": BS_ICON_MEDIUM,
    "megaphone": BS_ICON_MEGAPHONE,
    "megaphone-fill": BS_ICON_MEGAPHONE_FILL,
    "memory": BS_ICON_MEMORY,
    "menu-app": BS_ICON_MENU_APP,
    "menu-app-fill": BS_ICON_MENU_APP_FILL,
    "menu-button": BS_ICON_MENU_BUTTON,
    "menu-button-fill": BS_ICON_MENU_BUTTON_FILL,
    "menu-button-wide": BS_ICON_MENU_BUTTON_WIDE,
    "menu-button-wide-fill": BS_ICON_MENU_BUTTON_WIDE_FILL,
    "menu-down": BS_ICON_MENU_DOWN,
    "menu-up": BS_ICON_MENU_UP,
    "messenger": BS_ICON_MESSENGER,
    "meta": BS_ICON_META,
    "mic": BS_ICON_MIC,
    "mic-fill": BS_ICON_MIC_FILL,
    "mic-mute": BS_ICON_MIC_MUTE,
    "mic-mute-fill": BS_ICON_MIC_MUTE_FILL,
    "microsoft": BS_ICON_MICROSOFT,
    "microsoft-teams": BS_ICON_MICROSOFT_TEAMS,
    "minecart": BS_ICON_MINECART,
    "minecart-loaded": BS_ICON_MINECART_LOADED,
    "modem": BS_ICON_MODEM,
    "modem-fill": BS_ICON_MODEM_FILL,
    "moisture": BS_ICON_MOISTURE,
    "moon": BS_ICON_MOON,
    "moon-fill": BS_ICON_MOON_FILL,
    "moon-stars": BS_ICON_MOON_STARS,
    "moon-stars-fill": BS_ICON_MOON_STARS_FILL,
    "mortarboard": BS_ICON_MORTARBOARD,
    "mortarboard-fill": BS_ICON_MORTARBOARD_FILL,
    "motherboard": BS_ICON_MOTHERBOARD,
    "motherboard-fill": BS_ICON_MOTHERBOARD_FILL,
    "mouse": BS_ICON_MOUSE,
    "mouse-fill": BS_ICON_MOUSE_FILL,
    "mouse2": BS_ICON_MOUSE2,
    "mouse2-fill": BS_ICON_MOUSE2_FILL,
    "mouse3": BS_ICON_MOUSE3,
    "mouse3-fill": BS_ICON_MOUSE3_FILL,
    "music-note": BS_ICON_MUSIC_NOTE,
    "music-note-beamed": BS_ICON_MUSIC_NOTE_BEAMED,
    "music-note-list": BS_ICON_MUSIC_NOTE_LIST,
    "music-player": BS_ICON_MUSIC_PLAYER,
    "music-player-fill": BS_ICON_MUSIC_PLAYER_FILL,
    "newspaper": BS_ICON_NEWSPAPER,
    "nintendo-switch": BS_ICON_NINTENDO_SWITCH,
    "node-minus": BS_ICON_NODE_MINUS,
    "node-minus-fill": BS_ICON_NODE_MINUS_FILL,
    "node-plus": BS_ICON_NODE_PLUS,
    "node-plus-fill": BS_ICON_NODE_PLUS_FILL,
    "noise-reduction": BS_ICON_NOISE_REDUCTION,
    "nut": BS_ICON_NUT,
    "nut-fill": BS_ICON_NUT_FILL,
    "nvidia": BS_ICON_NVIDIA,
    "nvme": BS_ICON_NVME,
    "nvme-fill": BS_ICON_NVME_FILL,
    "octagon": BS_ICON_OCTAGON,
    "octagon-fill": BS_ICON_OCTAGON_FILL,
    "octagon-half": BS_ICON_OCTAGON_HALF,
    "opencollective": BS_ICON_OPENCOLLECTIVE,
    "optical-audio": BS_ICON_OPTICAL_AUDIO,
    "optical-audio-fill": BS_ICON_OPTICAL_AUDIO_FILL,
    "option": BS_ICON_OPTION,
    "outlet": BS_ICON_OUTLET,
    "p-circle": BS_ICON_P_CIRCLE,
    "p-circle-fill": BS_ICON_P_CIRCLE_FILL,
    "p-square": BS_ICON_P_SQUARE,
    "p-square-fill": BS_ICON_P_SQUARE_FILL,
    "paint-bucket": BS_ICON_PAINT_BUCKET,
    "palette": BS_ICON_PALETTE,
    "palette-fill": BS_ICON_PALETTE_FILL,
    "palette2": BS_ICON_PALETTE2,
    "paperclip": BS_ICON_PAPERCLIP,
    "paragraph": BS_ICON_PARAGRAPH,
    "pass": BS_ICON_PASS,
    "pass-fill": BS_ICON_PASS_FILL,
    "passport": BS_ICON_PASSPORT,
    "passport-fill": BS_ICON_PASSPORT_FILL,
    "patch-check": BS_ICON_PATCH_CHECK,
    "patch-check-fill": BS_ICON_PATCH_CHECK_FILL,
    "patch-exclamation": BS_ICON_PATCH_EXCLAMATION,
    "patch-exclamation-fill": BS_ICON_PATCH_EXCLAMATION_FILL,
    "patch-minus": BS_ICON_PATCH_MINUS,
    "patch-minus-fill": BS_ICON_PATCH_MINUS_FILL,
    "patch-plus": BS_ICON_PATCH_PLUS,
    "patch-plus-fill": BS_ICON_PATCH_PLUS_FILL,
    "patch-question": BS_ICON_PATCH_QUESTION,
    "patch-question-fill": BS_ICON_PATCH_QUESTION_FILL,
    "pause": BS_ICON_PAUSE,
    "pause-btn": BS_ICON_PAUSE_BTN,
    "pause-btn-fill": BS_ICON_PAUSE_BTN_FILL,
    "pause-circle": BS_ICON_PAUSE_CIRCLE,
    "pause-circle-fill": BS_ICON_PAUSE_CIRCLE_FILL,
    "pause-fill": BS_ICON_PAUSE_FILL,
    "paypal": BS_ICON_PAYPAL,
    "pc": BS_ICON_PC,
    "pc-display": BS_ICON_PC_DISPLAY,
    "pc-display-horizontal": BS_ICON_PC_DISPLAY_HORIZONTAL,
    "pc-horizontal": BS_ICON_PC_HORIZONTAL,
    "pci-card": BS_ICON_PCI_CARD,
    "pci-card-network": BS_ICON_PCI_CARD_NETWORK,
    "pci-card-sound": BS_ICON_PCI_CARD_SOUND,
    "peace": BS_ICON_PEACE,
    "peace-fill": BS_ICON_PEACE_FILL,
    "pen": BS_ICON_PEN,
    "pen-fill": BS_ICON_PEN_FILL,
    "pencil": BS_ICON_PENCIL,
    "pencil-fill": BS_ICON_PENCIL_FILL,
    "pencil-square": BS_ICON_PENCIL_SQUARE,
    "pentagon": BS_ICON_PENTAGON,
    "pentagon-fill": BS_ICON_PENTAGON_FILL,
    "pentagon-half": BS_ICON_PENTAGON_HALF,
    "people": BS_ICON_PEOPLE,
    "people-fill": BS_ICON_PEOPLE_FILL,
    "percent": BS_ICON_PERCENT,
    "person": BS_ICON_PERSON,
    "person-add": BS_ICON_PERSON_ADD,
    "person-arms-up": BS_ICON_PERSON_ARMS_UP,
    "person-badge": BS_ICON_PERSON_BADGE,
    "person-badge-fill": BS_ICON_PERSON_BADGE_FILL,
    "person-bounding-box": BS_ICON_PERSON_BOUNDING_BOX,
    "person-check": BS_ICON_PERSON_CHECK,
    "person-check-fill": BS_ICON_PERSON_CHECK_FILL,
    "person-circle": BS_ICON_PERSON_CIRCLE,
    "person-dash": BS_ICON_PERSON_DASH,
    "person-dash-fill": BS_ICON_PERSON_DASH_FILL,
    "person-down": BS_ICON_PERSON_DOWN,
    "person-exclamation": BS_ICON_PERSON_EXCLAMATION,
    "person-fill": BS_ICON_PERSON_FILL,
    "person-fill-add": BS_ICON_PERSON_FILL_ADD,
    "person-fill-check": BS_ICON_PERSON_FILL_CHECK,
    "person-fill-dash": BS_ICON_PERSON_FILL_DASH,
    "person-fill-down": BS_ICON_PERSON_FILL_DOWN,
    "person-fill-exclamation": BS_ICON_PERSON_FILL_EXCLAMATION,
    "person-fill-gear": BS_ICON_PERSON_FILL_GEAR,
    "person-fill-lock": BS_ICON_PERSON_FILL_LOCK,
    "person-fill-slash": BS_ICON_PERSON_FILL_SLASH,
    "person-fill-up": BS_ICON_PERSON_FILL_UP,
    "person-fill-x": BS_ICON_PERSON_FILL_X,
    "person-gear": BS_ICON_PERSON_GEAR,
    "person-heart": BS_ICON_PERSON_HEART,
    "person-hearts": BS_ICON_PERSON_HEARTS,
    "person-lines-fill": BS_ICON_PERSON_LINES_FILL,
    "person-lock": BS_ICON_PERSON_LOCK,
    "person-plus": BS_ICON_PERSON_PLUS,
    "person-plus-fill": BS_ICON_PERSON_PLUS_FILL,
    "person-raised-hand": BS_ICON_PERSON_RAISED_HAND,
    "person-rolodex": BS_ICON_PERSON_ROLODEX,
    "person-slash": BS_ICON_PERSON_SLASH,
    "person-square": BS_ICON_PERSON_SQUARE,
    "person-standing": BS_ICON_PERSON_STANDING,
    "person-standing-dress": BS_ICON_PERSON_STANDING_DRESS,
    "person-up": BS_ICON_PERSON_UP,
    "person-vcard": BS_ICON_PERSON_VCARD,
    "person-vcard-fill": BS_ICON_PERSON_VCARD_FILL,
    "person-video": BS_ICON_PERSON_VIDEO,
    "person-video2": BS_ICON_PERSON_VIDEO2,
    "person-video3": BS_ICON_PERSON_VIDEO3,
    "person-walking": BS_ICON_PERSON_WALKING,
    "person-wheelchair": BS_ICON_PERSON_WHEELCHAIR,
    "person-workspace": BS_ICON_PERSON_WORKSPACE,
    "person-x": BS_ICON_PERSON_X,
    "person-x-fill": BS_ICON_PERSON_X_FILL,
    "phone": BS_ICON_PHONE,
    "phone-fill": BS_ICON_PHONE_FILL,
    "phone-flip": BS_ICON_PHONE_FLIP,
    "phone-landscape": BS_ICON_PHONE_LANDSCAPE,
    "phone-landscape-fill": BS_ICON_PHONE_LANDSCAPE_FILL,
    "phone-vibrate": BS_ICON_PHONE_VIBRATE,
    "phone-vibrate-fill": BS_ICON_PHONE_VIBRATE_FILL,
    "pie-chart": BS_ICON_PIE_CHART,
    "pie-chart-fill": BS_ICON_PIE_CHART_FILL,
    "piggy-bank": BS_ICON_PIGGY_BANK,
    "piggy-bank-fill": BS_ICON_PIGGY_BANK_FILL,
    "pin": BS_ICON_PIN,
    "pin-angle": BS_ICON_PIN_ANGLE,
    "pin-angle-fill": BS_ICON_PIN_ANGLE_FILL,
    "pin-fill": BS_ICON_PIN_FILL,
    "pin-map": BS_ICON_PIN_MAP,
    "pin-map-fill": BS_ICON_PIN_MAP_FILL,
    "pinterest": BS_ICON_PINTEREST,
    "pip": BS_ICON_PIP,
    "pip-fill": BS_ICON_PIP_FILL,
    "play": BS_ICON_PLAY,
    "play-btn": BS_ICON_PLAY_BTN,
    "play-btn-fill": BS_ICON_PLAY_BTN_FILL,
    "play-circle": BS_ICON_PLAY_CIRCLE,
    "play-circle-fill": BS_ICON_PLAY_CIRCLE_FILL,
    "play-fill": BS_ICON_PLAY_FILL,
    "playstation": BS_ICON_PLAYSTATION,
    "plug": BS_ICON_PLUG,
    "plug-fill": BS_ICON_PLUG_FILL,
    "plugin": BS_ICON_PLUGIN,
    "plus": BS_ICON_PLUS,
    "plus-circle": BS_ICON_PLUS_CIRCLE,
    "plus-circle-dotted": BS_ICON_PLUS_CIRCLE_DOTTED,
    "plus-circle-fill": BS_ICON_PLUS_CIRCLE_FILL,
    "plus-lg": BS_ICON_PLUS_LG,
    "plus-slash-minus": BS_ICON_PLUS_SLASH_MINUS,
    "plus-square": BS_ICON_PLUS_SQUARE,
    "plus-square-dotted": BS_ICON_PLUS_SQUARE_DOTTED,
    "plus-square-fill": BS_ICON_PLUS_SQUARE_FILL,
    "postage": BS_ICON_POSTAGE,
    "postage-fill": BS_ICON_POSTAGE_FILL,
    "postage-heart": BS_ICON_POSTAGE_HEART,
    "postage-heart-fill": BS_ICON_POSTAGE_HEART_FILL,
    "postcard": BS_ICON_POSTCARD,
    "postcard-fill": BS_ICON_POSTCARD_FILL,
    "postcard-heart": BS_ICON_POSTCARD_HEART,
    "postcard-heart-fill": BS_ICON_POSTCARD_HEART_FILL,
    "power": BS_ICON_POWER,
    "prescription": BS_ICON_PRESCRIPTION,
    "prescription2": BS_ICON_PRESCRIPTION2,
    "printer": BS_ICON_PRINTER,
    "printer-fill": BS_ICON_PRINTER_FILL,
    "projector": BS_ICON_PROJECTOR,
    "projector-fill": BS_ICON_PROJECTOR_FILL,
    "puzzle": BS_ICON_PUZZLE,
    "puzzle-fill": BS_ICON_PUZZLE_FILL,
    "qr-code": BS_ICON_QR_CODE,
    "qr-code-scan": BS_ICON_QR_CODE_SCAN,
    "question": BS_ICON_QUESTION,
    "question-circle": BS_ICON_QUESTION_CIRCLE,
    "question-circle-fill": BS_ICON_QUESTION_CIRCLE_FILL,
    "question-diamond": BS_ICON_QUESTION_DIAMOND,
    "question-diamond-fill": BS_ICON_QUESTION_DIAMOND_FILL,
    "question-lg": BS_ICON_QUESTION_LG,
    "question-octagon": BS_ICON_QUESTION_OCTAGON,
    "question-octagon-fill": BS_ICON_QUESTION_OCTAGON_FILL,
    "question-square": BS_ICON_QUESTION_SQUARE,
    "question-square-fill": BS_ICON_QUESTION_SQUARE_FILL,
    "quora": BS_ICON_QUORA,
    "quote": BS_ICON_QUOTE,
    "r-circle": BS_ICON_R_CIRCLE,
    "r-circle-fill": BS_ICON_R_CIRCLE_FILL,
    "r-square": BS_ICON_R_SQUARE,
    "r-square-fill": BS_ICON_R_SQUARE_FILL,
    "radar": BS_ICON_RADAR,
    "radioactive": BS_ICON_RADIOACTIVE,
    "rainbow": BS_ICON_RAINBOW,
    "receipt": BS_ICON_RECEIPT,
    "receipt-cutoff": BS_ICON_RECEIPT_CUTOFF,
    "reception-0": BS_ICON_RECEPTION_0,
    "reception-1": BS_ICON_RECEPTION_1,
    "reception-2": BS_ICON_RECEPTION_2,
    "reception-3": BS_ICON_RECEPTION_3,
    "reception-4": BS_ICON_RECEPTION_4,
    "record": BS_ICON_RECORD,
    "record-btn": BS_ICON_RECORD_BTN,
    "record-btn-fill": BS_ICON_RECORD_BTN_FILL,
    "record-circle": BS_ICON_RECORD_CIRCLE,
    "record-circle-fill": BS_ICON_RECORD_CIRCLE_FILL,
    "record-fill": BS_ICON_RECORD_FILL,
    "record2": BS_ICON_RECORD2,
    "record2-fill": BS_ICON_RECORD2_FILL,
    "recycle": BS_ICON_RECYCLE,
    "reddit": BS_ICON_REDDIT,
    "regex": BS_ICON_REGEX,
    "repeat": BS_ICON_REPEAT,
    "repeat-1": BS_ICON_REPEAT_1,
    "reply": BS_ICON_REPLY,
    "reply-all": BS_ICON_REPLY_ALL,
    "reply-all-fill": BS_ICON_REPLY_ALL_FILL,
    "reply-fill": BS_ICON_REPLY_FILL,
    "rewind": BS_ICON_REWIND,
    "rewind-btn": BS_ICON_REWIND_BTN,
    "rewind-btn-fill": BS_ICON_REWIND_BTN_FILL,
    "rewind-circle": BS_ICON_REWIND_CIRCLE,
    "rewind-circle-fill": BS_ICON_REWIND_CIRCLE_FILL,
    "rewind-fill": BS_ICON_REWIND_FILL,
    "robot": BS_ICON_ROBOT,
    "rocket": BS_ICON_ROCKET,
    "rocket-fill": BS_ICON_ROCKET_FILL,
    "rocket-takeoff": BS_ICON_ROCKET_TAKEOFF,
    "rocket-takeoff-fill": BS_ICON_ROCKET_TAKEOFF_FILL,
    "router": BS_ICON_ROUTER,
    "router-fill": BS_ICON_ROUTER_FILL,
    "rss": BS_ICON_RSS,
    "rss-fill": BS_ICON_RSS_FILL,
    "rulers": BS_ICON_RULERS,
    "safe": BS_ICON_SAFE,
    "safe-fill": BS_ICON_SAFE_FILL,
    "safe2": BS_ICON_SAFE2,
    "safe2-fill": BS_ICON_SAFE2_FILL,
    "save": BS_ICON_SAVE,
    "save-fill": BS_ICON_SAVE_FILL,
    "save2": BS_ICON_SAVE2,
    "save2-fill": BS_ICON_SAVE2_FILL,
    "scissors": BS_ICON_SCISSORS,
    "scooter": BS_ICON_SCOOTER,
    "screwdriver": BS_ICON_SCREWDRIVER,
    "sd-card": BS_ICON_SD_CARD,
    "sd-card-fill": BS_ICON_SD_CARD_FILL,
    "search": BS_ICON_SEARCH,
    "search-heart": BS_ICON_SEARCH_HEART,
    "search-heart-fill": BS_ICON_SEARCH_HEART_FILL,
    "segmented-nav": BS_ICON_SEGMENTED_NAV,
    "send": BS_ICON_SEND,
    "send-arrow-down": BS_ICON_SEND_ARROW_DOWN,
    "send-arrow-down-fill": BS_ICON_SEND_ARROW_DOWN_FILL,
    "send-arrow-up": BS_ICON_SEND_ARROW_UP,
    "send-arrow-up-fill": BS_ICON_SEND_ARROW_UP_FILL,
    "send-check": BS_ICON_SEND_CHECK,
    "send-check-fill": BS_ICON_SEND_CHECK_FILL,
    "send-dash": BS_ICON_SEND_DASH,
    "send-dash-fill": BS_ICON_SEND_DASH_FILL,
    "send-exclamation": BS_ICON_SEND_EXCLAMATION,
    "send-exclamation-fill": BS_ICON_SEND_EXCLAMATION_FILL,
    "send-fill": BS_ICON_SEND_FILL,
    "send-plus": BS_ICON_SEND_PLUS,
    "send-plus-fill": BS_ICON_SEND_PLUS_FILL,
    "send-slash": BS_ICON_SEND_SLASH,
    "send-slash-fill": BS_ICON_SEND_SLASH_FILL,
    "send-x": BS_ICON_SEND_X,
    "send-x-fill": BS_ICON_SEND_X_FILL,
    "server": BS_ICON_SERVER,
    "shadows": BS_ICON_SHADOWS,
    "share": BS_ICON_SHARE,
    "share-fill": BS_ICON_SHARE_FILL,
    "shield": BS_ICON_SHIELD,
    "shield-check": BS_ICON_SHIELD_CHECK,
    "shield-exclamation": BS_ICON_SHIELD_EXCLAMATION,
    "shield-fill": BS_ICON_SHIELD_FILL,
    "shield-fill-check": BS_ICON_SHIELD_FILL_CHECK,
    "shield-fill-exclamation": BS_ICON_SHIELD_FILL_EXCLAMATION,
    "shield-fill-minus": BS_ICON_SHIELD_FILL_MINUS,
    "shield-fill-plus": BS_ICON_SHIELD_FILL_PLUS,
    "shield-fill-x": BS_ICON_SHIELD_FILL_X,
    "shield-lock": BS_ICON_SHIELD_LOCK,
    "shield-lock-fill": BS_ICON_SHIELD_LOCK_FILL,
    "shield-minus": BS_ICON_SHIELD_MINUS,
    "shield-plus": BS_ICON_SHIELD_PLUS,
    "shield-shaded": BS_ICON_SHIELD_SHADED,
    "shield-slash": BS_ICON_SHIELD_SLASH,
    "shield-slash-fill": BS_ICON_SHIELD_SLASH_FILL,
    "shield-x": BS_ICON_SHIELD_X,
    "shift": BS_ICON_SHIFT,
    "shift-fill": BS_ICON_SHIFT_FILL,
    "shop": BS_ICON_SHOP,
    "shop-window": BS_ICON_SHOP_WINDOW,
    "shuffle": BS_ICON_SHUFFLE,
    "sign-dead-end": BS_ICON_SIGN_DEAD_END,
    "sign-dead-end-fill": BS_ICON_SIGN_DEAD_END_FILL,
    "sign-do-not-enter": BS_ICON_SIGN_DO_NOT_ENTER,
    "sign-do-not-enter-fill": BS_ICON_SIGN_DO_NOT_ENTER_FILL,
    "sign-intersection": BS_ICON_SIGN_INTERSECTION,
    "sign-intersection-fill": BS_ICON_SIGN_INTERSECTION_FILL,
    "sign-intersection-side": BS_ICON_SIGN_INTERSECTION_SIDE,
    "sign-intersection-side-fill": BS_ICON_SIGN_INTERSECTION_SIDE_FILL,
    "sign-intersection-t": BS_ICON_SIGN_INTERSECTION_T,
    "sign-intersection-t-fill": BS_ICON_SIGN_INTERSECTION_T_FILL,
    "sign-intersection-y": BS_ICON_SIGN_INTERSECTION_Y,
    "sign-intersection-y-fill": BS_ICON_SIGN_INTERSECTION_Y_FILL,
    "sign-merge-left": BS_ICON_SIGN_MERGE_LEFT,
    "sign-merge-left-fill": BS_ICON_SIGN_MERGE_LEFT_FILL,
    "sign-merge-right": BS_ICON_SIGN_MERGE_RIGHT,
    "sign-merge-right-fill": BS_ICON_SIGN_MERGE_RIGHT_FILL,
    "sign-no-left-turn": BS_ICON_SIGN_NO_LEFT_TURN,
    "sign-no-left-turn-fill": BS_ICON_SIGN_NO_LEFT_TURN_FILL,
    "sign-no-parking": BS_ICON_SIGN_NO_PARKING,
    "sign-no-parking-fill": BS_ICON_SIGN_NO_PARKING_FILL,
    "sign-no-right-turn": BS_ICON_SIGN_NO_RIGHT_TURN,
    "sign-no-right-turn-fill": BS_ICON_SIGN_NO_RIGHT_TURN_FILL,
    "sign-railroad": BS_ICON_SIGN_RAILROAD,
    "sign-railroad-fill": BS_ICON_SIGN_RAILROAD_FILL,
    "sign-stop": BS_ICON_SIGN_STOP,
    "sign-stop-fill": BS_ICON_SIGN_STOP_FILL,
    "sign-stop-lights": BS_ICON_SIGN_STOP_LIGHTS,
    "sign-stop-lights-fill": BS_ICON_SIGN_STOP_LIGHTS_FILL,
    "sign-turn-left": BS_ICON_SIGN_TURN_LEFT,
    "sign-turn-left-fill": BS_ICON_SIGN_TURN_LEFT_FILL,
    "sign-turn-right": BS_ICON_SIGN_TURN_RIGHT,
    "sign-turn-right-fill": BS_ICON_SIGN_TURN_RIGHT_FILL,
    "sign-turn-slight-left": BS_ICON_SIGN_TURN_SLIGHT_LEFT,
    "sign-turn-slight-left-fill": BS_ICON_SIGN_TURN_SLIGHT_LEFT_FILL,
    "sign-turn-slight-right": BS_ICON_SIGN_TURN_SLIGHT_RIGHT,
    "sign-turn-slight-right-fill": BS_ICON_SIGN_TURN_SLIGHT_RIGHT_FILL,
    "sign-yield": BS_ICON_SIGN_YIELD,
    "sign-yield-fill": BS_ICON_SIGN_YIELD_FILL,
    "signal": BS_ICON_SIGNAL,
    "signpost": BS_ICON_SIGNPOST,
    "signpost-2": BS_ICON_SIGNPOST_2,
    "signpost-2-fill": BS_ICON_SIGNPOST_2_FILL,
    "signpost-fill": BS_ICON_SIGNPOST_FILL,
    "signpost-split": BS_ICON_SIGNPOST_SPLIT,
    "signpost-split-fill": BS_ICON_SIGNPOST_SPLIT_FILL,
    "sim": BS_ICON_SIM,
    "sim-fill": BS_ICON_SIM_FILL,
    "sim-slash": BS_ICON_SIM_SLASH,
    "sim-slash-fill": BS_ICON_SIM_SLASH_FILL,
    "sina-weibo": BS_ICON_SINA_WEIBO,
    "skip-backward": BS_ICON_SKIP_BACKWARD,
    "skip-backward-btn": BS_ICON_SKIP_BACKWARD_BTN,
    "skip-backward-btn-fill": BS_ICON_SKIP_BACKWARD_BTN_FILL,
    "skip-backward-circle": BS_ICON_SKIP_BACKWARD_CIRCLE,
    "skip-backward-circle-fill": BS_ICON_SKIP_BACKWARD_CIRCLE_FILL,
    "skip-backward-fill": BS_ICON_SKIP_BACKWARD_FILL,
    "skip-end": BS_ICON_SKIP_END,
    "skip-end-btn": BS_ICON_SKIP_END_BTN,
    "skip-end-btn-fill": BS_ICON_SKIP_END_BTN_FILL,
    "skip-end-circle": BS_ICON_SKIP_END_CIRCLE,
    "skip-end-circle-fill": BS_ICON_SKIP_END_CIRCLE_FILL,
    "skip-end-fill": BS_ICON_SKIP_END_FILL,
    "skip-forward": BS_ICON_SKIP_FORWARD,
    "skip-forward-btn": BS_ICON_SKIP_FORWARD_BTN,
    "skip-forward-btn-fill": BS_ICON_SKIP_FORWARD_BTN_FILL,
    "skip-forward-circle": BS_ICON_SKIP_FORWARD_CIRCLE,
    "skip-forward-circle-fill": BS_ICON_SKIP_FORWARD_CIRCLE_FILL,
    "skip-forward-fill": BS_ICON_SKIP_FORWARD_FILL,
    "skip-start": BS_ICON_SKIP_START,
    "skip-start-btn": BS_ICON_SKIP_START_BTN,
    "skip-start-btn-fill": BS_ICON_SKIP_START_BTN_FILL,
    "skip-start-circle": BS_ICON_SKIP_START_CIRCLE,
    "skip-start-circle-fill": BS_ICON_SKIP_START_CIRCLE_FILL,
    "skip-start-fill": BS_ICON_SKIP_START_FILL,
    "skype": BS_ICON_SKYPE,
    "slack": BS_ICON_SLACK,
    "slash": BS_ICON_SLASH,
    "slash-circle": BS_ICON_SLASH_CIRCLE,
    "slash-circle-fill": BS_ICON_SLASH_CIRCLE_FILL,
    "slash-lg": BS_ICON_SLASH_LG,
    "slash-square": BS_ICON_SLASH_SQUARE,
    "slash-square-fill": BS_ICON_SLASH_SQUARE_FILL,
    "sliders": BS_ICON_SLIDERS,
    "sliders2": BS_ICON_SLIDERS2,
    "sliders2-vertical": BS_ICON_SLIDERS2_VERTICAL,
    "smartwatch": BS_ICON_SMARTWATCH,
    "snapchat": BS_ICON_SNAPCHAT,
    "snow": BS_ICON_SNOW,
    "snow2": BS_ICON_SNOW2,
    "snow3": BS_ICON_SNOW3,
    "sort-alpha-down": BS_ICON_SORT_ALPHA_DOWN,
    "sort-alpha-down-alt": BS_ICON_SORT_ALPHA_DOWN_ALT,
    "sort-alpha-up": BS_ICON_SORT_ALPHA_UP,
    "sort-alpha-up-alt": BS_ICON_SORT_ALPHA_UP_ALT,
    "sort-down": BS_ICON_SORT_DOWN,
    "sort-down-alt": BS_ICON_SORT_DOWN_ALT,
    "sort-numeric-down": BS_ICON_SORT_NUMERIC_DOWN,
    "sort-numeric-down-alt": BS_ICON_SORT_NUMERIC_DOWN_ALT,
    "sort-numeric-up": BS_ICON_SORT_NUMERIC_UP,
    "sort-numeric-up-alt": BS_ICON_SORT_NUMERIC_UP_ALT,
    "sort-up": BS_ICON_SORT_UP,
    "sort-up-alt": BS_ICON_SORT_UP_ALT,
    "soundwave": BS_ICON_SOUNDWAVE,
    "sourceforge": BS_ICON_SOURCEFORGE,
    "speaker": BS_ICON_SPEAKER,
    "speaker-fill": BS_ICON_SPEAKER_FILL,
    "speedometer": BS_ICON_SPEEDOMETER,
    "speedometer2": BS_ICON_SPEEDOMETER2,
    "spellcheck": BS_ICON_SPELLCHECK,
    "spotify": BS_ICON_SPOTIFY,
    "square": BS_ICON_SQUARE,
    "square-fill": BS_ICON_SQUARE_FILL,
    "square-half": BS_ICON_SQUARE_HALF,
    "stack": BS_ICON_STACK,
    "stack-overflow": BS_ICON_STACK_OVERFLOW,
    "star": BS_ICON_STAR,
    "star-fill": BS_ICON_STAR_FILL,
    "star-half": BS_ICON_STAR_HALF,
    "stars": BS_ICON_STARS,
    "steam": BS_ICON_STEAM,
    "stickies": BS_ICON_STICKIES,
    "stickies-fill": BS_ICON_STICKIES_FILL,
    "sticky": BS_ICON_STICKY,
    "sticky-fill": BS_ICON_STICKY_FILL,
    "stop": BS_ICON_STOP,
    "stop-btn": BS_ICON_STOP_BTN,
    "stop-btn-fill": BS_ICON_STOP_BTN_FILL,
    "stop-circle": BS_ICON_STOP_CIRCLE,
    "stop-circle-fill": BS_ICON_STOP_CIRCLE_FILL,
    "stop-fill": BS_ICON_STOP_FILL,
    "stoplights": BS_ICON_STOPLIGHTS,
    "stoplights-fill": BS_ICON_STOPLIGHTS_FILL,
    "stopwatch": BS_ICON_STOPWATCH,
    "stopwatch-fill": BS_ICON_STOPWATCH_FILL,
    "strava": BS_ICON_STRAVA,
    "stripe": BS_ICON_STRIPE,
    "subscript": BS_ICON_SUBSCRIPT,
    "substack": BS_ICON_SUBSTACK,
    "subtract": BS_ICON_SUBTRACT,
    "suit-club": BS_ICON_SUIT_CLUB,
    "suit-club-fill": BS_ICON_SUIT_CLUB_FILL,
    "suit-diamond": BS_ICON_SUIT_DIAMOND,
    "suit-diamond-fill": BS_ICON_SUIT_DIAMOND_FILL,
    "suit-heart": BS_ICON_SUIT_HEART,
    "suit-heart-fill": BS_ICON_SUIT_HEART_FILL,
    "suit-spade": BS_ICON_SUIT_SPADE,
    "suit-spade-fill": BS_ICON_SUIT_SPADE_FILL,
    "suitcase": BS_ICON_SUITCASE,
    "suitcase-fill": BS_ICON_SUITCASE_FILL,
    "suitcase-lg": BS_ICON_SUITCASE_LG,
    "suitcase-lg-fill": BS_ICON_SUITCASE_LG_FILL,
    "suitcase2": BS_ICON_SUITCASE2,
    "suitcase2-fill": BS_ICON_SUITCASE2_FILL,
    "sun": BS_ICON_SUN,
    "sun-fill": BS_ICON_SUN_FILL,
    "sunglasses": BS_ICON_SUNGLASSES,
    "sunrise": BS_ICON_SUNRISE,
    "sunrise-fill": BS_ICON_SUNRISE_FILL,
    "sunset": BS_ICON_SUNSET,
    "sunset-fill": BS_ICON_SUNSET_FILL,
    "superscript": BS_ICON_SUPERSCRIPT,
    "symmetry-horizontal": BS_ICON_SYMMETRY_HORIZONTAL,
    "symmetry-vertical": BS_ICON_SYMMETRY_VERTICAL,
    "table": BS_ICON_TABLE,
    "tablet": BS_ICON_TABLET,
    "tablet-fill": BS_ICON_TABLET_FILL,
    "tablet-landscape": BS_ICON_TABLET_LANDSCAPE,
    "tablet-landscape-fill": BS_ICON_TABLET_LANDSCAPE_FILL,
    "tag": BS_ICON_TAG,
    "tag-fill": BS_ICON_TAG_FILL,
    "tags": BS_ICON_TAGS,
    "tags-fill": BS_ICON_TAGS_FILL,
    "taxi-front": BS_ICON_TAXI_FRONT,
    "taxi-front-fill": BS_ICON_TAXI_FRONT_FILL,
    "telegram": BS_ICON_TELEGRAM,
    "telephone": BS_ICON_TELEPHONE,
    "telephone-fill": BS_ICON_TELEPHONE_FILL,
    "telephone-forward": BS_ICON_TELEPHONE_FORWARD,
    "telephone-forward-fill": BS_ICON_TELEPHONE_FORWARD_FILL,
    "telephone-inbound": BS_ICON_TELEPHONE_INBOUND,
    "telephone-inbound-fill": BS_ICON_TELEPHONE_INBOUND_FILL,
    "telephone-minus": BS_ICON_TELEPHONE_MINUS,
    "telephone-minus-fill": BS_ICON_TELEPHONE_MINUS_FILL,
    "telephone-outbound": BS_ICON_TELEPHONE_OUTBOUND,
    "telephone-outbound-fill": BS_ICON_TELEPHONE_OUTBOUND_FILL,
    "telephone-plus": BS_ICON_TELEPHONE_PLUS,
    "telephone-plus-fill": BS_ICON_TELEPHONE_PLUS_FILL,
    "telephone-x": BS_ICON_TELEPHONE_X,
    "telephone-x-fill": BS_ICON_TELEPHONE_X_FILL,
    "tencent-qq": BS_ICON_TENCENT_QQ,
    "terminal": BS_ICON_TERMINAL,
    "terminal-dash": BS_ICON_TERMINAL_DASH,
    "terminal-fill": BS_ICON_TERMINAL_FILL,
    "terminal-plus": BS_ICON_TERMINAL_PLUS,
    "terminal-split": BS_ICON_TERMINAL_SPLIT,
    "terminal-x": BS_ICON_TERMINAL_X,
    "text-center": BS_ICON_TEXT_CENTER,
    "text-indent-left": BS_ICON_TEXT_INDENT_LEFT,
    "text-indent-right": BS_ICON_TEXT_INDENT_RIGHT,
    "text-left": BS_ICON_TEXT_LEFT,
    "text-paragraph": BS_ICON_TEXT_PARAGRAPH,
    "text-right": BS_ICON_TEXT_RIGHT,
    "text-wrap": BS_ICON_TEXT_WRAP,
    "textarea": BS_ICON_TEXTAREA,
    "textarea-resize": BS_ICON_TEXTAREA_RESIZE,
    "textarea-t": BS_ICON_TEXTAREA_T,
    "thermometer": BS_ICON_THERMOMETER,
    "thermometer-half": BS_ICON_THERMOMETER_HALF,
    "thermometer-high": BS_ICON_THERMOMETER_HIGH,
    "thermometer-low": BS_ICON_THERMOMETER_LOW,
    "thermometer-snow": BS_ICON_THERMOMETER_SNOW,
    "thermometer-sun": BS_ICON_THERMOMETER_SUN,
    "threads": BS_ICON_THREADS,
    "threads-fill": BS_ICON_THREADS_FILL,
    "three-dots": BS_ICON_THREE_DOTS,
    "three-dots-vertical": BS_ICON_THREE_DOTS_VERTICAL,
    "thunderbolt": BS_ICON_THUNDERBOLT,
    "thunderbolt-fill": BS_ICON_THUNDERBOLT_FILL,
    "ticket": BS_ICON_TICKET,
    "ticket-detailed": BS_ICON_TICKET_DETAILED,
    "ticket-detailed-fill": BS_ICON_TICKET_DETAILED_FILL,
    "ticket-fill": BS_ICON_TICKET_FILL,
    "ticket-perforated": BS_ICON_TICKET_PERFORATED,
    "ticket-perforated-fill": BS_ICON_TICKET_PERFORATED_FILL,
    "tiktok": BS_ICON_TIKTOK,
    "toggle-off": BS_ICON_TOGGLE_OFF,
    "toggle-on": BS_ICON_TOGGLE_ON,
    "toggle2-off": BS_ICON_TOGGLE2_OFF,
    "toggle2-on": BS_ICON_TOGGLE2_ON,
    "toggles": BS_ICON_TOGGLES,
    "toggles2": BS_ICON_TOGGLES2,
    "tools": BS_ICON_TOOLS,
    "tornado": BS_ICON_TORNADO,
    "train-freight-front": BS_ICON_TRAIN_FREIGHT_FRONT,
    "train-freight-front-fill": BS_ICON_TRAIN_FREIGHT_FRONT_FILL,
    "train-front": BS_ICON_TRAIN_FRONT,
    "train-front-fill": BS_ICON_TRAIN_FRONT_FILL,
    "train-lightrail-front": BS_ICON_TRAIN_LIGHTRAIL_FRONT,
    "train-lightrail-front-fill": BS_ICON_TRAIN_LIGHTRAIL_FRONT_FILL,
    "translate": BS_ICON_TRANSLATE,
    "transparency": BS_ICON_TRANSPARENCY,
    "trash": BS_ICON_TRASH,
    "trash-fill": BS_ICON_TRASH_FILL,
    "trash2": BS_ICON_TRASH2,
    "trash2-fill": BS_ICON_TRASH2_FILL,
    "trash3": BS_ICON_TRASH3,
    "trash3-fill": BS_ICON_TRASH3_FILL,
    "tree": BS_ICON_TREE,
    "tree-fill": BS_ICON_TREE_FILL,
    "trello": BS_ICON_TRELLO,
    "triangle": BS_ICON_TRIANGLE,
    "triangle-fill": BS_ICON_TRIANGLE_FILL,
    "triangle-half": BS_ICON_TRIANGLE_HALF,
    "trophy": BS_ICON_TROPHY,
    "trophy-fill": BS_ICON_TROPHY_FILL,
    "tropical-storm": BS_ICON_TROPICAL_STORM,
    "truck": BS_ICON_TRUCK,
    "truck-flatbed": BS_ICON_TRUCK_FLATBED,
    "truck-front": BS_ICON_TRUCK_FRONT,
    "truck-front-fill": BS_ICON_TRUCK_FRONT_FILL,
    "tsunami": BS_ICON_TSUNAMI,
    "tv": BS_ICON_TV,
    "tv-fill": BS_ICON_TV_FILL,
    "twitch": BS_ICON_TWITCH,
    "twitter": BS_ICON_TWITTER,
    "twitter-x": BS_ICON_TWITTER_X,
    "type": BS_ICON_TYPE,
    "type-bold": BS_ICON_TYPE_BOLD,
    "type-h1": BS_ICON_TYPE_H1,
    "type-h2": BS_ICON_TYPE_H2,
    "type-h3": BS_ICON_TYPE_H3,
    "type-h4": BS_ICON_TYPE_H4,
    "type-h5": BS_ICON_TYPE_H5,
    "type-h6": BS_ICON_TYPE_H6,
    "type-italic": BS_ICON_TYPE_ITALIC,
    "type-strikethrough": BS_ICON_TYPE_STRIKETHROUGH,
    "type-underline": BS_ICON_TYPE_UNDERLINE,
    "ubuntu": BS_ICON_UBUNTU,
    "ui-checks": BS_ICON_UI_CHECKS,
    "ui-checks-grid": BS_ICON_UI_CHECKS_GRID,
    "ui-radios": BS_ICON_UI_RADIOS,
    "ui-radios-grid": BS_ICON_UI_RADIOS_GRID,
    "umbrella": BS_ICON_UMBRELLA,
    "umbrella-fill": BS_ICON_UMBRELLA_FILL,
    "unindent": BS_ICON_UNINDENT,
    "union": BS_ICON_UNION,
    "unity": BS_ICON_UNITY,
    "universal-access": BS_ICON_UNIVERSAL_ACCESS,
    "universal-access-circle": BS_ICON_UNIVERSAL_ACCESS_CIRCLE,
    "unlock": BS_ICON_UNLOCK,
    "unlock-fill": BS_ICON_UNLOCK_FILL,
    "upc": BS_ICON_UPC,
    "upc-scan": BS_ICON_UPC_SCAN,
    "upload": BS_ICON_UPLOAD,
    "usb": BS_ICON_USB,
    "usb-c": BS_ICON_USB_C,
    "usb-c-fill": BS_ICON_USB_C_FILL,
    "usb-drive": BS_ICON_USB_DRIVE,
    "usb-drive-fill": BS_ICON_USB_DRIVE_FILL,
    "usb-fill": BS_ICON_USB_FILL,
    "usb-micro": BS_ICON_USB_MICRO,
    "usb-micro-fill": BS_ICON_USB_MICRO_FILL,
    "usb-mini": BS_ICON_USB_MINI,
    "usb-mini-fill": BS_ICON_USB_MINI_FILL,
    "usb-plug": BS_ICON_USB_PLUG,
    "usb-plug-fill": BS_ICON_USB_PLUG_FILL,
    "usb-symbol": BS_ICON_USB_SYMBOL,
    "valentine": BS_ICON_VALENTINE,
    "valentine2": BS_ICON_VALENTINE2,
    "vector-pen": BS_ICON_VECTOR_PEN,
    "view-list": BS_ICON_VIEW_LIST,
    "view-stacked": BS_ICON_VIEW_STACKED,
    "vignette": BS_ICON_VIGNETTE,
    "vimeo": BS_ICON_VIMEO,
    "vinyl": BS_ICON_VINYL,
    "vinyl-fill": BS_ICON_VINYL_FILL,
    "virus": BS_ICON_VIRUS,
    "virus2": BS_ICON_VIRUS2,
    "voicemail": BS_ICON_VOICEMAIL,
    "volume-down": BS_ICON_VOLUME_DOWN,
    "volume-down-fill": BS_ICON_VOLUME_DOWN_FILL,
    "volume-mute": BS_ICON_VOLUME_MUTE,
    "volume-mute-fill": BS_ICON_VOLUME_MUTE_FILL,
    "volume-off": BS_ICON_VOLUME_OFF,
    "volume-off-fill": BS_ICON_VOLUME_OFF_FILL,
    "volume-up": BS_ICON_VOLUME_UP,
    "volume-up-fill": BS_ICON_VOLUME_UP_FILL,
    "vr": BS_ICON_VR,
    "wallet": BS_ICON_WALLET,
    "wallet-fill": BS_ICON_WALLET_FILL,
    "wallet2": BS_ICON_WALLET2,
    "watch": BS_ICON_WATCH,
    "water": BS_ICON_WATER,
    "webcam": BS_ICON_WEBCAM,
    "webcam-fill": BS_ICON_WEBCAM_FILL,
    "wechat": BS_ICON_WECHAT,
    "whatsapp": BS_ICON_WHATSAPP,
    "wifi": BS_ICON_WIFI,
    "wifi-1": BS_ICON_WIFI_1,
    "wifi-2": BS_ICON_WIFI_2,
    "wifi-off": BS_ICON_WIFI_OFF,
    "wikipedia": BS_ICON_WIKIPEDIA,
    "wind": BS_ICON_WIND,
    "window": BS_ICON_WINDOW,
    "window-dash": BS_ICON_WINDOW_DASH,
    "window-desktop": BS_ICON_WINDOW_DESKTOP,
    "window-dock": BS_ICON_WINDOW_DOCK,
    "window-fullscreen": BS_ICON_WINDOW_FULLSCREEN,
    "window-plus": BS_ICON_WINDOW_PLUS,
    "window-sidebar": BS_ICON_WINDOW_SIDEBAR,
    "window-split": BS_ICON_WINDOW_SPLIT,
    "window-stack": BS_ICON_WINDOW_STACK,
    "window-x": BS_ICON_WINDOW_X,
    "windows": BS_ICON_WINDOWS,
    "wordpress": BS_ICON_WORDPRESS,
    "wrench": BS_ICON_WRENCH,
    "wrench-adjustable": BS_ICON_WRENCH_ADJUSTABLE,
    "wrench-adjustable-circle": BS_ICON_WRENCH_ADJUSTABLE_CIRCLE,
    "wrench-adjustable-circle-fill": BS_ICON_WRENCH_ADJUSTABLE_CIRCLE_FILL,
    "x": BS_ICON_X,
    "x-circle": BS_ICON_X_CIRCLE,
    "x-circle-fill": BS_ICON_X_CIRCLE_FILL,
    "x-diamond": BS_ICON_X_DIAMOND,
    "x-diamond-fill": BS_ICON_X_DIAMOND_FILL,
    "x-lg": BS_ICON_X_LG,
    "x-octagon": BS_ICON_X_OCTAGON,
    "x-octagon-fill": BS_ICON_X_OCTAGON_FILL,
    "x-square": BS_ICON_X_SQUARE,
    "x-square-fill": BS_ICON_X_SQUARE_FILL,
    "xbox": BS_ICON_XBOX,
    "yelp": BS_ICON_YELP,
    "yin-yang": BS_ICON_YIN_YANG,
    "youtube": BS_ICON_YOUTUBE,
    "zoom-in": BS_ICON_ZOOM_IN,
    "zoom-out": BS_ICON_ZOOM_OUT,
};
export {
    BS_ICON_0_CIRCLE,
    BS_ICON_0_CIRCLE_FILL,
    BS_ICON_0_SQUARE,
    BS_ICON_0_SQUARE_FILL,
    BS_ICON_1_CIRCLE,
    BS_ICON_1_CIRCLE_FILL,
    BS_ICON_1_SQUARE,
    BS_ICON_1_SQUARE_FILL,
    BS_ICON_123,
    BS_ICON_2_CIRCLE,
    BS_ICON_2_CIRCLE_FILL,
    BS_ICON_2_SQUARE,
    BS_ICON_2_SQUARE_FILL,
    BS_ICON_3_CIRCLE,
    BS_ICON_3_CIRCLE_FILL,
    BS_ICON_3_SQUARE,
    BS_ICON_3_SQUARE_FILL,
    BS_ICON_4_CIRCLE,
    BS_ICON_4_CIRCLE_FILL,
    BS_ICON_4_SQUARE,
    BS_ICON_4_SQUARE_FILL,
    BS_ICON_5_CIRCLE,
    BS_ICON_5_CIRCLE_FILL,
    BS_ICON_5_SQUARE,
    BS_ICON_5_SQUARE_FILL,
    BS_ICON_6_CIRCLE,
    BS_ICON_6_CIRCLE_FILL,
    BS_ICON_6_SQUARE,
    BS_ICON_6_SQUARE_FILL,
    BS_ICON_7_CIRCLE,
    BS_ICON_7_CIRCLE_FILL,
    BS_ICON_7_SQUARE,
    BS_ICON_7_SQUARE_FILL,
    BS_ICON_8_CIRCLE,
    BS_ICON_8_CIRCLE_FILL,
    BS_ICON_8_SQUARE,
    BS_ICON_8_SQUARE_FILL,
    BS_ICON_9_CIRCLE,
    BS_ICON_9_CIRCLE_FILL,
    BS_ICON_9_SQUARE,
    BS_ICON_9_SQUARE_FILL,
    BS_ICON_ACTIVITY,
    BS_ICON_AIRPLANE,
    BS_ICON_AIRPLANE_ENGINES,
    BS_ICON_AIRPLANE_ENGINES_FILL,
    BS_ICON_AIRPLANE_FILL,
    BS_ICON_ALARM,
    BS_ICON_ALARM_FILL,
    BS_ICON_ALEXA,
    BS_ICON_ALIGN_BOTTOM,
    BS_ICON_ALIGN_CENTER,
    BS_ICON_ALIGN_END,
    BS_ICON_ALIGN_MIDDLE,
    BS_ICON_ALIGN_START,
    BS_ICON_ALIGN_TOP,
    BS_ICON_ALIPAY,
    BS_ICON_ALPHABET,
    BS_ICON_ALPHABET_UPPERCASE,
    BS_ICON_ALT,
    BS_ICON_AMAZON,
    BS_ICON_AMD,
    BS_ICON_ANDROID,
    BS_ICON_ANDROID2,
    BS_ICON_APP,
    BS_ICON_APP_INDICATOR,
    BS_ICON_APPLE,
    BS_ICON_ARCHIVE,
    BS_ICON_ARCHIVE_FILL,
    BS_ICON_ARROW_90DEG_DOWN,
    BS_ICON_ARROW_90DEG_LEFT,
    BS_ICON_ARROW_90DEG_RIGHT,
    BS_ICON_ARROW_90DEG_UP,
    BS_ICON_ARROW_BAR_DOWN,
    BS_ICON_ARROW_BAR_LEFT,
    BS_ICON_ARROW_BAR_RIGHT,
    BS_ICON_ARROW_BAR_UP,
    BS_ICON_ARROW_CLOCKWISE,
    BS_ICON_ARROW_COUNTERCLOCKWISE,
    BS_ICON_ARROW_DOWN,
    BS_ICON_ARROW_DOWN_CIRCLE,
    BS_ICON_ARROW_DOWN_CIRCLE_FILL,
    BS_ICON_ARROW_DOWN_LEFT,
    BS_ICON_ARROW_DOWN_LEFT_CIRCLE,
    BS_ICON_ARROW_DOWN_LEFT_CIRCLE_FILL,
    BS_ICON_ARROW_DOWN_LEFT_SQUARE,
    BS_ICON_ARROW_DOWN_LEFT_SQUARE_FILL,
    BS_ICON_ARROW_DOWN_RIGHT,
    BS_ICON_ARROW_DOWN_RIGHT_CIRCLE,
    BS_ICON_ARROW_DOWN_RIGHT_CIRCLE_FILL,
    BS_ICON_ARROW_DOWN_RIGHT_SQUARE,
    BS_ICON_ARROW_DOWN_RIGHT_SQUARE_FILL,
    BS_ICON_ARROW_DOWN_SHORT,
    BS_ICON_ARROW_DOWN_SQUARE,
    BS_ICON_ARROW_DOWN_SQUARE_FILL,
    BS_ICON_ARROW_DOWN_UP,
    BS_ICON_ARROW_LEFT,
    BS_ICON_ARROW_LEFT_CIRCLE,
    BS_ICON_ARROW_LEFT_CIRCLE_FILL,
    BS_ICON_ARROW_LEFT_RIGHT,
    BS_ICON_ARROW_LEFT_SHORT,
    BS_ICON_ARROW_LEFT_SQUARE,
    BS_ICON_ARROW_LEFT_SQUARE_FILL,
    BS_ICON_ARROW_REPEAT,
    BS_ICON_ARROW_RETURN_LEFT,
    BS_ICON_ARROW_RETURN_RIGHT,
    BS_ICON_ARROW_RIGHT,
    BS_ICON_ARROW_RIGHT_CIRCLE,
    BS_ICON_ARROW_RIGHT_CIRCLE_FILL,
    BS_ICON_ARROW_RIGHT_SHORT,
    BS_ICON_ARROW_RIGHT_SQUARE,
    BS_ICON_ARROW_RIGHT_SQUARE_FILL,
    BS_ICON_ARROW_THROUGH_HEART,
    BS_ICON_ARROW_THROUGH_HEART_FILL,
    BS_ICON_ARROW_UP,
    BS_ICON_ARROW_UP_CIRCLE,
    BS_ICON_ARROW_UP_CIRCLE_FILL,
    BS_ICON_ARROW_UP_LEFT,
    BS_ICON_ARROW_UP_LEFT_CIRCLE,
    BS_ICON_ARROW_UP_LEFT_CIRCLE_FILL,
    BS_ICON_ARROW_UP_LEFT_SQUARE,
    BS_ICON_ARROW_UP_LEFT_SQUARE_FILL,
    BS_ICON_ARROW_UP_RIGHT,
    BS_ICON_ARROW_UP_RIGHT_CIRCLE,
    BS_ICON_ARROW_UP_RIGHT_CIRCLE_FILL,
    BS_ICON_ARROW_UP_RIGHT_SQUARE,
    BS_ICON_ARROW_UP_RIGHT_SQUARE_FILL,
    BS_ICON_ARROW_UP_SHORT,
    BS_ICON_ARROW_UP_SQUARE,
    BS_ICON_ARROW_UP_SQUARE_FILL,
    BS_ICON_ARROWS,
    BS_ICON_ARROWS_ANGLE_CONTRACT,
    BS_ICON_ARROWS_ANGLE_EXPAND,
    BS_ICON_ARROWS_COLLAPSE,
    BS_ICON_ARROWS_COLLAPSE_VERTICAL,
    BS_ICON_ARROWS_EXPAND,
    BS_ICON_ARROWS_EXPAND_VERTICAL,
    BS_ICON_ARROWS_FULLSCREEN,
    BS_ICON_ARROWS_MOVE,
    BS_ICON_ARROWS_VERTICAL,
    BS_ICON_ASPECT_RATIO,
    BS_ICON_ASPECT_RATIO_FILL,
    BS_ICON_ASTERISK,
    BS_ICON_AT,
    BS_ICON_AWARD,
    BS_ICON_AWARD_FILL,
    BS_ICON_BACK,
    BS_ICON_BACKPACK,
    BS_ICON_BACKPACK_FILL,
    BS_ICON_BACKPACK2,
    BS_ICON_BACKPACK2_FILL,
    BS_ICON_BACKPACK3,
    BS_ICON_BACKPACK3_FILL,
    BS_ICON_BACKPACK4,
    BS_ICON_BACKPACK4_FILL,
    BS_ICON_BACKSPACE,
    BS_ICON_BACKSPACE_FILL,
    BS_ICON_BACKSPACE_REVERSE,
    BS_ICON_BACKSPACE_REVERSE_FILL,
    BS_ICON_BADGE_3D,
    BS_ICON_BADGE_3D_FILL,
    BS_ICON_BADGE_4K,
    BS_ICON_BADGE_4K_FILL,
    BS_ICON_BADGE_8K,
    BS_ICON_BADGE_8K_FILL,
    BS_ICON_BADGE_AD,
    BS_ICON_BADGE_AD_FILL,
    BS_ICON_BADGE_AR,
    BS_ICON_BADGE_AR_FILL,
    BS_ICON_BADGE_CC,
    BS_ICON_BADGE_CC_FILL,
    BS_ICON_BADGE_HD,
    BS_ICON_BADGE_HD_FILL,
    BS_ICON_BADGE_SD,
    BS_ICON_BADGE_SD_FILL,
    BS_ICON_BADGE_TM,
    BS_ICON_BADGE_TM_FILL,
    BS_ICON_BADGE_VO,
    BS_ICON_BADGE_VO_FILL,
    BS_ICON_BADGE_VR,
    BS_ICON_BADGE_VR_FILL,
    BS_ICON_BADGE_WC,
    BS_ICON_BADGE_WC_FILL,
    BS_ICON_BAG,
    BS_ICON_BAG_CHECK,
    BS_ICON_BAG_CHECK_FILL,
    BS_ICON_BAG_DASH,
    BS_ICON_BAG_DASH_FILL,
    BS_ICON_BAG_FILL,
    BS_ICON_BAG_HEART,
    BS_ICON_BAG_HEART_FILL,
    BS_ICON_BAG_PLUS,
    BS_ICON_BAG_PLUS_FILL,
    BS_ICON_BAG_X,
    BS_ICON_BAG_X_FILL,
    BS_ICON_BALLOON,
    BS_ICON_BALLOON_FILL,
    BS_ICON_BALLOON_HEART,
    BS_ICON_BALLOON_HEART_FILL,
    BS_ICON_BAN,
    BS_ICON_BAN_FILL,
    BS_ICON_BANDAID,
    BS_ICON_BANDAID_FILL,
    BS_ICON_BANK,
    BS_ICON_BANK2,
    BS_ICON_BAR_CHART,
    BS_ICON_BAR_CHART_FILL,
    BS_ICON_BAR_CHART_LINE,
    BS_ICON_BAR_CHART_LINE_FILL,
    BS_ICON_BAR_CHART_STEPS,
    BS_ICON_BASKET,
    BS_ICON_BASKET_FILL,
    BS_ICON_BASKET2,
    BS_ICON_BASKET2_FILL,
    BS_ICON_BASKET3,
    BS_ICON_BASKET3_FILL,
    BS_ICON_BATTERY,
    BS_ICON_BATTERY_CHARGING,
    BS_ICON_BATTERY_FULL,
    BS_ICON_BATTERY_HALF,
    BS_ICON_BEHANCE,
    BS_ICON_BELL,
    BS_ICON_BELL_FILL,
    BS_ICON_BELL_SLASH,
    BS_ICON_BELL_SLASH_FILL,
    BS_ICON_BEZIER,
    BS_ICON_BEZIER2,
    BS_ICON_BICYCLE,
    BS_ICON_BING,
    BS_ICON_BINOCULARS,
    BS_ICON_BINOCULARS_FILL,
    BS_ICON_BLOCKQUOTE_LEFT,
    BS_ICON_BLOCKQUOTE_RIGHT,
    BS_ICON_BLUETOOTH,
    BS_ICON_BODY_TEXT,
    BS_ICON_BOOK,
    BS_ICON_BOOK_FILL,
    BS_ICON_BOOK_HALF,
    BS_ICON_BOOKMARK,
    BS_ICON_BOOKMARK_CHECK,
    BS_ICON_BOOKMARK_CHECK_FILL,
    BS_ICON_BOOKMARK_DASH,
    BS_ICON_BOOKMARK_DASH_FILL,
    BS_ICON_BOOKMARK_FILL,
    BS_ICON_BOOKMARK_HEART,
    BS_ICON_BOOKMARK_HEART_FILL,
    BS_ICON_BOOKMARK_PLUS,
    BS_ICON_BOOKMARK_PLUS_FILL,
    BS_ICON_BOOKMARK_STAR,
    BS_ICON_BOOKMARK_STAR_FILL,
    BS_ICON_BOOKMARK_X,
    BS_ICON_BOOKMARK_X_FILL,
    BS_ICON_BOOKMARKS,
    BS_ICON_BOOKMARKS_FILL,
    BS_ICON_BOOKSHELF,
    BS_ICON_BOOMBOX,
    BS_ICON_BOOMBOX_FILL,
    BS_ICON_BOOTSTRAP,
    BS_ICON_BOOTSTRAP_FILL,
    BS_ICON_BOOTSTRAP_REBOOT,
    BS_ICON_BORDER,
    BS_ICON_BORDER_ALL,
    BS_ICON_BORDER_BOTTOM,
    BS_ICON_BORDER_CENTER,
    BS_ICON_BORDER_INNER,
    BS_ICON_BORDER_LEFT,
    BS_ICON_BORDER_MIDDLE,
    BS_ICON_BORDER_OUTER,
    BS_ICON_BORDER_RIGHT,
    BS_ICON_BORDER_STYLE,
    BS_ICON_BORDER_TOP,
    BS_ICON_BORDER_WIDTH,
    BS_ICON_BOUNDING_BOX,
    BS_ICON_BOUNDING_BOX_CIRCLES,
    BS_ICON_BOX,
    BS_ICON_BOX_ARROW_DOWN,
    BS_ICON_BOX_ARROW_DOWN_LEFT,
    BS_ICON_BOX_ARROW_DOWN_RIGHT,
    BS_ICON_BOX_ARROW_IN_DOWN,
    BS_ICON_BOX_ARROW_IN_DOWN_LEFT,
    BS_ICON_BOX_ARROW_IN_DOWN_RIGHT,
    BS_ICON_BOX_ARROW_IN_LEFT,
    BS_ICON_BOX_ARROW_IN_RIGHT,
    BS_ICON_BOX_ARROW_IN_UP,
    BS_ICON_BOX_ARROW_IN_UP_LEFT,
    BS_ICON_BOX_ARROW_IN_UP_RIGHT,
    BS_ICON_BOX_ARROW_LEFT,
    BS_ICON_BOX_ARROW_RIGHT,
    BS_ICON_BOX_ARROW_UP,
    BS_ICON_BOX_ARROW_UP_LEFT,
    BS_ICON_BOX_ARROW_UP_RIGHT,
    BS_ICON_BOX_FILL,
    BS_ICON_BOX_SEAM,
    BS_ICON_BOX_SEAM_FILL,
    BS_ICON_BOX2,
    BS_ICON_BOX2_FILL,
    BS_ICON_BOX2_HEART,
    BS_ICON_BOX2_HEART_FILL,
    BS_ICON_BOXES,
    BS_ICON_BRACES,
    BS_ICON_BRACES_ASTERISK,
    BS_ICON_BRICKS,
    BS_ICON_BRIEFCASE,
    BS_ICON_BRIEFCASE_FILL,
    BS_ICON_BRIGHTNESS_ALT_HIGH,
    BS_ICON_BRIGHTNESS_ALT_HIGH_FILL,
    BS_ICON_BRIGHTNESS_ALT_LOW,
    BS_ICON_BRIGHTNESS_ALT_LOW_FILL,
    BS_ICON_BRIGHTNESS_HIGH,
    BS_ICON_BRIGHTNESS_HIGH_FILL,
    BS_ICON_BRIGHTNESS_LOW,
    BS_ICON_BRIGHTNESS_LOW_FILL,
    BS_ICON_BRILLIANCE,
    BS_ICON_BROADCAST,
    BS_ICON_BROADCAST_PIN,
    BS_ICON_BROWSER_CHROME,
    BS_ICON_BROWSER_EDGE,
    BS_ICON_BROWSER_FIREFOX,
    BS_ICON_BROWSER_SAFARI,
    BS_ICON_BRUSH,
    BS_ICON_BRUSH_FILL,
    BS_ICON_BUCKET,
    BS_ICON_BUCKET_FILL,
    BS_ICON_BUG,
    BS_ICON_BUG_FILL,
    BS_ICON_BUILDING,
    BS_ICON_BUILDING_ADD,
    BS_ICON_BUILDING_CHECK,
    BS_ICON_BUILDING_DASH,
    BS_ICON_BUILDING_DOWN,
    BS_ICON_BUILDING_EXCLAMATION,
    BS_ICON_BUILDING_FILL,
    BS_ICON_BUILDING_FILL_ADD,
    BS_ICON_BUILDING_FILL_CHECK,
    BS_ICON_BUILDING_FILL_DASH,
    BS_ICON_BUILDING_FILL_DOWN,
    BS_ICON_BUILDING_FILL_EXCLAMATION,
    BS_ICON_BUILDING_FILL_GEAR,
    BS_ICON_BUILDING_FILL_LOCK,
    BS_ICON_BUILDING_FILL_SLASH,
    BS_ICON_BUILDING_FILL_UP,
    BS_ICON_BUILDING_FILL_X,
    BS_ICON_BUILDING_GEAR,
    BS_ICON_BUILDING_LOCK,
    BS_ICON_BUILDING_SLASH,
    BS_ICON_BUILDING_UP,
    BS_ICON_BUILDING_X,
    BS_ICON_BUILDINGS,
    BS_ICON_BUILDINGS_FILL,
    BS_ICON_BULLSEYE,
    BS_ICON_BUS_FRONT,
    BS_ICON_BUS_FRONT_FILL,
    BS_ICON_C_CIRCLE,
    BS_ICON_C_CIRCLE_FILL,
    BS_ICON_C_SQUARE,
    BS_ICON_C_SQUARE_FILL,
    BS_ICON_CAKE,
    BS_ICON_CAKE_FILL,
    BS_ICON_CAKE2,
    BS_ICON_CAKE2_FILL,
    BS_ICON_CALCULATOR,
    BS_ICON_CALCULATOR_FILL,
    BS_ICON_CALENDAR,
    BS_ICON_CALENDAR_CHECK,
    BS_ICON_CALENDAR_CHECK_FILL,
    BS_ICON_CALENDAR_DATE,
    BS_ICON_CALENDAR_DATE_FILL,
    BS_ICON_CALENDAR_DAY,
    BS_ICON_CALENDAR_DAY_FILL,
    BS_ICON_CALENDAR_EVENT,
    BS_ICON_CALENDAR_EVENT_FILL,
    BS_ICON_CALENDAR_FILL,
    BS_ICON_CALENDAR_HEART,
    BS_ICON_CALENDAR_HEART_FILL,
    BS_ICON_CALENDAR_MINUS,
    BS_ICON_CALENDAR_MINUS_FILL,
    BS_ICON_CALENDAR_MONTH,
    BS_ICON_CALENDAR_MONTH_FILL,
    BS_ICON_CALENDAR_PLUS,
    BS_ICON_CALENDAR_PLUS_FILL,
    BS_ICON_CALENDAR_RANGE,
    BS_ICON_CALENDAR_RANGE_FILL,
    BS_ICON_CALENDAR_WEEK,
    BS_ICON_CALENDAR_WEEK_FILL,
    BS_ICON_CALENDAR_X,
    BS_ICON_CALENDAR_X_FILL,
    BS_ICON_CALENDAR2,
    BS_ICON_CALENDAR2_CHECK,
    BS_ICON_CALENDAR2_CHECK_FILL,
    BS_ICON_CALENDAR2_DATE,
    BS_ICON_CALENDAR2_DATE_FILL,
    BS_ICON_CALENDAR2_DAY,
    BS_ICON_CALENDAR2_DAY_FILL,
    BS_ICON_CALENDAR2_EVENT,
    BS_ICON_CALENDAR2_EVENT_FILL,
    BS_ICON_CALENDAR2_FILL,
    BS_ICON_CALENDAR2_HEART,
    BS_ICON_CALENDAR2_HEART_FILL,
    BS_ICON_CALENDAR2_MINUS,
    BS_ICON_CALENDAR2_MINUS_FILL,
    BS_ICON_CALENDAR2_MONTH,
    BS_ICON_CALENDAR2_MONTH_FILL,
    BS_ICON_CALENDAR2_PLUS,
    BS_ICON_CALENDAR2_PLUS_FILL,
    BS_ICON_CALENDAR2_RANGE,
    BS_ICON_CALENDAR2_RANGE_FILL,
    BS_ICON_CALENDAR2_WEEK,
    BS_ICON_CALENDAR2_WEEK_FILL,
    BS_ICON_CALENDAR2_X,
    BS_ICON_CALENDAR2_X_FILL,
    BS_ICON_CALENDAR3,
    BS_ICON_CALENDAR3_EVENT,
    BS_ICON_CALENDAR3_EVENT_FILL,
    BS_ICON_CALENDAR3_FILL,
    BS_ICON_CALENDAR3_RANGE,
    BS_ICON_CALENDAR3_RANGE_FILL,
    BS_ICON_CALENDAR3_WEEK,
    BS_ICON_CALENDAR3_WEEK_FILL,
    BS_ICON_CALENDAR4,
    BS_ICON_CALENDAR4_EVENT,
    BS_ICON_CALENDAR4_RANGE,
    BS_ICON_CALENDAR4_WEEK,
    BS_ICON_CAMERA,
    BS_ICON_CAMERA_FILL,
    BS_ICON_CAMERA_REELS,
    BS_ICON_CAMERA_REELS_FILL,
    BS_ICON_CAMERA_VIDEO,
    BS_ICON_CAMERA_VIDEO_FILL,
    BS_ICON_CAMERA_VIDEO_OFF,
    BS_ICON_CAMERA_VIDEO_OFF_FILL,
    BS_ICON_CAMERA2,
    BS_ICON_CAPSLOCK,
    BS_ICON_CAPSLOCK_FILL,
    BS_ICON_CAPSULE,
    BS_ICON_CAPSULE_PILL,
    BS_ICON_CAR_FRONT,
    BS_ICON_CAR_FRONT_FILL,
    BS_ICON_CARD_CHECKLIST,
    BS_ICON_CARD_HEADING,
    BS_ICON_CARD_IMAGE,
    BS_ICON_CARD_LIST,
    BS_ICON_CARD_TEXT,
    BS_ICON_CARET_DOWN,
    BS_ICON_CARET_DOWN_FILL,
    BS_ICON_CARET_DOWN_SQUARE,
    BS_ICON_CARET_DOWN_SQUARE_FILL,
    BS_ICON_CARET_LEFT,
    BS_ICON_CARET_LEFT_FILL,
    BS_ICON_CARET_LEFT_SQUARE,
    BS_ICON_CARET_LEFT_SQUARE_FILL,
    BS_ICON_CARET_RIGHT,
    BS_ICON_CARET_RIGHT_FILL,
    BS_ICON_CARET_RIGHT_SQUARE,
    BS_ICON_CARET_RIGHT_SQUARE_FILL,
    BS_ICON_CARET_UP,
    BS_ICON_CARET_UP_FILL,
    BS_ICON_CARET_UP_SQUARE,
    BS_ICON_CARET_UP_SQUARE_FILL,
    BS_ICON_CART,
    BS_ICON_CART_CHECK,
    BS_ICON_CART_CHECK_FILL,
    BS_ICON_CART_DASH,
    BS_ICON_CART_DASH_FILL,
    BS_ICON_CART_FILL,
    BS_ICON_CART_PLUS,
    BS_ICON_CART_PLUS_FILL,
    BS_ICON_CART_X,
    BS_ICON_CART_X_FILL,
    BS_ICON_CART2,
    BS_ICON_CART3,
    BS_ICON_CART4,
    BS_ICON_CASH,
    BS_ICON_CASH_COIN,
    BS_ICON_CASH_STACK,
    BS_ICON_CASSETTE,
    BS_ICON_CASSETTE_FILL,
    BS_ICON_CAST,
    BS_ICON_CC_CIRCLE,
    BS_ICON_CC_CIRCLE_FILL,
    BS_ICON_CC_SQUARE,
    BS_ICON_CC_SQUARE_FILL,
    BS_ICON_CHAT,
    BS_ICON_CHAT_DOTS,
    BS_ICON_CHAT_DOTS_FILL,
    BS_ICON_CHAT_FILL,
    BS_ICON_CHAT_HEART,
    BS_ICON_CHAT_HEART_FILL,
    BS_ICON_CHAT_LEFT,
    BS_ICON_CHAT_LEFT_DOTS,
    BS_ICON_CHAT_LEFT_DOTS_FILL,
    BS_ICON_CHAT_LEFT_FILL,
    BS_ICON_CHAT_LEFT_HEART,
    BS_ICON_CHAT_LEFT_HEART_FILL,
    BS_ICON_CHAT_LEFT_QUOTE,
    BS_ICON_CHAT_LEFT_QUOTE_FILL,
    BS_ICON_CHAT_LEFT_TEXT,
    BS_ICON_CHAT_LEFT_TEXT_FILL,
    BS_ICON_CHAT_QUOTE,
    BS_ICON_CHAT_QUOTE_FILL,
    BS_ICON_CHAT_RIGHT,
    BS_ICON_CHAT_RIGHT_DOTS,
    BS_ICON_CHAT_RIGHT_DOTS_FILL,
    BS_ICON_CHAT_RIGHT_FILL,
    BS_ICON_CHAT_RIGHT_HEART,
    BS_ICON_CHAT_RIGHT_HEART_FILL,
    BS_ICON_CHAT_RIGHT_QUOTE,
    BS_ICON_CHAT_RIGHT_QUOTE_FILL,
    BS_ICON_CHAT_RIGHT_TEXT,
    BS_ICON_CHAT_RIGHT_TEXT_FILL,
    BS_ICON_CHAT_SQUARE,
    BS_ICON_CHAT_SQUARE_DOTS,
    BS_ICON_CHAT_SQUARE_DOTS_FILL,
    BS_ICON_CHAT_SQUARE_FILL,
    BS_ICON_CHAT_SQUARE_HEART,
    BS_ICON_CHAT_SQUARE_HEART_FILL,
    BS_ICON_CHAT_SQUARE_QUOTE,
    BS_ICON_CHAT_SQUARE_QUOTE_FILL,
    BS_ICON_CHAT_SQUARE_TEXT,
    BS_ICON_CHAT_SQUARE_TEXT_FILL,
    BS_ICON_CHAT_TEXT,
    BS_ICON_CHAT_TEXT_FILL,
    BS_ICON_CHECK,
    BS_ICON_CHECK_ALL,
    BS_ICON_CHECK_CIRCLE,
    BS_ICON_CHECK_CIRCLE_FILL,
    BS_ICON_CHECK_LG,
    BS_ICON_CHECK_SQUARE,
    BS_ICON_CHECK_SQUARE_FILL,
    BS_ICON_CHECK2,
    BS_ICON_CHECK2_ALL,
    BS_ICON_CHECK2_CIRCLE,
    BS_ICON_CHECK2_SQUARE,
    BS_ICON_CHEVRON_BAR_CONTRACT,
    BS_ICON_CHEVRON_BAR_DOWN,
    BS_ICON_CHEVRON_BAR_EXPAND,
    BS_ICON_CHEVRON_BAR_LEFT,
    BS_ICON_CHEVRON_BAR_RIGHT,
    BS_ICON_CHEVRON_BAR_UP,
    BS_ICON_CHEVRON_COMPACT_DOWN,
    BS_ICON_CHEVRON_COMPACT_LEFT,
    BS_ICON_CHEVRON_COMPACT_RIGHT,
    BS_ICON_CHEVRON_COMPACT_UP,
    BS_ICON_CHEVRON_CONTRACT,
    BS_ICON_CHEVRON_DOUBLE_DOWN,
    BS_ICON_CHEVRON_DOUBLE_LEFT,
    BS_ICON_CHEVRON_DOUBLE_RIGHT,
    BS_ICON_CHEVRON_DOUBLE_UP,
    BS_ICON_CHEVRON_DOWN,
    BS_ICON_CHEVRON_EXPAND,
    BS_ICON_CHEVRON_LEFT,
    BS_ICON_CHEVRON_RIGHT,
    BS_ICON_CHEVRON_UP,
    BS_ICON_CIRCLE,
    BS_ICON_CIRCLE_FILL,
    BS_ICON_CIRCLE_HALF,
    BS_ICON_CIRCLE_SQUARE,
    BS_ICON_CLIPBOARD,
    BS_ICON_CLIPBOARD_CHECK,
    BS_ICON_CLIPBOARD_CHECK_FILL,
    BS_ICON_CLIPBOARD_DATA,
    BS_ICON_CLIPBOARD_DATA_FILL,
    BS_ICON_CLIPBOARD_FILL,
    BS_ICON_CLIPBOARD_HEART,
    BS_ICON_CLIPBOARD_HEART_FILL,
    BS_ICON_CLIPBOARD_MINUS,
    BS_ICON_CLIPBOARD_MINUS_FILL,
    BS_ICON_CLIPBOARD_PLUS,
    BS_ICON_CLIPBOARD_PLUS_FILL,
    BS_ICON_CLIPBOARD_PULSE,
    BS_ICON_CLIPBOARD_X,
    BS_ICON_CLIPBOARD_X_FILL,
    BS_ICON_CLIPBOARD2,
    BS_ICON_CLIPBOARD2_CHECK,
    BS_ICON_CLIPBOARD2_CHECK_FILL,
    BS_ICON_CLIPBOARD2_DATA,
    BS_ICON_CLIPBOARD2_DATA_FILL,
    BS_ICON_CLIPBOARD2_FILL,
    BS_ICON_CLIPBOARD2_HEART,
    BS_ICON_CLIPBOARD2_HEART_FILL,
    BS_ICON_CLIPBOARD2_MINUS,
    BS_ICON_CLIPBOARD2_MINUS_FILL,
    BS_ICON_CLIPBOARD2_PLUS,
    BS_ICON_CLIPBOARD2_PLUS_FILL,
    BS_ICON_CLIPBOARD2_PULSE,
    BS_ICON_CLIPBOARD2_PULSE_FILL,
    BS_ICON_CLIPBOARD2_X,
    BS_ICON_CLIPBOARD2_X_FILL,
    BS_ICON_CLOCK,
    BS_ICON_CLOCK_FILL,
    BS_ICON_CLOCK_HISTORY,
    BS_ICON_CLOUD,
    BS_ICON_CLOUD_ARROW_DOWN,
    BS_ICON_CLOUD_ARROW_DOWN_FILL,
    BS_ICON_CLOUD_ARROW_UP,
    BS_ICON_CLOUD_ARROW_UP_FILL,
    BS_ICON_CLOUD_CHECK,
    BS_ICON_CLOUD_CHECK_FILL,
    BS_ICON_CLOUD_DOWNLOAD,
    BS_ICON_CLOUD_DOWNLOAD_FILL,
    BS_ICON_CLOUD_DRIZZLE,
    BS_ICON_CLOUD_DRIZZLE_FILL,
    BS_ICON_CLOUD_FILL,
    BS_ICON_CLOUD_FOG,
    BS_ICON_CLOUD_FOG_FILL,
    BS_ICON_CLOUD_FOG2,
    BS_ICON_CLOUD_FOG2_FILL,
    BS_ICON_CLOUD_HAIL,
    BS_ICON_CLOUD_HAIL_FILL,
    BS_ICON_CLOUD_HAZE,
    BS_ICON_CLOUD_HAZE_FILL,
    BS_ICON_CLOUD_HAZE2,
    BS_ICON_CLOUD_HAZE2_FILL,
    BS_ICON_CLOUD_LIGHTNING,
    BS_ICON_CLOUD_LIGHTNING_FILL,
    BS_ICON_CLOUD_LIGHTNING_RAIN,
    BS_ICON_CLOUD_LIGHTNING_RAIN_FILL,
    BS_ICON_CLOUD_MINUS,
    BS_ICON_CLOUD_MINUS_FILL,
    BS_ICON_CLOUD_MOON,
    BS_ICON_CLOUD_MOON_FILL,
    BS_ICON_CLOUD_PLUS,
    BS_ICON_CLOUD_PLUS_FILL,
    BS_ICON_CLOUD_RAIN,
    BS_ICON_CLOUD_RAIN_FILL,
    BS_ICON_CLOUD_RAIN_HEAVY,
    BS_ICON_CLOUD_RAIN_HEAVY_FILL,
    BS_ICON_CLOUD_SLASH,
    BS_ICON_CLOUD_SLASH_FILL,
    BS_ICON_CLOUD_SLEET,
    BS_ICON_CLOUD_SLEET_FILL,
    BS_ICON_CLOUD_SNOW,
    BS_ICON_CLOUD_SNOW_FILL,
    BS_ICON_CLOUD_SUN,
    BS_ICON_CLOUD_SUN_FILL,
    BS_ICON_CLOUD_UPLOAD,
    BS_ICON_CLOUD_UPLOAD_FILL,
    BS_ICON_CLOUDS,
    BS_ICON_CLOUDS_FILL,
    BS_ICON_CLOUDY,
    BS_ICON_CLOUDY_FILL,
    BS_ICON_CODE,
    BS_ICON_CODE_SLASH,
    BS_ICON_CODE_SQUARE,
    BS_ICON_COIN,
    BS_ICON_COLLECTION,
    BS_ICON_COLLECTION_FILL,
    BS_ICON_COLLECTION_PLAY,
    BS_ICON_COLLECTION_PLAY_FILL,
    BS_ICON_COLUMNS,
    BS_ICON_COLUMNS_GAP,
    BS_ICON_COMMAND,
    BS_ICON_COMPASS,
    BS_ICON_COMPASS_FILL,
    BS_ICON_CONE,
    BS_ICON_CONE_STRIPED,
    BS_ICON_CONTROLLER,
    BS_ICON_COOKIE,
    BS_ICON_COPY,
    BS_ICON_CPU,
    BS_ICON_CPU_FILL,
    BS_ICON_CREDIT_CARD,
    BS_ICON_CREDIT_CARD_2_BACK,
    BS_ICON_CREDIT_CARD_2_BACK_FILL,
    BS_ICON_CREDIT_CARD_2_FRONT,
    BS_ICON_CREDIT_CARD_2_FRONT_FILL,
    BS_ICON_CREDIT_CARD_FILL,
    BS_ICON_CROP,
    BS_ICON_CROSSHAIR,
    BS_ICON_CROSSHAIR2,
    BS_ICON_CUP,
    BS_ICON_CUP_FILL,
    BS_ICON_CUP_HOT,
    BS_ICON_CUP_HOT_FILL,
    BS_ICON_CUP_STRAW,
    BS_ICON_CURRENCY_BITCOIN,
    BS_ICON_CURRENCY_DOLLAR,
    BS_ICON_CURRENCY_EURO,
    BS_ICON_CURRENCY_EXCHANGE,
    BS_ICON_CURRENCY_POUND,
    BS_ICON_CURRENCY_RUPEE,
    BS_ICON_CURRENCY_YEN,
    BS_ICON_CURSOR,
    BS_ICON_CURSOR_FILL,
    BS_ICON_CURSOR_TEXT,
    BS_ICON_DASH,
    BS_ICON_DASH_CIRCLE,
    BS_ICON_DASH_CIRCLE_DOTTED,
    BS_ICON_DASH_CIRCLE_FILL,
    BS_ICON_DASH_LG,
    BS_ICON_DASH_SQUARE,
    BS_ICON_DASH_SQUARE_DOTTED,
    BS_ICON_DASH_SQUARE_FILL,
    BS_ICON_DATABASE,
    BS_ICON_DATABASE_ADD,
    BS_ICON_DATABASE_CHECK,
    BS_ICON_DATABASE_DASH,
    BS_ICON_DATABASE_DOWN,
    BS_ICON_DATABASE_EXCLAMATION,
    BS_ICON_DATABASE_FILL,
    BS_ICON_DATABASE_FILL_ADD,
    BS_ICON_DATABASE_FILL_CHECK,
    BS_ICON_DATABASE_FILL_DASH,
    BS_ICON_DATABASE_FILL_DOWN,
    BS_ICON_DATABASE_FILL_EXCLAMATION,
    BS_ICON_DATABASE_FILL_GEAR,
    BS_ICON_DATABASE_FILL_LOCK,
    BS_ICON_DATABASE_FILL_SLASH,
    BS_ICON_DATABASE_FILL_UP,
    BS_ICON_DATABASE_FILL_X,
    BS_ICON_DATABASE_GEAR,
    BS_ICON_DATABASE_LOCK,
    BS_ICON_DATABASE_SLASH,
    BS_ICON_DATABASE_UP,
    BS_ICON_DATABASE_X,
    BS_ICON_DEVICE_HDD,
    BS_ICON_DEVICE_HDD_FILL,
    BS_ICON_DEVICE_SSD,
    BS_ICON_DEVICE_SSD_FILL,
    BS_ICON_DIAGRAM_2,
    BS_ICON_DIAGRAM_2_FILL,
    BS_ICON_DIAGRAM_3,
    BS_ICON_DIAGRAM_3_FILL,
    BS_ICON_DIAMOND,
    BS_ICON_DIAMOND_FILL,
    BS_ICON_DIAMOND_HALF,
    BS_ICON_DICE_1,
    BS_ICON_DICE_1_FILL,
    BS_ICON_DICE_2,
    BS_ICON_DICE_2_FILL,
    BS_ICON_DICE_3,
    BS_ICON_DICE_3_FILL,
    BS_ICON_DICE_4,
    BS_ICON_DICE_4_FILL,
    BS_ICON_DICE_5,
    BS_ICON_DICE_5_FILL,
    BS_ICON_DICE_6,
    BS_ICON_DICE_6_FILL,
    BS_ICON_DISC,
    BS_ICON_DISC_FILL,
    BS_ICON_DISCORD,
    BS_ICON_DISPLAY,
    BS_ICON_DISPLAY_FILL,
    BS_ICON_DISPLAYPORT,
    BS_ICON_DISPLAYPORT_FILL,
    BS_ICON_DISTRIBUTE_HORIZONTAL,
    BS_ICON_DISTRIBUTE_VERTICAL,
    BS_ICON_DOOR_CLOSED,
    BS_ICON_DOOR_CLOSED_FILL,
    BS_ICON_DOOR_OPEN,
    BS_ICON_DOOR_OPEN_FILL,
    BS_ICON_DOT,
    BS_ICON_DOWNLOAD,
    BS_ICON_DPAD,
    BS_ICON_DPAD_FILL,
    BS_ICON_DRIBBBLE,
    BS_ICON_DROPBOX,
    BS_ICON_DROPLET,
    BS_ICON_DROPLET_FILL,
    BS_ICON_DROPLET_HALF,
    BS_ICON_DUFFLE,
    BS_ICON_DUFFLE_FILL,
    BS_ICON_EAR,
    BS_ICON_EAR_FILL,
    BS_ICON_EARBUDS,
    BS_ICON_EASEL,
    BS_ICON_EASEL_FILL,
    BS_ICON_EASEL2,
    BS_ICON_EASEL2_FILL,
    BS_ICON_EASEL3,
    BS_ICON_EASEL3_FILL,
    BS_ICON_EGG,
    BS_ICON_EGG_FILL,
    BS_ICON_EGG_FRIED,
    BS_ICON_EJECT,
    BS_ICON_EJECT_FILL,
    BS_ICON_EMOJI_ANGRY,
    BS_ICON_EMOJI_ANGRY_FILL,
    BS_ICON_EMOJI_ASTONISHED,
    BS_ICON_EMOJI_ASTONISHED_FILL,
    BS_ICON_EMOJI_DIZZY,
    BS_ICON_EMOJI_DIZZY_FILL,
    BS_ICON_EMOJI_EXPRESSIONLESS,
    BS_ICON_EMOJI_EXPRESSIONLESS_FILL,
    BS_ICON_EMOJI_FROWN,
    BS_ICON_EMOJI_FROWN_FILL,
    BS_ICON_EMOJI_GRIMACE,
    BS_ICON_EMOJI_GRIMACE_FILL,
    BS_ICON_EMOJI_GRIN,
    BS_ICON_EMOJI_GRIN_FILL,
    BS_ICON_EMOJI_HEART_EYES,
    BS_ICON_EMOJI_HEART_EYES_FILL,
    BS_ICON_EMOJI_KISS,
    BS_ICON_EMOJI_KISS_FILL,
    BS_ICON_EMOJI_LAUGHING,
    BS_ICON_EMOJI_LAUGHING_FILL,
    BS_ICON_EMOJI_NEUTRAL,
    BS_ICON_EMOJI_NEUTRAL_FILL,
    BS_ICON_EMOJI_SMILE,
    BS_ICON_EMOJI_SMILE_FILL,
    BS_ICON_EMOJI_SMILE_UPSIDE_DOWN,
    BS_ICON_EMOJI_SMILE_UPSIDE_DOWN_FILL,
    BS_ICON_EMOJI_SUNGLASSES,
    BS_ICON_EMOJI_SUNGLASSES_FILL,
    BS_ICON_EMOJI_SURPRISE,
    BS_ICON_EMOJI_SURPRISE_FILL,
    BS_ICON_EMOJI_TEAR,
    BS_ICON_EMOJI_TEAR_FILL,
    BS_ICON_EMOJI_WINK,
    BS_ICON_EMOJI_WINK_FILL,
    BS_ICON_ENVELOPE,
    BS_ICON_ENVELOPE_ARROW_DOWN,
    BS_ICON_ENVELOPE_ARROW_DOWN_FILL,
    BS_ICON_ENVELOPE_ARROW_UP,
    BS_ICON_ENVELOPE_ARROW_UP_FILL,
    BS_ICON_ENVELOPE_AT,
    BS_ICON_ENVELOPE_AT_FILL,
    BS_ICON_ENVELOPE_CHECK,
    BS_ICON_ENVELOPE_CHECK_FILL,
    BS_ICON_ENVELOPE_DASH,
    BS_ICON_ENVELOPE_DASH_FILL,
    BS_ICON_ENVELOPE_EXCLAMATION,
    BS_ICON_ENVELOPE_EXCLAMATION_FILL,
    BS_ICON_ENVELOPE_FILL,
    BS_ICON_ENVELOPE_HEART,
    BS_ICON_ENVELOPE_HEART_FILL,
    BS_ICON_ENVELOPE_OPEN,
    BS_ICON_ENVELOPE_OPEN_FILL,
    BS_ICON_ENVELOPE_OPEN_HEART,
    BS_ICON_ENVELOPE_OPEN_HEART_FILL,
    BS_ICON_ENVELOPE_PAPER,
    BS_ICON_ENVELOPE_PAPER_FILL,
    BS_ICON_ENVELOPE_PAPER_HEART,
    BS_ICON_ENVELOPE_PAPER_HEART_FILL,
    BS_ICON_ENVELOPE_PLUS,
    BS_ICON_ENVELOPE_PLUS_FILL,
    BS_ICON_ENVELOPE_SLASH,
    BS_ICON_ENVELOPE_SLASH_FILL,
    BS_ICON_ENVELOPE_X,
    BS_ICON_ENVELOPE_X_FILL,
    BS_ICON_ERASER,
    BS_ICON_ERASER_FILL,
    BS_ICON_ESCAPE,
    BS_ICON_ETHERNET,
    BS_ICON_EV_FRONT,
    BS_ICON_EV_FRONT_FILL,
    BS_ICON_EV_STATION,
    BS_ICON_EV_STATION_FILL,
    BS_ICON_EXCLAMATION,
    BS_ICON_EXCLAMATION_CIRCLE,
    BS_ICON_EXCLAMATION_CIRCLE_FILL,
    BS_ICON_EXCLAMATION_DIAMOND,
    BS_ICON_EXCLAMATION_DIAMOND_FILL,
    BS_ICON_EXCLAMATION_LG,
    BS_ICON_EXCLAMATION_OCTAGON,
    BS_ICON_EXCLAMATION_OCTAGON_FILL,
    BS_ICON_EXCLAMATION_SQUARE,
    BS_ICON_EXCLAMATION_SQUARE_FILL,
    BS_ICON_EXCLAMATION_TRIANGLE,
    BS_ICON_EXCLAMATION_TRIANGLE_FILL,
    BS_ICON_EXCLUDE,
    BS_ICON_EXPLICIT,
    BS_ICON_EXPLICIT_FILL,
    BS_ICON_EXPOSURE,
    BS_ICON_EYE,
    BS_ICON_EYE_FILL,
    BS_ICON_EYE_SLASH,
    BS_ICON_EYE_SLASH_FILL,
    BS_ICON_EYEDROPPER,
    BS_ICON_EYEGLASSES,
    BS_ICON_FACEBOOK,
    BS_ICON_FAN,
    BS_ICON_FAST_FORWARD,
    BS_ICON_FAST_FORWARD_BTN,
    BS_ICON_FAST_FORWARD_BTN_FILL,
    BS_ICON_FAST_FORWARD_CIRCLE,
    BS_ICON_FAST_FORWARD_CIRCLE_FILL,
    BS_ICON_FAST_FORWARD_FILL,
    BS_ICON_FEATHER,
    BS_ICON_FEATHER2,
    BS_ICON_FILE,
    BS_ICON_FILE_ARROW_DOWN,
    BS_ICON_FILE_ARROW_DOWN_FILL,
    BS_ICON_FILE_ARROW_UP,
    BS_ICON_FILE_ARROW_UP_FILL,
    BS_ICON_FILE_BAR_GRAPH,
    BS_ICON_FILE_BAR_GRAPH_FILL,
    BS_ICON_FILE_BINARY,
    BS_ICON_FILE_BINARY_FILL,
    BS_ICON_FILE_BREAK,
    BS_ICON_FILE_BREAK_FILL,
    BS_ICON_FILE_CHECK,
    BS_ICON_FILE_CHECK_FILL,
    BS_ICON_FILE_CODE,
    BS_ICON_FILE_CODE_FILL,
    BS_ICON_FILE_DIFF,
    BS_ICON_FILE_DIFF_FILL,
    BS_ICON_FILE_EARMARK,
    BS_ICON_FILE_EARMARK_ARROW_DOWN,
    BS_ICON_FILE_EARMARK_ARROW_DOWN_FILL,
    BS_ICON_FILE_EARMARK_ARROW_UP,
    BS_ICON_FILE_EARMARK_ARROW_UP_FILL,
    BS_ICON_FILE_EARMARK_BAR_GRAPH,
    BS_ICON_FILE_EARMARK_BAR_GRAPH_FILL,
    BS_ICON_FILE_EARMARK_BINARY,
    BS_ICON_FILE_EARMARK_BINARY_FILL,
    BS_ICON_FILE_EARMARK_BREAK,
    BS_ICON_FILE_EARMARK_BREAK_FILL,
    BS_ICON_FILE_EARMARK_CHECK,
    BS_ICON_FILE_EARMARK_CHECK_FILL,
    BS_ICON_FILE_EARMARK_CODE,
    BS_ICON_FILE_EARMARK_CODE_FILL,
    BS_ICON_FILE_EARMARK_DIFF,
    BS_ICON_FILE_EARMARK_DIFF_FILL,
    BS_ICON_FILE_EARMARK_EASEL,
    BS_ICON_FILE_EARMARK_EASEL_FILL,
    BS_ICON_FILE_EARMARK_EXCEL,
    BS_ICON_FILE_EARMARK_EXCEL_FILL,
    BS_ICON_FILE_EARMARK_FILL,
    BS_ICON_FILE_EARMARK_FONT,
    BS_ICON_FILE_EARMARK_FONT_FILL,
    BS_ICON_FILE_EARMARK_IMAGE,
    BS_ICON_FILE_EARMARK_IMAGE_FILL,
    BS_ICON_FILE_EARMARK_LOCK,
    BS_ICON_FILE_EARMARK_LOCK_FILL,
    BS_ICON_FILE_EARMARK_LOCK2,
    BS_ICON_FILE_EARMARK_LOCK2_FILL,
    BS_ICON_FILE_EARMARK_MEDICAL,
    BS_ICON_FILE_EARMARK_MEDICAL_FILL,
    BS_ICON_FILE_EARMARK_MINUS,
    BS_ICON_FILE_EARMARK_MINUS_FILL,
    BS_ICON_FILE_EARMARK_MUSIC,
    BS_ICON_FILE_EARMARK_MUSIC_FILL,
    BS_ICON_FILE_EARMARK_PDF,
    BS_ICON_FILE_EARMARK_PDF_FILL,
    BS_ICON_FILE_EARMARK_PERSON,
    BS_ICON_FILE_EARMARK_PERSON_FILL,
    BS_ICON_FILE_EARMARK_PLAY,
    BS_ICON_FILE_EARMARK_PLAY_FILL,
    BS_ICON_FILE_EARMARK_PLUS,
    BS_ICON_FILE_EARMARK_PLUS_FILL,
    BS_ICON_FILE_EARMARK_POST,
    BS_ICON_FILE_EARMARK_POST_FILL,
    BS_ICON_FILE_EARMARK_PPT,
    BS_ICON_FILE_EARMARK_PPT_FILL,
    BS_ICON_FILE_EARMARK_RICHTEXT,
    BS_ICON_FILE_EARMARK_RICHTEXT_FILL,
    BS_ICON_FILE_EARMARK_RULED,
    BS_ICON_FILE_EARMARK_RULED_FILL,
    BS_ICON_FILE_EARMARK_SLIDES,
    BS_ICON_FILE_EARMARK_SLIDES_FILL,
    BS_ICON_FILE_EARMARK_SPREADSHEET,
    BS_ICON_FILE_EARMARK_SPREADSHEET_FILL,
    BS_ICON_FILE_EARMARK_TEXT,
    BS_ICON_FILE_EARMARK_TEXT_FILL,
    BS_ICON_FILE_EARMARK_WORD,
    BS_ICON_FILE_EARMARK_WORD_FILL,
    BS_ICON_FILE_EARMARK_X,
    BS_ICON_FILE_EARMARK_X_FILL,
    BS_ICON_FILE_EARMARK_ZIP,
    BS_ICON_FILE_EARMARK_ZIP_FILL,
    BS_ICON_FILE_EASEL,
    BS_ICON_FILE_EASEL_FILL,
    BS_ICON_FILE_EXCEL,
    BS_ICON_FILE_EXCEL_FILL,
    BS_ICON_FILE_FILL,
    BS_ICON_FILE_FONT,
    BS_ICON_FILE_FONT_FILL,
    BS_ICON_FILE_IMAGE,
    BS_ICON_FILE_IMAGE_FILL,
    BS_ICON_FILE_LOCK,
    BS_ICON_FILE_LOCK_FILL,
    BS_ICON_FILE_LOCK2,
    BS_ICON_FILE_LOCK2_FILL,
    BS_ICON_FILE_MEDICAL,
    BS_ICON_FILE_MEDICAL_FILL,
    BS_ICON_FILE_MINUS,
    BS_ICON_FILE_MINUS_FILL,
    BS_ICON_FILE_MUSIC,
    BS_ICON_FILE_MUSIC_FILL,
    BS_ICON_FILE_PDF,
    BS_ICON_FILE_PDF_FILL,
    BS_ICON_FILE_PERSON,
    BS_ICON_FILE_PERSON_FILL,
    BS_ICON_FILE_PLAY,
    BS_ICON_FILE_PLAY_FILL,
    BS_ICON_FILE_PLUS,
    BS_ICON_FILE_PLUS_FILL,
    BS_ICON_FILE_POST,
    BS_ICON_FILE_POST_FILL,
    BS_ICON_FILE_PPT,
    BS_ICON_FILE_PPT_FILL,
    BS_ICON_FILE_RICHTEXT,
    BS_ICON_FILE_RICHTEXT_FILL,
    BS_ICON_FILE_RULED,
    BS_ICON_FILE_RULED_FILL,
    BS_ICON_FILE_SLIDES,
    BS_ICON_FILE_SLIDES_FILL,
    BS_ICON_FILE_SPREADSHEET,
    BS_ICON_FILE_SPREADSHEET_FILL,
    BS_ICON_FILE_TEXT,
    BS_ICON_FILE_TEXT_FILL,
    BS_ICON_FILE_WORD,
    BS_ICON_FILE_WORD_FILL,
    BS_ICON_FILE_X,
    BS_ICON_FILE_X_FILL,
    BS_ICON_FILE_ZIP,
    BS_ICON_FILE_ZIP_FILL,
    BS_ICON_FILES,
    BS_ICON_FILES_ALT,
    BS_ICON_FILETYPE_AAC,
    BS_ICON_FILETYPE_AI,
    BS_ICON_FILETYPE_BMP,
    BS_ICON_FILETYPE_CS,
    BS_ICON_FILETYPE_CSS,
    BS_ICON_FILETYPE_CSV,
    BS_ICON_FILETYPE_DOC,
    BS_ICON_FILETYPE_DOCX,
    BS_ICON_FILETYPE_EXE,
    BS_ICON_FILETYPE_GIF,
    BS_ICON_FILETYPE_HEIC,
    BS_ICON_FILETYPE_HTML,
    BS_ICON_FILETYPE_JAVA,
    BS_ICON_FILETYPE_JPG,
    BS_ICON_FILETYPE_JS,
    BS_ICON_FILETYPE_JSON,
    BS_ICON_FILETYPE_JSX,
    BS_ICON_FILETYPE_KEY,
    BS_ICON_FILETYPE_M4P,
    BS_ICON_FILETYPE_MD,
    BS_ICON_FILETYPE_MDX,
    BS_ICON_FILETYPE_MOV,
    BS_ICON_FILETYPE_MP3,
    BS_ICON_FILETYPE_MP4,
    BS_ICON_FILETYPE_OTF,
    BS_ICON_FILETYPE_PDF,
    BS_ICON_FILETYPE_PHP,
    BS_ICON_FILETYPE_PNG,
    BS_ICON_FILETYPE_PPT,
    BS_ICON_FILETYPE_PPTX,
    BS_ICON_FILETYPE_PSD,
    BS_ICON_FILETYPE_PY,
    BS_ICON_FILETYPE_RAW,
    BS_ICON_FILETYPE_RB,
    BS_ICON_FILETYPE_SASS,
    BS_ICON_FILETYPE_SCSS,
    BS_ICON_FILETYPE_SH,
    BS_ICON_FILETYPE_SQL,
    BS_ICON_FILETYPE_SVG,
    BS_ICON_FILETYPE_TIFF,
    BS_ICON_FILETYPE_TSX,
    BS_ICON_FILETYPE_TTF,
    BS_ICON_FILETYPE_TXT,
    BS_ICON_FILETYPE_WAV,
    BS_ICON_FILETYPE_WOFF,
    BS_ICON_FILETYPE_XLS,
    BS_ICON_FILETYPE_XLSX,
    BS_ICON_FILETYPE_XML,
    BS_ICON_FILETYPE_YML,
    BS_ICON_FILM,
    BS_ICON_FILTER,
    BS_ICON_FILTER_CIRCLE,
    BS_ICON_FILTER_CIRCLE_FILL,
    BS_ICON_FILTER_LEFT,
    BS_ICON_FILTER_RIGHT,
    BS_ICON_FILTER_SQUARE,
    BS_ICON_FILTER_SQUARE_FILL,
    BS_ICON_FINGERPRINT,
    BS_ICON_FIRE,
    BS_ICON_FLAG,
    BS_ICON_FLAG_FILL,
    BS_ICON_FLOPPY,
    BS_ICON_FLOPPY_FILL,
    BS_ICON_FLOPPY2,
    BS_ICON_FLOPPY2_FILL,
    BS_ICON_FLOWER1,
    BS_ICON_FLOWER2,
    BS_ICON_FLOWER3,
    BS_ICON_FOLDER,
    BS_ICON_FOLDER_CHECK,
    BS_ICON_FOLDER_FILL,
    BS_ICON_FOLDER_MINUS,
    BS_ICON_FOLDER_PLUS,
    BS_ICON_FOLDER_SYMLINK,
    BS_ICON_FOLDER_SYMLINK_FILL,
    BS_ICON_FOLDER_X,
    BS_ICON_FOLDER2,
    BS_ICON_FOLDER2_OPEN,
    BS_ICON_FONTS,
    BS_ICON_FORWARD,
    BS_ICON_FORWARD_FILL,
    BS_ICON_FRONT,
    BS_ICON_FUEL_PUMP,
    BS_ICON_FUEL_PUMP_DIESEL,
    BS_ICON_FUEL_PUMP_DIESEL_FILL,
    BS_ICON_FUEL_PUMP_FILL,
    BS_ICON_FULLSCREEN,
    BS_ICON_FULLSCREEN_EXIT,
    BS_ICON_FUNNEL,
    BS_ICON_FUNNEL_FILL,
    BS_ICON_GEAR,
    BS_ICON_GEAR_FILL,
    BS_ICON_GEAR_WIDE,
    BS_ICON_GEAR_WIDE_CONNECTED,
    BS_ICON_GEM,
    BS_ICON_GENDER_AMBIGUOUS,
    BS_ICON_GENDER_FEMALE,
    BS_ICON_GENDER_MALE,
    BS_ICON_GENDER_NEUTER,
    BS_ICON_GENDER_TRANS,
    BS_ICON_GEO,
    BS_ICON_GEO_ALT,
    BS_ICON_GEO_ALT_FILL,
    BS_ICON_GEO_FILL,
    BS_ICON_GIFT,
    BS_ICON_GIFT_FILL,
    BS_ICON_GIT,
    BS_ICON_GITHUB,
    BS_ICON_GITLAB,
    BS_ICON_GLOBE,
    BS_ICON_GLOBE_AMERICAS,
    BS_ICON_GLOBE_ASIA_AUSTRALIA,
    BS_ICON_GLOBE_CENTRAL_SOUTH_ASIA,
    BS_ICON_GLOBE_EUROPE_AFRICA,
    BS_ICON_GLOBE2,
    BS_ICON_GOOGLE,
    BS_ICON_GOOGLE_PLAY,
    BS_ICON_GPU_CARD,
    BS_ICON_GRAPH_DOWN,
    BS_ICON_GRAPH_DOWN_ARROW,
    BS_ICON_GRAPH_UP,
    BS_ICON_GRAPH_UP_ARROW,
    BS_ICON_GRID,
    BS_ICON_GRID_1X2,
    BS_ICON_GRID_1X2_FILL,
    BS_ICON_GRID_3X2,
    BS_ICON_GRID_3X2_GAP,
    BS_ICON_GRID_3X2_GAP_FILL,
    BS_ICON_GRID_3X3,
    BS_ICON_GRID_3X3_GAP,
    BS_ICON_GRID_3X3_GAP_FILL,
    BS_ICON_GRID_FILL,
    BS_ICON_GRIP_HORIZONTAL,
    BS_ICON_GRIP_VERTICAL,
    BS_ICON_H_CIRCLE,
    BS_ICON_H_CIRCLE_FILL,
    BS_ICON_H_SQUARE,
    BS_ICON_H_SQUARE_FILL,
    BS_ICON_HAMMER,
    BS_ICON_HAND_INDEX,
    BS_ICON_HAND_INDEX_FILL,
    BS_ICON_HAND_INDEX_THUMB,
    BS_ICON_HAND_INDEX_THUMB_FILL,
    BS_ICON_HAND_THUMBS_DOWN,
    BS_ICON_HAND_THUMBS_DOWN_FILL,
    BS_ICON_HAND_THUMBS_UP,
    BS_ICON_HAND_THUMBS_UP_FILL,
    BS_ICON_HANDBAG,
    BS_ICON_HANDBAG_FILL,
    BS_ICON_HASH,
    BS_ICON_HDD,
    BS_ICON_HDD_FILL,
    BS_ICON_HDD_NETWORK,
    BS_ICON_HDD_NETWORK_FILL,
    BS_ICON_HDD_RACK,
    BS_ICON_HDD_RACK_FILL,
    BS_ICON_HDD_STACK,
    BS_ICON_HDD_STACK_FILL,
    BS_ICON_HDMI,
    BS_ICON_HDMI_FILL,
    BS_ICON_HEADPHONES,
    BS_ICON_HEADSET,
    BS_ICON_HEADSET_VR,
    BS_ICON_HEART,
    BS_ICON_HEART_ARROW,
    BS_ICON_HEART_FILL,
    BS_ICON_HEART_HALF,
    BS_ICON_HEART_PULSE,
    BS_ICON_HEART_PULSE_FILL,
    BS_ICON_HEARTBREAK,
    BS_ICON_HEARTBREAK_FILL,
    BS_ICON_HEARTS,
    BS_ICON_HEPTAGON,
    BS_ICON_HEPTAGON_FILL,
    BS_ICON_HEPTAGON_HALF,
    BS_ICON_HEXAGON,
    BS_ICON_HEXAGON_FILL,
    BS_ICON_HEXAGON_HALF,
    BS_ICON_HIGHLIGHTER,
    BS_ICON_HIGHLIGHTS,
    BS_ICON_HOSPITAL,
    BS_ICON_HOSPITAL_FILL,
    BS_ICON_HOURGLASS,
    BS_ICON_HOURGLASS_BOTTOM,
    BS_ICON_HOURGLASS_SPLIT,
    BS_ICON_HOURGLASS_TOP,
    BS_ICON_HOUSE,
    BS_ICON_HOUSE_ADD,
    BS_ICON_HOUSE_ADD_FILL,
    BS_ICON_HOUSE_CHECK,
    BS_ICON_HOUSE_CHECK_FILL,
    BS_ICON_HOUSE_DASH,
    BS_ICON_HOUSE_DASH_FILL,
    BS_ICON_HOUSE_DOOR,
    BS_ICON_HOUSE_DOOR_FILL,
    BS_ICON_HOUSE_DOWN,
    BS_ICON_HOUSE_DOWN_FILL,
    BS_ICON_HOUSE_EXCLAMATION,
    BS_ICON_HOUSE_EXCLAMATION_FILL,
    BS_ICON_HOUSE_FILL,
    BS_ICON_HOUSE_GEAR,
    BS_ICON_HOUSE_GEAR_FILL,
    BS_ICON_HOUSE_HEART,
    BS_ICON_HOUSE_HEART_FILL,
    BS_ICON_HOUSE_LOCK,
    BS_ICON_HOUSE_LOCK_FILL,
    BS_ICON_HOUSE_SLASH,
    BS_ICON_HOUSE_SLASH_FILL,
    BS_ICON_HOUSE_UP,
    BS_ICON_HOUSE_UP_FILL,
    BS_ICON_HOUSE_X,
    BS_ICON_HOUSE_X_FILL,
    BS_ICON_HOUSES,
    BS_ICON_HOUSES_FILL,
    BS_ICON_HR,
    BS_ICON_HURRICANE,
    BS_ICON_HYPNOTIZE,
    BS_ICON_IMAGE,
    BS_ICON_IMAGE_ALT,
    BS_ICON_IMAGE_FILL,
    BS_ICON_IMAGES,
    BS_ICON_INBOX,
    BS_ICON_INBOX_FILL,
    BS_ICON_INBOXES,
    BS_ICON_INBOXES_FILL,
    BS_ICON_INCOGNITO,
    BS_ICON_INDENT,
    BS_ICON_INFINITY,
    BS_ICON_INFO,
    BS_ICON_INFO_CIRCLE,
    BS_ICON_INFO_CIRCLE_FILL,
    BS_ICON_INFO_LG,
    BS_ICON_INFO_SQUARE,
    BS_ICON_INFO_SQUARE_FILL,
    BS_ICON_INPUT_CURSOR,
    BS_ICON_INPUT_CURSOR_TEXT,
    BS_ICON_INSTAGRAM,
    BS_ICON_INTERSECT,
    BS_ICON_JOURNAL,
    BS_ICON_JOURNAL_ALBUM,
    BS_ICON_JOURNAL_ARROW_DOWN,
    BS_ICON_JOURNAL_ARROW_UP,
    BS_ICON_JOURNAL_BOOKMARK,
    BS_ICON_JOURNAL_BOOKMARK_FILL,
    BS_ICON_JOURNAL_CHECK,
    BS_ICON_JOURNAL_CODE,
    BS_ICON_JOURNAL_MEDICAL,
    BS_ICON_JOURNAL_MINUS,
    BS_ICON_JOURNAL_PLUS,
    BS_ICON_JOURNAL_RICHTEXT,
    BS_ICON_JOURNAL_TEXT,
    BS_ICON_JOURNAL_X,
    BS_ICON_JOURNALS,
    BS_ICON_JOYSTICK,
    BS_ICON_JUSTIFY,
    BS_ICON_JUSTIFY_LEFT,
    BS_ICON_JUSTIFY_RIGHT,
    BS_ICON_KANBAN,
    BS_ICON_KANBAN_FILL,
    BS_ICON_KEY,
    BS_ICON_KEY_FILL,
    BS_ICON_KEYBOARD,
    BS_ICON_KEYBOARD_FILL,
    BS_ICON_LADDER,
    BS_ICON_LAMP,
    BS_ICON_LAMP_FILL,
    BS_ICON_LAPTOP,
    BS_ICON_LAPTOP_FILL,
    BS_ICON_LAYER_BACKWARD,
    BS_ICON_LAYER_FORWARD,
    BS_ICON_LAYERS,
    BS_ICON_LAYERS_FILL,
    BS_ICON_LAYERS_HALF,
    BS_ICON_LAYOUT_SIDEBAR,
    BS_ICON_LAYOUT_SIDEBAR_INSET,
    BS_ICON_LAYOUT_SIDEBAR_INSET_REVERSE,
    BS_ICON_LAYOUT_SIDEBAR_REVERSE,
    BS_ICON_LAYOUT_SPLIT,
    BS_ICON_LAYOUT_TEXT_SIDEBAR,
    BS_ICON_LAYOUT_TEXT_SIDEBAR_REVERSE,
    BS_ICON_LAYOUT_TEXT_WINDOW,
    BS_ICON_LAYOUT_TEXT_WINDOW_REVERSE,
    BS_ICON_LAYOUT_THREE_COLUMNS,
    BS_ICON_LAYOUT_WTF,
    BS_ICON_LIFE_PRESERVER,
    BS_ICON_LIGHTBULB,
    BS_ICON_LIGHTBULB_FILL,
    BS_ICON_LIGHTBULB_OFF,
    BS_ICON_LIGHTBULB_OFF_FILL,
    BS_ICON_LIGHTNING,
    BS_ICON_LIGHTNING_CHARGE,
    BS_ICON_LIGHTNING_CHARGE_FILL,
    BS_ICON_LIGHTNING_FILL,
    BS_ICON_LINE,
    BS_ICON_LINK,
    BS_ICON_LINK_45DEG,
    BS_ICON_LINKEDIN,
    BS_ICON_LIST,
    BS_ICON_LIST_CHECK,
    BS_ICON_LIST_COLUMNS,
    BS_ICON_LIST_COLUMNS_REVERSE,
    BS_ICON_LIST_NESTED,
    BS_ICON_LIST_OL,
    BS_ICON_LIST_STARS,
    BS_ICON_LIST_TASK,
    BS_ICON_LIST_UL,
    BS_ICON_LOCK,
    BS_ICON_LOCK_FILL,
    BS_ICON_LUGGAGE,
    BS_ICON_LUGGAGE_FILL,
    BS_ICON_LUNGS,
    BS_ICON_LUNGS_FILL,
    BS_ICON_MAGIC,
    BS_ICON_MAGNET,
    BS_ICON_MAGNET_FILL,
    BS_ICON_MAILBOX,
    BS_ICON_MAILBOX_FLAG,
    BS_ICON_MAILBOX2,
    BS_ICON_MAILBOX2_FLAG,
    BS_ICON_MAP,
    BS_ICON_MAP_FILL,
    BS_ICON_MARKDOWN,
    BS_ICON_MARKDOWN_FILL,
    BS_ICON_MARKER_TIP,
    BS_ICON_MASK,
    BS_ICON_MASTODON,
    BS_ICON_MEDIUM,
    BS_ICON_MEGAPHONE,
    BS_ICON_MEGAPHONE_FILL,
    BS_ICON_MEMORY,
    BS_ICON_MENU_APP,
    BS_ICON_MENU_APP_FILL,
    BS_ICON_MENU_BUTTON,
    BS_ICON_MENU_BUTTON_FILL,
    BS_ICON_MENU_BUTTON_WIDE,
    BS_ICON_MENU_BUTTON_WIDE_FILL,
    BS_ICON_MENU_DOWN,
    BS_ICON_MENU_UP,
    BS_ICON_MESSENGER,
    BS_ICON_META,
    BS_ICON_MIC,
    BS_ICON_MIC_FILL,
    BS_ICON_MIC_MUTE,
    BS_ICON_MIC_MUTE_FILL,
    BS_ICON_MICROSOFT,
    BS_ICON_MICROSOFT_TEAMS,
    BS_ICON_MINECART,
    BS_ICON_MINECART_LOADED,
    BS_ICON_MODEM,
    BS_ICON_MODEM_FILL,
    BS_ICON_MOISTURE,
    BS_ICON_MOON,
    BS_ICON_MOON_FILL,
    BS_ICON_MOON_STARS,
    BS_ICON_MOON_STARS_FILL,
    BS_ICON_MORTARBOARD,
    BS_ICON_MORTARBOARD_FILL,
    BS_ICON_MOTHERBOARD,
    BS_ICON_MOTHERBOARD_FILL,
    BS_ICON_MOUSE,
    BS_ICON_MOUSE_FILL,
    BS_ICON_MOUSE2,
    BS_ICON_MOUSE2_FILL,
    BS_ICON_MOUSE3,
    BS_ICON_MOUSE3_FILL,
    BS_ICON_MUSIC_NOTE,
    BS_ICON_MUSIC_NOTE_BEAMED,
    BS_ICON_MUSIC_NOTE_LIST,
    BS_ICON_MUSIC_PLAYER,
    BS_ICON_MUSIC_PLAYER_FILL,
    BS_ICON_NEWSPAPER,
    BS_ICON_NINTENDO_SWITCH,
    BS_ICON_NODE_MINUS,
    BS_ICON_NODE_MINUS_FILL,
    BS_ICON_NODE_PLUS,
    BS_ICON_NODE_PLUS_FILL,
    BS_ICON_NOISE_REDUCTION,
    BS_ICON_NUT,
    BS_ICON_NUT_FILL,
    BS_ICON_NVIDIA,
    BS_ICON_NVME,
    BS_ICON_NVME_FILL,
    BS_ICON_OCTAGON,
    BS_ICON_OCTAGON_FILL,
    BS_ICON_OCTAGON_HALF,
    BS_ICON_OPENCOLLECTIVE,
    BS_ICON_OPTICAL_AUDIO,
    BS_ICON_OPTICAL_AUDIO_FILL,
    BS_ICON_OPTION,
    BS_ICON_OUTLET,
    BS_ICON_P_CIRCLE,
    BS_ICON_P_CIRCLE_FILL,
    BS_ICON_P_SQUARE,
    BS_ICON_P_SQUARE_FILL,
    BS_ICON_PAINT_BUCKET,
    BS_ICON_PALETTE,
    BS_ICON_PALETTE_FILL,
    BS_ICON_PALETTE2,
    BS_ICON_PAPERCLIP,
    BS_ICON_PARAGRAPH,
    BS_ICON_PASS,
    BS_ICON_PASS_FILL,
    BS_ICON_PASSPORT,
    BS_ICON_PASSPORT_FILL,
    BS_ICON_PATCH_CHECK,
    BS_ICON_PATCH_CHECK_FILL,
    BS_ICON_PATCH_EXCLAMATION,
    BS_ICON_PATCH_EXCLAMATION_FILL,
    BS_ICON_PATCH_MINUS,
    BS_ICON_PATCH_MINUS_FILL,
    BS_ICON_PATCH_PLUS,
    BS_ICON_PATCH_PLUS_FILL,
    BS_ICON_PATCH_QUESTION,
    BS_ICON_PATCH_QUESTION_FILL,
    BS_ICON_PAUSE,
    BS_ICON_PAUSE_BTN,
    BS_ICON_PAUSE_BTN_FILL,
    BS_ICON_PAUSE_CIRCLE,
    BS_ICON_PAUSE_CIRCLE_FILL,
    BS_ICON_PAUSE_FILL,
    BS_ICON_PAYPAL,
    BS_ICON_PC,
    BS_ICON_PC_DISPLAY,
    BS_ICON_PC_DISPLAY_HORIZONTAL,
    BS_ICON_PC_HORIZONTAL,
    BS_ICON_PCI_CARD,
    BS_ICON_PCI_CARD_NETWORK,
    BS_ICON_PCI_CARD_SOUND,
    BS_ICON_PEACE,
    BS_ICON_PEACE_FILL,
    BS_ICON_PEN,
    BS_ICON_PEN_FILL,
    BS_ICON_PENCIL,
    BS_ICON_PENCIL_FILL,
    BS_ICON_PENCIL_SQUARE,
    BS_ICON_PENTAGON,
    BS_ICON_PENTAGON_FILL,
    BS_ICON_PENTAGON_HALF,
    BS_ICON_PEOPLE,
    BS_ICON_PEOPLE_FILL,
    BS_ICON_PERCENT,
    BS_ICON_PERSON,
    BS_ICON_PERSON_ADD,
    BS_ICON_PERSON_ARMS_UP,
    BS_ICON_PERSON_BADGE,
    BS_ICON_PERSON_BADGE_FILL,
    BS_ICON_PERSON_BOUNDING_BOX,
    BS_ICON_PERSON_CHECK,
    BS_ICON_PERSON_CHECK_FILL,
    BS_ICON_PERSON_CIRCLE,
    BS_ICON_PERSON_DASH,
    BS_ICON_PERSON_DASH_FILL,
    BS_ICON_PERSON_DOWN,
    BS_ICON_PERSON_EXCLAMATION,
    BS_ICON_PERSON_FILL,
    BS_ICON_PERSON_FILL_ADD,
    BS_ICON_PERSON_FILL_CHECK,
    BS_ICON_PERSON_FILL_DASH,
    BS_ICON_PERSON_FILL_DOWN,
    BS_ICON_PERSON_FILL_EXCLAMATION,
    BS_ICON_PERSON_FILL_GEAR,
    BS_ICON_PERSON_FILL_LOCK,
    BS_ICON_PERSON_FILL_SLASH,
    BS_ICON_PERSON_FILL_UP,
    BS_ICON_PERSON_FILL_X,
    BS_ICON_PERSON_GEAR,
    BS_ICON_PERSON_HEART,
    BS_ICON_PERSON_HEARTS,
    BS_ICON_PERSON_LINES_FILL,
    BS_ICON_PERSON_LOCK,
    BS_ICON_PERSON_PLUS,
    BS_ICON_PERSON_PLUS_FILL,
    BS_ICON_PERSON_RAISED_HAND,
    BS_ICON_PERSON_ROLODEX,
    BS_ICON_PERSON_SLASH,
    BS_ICON_PERSON_SQUARE,
    BS_ICON_PERSON_STANDING,
    BS_ICON_PERSON_STANDING_DRESS,
    BS_ICON_PERSON_UP,
    BS_ICON_PERSON_VCARD,
    BS_ICON_PERSON_VCARD_FILL,
    BS_ICON_PERSON_VIDEO,
    BS_ICON_PERSON_VIDEO2,
    BS_ICON_PERSON_VIDEO3,
    BS_ICON_PERSON_WALKING,
    BS_ICON_PERSON_WHEELCHAIR,
    BS_ICON_PERSON_WORKSPACE,
    BS_ICON_PERSON_X,
    BS_ICON_PERSON_X_FILL,
    BS_ICON_PHONE,
    BS_ICON_PHONE_FILL,
    BS_ICON_PHONE_FLIP,
    BS_ICON_PHONE_LANDSCAPE,
    BS_ICON_PHONE_LANDSCAPE_FILL,
    BS_ICON_PHONE_VIBRATE,
    BS_ICON_PHONE_VIBRATE_FILL,
    BS_ICON_PIE_CHART,
    BS_ICON_PIE_CHART_FILL,
    BS_ICON_PIGGY_BANK,
    BS_ICON_PIGGY_BANK_FILL,
    BS_ICON_PIN,
    BS_ICON_PIN_ANGLE,
    BS_ICON_PIN_ANGLE_FILL,
    BS_ICON_PIN_FILL,
    BS_ICON_PIN_MAP,
    BS_ICON_PIN_MAP_FILL,
    BS_ICON_PINTEREST,
    BS_ICON_PIP,
    BS_ICON_PIP_FILL,
    BS_ICON_PLAY,
    BS_ICON_PLAY_BTN,
    BS_ICON_PLAY_BTN_FILL,
    BS_ICON_PLAY_CIRCLE,
    BS_ICON_PLAY_CIRCLE_FILL,
    BS_ICON_PLAY_FILL,
    BS_ICON_PLAYSTATION,
    BS_ICON_PLUG,
    BS_ICON_PLUG_FILL,
    BS_ICON_PLUGIN,
    BS_ICON_PLUS,
    BS_ICON_PLUS_CIRCLE,
    BS_ICON_PLUS_CIRCLE_DOTTED,
    BS_ICON_PLUS_CIRCLE_FILL,
    BS_ICON_PLUS_LG,
    BS_ICON_PLUS_SLASH_MINUS,
    BS_ICON_PLUS_SQUARE,
    BS_ICON_PLUS_SQUARE_DOTTED,
    BS_ICON_PLUS_SQUARE_FILL,
    BS_ICON_POSTAGE,
    BS_ICON_POSTAGE_FILL,
    BS_ICON_POSTAGE_HEART,
    BS_ICON_POSTAGE_HEART_FILL,
    BS_ICON_POSTCARD,
    BS_ICON_POSTCARD_FILL,
    BS_ICON_POSTCARD_HEART,
    BS_ICON_POSTCARD_HEART_FILL,
    BS_ICON_POWER,
    BS_ICON_PRESCRIPTION,
    BS_ICON_PRESCRIPTION2,
    BS_ICON_PRINTER,
    BS_ICON_PRINTER_FILL,
    BS_ICON_PROJECTOR,
    BS_ICON_PROJECTOR_FILL,
    BS_ICON_PUZZLE,
    BS_ICON_PUZZLE_FILL,
    BS_ICON_QR_CODE,
    BS_ICON_QR_CODE_SCAN,
    BS_ICON_QUESTION,
    BS_ICON_QUESTION_CIRCLE,
    BS_ICON_QUESTION_CIRCLE_FILL,
    BS_ICON_QUESTION_DIAMOND,
    BS_ICON_QUESTION_DIAMOND_FILL,
    BS_ICON_QUESTION_LG,
    BS_ICON_QUESTION_OCTAGON,
    BS_ICON_QUESTION_OCTAGON_FILL,
    BS_ICON_QUESTION_SQUARE,
    BS_ICON_QUESTION_SQUARE_FILL,
    BS_ICON_QUORA,
    BS_ICON_QUOTE,
    BS_ICON_R_CIRCLE,
    BS_ICON_R_CIRCLE_FILL,
    BS_ICON_R_SQUARE,
    BS_ICON_R_SQUARE_FILL,
    BS_ICON_RADAR,
    BS_ICON_RADIOACTIVE,
    BS_ICON_RAINBOW,
    BS_ICON_RECEIPT,
    BS_ICON_RECEIPT_CUTOFF,
    BS_ICON_RECEPTION_0,
    BS_ICON_RECEPTION_1,
    BS_ICON_RECEPTION_2,
    BS_ICON_RECEPTION_3,
    BS_ICON_RECEPTION_4,
    BS_ICON_RECORD,
    BS_ICON_RECORD_BTN,
    BS_ICON_RECORD_BTN_FILL,
    BS_ICON_RECORD_CIRCLE,
    BS_ICON_RECORD_CIRCLE_FILL,
    BS_ICON_RECORD_FILL,
    BS_ICON_RECORD2,
    BS_ICON_RECORD2_FILL,
    BS_ICON_RECYCLE,
    BS_ICON_REDDIT,
    BS_ICON_REGEX,
    BS_ICON_REPEAT,
    BS_ICON_REPEAT_1,
    BS_ICON_REPLY,
    BS_ICON_REPLY_ALL,
    BS_ICON_REPLY_ALL_FILL,
    BS_ICON_REPLY_FILL,
    BS_ICON_REWIND,
    BS_ICON_REWIND_BTN,
    BS_ICON_REWIND_BTN_FILL,
    BS_ICON_REWIND_CIRCLE,
    BS_ICON_REWIND_CIRCLE_FILL,
    BS_ICON_REWIND_FILL,
    BS_ICON_ROBOT,
    BS_ICON_ROCKET,
    BS_ICON_ROCKET_FILL,
    BS_ICON_ROCKET_TAKEOFF,
    BS_ICON_ROCKET_TAKEOFF_FILL,
    BS_ICON_ROUTER,
    BS_ICON_ROUTER_FILL,
    BS_ICON_RSS,
    BS_ICON_RSS_FILL,
    BS_ICON_RULERS,
    BS_ICON_SAFE,
    BS_ICON_SAFE_FILL,
    BS_ICON_SAFE2,
    BS_ICON_SAFE2_FILL,
    BS_ICON_SAVE,
    BS_ICON_SAVE_FILL,
    BS_ICON_SAVE2,
    BS_ICON_SAVE2_FILL,
    BS_ICON_SCISSORS,
    BS_ICON_SCOOTER,
    BS_ICON_SCREWDRIVER,
    BS_ICON_SD_CARD,
    BS_ICON_SD_CARD_FILL,
    BS_ICON_SEARCH,
    BS_ICON_SEARCH_HEART,
    BS_ICON_SEARCH_HEART_FILL,
    BS_ICON_SEGMENTED_NAV,
    BS_ICON_SEND,
    BS_ICON_SEND_ARROW_DOWN,
    BS_ICON_SEND_ARROW_DOWN_FILL,
    BS_ICON_SEND_ARROW_UP,
    BS_ICON_SEND_ARROW_UP_FILL,
    BS_ICON_SEND_CHECK,
    BS_ICON_SEND_CHECK_FILL,
    BS_ICON_SEND_DASH,
    BS_ICON_SEND_DASH_FILL,
    BS_ICON_SEND_EXCLAMATION,
    BS_ICON_SEND_EXCLAMATION_FILL,
    BS_ICON_SEND_FILL,
    BS_ICON_SEND_PLUS,
    BS_ICON_SEND_PLUS_FILL,
    BS_ICON_SEND_SLASH,
    BS_ICON_SEND_SLASH_FILL,
    BS_ICON_SEND_X,
    BS_ICON_SEND_X_FILL,
    BS_ICON_SERVER,
    BS_ICON_SHADOWS,
    BS_ICON_SHARE,
    BS_ICON_SHARE_FILL,
    BS_ICON_SHIELD,
    BS_ICON_SHIELD_CHECK,
    BS_ICON_SHIELD_EXCLAMATION,
    BS_ICON_SHIELD_FILL,
    BS_ICON_SHIELD_FILL_CHECK,
    BS_ICON_SHIELD_FILL_EXCLAMATION,
    BS_ICON_SHIELD_FILL_MINUS,
    BS_ICON_SHIELD_FILL_PLUS,
    BS_ICON_SHIELD_FILL_X,
    BS_ICON_SHIELD_LOCK,
    BS_ICON_SHIELD_LOCK_FILL,
    BS_ICON_SHIELD_MINUS,
    BS_ICON_SHIELD_PLUS,
    BS_ICON_SHIELD_SHADED,
    BS_ICON_SHIELD_SLASH,
    BS_ICON_SHIELD_SLASH_FILL,
    BS_ICON_SHIELD_X,
    BS_ICON_SHIFT,
    BS_ICON_SHIFT_FILL,
    BS_ICON_SHOP,
    BS_ICON_SHOP_WINDOW,
    BS_ICON_SHUFFLE,
    BS_ICON_SIGN_DEAD_END,
    BS_ICON_SIGN_DEAD_END_FILL,
    BS_ICON_SIGN_DO_NOT_ENTER,
    BS_ICON_SIGN_DO_NOT_ENTER_FILL,
    BS_ICON_SIGN_INTERSECTION,
    BS_ICON_SIGN_INTERSECTION_FILL,
    BS_ICON_SIGN_INTERSECTION_SIDE,
    BS_ICON_SIGN_INTERSECTION_SIDE_FILL,
    BS_ICON_SIGN_INTERSECTION_T,
    BS_ICON_SIGN_INTERSECTION_T_FILL,
    BS_ICON_SIGN_INTERSECTION_Y,
    BS_ICON_SIGN_INTERSECTION_Y_FILL,
    BS_ICON_SIGN_MERGE_LEFT,
    BS_ICON_SIGN_MERGE_LEFT_FILL,
    BS_ICON_SIGN_MERGE_RIGHT,
    BS_ICON_SIGN_MERGE_RIGHT_FILL,
    BS_ICON_SIGN_NO_LEFT_TURN,
    BS_ICON_SIGN_NO_LEFT_TURN_FILL,
    BS_ICON_SIGN_NO_PARKING,
    BS_ICON_SIGN_NO_PARKING_FILL,
    BS_ICON_SIGN_NO_RIGHT_TURN,
    BS_ICON_SIGN_NO_RIGHT_TURN_FILL,
    BS_ICON_SIGN_RAILROAD,
    BS_ICON_SIGN_RAILROAD_FILL,
    BS_ICON_SIGN_STOP,
    BS_ICON_SIGN_STOP_FILL,
    BS_ICON_SIGN_STOP_LIGHTS,
    BS_ICON_SIGN_STOP_LIGHTS_FILL,
    BS_ICON_SIGN_TURN_LEFT,
    BS_ICON_SIGN_TURN_LEFT_FILL,
    BS_ICON_SIGN_TURN_RIGHT,
    BS_ICON_SIGN_TURN_RIGHT_FILL,
    BS_ICON_SIGN_TURN_SLIGHT_LEFT,
    BS_ICON_SIGN_TURN_SLIGHT_LEFT_FILL,
    BS_ICON_SIGN_TURN_SLIGHT_RIGHT,
    BS_ICON_SIGN_TURN_SLIGHT_RIGHT_FILL,
    BS_ICON_SIGN_YIELD,
    BS_ICON_SIGN_YIELD_FILL,
    BS_ICON_SIGNAL,
    BS_ICON_SIGNPOST,
    BS_ICON_SIGNPOST_2,
    BS_ICON_SIGNPOST_2_FILL,
    BS_ICON_SIGNPOST_FILL,
    BS_ICON_SIGNPOST_SPLIT,
    BS_ICON_SIGNPOST_SPLIT_FILL,
    BS_ICON_SIM,
    BS_ICON_SIM_FILL,
    BS_ICON_SIM_SLASH,
    BS_ICON_SIM_SLASH_FILL,
    BS_ICON_SINA_WEIBO,
    BS_ICON_SKIP_BACKWARD,
    BS_ICON_SKIP_BACKWARD_BTN,
    BS_ICON_SKIP_BACKWARD_BTN_FILL,
    BS_ICON_SKIP_BACKWARD_CIRCLE,
    BS_ICON_SKIP_BACKWARD_CIRCLE_FILL,
    BS_ICON_SKIP_BACKWARD_FILL,
    BS_ICON_SKIP_END,
    BS_ICON_SKIP_END_BTN,
    BS_ICON_SKIP_END_BTN_FILL,
    BS_ICON_SKIP_END_CIRCLE,
    BS_ICON_SKIP_END_CIRCLE_FILL,
    BS_ICON_SKIP_END_FILL,
    BS_ICON_SKIP_FORWARD,
    BS_ICON_SKIP_FORWARD_BTN,
    BS_ICON_SKIP_FORWARD_BTN_FILL,
    BS_ICON_SKIP_FORWARD_CIRCLE,
    BS_ICON_SKIP_FORWARD_CIRCLE_FILL,
    BS_ICON_SKIP_FORWARD_FILL,
    BS_ICON_SKIP_START,
    BS_ICON_SKIP_START_BTN,
    BS_ICON_SKIP_START_BTN_FILL,
    BS_ICON_SKIP_START_CIRCLE,
    BS_ICON_SKIP_START_CIRCLE_FILL,
    BS_ICON_SKIP_START_FILL,
    BS_ICON_SKYPE,
    BS_ICON_SLACK,
    BS_ICON_SLASH,
    BS_ICON_SLASH_CIRCLE,
    BS_ICON_SLASH_CIRCLE_FILL,
    BS_ICON_SLASH_LG,
    BS_ICON_SLASH_SQUARE,
    BS_ICON_SLASH_SQUARE_FILL,
    BS_ICON_SLIDERS,
    BS_ICON_SLIDERS2,
    BS_ICON_SLIDERS2_VERTICAL,
    BS_ICON_SMARTWATCH,
    BS_ICON_SNAPCHAT,
    BS_ICON_SNOW,
    BS_ICON_SNOW2,
    BS_ICON_SNOW3,
    BS_ICON_SORT_ALPHA_DOWN,
    BS_ICON_SORT_ALPHA_DOWN_ALT,
    BS_ICON_SORT_ALPHA_UP,
    BS_ICON_SORT_ALPHA_UP_ALT,
    BS_ICON_SORT_DOWN,
    BS_ICON_SORT_DOWN_ALT,
    BS_ICON_SORT_NUMERIC_DOWN,
    BS_ICON_SORT_NUMERIC_DOWN_ALT,
    BS_ICON_SORT_NUMERIC_UP,
    BS_ICON_SORT_NUMERIC_UP_ALT,
    BS_ICON_SORT_UP,
    BS_ICON_SORT_UP_ALT,
    BS_ICON_SOUNDWAVE,
    BS_ICON_SOURCEFORGE,
    BS_ICON_SPEAKER,
    BS_ICON_SPEAKER_FILL,
    BS_ICON_SPEEDOMETER,
    BS_ICON_SPEEDOMETER2,
    BS_ICON_SPELLCHECK,
    BS_ICON_SPOTIFY,
    BS_ICON_SQUARE,
    BS_ICON_SQUARE_FILL,
    BS_ICON_SQUARE_HALF,
    BS_ICON_STACK,
    BS_ICON_STACK_OVERFLOW,
    BS_ICON_STAR,
    BS_ICON_STAR_FILL,
    BS_ICON_STAR_HALF,
    BS_ICON_STARS,
    BS_ICON_STEAM,
    BS_ICON_STICKIES,
    BS_ICON_STICKIES_FILL,
    BS_ICON_STICKY,
    BS_ICON_STICKY_FILL,
    BS_ICON_STOP,
    BS_ICON_STOP_BTN,
    BS_ICON_STOP_BTN_FILL,
    BS_ICON_STOP_CIRCLE,
    BS_ICON_STOP_CIRCLE_FILL,
    BS_ICON_STOP_FILL,
    BS_ICON_STOPLIGHTS,
    BS_ICON_STOPLIGHTS_FILL,
    BS_ICON_STOPWATCH,
    BS_ICON_STOPWATCH_FILL,
    BS_ICON_STRAVA,
    BS_ICON_STRIPE,
    BS_ICON_SUBSCRIPT,
    BS_ICON_SUBSTACK,
    BS_ICON_SUBTRACT,
    BS_ICON_SUIT_CLUB,
    BS_ICON_SUIT_CLUB_FILL,
    BS_ICON_SUIT_DIAMOND,
    BS_ICON_SUIT_DIAMOND_FILL,
    BS_ICON_SUIT_HEART,
    BS_ICON_SUIT_HEART_FILL,
    BS_ICON_SUIT_SPADE,
    BS_ICON_SUIT_SPADE_FILL,
    BS_ICON_SUITCASE,
    BS_ICON_SUITCASE_FILL,
    BS_ICON_SUITCASE_LG,
    BS_ICON_SUITCASE_LG_FILL,
    BS_ICON_SUITCASE2,
    BS_ICON_SUITCASE2_FILL,
    BS_ICON_SUN,
    BS_ICON_SUN_FILL,
    BS_ICON_SUNGLASSES,
    BS_ICON_SUNRISE,
    BS_ICON_SUNRISE_FILL,
    BS_ICON_SUNSET,
    BS_ICON_SUNSET_FILL,
    BS_ICON_SUPERSCRIPT,
    BS_ICON_SYMMETRY_HORIZONTAL,
    BS_ICON_SYMMETRY_VERTICAL,
    BS_ICON_TABLE,
    BS_ICON_TABLET,
    BS_ICON_TABLET_FILL,
    BS_ICON_TABLET_LANDSCAPE,
    BS_ICON_TABLET_LANDSCAPE_FILL,
    BS_ICON_TAG,
    BS_ICON_TAG_FILL,
    BS_ICON_TAGS,
    BS_ICON_TAGS_FILL,
    BS_ICON_TAXI_FRONT,
    BS_ICON_TAXI_FRONT_FILL,
    BS_ICON_TELEGRAM,
    BS_ICON_TELEPHONE,
    BS_ICON_TELEPHONE_FILL,
    BS_ICON_TELEPHONE_FORWARD,
    BS_ICON_TELEPHONE_FORWARD_FILL,
    BS_ICON_TELEPHONE_INBOUND,
    BS_ICON_TELEPHONE_INBOUND_FILL,
    BS_ICON_TELEPHONE_MINUS,
    BS_ICON_TELEPHONE_MINUS_FILL,
    BS_ICON_TELEPHONE_OUTBOUND,
    BS_ICON_TELEPHONE_OUTBOUND_FILL,
    BS_ICON_TELEPHONE_PLUS,
    BS_ICON_TELEPHONE_PLUS_FILL,
    BS_ICON_TELEPHONE_X,
    BS_ICON_TELEPHONE_X_FILL,
    BS_ICON_TENCENT_QQ,
    BS_ICON_TERMINAL,
    BS_ICON_TERMINAL_DASH,
    BS_ICON_TERMINAL_FILL,
    BS_ICON_TERMINAL_PLUS,
    BS_ICON_TERMINAL_SPLIT,
    BS_ICON_TERMINAL_X,
    BS_ICON_TEXT_CENTER,
    BS_ICON_TEXT_INDENT_LEFT,
    BS_ICON_TEXT_INDENT_RIGHT,
    BS_ICON_TEXT_LEFT,
    BS_ICON_TEXT_PARAGRAPH,
    BS_ICON_TEXT_RIGHT,
    BS_ICON_TEXT_WRAP,
    BS_ICON_TEXTAREA,
    BS_ICON_TEXTAREA_RESIZE,
    BS_ICON_TEXTAREA_T,
    BS_ICON_THERMOMETER,
    BS_ICON_THERMOMETER_HALF,
    BS_ICON_THERMOMETER_HIGH,
    BS_ICON_THERMOMETER_LOW,
    BS_ICON_THERMOMETER_SNOW,
    BS_ICON_THERMOMETER_SUN,
    BS_ICON_THREADS,
    BS_ICON_THREADS_FILL,
    BS_ICON_THREE_DOTS,
    BS_ICON_THREE_DOTS_VERTICAL,
    BS_ICON_THUNDERBOLT,
    BS_ICON_THUNDERBOLT_FILL,
    BS_ICON_TICKET,
    BS_ICON_TICKET_DETAILED,
    BS_ICON_TICKET_DETAILED_FILL,
    BS_ICON_TICKET_FILL,
    BS_ICON_TICKET_PERFORATED,
    BS_ICON_TICKET_PERFORATED_FILL,
    BS_ICON_TIKTOK,
    BS_ICON_TOGGLE_OFF,
    BS_ICON_TOGGLE_ON,
    BS_ICON_TOGGLE2_OFF,
    BS_ICON_TOGGLE2_ON,
    BS_ICON_TOGGLES,
    BS_ICON_TOGGLES2,
    BS_ICON_TOOLS,
    BS_ICON_TORNADO,
    BS_ICON_TRAIN_FREIGHT_FRONT,
    BS_ICON_TRAIN_FREIGHT_FRONT_FILL,
    BS_ICON_TRAIN_FRONT,
    BS_ICON_TRAIN_FRONT_FILL,
    BS_ICON_TRAIN_LIGHTRAIL_FRONT,
    BS_ICON_TRAIN_LIGHTRAIL_FRONT_FILL,
    BS_ICON_TRANSLATE,
    BS_ICON_TRANSPARENCY,
    BS_ICON_TRASH,
    BS_ICON_TRASH_FILL,
    BS_ICON_TRASH2,
    BS_ICON_TRASH2_FILL,
    BS_ICON_TRASH3,
    BS_ICON_TRASH3_FILL,
    BS_ICON_TREE,
    BS_ICON_TREE_FILL,
    BS_ICON_TRELLO,
    BS_ICON_TRIANGLE,
    BS_ICON_TRIANGLE_FILL,
    BS_ICON_TRIANGLE_HALF,
    BS_ICON_TROPHY,
    BS_ICON_TROPHY_FILL,
    BS_ICON_TROPICAL_STORM,
    BS_ICON_TRUCK,
    BS_ICON_TRUCK_FLATBED,
    BS_ICON_TRUCK_FRONT,
    BS_ICON_TRUCK_FRONT_FILL,
    BS_ICON_TSUNAMI,
    BS_ICON_TV,
    BS_ICON_TV_FILL,
    BS_ICON_TWITCH,
    BS_ICON_TWITTER,
    BS_ICON_TWITTER_X,
    BS_ICON_TYPE,
    BS_ICON_TYPE_BOLD,
    BS_ICON_TYPE_H1,
    BS_ICON_TYPE_H2,
    BS_ICON_TYPE_H3,
    BS_ICON_TYPE_H4,
    BS_ICON_TYPE_H5,
    BS_ICON_TYPE_H6,
    BS_ICON_TYPE_ITALIC,
    BS_ICON_TYPE_STRIKETHROUGH,
    BS_ICON_TYPE_UNDERLINE,
    BS_ICON_UBUNTU,
    BS_ICON_UI_CHECKS,
    BS_ICON_UI_CHECKS_GRID,
    BS_ICON_UI_RADIOS,
    BS_ICON_UI_RADIOS_GRID,
    BS_ICON_UMBRELLA,
    BS_ICON_UMBRELLA_FILL,
    BS_ICON_UNINDENT,
    BS_ICON_UNION,
    BS_ICON_UNITY,
    BS_ICON_UNIVERSAL_ACCESS,
    BS_ICON_UNIVERSAL_ACCESS_CIRCLE,
    BS_ICON_UNLOCK,
    BS_ICON_UNLOCK_FILL,
    BS_ICON_UPC,
    BS_ICON_UPC_SCAN,
    BS_ICON_UPLOAD,
    BS_ICON_USB,
    BS_ICON_USB_C,
    BS_ICON_USB_C_FILL,
    BS_ICON_USB_DRIVE,
    BS_ICON_USB_DRIVE_FILL,
    BS_ICON_USB_FILL,
    BS_ICON_USB_MICRO,
    BS_ICON_USB_MICRO_FILL,
    BS_ICON_USB_MINI,
    BS_ICON_USB_MINI_FILL,
    BS_ICON_USB_PLUG,
    BS_ICON_USB_PLUG_FILL,
    BS_ICON_USB_SYMBOL,
    BS_ICON_VALENTINE,
    BS_ICON_VALENTINE2,
    BS_ICON_VECTOR_PEN,
    BS_ICON_VIEW_LIST,
    BS_ICON_VIEW_STACKED,
    BS_ICON_VIGNETTE,
    BS_ICON_VIMEO,
    BS_ICON_VINYL,
    BS_ICON_VINYL_FILL,
    BS_ICON_VIRUS,
    BS_ICON_VIRUS2,
    BS_ICON_VOICEMAIL,
    BS_ICON_VOLUME_DOWN,
    BS_ICON_VOLUME_DOWN_FILL,
    BS_ICON_VOLUME_MUTE,
    BS_ICON_VOLUME_MUTE_FILL,
    BS_ICON_VOLUME_OFF,
    BS_ICON_VOLUME_OFF_FILL,
    BS_ICON_VOLUME_UP,
    BS_ICON_VOLUME_UP_FILL,
    BS_ICON_VR,
    BS_ICON_WALLET,
    BS_ICON_WALLET_FILL,
    BS_ICON_WALLET2,
    BS_ICON_WATCH,
    BS_ICON_WATER,
    BS_ICON_WEBCAM,
    BS_ICON_WEBCAM_FILL,
    BS_ICON_WECHAT,
    BS_ICON_WHATSAPP,
    BS_ICON_WIFI,
    BS_ICON_WIFI_1,
    BS_ICON_WIFI_2,
    BS_ICON_WIFI_OFF,
    BS_ICON_WIKIPEDIA,
    BS_ICON_WIND,
    BS_ICON_WINDOW,
    BS_ICON_WINDOW_DASH,
    BS_ICON_WINDOW_DESKTOP,
    BS_ICON_WINDOW_DOCK,
    BS_ICON_WINDOW_FULLSCREEN,
    BS_ICON_WINDOW_PLUS,
    BS_ICON_WINDOW_SIDEBAR,
    BS_ICON_WINDOW_SPLIT,
    BS_ICON_WINDOW_STACK,
    BS_ICON_WINDOW_X,
    BS_ICON_WINDOWS,
    BS_ICON_WORDPRESS,
    BS_ICON_WRENCH,
    BS_ICON_WRENCH_ADJUSTABLE,
    BS_ICON_WRENCH_ADJUSTABLE_CIRCLE,
    BS_ICON_WRENCH_ADJUSTABLE_CIRCLE_FILL,
    BS_ICON_X,
    BS_ICON_X_CIRCLE,
    BS_ICON_X_CIRCLE_FILL,
    BS_ICON_X_DIAMOND,
    BS_ICON_X_DIAMOND_FILL,
    BS_ICON_X_LG,
    BS_ICON_X_OCTAGON,
    BS_ICON_X_OCTAGON_FILL,
    BS_ICON_X_SQUARE,
    BS_ICON_X_SQUARE_FILL,
    BS_ICON_XBOX,
    BS_ICON_YELP,
    BS_ICON_YIN_YANG,
    BS_ICON_YOUTUBE,
    BS_ICON_ZOOM_IN,
    BS_ICON_ZOOM_OUT,
};

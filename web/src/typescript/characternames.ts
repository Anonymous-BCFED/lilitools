/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/main/docs/suggestions.md
 */
export class CharacterNames {
  Feminine: string;
  Androgynous: string;
  Masculine: string;

  constructor() {
    this.Feminine = "";
    this.Androgynous = "";
    this.Masculine = "";
  }

  fromDict(data: { [name: string]: any }): void {
    this.Feminine = data["feminine"];
    this.Masculine = data["masculine"];
    this.Androgynous = data["androgynous"];
  }

  toDict(): { feminine: string; masculine: string; androgynous: string; } {
    return {
      feminine: this.Feminine,
      masculine: this.Masculine,
      androgynous: this.Androgynous,
    };
  }
}

import { $E } from "../../utils";
import $ from "jquery";
export class LoadingDialog {
  $body: JQuery<HTMLElement>;
  $loading: JQuery<HTMLElement>;
  $loadtext: JQuery<HTMLElement>;
  $progress_outer: JQuery<HTMLElement>;
  $progress: JQuery<HTMLElement>;

  static _I: LoadingDialog | null = null;
  static MDID: string = "LOADING";

  static instance(): LoadingDialog {
    if (LoadingDialog._I === null) {
      LoadingDialog._I = new LoadingDialog(
        $(document.getElementsByTagName("body"))
      );
    }
    return LoadingDialog._I;
  }

  constructor(body: JQuery<HTMLElement>) {
    this.$body = body;
    this.$loading = $E("div", { id: "dlg-loading" }).appendTo(this.$body);
    this.$loadtext = $E("label", { id: "loading-top-text" })
      .text("LOADING...")
      .appendTo(this.$loading);
    this.$progress_outer = $E("div", { class: "progress" }).appendTo(
      this.$loading
    );

    this.$progress = $E("div", {
      class: "progress-bar",
      role: "progressbar",
      "aria-valuenow": "0",
      "aria-valuemin": "0",
      "aria-valuemax": "1",
    })
      .text("0%")
      .appendTo(this.$progress_outer);
  }

  set(current: number, total: number, text: string): void {
    this.$loadtext.text(text);
    this.$progress
      .attr("aria-valuenow", total.toString())
      .attr("aria-valuemax", current.toString())
      .css("width", ((current / total) * 100).toString() + "%")
      .text(`(${current}/${total})`)
      .removeClass("progress-bar-striped")
      .removeClass("progress-bar-animated")
      .show();
    this.$loading.fadeIn();
  }

  hide(): void {
    this.$loading.fadeOut();
  }

  setIndeterminate(text: string): void {
    this.$loadtext.text(`${text} (?/?)`);
    this.$progress
      .attr("aria-valuenow", "10")
      .attr("aria-valuemax", "10")
      .css("width", "100%")
      .addClass("progress-bar-striped")
      .addClass("progress-bar-animated")
      .show();
    this.$loading.fadeIn();
  }
}

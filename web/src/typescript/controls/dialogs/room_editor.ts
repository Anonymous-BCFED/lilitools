import $ from "jquery";
import { DynamicModal } from "../../dynmodal";
import { BSForm } from "../form";
import { $E, assertNotNull, enumToSelDict } from "../../utils";
import { Room } from "../../room";
import { ERoomPurpose } from "../../enums/roompurpose";
export class RoomEditor {
  static _I: RoomEditor;

  room: Room | null;
  new: boolean;
  modal: DynamicModal;
  form: BSForm;
  txtWorldID: JQuery<HTMLElement>;
  txtX: JQuery<HTMLElement>;
  txtY: JQuery<HTMLElement>;
  txtName: JQuery<HTMLElement>;
  selPurpose: JQuery<HTMLElement>;
  txtSlots: JQuery<HTMLElement>;
  btnClose: JQuery<HTMLElement>;
  btnSaveChanges: JQuery<HTMLElement>;

  static MDID: string = "ROOMEDITOR";

  static instance(): RoomEditor {
    if (
      RoomEditor._I === null ||
      DynamicModal.checkIfRebuildNeeded(this.MDID)
    ) {
      RoomEditor._I = new RoomEditor();
    }
    return RoomEditor._I;
  }

  static display(room: Room | null): RoomEditor {
    let re = RoomEditor.instance();
    re.__configure(room);
    re.show();
    return re;
  }

  __configure(room: Room | null) {
    this.room = room;
    this.new = false;
    if (this.room === null) {
      this.room = new Room();
      this.new = true;
    }
    this.txtWorldID.val(this.room.WorldID);
    this.txtX.val(this.room.X);
    this.txtY.val(this.room.Y);
    this.txtName.val(this.room.Name);
    this.txtSlots.val(this.room.Slots);
  }

  constructor() {
    var _this: RoomEditor = this;
    this.room = null;
    this.new = false;
    this.modal = DynamicModal.instance(RoomEditor.MDID);
    this.modal.setTitle("Room Editor");
    this.modal.clearBody();
    this.modal.clearFooter();
    this.form = new BSForm(this.modal.body as JQuery);
    this.txtWorldID = this.form
      .addTextbox(
        "text",
        "World ID",
        "txtWorldID",
        "Unique name of the world this room is in."
      )
      .val("");
    this.txtX = this.form
      .addTextbox(
        "number",
        "X",
        "txtX",
        "X-Coordinate of this room in the world."
      )
      .val("");
    this.txtY = this.form
      .addTextbox(
        "number",
        "Y",
        "txtY",
        "Y-Coordinate of this room in the world."
      )
      .val("");
    this.txtName = this.form
      .addTextbox(
        "text",
        "Name",
        "txtName",
        "Name of the room (Training Room 1, for instance).  MUST match the room name in the game."
      )
      .prop("required", true)
      .val("");
    this.selPurpose = this.form
      .addSelect(
        "Purpose",
        "selPurpose",
        enumToSelDict(ERoomPurpose),
        "What is this room used for?"
      )
      .prop("required", true)
      .val("Unknown");
    this.txtSlots = this.form
      .addTextbox(
        "number",
        "Slots",
        "txtSlots",
        "The maximum number of NPCs that can be stored in this room as slaves."
      )
      .prop("required", true)
      .attr("min", "1")
      .attr("max", "4")
      .val("");

    this.btnClose = $E("button", {
      type: "button",
      class: "btn btn-secondary",
      "data-bs-dismiss": "modal",
    });
    this.modal.addToFooter(this.btnClose);
    this.btnClose.text("Close");

    this.btnSaveChanges = $E("button", {
      type: "button",
      class: "btn btn-primary",
    });
    this.modal.addToFooter(this.btnSaveChanges);
    function ensureIsInteger(el: JQuery<HTMLElement>, label: string): boolean {
      try {
        parseInt(el.val() as string);
        el.removeClass("error");
        return true;
      } catch (error) {
        window.toasts.danger(`${label} is not an integer.`);
        _this.txtX.addClass("error");
        return false;
      }
    }
    this.btnSaveChanges.text("Save Changes").on("click", function () {
      assertNotNull(_this.room);
      // TODO: Validation
      var xhr;

      var validated: boolean = true;
      if (!ensureIsInteger(_this.txtX, "X")) validated = false;
      if (!ensureIsInteger(_this.txtY, "Y")) validated = false;
      if (!ensureIsInteger(_this.txtSlots, "Slots")) validated = false;
      if (_this.new) {
        xhr = $.post("/api/training_room/new", {
          name: _this.txtName.val(),
          purpose: _this.selPurpose.val(),
          slots: parseInt(_this.txtSlots.val() as string),
        });
      } else {
        var wid = _this.txtWorldID.val() as string;
        var x = parseInt(_this.txtX.val() as string);
        var y = parseInt(_this.txtY.val() as string);
        xhr = $.post(`/api/training_room/${wid}/${x}/${y}/edit`, {
          worldid: wid,
          x: x,
          y: y,
          name: _this.txtName.val(),
          purpose: _this.selPurpose.val(),
          slots: parseInt(_this.txtSlots.val() as string),
        });
      }
      return xhr
        .done((data: Record<string, any>) => {
          assertNotNull(_this.room);
          if (data.ok) {
            window.toasts.success(
              `Successfully edited room ${_this.room.Name}!`
            );
            _this.modal.hide();
          } else {
            window.toasts.danger("Failed to edit room: " + data.message);
          }
          window.reloadData(() => {
            window.currentPage.reload();
          });
        })
        .fail((data: Record<string, any>) => {
          var needle;
          if (
            !((needle = "message"),
            Array.from(Object.keys(data)).includes(needle))
          ) {
            window.toasts.danger("Failed to edit. See console.");
          } else {
            window.toasts.danger("Failed to edit room: " + data.message);
          }
        });
    });
    this.modal.show();
  }

  show() {
    this.modal.show();
  }
}

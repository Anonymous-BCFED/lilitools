import $ from "jquery";
import { SlaveInfo } from "../../slave_info";
import { DynamicModal } from "../../dynmodal";
import { BSForm } from "../form";
import {
  $E,
  $mkFontAwesome,
  EFontAwesomeStyle,
  assertNotNull,
  enumToSelDict,
} from "../../utils";
import { EStatus } from "../../enums/status";
import { EPlans } from "../../enums/plans";
import { ETreatment } from "../../enums/treatment";

export class SlaveEditor {
  static _I: SlaveEditor | null;

  slave: SlaveInfo | null;
  modal: DynamicModal;
  form: BSForm;
  txtIndex: JQuery<HTMLElement>;
  txtSID: JQuery<HTMLElement>;
  txtID: JQuery<HTMLElement>;
  txtName: JQuery<HTMLElement>;
  txtSpecies: JQuery<HTMLElement>;
  txtGenderIdentity: JQuery<HTMLElement>;
  txtJobs: JQuery<HTMLElement>;
  txtHomeLocation: JQuery<HTMLElement>;
  txtCurrentLocation: JQuery<HTMLElement>;
  txtAffection: JQuery<HTMLElement>;
  txtObedience: JQuery<HTMLElement>;
  txtValue: JQuery<HTMLElement>;
  selPlans: JQuery<HTMLElement>;
  selStatus: JQuery<HTMLElement>;
  selTreatment: JQuery<HTMLElement>;
  rowTags: JQuery<HTMLElement>;
  txtNotes: JQuery<HTMLElement>;
  btnClose: JQuery<HTMLElement>;
  btnSaveChanges: JQuery<HTMLElement>;
  txtDetectedGender: JQuery<HTMLElement>;
  divTags: JQuery<HTMLElement>;
  tags: string[];

  static MDID: string="SLAVEEDIT";

  static instance(): SlaveEditor {
    if (SlaveEditor._I === null || DynamicModal.checkIfRebuildNeeded(this.MDID)) {
      SlaveEditor._I = new SlaveEditor();
    }
    return SlaveEditor._I;
  }

  static display(slave: SlaveInfo): SlaveEditor {
    let se = SlaveEditor.instance();
    se.__configure(slave);
    se.show();
    return se;
  }

  __configure(slave: SlaveInfo): void {
    this.slave = slave;
    this.txtIndex.val(this.slave.Index);
    this.txtSID.val(this.slave.SID);
    this.txtID.val(this.slave.ID);
    this.txtName.val(this.slave.Name);
    this.txtSpecies.val(this.slave.Race);
    this.txtGenderIdentity.val(this.slave.GenderIdentity.slang);
    this.txtDetectedGender.val(this.slave.DetectedGender.slang);
    this.txtJobs.val(this.slave.Jobs.join(", "));
    this.txtHomeLocation.val(this.slave.HomeLocationName);
    this.txtCurrentLocation.val(this.slave.CurrentLocationName);
    this.txtAffection.val(this.slave.Affection);
    this.txtObedience.val(this.slave.Obedience);
    this.txtValue.val(this.slave.Value);
    this.selPlans.val(EPlans.ValueToString(this.slave.Plans));
    this.selStatus.val(EStatus.ValueToString(this.slave.Status));
    this.selTreatment.val(ETreatment.ValueToString(this.slave.Treatment));
    this.txtNotes.val(this.slave.Notes);

    this.tags = [];
    this.clearAllTags();
    this.slave.Tags.forEach((tag) => {
      if (tag.trim() == "") return;
      this.addTag(tag);
    });
  }
  addTag(tag: string) {
    var _this = this;
    this.tags.push(tag);
    let stagid = tag.replace(/[^A-Za-z0-9]+/, "_").toLowerCase();
    this.divTags.append(
      $E("span", {
        class: "tag tag-userstyle-" + stagid,
        id: `tag-${stagid}`,
      })
        .append($E("label", {}, tag))
        .append(
          $E("button", { type: "button" })
            .append($mkFontAwesome("xmark", EFontAwesomeStyle.SOLID))
            .on("click", { tagid: tag, stagid: stagid }, (e) => {
              assertNotNull(_this.slave);
              let tagid = e.data.tagid;
              $.post("/api/slave/tag/delete", {
                id: _this.slave.ID,
                tag: e.data.stagid,
              })
                .done(function () {
                  _this.tags = _this.tags.filter((x) => {
                    return x !== tagid;
                  });
                  $("#tag-" + e.data.stagid).remove();
                })
                .fail(function (data: any) {
                  var needle;
                  if (
                    !((needle = "message"),
                    Array.from(Object.keys(data)).includes(needle))
                  ) {
                    window.toasts.danger("Failed to edit. See console.");
                  } else {
                    window.toasts.danger(
                      "Failed to edit slave: " + data.message
                    );
                  }
                });
            })
        )
    );
  }
  clearAllTags() {
    this.tags = [];
    this.divTags.html("");
  }

  constructor() {
    var _this = this;
    this.tags = [];
    //console.dir @slave
    this.slave = null;
    this.modal = DynamicModal.instance(SlaveEditor.MDID);
    this.modal.setTitle("Slave Editor");
    this.modal.clearBody();
    this.modal.clearFooter();
    assertNotNull(this.modal.body);
    this.form = new BSForm(this.modal.body);
    this.txtIndex = this.form
      .addTextbox(
        "text",
        "Index",
        "txtIndex",
        "Order of the NPC slavery record in the savefile. Useful for sorting based on acquisition time."
      )
      .prop("disabled", true)
      .prop("readonly", true)
      .val("");
    this.txtSID = this.form
      .addTextbox("text", "SID", "txtSID", "Unique Slave ID of the NPC.")
      .prop("disabled", true)
      .prop("readonly", true)
      .val("");
    this.txtID = this.form
      .addTextbox("text", "ID", "txtID", "Unique ID of the NPC.")
      .prop("disabled", true)
      .prop("readonly", true)
      .val("");
    this.txtName = this.form
      .addTextbox("text", "Name", "txtName", "First name of the NPC.")
      .prop("disabled", true)
      .prop("readonly", true)
      .val("");
    this.txtSpecies = this.form
      .addTextbox(
        "text",
        "Species",
        "txtSpecies",
        "The species type of the NPC, as represented by the game."
      )
      .prop("disabled", true)
      .prop("readonly", true)
      .val("");
    this.txtGenderIdentity = this.form
      .addTextbox(
        "text",
        "Gender Identity",
        "txtGenderIdentity",
        "NPC's gender identity"
      )
      .prop("disabled", true)
      .prop("readonly", true)
      .val("");
    this.txtDetectedGender = this.form
      .addTextbox(
        "text",
        "Detected Gender",
        "txtDetectedGender",
        "NPC's gender as seen by others"
      )
      .prop("disabled", true)
      .prop("readonly", true)
      .val("");
    this.txtJobs = this.form
      .addTextbox(
        "text",
        "Jobs",
        "txtJobs",
        "A list of jobs currently assigned to this slave, separated by commas. May be blank."
      )
      .prop("disabled", true)
      .prop("readonly", true)
      .val("");
    this.txtHomeLocation = this.form
      .addTextbox(
        "text",
        "Home Cell",
        "txtHomeLocation",
        "Which cell this NPC calls home."
      )
      .prop("disabled", true)
      .prop("readonly", true)
      .val("");
    this.txtCurrentLocation = this.form
      .addTextbox(
        "text",
        "Current Cell",
        "txtCurrentLocation",
        "Where the NPC currently is."
      )
      .prop("disabled", true)
      .prop("readonly", true)
      .val("");
    this.txtAffection = this.form
      .addTextbox(
        "text",
        "Affection",
        "txtAffection",
        "How much this NPC likes you or not."
      )
      .prop("disabled", true)
      .prop("readonly", true)
      .val("");
    this.txtObedience = this.form
      .addTextbox(
        "text",
        "Obedience",
        "txtObedience",
        "How likely the slave is willing to obey your commands."
      )
      .prop("disabled", true)
      .prop("readonly", true)
      .val("");
    this.txtValue = this.form
      .addTextbox(
        "text",
        "Value",
        "txtValue",
        "How many flames this slave currently is worth."
      )
      .prop("disabled", true)
      .prop("readonly", true)
      .val("");

    this.selPlans = this.form
      .addSelect(
        "Plans",
        "selPlans",
        enumToSelDict(EPlans),
        "What plans you have for this slave, if any."
      )
      .val("");
    this.selStatus = this.form
      .addSelect(
        "Status",
        "selStatus",
        enumToSelDict(EStatus),
        "Current status of the slave in question."
      )
      .val("");
    this.selTreatment = this.form
      .addSelect(
        "Treatment",
        "selTreatment",
        enumToSelDict(ETreatment),
        "How to treat this slave."
      )
      .val("");

    this.divTags = $E("div", { id: "divTags" });
    this.rowTags = this.form.addRow("Tags", "rowTags");
    let addText = $E("input", {
      type: "textbox",
      id: "txtNewTag",
      class: "form-control",
    });
    let addBtn = $E(
      "button",
      { type: "button", class: "btn btn-primary" },
      "Add"
    ).on("click", (e) => {
      let v = addText.val();
      if (v != undefined && v != null) {
        v = v.toString();
        _this.addTag(v);
      }
      addText.val("");
      return;
    });
    let addbar = $E("div", { class: "tags-hdr" })
      .append(addText)
      .append(addBtn);
    this.rowTags.append(addbar);
    this.rowTags.append(this.divTags);
    this.txtNotes = this.form
      .addTextarea(
        "Notes",
        "txtNotes",
        "Any notes you may have about this character."
      )
      .val("");
    this.btnClose = $E("button", {
      type: "button",
      class: "btn btn-secondary",
      "data-bs-dismiss": "modal",
    });
    this.modal.addToFooter(this.btnClose);
    this.btnClose.text("Close");
    this.modal.show();
    this.btnSaveChanges = $E("button", {
      type: "button",
      class: "btn btn-primary",
    });
    this.modal.addToFooter(this.btnSaveChanges);
    this.btnSaveChanges.text("Save Changes").on("click", function () {
      // TODO: Validation
      assertNotNull(_this.slave);
      return $.post("/api/slave/edit", {
        id: _this.slave.ID,
        plans: _this.selPlans.val(),
        status: _this.selStatus.val(),
        treatment: _this.selTreatment.val(),
        tags: _this.tags,
        notes: _this.txtNotes.val(),
      })
        .done(function (data) {
          assertNotNull(_this.slave);

          if (data.ok) {
            window.toasts.success(
              "Successfully edited ".concat(_this.slave.ID, "!")
            );
            _this.modal.hide();
          } else {
            window.toasts.danger("Failed to edit slave: " + data.message);
          }
          window.reloadData(function () {
            window.currentPage.reload();
          });
        })
        .fail(function (data: any) {
          var needle;
          if (
            !((needle = "message"),
            Array.from(Object.keys(data)).includes(needle))
          ) {
            window.toasts.danger("Failed to edit. See console.");
          } else {
            window.toasts.danger("Failed to edit slave: " + data.message);
          }
        });
    });
  }

  show(): void {
    this.modal.show();
  }
}

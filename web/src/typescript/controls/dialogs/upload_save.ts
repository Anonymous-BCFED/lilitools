import { DynamicModal } from "../../dynmodal";
import { $E } from "../../utils";
import { BSForm } from "../form";
import $ from "jquery";

export class UploadSaveDialog {
  static MDID: string = "UPLOAD";
  initialSaveFile: string;
  modal: DynamicModal;
  form: BSForm;
  txtSaveFile: JQuery<HTMLElement>;
  btnClose: JQuery<HTMLElement>;
  btnSaveChanges: JQuery<HTMLElement>;
  constructor(initialSaveFile: string) {
    var _this = this;
    this.initialSaveFile = initialSaveFile;
    this.modal = DynamicModal.instance(UploadSaveDialog.MDID);
    this.modal.setTitle("Upload Save");
    this.modal.clearBody();
    this.modal.clearFooter();
    this.form = new BSForm(this.modal.body as JQuery);
    this.txtSaveFile = this.form
      .addTextbox(
        "text",
        "Save File",
        "txtSaveFile",
        "The XML file that is your save.  Usually found at data/saves/."
      )
      .attr("data-toggle", "dropdown")
      .val(this.initialSaveFile)
      .on("input", function () {
        _this.triggerAutocomplete();
      })
      .on("blur", function () {
        var dd = _this.txtSaveFile.parent();
        dd.find(".dropdown-menu").fadeOut();
      });
    this.btnClose = $E("button", {
      type: "button",
      class: "btn btn-secondary",
      "data-bs-dismiss": "modal",
    });
    this.modal.addToFooter(this.btnClose);
    this.btnClose.text("Close");
    this.modal.show();
    this.btnSaveChanges = $E("button", {
      type: "button",
      class: "btn btn-primary",
    });
    this.modal.addToFooter(this.btnSaveChanges);
    this.btnSaveChanges.text("Import").on("click", function () {
      // TODO: Validation
      $.post("/api/slaves/import", { path: _this.txtSaveFile.val() })
        .done(function (data) {
          if (data.ok) {
            window.toasts.success("Successfully imported!");
          } else {
            window.toasts.danger("Failed to import save: " + data.message);
          }
          _this.modal.hide();
          window.reloadData(() => {
            window.currentPage.reload();
          });
        })
        .fail(function (data) {
          //console && console.debug data
          switch (data.status) {
            case 400:
              window.toasts.danger("File does not exist.");
              break;
            case 403:
              window.toasts.danger(data.responseText);
              break;
            case 500:
              window.toasts.danger(
                "Failed to import save (HTTP 500: INTERNAL ERROR) - Check server console."
              );
              break;
          }
        });
    });
  }
  show() {
    return this.modal.show();
  }
  triggerAutocomplete() {
    var _this = this;
    $.get({
      url: "/api/autocomplete/path",
      data: { q: this.txtSaveFile.val() },
    })
      .done(function (data: [string, string][]) {
        var dd = _this.txtSaveFile.parent();
        dd.find(".dropdown-menu").remove();
        var ddm = $E("div", { class: "dropdown-menu" }).appendTo(dd);
        data.forEach((entry) => {
          var [entryText, entryType] = entry;
          var faIcon = $E("i", {
            class: (function () {
              switch (entryType) {
                case "f":
                  return "fas fa-file";
                case "d":
                  return "fas fa-folder";
                default:
                  return "fas fa-exclamation-circle";
              }
            })(),
          });
          $E("a", {
            class: "dropdown-item",
            "data-value": entryText,
          })
            .text(entryText)
            .appendTo(ddm)
            .on("click", function () {
              $("#txtSaveFile")
                .val($(this).attr("data-value") as string)
                .trigger("input");
            })
            .prepend(faIcon);
        });
        ddm.show();
      })
      .fail(function (data) {
        window.toasts.danger(
          "Failed to complete autocomplete request. See console (F12)."
        );
      });
  }
}

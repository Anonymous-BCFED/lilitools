import { $E } from "../utils";

export class BaseFormControl {
  form: JQuery<HTMLElement>;
  label: JQuery<HTMLElement>;
  control: JQuery<HTMLElement>;
  required: boolean;
  constructor($form: JQuery, $label: JQuery, $control: JQuery) {
    this.form = $form;
    this.label = $label;
    this.control = $control;
    this.required = false;
  }
  isRequired() {
    this.control.prop("required", true);
    this.required = true;
    return this;
  }
  checkRequired() {
    return this.control.val() !== null && this.control.val() !== "";
  }
  getValue() {
    return this.control.val();
  }
}
export class BSForm {
  root: JQuery;
  form: JQuery;
  rows: JQuery[];
  constructor(root: JQuery) {
    this.root = root;
    this.form = $E("form");
    this.form.appendTo(this.root);
    this.rows = [];
  }
  addTextbox(
    type: string,
    label: string,
    name: string,
    desc: string | null = null
  ) {
    if (desc === void 0) {
      desc = null;
    }
    var dd = $E("div", { class: "dropdown" });
    var txt = $E("input", {
      type: type,
      class: "form-control",
      id: name,
      name: name,
    }).appendTo(dd);
    var row = this.addRow(label, name).append(dd);
    if (desc) {
      this.addDesc(row, txt, desc);
    }
    return txt;
  }
  addTextarea(label: string, name: string, desc: string | null = null) {
    if (desc === void 0) {
      desc = null;
    }
    var txt = $E("textarea", {
      class: "form-control",
      id: name,
      name: name,
    });
    var row = this.addRow(label, name).append(txt);
    if (desc) {
      this.addDesc(row, txt, desc);
    }
    return txt;
  }
  addDesc($row: JQuery, $ctl: JQuery, desc: string | null) {
    var descid = `desc-${$ctl.attr("name") as string}`;
    $ctl.attr("aria-describedby", descid);
    var descctl = $E("div", {
      class: "form-text",
      id: descid,
    });
    if (desc) descctl.text(desc);
    $row.append(descctl);
  }
  addRow(label: string, name: string) {
    var row = $E("div", { class: "mb-3" });
    var lbl = $E("label", {
      class: "form-label",
      for: name,
    });
    lbl.appendTo(row);
    lbl.text(label);
    this.rows.push(row);
    row.appendTo(this.form);
    return row;
  }
  addSelect(
    label: string,
    name: string,
    data: Record<string, any>,
    desc: string | null = null
  ) {
    if (desc === void 0) {
      desc = null;
    }
    var sel = $E("select", {
      class: "form-select",
      "aria-label": label,
      name: name,
      id: name,
    });
    for (
      var _i = 0, _a = Array.from(Object.entries(data));
      _i < _a.length;
      _i++
    ) {
      var _b = _a[_i],
        k = _b[0],
        v = _b[1];
      var o = $E("option", { value: k });
      o.text(v);
      o.appendTo(sel);
    }
    var row = this.addRow(label, name).append(sel);
    if (desc) {
      this.addDesc(row, sel, desc);
    }
    return sel;
  }
}

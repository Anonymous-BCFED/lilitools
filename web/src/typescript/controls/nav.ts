import { $bsicon, $new, assert, assertNotNull } from "../utils";

export class _NavItem {
  container: NavBar;
  jqElem: JQuery | null;
  jqElemA: JQuery | null;
  text: string;
  active: boolean;
  disabled: boolean;
  onClick: (() => void) | null;
  constructor(container: NavBar) {
    this.container = container;
    this.jqElem = null;
    this.jqElemA = null;
    this.text = "";
    this.active = false;
    this.disabled = false;
    this.onClick = null;
    this.buildElems();
  }
  buildElems() {
    var _this = this;
    this.jqElem = $new("li").addClass("nav-item");
    if (this.active) {
      this.jqElem.addClass("active");
    }
    if (this.disabled) {
      this.jqElem.addClass("disabled");
    }
    this.jqElemA = $new("a")
      .addClass("nav-link")
      .on("click", function (target) {
        if (_this.disabled) {
          return;
        }
        _this.container.handleClick(_this, target);
        _this.onClick && _this.onClick();
      });
    return this.jqElemA.appendTo(this.jqElem);
  }
  setName(text: string) {
    assertNotNull(this.jqElemA, "jqElemA");
    this.text = text;
    this.jqElemA.text(this.text);
    this.buildElems();
  }
  getName() {
    return this.text;
  }
  setActive(active: boolean) {
    assertNotNull(this.jqElemA, "jqElemA");
    this.active = active;
    this.jqElemA.removeClass("active");
    if (this.active) {
      return this.jqElemA.addClass("active");
    }
  }
  getActive(): boolean {
    return this.active;
  }
  setDisabled(disabled: boolean) {
    assertNotNull(this.jqElemA, "jqElemA");
    this.disabled = disabled;
    this.jqElemA.removeClass("disabled");
    if (this.disabled) {
      return this.jqElemA.addClass("disabled");
    }
  }
  getDisabled() {
    return this.disabled;
  }
  setOnClick(onClick: () => void) {
    this.onClick = onClick;
  }
  click() {
    assertNotNull(this.jqElemA, "jqElemA");
    this.jqElemA.trigger("click");
  }
}
export class NavBar {
  jqElem: JQuery<HTMLElement>;
  parentElem: JQuery<HTMLElement>;
  items: _NavItem[];
  itemsByName: Record<string, _NavItem>;
  jqContainer: JQuery<HTMLElement> | null;
  div: JQuery<HTMLElement> | null;
  brand: JQuery<HTMLElement> | null;
  constructor($parentElem: JQuery) {
    this.parentElem = $parentElem;
    this.items = [];
    this.itemsByName = {};
    this.jqContainer = null;
    this.jqElem = this.makeNavHTML();
    this.parentElem.append(this.jqElem);
    this.div = null;
    this.brand = null;
  }
  addToContainer(tab: _NavItem) {
    assertNotNull(tab.jqElem);
    this.jqContainer?.append(tab.jqElem);
  }
  makeNavHTML(): JQuery {
    var nav = $new("nav")
      .addClass("navbar")
      .addClass("navbar-expand-lg")
      .addClass("navbar-dark")
      .addClass("bg-dark");
    this.div = $new("div").addClass("container-fluid");
    this.div.appendTo(nav);
    this.brand = $new("a").addClass("navbar-brand");
    this.brand.appendTo(this.div);
    this.brand.text("Slave Manager");
    // <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    //   <span class="navbar-toggler-icon"></span>
    // </button>
    var toggler = $new("button")
      .addClass("navbar-toggler")
      .attr("type", "button")
      .attr("data-bs-toggle", "collapse")
      .attr("data-bs-target", "#navbarNav")
      .attr("aria-controls", "navbarNav")
      .attr("aria-expanded", "false")
      .attr("aria-label", "Toggle navigation");
    toggler.appendTo(this.div);
    toggler.append($bsicon("chevron-right"));
    var divc = $new("div")
      .addClass("container")
      .addClass("navbar-collapse")
      .attr("id", "navbarNav");
    divc.appendTo(this.div);
    this.jqContainer = $new("ul").addClass("navbar-nav");
    this.jqContainer.appendTo(divc);
    return nav;
  }
  create(label: string, onClick: (() => void) | null) {
    if (onClick === void 0) {
      onClick = null;
    }
    var tab = new _NavItem(this);
    this.addToContainer(tab);
    tab.setName(label);
    if (onClick) {
      tab.setOnClick(onClick);
    }
    this.items.push(tab);
    this.itemsByName[label] = tab;
    return tab;
  }
  handleClick(tab: _NavItem, target: any) {
    this.items.forEach(function (x) {
      x.setActive(false);
    });
    tab.setActive(true);
  }
}

import { $new } from "../utils";
import { NavBar } from "./nav";

export class TabBar extends NavBar {
  makeNavHTML(): JQuery {
    this.jqContainer = $new("ul").addClass("nav").addClass("nav-tabs");
    return this.jqContainer;
  }
}

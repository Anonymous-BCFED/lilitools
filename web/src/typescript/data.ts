import $ from "jquery";

import { Room } from "./room";
import { SlaveInfo } from "./slave_info";
import { LoadingDialog } from "./controls/dialogs/loading";

let SAVEFILE = "";
let SLAVES: SlaveInfo[] = [];
let SLAVES_BY_ID: { [key: string]: SlaveInfo } = {};
let SLAVES_BY_NAME: { [key: string]: SlaveInfo } = {};

let ROOMS: Room[] = [];

export function getAllSlaves(): SlaveInfo[] {
  return SLAVES;
}

export function getSlaveByName(name: string): SlaveInfo {
  return SLAVES_BY_NAME[name];
}

export function getSlaveByID(id: string): SlaveInfo {
  return SLAVES_BY_ID[id];
}

export function getAllRooms(): Room[] {
  return ROOMS;
}

export function loadRooms(after: any): JQuery.jqXHR<any> {
  return $.get("/api/training_rooms", (response: any): void => {
    ROOMS = [];

    if (!response.ok) {
      return;
    }

    SAVEFILE = response["save-file"];

    for (var trdata of response.training_rooms) {
      var tr = new Room();
      tr.fromDict(trdata);
      ROOMS.push(tr);
    }

    after && after();
  });
}

export function loadSlaves(after: any): void {
  $.get("/api/slaves", (response: any): void => {
    SLAVES = [];
    SLAVES_BY_ID = {};
    SLAVES_BY_NAME = {};

    if (!response.ok) {
      return;
    }

    SAVEFILE = response["save-file"];

    for (var sdata of response.slaves) {
      var slave = new SlaveInfo();
      slave.fromDict(sdata);
      SLAVES.push(slave);
      SLAVES_BY_ID[slave.ID] = slave;
      SLAVES_BY_NAME[slave.Name] = slave;
    }

    after && after();
  });
}

export function reloadData(after: any) {
  let dlgLoading = LoadingDialog.instance();
  var st8 = window.currentPage.getState();
  dlgLoading.set(0, 2, "Loading training rooms...");
  return loadRooms(function () {
    dlgLoading.set(1, 2, "Loading slaves...");
    return loadSlaves(function () {
      dlgLoading.hide();
      window.currentPage.setState(st8);
      return after && after();
    });
  });
}
window.reloadData = reloadData;

export function getSavePath(): string {
  return SAVEFILE;
}

export function setSavePath(val: string) {
  SAVEFILE = val;
}

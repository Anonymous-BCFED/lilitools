import $ from "jquery";
import { $E } from "./utils";
import { Modal } from "bootstrap";
// Singleton
export class DynamicModal {
  static checkIfRebuildNeeded(mdid: string): boolean {
    return this.MODAL?.mdid != mdid;
  }
  mdid: string;
  title: JQuery | null;
  x: JQuery | null;
  body: JQuery | null;
  footer: JQuery | null;
  bsm: Modal | null;
  elem: any | null;
  static MODAL: DynamicModal | null = null;

  constructor(mdid: string) {
    this.mdid = mdid;
    this.title = null;
    this.x = null;
    this.body = null;
    this.footer = null;
    this.buildHTML();
    this.bsm = new Modal(this.elem.get()[0]);
  }
  buildHTML(): void {
    var body = $("body");
    this.elem = $E("div", {
      class: "modal",
      "tab-index": "-1",
    });
    this.elem.appendTo(body);
    var dlg = $E("div", { class: "modal-dialog" });
    dlg.appendTo(this.elem);
    var content = $E("div", { class: "modal-content" }).appendTo(dlg);
    var hdr = $E("div", { class: "modal-header" }).appendTo(content);
    this.title = $E("h5", { class: "modal-title" }).appendTo(hdr);
    this.setTitle("Header");
    // <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    this.x = $E("button", {
      type: "button",
      class: "btn-close",
      "data-bs-dismiss": "modal",
      "aria-label": "Close",
    }).appendTo(hdr);
    this.body = $E("div", { class: "modal-body" }).appendTo(content);
    this.footer = $E("div", { class: "modal-footer" }).appendTo(content);
    //@addToBody $ '<p>call <code>addToBody()</code> on DynamicModal.</p>'
  }
  setTitle(text: string): void {
    if (this.title != null) {
      this.title.text(text);
    }
  }
  addToBody($e: JQuery): void {
    if (this.body != null) {
      this.body.append($e);
    }
  }
  clearBody(): void {
    if (this.body != null) {
      this.body.html("");
    }
  }
  addToFooter(
    $e: string | JQuery.TypeOrArray<JQuery.Node | JQuery<JQuery.Node>>
  ): void {
    if (this.footer != null) {
      this.footer.append($e);
    }
  }
  clearFooter(): void {
    if (this.footer != null) {
      this.footer.html("");
    }
  }
  static instance(mdid: string) {
    if (!DynamicModal.MODAL) {
      DynamicModal.MODAL = new DynamicModal(mdid);
    }
    return DynamicModal.MODAL;
  }
  show(): void {
    if (this.bsm != null) {
      this.bsm.show();
    }
  }
  hide(): void {
    if (this.bsm != null) {
      this.bsm.hide();
    }
  }
}

enum EPronounType {
  FEMININE = 0,
  NEUTRAL = 1,
  MASCULINE = 2,
}

export class _GenderInfo {
  slang: string;
  hasPenis: boolean;
  hasVagina: boolean;
  hasBalls: boolean;
  pronouns: EPronounType;

  constructor(
    slang: string,
    hasPenis: boolean,
    hasVagina: boolean,
    hasBalls: boolean,
    pronouns: EPronounType
  ) {
    this.slang = slang;
    this.hasPenis = hasPenis;
    this.hasVagina = hasVagina;
    this.hasBalls = hasBalls;
    this.pronouns = pronouns;
  }

  getID(): string {
    let o: string[] = [this.pronouns.toString()[0]];
    if (this.hasPenis) {
      o.push("P");
    }
    if (this.hasVagina) {
      o.push("V");
    }
    if (this.hasBalls) {
      o.push("B");
    }
    o.push(this.slang.toUpperCase());
    return o.join("_");
  }
}

export class EGender {
  static M_P_V_B_HERMAPHRODITE: _GenderInfo = new _GenderInfo(
    "Hermaphrodite",
    true,
    true,
    true,
    EPronounType.MASCULINE
  );
  static M_P_V_HERMAPHRODITE: _GenderInfo = new _GenderInfo(
    "Hermaphrodite",
    true,
    true,
    false,
    EPronounType.MASCULINE
  );
  static M_P_B_BUSTYBOY: _GenderInfo = new _GenderInfo(
    "Bustyboy",
    true,
    false,
    true,
    EPronounType.MASCULINE
  );
  static M_P_MALE: _GenderInfo = new _GenderInfo(
    "Male",
    true,
    false,
    false,
    EPronounType.MASCULINE
  );
  static M_V_B_BUTCH: _GenderInfo = new _GenderInfo(
    "Butch",
    false,
    true,
    true,
    EPronounType.MASCULINE
  );
  static M_V_CUNTBOY: _GenderInfo = new _GenderInfo(
    "Cuntboy",
    false,
    true,
    false,
    EPronounType.MASCULINE
  );
  static M_B_MANNEQUIN: _GenderInfo = new _GenderInfo(
    "Mannequin",
    false,
    false,
    true,
    EPronounType.MASCULINE
  );
  static M_MANNEQUIN: _GenderInfo = new _GenderInfo(
    "Mannequin",
    false,
    false,
    false,
    EPronounType.MASCULINE
  );

  static F_P_V_B_FUTANARI: _GenderInfo = new _GenderInfo(
    "Futanari",
    true,
    true,
    true,
    EPronounType.FEMININE
  );
  static F_P_V_FUTANARI: _GenderInfo = new _GenderInfo(
    "Futanari",
    true,
    true,
    false,
    EPronounType.FEMININE
  );
  static F_P_B_SHEMALE: _GenderInfo = new _GenderInfo(
    "Shemale",
    true,
    false,
    true,
    EPronounType.FEMININE
  );
  static F_P_TRAP: _GenderInfo = new _GenderInfo(
    "Trap",
    true,
    false,
    false,
    EPronounType.FEMININE
  );
  static F_V_B_FEMALE: _GenderInfo = new _GenderInfo(
    "Female",
    false,
    true,
    true,
    EPronounType.FEMININE
  );
  static F_V_FEMALE: _GenderInfo = new _GenderInfo(
    "Female",
    false,
    true,
    false,
    EPronounType.FEMININE
  );
  static F_B_DOLL: _GenderInfo = new _GenderInfo(
    "Doll",
    false,
    false,
    true,
    EPronounType.FEMININE
  );
  static F_DOLL: _GenderInfo = new _GenderInfo(
    "Doll",
    false,
    false,
    false,
    EPronounType.FEMININE
  );

  static N_P_V_B_HERMAPHRODITE: _GenderInfo = new _GenderInfo(
    "Hermaphrodite",
    true,
    true,
    true,
    EPronounType.NEUTRAL
  );
  static N_P_V_HERMAPHRODITE: _GenderInfo = new _GenderInfo(
    "Hermaphrodite",
    true,
    true,
    false,
    EPronounType.NEUTRAL
  );
  static N_P_B_SHEMALE: _GenderInfo = new _GenderInfo(
    "Shemale",
    true,
    false,
    true,
    EPronounType.NEUTRAL
  );
  static N_P_TRAP: _GenderInfo = new _GenderInfo(
    "Trap",
    true,
    false,
    false,
    EPronounType.NEUTRAL
  );
  static N_V_B_TOMBOY: _GenderInfo = new _GenderInfo(
    "Tomboy",
    false,
    true,
    true,
    EPronounType.NEUTRAL
  );
  static N_V_TOMBOY: _GenderInfo = new _GenderInfo(
    "Tomboy",
    false,
    true,
    false,
    EPronounType.NEUTRAL
  );
  static N_B_DOLL: _GenderInfo = new _GenderInfo(
    "Doll",
    false,
    false,
    true,
    EPronounType.NEUTRAL
  );
  static N_NEUTER: _GenderInfo = new _GenderInfo(
    "Neuter",
    false,
    false,
    false,
    EPronounType.NEUTRAL
  );

  static ValueToString(
    val: _GenderInfo,
    sep: string | null = null,
    start_end: string | null = null
  ): string {
    if (sep == null) {
      sep = ", ";
    }
    if (start_end == null) {
      start_end = "";
    }
    let o = val.getID();
    if (start_end.length === 2) {
      o = start_end[0] + o + start_end[1];
    }
    return o;
  }

  static StringToValue(key: string): _GenderInfo | null {
    switch (key) {
      case "M_P_V_B_HERMAPHRODITE":
        return this.M_P_V_B_HERMAPHRODITE;
      case "M_P_V_HERMAPHRODITE":
        return this.M_P_V_HERMAPHRODITE;
      case "M_P_B_BUSTYBOY":
        return this.M_P_B_BUSTYBOY;
      case "M_P_MALE":
        return this.M_P_MALE;
      case "M_V_B_BUTCH":
        return this.M_V_B_BUTCH;
      case "M_V_CUNTBOY":
        return this.M_V_CUNTBOY;
      case "M_B_MANNEQUIN":
        return this.M_B_MANNEQUIN;
      case "M_MANNEQUIN":
        return this.M_MANNEQUIN;

      case "F_P_V_B_FUTANARI":
        return this.F_P_V_B_FUTANARI;
      case "F_P_V_FUTANARI":
        return this.F_P_V_FUTANARI;
      case "F_P_B_SHEMALE":
        return this.F_P_B_SHEMALE;
      case "F_P_TRAP":
        return this.F_P_TRAP;
      case "F_V_B_FEMALE":
        return this.F_V_B_FEMALE;
      case "F_V_FEMALE":
        return this.F_V_FEMALE;
      case "F_B_DOLL":
        return this.F_B_DOLL;
      case "F_DOLL":
        return this.F_DOLL;

      case "N_P_V_B_HERMAPHRODITE":
        return this.N_P_V_B_HERMAPHRODITE;
      case "N_P_V_HERMAPHRODITE":
        return this.N_P_V_HERMAPHRODITE;
      case "N_P_B_SHEMALE":
        return this.N_P_B_SHEMALE;
      case "N_P_TRAP":
        return this.N_P_TRAP;
      case "N_V_B_TOMBOY":
        return this.N_V_B_TOMBOY;
      case "N_V_TOMBOY":
        return this.N_V_TOMBOY;
      case "N_B_DOLL":
        return this.N_B_DOLL;
      case "N_NEUTER":
        return this.N_NEUTER;
    }
    return null;
  }

  static Keys(): string[] {
    return [
      "M_P_V_B_HERMAPHRODITE",
      "M_P_V_HERMAPHRODITE",
      "M_P_B_BUSTYBOY",
      "M_P_MALE",
      "M_V_B_BUTCH",
      "M_V_CUNTBOY",
      "M_B_MANNEQUIN",
      "M_MANNEQUIN",

      "F_P_V_B_FUTANARI",
      "F_P_V_FUTANARI",
      "F_P_B_SHEMALE",
      "F_P_TRAP",
      "F_V_B_FEMALE",
      "F_V_FEMALE",
      "F_B_DOLL",
      "F_DOLL",

      "N_P_V_B_HERMAPHRODITE",
      "N_P_V_HERMAPHRODITE",
      "N_P_B_SHEMALE",
      "N_P_TRAP",
      "N_V_B_TOMBOY",
      "N_V_TOMBOY",
      "N_B_DOLL",
      "N_NEUTER",
    ];
  }

  static Values(): _GenderInfo[] {
    return [
      this.M_P_V_B_HERMAPHRODITE,
      this.M_P_V_HERMAPHRODITE,
      this.M_P_B_BUSTYBOY,
      this.M_P_MALE,
      this.M_V_B_BUTCH,
      this.M_V_CUNTBOY,
      this.M_B_MANNEQUIN,
      this.M_MANNEQUIN,

      this.F_P_V_B_FUTANARI,
      this.F_P_V_FUTANARI,
      this.F_P_B_SHEMALE,
      this.F_P_TRAP,
      this.F_V_B_FEMALE,
      this.F_V_FEMALE,
      this.F_B_DOLL,
      this.F_DOLL,

      this.N_P_V_B_HERMAPHRODITE,
      this.N_P_V_HERMAPHRODITE,
      this.N_P_B_SHEMALE,
      this.N_P_TRAP,
      this.N_V_B_TOMBOY,
      this.N_V_TOMBOY,
      this.N_B_DOLL,
      this.N_NEUTER,
    ];
  }

  static Count(): number {
    return 24;
  }

  static Min(): _GenderInfo {
    return this.N_NEUTER;
  }

  static Max(): _GenderInfo {
    return this.M_P_V_B_HERMAPHRODITE;
  }
}

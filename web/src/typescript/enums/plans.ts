/**
 * @enumdef: EPlans
 */
export class EPlans {
  static readonly _DEFAULT: string = '';
  static readonly _ERROR: string = '';
  static readonly NONE: string = "";
  static readonly SELL: string = "s";
  static readonly KEEP: string = "k";
  static readonly RELEASE: string = "r";
  static readonly COW: string = "c";

  static ValueToString(
    val: string,
    sep: string = ", ",
    start_end: string = ""
  ): string {
    let o: string = "";
    switch (val) {
      case "":
        o = "NONE";
        break;
      case "s":
        o = "SELL";
        break;
      case "k":
        o = "KEEP";
        break;
      case "r":
        o = "RELEASE";
        break;
      case "c":
        o = "COW";
        break;
    }
    if (start_end.length == 1) {
      return start_end + o + start_end;
    }
    if (start_end.length == 2) {
      return start_end[0] + o + start_end[1];
    }
    return o
  }

  static StringToValue(key: string): string {
    switch (key) {
      case "NONE":
        return "";
      case "SELL":
        return "s";
      case "KEEP":
        return "k";
      case "RELEASE":
        return "r";
      case "COW":
        return "c";
    }
    return ""
  }

  static Keys(): string[] {
    return ["NONE", "SELL", "KEEP", "RELEASE", "COW"];
  }

  static Values(): string[] {
    return ["", "s", "k", "r", "c"];
  }

  static Count(): number {
    return 5;
  }
}


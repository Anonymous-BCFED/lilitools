/**
 * @enumdef: ERoomPurpose
 */
export class ERoomPurpose {
  static readonly _DEFAULT: number = 0;
  static readonly _ERROR: number = 0;
  static readonly TRAINING: number = 0;
  static readonly RECOVERY: number = 1;
  static readonly UNKNOWN: number = 2;
  static readonly COWSTALL: number = 3;
  static readonly DOLLSTORAGE: number = 4;
  static readonly COMPANIONS: number = 5;
  static readonly GUEST: number = 6;
  static readonly STAFF: number = 7;

  static ValueToString(
    val: number,
    sep: string = ", ",
    start_end: string = ""
  ): string {
    let o: string = "";
    switch (val) {
      case 0:
        o = "TRAINING";
        break;
      case 1:
        o = "RECOVERY";
        break;
      case 2:
        o = "UNKNOWN";
        break;
      case 3:
        o = "COWSTALL";
        break;
      case 4:
        o = "DOLLSTORAGE";
        break;
      case 5:
        o = "COMPANIONS";
        break;
      case 6:
        o = "GUEST";
        break;
      case 7:
        o = "STAFF";
        break;
    }
    if (start_end.length == 1) {
      return start_end + o + start_end;
    }
    if (start_end.length == 2) {
      return start_end[0] + o + start_end[1];
    }
    return o
  }

  static StringToValue(key: string): number {
    switch (key) {
      case "TRAINING":
        return 0;
      case "RECOVERY":
        return 1;
      case "UNKNOWN":
        return 2;
      case "COWSTALL":
        return 3;
      case "DOLLSTORAGE":
        return 4;
      case "COMPANIONS":
        return 5;
      case "GUEST":
        return 6;
      case "STAFF":
        return 7;
    }
    return 0
  }

  static Keys(): string[] {
    return ["TRAINING", "RECOVERY", "UNKNOWN", "COWSTALL", "DOLLSTORAGE", "COMPANIONS", "GUEST", "STAFF"];
  }

  static Values(): number[] {
    return [0, 1, 2, 3, 4, 5, 6, 7];
  }

  static Count(): number {
    return 8;
  }

  static Min(): number {
    return 0;
  }

  static Max(): number {
    return 7;
  }
  static Width(): number {
    return 3;
  }
}


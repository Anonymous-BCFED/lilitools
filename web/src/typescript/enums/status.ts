/**
 * @enumdef: EStatus
 */
export class EStatus {
  static readonly _DEFAULT: string = '-';
  static readonly _ERROR: string = '-';
  static readonly IDLE: string = "-";
  static readonly TRAINING: string = "t";
  static readonly MILKING: string = "m";
  static readonly WORKING: string = "w";
  static readonly REJOB: string = "r";

  static ValueToString(
    val: string,
    sep: string = ", ",
    start_end: string = ""
  ): string {
    let o: string = "";
    switch (val) {
      case "-":
        o = "IDLE";
        break;
      case "t":
        o = "TRAINING";
        break;
      case "m":
        o = "MILKING";
        break;
      case "w":
        o = "WORKING";
        break;
      case "r":
        o = "REJOB";
        break;
    }
    if (start_end.length == 1) {
      return start_end + o + start_end;
    }
    if (start_end.length == 2) {
      return start_end[0] + o + start_end[1];
    }
    return o
  }

  static StringToValue(key: string): string {
    switch (key) {
      case "IDLE":
        return "-";
      case "TRAINING":
        return "t";
      case "MILKING":
        return "m";
      case "WORKING":
        return "w";
      case "REJOB":
        return "r";
    }
    return "-"
  }

  static Keys(): string[] {
    return ["IDLE", "TRAINING", "MILKING", "WORKING", "REJOB"];
  }

  static Values(): string[] {
    return ["-", "t", "m", "w", "r"];
  }

  static Count(): number {
    return 5;
  }
}


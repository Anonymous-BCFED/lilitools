/**
 * @enumdef: ETreatment
 */
export class ETreatment {
  static readonly _DEFAULT: string = '-';
  static readonly _ERROR: string = '-';
  static readonly UNDECIDED: string = "-";
  static readonly DEGRADE: string = "d";
  static readonly UPLIFT: string = "u";

  static ValueToString(
    val: string,
    sep: string = ", ",
    start_end: string = ""
  ): string {
    let o: string = "";
    switch (val) {
      case "-":
        o = "UNDECIDED";
        break;
      case "d":
        o = "DEGRADE";
        break;
      case "u":
        o = "UPLIFT";
        break;
    }
    if (start_end.length == 1) {
      return start_end + o + start_end;
    }
    if (start_end.length == 2) {
      return start_end[0] + o + start_end[1];
    }
    return o
  }

  static StringToValue(key: string): string {
    switch (key) {
      case "UNDECIDED":
        return "-";
      case "DEGRADE":
        return "d";
      case "UPLIFT":
        return "u";
    }
    return "-"
  }

  static Keys(): string[] {
    return ["UNDECIDED", "DEGRADE", "UPLIFT"];
  }

  static Values(): string[] {
    return ["-", "d", "u"];
  }

  static Count(): number {
    return 3;
  }
}


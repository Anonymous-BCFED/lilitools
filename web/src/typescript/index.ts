import $ from "jquery";
// import { $E, $bsicon } from "./utils";
require("datatables.net-bs5");

import { getAllSlaves, getSavePath, reloadData } from "./data";
import { extendDT } from "./exts/dt-sorting";
import { NavBar } from "./controls/nav";
import { SlavesPage } from "./pages/slaves";
import { LoadingDialog } from "./controls/dialogs/loading";
import { UploadSaveDialog } from "./controls/dialogs/upload_save";
import { SlaveJobsPage } from "./pages/jobs";
import { SlaveJobsGanttPage } from "./pages/slave_jobs_gantt";
import { RoomsPage } from "./pages/rooms";
import { ToastManager } from "./toasts";
import { $E, $bsicon } from "./utils";

require("/src/scss/style.scss");
//import '/src/scss/datatables.scss'
require("bootstrap/dist/css/bootstrap.min.css");
require("bootstrap-icons/font/bootstrap-icons.css");
window.dumpSlaves = function () {
  return console.debug(getAllSlaves());
};
window.currentPage = null;
window.currentPageClass = null;

window.setPage = (pageClass: any, $container: JQuery): void => {
  var _inner = function () {
    window.currentPageClass = pageClass;
    window.currentPage = new pageClass($container);
    window.currentPage.build();
    window.currentPage.reload();
  };
  if (window.currentPage !== null) {
    window.currentPage.close(function () {
      window.currentPage = null;
      return _inner();
    });
  } else {
    _inner();
  }
};
$(() => {
  extendDT();
  window.slaveEditor = null;
  var $BODY = $(document.getElementsByTagName("body"));
  LoadingDialog.instance().setIndeterminate("LOADING");
  $("head").append(
    '<link rel="stylesheet" type="text/css" href="/userstyle.css">'
  );
  var $nav = new NavBar($BODY);
  window.navbar = $nav;
  window.toasts = new ToastManager($BODY);
  var $container = $E("div", {
    class: "container py-3",
    id: "pg-home",
  }).appendTo($BODY);
  window.currentPage = new SlavesPage($container);
  window.currentPage.build();
  $nav.create("Slaves", function () {
    window.setPage(SlavesPage, $container);
  });
  $nav.create("Training Rooms", function () {
    window.setPage(RoomsPage, $container);
  });
  //   $nav.create("Slave Details", () => {
  //     setPage(SlaveDetailsPage, $container);
  //   });
  $nav.create("Jobs", function () {
    window.setPage(SlaveJobsPage, $container);
  });
  $nav.create("Gantt", function () {
    window.setPage(SlaveJobsGanttPage, $container);
  });
  var tab = $nav.create("Reload", function () {
    window.currentPage.reload();
  });
  tab.jqElemA?.prepend($bsicon("arrow-clockwise"));
  $nav.create("Import", function () {
    return new UploadSaveDialog(getSavePath());
  });
  reloadData(function () {
    $("#loading-spinner").fadeOut({
      done: function () {
        document.getElementById("loading-style")?.remove();
        document.getElementById("loading-spinner")?.remove();
        window.currentPage.reload();
      },
    });
  });
});

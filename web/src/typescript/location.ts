import { assert, assertNotNull } from "./utils";

export class Point2I {
  x: number;
  y: number;
  constructor(
    x: number | undefined = undefined,
    y: number | undefined = undefined
  ) {
    this.x = x ?? 0;
    this.y = y ?? 0;
  }
}

const REG_WORLD_POS: RegExp = /([A-Za-z0-9_]+)@([0-9]+),([0-9]+)/;

export class Location {
  worldID: string;
  position: Point2I;

  constructor(
    worldId: string | undefined = undefined,
    position: Point2I | undefined = undefined
  ) {
    this.worldID = worldId ?? "";
    this.position = position ?? new Point2I(0, 0);
  }

  static FromDict(d: { [key: string]: any }): Location {
    return new Location(
      d["world"],
      new Point2I(d["position"][0], d["position"][1])
    );
  }

  static FromStr(v: string): Location {
    var matches: RegExpMatchArray|null = v.match(REG_WORLD_POS);
    assertNotNull(matches, "matches");
    
    var m = matches[0];
    return new Location(m[1], new Point2I(parseInt(m[2]), parseInt(m[3])));
  }

  toString() : string {
    return `${this.worldID} @ (${this.position.x}, ${this.position.y})`;
  }
}

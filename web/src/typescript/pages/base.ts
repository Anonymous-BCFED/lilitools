import $ from "jquery";
import { $E } from "../utils";
export class BasePage {
  container: JQuery<HTMLElement>;
  id: string;
  pageDivID: string;
  name: string;
  headerElem: JQuery<HTMLElement> | null;
  mainElem: JQuery<HTMLElement> | null;
  footerElem: JQuery<HTMLElement> | null;
  mainDivElem: JQuery<HTMLElement> | null;
  header: JQuery<HTMLElement> | null;
  constructor(container: JQuery) {
    this.destroyContent = this.destroyContent.bind(this);
    this.container = container;
    this.id = "";
    this.pageDivID = "pg-unknown";
    this.name = "";
    this.headerElem = null;
    this.mainElem = null;
    this.footerElem = null;
    this.mainDivElem = null;
    this.header = null;
  }
  close(whenDone: CallableFunction): void {
    var t = this;
    $("header, main, footer").fadeOut(function () {
      t.destroyContent();
      $("header, main, footer").remove();
      whenDone && whenDone();
    });
  }
  destroyContent(): void {}
  getPageDivClasses(): string {
    return "container py-3";
  }
  build(): void {
    this.container.removeClass();
    this.container.attr({
      id: this.pageDivID,
      class: this.getPageDivClasses(),
    });
    this.headerElem = $E("header").appendTo(this.container);
    this.buildHeader();
    this.mainElem = $E("main").appendTo(this.container);
    this.buildMainDiv();
    this.buildContent();
    this.buildFooter();
  }
  getMainDivClasses(): string {
    return "container py-3";
  }
  buildMainDiv(): void {
    if (this.mainElem == null) {
      throw new Error("mainElem not ready");
    }
    this.mainDivElem = $E("div", { class: this.getMainDivClasses() }).appendTo(
      this.mainElem
    );
  }
  getState(): Map<String, any> {
    return new Map<String, any>();
  }
  setState(data: Map<String, any>): void {}
  buildHeader(): void {}
  buildFooter(): void {
    var $copyline;
    this.footerElem = $E("footer",{'id':'boilerplate'})
      .addClass("pt-4")
      .addClass("my-md-5")
      .addClass("pt-md-5")
      .addClass("border-top")
      .appendTo(this.container);
    $copyline = $E("small")
      .addClass("d-block")
      .addClass("mb-3")
      .addClass("text-muted")
      .html(
        "lilitools &copy;2022-2024 LiliTools Contributors. Available under the MIT Open Source License."
      )
      .appendTo(this.footerElem);
  }
  buildContent(): void {}
  reload(): void {}
}

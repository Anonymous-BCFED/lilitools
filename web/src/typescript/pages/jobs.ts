import { getAllSlaves } from "../data";
import { SlaveInfo } from "../slave_info";
import { $E, $mkFontAwesome, $new, EFontAwesomeStyle, assertNotNull, range } from "../utils";
import { BasePage } from "./base";
import $ from "jquery";
import {
  ALL_JOBS,
  ID2JOB,
  JOB_BEDROOM,
  JOB_CLEANING,
  JOB_DINING_HALL,
  JOB_GARDEN,
  JOB_IDLE,
  JOB_KITCHEN,
  JOB_LAB_ASSISTANT,
  JOB_LIBRARY,
  JOB_MILKING,
  JOB_OFFICE,
  JOB_PROSTITUTE,
  JOB_PUBLIC_STOCKS,
  JOB_SECURITY,
  JOB_SPA,
  JOB_SPA_RECEPTIONIST,
  JOB_TEST_SUBJECT,
} from "../slave_jobs";

export class SlaveJobsPage extends BasePage {
  $table: JQuery | null;
  counts: { [job_id: string]: { [hour: number]: number } };
  rows: JQuery[];
  constructor(container: JQuery) {
    super(container);
    this.$table = null;
    this.counts = {};
    this.rows = [];
  }
  buildHeader(): void {
    if (this.headerElem == null) {
      throw new Error("headerElem not ready");
    }
    $new("h1").text("Slave Jobs").appendTo(this.headerElem);
  }
  reload(): void {
    console && console.log("SlaveJobsPage.reload()");
    this.rows = [];
    this.counts = {};
    $(".slave-job-row").remove();
    $(".slave-job-count-row").remove();
    $(".count-sep").remove();
    var slaves = getAllSlaves();
    slaves.sort(function (a: SlaveInfo, b: SlaveInfo) {
      if (a.Name > b.Name) {
        return 1;
      } else if (b.Name > a.Name) {
        return -1;
      } else {
        return 0;
      }
    });
    //return
    ALL_JOBS.forEach((j) => {
      slaves.forEach((s) => {
        if (s.PrimaryJob === j.id) {
          this.addSlave(s);
        }
      });
    });
    this.buildCounts();
  }
  buildCounts(): void {
    if (this.$table == null) throw new Error("$table is not ready");

    this.$table.append(
      $E("tr", { class: "count-sep" }).append($E("td", { colspan: "25" }))
    );
    for (var jid in this.counts) {
      var counts = this.counts[jid];
      var tr = $E("tr", { class: "slave-job-count-row job-".concat(jid) });
      tr.append($E("th", {}, "#/".concat(jid)));
      for (var i: number = 0; i < 24; i++) {
        var c: number = counts[i];
        tr.append(
          $E(
            "td",
            { class: `job-${jid} jobhour-col-${i}` },
            c > 0 ? `${c}` : ""
          )
        );
      }
      tr.appendTo(this.$table);
    }
  }
  addSlave(slave: SlaveInfo): void {
    assertNotNull(this.$table);
    var tr = $E("tr", { class: "slave-job-row" });
    tr.append($E("th", {}, slave.Name));
    for (var i = 0; i < 24; i++) {
      var h = i.toString().padStart(2, "0");
      var job = ID2JOB[slave.JobHours[i]];
      if (job === undefined) {
        job = JOB_IDLE;
      }
      var $cell = $E("td", { class: `job-${job.id} jobhour-col-${h}` }).append(
        (function () {
          switch (job) {
            case JOB_IDLE:
              return $mkFontAwesome('circle-minus', EFontAwesomeStyle.SOLID)
            case JOB_MILKING:
              return $mkFontAwesome("droplet", EFontAwesomeStyle.SOLID);
            case JOB_SECURITY:
              return $mkFontAwesome("shield-halved", EFontAwesomeStyle.SOLID);
            case JOB_BEDROOM:
              return $mkFontAwesome("bed", EFontAwesomeStyle.SOLID);
            case JOB_CLEANING:
              return $mkFontAwesome("broom", EFontAwesomeStyle.SOLID);
            case JOB_DINING_HALL:
              return $mkFontAwesome("utensils", EFontAwesomeStyle.SOLID);
            case JOB_GARDEN:
              return $mkFontAwesome("leaf", EFontAwesomeStyle.SOLID);
            case JOB_KITCHEN:
              return $mkFontAwesome("kitchen-set", EFontAwesomeStyle.SOLID);
            case JOB_LAB_ASSISTANT:
              return $mkFontAwesome("flask", EFontAwesomeStyle.SOLID);
            case JOB_LIBRARY:
              return $mkFontAwesome("book", EFontAwesomeStyle.SOLID);
            case JOB_OFFICE:
              return $mkFontAwesome("briefcase", EFontAwesomeStyle.SOLID);
            case JOB_PROSTITUTE:
              return $mkFontAwesome("smoking", EFontAwesomeStyle.SOLID);
            case JOB_PUBLIC_STOCKS:
              return $mkFontAwesome("handcuffs", EFontAwesomeStyle.SOLID);
            case JOB_SPA:
              return $mkFontAwesome("hot-tub-person", EFontAwesomeStyle.SOLID);
            case JOB_SPA_RECEPTIONIST:
              return $mkFontAwesome("bell-concierge", EFontAwesomeStyle.SOLID);
            case JOB_TEST_SUBJECT:
              return $mkFontAwesome("transgender", EFontAwesomeStyle.SOLID);
            default:
              return $mkFontAwesome("bug", EFontAwesomeStyle.SOLID);
          }
        })()
      );
      this.rows.push($cell);
      if (job !== JOB_IDLE) {
        if (!(job.id in this.counts)) {
          this.counts[job.id] = range(0, this.rows.length, false).map(function (
            j
          ) {
            return 0;
          });
        }
        this.counts[job.id][i]++;
      }
      $cell.appendTo(tr);
    }
    tr.appendTo(this.$table ?? "");
  }
  getJobTimeLabels(): JQuery {
    var o = $E("tr", { class: "slave-jobs-time-labels-row" });
    o.append($E("th").text(""));
    for (var i = 0; i < 24; i++) {
      o.append(
        $E("th", { class: "slave-jobs-time-label vertical-orientation" }).text(
          "".concat(i.toString().padStart(2, "0"), ":00")
        )
      );
    }
    return o;
  }
  buildContent(): void {
    assertNotNull(this.mainDivElem);
    $new("p")
      .text(
        "This shows how many slaves have a particular job set for a given time slot."
      )
      .appendTo(this.mainDivElem);
    var jobs = [];
    this.$table = $new("table")
      .append(this.getJobTimeLabels())
      .appendTo(this.mainDivElem);
    var slaves = getAllSlaves();
    slaves.sort(function (a, b) {
      if (a.Name > b.Name) {
        return 1;
      } else if (b.Name > a.Name) {
        return -1;
      } else {
        return 0;
      }
    });
    //return
    ALL_JOBS.forEach((j) => {
      slaves.forEach((s) => {
        if (s.PrimaryJob === j.id) {
          this.addSlave(s);
        }
      });
    });
    this.buildCounts();
  }
}
import $ from "jquery";
import {
  $E,
  $mkFontAwesome,
  $new,
  EFontAwesomeStyle,
  assertNotNull,
} from "../utils";
import { RoomEditor } from "../controls/dialogs/room_editor";
import { BasePage } from "./base";
import { Room } from "../room";
import { getAllRooms as getAllRooms, reloadData } from "../data";
import { ERoomPurpose } from "../enums/roompurpose";
export class RoomsPage extends BasePage {
  trainingRoomContainer: JQuery<HTMLElement> | null;
  recoveryRoomContainer: JQuery<HTMLElement> | null;
  roomCards: JQuery<HTMLElement>[];
  constructor(elem: JQuery) {
    super(elem);
    this.header = null;
    this.trainingRoomContainer = null;
    this.recoveryRoomContainer = null;
    this.roomCards = [];
  }
  buildHeader() {
    assertNotNull(this.headerElem);
    var $tb;
    $new("h1").text("Rooms").appendTo(this.headerElem);
    return ($tb = $E("div", {
      class: "btn-toolbar",
      role: "toolbar",
      "aria-label": "Controls for manipulating rooms.",
    })
      .appendTo(this.headerElem)
      .append(
        $E("div", {
          class: "btn-group",
          role: "group",
          "aria-label": "General controls",
        }).append(
          $E("button", {
            type: "button",
            class: "btn btn-success",
          })
            .on("click", function () {
              RoomEditor.display(null);
            })
            // .append($mk_fa_i("fas fa-plus fa-lg fa-fw"))
            .append(
              $mkFontAwesome("plus", EFontAwesomeStyle.SOLID)
                .addClass("fa-lg")
                .addClass("fa-fw")
            )
            .append(" Add")
        )
      ));
  }
  reload(): void {
    assertNotNull(this.trainingRoomContainer);
    assertNotNull(this.recoveryRoomContainer);
    var _this = this;
    console && console.log("TrainingRoomPage.reload()");
    this.roomCards.forEach(($card) => {
      $card.fadeOut(() => {
        $(this).remove();
      });
    });
    this.roomCards = [];
    this.trainingRoomContainer.html();
    this.recoveryRoomContainer.html();
    Array.from(getAllRooms()).map(function (room) {
      assertNotNull(_this.trainingRoomContainer);
      assertNotNull(_this.recoveryRoomContainer);
      if (room.Purpose == ERoomPurpose.TRAINING) {
        return _this.addRoomTile(room, _this.trainingRoomContainer);
      } else if (room.Purpose == ERoomPurpose.RECOVERY) {
        return _this.addRoomTile(room, _this.recoveryRoomContainer);
      }
    });
  }
  addRoomTile(room: Room, container: JQuery<HTMLElement>) {
    assertNotNull(container);
    return this.roomCards.push(
      $E("div", { class: "col" })
        .appendTo(container)
        .append(
          $E("div", { class: "card" })
            .css("width", "20rem")
            .append(
              $E("div", { class: "card-header d-flex" })
                .append(
                  $E("h5", { class: "flex-grow-1" })
                    .text(room.Name)
                    .css("display", "inline-block")
                  //.css('padding', '.375rem .375rem')
                )
                .append(
                  $E("div", {
                    class: "btn-group",
                    role: "group",
                  })
                    .append(
                      $E("button", {
                        type: "button",
                        class: "btn btn-primary btn-sm",
                      })
                        // .append($mk_fa_i("fas fa-edit"))
                        .append($mkFontAwesome("edit", EFontAwesomeStyle.SOLID))
                        .on("click", function () {
                          RoomEditor.display(room);
                        })
                    )
                    .append(
                      $E("button", {
                        type: "button",
                        class: "btn btn-danger btn-sm",
                      })
                        // .append($mk_fa_i("fas fa-times"))
                        .append(
                          $mkFontAwesome("times", EFontAwesomeStyle.SOLID)
                        )
                        .on("click", function () {
                          return $.post({
                            url: `/api/training_room/${room.WorldID}/${room.X}/${room.Y}/delete`,
                            method: "POST",
                          })
                            .done(function (response: any) {
                              if (response.ok) {
                                window.toasts.success("Success!");
                              } else {
                                window.toasts.danger(
                                  "Could not delete room: ".concat(
                                    response.message
                                  )
                                );
                                return;
                              }
                              reloadData(function () {
                                window.currentPage.reload();
                              });
                            })
                            .fail(function (data: any) {
                              var needle;
                              if (
                                !((needle = "message"),
                                Array.from(Object.keys(data)).includes(needle))
                              ) {
                                window.toasts.danger(
                                  "Failed to edit. See console."
                                );
                              } else {
                                window.toasts.danger(
                                  "Failed to edit slave: " + data.message
                                );
                              }
                            });
                        })
                    )
                    .css("margin-left", "1em")
                )
            )
            .append(
              $E("div", { class: "card-body" }).append(
                $E("div", { class: "container" })
                  .append(
                    $E("div", { class: "row" })
                      .append(
                        $E("div", { class: "col fw-bold text-end" }).text(
                          "World ID:"
                        )
                      )
                      .append(
                        $E("div", { class: "col" }).text(`${room.WorldID}`)
                      )
                  )
                  .append(
                    $E("div", { class: "row" })
                      .append(
                        $E("div", { class: "col fw-bold text-end" }).text("X:")
                      )
                      .append($E("div", { class: "col" }).text(`${room.X}`))
                  )
                  .append(
                    $E("div", { class: "row" })
                      .append(
                        $E("div", { class: "col fw-bold text-end" }).text("Y:")
                      )
                      .append($E("div", { class: "col" }).text(`${room.Y}`))
                  )
                  .append(
                    $E("div", { class: "row" })
                      .append(
                        $E("div", { class: "col fw-bold text-end" }).text(
                          "Purpose:"
                        )
                      )
                      .append(
                        $E("div", { class: "col" }).text(`${room.Purpose}`)
                      )
                  )
                  .append(
                    $E("div", { class: "row" })
                      .append(
                        $E("div", { class: "col fw-bold text-end" }).text(
                          "Beds:"
                        )
                      )
                      .append($E("div", { class: "col" }).text(`${room.Slots}`))
                  )
              )
            )
        )
    );
  }
  buildContent(): void {
    assertNotNull(this.mainDivElem, "mainDivElem");
    $E("h2").text("Training Rooms").appendTo(this.mainDivElem);
    $new("p")
      .text(
        "Training rooms are where slaves are trained to have maximum Obedience."
      )
      .appendTo(this.mainDivElem);
    $E("p").text("Each room should contain:").appendTo(this.mainDivElem);
    var ulUpgrades = $E("ul").appendTo(this.mainDivElem);
    $E("li", {}, "Dog bowls").appendTo(ulUpgrades);
    $E("li", {}, "Steel bed").appendTo(ulUpgrades);
    $E("li", {}, "Obedience trainer").appendTo(ulUpgrades);

    this.trainingRoomContainer = $E("div", { class: "row g-4" }).appendTo(
      this.mainDivElem
    );

    $E("h2").text("Recovery Rooms").appendTo(this.mainDivElem);
    $E("p")
      .text(
        "Recovery rooms are designed to optimized to train slaves to maximum Affection."
      )
      .appendTo(this.mainDivElem);
    $E("p").text("Each room should contain:").appendTo(this.mainDivElem);
    var ulUpgrades = $E("ul").appendTo(this.mainDivElem);
    $E("li", {}, "Room Service").appendTo(ulUpgrades);
    $E("li", {}, "Double-Size Bed").appendTo(ulUpgrades);
    this.recoveryRoomContainer = $E("div", { class: "row g-4" }).appendTo(
      this.mainDivElem
    );
  }
}

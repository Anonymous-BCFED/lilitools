import { RoomEditor } from "../controls/dialogs/room_editor";
import { getAllRooms, reloadData } from "../data";
import { Room } from "../room";
import {
  $E,
  $mkFontAwesome,
  $new,
  EFontAwesomeStyle,
  assertNotNull,
} from "../utils";
import { BasePage } from "./base";
import $ from "jquery";
export class SlaveDetailsPage extends BasePage {
  static roomEditor: RoomEditor;
  roomContainer: JQuery<HTMLElement> | null;
  selectedSlave: JQuery<HTMLElement> | null;
  roomCards: JQuery<HTMLElement>[];
  constructor(elem: JQuery) {
    super(elem);
    this.header = null;
    this.roomContainer = null;
    this.selectedSlave = null;
    this.roomCards = [];
  }
  buildHeader() {
    assertNotNull(this.headerElem);
    var $tb;
    $new("h1").text("Slave Details").appendTo(this.headerElem);
    $tb = $E("div", {
      class: "btn-toolbar",
      role: "toolbar",
      "aria-label": "Detailed information on a given slave.",
    })
      .appendTo(this.headerElem)
      .append(
        $E("div", {
          class: "btn-group",
          role: "group",
          "aria-label": "General controls",
        }).append(
          $E("button", {
            type: "button",
            class: "btn btn-success",
          })
            .on("click", function () {
              RoomEditor.display(null);
            })
            // .append($mk_fa_i("fas fa-plus fa-lg fa-fw"))
            .append(
              $mkFontAwesome("plus", EFontAwesomeStyle.SOLID)
                .addClass("fa-lg")
                .addClass("fa-fw")
            )
            .append(" Add")
        )
      );
  }
  reload(): void {
    assertNotNull(this.roomContainer, "roomContainer");
    var _this = this;
    console && console.log("TrainingRoomPage.reload()");
    for (var _i = 0, _a = Array.from(this.roomCards); _i < _a.length; _i++) {
      var $card = _a[_i];
      $card.fadeOut(function () {
        $(this).remove();
      });
    }
    this.roomCards = [];
    this.roomContainer.html();
    getAllRooms().forEach((room) => {
      _this.addRoomTile(room);
    });
  }
  addRoomTile(room: Room): void {
    assertNotNull(this.roomContainer, "roomContainer");
    this.roomCards.push(
      $E("div", { class: "col" })
        .appendTo(this.roomContainer)
        .append(
          $E("div", { class: "card" })
            .css("width", "20rem")
            .append(
              $E("div", { class: "card-header d-flex" })
                .append(
                  $E("h5", { class: "flex-grow-1" })
                    .text(room.Name)
                    .css("display", "inline-block")
                  //.css 'padding', '.375rem .375rem'
                )
                .append(
                  $E("div", {
                    class: "btn-group",
                    role: "group",
                  })
                    .append(
                      $E("button", {
                        type: "button",
                        class: "btn btn-primary btn-sm",
                      })
                        // .append($mk_fa_i("fas fa-edit"))
                        .append($mkFontAwesome("edit", EFontAwesomeStyle.SOLID))
                        .on("click", function () {
                          RoomEditor.display(room);
                        })
                    )
                    .append(
                      $E("button", {
                        type: "button",
                        class: "btn btn-danger btn-sm",
                      })
                        // .append($mk_fa_i("fas fa-times"))
                        .append(
                          $mkFontAwesome("times", EFontAwesomeStyle.SOLID)
                        )
                        .on("click", function () {
                          return $.post({
                            url: `/api/training_room/${room.WorldID}/${room.X}/${room.Y}/delete`,
                            method: "POST",
                          })
                            .done(function (response) {
                              if (response.ok) {
                                window.toasts.success("Success!");
                              } else {
                                window.toasts.danger(
                                  "Could not delete room: ".concat(
                                    response.message
                                  )
                                );
                                return;
                              }
                              reloadData(function () {
                                window.currentPage.reload();
                              });
                            })
                            .fail(function (data: any) {
                              var needle;
                              if (
                                !((needle = "message"),
                                Array.from(Object.keys(data)).includes(needle))
                              ) {
                                window.toasts.danger(
                                  "Failed to edit. See console."
                                );
                              } else {
                                window.toasts.danger(
                                  "Failed to edit slave: " + data.message
                                );
                              }
                            });
                        })
                    )
                    .css("margin-left", "1em")
                )
            )
            .append(
              $E("div", { class: "card-body" }).append(
                $E("div", { class: "container" })
                  .append(
                    $E("div", { class: "row" })
                      .append(
                        $E("div", { class: "col fw-bold text-end" }).text(
                          "Room Name:"
                        )
                      )
                      .append(
                        $E("div", { class: "col" }).text(room.Name)
                      )
                  )
                  .append(
                    $E("div", { class: "row" })
                      .append(
                        $E("div", { class: "col fw-bold text-end" }).text(
                          "Beds:"
                        )
                      )
                      .append(
                        $E("div", { class: "col" }).text(room.Slots.toString())
                      )
                  )
              )
            )
        )
    );
  }
  buildContent(): void {
    assertNotNull(this.mainDivElem);
    $new("p")
      .text(
        "Training rooms are where slaves are trained to have maximum Obedience.  Each room should have dog bowls, a steel bed, and an obedience trainer."
      )
      .appendTo(this.mainDivElem);
    /*
        <div class="card" style="width: 18rem;">
            <div class="card-header">
                Featured
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">An item</li>
                <li class="list-group-item">A second item</li>
                <li class="list-group-item">A third item</li>
            </ul>
        </div>
        */
    this.roomContainer = $E(
      "div",
      //<div class="row row-cols-1 row-cols-md-3 g-4">
      //'class': 'row row-cols-1 row-cols-md-3 g-4'
      { class: "row g-4" }
    ).appendTo(this.mainDivElem);
  }
}

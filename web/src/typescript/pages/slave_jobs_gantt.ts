import $ from "jquery";
import { BasePage } from "./base";
import {
  $E,
  $new,
  assertNotNull,
  setCSSVariable,
} from "../utils";
import { SlaveInfo } from "../slave_info";
import { ALL_JOBS, ID2JOB, JOB_IDLE } from "../slave_jobs";
import { getAllSlaves } from "../data";
export class SlaveJobsGanttPage extends BasePage {
  headers: JQuery<HTMLElement>[];
  rows: JQuery<HTMLElement>[];
  $container: JQuery<HTMLElement> | null;
  $chart: JQuery<HTMLElement> | null;
  counts: {};
  maxNameWidth: number;
  names_header: JQuery<HTMLElement> | null;
  constructor(elem: JQuery) {
    super(elem);
    this.headers = [];
    this.rows = [];
    this.$container = null;
    this.$chart = null;
    this.counts = {};
    this.maxNameWidth = 0;
    this.names_header = null;
  }
  buildHeader(): void {
    assertNotNull(this.headerElem, "headerElem");
    $new("h1").text("Slave Jobs").appendTo(this.headerElem);
  }
  reload(): void {
    console && console.log("SlaveJobsGanttPage.reload()");
    this.rows = [];
    this.counts = {};
    this.rebuildData();
  }
  addSlave(slave: SlaveInfo, pregen: boolean) {
    var i;
    var [row, id] = this.createRow();
    id.text(slave.getName());
    if (pregen) {
      this.maxNameWidth = Math.max(this.maxNameWidth, id.width() ?? 0);
      return;
    }
    var hdr = this.headers[23];
    id.css({
      width: hdr.position().left + (hdr.width() ?? 0) - id.position().left,
    });
    var ul = $E("ul", {});
    row.append(ul);
    var cur_job = null;
    var job_li: JQuery | null = null;
    var start = 0;
    for (i = 0; i < 24; i++) {
      var h = (i * 100).toString().padStart(4, "0");
      var job = ID2JOB[slave.JobHours[i]];
      if (job === undefined) {
        job = JOB_IDLE;
      }
      if (i === 0 || cur_job !== job) {
        if (cur_job !== null && job_li !== null) {
          hdr = this.headers[i - 1];
          job_li.css({
            width:
              hdr.position().left + (hdr.width() ?? 0) - job_li.position().left,
          });
          job_li.text(job_li.text());
        }
        cur_job = job;
        hdr = this.headers[i];
        job_li = $E(
          "li",
          { class: `item job-${job.id} jobhour-col-${i + 1}` },
          job.id
        );
        ul.append(job_li);
        job_li.css({ left: hdr.position().left - job_li.position().left });
        start = i;
      }
    }

    if (cur_job !== null && job_li !== null) {
      hdr = this.headers[23];
      job_li.css({
        width:
          hdr.position().left + (hdr.width() ?? 0) - job_li.position().left,
      });
      job_li.text(`${job_li.text()} ${start}-${i}`);
    }
    // tr.appendTo @$table
  }
  addHeaders(): void {
    assertNotNull(this.$chart, "$chart");

    var headers = $E("ul", { class: "header-row" });
    this.names_header = $E("li", {}, "");
    headers.append(this.names_header);
    for (var i = 0; i < 24; i++) {
      var h = (i * 100).toString().padStart(4, "0");
      var hdr = $E("li", { class: "header" }, "".concat(h));
      headers.append(hdr);
      this.headers.push(hdr);
    }
    headers.appendTo(this.$chart);
  }
  createRow(): [row: JQuery<HTMLElement>, id: JQuery<HTMLElement>] {
    assertNotNull(this.$chart, "$chart");
    var o = $E("ul", { class: "item-row" });
    o.appendTo(this.$chart);
    var rowheader = $E("li", { class: "header" });
    o.append(rowheader);
    this.rows.push(o);
    return [o, rowheader];
  }
  destroyContent(): void {
    if (this.$chart) this.$chart.remove();
  }
  buildContent(): void {
    assertNotNull(this.mainDivElem, "mainDivElem");
    $new("p")
      .text(
        "This shows how many slaves have a particular job set for a given time slot."
      )
      .appendTo(this.mainDivElem);
    var jobs = [];
    this.$chart = $E("div", { class: "gantt-chart" });
    this.$chart.appendTo(this.mainDivElem);
    this.addHeaders();
    this.rebuildData();
  }
  rebuildData() {
    assertNotNull(this.names_header, "names_header");
    var s;
    this.maxNameWidth = 0;
    for (var _i = 0, _a = Array.from(getAllSlaves()); _i < _a.length; _i++) {
      s = _a[_i];
      this.addSlave(s, true);
    }
    this.names_header.width(this.maxNameWidth);
    $("ul.item-row").remove();
    var slaves = getAllSlaves();
    slaves.sort(function (a: SlaveInfo, b: SlaveInfo): number {
      if (a.Name > b.Name) {
        return 1;
      } else if (b.Name > a.Name) {
        return -1;
      } else {
        return 0;
      }
    });
    ALL_JOBS.forEach((j) => {
      slaves.forEach((s) => {
        if (s.PrimaryJob === j.id) {
          this.addSlave(s, false);
        }
      });
    });
    //@buildCounts()
    var lastrow = this.rows.slice(-1)[0].last().last();
    console.log(lastrow);
    setCSSVariable(
      "--divider-height",
      `${lastrow.position().top + (lastrow.height() ?? 0)}px`
    );
  }
}

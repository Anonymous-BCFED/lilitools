import $ from "jquery";
import { BasePage } from "./base";
import { $bsicon, $E, $new, assertNotNull, numberWithCommas } from "../utils";
import { getAllSlaves, getAllRooms, getSlaveByID } from "../data";
import { EPlans } from "../enums/plans";
import { EStatus } from "../enums/status";
import { TabBar } from "../controls/tabs";
import { SlaveEditor } from "../controls/dialogs/slave_editor";
import { SlaveInfo } from "../slave_info";
import { CURRENCY_SYMBOL, SLAVEID_SYMBOL } from "../consts";
import DataTable, { Api } from "datatables.net";
import { _NavItem } from "../controls/nav";
import { ToastManager } from "../toasts";
import { SpeciesInfo } from "../species_info";

function selectSlave(slaveID: string) {
  SlaveEditor.display(getSlaveByID(slaveID));
}

enum ECurrentTab {
  MASTER_LIST,
  TRAINING,
  MILKING,
  SELLING,
  RELEASING,
}
type TTabTable = {
  [t: number]: _NavItem;
};
const COL_INDEX: number = 0;
const COL_SID: number = 1;
const COL_ID: number = 2;
const COL_NAME: number = 3;
const COL_SPECIES: number = 4;
const COL_GENDER_IDENTITY: number = 5;
const COL_DETECTED_GENDER: number = 6;
const COL_JOBS: number = 7;
const COL_HOME: number = 8;
const COL_LOCATION: number = 9;
const COL_AFFECTION: number = 10;
const COL_OBEDIENCE: number = 11;
const COL_VALUE: number = 12;
const COL_VALUE_PER_DAY: number = 13;
const COL_PLANS: number = 14;
const COL_STATUS: number = 15;
const COL_TREATMENT: number = 16;
const COL_TAGS: number = 17;
const COL_NOTES: number = 18;

var CURRENT_SORT: any | null = null;
var CURRENT_TAB = ECurrentTab.MASTER_LIST;
var LAST_SORT: any | null = null;
var LAST_TAB = ECurrentTab.MASTER_LIST;

var TAB_MASTER_LIST: _NavItem | null = null;
var TAB_MILKING: _NavItem | null = null;
var TAB_RELEASING: _NavItem | null = null;
var TAB_SELLING: _NavItem | null = null;
var TAB_TRAINING: _NavItem | null = null;

function buildReport(filter: ((slave: SlaveInfo) => boolean) | null) {
  var o: any[] = [];
  getAllSlaves().forEach((slave) => {
    if (filter === null || filter(slave)) {
      o.push(slave.toRow());
    }
  });
  return o;
}
function buildTrainingReport(): string[][] {
  var rooms: { [key: string]: (SlaveInfo | null)[] } = {};
  var trainingRooms = getAllRooms();
  trainingRooms.forEach((room) => {
    rooms[room.Name] = Array.from(Array(room.Slots)).fill(null);
  });
  getAllSlaves().forEach((slave) => {
    var needle;
    var home = slave.HomeLocationName;
    if (((needle = home), Array.from(Object.keys(rooms)).includes(needle))) {
      for (var i = 0; i < rooms[home].length; i++) {
        if (rooms[home][i] === null) {
          rooms[home][i] = slave;
          break;
        }
      }
    }
  });
  var rows: any[][] = [];
  trainingRooms.forEach((room) => {
    for (var i = 0; i < room.Slots; i++) {
      var slave: SlaveInfo | null;
      var row = [];
      if ((slave = rooms[room.Name][i]) === null) {
        row = Array.from(Array(SlaveInfo.COLUMNS.length)).fill("");
      } else {
        row = slave.toRow();
      }
      row[COL_HOME] = `${room.Name} (#${i + 1})`;
      rows.push(row);
    }
  });
  return rows;
}
function getSlaveTable() {
  return window.currentPage.table;
}
export class SlavesPage extends BasePage {
  tabs: TTabTable;
  table: Api<any> | null;
  constructor(elem: JQuery) {
    super(elem);
    this.pageDivID = "pg-slaves";
    this.header = null;
    this.tabs = {} as TTabTable;
    this.table = null;
  }
  setHeader(text: string) {
    assertNotNull(this.header, "header");
    this.header.text(text);
  }
  makeTabHandler(sortBy: [number, string], reportBuilder: () => any[][]) {
    let _this = this;
    return () => {
      _this.reconfigureTable(sortBy, reportBuilder);
    };
  }
  reconfigureTable(sortBy: [number, string], reportBuilder: () => any[][]) {
    //console.log sortBy, reportBuilder
    //console.log reportBuilder()
    assertNotNull(this.table, "table");
    this.table.clear();
    this.table.rows.add(reportBuilder());
    this.table.order(sortBy).draw();
    CURRENT_SORT = sortBy;
  }
  buildAccordionList(id: string): JQuery<HTMLElement> {
    var $accordions = $E("div", { id: id, class: "accordion" });
    return $accordions;
  }
  buildAccordionItem(
    $parent: JQuery<HTMLElement>,
    id_suffix: string,
    header_text: string,
    $content: JQuery<HTMLElement>,
    collapsed: boolean = false
  ): JQuery<HTMLElement> {
    var idItem = "accordion-item-" + id_suffix;
    var idHeader = "accordion-header-" + id_suffix;
    var idCollapse = "accordion-collapse-" + id_suffix;
    var $accordionItem = $E("div", {
      class: "accordion-item",
      id: idItem,
    });
    var btnClasses = ["accordion-button"];
    var collapseClasses = ["accordion-collapse", "collapse"];
    if (collapsed) {
      btnClasses.push("collapsed");
    } else {
      collapseClasses.push("show");
    }
    // <h2 class="accordion-header" id="headingOne">
    //   <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
    //     Accordion Item #1
    //   </button>
    // </h2>
    $accordionItem.append(
      $E("h2", {
        class: "accordion-header",
        id: idHeader,
      }).append(
        $E(
          "button",
          {
            class: btnClasses.join(" "),
            type: "button",
            "data-bs-toggle": "collapse",
            "data-bs-target": "#" + idCollapse,
            "aria-expanded": "true",
            "aria-controls": idCollapse,
          },
          header_text
        )
      )
    );
    // <div
    //   id="collapseOne"
    //   class="accordion-collapse collapse show"
    //   aria-labelledby="headingOne"
    //   data-bs-parent="#accordionExample"
    // >
    //   <div class="accordion-body">
    //     <strong>This is the first item's accordion body.</strong> It is shown by
    //     default, until the collapse plugin adds the appropriate classes that we
    //     use to style each element. These classes control the overall appearance,
    //     as well as the showing and hiding via CSS transitions. You can modify
    //     any of this with custom CSS or overriding our default variables. It's
    //     also worth noting that just about any HTML can go within the{" "}
    //     <code>.accordion-body</code>, though the transition does limit overflow.
    //   </div>
    // </div>;
    $accordionItem.append(
      $E("div", {
        id: idCollapse,
        class: collapseClasses.join(" "),
        "aria-labelledby": idHeader,
        "data-bs-parent": "#" + $parent.attr("id"),
      }).append($E("div", { class: "accordion-body" }).append($content))
    );
    return $accordionItem;
  }
  buildHeader(): void {
    var _this = this;
    assertNotNull(this.headerElem, "headerElem");
    $new("h1").text("Slaves").appendTo(this.headerElem);

    var $accordionList = this.buildAccordionList("options-list");
    var formcheck = $E("div", { class: "form-check" });
    var chkShowIDs = $E("input", {
      type: "checkbox",
      class: "form-check-input",
      name: "chkShowLTIDs",
      id: "chkShowLTIDs",
      "aria-describedby": "chkShowLTIDsDesc",
    });
    chkShowIDs.appendTo(formcheck);
    $new("label")
      .text("Show LT IDs")
      .addClass("form-check-label")
      .attr("for", "chkShowLTIDs")
      .appendTo(formcheck);
    $new("span")
      .attr("id", "chkShowLTIDsDesc")
      .addClass("form-text")
      .text(" Shows Lilith's Throne's internal IDs for each character.")
      .appendTo(formcheck);
    var $accordionItem = this.buildAccordionItem(
      $accordionList,
      "options",
      "Options",
      formcheck,
      true
    );
    $accordionList.append($accordionItem).appendTo(this.headerElem);

    chkShowIDs.on("click", (chk) => {
      if (chkShowIDs.is(":checked")) {
        // $("#table-slaves").addClass("hide-ids");
        this.table?.column(2).visible(true);
      } else {
        this.table?.column(2).visible(false);
      }
    });
    chkShowIDs.prop("checked", false);
    var tabs = new TabBar(this.headerElem);
    TAB_MASTER_LIST = tabs.create(
      "Master List",
      this.makeTabHandler([COL_NAME, "asc"], function () {
        CURRENT_TAB = ECurrentTab.MASTER_LIST;
        CURRENT_SORT = null;
        _this.setHeader("Master List");
        return buildReport(null);
      })
    );
    TAB_TRAINING = tabs.create(
      "Training",
      this.makeTabHandler([COL_OBEDIENCE, "desc"], function () {
        CURRENT_TAB = ECurrentTab.TRAINING;
        CURRENT_SORT = null;
        _this.setHeader("Training");
        return buildTrainingReport();
      })
    );
    TAB_MILKING = tabs.create(
      "Milking",
      this.makeTabHandler([COL_VALUE, "desc"], function () {
        CURRENT_TAB = ECurrentTab.MILKING;
        CURRENT_SORT = null;
        _this.setHeader("Milking");
        return buildReport(function (slave) {
          return slave.Status === EStatus.MILKING;
        });
      })
    );
    TAB_SELLING = tabs.create(
      "Selling",
      this.makeTabHandler([COL_VALUE, "desc"], function () {
        CURRENT_TAB = ECurrentTab.SELLING;
        CURRENT_SORT = null;
        _this.setHeader("Selling");
        return buildReport(function (slave) {
          return slave.Plans === EPlans.SELL;
        });
      })
    );
    TAB_RELEASING = tabs.create(
      "Releasing",
      this.makeTabHandler([COL_AFFECTION, "desc"], function () {
        CURRENT_TAB = ECurrentTab.RELEASING;
        CURRENT_SORT = null;
        _this.setHeader("Releasing");
        return buildReport(function (slave) {
          return slave.Plans === EPlans.RELEASE;
        });
      })
    );
    this.tabs = {
      [ECurrentTab.MASTER_LIST]: TAB_MASTER_LIST,
      [ECurrentTab.TRAINING]: TAB_TRAINING,
      [ECurrentTab.MILKING]: TAB_MILKING,
      [ECurrentTab.SELLING]: TAB_SELLING,
      [ECurrentTab.RELEASING]: TAB_RELEASING,
    };
  }
  getState(): Map<String, any> {
    return new Map<String, any>([
      ["tab", CURRENT_TAB],
      ["sort", this.table?.order()],
    ]);
  }
  setState(data: Map<String, any>): void {
    var tab: ECurrentTab | null = null;
    if (data.has("tab")) {
      this.tabs[data.get("tab")].click();
    }
    if (data.has("sort") && this.table != null && data.get("sort") != null) {
      this.table.order(data.get("sort")).draw();
    }
  }
  reload(): void {
    var state = this.getState();
    this.tabs[ECurrentTab.MASTER_LIST].click();
    this.setState(state);
  }
  destroyContent() {
    return this.table?.destroy(false);
  }
  // getPageDivClasses(): string {
  //   return "container py-3"; // Skip container
  // }
  // getMainDivClasses(): string {
  //   return "container py-3"; // Skip container
  // }
  buildContent() {
    assertNotNull(this.mainDivElem, "mainDivElem");
    this.header = $E("h2").text("...").appendTo(this.mainDivElem);
    var $table = $E("table", {
      class: "table table-striped",
      id: "table-slaves",
    }).appendTo(this.mainDivElem);
    var $thead = $new("thead").appendTo($table);
    var $tr = $new("tr").appendTo($thead);
    SlaveInfo.COLUMNS.forEach((column) => {
      $new("th").text(column).attr("scope", "column").appendTo($tr);
    });
    $new("tbody").appendTo($table);
    this.table = new DataTable($table, {
      paging: false,
      //order: [[sortColumn, ascOrDesc]]
      columnDefs: [
        {
          targets: COL_SPECIES,
          type: "lili-species",
          className: "col-species",
          render: function (data: SpeciesInfo, type: String, row: any) {
            if (data === null) {
              return "NULL";
            } else {
              return `<span x-species-race="${data.Race}" x-species-stage="${data.RaceStage}" x-species-subspecies="${data.Subspecies}">${data.Label}</span>`;
            }
          },
        },
        {
          targets: COL_VALUE,
          type: "lili-currency",
          className: "col-value",
          render: function (data: number, type: String, row: any) {
            if (type === "sort") {
              return data;
            }
            if (data === null) {
              return "NULL";
            } else {
              return `<span class="currency">${numberWithCommas(data)}</span>`;
            }
          },
        },
        {
          targets: COL_VALUE_PER_DAY,
          type: "lili-currency",
          className: "col-value-per-day",
          render: function (data: number, type: String, row: any) {
            if (type === "sort") {
              return data;
            }
            if (data === null) {
              return "NULL";
            } else {
              return `<span class="currency">${numberWithCommas(
                data
              )}</span>/day`;
            }
          },
        },
        {
          targets: COL_ID,
          type: "lili-charid",
          className: "col-id",
          visible: false,
          render: function (data) {
            if (data === null) {
              return "NULL";
            } else {
              return ""
                .concat(data)
                .concat('<i class="fa-regular fa-copy"></i>');
            }
          },
        },
        {
          targets: COL_SID,
          type: "lili-slaveid",
          className: "col-sid",
          render: function (data) {
            if (data === null) {
              return "NULL";
            } else {
              return ""
                .concat(data)
                .concat('<i class="fa-regular fa-copy"></i>');
            }
          },
        },
      ],
    });
    $table
      .find("tbody")
      .on("click", "tr", function () {
        selectSlave(getSlaveTable().row(this).data()[COL_ID]);
      })
      .on("click", "td.col-id", function (event: JQuery.Event) {
        let sid = getSlaveTable().row(this).data()[COL_ID];
        navigator.clipboard.writeText(sid);
        window.toasts.success(`Copied ${sid} to clipboard!`);
        event.stopPropagation();
      })
      .on("click", "td.col-sid", function (event: JQuery.Event) {
        let sid = getSlaveTable().row(this).data()[COL_SID];
        navigator.clipboard.writeText(sid);
        window.toasts.success(`Copied ${sid} to clipboard!`);
        event.stopPropagation();
      });
    this.table.on("order.dt", () => {
      CURRENT_SORT = this.table?.order();
      console.log("order=", CURRENT_SORT);
    });
    $table.addClass("hide-ids");
  }
}

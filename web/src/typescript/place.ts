export class Place {
    ID:String;
    Icon:String;
    Name:String;
    Classes:String[];

    constructor(){
        this.ID="";
        this.Icon="";
        this.Name="";
        this.Classes=[];
    }

    fromDict(data:Record<string,any>):void{
        this.ID = data["id"];
        this.Name = data["name"];
        this.Icon = data["svgString"];
        this.Classes=[];
        
    }
}
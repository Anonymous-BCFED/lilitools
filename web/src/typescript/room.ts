import { ERoomPurpose } from "./enums/roompurpose";
import { $E } from "./utils";

export class Room {
  WorldID: string;
  X: number;
  Y: number;
  Name: string;
  Slots: number;
  Purpose: ERoomPurpose;
  Type: string;

  mapElement: Element | null;
  constructor() {
    this.WorldID = "";
    this.X = 0;
    this.Y = 0;
    this.Name = "";
    this.Slots = 0;
    this.Purpose = ERoomPurpose.TRAINING;
    this.Type = "";

    this.mapElement = null;
  }
  fromDict(data: Record<string, any>) {
    this.WorldID = data["world-id"];
    this.X = parseInt(data.pos[0]);
    this.Y = parseInt(data.pos[1]);
    this.Name = data.name;
    this.Slots = parseInt(data.slots);
    this.Purpose = data.purpose as ERoomPurpose;
    this.Type = data.type;
  }
  getClassesForMap(): String[] {
    var o: string[] = ["room"];
    var bgcolor: String = "";
    var icon: String = "";
    // switch (this.Type) {
    //   case "GENERIC_IMPASSIBLE":
    //     bgcolor = "BASE_GREY";
    //     break;
    //   case "GENERIC_EMPTY_TILE":
    //     bgcolor = "BASE_CRIMSON";
    //     icon = "icon-slaveralley";
    //     break;
    //   case "GENERIC_HOLDING_CELL":
    //     bgcolor = "BASE_GREY";
    //     icon = "icon-slaveralley";
    //     break;
    //   case "GENERIC_HOLDING_CELL":
    //     bgcolor = "BASE_GREY";
    //     icon = "icon-slaveralley";
    //     break;
    // }
    // switch (bgcolor) {
    //   case "BASE_CRIMSON":
    //     o.push("place-bg-base-crimson");
    //     break;
    //   case "BASE_GREY":
    //     o.push("place-bg-base-grey");
    //     break;
    //   case "MAP_BACKGROUND_DANGEROUS":
    //     o.push("place-bg-dangerous");
    //     break;
    //   default:
    //     break;
    // }

    return o;
  }
  addElement(mapEl: Element) {
    var e: HTMLSpanElement = document.createElement("span");
    e.classList.add("room");
  }
}

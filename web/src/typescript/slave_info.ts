/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS206: Consider reworking classes to avoid initClass
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/main/docs/suggestions.md
 */
import { EGender, _GenderInfo } from "./enums/gender";
import { EPlans } from "./enums/plans";
import { EStatus } from "./enums/status";
import { CharacterNames } from "./characternames";
import { ETreatment } from "./enums/treatment";
import { Location } from "./location";
import { SpeciesInfo } from "./species_info";

export class SlaveInfo {
  static COLUMNS = [
    "#", // 0
    "SID", // 1
    "ID", // 2
    "Name", // 3
    "Race", // 4
    "Gender Ident", // 5
    "Calc. Gender", // 6
    "Jobs", // 7
    "Home Loc.", // 8
    "Current Loc.", // 9
    "Affection", // 10
    "Obedience", // 11
    "Value", // 12
    "Value/Day", // 13
    "Plans", // 14
    "Status", // 15
    "Treatment", // 16
    "Tags", // 17
    "Notes", // 18
  ];

  Index: number;
  ID: string;
  SID: string;
  Name: string;
  Names: CharacterNames;
  Species: string;
  GenderIdentity: _GenderInfo;
  DetectedGender: _GenderInfo;
  Jobs: string[];
  HomeLocation: Location;
  HomeLocationName: string;
  CurrentLocation: Location;
  CurrentLocationName: string;
  Obedience: number;
  Plans: string;
  Status: string;
  Treatment: string;
  Notes: string;
  Femininity: number;
  Affection: number;
  Value: number;
  ValuePerDay: number;
  Race: string;
  Tags: string[];
  JobHours: string[];
  PrimaryJob: string;
  Subspecies: string;
  CalculatedRace: string;
  CalculatedRaceStage: string;

  constructor() {
    this.Index = 0;
    this.ID = "";
    this.SID = "";
    this.Name = "";
    this.Names = new CharacterNames();
    this.Species = "";
    this.GenderIdentity = EGender.N_NEUTER;
    this.DetectedGender = EGender.N_NEUTER;
    this.Jobs = [];
    this.HomeLocation = new Location();
    this.HomeLocationName = "";
    this.CurrentLocation = new Location();
    this.CurrentLocationName = "";
    this.Obedience = 0.0;
    this.Plans = EPlans.NONE;
    this.Status = EStatus.IDLE;
    this.Treatment = ETreatment.UNDECIDED;
    this.Notes = "";
    this.Femininity = 0.0;
    this.Affection = 0.0;
    this.Value = 0;
    this.ValuePerDay = 0;
    this.Race = "";
    this.Tags = [];
    this.JobHours = [];
    this.PrimaryJob = "";
    this.Subspecies = "";
    this.CalculatedRace = "";
    this.CalculatedRaceStage = "LESSER";
  }

  fromDict(data: { [key: string]: any }): void {
    this.Index = data["index"];
    this.ID = data["id"];
    this.SID = data["sidstr"];
    this.Names.fromDict(data["names"]);
    this.Species = data["species"];
    this.GenderIdentity =
      EGender.StringToValue(data["genderIdentity"]) ?? EGender.N_NEUTER;
    this.DetectedGender =
      EGender.StringToValue(data["detectedGender"]) ?? EGender.N_NEUTER;
    this.Jobs = data["jobs"];
    this.HomeLocation = Location.FromDict(data["homeLoc"]);
    this.HomeLocationName = data["homeLoc"]["name"];
    this.CurrentLocation = Location.FromDict(data["currentLoc"]);
    this.CurrentLocationName = data["currentLoc"]["name"];
    this.Affection = parseFloat(data["affection"]);
    this.Obedience = data["obedience"];
    this.Plans = data["plans"];
    this.Notes = data["notes"];
    this.Femininity = parseFloat(data["femininity"]);
    this.Status = data["status"];

    this.Race = data["race"];
    this.Subspecies = data["subspecies"];
    this.Value = data["value"];
    this.ValuePerDay = data["vpd"];

    this.Name = data["name"] || this.getName();
    this.Tags = data["tags"] || [];
    this.JobHours = data["jobHours"] || [];
    this.PrimaryJob = data["primaryJob"];
    this.Treatment = data["treatment"];
    this.CalculatedRace = data["calculatedRace"];
    this.CalculatedRaceStage = data["calculatedRaceStage"];
  }

  getName(): string {
    if (this.Femininity <= 39) {
      return this.Names.Masculine;
    } else if (this.Femininity > 60) {
      return this.Names.Feminine;
    }
    return this.Names.Androgynous;
  }

  toRow(): any[] {
    return [
      this.Index,
      this.SID,
      this.ID,
      this.Name,
      new SpeciesInfo(
        this.Species,
        this.CalculatedRaceStage,
        this.CalculatedRace,
        this.Subspecies
      ),
      this.GenderIdentity.slang,
      this.DetectedGender.slang,
      this.Jobs.join(", "),
      this.HomeLocationName,
      this.CurrentLocationName,
      Math.round(this.Affection * 100) / 100,
      Math.round(this.Obedience * 100) / 100,
      this.Value,
      Math.round(this.ValuePerDay),
      EPlans.ValueToString(this.Plans),
      EStatus.ValueToString(this.Status),
      ETreatment.ValueToString(this.Treatment),
      this.Tags.join(", "),
      this.Notes,
    ];
  }
}

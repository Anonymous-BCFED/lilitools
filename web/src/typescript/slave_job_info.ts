
export class _SlaveJobInfo {
  id: string;
  color: string;
  colorHex: string;
  hourlyEventChance: number;
  slaveLimit: number;
  hourlyStaminaDrain: number;
  nameFeminine: string;
  nameMasculine: string;
  description: string;
  affectionGain: number;
  obedienceGain: number;
  constructor(
    id: string,
    color: string,
    colorHex: string,
    hourlyEventChance: number,
    slaveLimit: number,
    hourlyStaminaDrain: number,
    nameFeminine: string,
    nameMasculine: string,
    description: string,
    affectionGain: number,
    obedienceGain: number
  ) {
    this.id = id;
    this.color = color;
    this.colorHex = colorHex;
    this.hourlyEventChance = hourlyEventChance;
    this.slaveLimit = slaveLimit;
    this.hourlyStaminaDrain = hourlyStaminaDrain;
    this.nameFeminine = nameFeminine;
    this.nameMasculine = nameMasculine;
    this.description = description;
    this.affectionGain = affectionGain;
    this.obedienceGain = obedienceGain;
  }
}

export class SpeciesInfo {
  Label: string;
  RaceStage: string;
  Race: string;
  Subspecies: string;
  constructor(label: string, stage: string, race: string, subspecies: string) {
    this.Label = label;
    this.RaceStage = stage;
    this.Race = race;
    this.Subspecies = subspecies;
  }
}

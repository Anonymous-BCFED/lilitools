import { Toast } from "bootstrap";
import { $E } from "./utils";

export class _ToastData {
  classes: string[];
  btnClasses: string[];
  constructor(classes: string[] | null, btnClasses: string[] | null) {
    if (classes == null) {
      classes = [];
    }
    if (btnClasses == null) {
      btnClasses = [];
    }
    this.classes = classes;
    this.btnClasses = btnClasses;
  }
}
export class EToastType {
  static readonly SUCCESS = new _ToastData(
    ["bg-success", "text-white"],
    ["btn-close-white"]
  );
  static readonly WARNING = new _ToastData(
    ["bg-warning", "text-dark"],
    ["btn-close-dark"]
  );
  static readonly DANGER = new _ToastData(
    ["bg-danger", "text-white"],
    ["btn-close-white"]
  );
}

export class ToastManager {
  container: JQuery;
  constructor($body: JQuery) {
    this.container = $E("div", { class: "toast-container" }).appendTo($body);
  }
  success(message: string) {
    return this.add(message, EToastType.SUCCESS);
  }
  danger(message: string) {
    return this.add(message, EToastType.DANGER);
  }
  warning(message: string) {
    return this.add(message, EToastType.WARNING);
  }
  add(message: string, type: _ToastData) {
    var toastElem = $E("div", {
      class: ["toast", "align-items-center", "border-0"]
        .concat(type.classes)
        .join(" "),
      role: "alert",
      "aria-live": "assertive",
      "aria-atomic": "true",
    });
    var toastFlex = $E("div", { class: "d-flex" }).appendTo(toastElem);
    var toastBody = $E("div", { class: "toast-body" })
      .appendTo(toastFlex)
      .text(message);
    $E("button", {
      type: "button",
      class: ["btn-close", "me-2", "m-auto"].concat(type.btnClasses).join(" "),
    }).appendTo(toastFlex);
    toastElem.appendTo(this.container);
    new Toast(toastElem.get(0) as HTMLElement).show();
  }
}

import $ from "jquery";

import { BOOTSTRAP_ICONS } from "./_GEN_bsicons";
import { HtmlTagObject } from "html-webpack-plugin";

class AssertionFailure extends Error {}
export function assert(value: boolean, msg: string = "Assertion failed"): void {
  if (console) {
    console.assert(msg);
  } else {
    if (!value) {
      throw new AssertionFailure(msg);
    }
  }
}

export function assertNotNull<T>(
  value: T,
  varName: string = "value"
): asserts value is NonNullable<T> {
  assert(value !== null, `${varName} is null`);
}

export function assertNotUndefined<T>(
  value: T,
  varName: string = "value"
): asserts value is NonNullable<T> {
  assert(value !== undefined, `${varName} is null`);
}

export function $new(tag: string): JQuery {
  return $(document.createElement(tag));
}

export function enumToSelDict(etype: any): { [k: string]: string } {
  var planOptions: { [k: string]: string } = {};
  etype.Keys().forEach((o: string) => {
    planOptions[o] = o;
  });
  return planOptions;
}

export function $E(
  tag: string,
  attr: { [key: string]: string } | null = null,
  text: string | null = null
) {
  var e = $(document.createElement(tag));
  if (attr) {
    for (
      var _i = 0, _a = Array.from(Object.entries(attr));
      _i < _a.length;
      _i++
    ) {
      var _b = _a[_i],
        k = _b[0],
        v = _b[1];
      e.attr(k, v);
    }
  }
  if (text) {
    e.text(text);
  }
  return e;
}
export function $img(data: string): JQuery {
  var i = new Image();
  i.src = data;
  return $(i);
}
export function $bsicon(iconID: string): JQuery {
  return $img(BOOTSTRAP_ICONS[iconID]).addClass("bs-icon");
}
export function numberWithCommas(x: number) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
export function $mk_fa_i(cls: string): JQuery {
  // <i class="fas fa-plus"></i>
  var el = document.createElement("i");
  el.className = cls;
  return $(el);
}
export function $mk_fa_i_str(cls: string): string {
  // <i class="fas fa-plus"></i>
  return `<i class="${cls}"></i>`;
}
export enum EFontAwesomeStyle {
  SOLID = "fa-solid",
  REGULAR = "fa-regular",
  LIGHT = "fa-light",
  DUOTONE = "fa-duotone",
  THIN = "fa-thin",
  SHARP_SOLID = "fa-sharp fa-solid",
  SHARP_REGULAR = "fa-sharp fa-regular",
  SHARP_LIGHT = "fa-sharp fa-light",
  BRANDS = "fa-brands",
}
export function $mkFontAwesome(
  icon_id: string,
  fa_style: string = EFontAwesomeStyle.REGULAR,
  color: string | null = null,
  tag: string = "i"
): JQuery {
  var el = document.createElement(tag);
  el.className = `${fa_style} fa-${icon_id}`;
  if (color !== null) {
    el.style.color = color;
  }
  return $(el);
}
export function setCSSVariable(varName: string, value: string) {
  console.log(`Setting ${varName} to ${value}`);
  let sel = null;
  if ((sel = document.querySelector(":root")) == null) return null;
  (sel as HTMLHtmlElement).style.setProperty(varName, value);
}

export function range(left: number, right: number, inclusive: boolean) {
  var range = [];
  var ascending = left < right;
  var end = !inclusive ? right : ascending ? right + 1 : right - 1;
  for (var i = left; ascending ? i < end : i > end; ascending ? i++ : i--) {
    range.push(i);
  }
  return range;
}

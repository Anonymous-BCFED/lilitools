const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const SimpleProgressWebpackPlugin = require('simple-progress-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HTMLInlineCSSWebpackPlugin = require("html-inline-css-webpack-plugin").default;

const autoprefixer = require('autoprefixer');


module.exports = {
    context: path.resolve(__dirname),
    entry: {
        index: ['./src/index.html', './src/typescript/index.ts'],
    },
    performance: {
        hints: false,
        maxEntrypointSize: 512000,
        maxAssetSize: 512000
    },
    resolve: {
        extensions: ['.ts', '.js'],
    },
    module: {
        rules: [
            {
                test: /\.html$/,
                loader: "html-loader",
            },
            {
                test: /\.scss$/i,
                use: [
                    // Creates `style` nodes from JS strings
                    "style-loader",
                    // Translates CSS into CommonJS
                    "css-loader",
                    {
                        // Loader for webpack to process CSS with PostCSS
                        loader: 'postcss-loader',
                        options: {
                            postcssOptions: {
                                plugins: [
                                    autoprefixer
                                ]
                            }
                        }
                    },

                    // Compiles Sass to CSS
                    "sass-loader",
                ],
            },
            // {
            //     test: /\.coffee$/,
            //     loader: "coffee-loader",
            // },
            {
                test: /\.ts$/,
                loader: "ts-loader",
            },
            {
                test: /\.css$/i,
                use: [
                    "style-loader",
                    "css-loader",
                    {
                        // Loader for webpack to process CSS with PostCSS
                        loader: 'postcss-loader',
                        options: {
                            postcssOptions: {
                                plugins: [
                                    autoprefixer
                                ]
                            }
                        }
                    }
                ],
            },
            {

                test: /\.(png|jpg|jpeg|gif|svg)$/i,
                type: 'asset/resource',
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                include: path.resolve(__dirname, './node_modules/bootstrap-icons/font/fonts'),
                use: {
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'webfonts',
                        publicPath: '../webfonts',
                    },
                }
            }
        ],
    },

    plugins: [
        new SimpleProgressWebpackPlugin(),
        new HtmlWebpackPlugin({
            title: 'Slavesheet v0.0.1',
            inject: true,
            template: 'src/index.html'
        }),
    ],
    output: {
        filename: '[name]-[fullhash].bundle.js',
        path: path.resolve(__dirname, 'static'),
        clean: true,
        publicPath: '/static/',
        assetModuleFilename: 'images/[hash][ext][query]'
    },
};